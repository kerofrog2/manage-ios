//
//  ArchiveList.h
//  Manage
//
//  Created by Cliff Viegas on 3/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class ListItem;

// Data Collections
@class TaskListCollection;

@interface ArchiveList : NSObject {
	NSMutableArray		*listItems;					// To store all archived list items
	NSMutableArray		*lists;						// To store all archived lists
}

@property (nonatomic, retain)		NSMutableArray		*listItems;
@property (nonatomic, retain)		NSMutableArray		*lists;

#pragma mark Initialisation
- (id)initWithArchivedListItems;

#pragma mark Load Methods
- (void)loadArchivedListItems;
- (void)loadBasicArchivedLists;

#pragma mark Delete Methods
- (void)deleteAllArchives;
- (void)deleteListAtIndex:(NSInteger)index permanent:(BOOL)isPermanent;
- (void)deleteListItemAtIndex:(NSInteger)index permanent:(BOOL)isPermanent;

#pragma mark Get Methods
- (NSInteger)getIndexOfListItemWithID:(NSInteger)listItemID;
- (NSInteger)getIndexOfListItemWithToodledoServerID:(SInt64)theToodledoServerID;

#pragma mark Query Methods
- (BOOL)doesListItemExistWithToodledoServerID:(SInt64)theToodledoServerID;

#pragma mark Helper Methods
- (void)updateTaskListCollection:(TaskListCollection *)taskListCollection withListsCreatedAfter:(NSString *)dateTimeString;
- (void)updateTaskListCollection:(TaskListCollection *)taskListCollection withListItemsCreatedAfter:(NSString *)dateTimeString getNonSynced:(BOOL)getNonSynced;
- (void)updateTaskListCollection:(TaskListCollection *)taskListCollection withListItemsModifiedAfter:(NSString *)dateTimeString;
- (NSInteger)getIndexOfTaskListWithToodledoFolderID:(NSInteger)theToodledoFolderID;
- (NSInteger)getIndexOfTaskListWithID:(NSInteger)theListID;
- (NSString *)getParentListTitleOfListItemWithParentListID:(NSInteger)parentListID usingMasterTaskListCollection:(TaskListCollection *)masterTaskListCollection;
- (BOOL)hasSubtasksForListItemAtIndex:(NSInteger)listItemIndex;

@end
