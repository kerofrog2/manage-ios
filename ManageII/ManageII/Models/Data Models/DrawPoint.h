//
//  DrawPoint.h
//  Manage
//
//  Created by Cliff Viegas on 23/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

#define	kNoPoint		-500

@interface DrawPoint : NSObject {
	CGFloat		x;
	CGFloat		y;
}

@property (nonatomic, assign)	CGFloat	x;
@property (nonatomic, assign)	CGFloat y;

#pragma mark Update Methods
- (void)updateWithCGPoint:(CGPoint)newPoint;

@end
