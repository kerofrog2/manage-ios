//
//  Notebook.m
//  Manage
//
//  Created by Cliff Viegas on 17/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "Notebook.h"

// Method Helper
#import "MethodHelper.h"

// Data Model
#import "TaskList.h"

// Business Layer
#import "BLNotebook.h"

@implementation Notebook

@synthesize notebookID;
@synthesize title;
@synthesize creationDate;
@synthesize itemOrder;
@synthesize lists;
@synthesize isDirty;
@synthesize selectedListIndex;

#pragma mark - Memory Management

- (void)dealloc {
//    [title release];
//    [creationDate release];
//    //[lists release];
//    [super dealloc];
}

#pragma mark - Initialisation

- (id)init {
    self = [super init];
    if (self) {
        self.lists = [[NSMutableArray alloc] init];// autorelease];
    }
    return self;
}

- (id)initWithNotebookID:(NSInteger)theNotebookID title:(NSString *)theTitle itemOrder:(NSInteger)theItemOrder lists:(NSString *)theLists {
    self = [super init];
    if (self) {
        self.notebookID = theNotebookID;
        self.title = theTitle;
        self.itemOrder = theItemOrder;
        self.creationDate = @"";
        self.isDirty = FALSE;
        self.selectedListIndex = 0;
        
        // Populate the lists
        self.lists = [[NSMutableArray alloc] initWithArray:[theLists componentsSeparatedByString:@","] copyItems:YES];// autorelease];
    }
    return self;
}

// Creates a new notebook
- (id)initNewNotebook {
	if ((self = [super init])) {
        
		self.lists = [[NSMutableArray alloc] initWithCapacity:5];// autorelease];
		self.title = @"New Category";//** Folder
        self.itemOrder = [BLNotebook getNextNotebookOrder];
        self.creationDate = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATEONLY_FORMAT];
        self.isDirty = FALSE;
        self.selectedListIndex = 0;
        self.notebookID = [BLNotebook insertNewNotebook:self];
	}
	return self;
}

#pragma mark - Load Methods

- (void)loadLists:(NSString *)theLists {
    // Remove any existing lists
    [self.lists removeAllObjects];
    
    // Get rid of any spaces
    theLists = [theLists stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // Create a local array
    NSArray *localArray = [theLists componentsSeparatedByString:@","];

    for (NSString *list in localArray) {
        if ([list length] > 0) {
            [self.lists addObject:list];
        }
    }
}

#pragma mark - Get Methods

- (NSInteger)getIndexOfListWithID:(NSInteger)theListID {
    for (NSInteger i = 0; i < [self.lists count]; i++) {
        NSString *listIDString = [self.lists objectAtIndex:i];
        if ([listIDString integerValue] == theListID) {
            return i;
        }
    }
    
    // Failed, not found
    return -1;
}

#pragma mark - Delete Methods

- (BOOL)removeInstancesOfListID:(NSInteger)theListID {
    // Have to go backwards
    for (NSInteger i = [self.lists count] - 1; i >= 0; i--) {
        NSString *listID = [self.lists objectAtIndex:i];
        NSString *theListIDString = [NSString stringWithFormat:@"%d", theListID];
        if ([listID isEqualToString:theListIDString]) {
            [self.lists removeObjectAtIndex:i];
            self.isDirty = TRUE;
        }
    }
    
    if ([self isDirty] == TRUE) {
        [self updateDatabase];
        self.isDirty = FALSE;
        return TRUE;
    } 
    
    return FALSE;
}

- (BOOL)removeInstancesOfListIDNotFoundInArray:(NSArray *)taskListsArray {
    for (NSInteger i = [self.lists count] - 1; i >= 0; i--) {
        BOOL found = FALSE;

        NSString *listID = [self.lists objectAtIndex:i];
        
        for (TaskList *taskList in taskListsArray) {
            NSString *theListIDString = [NSString stringWithFormat:@"%d", taskList.listID];
            if ([listID isEqualToString:theListIDString]) {
                found = TRUE;
                break;
            }
        }
        
        if (found == FALSE) {
            [self.lists removeObjectAtIndex:i];
            self.isDirty = TRUE;
        }
    }
    
    if ([self isDirty] == TRUE) {
        [self updateDatabase];
        self.isDirty = FALSE;
        return TRUE;
    } 
    
    return FALSE;
}

#pragma mark - Check Methods

- (BOOL)containsListWithID:(NSInteger)theListID {
    for (NSString *listIDString in self.lists) {
        NSInteger listID = [listIDString integerValue];
        if (listID == theListID) {
            return TRUE;
        }
    }
    return FALSE;
}

#pragma mark - Database Methods

- (void)updateDatabase {
    [BLNotebook updateDatabaseWithNotebook:self];
}
// TODO: Whenever a list is deleted need to also remove reference to it...
// This is done within the self.lists array, which will then need to be updated in DB... hmm...  Can call DB update from here too, which will then do the conversion in
// the business layer

@end
