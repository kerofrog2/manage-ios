//
//  DeletedListItem.m
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "DeletedListItem.h"

// Business Layer
#import "BLDeletedListItem.h"

// Data Models
#import "ListItem.h"

// Method Helper
#import "MethodHelper.h"

@implementation DeletedListItem

@synthesize listItemID;
@synthesize title;
@synthesize listID;
@synthesize parentListItemID;
@synthesize toodledoTaskID;
@synthesize deletedDateTime;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[title release];
//	[deletedDateTime release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		
	}
	return self;
}

#pragma mark -
#pragma mark Public Methods

- (void)softDeleteListItem:(ListItem *)listItem {
	self.listItemID = listItem.listItemID;
	self.title = listItem.title;
	self.listID = listItem.listID;
	self.parentListItemID = listItem.parentListItemID;
	self.toodledoTaskID = listItem.toodledoTaskID;
	self.deletedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	
	[BLDeletedListItem insertNewDeletedListItem:self];
}

+ (void)emptyDeletedListItemDatabase {
	[BLDeletedListItem emptyDatabase];
}

@end
