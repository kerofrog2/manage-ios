//
//  List.h
//  Manage
//
//  Created by Cliff Viegas on 17/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreText/CoreText.h>


// Data Models
@class ListItem;
@class ArchiveList;

// Data Collections
@class TagCollection;

// CREATE TABLE "List" ("ListID" INTEGER PRIMARY KEY  NOT NULL , "Title" VARCHAR NOT NULL  DEFAULT 'List', "CreationDate" VARCHAR)

typedef enum {
	EnumSortByList = 0,
	EnumSortByPriority = 1,
	EnumSortByDueDate = 2,
	EnumSortByCompleted = 3
} EnumSortBy;

// Constants (task object uses this)
#define kTotalTaskSectionHeight		616

@interface TaskList : NSObject {
	NSMutableArray		*listItems;
	NSInteger			listID;
	NSString			*title;
	NSString			*creationDate;
	NSString			*screenshot;
	NSInteger			currentSortType;
	NSInteger			lastSortType;
	NSInteger			listOrder;
	BOOL				completed;
	NSString			*completedDateTime;
	BOOL				archived;
	NSString			*archivedDateTime;
	NSInteger			toodledoFolderID;
	NSString			*creationDateTime;
	
	// Helper methods
	BOOL				mainViewRequiresUpdate;
	BOOL				markedForDeletion;
	
	// Only used for displaying tags when exporting to pdf
	CGRect				taskItemFrame;
	NSInteger			currentRowHeight;
	NSInteger			m_totalHeight;
    
    // 2.0 properties
    BOOL                isNote;
    NSString            *scribble;
   // NSDictionary        *glScribbleDictionary;
}

@property	(nonatomic, retain)		NSMutableArray		*listItems;
@property	(nonatomic, assign)		NSInteger			listID;
@property	(nonatomic, copy)		NSString			*title;
@property	(nonatomic, copy)		NSString			*creationDate;
@property	(nonatomic, copy)		NSString			*screenshot;
@property	(nonatomic, assign)		NSInteger			currentSortType;
@property	(nonatomic, assign)		NSInteger			lastSortType;
@property	(nonatomic, assign)		NSInteger			listOrder;
@property	(nonatomic, assign)		BOOL				completed;
@property	(nonatomic, copy)		NSString			*completedDateTime;
@property	(nonatomic, assign)		BOOL				archived;
@property	(nonatomic, copy)		NSString			*archivedDateTime;
@property	(nonatomic, assign)		NSInteger			toodledoFolderID;
@property	(nonatomic, copy)		NSString			*creationDateTime;

// Helper methods
@property	(nonatomic, assign)		BOOL				markedForDeletion;
@property	(nonatomic, assign)		BOOL				mainViewRequiresUpdate;
@property	(nonatomic, assign)		CGRect				taskItemFrame;
@property	(nonatomic, assign)		NSInteger			currentRowHeight;
@property	(nonatomic, assign)		NSInteger			m_totalHeight;

// 2.0 properties
@property (nonatomic, assign)       BOOL                isNote;
@property (nonatomic, copy)         NSString            *scribble;
//@property (nonatomic, retain)       NSDictionary        *glScribbleDictionary;

#pragma mark Initialisation
- (id)initExistingWithListID:(NSInteger)aListID andTitle:(NSString *)theTitle;
- (id)initNewList;
- (id)initNewNote;
- (id)initWithTaskListCopy:(TaskList *)taskListCopy;
- (id)initWithDuplicateTaskList:(TaskList *)taskListCopy;
- (id)initWithBasicTaskListCopy:(TaskList *)taskListCopy;

#pragma mark Get Methods
- (NSInteger)getIndexOfListItemWithID:(NSInteger)listItemID;
- (BOOL)canArchiveSubListItemAtIndex:(NSInteger)listItemIndex;
- (BOOL)hasSubtasksForListItemAtIndex:(NSInteger)listItemIndex;
- (BOOL)hasIncompleteSubtasksForListItemAtIndex:(NSInteger)listItemIndex;
- (BOOL)hasIncompleteSubtasksForListItemAtIndex:(NSInteger)listItemIndex forCompletedDuration:(NSInteger)completedDuration 
						 andShowCompletedStatus:(BOOL)showCompletedStatus;
- (NSInteger)getCountOfSubtasksForListItemAtIndex:(NSInteger)listItemIndex;
- (NSInteger)getNextListItemOrder;
- (NSInteger)getFirstListItemOrder;
- (ListItem *)getListItemReferenceFromListItemWithToodledoTaskID:(SInt64)theTaskID;
+ (NSInteger)getCountOfListsWithTitle:(NSString *)theTitle;
- (NSInteger)getCountOfNonOverdueTasks;
- (NSInteger)getCountOfOverdueTasks;

#pragma mark Delete Methods
- (void)deleteListItemAtIndex:(NSInteger)index permanent:(BOOL)isPermanent;
- (void)deleteListItemRetainScribbleAtIndex:(NSInteger)index permanent:(BOOL)isPermanent;

#pragma mark Move Methods
- (BOOL)moveListItemFrom:(NSInteger)fromIndex to:(NSInteger)toIndex;

#pragma mark Update Database
- (void)updateDatabase;
- (void)updateDatabaseWithoutEditLog;
- (void)updateListOrder;

#pragma mark Archive Methods
- (NSInteger)archiveAllCompletedItems:(BOOL)getArchivedCount;
- (NSInteger)archiveAllCompletedItemsUsingDuration:(NSInteger)duration;
- (NSInteger)archiveAllCompletedItemsToArchiveList:(ArchiveList *)archiveList;
- (void)archiveAllItems;
- (void)archiveAllItemsToArchiveList:(ArchiveList *)archiveList;
- (void)archiveListItemAtIndex:(NSInteger)index;

#pragma mark Mail Methods
- (NSString *)getTextMailMessageWithTagCollection:(TagCollection *)tagCollection showSubtasks:(BOOL)showSubtasks;

#pragma mark PDF Mail Methods
- (NSString *)createPDFMessageFromNote;
- (NSString *)createPDFMessageWithTagCollection:(TagCollection *)tagCollection showSubtasks:(BOOL)showSubtasks;
- (void)drawDueDateAtIndex:(NSInteger)index forListItem:(ListItem *)listItem andTagsView:(UIView *)tagsView;
- (void)drawHighlighterAtIndex:(NSInteger)index forListItem:(ListItem *)listItem showSubtasks:(BOOL)showSubtasks;
- (CFRange)renderPage:(NSInteger)pageNum withTextRange:(CFRange)currentRange
	   andFramesetter:(CTFramesetterRef)framesetter;
- (void)drawPageNumber:(NSInteger)pageNum;
- (UIView *)allocTagsForListItem:(ListItem *)listItem andTagCollection:(TagCollection *)tagCollection forRow:(NSInteger)row 
			   andTaskItemFrame:(CGRect)textRect andHasDate:(BOOL)hasDate ;

#pragma mark Repeating Tasks Methods
- (ListItem *)allocProcessRepeatingTaskAtIndex:(NSInteger)index;

#pragma mark Class Methods
- (NSInteger)getCompletedDateDifferenceFromToday;
- (NSInteger)getNextListOrder;
- (void)updateSubtasksListItemOrderForListItem:(ListItem *)parentListItem;
- (void)updateSubtasksSubListItemAndListItemOrderForListItem:(ListItem *)parentListItem;
- (void)updateListItemOrderFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex ignoringParentListItemID:(NSInteger)parentListItemID;
- (void)updateSubListItemOrderFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex; 
- (void)sortListWithSortType:(NSInteger)sortType;
- (void)updateNewParentForSubTask:(ListItem *)subTaskItem atIndex:(NSInteger)subTaskIndex;

@end
