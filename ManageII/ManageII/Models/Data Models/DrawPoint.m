//
//  DrawPoint.m
//  Manage
//
//  Created by Cliff Viegas on 23/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "DrawPoint.h"


@implementation DrawPoint

@synthesize x;
@synthesize y;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		self.x = kNoPoint;
		self.y = kNoPoint;
	}
	return self;
}

#pragma mark -
#pragma mark Update Methods

- (void)updateWithCGPoint:(CGPoint)newPoint {
	self.x = newPoint.x;
	self.y = newPoint.y;
}

@end
