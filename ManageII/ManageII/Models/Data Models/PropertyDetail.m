//
//  PropertyDetail.m
//  Manage
//
//  Created by Cliff Viegas on 5/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "PropertyDetail.h"


@implementation PropertyDetail

@synthesize name;
@synthesize friendlyName;
@synthesize section;
@synthesize propertyType;
@synthesize description;
@synthesize enabled;
@synthesize list;

#pragma mark -
#pragma mark Memory Management

//- (void)dealloc {
//	[name release];
//	[friendlyName release];
//	[section release];
//	//[list release];
//	[description release];
//	[super dealloc];
//}

#pragma mark -
#pragma mark Initialisation

- (id)initWithName:(NSString *)aName andFriendlyName:(NSString *)aFriendlyName
		andSection:(NSString *)aSection andPropertyType:(NSInteger)aType {
	if (self = [super init]) {
		self.name = aName;
		self.friendlyName = aFriendlyName;
		self.section = aSection;
		self.propertyType = aType;
		self.enabled = TRUE;
		self.list = [[NSMutableArray alloc] init];//autorelease
		self.description = @"";
	}
	return self;
}

- (id)initWithName:(NSString *)aName andFriendlyName:(NSString *)aFriendlyName
		andSection:(NSString *)aSection andPropertyType:(NSInteger)aType andEnabled:(BOOL)isEnabled {
	if (self = [super init]) {
		self.name = aName;
		self.friendlyName = aFriendlyName;
		self.section = aSection;
		self.propertyType = aType;
		self.enabled = isEnabled;
		self.list = [[NSMutableArray alloc] init];//autorelease
		self.description = @"";
	}
	return self;
}

@end
