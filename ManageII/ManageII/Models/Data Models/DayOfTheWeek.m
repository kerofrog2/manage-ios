//
//  DayOfTheWeek.m
//  Manage
//
//  Created by Cliff Viegas on 10/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "DayOfTheWeek.h"


@implementation DayOfTheWeek

@synthesize title;
@synthesize selected;
@synthesize compValue;	// Value used in comparators for NSCalender

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[title release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		self.title = @"";
		self.selected = FALSE;
		self.compValue = 0;
	}
	return self;
}

- (id)initWithTitle:(NSString *)theTitle andSelected:(BOOL)isSelected andCompValue:(NSInteger)theCompValue {
	if (self = [super init]) {
		self.title = theTitle;
		self.selected = isSelected;
		self.compValue = theCompValue;
	}
	return self;
}

@end
