//
//  RecurringListItem.m
//  Manage
//
//  Created by Cliff Viegas on 9/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "RecurringListItem.h"

// Business Layer
#import "BLRecurringListItem.h"

// Method Helper
#import "MethodHelper.h"

// DM Helper
#import "DMHelper.h"

// Data Models
#import "ListItem.h"
#import "DayOfTheWeek.h"

@implementation RecurringListItem

@synthesize recurringListItemID;
@synthesize frequency;
@synthesize frequencyMeasurement;
@synthesize daysOfTheWeek;
@synthesize useCompletedDate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[frequencyMeasurement release];
//	[daysOfTheWeek release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if ((self = [super init])) {
		self.recurringListItemID = -1;
		//self.hostListID = theListItem.listID;
		self.frequency = 0;
		self.frequencyMeasurement = @"";
		self.daysOfTheWeek = @"";
		self.useCompletedDate = TRUE;
		/*self.lastPushDate = @"";
		self.startPushDate = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATEONLY_FORMAT];
		self.notes = theListItem.notes;
		self.title = theListItem.title;
		
		// Need a duplicate scribble
		self.scribble = theListItem.scribble;*/
	}
	return self;
}

// Init with a recurring list item id that has been passed
- (id)initWithRecurringListItemID:(NSInteger)theRecurringListItemID {
	if ((self = [super init])) {
		NSDictionary *recurringItemDictionary = [BLRecurringListItem getRecurringListItemWithID:theRecurringListItemID];
		
		self.recurringListItemID = [DMHelper getNSIntegerValueForKey:@"RecurringListItemID" fromDictionary:recurringItemDictionary];
		//self.hostListID = [DMHelper getNSIntegerValueForKey:@"HostListID" fromDictionary:recurringItemDictionary];
		self.frequency = [DMHelper getNSIntegerValueForKey:@"Frequency" fromDictionary:recurringItemDictionary];
		self.frequencyMeasurement = [DMHelper getNSStringValueForKey:@"FrequencyMeasurement" fromDictionary:recurringItemDictionary];
		self.daysOfTheWeek = [DMHelper getNSStringValueForKey:@"DaysOfTheWeek" fromDictionary:recurringItemDictionary];
		self.useCompletedDate = [DMHelper getBoolValueForKey:@"UseCompletedDate" fromDictionary:recurringItemDictionary];
		/*self.startPushDate = [DMHelper getNSStringValueForKey:@"StartPushDate" fromDictionary:recurringItemDictionary];
		self.lastPushDate = [DMHelper getNSStringValueForKey:@"LastPushDate" fromDictionary:recurringItemDictionary];
		self.title = [DMHelper getNSStringValueForKey:@"Title" fromDictionary:recurringItemDictionary];
		self.notes = [DMHelper getNSStringValueForKey:@"Notes" fromDictionary:recurringItemDictionary];
		self.scribble = [DMHelper getNSStringValueForKey:@"Scribble" fromDictionary:recurringItemDictionary];*/
	}
	return self;
}


#pragma mark -
#pragma mark Load Methods

- (void)loadUsingToodledoRepeatString:(NSString *)repeatString repeatFrom:(NSInteger)repeatFrom {
	NSArray *words = [repeatString componentsSeparatedByString:@" "];
	
	if ([[words objectAtIndex:0] isEqualToString:@"The"]) {
		NSString *ordinal = [words objectAtIndex:1];
		ordinal = [self getWeekOrdinalStringFromToodledoString:ordinal];
		self.frequencyMeasurement = ordinal;
		
		NSString *weekday = [words objectAtIndex:2];
		weekday = [self replaceShortenedToodledoWeekday:weekday];
		self.daysOfTheWeek = weekday;
		
	} else if ([[words objectAtIndex:0] isEqualToString:@"Every"]) {
		NSString *word = [words objectAtIndex:1];
		word = [word stringByReplacingOccurrencesOfString:@"," withString:@""];
		if ([word isEqualToString:@"Mon"] || [word isEqualToString:@"Tue"] || [word isEqualToString:@"Wed"]
			|| [word isEqualToString:@"Thu"] || [word isEqualToString:@"Fri"]
			|| [word isEqualToString:@"Sat"] || [word isEqualToString:@"Sun"]) {
			
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Every " withString:@""];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Mon" withString:@"Monday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Tue" withString:@"Tuesday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Wed" withString:@"Wednesday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Thu" withString:@"Thursday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Fri" withString:@"Friday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Sat" withString:@"Saturday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Sun" withString:@"Sunday"];
			
			[self setDaysOfTheWeek:repeatString];
			
		} else {
			[self setFrequency:[[words objectAtIndex:1] integerValue]];
			NSString *measurement = [words objectAtIndex:2];
			
			measurement = [measurement capitalizedString];
			
			if ([measurement isEqualToString:@"Day"]) {
				measurement = @"Days";
			} else if ([measurement isEqualToString:@"Week"]) {
				measurement = @"Weeks";
			} else if ([measurement isEqualToString:@"Month"]) {
				measurement = @"Months";
			} else if ([measurement isEqualToString:@"Year"]) {
				measurement = @"Years";
			}
			[self setFrequencyMeasurement:measurement];
		}
	}
	
	if (repeatFrom == 0) {
		// From due date
		[self setUseCompletedDate:FALSE];
	} else {
		// From completed date
		[self setUseCompletedDate:TRUE];
	}
}

#pragma mark -
#pragma mark Update Methods



#pragma mark -
#pragma mark Delete Methods

/*- (void)deleteCurrentScribble {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSString *scribbleName = [self scribble];
	if ([scribbleName length] == 0) {
		return;
	}
	
	NSString *scribblePath = [NSString stringWithFormat:@"%@%@",
							  [MethodHelper getScribblePath], scribbleName];
	
	if ([fileManager fileExistsAtPath:scribblePath]) {
		NSError *deleteFileError = nil;
		
		[fileManager removeItemAtPath:scribblePath error:&deleteFileError];
		
		if (deleteFileError) {
			NSLog(@"%@", [deleteFileError localizedDescription]);
		}
	}
	
	self.scribble = @"";
}*/

#pragma mark -
#pragma mark Class Methods

- (NSDate *)getNewDueDateUsingExistingDueDate:(NSString *)existingDueDate {
	NSMutableArray *weekdaysArray = [self getWeekdaysArrayCopy];// retain];

	BOOL useCompletionDate = [self useCompletedDate];
	
	// Just in case there is no due date, we will be forced to use completion date
	if (useCompletionDate == FALSE && [existingDueDate length] == 0) {
		useCompletionDate = TRUE;
	}
	
	// Get the start date
	NSDate *startDate = [NSDate date];
	
	if (useCompletionDate == FALSE) {
		startDate = [MethodHelper dateFromString:existingDueDate usingFormat:K_DATEONLY_FORMAT];
	}
	
	// This is going to move forward, just need to work out how much
	NSInteger daysToMoveForward = 0;
	NSInteger weeksToMoveForward = 0;
	NSInteger monthsToMoveForward = 0;
	NSInteger yearsToMoveForward = 0;
	
	// Get the new date
	//unsigned units = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components = [calendar components:NSWeekdayCalendarUnit | NSWeekCalendarUnit fromDate:startDate];
	
	
	// Init the new date
	
	
	if ([self.frequencyMeasurement isEqualToString:@""] && [self.daysOfTheWeek isEqualToString:@""]) {
		daysToMoveForward = self.frequency;
	} else if ([self.frequencyMeasurement isEqualToString:@""] == FALSE && [self.daysOfTheWeek isEqualToString:@""]) {
		if ([self.frequencyMeasurement isEqualToString:@"Days"]) {
			daysToMoveForward = self.frequency;
		} else if ([self.frequencyMeasurement isEqualToString:@"Weeks"]) {
			weeksToMoveForward = self.frequency;
		} else if ([self.frequencyMeasurement isEqualToString:@"Months"]) {
			monthsToMoveForward = self.frequency;
		} else if ([self.frequencyMeasurement isEqualToString:@"Years"]) {
			yearsToMoveForward = self.frequency;
		}
	} else if ([self.daysOfTheWeek isEqualToString:@""] == FALSE 
			   && [self.frequencyMeasurement isEqualToString:@""] == TRUE) {
		// Set which days of the week are selected
		[self setSelectedWeekdaysForArray:weekdaysArray];
		
		// Get days difference
		daysToMoveForward = [self getDayDifferenceUsingCurrentDay:[components weekday] andWeekdaysArray:weekdaysArray];
	} else if ([self frequency] == 0 && [self.frequencyMeasurement length] > 0
			   && [self.daysOfTheWeek length] > 0) {
		//NSRange days = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:startDate];
		// Days in the current month
	
		
		NSDate *newDate = [self getNewDateForXDOfEachMonthUsingCalendar:(NSCalendar *)calendar andStartDate:startDate];
		
//		[weekdaysArray release];
		
		return newDate;
		
	}
	
	if (weeksToMoveForward > 0) {
		daysToMoveForward = weeksToMoveForward * 7;
	}
	
	// Need new blank components
	NSDateComponents *newComponents = [[NSDateComponents alloc] init];
	[newComponents setDay:daysToMoveForward];
	[newComponents setMonth:monthsToMoveForward];
	[newComponents setYear:yearsToMoveForward];
	
	
	
	NSDate *newDate = [calendar dateByAddingComponents:newComponents toDate:startDate options:0];
//	[newComponents release];
	
	// Release week days array
//	[weekdaysArray release];
	
	return newDate;
}

- (NSDate *)getNewDateForXDOfEachMonthUsingCalendar:(NSCalendar *)calendar andStartDate:(NSDate *)startDate {
	// Get the components of the start date
	NSDateComponents *startDateComponents = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit | NSDayCalendarUnit) fromDate:startDate];
	
	// Get new date components too
	NSDateComponents *newDateComponents = [[NSDateComponents alloc] init];
	[newDateComponents setYear:[startDateComponents year]];
	[newDateComponents setMonth:[startDateComponents month]];

	// Get the new weekday ordinal
	NSInteger newWeekday = [self getWeekdayNumberFromWeekday:[self daysOfTheWeek]];
	NSInteger theWeekdayOrdinal = [self getWeekNumberFromWeekString:[self frequencyMeasurement]];
	if (theWeekdayOrdinal == 0) {
		// Do something to make sure we on last week
		theWeekdayOrdinal = [self getLastWeekdayOrdinalWithCalendar:calendar weekday:newWeekday month:[startDateComponents month] 
															   year:[startDateComponents year]];
	}
	[newDateComponents setWeekdayOrdinal:theWeekdayOrdinal];
	
	// Set the weekday
	[newDateComponents setWeekday:newWeekday];

	// Set the new date
	NSDate *newDate = [calendar dateFromComponents:newDateComponents];
	
//	[newDateComponents release];
	
	// Get the updated date components
	NSDateComponents *updatedNewDateComponents = [calendar components:(NSDayCalendarUnit) fromDate:newDate];
	
	//NSLog(@"new start date components date is: %@", [MethodHelper stringFromDate:newDate usingFormat:K_DATEONLY_FORMAT]);
	
	// Now need to test whether this is before start date
	if ([startDateComponents day] >= [updatedNewDateComponents day]) {
		// It does, so we need to move it to next month
		NSDateComponents *moveForwardComponents = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekdayOrdinalCalendarUnit | NSWeekdayCalendarUnit) fromDate:newDate];
		[moveForwardComponents setMonth:[moveForwardComponents month] + 1];
		[moveForwardComponents setWeekday:newWeekday];
		
		// Get the new weekday ordinal
		NSInteger theWeekdayOrdinal = [self getWeekNumberFromWeekString:[self frequencyMeasurement]];
		if (theWeekdayOrdinal == 0) {
			// Do something to make sure we on last week
			theWeekdayOrdinal = [self getLastWeekdayOrdinalWithCalendar:calendar weekday:[moveForwardComponents weekday] month:[moveForwardComponents month] 
																   year:[moveForwardComponents year]];
		}
		[moveForwardComponents setWeekdayOrdinal:theWeekdayOrdinal];

		newDate = [calendar dateFromComponents:moveForwardComponents];
		//newDate = [calendar dateByAddingComponents:moveForwardComponents 
		//									toDate:newDate 
		//								   options:0];
	}

	
	return newDate;
	// Create a new set of components that we can modify
	/*NSDateComponents *comps = [[NSDateComponents alloc] init];
	[comps setMonth:[startDateComponents month]];
	[comps setYear:[startDateComponents year]];
	
	NSInteger theWeekdayOrdinal = [self getWeekNumberFromWeekString:[self frequencyMeasurement]];
	if (theWeekdayOrdinal == 0) {
		// Do something to make sure we on last week
		theWeekdayOrdinal = [self getLastWeekdayOrdinalWithCalendar:calendar day:[startDateComponents day] month:[startDateComponents month] 
															   year:[startDateComponents year]];
	}
	
	
	[comps setWeekday:[self getWeekdayNumberFromWeekday:[self daysOfTheWeek]]];
	

	
	NSDate *blahblah = [calendar dateFromComponents:comps];
	
	
	
	//NSInteger weekdayIndex = [self getWeekNumberFromWeekString:[self daysOfTheWeek]];
	
	//[repeatingDate setDay:1];
	//[repeatingDate setMonth:5];
	[startDateComponents setWeekday:1]; 
	//[repeatingDate setWeekdayOrdinal:2];
	//[repeatingDate setWeek:[self frequencyMeasurement]];
	
	NSDate *testingDate = [calendar dateFromComponents:startDateComponents];
	NSLog(@"testing date is %@", [MethodHelper stringFromDate:testingDate usingFormat:K_DATEONLY_FORMAT]);
	
	NSDateComponents *testComponents = [[NSDateComponents alloc] init];
	[testComponents setWeek:2];
	[testComponents setWeekday:0];
	NSDate *testDate = [calendar dateFromComponents:testComponents];
	NSLog(@"DATE IS: %@", [MethodHelper stringFromDate:testDate usingFormat:K_DATEONLY_FORMAT]);*/
	
}

- (NSInteger)getDayDifferenceUsingCurrentDay:(NSInteger)currentDay andWeekdaysArray:(NSMutableArray *)weekdaysArray {
	// To get how far to move forward, if end day is higher, then just minus it by start day
	// eg: end day sat: 7, start day: Tue: 3.  7 - 3 = 4.  Move 4 days forward
	
	// If start day is larger (or the same), then do 7 - start day, then add end day
	// eg: end day sat: 7, start day: tue: 3.  7 - 7 = 0.  0 + 3 = 3.  Move 3 days forward
	
	// Go through the weekdays array looking for the weekday that is larger and is closest to current day
	// If none larger are found, then get the first selected
	
	NSInteger largestGap = 8;
	NSInteger dayDifference = -1;
	for (DayOfTheWeek *day in weekdaysArray) {
		if ([day selected] == TRUE) {
			if (day.compValue - currentDay > 0 && day.compValue - currentDay < largestGap) {
				dayDifference = day.compValue - currentDay;
				largestGap = dayDifference;
			}
		}
	}
	
	
	if (dayDifference == -1) {
		// Set smallest gap to 8, we now need to find the smallest gap
		NSInteger smallestGap = 8;
		
		// Need to go the other way
		for (DayOfTheWeek *day in weekdaysArray) {
			if ([day selected] == TRUE) {
				if ((7 - currentDay) + day.compValue < smallestGap) {
					dayDifference = (7 - currentDay) + day.compValue;
					smallestGap = dayDifference;
				}
			}
		}
	}
	
	if (dayDifference == -1) {
		dayDifference = 1;
	} 
	
	return dayDifference;
}

- (NSMutableArray *)getWeekdaysArrayCopy {
	NSMutableArray *weekdaysArray = [[NSMutableArray alloc] init];// autorelease];
	
	DayOfTheWeek *monday = [[DayOfTheWeek alloc] initWithTitle:@"Monday" andSelected:FALSE andCompValue:2];// autorelease];
    DayOfTheWeek *tuesday = [[DayOfTheWeek alloc] initWithTitle:@"Tuesday" andSelected:FALSE andCompValue:3];// autorelease];
	DayOfTheWeek *wednesday = [[DayOfTheWeek alloc] initWithTitle:@"Wednesday" andSelected:FALSE andCompValue:4];// autorelease];
	DayOfTheWeek *thursday = [[DayOfTheWeek alloc] initWithTitle:@"Thursday" andSelected:FALSE andCompValue:5];// autorelease];
	DayOfTheWeek *friday = [[DayOfTheWeek alloc] initWithTitle:@"Friday" andSelected:FALSE andCompValue:6];// autorelease];
	DayOfTheWeek *saturday = [[DayOfTheWeek alloc] initWithTitle:@"Saturday" andSelected:FALSE andCompValue:7];// autorelease];
	DayOfTheWeek *sunday = [[DayOfTheWeek alloc] initWithTitle:@"Sunday" andSelected:FALSE andCompValue:1];// autorelease];
	
	NSArray *localArray = [[NSArray alloc] initWithObjects:monday, tuesday, wednesday, thursday,
						   friday, saturday, sunday, nil];
	[weekdaysArray addObjectsFromArray:localArray];
//	[localArray release];
	
	return weekdaysArray;
}

- (void)setSelectedWeekdaysForArray:(NSMutableArray *)weekdaysArray {
	NSArray *days = [self.daysOfTheWeek componentsSeparatedByString:@","];
	
	for (NSString *day in days) {
        NSString *aDay = [day stringByReplacingOccurrencesOfString:@" " withString:@""];
		NSInteger index = [self getArrayIndexOfDay:aDay forWeekdaysArray:weekdaysArray];
		
		if (index != -1) {
			DayOfTheWeek *dayOfTheWeek = [weekdaysArray objectAtIndex:index];
			dayOfTheWeek.selected = TRUE;
		}
	}
}

- (NSInteger)getArrayIndexOfDay:(NSString *)day forWeekdaysArray:(NSMutableArray *)weekdaysArray {
	NSInteger i = 0;
	for (DayOfTheWeek *dayOfTheWeek in weekdaysArray) {

		if ([dayOfTheWeek.title isEqualToString:day]) {
			return i;
		}
		i++;
	}
	return -1;
}

- (NSInteger)getWeekNumberFromWeekString:(NSString *)weekNumberString {
	if ([weekNumberString isEqualToString:@"First"]) {
		return 1;
	} else if ([weekNumberString isEqualToString:@"Second"]) {
		return 2;
	} else if ([weekNumberString isEqualToString:@"Third"]) {
		return 3;
	} else if ([weekNumberString isEqualToString:@"Fourth"]) {
		return 4;
	} else if ([weekNumberString isEqualToString:@"Last"]) {
		return 0;
	}
	
	return 1;
}

// Converts the toodledo string to the correct week number
- (NSString *)getWeekOrdinalStringFromToodledoString:(NSString *)toodledoString {
	if ([toodledoString isEqualToString:@"1st"]) {
		return @"First";
	} else if ([toodledoString isEqualToString:@"2nd"]) {
		return @"Second";
	} else if ([toodledoString isEqualToString:@"3rd"]) {
		return @"Third";
	} else if ([toodledoString isEqualToString:@"4th"]) {
		return @"Fourth";
	} else if ([toodledoString isEqualToString:@"5th"]) {
		return @"Last";
	} else if ([toodledoString isEqualToString:@"6th"]) {
		return @"Last";
	} 
	
	return @"Last";
}

- (NSString *)replaceShortenedToodledoWeekday:(NSString *)toodledoWeekday {
	if ([toodledoWeekday isEqualToString:@"Mon"]) {
		return @"Monday";
	} else if ([toodledoWeekday isEqualToString:@"Tue"]) {
		return @"Tuesday";
	} else if ([toodledoWeekday isEqualToString:@"Wed"]) {
		return @"Wednesday";
	} else if ([toodledoWeekday isEqualToString:@"Thu"]) {
		return @"Thursday";
	} else if ([toodledoWeekday isEqualToString:@"Fri"]) {
		return @"Friday";
	} else if ([toodledoWeekday isEqualToString:@"Sat"]) {
		return @"Saturday";
	} else if ([toodledoWeekday isEqualToString:@"Sun"]) {
		return @"Sunday";
	}
	
	return @"Monday";
}

- (NSInteger)getWeekdayNumberFromWeekday:(NSString *)theWeekday {
	if ([theWeekday isEqualToString:@"Sunday"]) {
		return 1;
	} else if ([theWeekday isEqualToString:@"Monday"]) {
		return 2;
	} else if ([theWeekday isEqualToString:@"Tuesday"]) {
		return 3;
	} else if ([theWeekday isEqualToString:@"Wednesday"]) {
		return 4;
	} else if ([theWeekday isEqualToString:@"Thursday"]) {
		return 5;
	} else if ([theWeekday isEqualToString:@"Friday"]) {
		return 6;
	} else if ([theWeekday isEqualToString:@"Saturday"]) {
		return 7;
	}
	return 1;
}

// Get the last weekday ordinal for the passed month
- (NSInteger)getLastWeekdayOrdinalWithCalendar:(NSCalendar *)calendar weekday:(NSInteger)weekday month:(NSInteger)month year:(NSInteger)year {
	NSDateComponents *startDateComps = [[NSDateComponents alloc] init];
	[startDateComps setWeekday:weekday];
	[startDateComps setMonth:month];
	[startDateComps setYear:year];
	
	[startDateComps setWeekdayOrdinal:5];
	
	NSDate *newTestDate = [calendar dateFromComponents:startDateComps];
//	[startDateComps release];
	NSDateComponents *newTestDateComps = [calendar components:(NSMonthCalendarUnit) fromDate:newTestDate];
	
	
	if ([newTestDateComps month] != month) {
		return 4;
	}
	
	return 5;
}

- (NSString *)getToodledoRepeatString {
	if (self.frequency == 0 && [self.daysOfTheWeek length] > 0 &&
		[self.frequencyMeasurement length] == 0) {
		// Days of the week
		return [NSString stringWithFormat:@"Every %@",
				self.daysOfTheWeek];
	} else if (self.frequency != 0 && [self.frequencyMeasurement length] > 0
			   && [self.daysOfTheWeek length] == 0) {
		// Every x Measurment
		return [NSString stringWithFormat:@"Every %d %@",
				self.frequency, self.frequencyMeasurement];
	} else if (self.frequency == 0 && [self.frequencyMeasurement length] > 0
			   && [self.daysOfTheWeek length] > 0) {
		return [NSString stringWithFormat:@"%@ %@", self.frequencyMeasurement,
				self.daysOfTheWeek];
	} else if (self.frequency == 1 && [self.frequencyMeasurement length] == 0
			   && [self.daysOfTheWeek length] == 0) {
		return @"Daily";
	} else if (self.frequency == 7 && [self.frequencyMeasurement length] == 0
			   && [self.daysOfTheWeek length] == 0) {
		return @"Weekly";
	} else if (self.frequency == 14 && [self.frequencyMeasurement length] == 0
			   && [self.daysOfTheWeek length] == 0) {
		return @"Every 2 Weeks";
	} 
	return @"";
}

#pragma mark -
#pragma mark Database Methods

// Returns the correct recurring list item ID
- (NSInteger)insertSelfIntoDatabase {
	return [BLRecurringListItem insertNewRecurringListItem:self];
}

- (void)updateSelfInDatabase {
	[BLRecurringListItem updateDatabaseWithRecurringListItem:self];
}

- (void)deleteSelfFromDatabase {
	// Need to check if the scribble image exists before attempting to delete
	/*NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSString *scribbleName = [self scribble];
	if ([scribbleName length] == 0) {
		scribbleName = @"GarbageValue";
	}
	
	NSString *scribblePath = [NSString stringWithFormat:@"%@%@",
							  [MethodHelper getScribblePath], scribbleName];
	
	if ([fileManager fileExistsAtPath:scribblePath]) {
		NSError *deleteFileError = nil;
		
		[fileManager removeItemAtPath:scribblePath error:&deleteFileError];
		
		if (deleteFileError) {
			NSLog(@"%@", [deleteFileError localizedDescription]);
		}
	}*/
	
	// Now delete from the database
	[BLRecurringListItem deleteRecurringListItemWithRecurringListItemID:self.recurringListItemID];
}

@end
