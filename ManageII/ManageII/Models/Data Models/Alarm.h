//
//  Alarm.h
//  Manage
//
//  Created by Cliff Viegas on 14/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListItem.h"
#import "Alarm.h"

@interface Alarm : NSObject {
	NSString		*alarmGUID;
	NSInteger		listItemID;
	NSString		*fireDateTime;
	NSInteger		delay;			// Time before due date to fire alarm - (note: nothing happens if no due date)
	NSString		*measurement;	// Minute, Hour, Day
}

@property	(nonatomic, copy)		NSString	*alarmGUID;
@property	(nonatomic, assign)		NSInteger	listItemID;
@property	(nonatomic, copy)		NSString	*fireDateTime;
@property	(nonatomic, assign)		NSInteger	delay;
@property	(nonatomic, copy)		NSString	*measurement;

#pragma mark Initialisation
- (id)initWithListItemID:(NSInteger)theListItemID;
- (id)initWithAlarmGUID:(NSString *)theAlarmGUID;

#pragma mark Database Methods
- (void)insertSelfIntoDatabase;
- (void)updateDatabase;
- (void)deleteSelfFromDatabase;

#pragma mark Helper Methods
- (BOOL)removeFromNotificationQueue;

#pragma mark - Add Notification To Queue -
// Will create a new notification to add to queue.  Need to pass alarm
+ (void)addNewNotificationToQueue:(Alarm *)alarm andListItem:(ListItem *) item;

@end
