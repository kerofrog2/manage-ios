//
//  Sound.h
//  Manage
//
//  Created by Kerofrog on 7/5/13.
//
//

#import <Foundation/Foundation.h>

@interface Sound : NSObject

@property (nonatomic, retain)		NSString	*textSound;
@property (nonatomic)               BOOL        isSelected;

#pragma mark Instance Method

/**
 Init data souce for alert
 **/

+ (NSMutableArray *)createDataSource;

@end
