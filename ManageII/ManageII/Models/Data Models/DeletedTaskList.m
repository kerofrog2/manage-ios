//
//  DeletedList.m
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "DeletedTaskList.h"

// Business Layer
#import "BLDeletedList.h"

// Data Models
#import "TaskList.h"

// Method Helper
#import "MethodHelper.h"

@implementation DeletedTaskList

@synthesize listID;
@synthesize title;
@synthesize toodledoFolderID;
@synthesize deletedDateTime;
@synthesize isNote;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[title release];
//	[deletedDateTime release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		
	}
	return self;
}

#pragma mark -
#pragma mark Public Methods

- (void)softDeleteList:(TaskList *)taskList {
	self.listID = taskList.listID;
	self.title = taskList.title;
	self.toodledoFolderID = taskList.toodledoFolderID;
	self.deletedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	self.isNote = taskList.isNote;
    
	// Put this into deleted list table
	[BLDeletedList insertNewDeletedList:self];
}

+ (void)emptyDeletedListDatabase {
	[BLDeletedList emptyDatabase];
}

@end
