//
//  AlertTime.m
//  Manage
//
//  Created by Cliff Viegas on 6/6/13.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "AlertTime.h"
#import "ListItem.h"
#import "MethodHelper.h"


@implementation AlertTime

@synthesize timeAlert = _timeAlert;
@synthesize text = _text;
@synthesize isSelected =_isSelected;


#pragma mark - Initialization

- (id)init {
    self = [super init];
    
    if (self) {
        // init value for object
        [self setDefaultValues];
    }
    
    return self;
}

-(void)dealloc {
//    [_timeAlert release];
//    [_text release];
//    
//    [super dealloc];
}

#pragma mark - Private Methods

// init value for object
- (void)setDefaultValues {
    self.timeAlert = @"-1";
    self.text = @"None";
    self.isSelected = NO;
}

+ (NSMutableArray *)createDataSource {
    
    NSMutableArray *alertTimeArray = [[NSMutableArray alloc]init];
    
    NSMutableArray *timeArray = [[NSMutableArray alloc]initWithObjects:@"None", @"Every Sunday", @"Every Monday", @"Every Tuesday", @"Every Wednesday", @"Every Thursday", @"Every Friday", @"Every Saturday",  nil];
    NSMutableArray *numberOfMinuteArray = [[NSMutableArray alloc]initWithObjects:@"-1", @"1", @"2", @"3", @"4", @"5", @"6", @"7",  nil];
    
    for (NSInteger i=0; i<[timeArray count]; i++) {
        AlertTime *alertTime = [[AlertTime alloc]init];
        alertTime.text = [timeArray objectAtIndex:i];
        alertTime.timeAlert = [[NSString alloc] initWithString:[numberOfMinuteArray objectAtIndex:i]];
        
        [alertTimeArray addObject:alertTime];
//        [alertTime release];
    }
    
//    [timeArray release];
//    [numberOfMinuteArray release];
    
    return alertTimeArray;
}

#pragma mark - Implement Instance Methods

+ (NSString *)textOnAlertWithMinute:(NSString *)minute{
    
    if ([minute isEqualToString:@"-1"]) {
        return @"None";
    }
    if ([minute isEqualToString:@"1"]) {
        return @"Every Sunday";
    }
    if ([minute isEqualToString:@"2"]) {
        return @"Every Monday";
    }
    if ([minute isEqualToString:@"3"]) {
        return @"Every Tuesday";
    }
    if ([minute isEqualToString:@"4"]) {
        return @"Every Wednesday";
    }
    if ([minute isEqualToString:@"5"]) {
        return @"Every Thursday";
    }
    if ([minute isEqualToString:@"6"]) {
        return @"Every Friday";
    }
    if ([minute isEqualToString:@"7"]) {
        return @"Every Saturday";
    }
    return @"None";
}

#pragma mark -
#pragma mark Helper Methods

+ (BOOL)removeFromNotificationQueue:(NSInteger) itemID {
	NSArray *scheduledNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
	
	// Find and remove the existing local notification
	BOOL found = FALSE;
	for (int i = 0; i < [scheduledNotifications count] && found == FALSE; i++) {
		UILocalNotification *localNotification = [scheduledNotifications objectAtIndex:i];
		
		// Need to make sure that we assign alarm guid to each local notification
		NSString *key = [localNotification.userInfo objectForKey:@"AlarmItemID"];
		
		if ([key isEqualToString:[NSString stringWithFormat:@"%d", itemID]]) {
            
			[[UIApplication sharedApplication] cancelLocalNotification:localNotification];
			found = TRUE;
		}
	}
    
    //	BOOL found = FALSE;
	return found;
}

#pragma mark - Add Notification To Queue -
// Will create a new notification to add to queue.  Need to pass alarm
+ (void)addNewNotificationToQueue:(ListItem *)item{
    UILocalNotification *alarmNotification = [[UILocalNotification alloc] init];
	
	if (alarmNotification == nil) {
		[MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Unable to create local notification (alarm).  Please report this issue to support@kerofrog.com.au"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	alarmNotification.timeZone = [NSTimeZone defaultTimeZone];
    
    // Check if alarm for everyday in week
    if ([item.alertTime length] > 0 && item.alertTime != nil && ![item.alertTime isEqualToString:@"-1"]) {
        
        NSInteger time = [item.alertTime intValue];
        // Get Calendar
        NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDate *pickerDate = [MethodHelper dateFromString:item.dueTime usingFormat:K_TIMEONLY_FORMAT];
        NSLog(@"%@",pickerDate);
        
        NSDateComponents *dateComponents = [calender components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSWeekCalendarUnit)fromDate:pickerDate];
        
        [dateComponents setDay:time];
        [dateComponents setWeekday:time];
        NSDate *itemDate = [calender dateFromComponents:dateComponents];
    
        alarmNotification.fireDate = itemDate;
        alarmNotification.repeatInterval = NSWeekdayCalendarUnit;
        
    }else{
        
        alarmNotification.fireDate = [MethodHelper dateFromString:[NSString stringWithFormat:@"%@", item.dueTime] usingFormat:K_DATETIME_FORMAT];
    }
    
    if ([item.scribble length] > 0) {
        if ([item.title length] > 0) {
            alarmNotification.alertBody = item.title;
        }else{
            
            if ([item.notes length] > 120) {
                // Cut notes string
                item.notes = [[item.notes substringToIndex:120] stringByAppendingString:@" ..."];
            }
            alarmNotification.alertBody = item.notes;
        }
    }else{
        alarmNotification.alertBody = item.title;
    }

    alarmNotification.alertAction = @"View";
	alarmNotification.soundName = UILocalNotificationDefaultSoundName;
	
	// Need to create key using AlarmGUID for reference
	NSDictionary *infoDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d", item.listItemID] forKey:@"AlarmItemID"];
	alarmNotification.userInfo = infoDict;
	
	// Schedule the local notification
	[[UIApplication sharedApplication] scheduleLocalNotification:alarmNotification];
//	[alarmNotification release];
	
	// Need to check if this is ios 4 or above, otherwise display warning

}


@end
