//
//  List.m
//  Manage
//
//  Created by Cliff Viegas on 17/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "TaskList.h"

// Quartzcore
#import <QuartzCore/QuartzCore.h>

// Business Layer
#import "BLList.h"
#import "BLListItem.h"
#import "BLListItemTag.h"

// Custom UI Objects
#import "MoveArray.h"
#import "Alarm.h"

// Data Models
#import "RecurringListItem.h"
#import "ListItem.h"
#import "ArchiveList.h"
#import "Tag.h"
#import "ApplicationSetting.h"
#import "DeletedListItem.h"
#import "AlertTime.h"

// Data Collections
#import "TagCollection.h"
#import "ListItemTagCollection.h"
#import "AlarmCollection.h"

// Method Helpers
#import "MethodHelper.h"
#import "DMHelper.h"

// Views
#import "QuartzTagView.h"

// GLDraw ES2
#import "GLDrawES2ViewController.h"


@interface TaskList()
- (void)loadListItemsWithListID:(NSInteger)theListID andSortType:(NSInteger)sorted; 
@end


@implementation TaskList

// Properties
@synthesize listItems;
@synthesize listID;
@synthesize title;
@synthesize creationDate;
@synthesize screenshot;
@synthesize currentSortType;
@synthesize lastSortType;
@synthesize listOrder;
@synthesize completed;
@synthesize completedDateTime;
@synthesize archived;
@synthesize archivedDateTime;
@synthesize toodledoFolderID;
@synthesize creationDateTime;

// Helper properties
@synthesize mainViewRequiresUpdate;
@synthesize markedForDeletion;
@synthesize taskItemFrame;
@synthesize currentRowHeight;
@synthesize m_totalHeight;

// 2.0 properties
@synthesize isNote;
@synthesize scribble;
//@synthesize glScribbleDictionary;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[self.listItems release];
//	[self.title release];
//	[self.creationDate release];
//	[self.screenshot release];
//	[self.completedDateTime release];
//	[self.archivedDateTime release];
//	[self.creationDateTime release];
//    [self.scribble release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if ((self = [super init])) {
		self.listItems = [NSMutableArray array];
		
		self.currentSortType = EnumSortByList;
		self.lastSortType = EnumSortByList;
		
		// Update required init to false
		self.mainViewRequiresUpdate = FALSE;
		self.markedForDeletion = FALSE;
	}
	return self;
}

// This method is called from elsewhere where rest of properties are specified
- (id)initExistingWithListID:(NSInteger)aListID andTitle:(NSString *)theTitle {
	if ((self = [super init])) {
		// Init the array of items
		self.listID = aListID;
		self.listItems = [NSMutableArray array];
		
		self.title = theTitle;
		
		// Now need to populate these list items
		[self loadListItemsWithListID:aListID andSortType:EnumSortByList];
		
		// Init the sort type
		self.currentSortType = EnumSortByList;
		self.lastSortType = EnumSortByList;
		
		// Init main view requires update
		self.mainViewRequiresUpdate = FALSE;
		self.markedForDeletion = FALSE;
	}
	return self;
}

// Creates a new list
- (id)initNewList {
	if ((self = [super init])) {
		// Init the array of items
		self.listItems = [NSMutableArray array];
		
		// Init the sort type
		self.currentSortType = EnumSortByList;
		self.lastSortType = EnumSortByList;
		
		// Apply the passed variables to this instance of a page data model
		self.title = @"New List";
		self.creationDate = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATEONLY_FORMAT];
		self.screenshot = @"";
		self.listOrder = [BLList getNextListOrder];
		self.completedDateTime = @"";
		self.completed = FALSE;
		self.archived = FALSE;
		self.archivedDateTime = @"";
		self.toodledoFolderID = -1;
		self.creationDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		self.isNote = NO;
        self.scribble = @"";
        
		// Insert the newly created list into the database
		[ApplicationSetting updateSettingName:@"LastAddListDateTime" withData:self.creationDateTime];
		self.listID = [BLList insertNewList:self];
        
	}
	return self;
}

// Creates a new note
- (id)initNewNote {
	if ((self = [super init])) {
		// Init the array of items
		self.listItems = [NSMutableArray array];
		
		// Init the sort type
		self.currentSortType = EnumSortByList;
		self.lastSortType = EnumSortByList;
		
		// Apply the passed variables to this instance of a page data model
		self.title = @"New Note";
		self.creationDate = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATEONLY_FORMAT];
		self.screenshot = @"";
		self.listOrder = [BLList getNextListOrder];
		self.completedDateTime = @"";
		self.completed = FALSE;
		self.archived = FALSE;
		self.archivedDateTime = @"";
		self.toodledoFolderID = -1;
		self.creationDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		self.isNote = YES;
        self.scribble = @"";
        
		// Insert the newly created list into the database
		[ApplicationSetting updateSettingName:@"LastAddListDateTime" withData:self.creationDateTime];
		self.listID = [BLList insertNewList:self];
		//self.listID = [BLList insertNewListWithTitle:self.title andCreationDate:self.creationDate andListOrder:self.listOrder];
	}
	return self;
}

// Init with a copy of another task list
- (id)initWithTaskListCopy:(TaskList *)taskListCopy {
	if ((self = [super init])) {
		// Init the array of items
		self.listItems = [NSMutableArray array];
		
		self.currentSortType = taskListCopy.currentSortType;
		self.lastSortType = taskListCopy.lastSortType;
		
		self.title = taskListCopy.title;
		self.creationDate = taskListCopy.creationDate;
		self.screenshot = taskListCopy.screenshot;
		self.listID = taskListCopy.listID;
		self.listOrder = taskListCopy.listOrder;
		self.completedDateTime = taskListCopy.completedDateTime;
		self.completed = taskListCopy.completed;
		self.archived = taskListCopy.archived;
		self.archivedDateTime = taskListCopy.archivedDateTime;
		self.toodledoFolderID = taskListCopy.toodledoFolderID;
		self.creationDateTime = taskListCopy.creationDateTime;
        self.isNote = taskListCopy.isNote;
        self.scribble = taskListCopy.scribble;

		// Helper property
		self.mainViewRequiresUpdate = taskListCopy.mainViewRequiresUpdate;
		self.markedForDeletion = taskListCopy.markedForDeletion;
		
		for (ListItem *listItemCopy in taskListCopy.listItems) {
			ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItemCopy];
			[self.listItems addObject:newListItem];
//			[newListItem release];
		}

	}
	return self;
}

// Init with duplicate (reset values) copy of another list
- (id)initWithDuplicateTaskList:(TaskList *)taskListCopy {
	// This version resets date, tags, completed etc. values
	if ((self = [super init])) {
		// Init the array of items
		self.listItems = [NSMutableArray array];
		
		self.currentSortType = taskListCopy.currentSortType;
		self.lastSortType = taskListCopy.lastSortType;
		
		self.title = taskListCopy.title;
		self.creationDate = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATEONLY_FORMAT];
		self.screenshot = @"";
		self.listOrder = [BLList getNextListOrder];
		self.completedDateTime = @"";
		self.completed = FALSE;
		self.archived = FALSE;
		self.archivedDateTime = FALSE;
		self.toodledoFolderID = -1;
		self.creationDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		self.isNote = taskListCopy.isNote;
        self.scribble = taskListCopy.scribble;
        
		// Update required init to false
		self.mainViewRequiresUpdate = FALSE;
		self.markedForDeletion = FALSE;
		
		[ApplicationSetting updateSettingName:@"LastAddListDateTime" withData:self.creationDateTime];
		self.listID = [BLList insertNewList:self];
		
		NSInteger lastListItemID = 0;
        
        if ([taskListCopy isNote] == TRUE) {
            self.scribble = [MethodHelper getCopyOfScribble:taskListCopy.scribble];
        }
        
		for (ListItem *listItemCopy in taskListCopy.listItems) {
            //(update alerttime field)
			ListItem *newListItem = [[ListItem alloc] initNewListItemWithID:self.listID 
																   andTitle:listItemCopy.title 
																   andNotes:listItemCopy.notes 
															   andCompleted:NO 
															 andParentTitle:self.title 
																 andDueDate:@""
                                                                 andDueTime:@""
                                                               andAlertTime:@""
                                                                   andSound:@""
                                                                  andSnooze:NO
												   andListItemTagCollection:listItemCopy.listItemTagCollection
													   andCompletedDateTime:@""
                                                                andPriority:listItemCopy.priority 
                                                                   addToEnd:FALSE];
			
			// Update other required values
			newListItem.itemOrder = listItemCopy.itemOrder;
			newListItem.creationDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
			newListItem.subItemOrder = listItemCopy.subItemOrder;
			
			if ([listItemCopy parentListItemID] != -1) {
				// Need the new parent list item id
				newListItem.parentListItemID = lastListItemID;
			} else {
				lastListItemID = newListItem.listItemID;
			}
			
			// Make copy of the original scribble
			if ([listItemCopy.scribble length] > 0) {
				newListItem.scribble = [MethodHelper getCopyOfScribble:listItemCopy.scribble];
			}
			
			// Update db
			[newListItem updateDatabase];
			
			[self.listItems addObject:newListItem];
//			[newListItem release];
		}
	}
	
	return self;
}

// Init with a copy of another task list (without copying list items)
- (id)initWithBasicTaskListCopy:(TaskList *)taskListCopy {
	if ((self = [super init])) {
        if (self.listItems != nil) {
//            [self.listItems release];
        }
		self.listItems = [NSMutableArray array];
		
		self.currentSortType = taskListCopy.currentSortType;
		self.lastSortType = taskListCopy.lastSortType;
		
		self.title = taskListCopy.title;
		self.creationDate = taskListCopy.creationDate;
		self.screenshot = taskListCopy.screenshot;
		self.listID = taskListCopy.listID;
		self.listOrder = taskListCopy.listOrder;
		self.completedDateTime = taskListCopy.completedDateTime;
		self.completed = taskListCopy.completed;
		self.archived = taskListCopy.archived;
		self.archivedDateTime = taskListCopy.archivedDateTime;
		self.toodledoFolderID = taskListCopy.toodledoFolderID;
		self.creationDateTime = taskListCopy.creationDateTime;
		self.isNote = taskListCopy.isNote;
        self.scribble = taskListCopy.scribble;
        
		self.mainViewRequiresUpdate = taskListCopy.mainViewRequiresUpdate;
		self.markedForDeletion = taskListCopy.markedForDeletion;
	}
	return self;
}

// TODO: Add DueTime to alert
- (void)loadListItemsWithListID:(NSInteger)theListID andSortType:(NSInteger)sorted {
	// Get the array of all listItems for this list id in the database
	NSArray *localArray = [BLListItem getAllListItemsWithListID:theListID sortedBy:sorted];
	
	// Create a new object for each list taken from the database and add it to our list of lists
	for (NSDictionary *listItemDictionary in localArray) {
		NSInteger theListID = [DMHelper getNSIntegerValueForKey:@"ListID" fromDictionary:listItemDictionary];
		NSInteger thePriority = [DMHelper getNSIntegerValueForKey:@"Priority" fromDictionary:listItemDictionary];
		NSInteger theListItemID = [DMHelper getNSIntegerValueForKey:@"ListItemID" fromDictionary:listItemDictionary];
		//Tan Nguyen remove autorelease
		ListItem *listItem = [[ListItem alloc] initExistingListItemWithListItemID:theListItemID
																	 andListID:theListID
																 andPriority:thePriority
																andParentTitle:self.title];
		
		listItem.title = [DMHelper getNSStringValueForKey:@"Title" fromDictionary:listItemDictionary];
		listItem.creationDateTime = [DMHelper getNSStringValueForKey:@"CreationDateTime" fromDictionary:listItemDictionary];
		listItem.dueDate = [DMHelper getNSStringValueForKey:@"DueDate" fromDictionary:listItemDictionary];
        listItem.dueTime = [DMHelper getNSStringValueForKey:@"DueTime" fromDictionary:listItemDictionary];
        // Alert time
        listItem.alertTime = [DMHelper getNSStringValueForKey:@"AlertTime" fromDictionary:listItemDictionary];
        listItem.sound = [DMHelper getNSStringValueForKey:@"Sound" fromDictionary:listItemDictionary];
        listItem.hasSnooze = [DMHelper getBoolValueForKey:@"Snooze" fromDictionary:listItemDictionary];
        
		listItem.notes = [DMHelper getNSStringValueForKey:@"Notes" fromDictionary:listItemDictionary];
		listItem.scribble = [DMHelper getNSStringValueForKey:@"Scribble" fromDictionary:listItemDictionary];
		listItem.completed = [DMHelper getBoolValueForKey:@"Completed" fromDictionary:listItemDictionary];
		listItem.itemOrder = [DMHelper getIntegerValueForKey:@"ItemOrder" fromDictionary:listItemDictionary];
		listItem.parentListItemID = [DMHelper getIntegerValueForKey:@"ParentListItemID" fromDictionary:listItemDictionary];
		listItem.subItemOrder = [DMHelper getIntegerValueForKey:@"SubItemOrder" fromDictionary:listItemDictionary];
		listItem.completedDateTime = [DMHelper getNSStringValueForKey:@"CompletedDateTime" fromDictionary:listItemDictionary];
		listItem.archived = [DMHelper getBoolValueForKey:@"Archived" fromDictionary:listItemDictionary];
		listItem.archivedDateTime = [DMHelper getNSStringValueForKey:@"ArchivedDateTime" fromDictionary:listItemDictionary];
		listItem.recurringListItemID = [DMHelper getNSIntegerValueForKey:@"RecurringListItemID" fromDictionary:listItemDictionary];
		listItem.hasAlarms = [DMHelper getBoolValueForKey:@"HasAlarms" fromDictionary:listItemDictionary];
		listItem.modifiedDateTime = [DMHelper getNSStringValueForKey:@"ModifiedDateTime" fromDictionary:listItemDictionary];
		listItem.toodledoTaskID = [DMHelper getSInt64ValueForKey:@"ToodledoTaskID" fromDictionary:listItemDictionary];
        
        
		[self.listItems addObject:listItem];
        //Tan Nguyen remove manual release
//        [listItem release];
	}
	
}


#pragma mark -
#pragma mark Get Methods

- (NSInteger)getIndexOfListItemWithID:(NSInteger)listItemID {
	int i = 0;
	for (ListItem *list in self.listItems) {
		if (list.listItemID == listItemID) {
			return i;
		}
		i++;
	}
	
	// Not found
	return -1;
}

- (BOOL)canArchiveSubListItemAtIndex:(NSInteger)listItemIndex {
	ListItem *selectedListItem = [self.listItems objectAtIndex:listItemIndex];
	
	// Exit with false if this is not a sublist item
	if ([selectedListItem parentListItemID] == -1) {
		return FALSE;
	}
	
	// Check if the parent is completed first
	NSInteger parentIndex = [self getIndexOfListItemWithID:selectedListItem.parentListItemID];
	
    // Check to make sure the parent index is within bounds
    if (parentIndex < 0 || parentIndex > [self.listItems count] - 1) {
        return FALSE;
    }
    
	if ([[self.listItems objectAtIndex:parentIndex] completed] == FALSE) {
		return FALSE;
	}
	
	// If this is a parent task, then we need to make sure all subtasks are completed
	for (int i = [self.listItems count] - 1; i >= 0; i--) {
		if (i != listItemIndex) {
			ListItem *listItem = [self.listItems objectAtIndex:i];
			
			if ([listItem parentListItemID] == selectedListItem.parentListItemID && [listItem completed] == FALSE) {
				return FALSE;
			}
		}
	}
	
	return TRUE;
}

// Check if the list item has another item that is incomplete with same parent list item
// Also check if the parent list item is complete

- (BOOL)hasSubtasksForListItemAtIndex:(NSInteger)listItemIndex {
	ListItem *selectedListItem = [self.listItems objectAtIndex:listItemIndex];
    
	for (ListItem *listItem in self.listItems) {
		if (listItem.parentListItemID == selectedListItem.listItemID) {
			return TRUE;
		}
	}
	return FALSE;
}

- (BOOL)hasIncompleteSubtasksForListItemAtIndex:(NSInteger)listItemIndex {
	ListItem *selectedListItem = [self.listItems objectAtIndex:listItemIndex];
	
	for (ListItem *listItem in self.listItems) {
		if (listItem.parentListItemID == selectedListItem.listItemID && [listItem completed] == FALSE) {
			return TRUE;
		}
	}
	return FALSE;
}


- (BOOL)hasIncompleteSubtasksForListItemAtIndex:(NSInteger)listItemIndex forCompletedDuration:(NSInteger)completedDuration 
						 andShowCompletedStatus:(BOOL)showCompletedStatus {
	ListItem *selectedListItem = [self.listItems objectAtIndex:listItemIndex];

	
	for (ListItem *listItem in self.listItems) {
		if (listItem.parentListItemID == selectedListItem.listItemID && listItem.completed == FALSE) {
			return TRUE;
		} else if (listItem.parentListItemID == selectedListItem.listItemID && listItem.completed == TRUE
				   && showCompletedStatus == TRUE) {
			// Need to check the duration
			NSInteger dayDifference = [listItem getCompletedDateDifferenceFromToday];
			// Convert to positive
			dayDifference = dayDifference * -1;
			if (dayDifference <= completedDuration || completedDuration == 0) {
				return TRUE;
			}
		}
	}
	return FALSE;
}

- (NSInteger)getCountOfSubtasksForListItemAtIndex:(NSInteger)listItemIndex {
	NSInteger count = 0;
	
	ListItem *selectedListItem = [self.listItems objectAtIndex:listItemIndex];
	
	for (ListItem *listItem in self.listItems) {
		if (listItem.parentListItemID == selectedListItem.listItemID) {
			count++;
		}
	}
	
	return count;
}

- (NSInteger)getNextListItemOrder {
	return [BLListItem getNextItemOrderForListID:[self listID]];
}

- (NSInteger)getFirstListItemOrder {
    return [BLListItem getFirstItemOrderForListID:[self listID]];
}

- (ListItem *)getListItemReferenceFromListItemWithToodledoTaskID:(SInt64)theTaskID {
	ListItem *theListItem = nil;
	
	for (ListItem *listItem in self.listItems) {
		if ([listItem toodledoTaskID] == theTaskID) {
			theListItem = listItem;
			break;
		}
	}
	
	return theListItem;
}

+ (NSInteger)getCountOfListsWithTitle:(NSString *)theTitle {
	return [BLList getNumberOfListsWithTitle:theTitle];
}

- (NSInteger)getCountOfNonOverdueTasks {
    NSInteger count = 0;
    for (ListItem *listItem in self.listItems) {
        if ([listItem completed] == TRUE || [listItem hasJustComplete] == TRUE) {
            continue;
        }
        
        if ([[listItem dueDate] length] == 0) {
            count++;
            continue;
        }
        
        NSString *dateString = [listItem dueDate];
        NSDate *theDate = [MethodHelper dateFromString:dateString usingFormat:K_DATEONLY_FORMAT];
        // Today
        NSString *today = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATEONLY_FORMAT];
        
        if ([MethodHelper isDateInPast:theDate] == FALSE || [dateString isEqualToString:today]) {// Tasks count include due tasks(today and future day) and no due tasks
            count++;
        }
        
    }
    return count;
}

- (NSInteger)getCountOfOverdueTasks {
    NSInteger count = 0;
    for (ListItem *listItem in self.listItems) {
        if ([[listItem dueDate] length] == 0 || [listItem completed] == TRUE || [listItem hasJustComplete] == TRUE) {
            continue;
        }
        
        NSString *dateString = [listItem dueDate];
        NSDate *theDate = [MethodHelper dateFromString:dateString usingFormat:K_DATEONLY_FORMAT];
        // Today
        NSString *today = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATEONLY_FORMAT];
        if ([MethodHelper isDateInPast:theDate] && ![dateString isEqualToString:today]) {// overdue tasks include past day
            count++;
        }
    }
    return count;
}

#pragma mark -
#pragma mark Delete Methods

- (void)deleteListItemAtIndex:(NSInteger)index permanent:(BOOL)isPermanent {
	NSInteger listItemID = [[self.listItems objectAtIndex:index] listItemID];
	
	// Look if image exists to delete that
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSString *scribbleName = [[self.listItems objectAtIndex:index] scribble];
	
	if ([scribbleName length] == 0) {
		scribbleName = @"GarbageValue";
	}
	
	NSString *imagePath = [NSString stringWithFormat:@"%@%@",
						   [MethodHelper getScribblePath], scribbleName];
	
	if ([fileManager fileExistsAtPath:imagePath]) {
		NSError *deleteFileError = nil;
		// Remove the file
		
		[fileManager removeItemAtPath:imagePath error:&deleteFileError];
		if (deleteFileError) {
			NSLog(@"%@", [deleteFileError localizedDescription]);
		}
	}
	
    // Delete png version if it exists
    NSRange glsRange = [scribbleName rangeOfString:@".gls"];
    NSString *pngScribbleName = @"GarbageValue";
    if (glsRange.length > 0) {
        pngScribbleName = [scribbleName substringToIndex:glsRange.location];

        pngScribbleName = [NSString stringWithFormat:@"%@.png", pngScribbleName];

    }
    NSString *pngImagePath = [NSString stringWithFormat:@"%@%@",
                              [MethodHelper getScribblePath], pngScribbleName];

    if ([fileManager fileExistsAtPath:pngImagePath]) {
		NSError *deleteFileError = nil;
		// Remove the file
		
		[fileManager removeItemAtPath:pngImagePath error:&deleteFileError];
		if (deleteFileError) {
			NSLog(@"%@", [deleteFileError localizedDescription]);
		}
	}
    
	// Also need to delete tags that have the list item id
	[BLListItemTag deleteAllListItemTagsWithListItemID:listItemID];
	
	// Don't do a permanent delete.. if requested
	if (isPermanent == FALSE) {
		ListItem *listItem = [self.listItems objectAtIndex:index];
		DeletedListItem *deletedListItem = [[DeletedListItem alloc] init];
		[deletedListItem softDeleteListItem:listItem];
//		[deletedListItem release];
	}
	
	// Delete the object from the database
	[BLListItem deleteListItemWithListItemID:listItemID];
	
	// Update last delete task setting
	[ApplicationSetting updateSettingName:@"LastDeleteTaskDateTime" withData:[MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT]];
	
	// Remove the object from the collection
	[self.listItems removeObjectAtIndex:index];
}

- (void)deleteListItemRetainScribbleAtIndex:(NSInteger)index permanent:(BOOL)isPermanent {
	NSInteger listItemID = [[self.listItems objectAtIndex:index] listItemID];
	
	// Also need to delete tags that have the list item id
	[BLListItemTag deleteAllListItemTagsWithListItemID:listItemID];
	
	// Don't do a permanent delete.. if requested
	if (isPermanent == FALSE) {
		ListItem *listItem = [self.listItems objectAtIndex:index];
		DeletedListItem *deletedListItem = [[DeletedListItem alloc] init];
		[deletedListItem softDeleteListItem:listItem];
//		[deletedListItem release];
	}
	
	// Delete the object from the database
	[BLListItem deleteListItemWithListItemID:listItemID];
	
	// Update last delete task setting
	[ApplicationSetting updateSettingName:@"LastDeleteTaskDateTime" withData:[MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT]];
	
	// Remove the object from the collection
	[self.listItems removeObjectAtIndex:index];
}

#pragma mark - Move Methods

- (BOOL)moveListItemFrom:(NSInteger)fromIndex to:(NSInteger)toIndex {
	[self.listItems moveObjectFromIndex:fromIndex toIndex:toIndex];
	
	// Update the item order for the affected list items
	ListItem *listItem = [self.listItems objectAtIndex:toIndex];
	
	// It needs to be equal to the next or previous item order
	if (toIndex == 0 || fromIndex > toIndex) {
		if (listItem.parentListItemID == -1) {
			listItem.itemOrder = [[self.listItems objectAtIndex:toIndex + 1] itemOrder];
		} else {
            // Need to find new parent, and update accordingly.
			listItem.subItemOrder = [[self.listItems objectAtIndex:toIndex + 1] subItemOrder];
		}
		
	} else if (toIndex == [self.listItems count] - 1 || fromIndex < toIndex) {
		if (listItem.parentListItemID == -1) {
            listItem.itemOrder = [[self.listItems objectAtIndex:toIndex - 1] itemOrder];
		} else {
			listItem.subItemOrder = [[self.listItems objectAtIndex:toIndex - 1] subItemOrder];
		}
	}
	
	// Then need to subtract (or add) 1 to every single list item order after the current one
	BOOL movingParentWithSubtasks = FALSE;
    if (listItem.parentListItemID == -1 && [self hasSubtasksForListItemAtIndex:toIndex]) {
		// Test if it works first..
		movingParentWithSubtasks = TRUE;
		
		NSInteger numberOfSubtasks = [self getCountOfSubtasksForListItemAtIndex:toIndex];
		
		[self updateListItemOrderFromIndex:fromIndex toIndex:toIndex ignoringParentListItemID:listItem.listItemID];
		
		// Also need to update the list item order for any subtasks
		[self updateSubtasksListItemOrderForListItem:listItem];
		
		NSInteger bufferCount = 1;
		if (fromIndex > toIndex) {
			for (int i = fromIndex + 1; i < (fromIndex + 1) + numberOfSubtasks; i++) {
				[self.listItems moveObjectFromIndex:i toIndex:toIndex + bufferCount];
				bufferCount++;
			}
		} else if (toIndex > fromIndex) {
			bufferCount = 0;
			for (int i = fromIndex + (numberOfSubtasks - 1); i >= fromIndex; i--) {
				[self.listItems moveObjectFromIndex:i toIndex:toIndex - bufferCount];
				bufferCount++;
			}
		}
	} else if (listItem.parentListItemID == -1) {
		// Need to use -100 as we dont want to ignore any list items and there are list items with parentListItemID = -1
		[self updateListItemOrderFromIndex:fromIndex toIndex:toIndex ignoringParentListItemID:-100];
	} else {
		[self updateSubListItemOrderFromIndex:fromIndex toIndex:toIndex];
	}
    
    return movingParentWithSubtasks;
}

#pragma mark -
#pragma mark Update Database

- (void)updateDatabase {
	// Update database with self
	[ApplicationSetting updateSettingName:@"LastEditListDateTime" withData:[MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT]];
	[BLList updateDatabaseWithList:self];
}

- (void)updateDatabaseWithoutEditLog {
	[BLList updateDatabaseWithList:self];
}

- (void)updateListOrder {
	// Update list order with self properties
	[BLList updateListOrder:self.listOrder forListID:self.listID];
}

#pragma mark -
#pragma mark Archive Methods

// Archiving will either occur on application launch, manual or after one day
// Get archived count is for when we want to count how many items got archived
- (NSInteger)archiveAllCompletedItems:(BOOL)getArchivedCount {
	NSInteger nonArchivedItemsCount = 0;		// For tracking itmes that should be archived but cant be
	NSInteger archivedListItemCount = 0;
	
	// Have to do the update backwards in the array, so we can remove items
	for (int i = [self.listItems count] - 1; i >= 0; i--) {
		ListItem *listItem = [self.listItems objectAtIndex:i];
		if ([listItem completed] == TRUE && [listItem archived] == FALSE) {
			
			if ([listItem parentListItemID] != -1) {
				if ([self canArchiveSubListItemAtIndex:i] == FALSE) {
					nonArchivedItemsCount++;
					continue;
				}
			}
			
			// Make sure task does not have any subtasks before archiving
			if ([self hasSubtasksForListItemAtIndex:i]) {
				// Do not archive - do nothing
				nonArchivedItemsCount++;
			} else {
				self.mainViewRequiresUpdate = TRUE;
				
				// Set parent list item id to -1, as we remove 'subtask' status when archiving
				//[listItem setParentListItemID:-1];
				
				listItem.archivedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
				//listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
				[listItem setArchived:TRUE];
				// No need to delete from other table as this task was already set to complete
				[listItem setRecurringListItemID:-1];
				// Update database
				[listItem updateDatabaseWithoutModifiedLog];
				
				// Now remove this list item from the tasklist
				[self.listItems removeObjectAtIndex:i];
				
				// Update the number of list items archived
				archivedListItemCount++;
			}

		}
	}
	
	if (getArchivedCount == TRUE) {
		return archivedListItemCount;
	}
	
	return nonArchivedItemsCount;
}

// Returns the number of archived items
- (NSInteger)archiveAllCompletedItemsUsingDuration:(NSInteger)duration {
	NSInteger archivedItemCount = 0;
	
	// Have to do the update backwards in the array, so we can remove items
	for (int i = [self.listItems count] - 1; i >= 0; i--) {
		ListItem *listItem = [self.listItems objectAtIndex:i];
		if ([listItem completed] == TRUE && [listItem archived] == FALSE) {
			// Negative number for date in past, multiply by -1 to make it positive
			NSInteger dateDifference = [listItem getCompletedDateDifferenceFromToday] * -1;
			
			if ([listItem parentListItemID] != -1) {
				if ([self canArchiveSubListItemAtIndex:i] == FALSE) {
					continue;
				}
			}
			
			// Make sure task does not have any subtasks before archiving
			if ([self hasSubtasksForListItemAtIndex:i]) {
				// Do not archive - do nothing
				
			} else if (dateDifference >= duration) {
				self.mainViewRequiresUpdate = TRUE;
				
				// Set parent list item id to -1, as we remove 'subtask' status when archiving
				//[listItem setParentListItemID:-1];
				
				listItem.archivedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
				//listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
				[listItem setArchived:TRUE];
				// No need to delete from other table as this task was already set to complete
				[listItem setRecurringListItemID:-1];
				// Update database
				[listItem updateDatabaseWithoutModifiedLog];
				
				// Now remove this list item from the tasklist
				[self.listItems removeObjectAtIndex:i];
				
				// Update the amount of items archived
				archivedItemCount++;
			}
			
		}
	}
	
	return archivedItemCount;
}

// Archiving will either occur on application launch, manual or after one day
- (NSInteger)archiveAllCompletedItemsToArchiveList:(ArchiveList *)archiveList {
	NSInteger nonArchivedItemsCount = 0;		// For tracking itmes that should be archived but cant be
	
	// Have to do the update backwards in the array, so we can remove items
	for (int i = [self.listItems count] - 1; i >= 0; i--) {
		ListItem *listItem = [self.listItems objectAtIndex:i];
		if ([listItem completed] == TRUE && [listItem archived] == FALSE) {
			// Check if the list item has another item that is incomplete with same parent list item
			// Also check if the parent list item is complete
			
			if ([listItem parentListItemID] != -1) {
				if ([self canArchiveSubListItemAtIndex:i] == FALSE) {
					nonArchivedItemsCount++;
					continue;
				}
			}
			
			// Make sure task does not have any subtasks before archiving (if it does, it is because they could not be archived)
			if ([self hasSubtasksForListItemAtIndex:i]) {
				// Do not archive - do nothing
				nonArchivedItemsCount++;
			} else {
				self.mainViewRequiresUpdate = TRUE;
				
				// Set parent list item id to -1, as we remove 'subtask' status when archiving
				//[listItem setParentListItemID:-1];
				
				listItem.archivedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
				//listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
				[listItem setArchived:TRUE];
				// No need to delete from other table as this task was already set to complete
				[listItem setRecurringListItemID:-1];
				// Update database
				[listItem updateDatabaseWithoutModifiedLog];
				
				// Need to make copy of list item to place in archive list array
				ListItem *listItemCopy = [[ListItem alloc] initWithListItemCopy:listItem];
				[archiveList.listItems insertObject:listItemCopy atIndex:0];
//				[listItemCopy release];
				
				// Now remove this list item from the tasklist
				[self.listItems removeObjectAtIndex:i];
			}
			
			
			
		}
	}
	
	
	return nonArchivedItemsCount;
}

// Archive all list items, completed or not (when list is archived)
- (void)archiveAllItems {
	self.mainViewRequiresUpdate = TRUE;
	
	// Have to do the update backwards in the array, so we can remove items
	for (int i = [self.listItems count] - 1; i >= 0; i--) {
		ListItem *listItem = [self.listItems objectAtIndex:i];

		// Set parent list item id to -1, as we remove 'subtask' status when archiving
		//[listItem setParentListItemID:-1];
		
		listItem.archivedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		//listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		[listItem setArchived:TRUE];
		[listItem deleteRepeatingTaskDetails];
		// Update database
		[listItem updateDatabaseWithoutModifiedLog];
		// Need to delete recurring task too, if it exists
		if ([listItem recurringListItemID] != -1) {
			
		}
		
		// Now remove this list item from the tasklist
		[self.listItems removeObjectAtIndex:i];
	}
}

- (void)archiveAllItemsToArchiveList:(ArchiveList *)archiveList {
	self.mainViewRequiresUpdate = TRUE;
	
	// Have to do the update backwards in the array, so we can remove items
	for (int i = [self.listItems count] - 1; i >= 0; i--) {
		ListItem *listItem = [self.listItems objectAtIndex:i];
		
		// Set parent list item id to -1, as we remove 'subtask' status when archiving
		//[listItem setParentListItemID:-1];
		
		listItem.archivedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		//listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		[listItem setArchived:TRUE];
		[listItem deleteRepeatingTaskDetails];
		// Update database
		[listItem updateDatabaseWithoutModifiedLog];
		
		// Need to make copy of list item to place in archive list array
		ListItem *listItemCopy = [[ListItem alloc] initWithListItemCopy:listItem];
		[archiveList.listItems insertObject:listItemCopy atIndex:0];
//		[listItemCopy release];
		
		// Now remove this list item from the tasklist
		[self.listItems removeObjectAtIndex:i];
	}
}

- (void)archiveListItemAtIndex:(NSInteger)index {
	ListItem *listItem = [self.listItems objectAtIndex:index];
	
	self.mainViewRequiresUpdate = TRUE;
	
	// Set parent list item id to -1, don't do this anymore as causing issues
	//[listItem setParentListItemID:-1];
	listItem.archivedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	//listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	[listItem setArchived:TRUE];
	
	if ([listItem completed] == TRUE) {
		listItem.recurringListItemID = -1;
	} else if ([listItem completed] == FALSE) {
		[listItem deleteRepeatingTaskDetails];
	}
    
    
    // If List Item has alarm, remove alarm when list item is archived
    // Init any loaded alarms
    AlarmCollection *alarmCollection = [[AlarmCollection alloc] init];
    [alarmCollection loadAlarmsWithListItemID:listItem.listItemID];
    
    if ([alarmCollection.alarms count] > 0) {
        listItem.hasAlarms = FALSE;

        for (Alarm *alarm in alarmCollection.alarms) {
            // Remove notification
            [alarm removeFromNotificationQueue];
            
            // Get alarmGUID
            NSString *alarmGUID = alarm.alarmGUID;
            
            Alarm *alarmDelete = [[Alarm alloc] initWithAlarmGUID:alarmGUID];
            
            // Delete the alarm from database
            [alarmDelete deleteSelfFromDatabase];
//            [alarmDelete release];
        }
        
//        [alarmCollection release];
    }
	
	// Update database
	[listItem updateDatabaseWithoutModifiedLog];
	
	// Now remove this list item from the tasklist
	[self.listItems removeObjectAtIndex:index];
}



#pragma mark -
#pragma mark Mail Methods

- (NSString *)getTextMailMessageWithTagCollection:(TagCollection *)tagCollection showSubtasks:(BOOL)showSubtasks {
	NSMutableString *mailMessage = [NSMutableString stringWithString:@""];
	
	// Add header in message
	[mailMessage appendString:self.title];
	[mailMessage appendString:@"\n\n"];
	
	for (ListItem *listItem in self.listItems) {
		
		// Check if listitem is task or sub task
		if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
			[mailMessage appendString:@" - "];
		} else {
			[mailMessage appendString:@"   -- "];
		}
		
		if (listItem.priority == 1) {
			[mailMessage appendString:@"(Important) "];
		} else if (listItem.priority == 2) {
			[mailMessage appendString:@"(Urgent) "];
		}
		
		[mailMessage appendString:listItem.title];
		
		if ([listItem.dueDate length] > 0 && listItem.dueDate != nil) {
			NSDate *theDate = [MethodHelper dateFromString:listItem.dueDate usingFormat:K_DATEONLY_FORMAT];
			[mailMessage appendFormat:@" (%@)", 
			 [MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterFullStyle withYear:YES]];
		}
		
		
		NSMutableString *tagsString = [NSMutableString stringWithString:@""];
		BOOL firstRun = TRUE;
		for (Tag *tag in tagCollection.tags) {
			if ([listItem.listItemTagCollection containsTagWithID:tag.tagID] == FALSE) {
				continue;
			}
			
			if (firstRun == TRUE) {
				[tagsString appendFormat:@" {%@", tag.name];
				firstRun = FALSE;
			} else {
				[tagsString appendFormat:@", %@", tag.name];
			}
		}
		
		if ([tagsString length] > 0) {
			[tagsString appendString:@"}"];
			[mailMessage appendString:tagsString];
		}
		
		[mailMessage appendString:@"\n"];
		
		// Check for any notes
		if ([listItem.notes length] > 0) {
			if (listItem.parentListItemID == -1) {
				[mailMessage appendString:@"  [Notes: "];
			} else {
				[mailMessage appendString:@"      [Notes: "];
			}
			
			[mailMessage appendFormat:@"%@]\n", listItem.notes];
		}
	}
	
	return mailMessage;
}

#pragma mark -
#pragma mark PDF Mail Methods

- (NSString *)createPDFMessageFromNote {
    // CGRectMake(68, 85, 634, 705)
    NSString *saveDirectory = [MethodHelper getScribblePath];
    
    NSString *pdfFileName = [saveDirectory stringByAppendingPathComponent:@"Manage Note.pdf"];
    
    // Create the PDF context using the default page size of 612 x 792.
	UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
	
	// Mark the beginning of a new page.
	UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
    // 590, 656
    
    NSString *scribblePath;

    
    NSRange glsRange = [self.scribble rangeOfString:@".gls"];
    if (glsRange.location != NSNotFound) {
        NSString *pngScribble = [self.scribble substringToIndex:glsRange.location];
        pngScribble = [NSString stringWithFormat:@"%@.png", pngScribble];
        
        scribblePath = [NSString stringWithFormat:@"%@%@",
                        [MethodHelper getScribblePath],
                        pngScribble];
        
    }
    
    
    UIImage *scribbleImage = [UIImage imageWithContentsOfFile:scribblePath];
    
    scribbleImage = [MethodHelper scaleImage:scribbleImage 
                                    maxWidth:760 maxHeight:775];
    
    CGRect drawingRect = CGRectMake(11, 95, 590, 602);
    
    [scribbleImage drawInRect:drawingRect];
    
    // Close the PDF context and write the contents out.
	UIGraphicsEndPDFContext();
    
	return pdfFileName;
}

- (NSString *)createPDFMessageWithTagCollection:(TagCollection *)tagCollection showSubtasks:(BOOL)showSubtasks {
	NSString *saveDirectory = [MethodHelper getScribblePath];
	
	NSString *pdfFileName = [saveDirectory stringByAppendingPathComponent:@"Manage Task List.pdf"];
	
	// Create the PDF context using the default page size of 612 x 792.
	UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
	
	// Mark the beginning of a new page.
	UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
	
	// Set up completed and not completed images
	UIImage *completedImage = [UIImage imageNamed:@"BoxTicked.png"];
	UIImage *notCompletedImage = [UIImage imageNamed:@"BoxNotTicked.png"];
	
	UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 553, 1)];
	[lineView setBackgroundColor:[UIColor lightGrayColor]];
	[lineView setAlpha:0.3f];
	UIGraphicsBeginImageContext(lineView.bounds.size);
	[lineView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *lineViewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
//	[lineView release];
	
	NSInteger currentPage = 0;
	
	// Total height is 616
	self.m_totalHeight = 0;
	//NSInteger lastRowBuffer = 0;
	self.currentRowHeight = 44;
	
	int i = 0;
	for (ListItem *listItem in self.listItems) {
		
		if (i == 0) {
			// Draw title
			NSString *titleString = self.title;
			[titleString drawInRect:CGRectMake(100, 35, 412, 40) 
						   withFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0] 
					  lineBreakMode:NSLineBreakByTruncatingTail
						  alignment:NSTextAlignmentCenter];
			
			[lineViewImage drawInRect:CGRectMake(25, 84, 553, 1)];
			// Draw a page number at the bottom of each page
			currentPage++;
			[self drawPageNumber:currentPage];
              
            // Draw date at bottom right
            NSString *listDate = self.creationDate;
           
            NSDate *theDate = [MethodHelper dateFromString:listDate usingFormat:K_DATEONLY_FORMAT];
            listDate = [MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES];

            [listDate drawInRect:CGRectMake(100, 55, 412, 40) 
						   withFont:[UIFont fontWithName:@"Helvetica" size:13.0] 
					  lineBreakMode:NSLineBreakByTruncatingTail
						  alignment:NSTextAlignmentCenter];
		}
		
		// Item Title
		CGRect textRect = CGRectZero;
		taskItemFrame = CGRectZero;
		
		// Setup tags, and return text rect width
		
		// Task
		if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
			textRect = CGRectMake(55, m_totalHeight + 86 + 12, 450, 21);
		} else {
			// Subtask
			textRect = CGRectMake(78, m_totalHeight + 86 + 12, 427, 21);
		}
		
		BOOL hasDueDate = FALSE;
		
		if ([listItem.dueDate length] > 0 && listItem.dueDate != nil) {
			hasDueDate = TRUE;
		}
		
		// Scribbles
		if ([listItem.scribble length] > 0 && listItem.scribble != nil) {
			NSString *scribblePath = [NSString stringWithFormat:@"%@%@",
									  [MethodHelper getScribblePath],
									  listItem.scribble];
            
            UIImage *scribbleImage;
            
            // Load the list item dictionary
            NSRange glsRange = [listItem.scribble rangeOfString:@".gls"];
            if (glsRange.length > 0) {
                NSString *pngScribble = [listItem.scribble substringToIndex:glsRange.location];
                pngScribble = [NSString stringWithFormat:@"%@.png", pngScribble];
                
                scribblePath = [NSString stringWithFormat:@"%@%@",
                                [MethodHelper getScribblePath],
                                pngScribble];
                
            } 
            
            
            scribbleImage = [UIImage imageWithContentsOfFile:scribblePath];
//            UIImage *scribbleImageTemp = [UIImage new];
//			UIImage * scribbleImageTemp = [[MethodHelper scaleImage:scribbleImage 
//											maxWidth:500 maxHeight:44] copy];
            scribbleImage = [MethodHelper scaleImage:scribbleImage 
                                                           maxWidth:500 maxHeight:44];

//            [scribbleImage release];
//            scribbleImage = nil;
//            scribbleImage = scribbleImageTemp;
//            [scribbleImageTemp release];
			
			//scribbleImage = [MethodHelper cropImage:scribbleImage usingCropRect:CGRectMake(0, 0, self.taskItemFrame.size.width, 44)];
			
			CGRect drawingRect = CGRectZero;
			
			// Task
			if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
				drawingRect = CGRectMake(50, m_totalHeight + 86, 500, 44);
			} else {
				// Subtask
				drawingRect = CGRectMake(73, m_totalHeight + 86, 500, 44);
			}
			
			[scribbleImage drawInRect:drawingRect];
//            [scribbleImage release];
		}
		
		UIView *tagsView = [self allocTagsForListItem:listItem 
									andTagCollection:tagCollection 
											  forRow:i 
									andTaskItemFrame:textRect
										  andHasDate:hasDueDate];
		if (tagsView != nil) {
			UIGraphicsBeginImageContext(tagsView.bounds.size);
			[tagsView.layer renderInContext:UIGraphicsGetCurrentContext()];
			UIImage *tagsViewImage = UIGraphicsGetImageFromCurrentImageContext();
			UIGraphicsEndImageContext();
			[tagsViewImage drawInRect:tagsView.frame];
		}

		NSString *listItemString = listItem.title;
		
		if (listItem.completed) {
			[[UIColor grayColor] setFill];
		} else {
			[[UIColor darkTextColor] setFill];
		}

		// Task item frame is edited in allocTagsForListItem method.  Did not want to do it this
		// way but i needed to return a UIView from that method and so couldn't fit the updated frame with it
		if (taskItemFrame.size.width > 0) {
			textRect = taskItemFrame;
		} else {
			taskItemFrame = textRect;
		}

		
		[listItemString drawInRect:textRect 
						  withFont:[UIFont fontWithName:@"Helvetica" size:14.0]
					 lineBreakMode:NSLineBreakByTruncatingTail
						 alignment:NSTextAlignmentLeft];
		
		// Completed icon
		CGRect completedRect = CGRectZero;
		
		// Task
		if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
			completedRect = CGRectMake(18, m_totalHeight + 86, 40, 40);
		} else {
			// Subtask
			completedRect = CGRectMake(41, m_totalHeight + 86, 40, 40);
		}
		
		if (listItem.completed) {
			[completedImage drawInRect:completedRect];
		} else {
			[notCompletedImage drawInRect:completedRect];
		}
		
		// Highlighters
		if (listItem.priority != 0 && listItem.completed == NO) {
			[self drawHighlighterAtIndex:i forListItem:listItem showSubtasks:showSubtasks];
		}
		
		// Draw the due date
		[self drawDueDateAtIndex:i forListItem:listItem andTagsView:tagsView];
		
		// Now can release tags view
//        if (tagsView != nil) {
//            [tagsView release];
//        }
		
		
		if (listItem.completed) {
			[[UIColor grayColor] setFill];
		} else {
			[[UIColor darkTextColor] setFill];
		}
		
		// Draw any notes
		if ([listItem.notes length] > 0) {
			CGRect notesRect = CGRectZero;
			
			NSString *notesString = listItem.notes;
			UIFont *notesFont = [UIFont fontWithName:@"Helvetica" size:10.0];
			
			NSInteger thisMaxRowHeight = kTotalTaskSectionHeight - self.currentRowHeight;
			
			// Need to get the height of it
			CGSize notesSize = [notesString sizeWithFont:notesFont
								   constrainedToSize:CGSizeMake(taskItemFrame.size.width, thisMaxRowHeight) lineBreakMode:NSLineBreakByWordWrapping];
			
            // Make sure the notes don't go past the end of the page, if they do, then we need to split them up

            if (notesSize.height + self.m_totalHeight >= kTotalTaskSectionHeight) {
                notesSize.height = kTotalTaskSectionHeight - self.m_totalHeight;
                
                // Write a function that will incrementally take away the last character until it fits (-3 for truncation dots). Then
                // Return our leftover string, print this on a new page and do the test again
            }
            
            /*
             while (notesSize.height + self.m_totalHeight >= kTotalTaskSectionHeight) {
             notesSize.height = kTotalTaskSectionHeight - self.m_totalHeight;
             
             // Write a function that will incrementally take away the last character until it fits (-3 for truncation dots). Then
             // Return our leftover string, print this on a new page and do the test again
             // Do it in binary form, so that we take away half and so forth
             NSString *newString = [MethodHelper concatenatedString:notesString 
             font:notesFont 
             maxSize:CGSizeMake(taskItemFrame.size.width, kTotalTaskSectionHeight - self.m_totalHeight) 
             lineBreakMode:UILineBreakModeWordWrap];
             [newString retain];
             
             while ([newString isEqualToString:notesString] == FALSE) {
             
             notesString = [notesString substringFromIndex:[newString length] - 1];
             
             
             // Add to total row height (-10 so difference isnt so pronounced.
             if (notesSize.height > 8) {
             self.currentRowHeight = (notesSize.height - 8) + (self.currentRowHeight + 2);
             }
             
             if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
             // Task
             notesRect = CGRectMake((55 + 8), m_totalHeight + 86 + 12 + (taskItemFrame.size.height - 4), (taskItemFrame.size.width - 7), notesSize.height);
             } else {
             // Subtask
             notesRect = CGRectMake((78 + 8), m_totalHeight + 86 + 12 + (taskItemFrame.size.height - 4), (taskItemFrame.size.width - 7), notesSize.height);
             }
             
             [newString drawInRect:notesRect withFont:notesFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
             
             // Draw line (replaced +44 with rowheight option)
             [lineViewImage drawInRect:CGRectMake(25, m_totalHeight + self.currentRowHeight + 84, 553, 1)];
             
             // Increment total height
             m_totalHeight = self.currentRowHeight + m_totalHeight;
             
             // Reset row height back to 44
             self.currentRowHeight = 44;
             
             // End of adding elements
             i++;
             
             if (i >= 14 || m_totalHeight >= kTotalTaskSectionHeight) {
             // Mark the beginning of a new page.
             UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
             i = 0;
             m_totalHeight = 0;
             }
             
             notesSize = [notesString sizeWithFont:notesFont
             constrainedToSize:CGSizeMake(taskItemFrame.size.width, thisMaxRowHeight) lineBreakMode:UILineBreakModeWordWrap];
             
             newString = [MethodHelper concatenatedString:notesString 
             font:notesFont 
             maxSize:CGSizeMake(taskItemFrame.size.width, thisMaxRowHeight) 
             lineBreakMode:UILineBreakModeWordWrap];
             [newString retain];
             } 
             
             */
            
			// Add to total row height (-10 so difference isnt so pronounced.
			if (notesSize.height > 8) {
				self.currentRowHeight = (notesSize.height - 8) + (self.currentRowHeight + 2);
			}
			
			
			if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
				// Task
				notesRect = CGRectMake((55 + 8), m_totalHeight + 86 + 12 + (taskItemFrame.size.height - 4), (taskItemFrame.size.width - 7), notesSize.height);
			} else {
				// Subtask
				notesRect = CGRectMake((78 + 8), m_totalHeight + 86 + 12 + (taskItemFrame.size.height - 4), (taskItemFrame.size.width - 7), notesSize.height);
			}

			
			[notesString drawInRect:notesRect withFont:notesFont lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentLeft];
		}
		
		// Draw line (replaced +44 with rowheight option)
		[lineViewImage drawInRect:CGRectMake(25, m_totalHeight + self.currentRowHeight + 84, 553, 1)];
		
		// Increment total height
		m_totalHeight = self.currentRowHeight + m_totalHeight;
		
		// Reset row height back to 44
		self.currentRowHeight = 44;
		
		// End of adding elements
		i++;
		
		if (i >= 14 || m_totalHeight >= kTotalTaskSectionHeight) {
			// Mark the beginning of a new page.
			UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
			i = 0;
			m_totalHeight = 0;
		}
	}
	
	// Close the PDF context and write the contents out.
	UIGraphicsEndPDFContext();

	return pdfFileName;
}

- (void)drawDueDateAtIndex:(NSInteger)index forListItem:(ListItem *)listItem andTagsView:(UIView *)tagsView {
	if ([listItem.dueDate length] > 0 && listItem.dueDate != nil) {
		NSDate *theDate = [MethodHelper dateFromString:listItem.dueDate usingFormat:K_DATEONLY_FORMAT];
		NSDate *today = [NSDate date];
		
		NSTimeInterval difference = [theDate timeIntervalSinceDate:today];
		
		NSString *dateString = @"";
		
		// 86400 seconds in one day
		if (difference < -86400 && [listItem completed] == FALSE) {
			[[UIColor colorWithRed:0.8274509 green:0.0235294 blue:0.0 alpha:1.0] setFill];
			dateString = @"overdue";
		} else {
			dateString = [MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterMediumStyle withYear:NO];
		}
		
		CGRect dateFrame = CGRectMake(453, m_totalHeight + 86 + 14, 120, 21);
		
		if (tagsView != nil) {
			dateFrame = CGRectMake(dateFrame.origin.x, dateFrame.origin.y - 12, dateFrame.size.width, dateFrame.size.height);
		}
		
		[dateString drawInRect:dateFrame
					  withFont:[UIFont fontWithName:@"Helvetica" size:11.0] 
				 lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentRight];
	} 
}

- (void)drawHighlighterAtIndex:(NSInteger)index forListItem:(ListItem *)listItem showSubtasks:(BOOL)showSubtasks {
	UIFont *myFont = [UIFont fontWithName:@"Helvetica" size:14.0];

	CGSize size = [listItem.title sizeWithFont:myFont];

	if (taskItemFrame.size.width > 35) {
		size.width = taskItemFrame.size.width;
	}
	
	if (size.width > 450) {
		size.width = 450;
	}
	
	if (size.width < 35) {
		size.width = 35;
	}
	
	UIView *highlightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width + 10, 21)];
	[highlightView setAlpha:0.3f];
	
	// Highlighters
	if (listItem.priority == 1) {
		[highlightView setBackgroundColor:[UIColor yellowColor]];
	} else if (listItem.priority == 2) {
		[highlightView setBackgroundColor:[UIColor redColor]];
	}
	
	UIGraphicsBeginImageContext(highlightView.bounds.size);
	[highlightView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *highlightViewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
//	[highlightView release];
	
	CGRect highlightViewFrame = CGRectMake(50, m_totalHeight + 86 + 11, size.width + 10, 21);
	
	if ([listItem parentListItemID] != -1 && showSubtasks == TRUE) {
		highlightViewFrame = CGRectMake(73, m_totalHeight + 86 + 11, size.width + 10, 21);
	}
	
	[highlightViewImage drawInRect:highlightViewFrame];
	
}

// Use Core Text to draw the text in a frame on the page.
- (CFRange)renderPage:(NSInteger)pageNum withTextRange:(CFRange)currentRange
	   andFramesetter:(CTFramesetterRef)framesetter {
	// Get the graphics context.
	CGContextRef    currentContext = UIGraphicsGetCurrentContext();
	
	// Put the text matrix into a known state. This ensures
	// that no old scaling factors are left in place.
	CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
	
	
	// Create a path object to enclose the text. Use 72 point
	// margins all around the text.
	CGRect    frameRect = CGRectMake(32, 32, 548, 648);
	CGMutablePathRef framePath = CGPathCreateMutable();
	CGPathAddRect(framePath, NULL, frameRect);
	
	// Get the frame that will do the rendering.
	// The currentRange variable specifies only the starting point. The framesetter
	// lays out as much text as will fit into the frame.
	CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
	CGPathRelease(framePath);
	
	// Core Text draws from the bottom-left corner up, so flip
	// the current transform prior to drawing.
	CGContextTranslateCTM(currentContext, 0, 792);
	CGContextScaleCTM(currentContext, 1.0, -1.0);
	
	// Draw the frame.
	CTFrameDraw(frameRef, currentContext);
	
	// Update the current range based on what was drawn.
	currentRange = CTFrameGetVisibleStringRange(frameRef);
	currentRange.location += currentRange.length;
	currentRange.length = 0;
	CFRelease(frameRef);
	
	return currentRange;
}

// Draw page number on current page
- (void)drawPageNumber:(NSInteger)pageNum
{
	NSString* pageString = [NSString stringWithFormat:@"Page %d", pageNum];
	UIFont* theFont = [UIFont systemFontOfSize:12];
	CGSize maxSize = CGSizeMake(612, 72);
	
	CGSize pageStringSize = [pageString sizeWithFont:theFont
								   constrainedToSize:maxSize
                                       lineBreakMode:NSLineBreakByClipping];
	CGRect stringRect = CGRectMake(((612.0 - pageStringSize.width) / 2.0),
								   720.0 + ((72.0 - pageStringSize.height) / 2.0) ,
								   pageStringSize.width,
								   pageStringSize.height);
	
	[pageString drawInRect:stringRect withFont:theFont];
}

// Get tags for our pdf export
- (UIView *)allocTagsForListItem:(ListItem *)listItem andTagCollection:(TagCollection *)tagCollection forRow:(NSInteger)row 
			   andTaskItemFrame:(CGRect)textRect andHasDate:(BOOL)hasDate {
	//(453, (i * 44) + 86 + 14, 120, 21)  
	UIView *tagsView = nil;
	
	CGFloat tagBuffer = 10.0;
	CGFloat tagBorder = 8.0;
	
	CGFloat totalBuffer = 0.0; //+ (tagBuffer / 2);
	
	NSInteger extraTagCount = 0;
	NSInteger indexCount = 0;
	
	CGFloat widthCheck = 595.0;
	
	//for (ListItemTag *listItemTag in listItem.listItemTagCollection.listItemTags) {
	for (Tag *theTag in tagCollection.tags) {
		
		if ([listItem.listItemTagCollection containsTagWithID:theTag.tagID] == FALSE) {
			continue;
		}
		
		// We have a tag, so init the tags view
		if (tagsView == nil) {
			NSInteger yOriginBuffer = 12;
			if (hasDate == TRUE) {
				yOriginBuffer = 18;
			}
			
			tagsView = [[UIView alloc] initWithFrame:CGRectMake(328, m_totalHeight + 86 + yOriginBuffer, 245, 21)];
		}
		
		NSString *tagName = theTag.name; //[tagCollection getNameForTagWithID:listItemTag.tagID];
		
		CGSize tagSize = [tagName sizeWithFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
		
		// Need to make sure total buffer is less than the right border of text view (add 100 to allow it to cut partway through)
		if ((widthCheck + 100) - (totalBuffer + tagSize.width + tagBorder) < textRect.origin.x + textRect.size.width) {
			extraTagCount++;
			if (indexCount == [listItem.listItemTagCollection.listItemTags count] - 1) {
				// Last index
				tagName = [NSString stringWithFormat:@"+%d", extraTagCount];
				tagSize = [tagName sizeWithFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
			} else {
				indexCount++;
				continue;
			}
		}
		
		// Add the label to the tag view
		UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(tagsView.frame.size.width - totalBuffer - tagBorder - tagSize.width - 5, 
																	  -1, 
																	  tagSize.width + tagBorder, tagsView.frame.size.height)];
		
		totalBuffer = totalBuffer + tagLabel.frame.size.width + tagBuffer;
		
		[tagLabel setBackgroundColor:[UIColor clearColor]];
		[tagLabel setTextAlignment:NSTextAlignmentCenter];
		[tagLabel setTextColor:[UIColor darkTextColor]];
		[tagLabel setText:tagName];
		[tagLabel setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
		
		QuartzTagView *quartzTagView = [[QuartzTagView alloc] initWithFrame:CGRectMake(tagLabel.frame.origin.x - (tagBorder / 2), 
																					   0, tagLabel.frame.size.width + tagBorder, 
																					   20)
                                        andTagColour:theTag.colour];
		//quartzTagView.whiteBackground = TRUE;
		
		// If this goes over task item then make it transparent
		[quartzTagView setAlpha:0.9f];
		[tagLabel setAlpha:0.9f];
		
		[tagsView addSubview:quartzTagView];
//		[quartzTagView release];
		
		if (listItem.completed || listItem.archived) {
			[tagsView setAlpha:0.3f];
		} else {
			[tagsView setAlpha:1.0f];
		}
		
		
		//[self.tagsView addSubview:tagLabel];
		[tagsView addSubview:tagLabel];
//		[tagLabel release];
		
		// Increment index count
		indexCount++;
		
		// Position each label and reposition rest of row
	}
	
	if (widthCheck - totalBuffer < textRect.origin.x + textRect.size.width) {
		CGFloat cutAmount = (textRect.origin.x + textRect.size.width) - (widthCheck - totalBuffer);
		cutAmount += (tagBuffer * 2) + 10;
		
		taskItemFrame = CGRectMake(textRect.origin.x, textRect.origin.y, 
								   textRect.size.width - cutAmount, textRect.size.height);
	}
	
	return tagsView;
	
}

#pragma mark -
#pragma mark Repeating Tasks Methods

- (ListItem *)allocProcessRepeatingTaskAtIndex:(NSInteger)index {
	if (index >= [self.listItems count]) {
		return nil;
	}
	
	ListItem *listItem = [self.listItems objectAtIndex:index];
	if ([listItem recurringListItemID] == -1) {
		return nil;
	}
	
	// Must set due date depending on settings of list item
	// Need the repeating item
	RecurringListItem *recurringListItem = [[RecurringListItem alloc] initWithRecurringListItemID:[listItem recurringListItemID]];
	
	NSDate *calculatedDueDate = [recurringListItem getNewDueDateUsingExistingDueDate:listItem.dueDate];
	NSString *newDueDate = [MethodHelper stringFromDate:calculatedDueDate usingFormat:K_DATEONLY_FORMAT];
//	[recurringListItem release];
	
	// Cool, this list item needs to be 'copied', then have its recurring list item ID set back to -1 (update alerttime field)
	ListItem *repeatingListItem = [[ListItem alloc] initNewListItemWithID:listItem.listID 
																 andTitle:listItem.title 
																 andNotes:listItem.notes 
															 andCompleted:NO 
														   andParentTitle:listItem.parentListTitle 
															   andDueDate:newDueDate
                                                               andDueTime:listItem.dueTime
                                                             andAlertTime:listItem.alertTime
                                                                 andSound:listItem.sound
                                                                andSnooze:listItem.hasSnooze
												 andListItemTagCollection:listItem.listItemTagCollection 
													 andCompletedDateTime:@""
                                                              andPriority:listItem.priority
                                                                 addToEnd:FALSE];
	repeatingListItem.creationDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	repeatingListItem.subItemOrder = 0;
	repeatingListItem.parentListItemID = -1;
	repeatingListItem.recurringListItemID = listItem.recurringListItemID;
	repeatingListItem.toodledoTaskID = listItem.toodledoTaskID;

	// Make copy of original scribble, (if needed)
	if ([listItem.scribble length] > 0) {
		repeatingListItem.scribble = [MethodHelper getCopyOfScribble:listItem.scribble];
        
	}
	
	// Insert the repeating list item into first row
	//[self.listItems insertObject:repeatingListItem atIndex:0];
	
	// Update the database with extra changes, unfortunately this means there were 2 DB updates, but the original reason was we needed to get the list item
	// ID before creating the object in the DB.  Would still require 2 DB transactions to fetch ne next ID, and go for it.  Although this ID may be auto increment???
	
	//[repeatingListItem updateDatabase];
	
	//[repeatingListItem release];
	
	return repeatingListItem;
}

#pragma mark -
#pragma mark Class Methods

// Get completed date time difference
- (NSInteger)getCompletedDateDifferenceFromToday {
	if (self.completedDateTime == nil || [self.completedDateTime length] == 0) {
		// Just return a large number, but this should never occur
		return 100;
	}
	
	NSDate *lastDate = [MethodHelper dateFromString:self.completedDateTime usingFormat:K_DATETIME_FORMAT];
	NSString *todayDateString = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	NSDate *todaysDate = [MethodHelper dateFromString:todayDateString usingFormat:K_DATETIME_FORMAT];
	NSTimeInterval lastDiff = [lastDate timeIntervalSinceNow];
	NSTimeInterval todaysDiff = [todaysDate timeIntervalSinceNow];
	NSTimeInterval dateDiff = lastDiff - todaysDiff;
	NSInteger dayDifference = (int)(dateDiff / 86400.0f);
	return dayDifference;
}

- (NSInteger)getNextListOrder {
	return [BLList getNextListOrder];
}

- (void)updateSubtasksListItemOrderForListItem:(ListItem *)parentListItem {
	for (ListItem *listItem in self.listItems) {
		if ([listItem parentListItemID] == parentListItem.listItemID) {
			// Update the item order
			listItem.itemOrder = parentListItem.itemOrder;
			// Shit need to update database still.  [
			[listItem updateDatabaseWithoutModifiedLog];
		}
	}
}

// Will go through and update the subtasks list item order and their sub item order.  Only used for when moving subtask from start of list to end or beginning
- (void)updateSubtasksSubListItemAndListItemOrderForListItem:(ListItem *)parentListItem {
    NSInteger newSubItemOrder = 0;
	for (ListItem *listItem in self.listItems) {
		if ([listItem parentListItemID] == parentListItem.listItemID) {
			// Update the item order
			listItem.itemOrder = parentListItem.itemOrder;
            
            // Update the sub item order
            listItem.subItemOrder = newSubItemOrder;
            // Increment sub item order for the next encountered sub item.
            newSubItemOrder++;
            
			// Shit need to update database still.  [
			[listItem updateDatabaseWithoutModifiedLog];
		}
	}
}


// To correctly ignore the parent list item ID, we need to pass in a large negative to as there are list items with parentListItemID = -1, -100 is a good number
- (void)updateListItemOrderFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex ignoringParentListItemID:(NSInteger)parentListItemID {
	// First list item itemOrder has already been set, so ignore it (but still update it in database)
	[BLListItem updateItemOrder:[[self.listItems objectAtIndex:toIndex] itemOrder] 
				  forListItemID:[[self.listItems objectAtIndex:toIndex] listItemID]];
	
	// Update all the list items
	if (fromIndex < toIndex) {
		for (int i = fromIndex; i <= toIndex - 1; i++) {
			ListItem *listItem = [self.listItems objectAtIndex:i];
			
			if (listItem.parentListItemID != parentListItemID) {
				listItem.itemOrder++;
			}
			
			[BLListItem updateItemOrder:listItem.itemOrder 
						  forListItemID:listItem.listItemID];
		}
	} else if (fromIndex > toIndex) {
		for (int i = fromIndex; i >= toIndex + 1; i--) {
			ListItem *listItem = [self.listItems objectAtIndex:i];
			
			if (listItem.parentListItemID != parentListItemID) {
				listItem.itemOrder--;
                
			}
			
			[BLListItem updateItemOrder:listItem.itemOrder 
						  forListItemID:listItem.listItemID];
		}
	}
}


- (void)updateSubListItemOrderFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
	// First list item subItemOrder has already been set, so ignore it (but still update it in database)
	[BLListItem updateSubItemOrder:[[self.listItems objectAtIndex:toIndex] subItemOrder] 
					 forListItemID:[[self.listItems objectAtIndex:toIndex] listItemID]];
	
	// Update all the list items
	if (fromIndex < toIndex) {
		for (int i = fromIndex; i <= toIndex - 1; i++) {
			ListItem *listItem = [self.listItems objectAtIndex:i];
			listItem.subItemOrder++;
			
			[BLListItem updateSubItemOrder:listItem.subItemOrder 
							 forListItemID:listItem.listItemID];
		}
	} else if (fromIndex > toIndex) {
		for (int i = fromIndex; i >= toIndex + 1; i--) {
			ListItem *listItem = [self.listItems objectAtIndex:i];
			listItem.subItemOrder--;
			
			[BLListItem updateSubItemOrder:listItem.subItemOrder 
							 forListItemID:listItem.listItemID];
		}
	}
}

- (void)sortListWithSortType:(NSInteger)sortType {
	// Create our different sort descriptors
	NSSortDescriptor *sortItemOrder = [[NSSortDescriptor alloc] initWithKey:@"itemOrder"ascending:NO];// autorelease];
	NSSortDescriptor *sortParentListItemID = [[NSSortDescriptor alloc] initWithKey:@"parentListItemID" ascending:YES];// autorelease];
	NSSortDescriptor *sortSubItemOrder = [[NSSortDescriptor alloc] initWithKey:@"subItemOrder" ascending:NO];// autorelease];
	NSSortDescriptor *sortPriority = [[NSSortDescriptor alloc] initWithKey:@"priority" ascending:NO];// autorelease];
	NSSortDescriptor *sortCreationDateTime = [[NSSortDescriptor alloc] initWithKey:@"creationDateTime"ascending:NO];// autorelease];
	NSSortDescriptor *sortDueDate = [[NSSortDescriptor alloc] initWithKey:@"dueDate" ascending:YES selector:@selector(localizedCompare:)];// autorelease];
	NSSortDescriptor *sortCompleted = [[NSSortDescriptor alloc] initWithKey:@"completed" ascending:NO];// autorelease];
	NSSortDescriptor *sortCompletedDateTime = [[NSSortDescriptor alloc] initWithKey:@"completedDateTime" ascending:NO];// autorelease];
	
	NSArray *sortDescriptors = nil;
	
	switch (sortType) {
		case EnumSortByList:
			sortDescriptors = [NSArray arrayWithObjects:sortItemOrder, sortParentListItemID, 
										sortSubItemOrder, sortCreationDateTime, nil];
			break;
		case EnumSortByPriority:
			sortDescriptors = [NSArray arrayWithObjects:sortPriority, sortItemOrder, sortParentListItemID,
							   sortSubItemOrder, sortCreationDateTime, nil];
			break;
		case EnumSortByDueDate:
			sortDescriptors = [NSArray arrayWithObjects:sortDueDate, sortItemOrder, sortParentListItemID, 
							   sortSubItemOrder, sortCreationDateTime, nil];
			break;
		case EnumSortByCompleted:
			sortDescriptors = [NSArray arrayWithObjects:sortCompleted, sortCompletedDateTime, sortItemOrder, sortParentListItemID,
							   sortSubItemOrder, sortCreationDateTime, nil];
			break;
		default:
			break;
	}
	
	NSArray *sortedArray = [self.listItems sortedArrayUsingDescriptors:sortDescriptors];
	[self.listItems removeAllObjects];
	[self.listItems addObjectsFromArray:sortedArray];
	
	if (sortType == EnumSortByDueDate) {
		// Empty due dates are at the front, move them all to back
		// First place a copy of every list item with no due date in second array
		/*NSMutableArray *emptyDueDateArray = [[NSMutableArray alloc] init];
		for (ListItem *listItem in self.listItems) {
			if (listItem.dueDate == nil || [listItem.dueDate length] == 0) {
				ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
				[emptyDueDateArray addObject:newListItem];
				[newListItem release];
			}
		}
		
		// Ok, now have a copy of all, now remove any empty due date list items
		if ([self.listItems count] > 0) {
			for (int i = [self.listItems count] - 1; i >= 0; i--) {
				ListItem *listItem = [self.listItems objectAtIndex:i];
				if (listItem.dueDate == nil || [listItem.dueDate length] == 0) {
					[self.listItems removeObjectAtIndex:i];
					
				}
			}
		}
		
		// Ok, now place the empty due dates array at end of list items array
		[self.listItems addObjectsFromArray:emptyDueDateArray];
		[emptyDueDateArray release];*/
		
		// Dont want to move the same object twice, so set marker the first time
		ListItem *markerListItem = nil;
		for (int i = [self.listItems count] - 1; i > 0; i--) {
			ListItem *listItem = [self.listItems objectAtIndex:i];
			if (listItem.dueDate != nil && [listItem.dueDate length] > 0) {
				if (markerListItem != nil && [markerListItem isEqual:listItem]) {
					// Exit
					return;
				} else if (markerListItem == nil) {
					markerListItem = listItem;
				}
				[self.listItems moveObjectFromIndex:i toIndex:0];
				i++;
			}
		}
	}
}

// Need to see if there is a new parent task assigned for this subtask and update
// Note: For this to work we need to update all the old sub items with new order, then find the new parent listItem and update its sub item orders as well
- (void)updateNewParentForSubTask:(ListItem *)subTaskItem atIndex:(NSInteger)subTaskIndex {
    if (subTaskIndex == 0) {
        return;
    }
    
    ListItem *parentListItem = nil;
    for (int i = subTaskIndex; i >= 0; i--) {
        parentListItem = [self.listItems objectAtIndex:i];
        if ([parentListItem parentListItemID] == -1) {
            // Is a parent task
            break;
        }
    }
    
    if (parentListItem == nil) {
        return;
    }
    
    // Update the list item with new parent list item id and item order
    subTaskItem.parentListItemID = parentListItem.listItemID;
    subTaskItem.itemOrder = parentListItem.itemOrder;
}

@end
