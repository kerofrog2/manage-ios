//
//  Alarm.m
//  Manage
//
//  Created by Cliff Viegas on 14/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "Alarm.h"

// Method Helper
#import "MethodHelper.h"

// Business Layer
#import "BLAlarm.h"

// Data Layer
#import "SQLiteAccess.h"

// Dictionary Method Helper
#import "DMHelper.h"

@implementation Alarm

@synthesize alarmGUID;
@synthesize listItemID;
@synthesize fireDateTime;
@synthesize delay;
@synthesize measurement;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[alarmGUID release];
//	[fireDateTime release];
//	[measurement release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		self.alarmGUID = @"";
		self.listItemID = -1;
		self.fireDateTime = @"";
		self.delay = -1;
		self.measurement = @"";
	}
	return self;
}

- (id)initWithListItemID:(NSInteger)theListItemID {
	if (self = [super init]) {
		// Get guid
		self.alarmGUID = [MethodHelper getUniqueGUID];
		
		self.listItemID = theListItemID;
		
		// Firedate, measurement and other details will be added by calling method
		self.fireDateTime = @"";
		self.delay = -1;
		self.measurement = @"";
	}
	return self;
}

- (id)initWithAlarmGUID:(NSString *)theAlarmGUID {
	if (self = [super init]) {
		NSDictionary *alarmDictionary = [BLAlarm getAlarmWithAlarmGUID:theAlarmGUID];
		self.listItemID = [DMHelper getNSIntegerValueForKey:@"ListItemID" fromDictionary:alarmDictionary];
		self.alarmGUID = [DMHelper getNSStringValueForKey:@"AlarmGUID" fromDictionary:alarmDictionary];
		self.fireDateTime = [DMHelper getNSStringValueForKey:@"FireDateTime" fromDictionary:alarmDictionary];
		self.delay = [DMHelper getNSIntegerValueForKey:@"Delay" fromDictionary:alarmDictionary];
		self.measurement = [DMHelper getNSStringValueForKey:@"Measurement" fromDictionary:alarmDictionary];
	}
	return self;
}


#pragma mark -
#pragma mark Database Methods

- (void)insertSelfIntoDatabase {
	[BLAlarm insertNewAlarm:self];
}

- (void)updateDatabase {
	[BLAlarm updateDatabaseWithAlarm:self];
}

- (void)deleteSelfFromDatabase {
	[BLAlarm deleteAlarmWithGUID:self.alarmGUID];
}

#pragma mark -
#pragma mark Helper Methods

- (BOOL)removeFromNotificationQueue {
	NSArray *scheduledNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
	
	// Find and remove the existing local notification
	BOOL found = FALSE;
	for (int i = 0; i < [scheduledNotifications count] && found == FALSE; i++) {
		UILocalNotification *localNotification = [scheduledNotifications objectAtIndex:i];
		
		// Need to make sure that we assign alarm guid to each local notification
		NSString *key = [localNotification.userInfo objectForKey:@"AlarmGUID"];
		
		if ([key isEqualToString:self.alarmGUID]) {
			[[UIApplication sharedApplication] cancelLocalNotification:localNotification];
			found = TRUE;
		}
	}

//	BOOL found = FALSE;
	return found;
}

#pragma mark - Add Notification To Queue -
// Will create a new notification to add to queue.  Need to pass alarm
+ (void)addNewNotificationToQueue:(Alarm *)alarm andListItem:(ListItem *)item{
    UILocalNotification *alarmNotification = [[UILocalNotification alloc] init];
	
	if (alarmNotification == nil) {
		[MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Unable to create local notification (reminder).  Please report this issue to support@kerofrog.com.au"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	alarmNotification.timeZone = [NSTimeZone defaultTimeZone];
    alarmNotification.fireDate = [MethodHelper dateFromString:[NSString stringWithFormat:@"%@", alarm.fireDateTime] usingFormat:K_DATETIME_FORMAT];
    
    if ([item.scribble length] > 0) {
        if ([item.title length] > 0) {
            alarmNotification.alertBody = item.title;
        }else{
            
            if ([item.notes length] > 120) {
                // Cut notes string
                item.notes = [[item.notes substringToIndex:120] stringByAppendingString:@" ..."];
            }
            alarmNotification.alertBody = item.notes;
        }
    }else{
        alarmNotification.alertBody = item.title;
    }
    
    alarmNotification.alertAction = @"View";
	alarmNotification.soundName = UILocalNotificationDefaultSoundName;
	
	// Need to create key using AlarmGUID for reference
    NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d", item.listItemID],@"AlarmItemID",[NSString stringWithFormat:@"%@", alarm.alarmGUID],@"AlarmGUID", nil];
	//NSDictionary *infoDict = [NSDictionary dictionaryWithObjects:[NSString stringWithFormat:@"%d", item.listItemID] forKey:@"AlarmItemID"];
	alarmNotification.userInfo = infoDict;
	
	// Schedule the local notification
	[[UIApplication sharedApplication] scheduleLocalNotification:alarmNotification];
//	[alarmNotification release];
	
	// Need to check if this is ios 4 or above, otherwise display warning

    
}


@end
