//
//  BLListItemTag.h
//  Manage
//
//  Created by Cliff Viegas on 10/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ListItemTag;

@interface BLListItemTag : NSObject {

}

#pragma mark DELETE Methods
+ (void)deleteListItemTagWithTagID:(NSInteger)tagID andListItemID:(NSInteger)listItemID;
+ (void)deleteAllListItemTagsWithListItemID:(NSInteger)listItemID;
+ (void)deleteAllListItemTagsWithTagID:(NSInteger)tagID;
+ (void)deleteAllListItemTags;

#pragma mark INSERT Methods
+ (NSInteger)insertNewListItemTag:(ListItemTag *)listItemTag;

@end
