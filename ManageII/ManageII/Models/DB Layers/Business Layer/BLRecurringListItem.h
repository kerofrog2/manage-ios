//
//  BLRecurringListItem.h
//  Manage
//
//  Created by Cliff Viegas on 9/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class RecurringListItem;

@interface BLRecurringListItem : NSObject {

}

#pragma mark SELECT Methods
+ (NSDictionary *)getRecurringListItemWithID:(NSInteger)theRecurringListItemID;
+ (BOOL)checkIfRecurringListItemExistsWithID:(NSInteger)theRecurringListItemID;

#pragma mark INSERT Methods
+ (NSInteger)insertNewRecurringListItem:(RecurringListItem *)recurringListItem;

#pragma mark UPDATE Methods
+ (void)updateDatabaseWithRecurringListItem:(RecurringListItem *)recurringListItem; 

#pragma mark DELETE Methods
+ (void)deleteRecurringListItemWithRecurringListItemID:(NSInteger)recurringListItemID;
+ (void)deleteAllRecurringListItems;

#pragma mark Class Methods
+ (NSInteger)getNextRecurringListItemID;

@end
