//
//  BLDeletedListItem.h
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class DeletedListItem;

@interface BLDeletedListItem : NSObject {

}

#pragma mark SELECT Methods
+ (NSArray *)getAllDeletedListItems;

# pragma INSERT Methods
+ (void)insertNewDeletedListItem:(DeletedListItem *)deletedListItem;
+ (void)emptyDatabase;

@end
