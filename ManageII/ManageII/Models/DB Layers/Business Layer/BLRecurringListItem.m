//
//  BLRecurringListItem.m
//  Manage
//
//  Created by Cliff Viegas on 9/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "BLRecurringListItem.h"

// Data Layer
#import "SQLiteAccess.h"

// Data Models
#import "RecurringListItem.h"

// Method Helper
#import "MethodHelper.h"

@implementation BLRecurringListItem

#pragma mark -
#pragma mark SELECT Methods

// Get the recurring list with the passed ID
+ (NSDictionary *)getRecurringListItemWithID:(NSInteger)theRecurringListItemID {
	NSString *sql = [NSString stringWithFormat:@"SELECT * FROM RecurringListItem WHERE RecurringListItemID = %d", theRecurringListItemID];
	return [SQLiteAccess selectOneRowWithSQL:sql];
}

// Check if the passed recurring list item ID exists in the table
+ (BOOL)checkIfRecurringListItemExistsWithID:(NSInteger)theRecurringListItemID {
	NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM RecurringListItem WHERE RecurringListItemID = %d", theRecurringListItemID];
	
	NSInteger count = [[SQLiteAccess selectOneValueSQL:sql] integerValue];
	
	if (count == 0) {
		return FALSE;
	}
	
	return TRUE;
}

#pragma mark -
#pragma mark INSERT Methods

// Insert the passed recurringlistitem and returns the created recurring list item id
+ (NSInteger)insertNewRecurringListItem:(RecurringListItem *)recurringListItem {
	NSInteger newRecurringListItemID = [self getNextRecurringListItemID];
	
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO RecurringListItem (RecurringListItemID, "
					 "Frequency, FrequencyMeasurement, DaysOfTheWeek, UseCompletedDate) Values (%d, %d, %@, %@, %d)",
					 newRecurringListItemID, recurringListItem.frequency, 
					 [MethodHelper escapeStringsForSQL:recurringListItem.frequencyMeasurement],
					 [MethodHelper escapeStringsForSQL:recurringListItem.daysOfTheWeek],
					 recurringListItem.useCompletedDate];
	//NSLog(@"%@", sql);
	[SQLiteAccess insertWithSQL:sql];
	return newRecurringListItemID;
}

#pragma mark -
#pragma mark UPDATE Methods

+ (void)updateDatabaseWithRecurringListItem:(RecurringListItem *)recurringListItem {
	NSString *sql = [NSString stringWithFormat:@"UPDATE RecurringListItem SET Frequency = %d, "
					 "FrequencyMeasurement = %@, DaysOfTheWeek = %@, UseCompletedDate = %d WHERE RecurringListItemID = %d",
					 recurringListItem.frequency,
					 [MethodHelper escapeStringsForSQL:recurringListItem.frequencyMeasurement],
					 [MethodHelper escapeStringsForSQL:recurringListItem.daysOfTheWeek],
					 recurringListItem.useCompletedDate,
					 recurringListItem.recurringListItemID];
	//NSLog(@"%@", sql);
	[SQLiteAccess updateWithSQL:sql];
}


#pragma mark -
#pragma mark DELETE Methods

+ (void)deleteRecurringListItemWithRecurringListItemID:(NSInteger)recurringListItemID {
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM RecurringListItem WHERE RecurringListItemID = %d", recurringListItemID];
	//NSLog(@"%@", sql);
	[SQLiteAccess deleteWithSQL:sql];
}

+ (void)deleteAllRecurringListItems {
	NSString *sql = @"DELETE FROM RecurringListItem";
	[SQLiteAccess deleteWithSQL:sql];
}

#pragma mark -
#pragma mark Class Methods

+ (NSInteger)getNextRecurringListItemID {
	NSString *sql = @"SELECT RecurringListItemID FROM RecurringListItem ORDER BY RecurringListItemID DESC LIMIT 1";
	NSString *largestID = [SQLiteAccess selectOneValueSQL:sql];
	NSInteger nextID = 0;
	if (largestID != nil) {
		nextID = [largestID integerValue] + 1;
	}
	return nextID;
}



@end
