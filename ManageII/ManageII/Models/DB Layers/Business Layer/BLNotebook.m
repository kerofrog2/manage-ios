//
//  BLNotebook.m
//  Manage
//
//  Created by Cliff Viegas on 17/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "BLNotebook.h"

// Data Layer
#import "SQLiteAccess.h"

// Method Helper
#import "MethodHelper.h"

// Data Models
#import "Notebook.h"

@implementation BLNotebook

#pragma mark - SELECT Methods

+ (NSArray *)getAllNotebooks {
	NSString *sql = @"SELECT * FROM Notebook ORDER BY ItemOrder DESC";
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

+ (NSInteger)getNextNotebookOrder {
	NSString *sql = @"SELECT ItemOrder FROM Notebook ORDER BY ItemOrder DESC LIMIT 1";
	NSString *largestListOrder = [SQLiteAccess selectOneValueSQL:sql];
	
	NSInteger nextListOrder = 0;
	if (largestListOrder != nil && [largestListOrder isEqual:[NSNull null]] == FALSE) {
		nextListOrder = [largestListOrder integerValue] + 1;
	}
	
	//NSLog(@"%@", sql);
	return nextListOrder;
}

#pragma mark - DELETE Methods

+ (void)deleteNotebookWithID:(NSInteger)notebookID {
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM Notebook WHERE NotebookID = %d", notebookID];
	
	//NSLog(@"%@", sql);
	[SQLiteAccess deleteWithSQL:sql];
	// Also need to delete list items
}

#pragma mark - INSERT Methods

+ (NSInteger)insertNewNotebook:(Notebook *)notebook {
    NSString *listsString = @"";
    
    BOOL first = TRUE;
    for (NSString *listNumber in notebook.lists) {
        if (first) {
            listsString = listNumber;
            first = FALSE;
        } else if (first == FALSE) {
            listsString = [NSString stringWithFormat:@"%@, %@", listsString, listNumber];
        }
    }
    
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO Notebook (Title, CreationDate, ItemOrder, Lists) "
                     "VALUES (%@, %@, %d, %@)",
                     [MethodHelper escapeStringsForSQL:notebook.title], [MethodHelper escapeStringsForSQL:notebook.creationDate],
                     notebook.itemOrder, [MethodHelper escapeStringsForSQL:listsString]];
    
	//NSLog(@"%@", sql);
	return [[SQLiteAccess insertWithSQL:sql] integerValue];
}

#pragma mark - UPDATE Methods

+ (void)updateDatabaseWithNotebook:(Notebook *)notebook {
    // To convert the list into a string for the DB
    NSString *listsString = @"";
    
    BOOL first = TRUE;
    for (NSString *listNumber in notebook.lists) {
        if (first) {
            
            listsString = listNumber;
            
            first = FALSE;
        } else if (first == FALSE) {
            listsString = [NSString stringWithFormat:@"%@, %@", listsString, listNumber];
        }
    }
    
    NSString *sql = [NSString stringWithFormat:@"UPDATE Notebook SET Title = %@, CreationDate = %@, ItemOrder = %d, Lists = %@ "
                     "WHERE NotebookID = %d", [MethodHelper escapeStringsForSQL:notebook.title],
                     [MethodHelper escapeStringsForSQL:notebook.creationDate], notebook.itemOrder,
                     [MethodHelper escapeStringsForSQL:listsString], notebook.notebookID];
    //NSLog(@"%@", sql);
    [SQLiteAccess updateWithSQL:sql];
}

@end
