//
//  BLApplicationSetting.m
//  Manage
//
//  Created by Cliff Viegas on 5/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "BLApplicationSetting.h"

// Data Layer
#import "SQLiteAccess.h"

// Data Models
#import "ApplicationSetting.h"

// Method Helper
#import "MethodHelper.h"

@implementation BLApplicationSetting

#pragma mark -
#pragma mark SELECT Methods

+ (NSArray *)getAllApplicationSettings {
	NSString *sql = @"SELECT * FROM ApplicationSetting";
	NSArray *settingsArray = [SQLiteAccess selectManyRowsWithSQL:sql];
	return settingsArray;
}

+ (NSArray *)getAllUserApplicationSettings {
	NSString *sql = @"SELECT * FROM ApplicationSetting WHERE SettingID >= 200 AND SettingID < 300";
	NSArray *settingsArray = [SQLiteAccess selectManyRowsWithSQL:sql];
	return settingsArray;
}

+ (NSString *)getSettingData:(NSString *)settingName {
	NSString *sql = [NSString stringWithFormat:@"SELECT Data FROM ApplicationSetting WHERE Name = %@",
					 [self escapeStringsForSQL:settingName]];
	return [SQLiteAccess selectOneValueSQL:sql];
}

+ (NSString *)getVersionNumber {
	NSString *sql = @"SELECT COUNT (*) FROM ApplicationSetting WHERE Name = 'VersionNumber'";
	NSInteger count = [[SQLiteAccess selectOneValueSQL:sql] integerValue];
	
	if (count == 0) {
		return @"0";
	}
	
	sql = @"SELECT Data FROM ApplicationSetting WHERE Name = 'VersionNumber'";
	return [SQLiteAccess selectOneValueSQL:sql];
}

#pragma mark -
#pragma mark UPDATE Methods

+ (void)updateSetting:(NSString *)settingName withData:(NSString *)data {
	
	NSString *sql = [NSString stringWithFormat:@"UPDATE ApplicationSetting SET Data = %@ WHERE Name = %@",
					 [self escapeStringsForSQL:data],
					 [self escapeStringsForSQL:settingName]];
	[SQLiteAccess updateWithSQL:sql];
}

#pragma mark -
#pragma mark INSERT Methods

+ (void)insertSetting:(ApplicationSetting *)appSetting {
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO ApplicationSetting (Name, Data) VALUES "
					 "(%@, %@)", 
					 [MethodHelper escapeStringsForSQL:appSetting.name], 
					 [MethodHelper escapeStringsForSQL:appSetting.data]];
	[SQLiteAccess insertWithSQL:sql];
}


#pragma mark -
#pragma mark Helper Methods

// Adds escape characters for strings that have inverted commas
+ (NSString *)escapeStringsForSQL:(NSString *)inputString {
	if([inputString isEqualToString:@""]){
		return @"null";
	}
	NSString *returnValue = [inputString stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
	returnValue = [NSString stringWithFormat:@"'%@'", returnValue];
	return returnValue;
}

+ (BOOL)checkIfSettingExists:(NSString *)settingName {
	NSString *sql = [NSString stringWithFormat:@"SELECT COUNT (*) FROM ApplicationSetting WHERE Name = %@",
					 [self escapeStringsForSQL:settingName]];
	NSInteger count = [[SQLiteAccess selectOneValueSQL:sql] integerValue];
	
	if (count > 0) {
		return TRUE;
	}
	return FALSE;
}

@end
