//
//  BLDeletedList.h
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class DeletedTaskList;

@interface BLDeletedList : NSObject {

}

#pragma mark SELECT Methods
+ (NSArray *)getAllDeletedLists;

#pragma mark INSERT Methods
+ (void)insertNewDeletedList:(DeletedTaskList *)deletedList;

#pragma mark DELETE Methods
+ (void)emptyDatabase;

@end
