//
//  TagCollection.m
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "TagCollection.h"

// Data Models
#import "Tag.h"

// Business Layer
#import "BLTag.h"
#import "BLListItemTag.h"

// Dictionary Method Helper
#import "DMHelper.h"

@interface TagCollection()
// Tag Sorting
- (NSInteger)getStartIndex;
- (NSInteger)getUpdatedChildTagsInsertIndex:(NSInteger)index withFocusTagID:(NSInteger)focusTagID;
@end


@implementation TagCollection


@synthesize tags;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[self.tags release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithAllTags {
	if ((self = [super init])) {
		self.tags = [NSMutableArray array];
		
		// Get the array of all tags in the database
		NSArray *localArray = [BLTag getAllTags];
		
		// Create a new object for each tag taken from the database
		for (NSDictionary *tagDictionary in localArray) {
			NSInteger theTagID = [DMHelper getNSIntegerValueForKey:@"TagID" fromDictionary:tagDictionary];
			NSString *theName = [DMHelper getNSStringValueForKey:@"Name" fromDictionary:tagDictionary];
			NSInteger theParentTagID = [DMHelper getNSIntegerValueForKey:@"ParentTagID" fromDictionary:tagDictionary];
			NSInteger theTagOrder = [DMHelper getNSIntegerValueForKey:@"TagOrder" fromDictionary:tagDictionary];
			NSInteger theTagDepth = [DMHelper getNSIntegerValueForKey:@"TagDepth" fromDictionary:tagDictionary];
            NSString *theTagColour = [DMHelper getNSStringValueForKey:@"Colour" fromDictionary:tagDictionary];
			
			Tag *tag = [[Tag alloc] initWithTagID:theTagID andName:theName 
									andParentTagID:theParentTagID andTagOrder:theTagOrder
									   andTagDepth:theTagDepth
                                     andTagColour:theTagColour];
			
			[self.tags addObject:tag];
//			[tag release];
		}
		
		// Need to sort the array
		[self sortTags];
	}
	return self;
}

- (id)initWithAllTagsForParentTagID:(NSInteger)parentTagID {
	if ((self = [super init])) {
		self.tags = [NSMutableArray array];
		
		// Get the array of all tags in the database
		NSArray *localArray = [BLTag getAllTagsWithParentTagID:parentTagID];
		
		// Create a new object for each tag taken from the database
		for (NSDictionary *tagDictionary in localArray) {
			NSInteger theTagID = [DMHelper getNSIntegerValueForKey:@"TagID" fromDictionary:tagDictionary];
			NSString *theName = [DMHelper getNSStringValueForKey:@"Name" fromDictionary:tagDictionary];
			NSInteger theParentTagID = [DMHelper getNSIntegerValueForKey:@"ParentTagID" fromDictionary:tagDictionary];
			NSInteger theTagOrder = [DMHelper getNSIntegerValueForKey:@"TagOrder" fromDictionary:tagDictionary];
			NSInteger theTagDepth = [DMHelper getNSIntegerValueForKey:@"TagDepth" fromDictionary:tagDictionary];
			NSString *theTagColour = [DMHelper getNSStringValueForKey:@"Colour" fromDictionary:tagDictionary];
            
			Tag *tag = [[Tag alloc] initWithTagID:theTagID andName:theName
									andParentTagID:theParentTagID andTagOrder:theTagOrder
									   andTagDepth:theTagDepth
                                      andTagColour:theTagColour];// autorelease];
			
			[self.tags addObject:tag];
		}

	}
	
	return self;
}

- (id)initWithAllTagsIgnoringTagFamily:(NSMutableArray *)tagFamily {
	if ((self = [super init])) {
		self.tags = [NSMutableArray array];
		
		// Get the array of all tags in the database
		NSArray *localArray = [BLTag getAllTags];
		
		// Create a new object for each tag taken from the database
		for (NSDictionary *tagDictionary in localArray) {
			NSInteger theTagID = [DMHelper getNSIntegerValueForKey:@"TagID" fromDictionary:tagDictionary];
			NSString *theName = [DMHelper getNSStringValueForKey:@"Name" fromDictionary:tagDictionary];
			NSInteger theParentTagID = [DMHelper getNSIntegerValueForKey:@"ParentTagID" fromDictionary:tagDictionary];
			NSInteger theTagOrder = [DMHelper getNSIntegerValueForKey:@"TagOrder" fromDictionary:tagDictionary];
			NSInteger theTagDepth = [DMHelper getNSIntegerValueForKey:@"TagDepth" fromDictionary:tagDictionary];
			NSString *theTagColour = [DMHelper getNSStringValueForKey:@"Colour" fromDictionary:tagDictionary];
            
			Tag *tag = [[Tag alloc] initWithTagID:theTagID andName:theName
									andParentTagID:theParentTagID andTagOrder:theTagOrder
									   andTagDepth:theTagDepth
                                      andTagColour:theTagColour];// autorelease];
			
			// Make sure tag is not in the tag family
			BOOL found = FALSE;
			for (Tag *familyTag in tagFamily) {
				if (familyTag.tagID == tag.tagID) {
					found = TRUE;
				}
			}
			
			if (found == FALSE) {
				[self.tags addObject:tag];
			}
		}
		
		// Need to sort the array
		[self sortTags];
	}
	return self;
}

#pragma mark -
#pragma mark Reload Tags Methods

- (void)reloadAllTags {
	[self.tags removeAllObjects];
	
	// Get the array of all tags in the database
	NSArray *localArray = [BLTag getAllTags];
	
	// Create a new object for each tag taken from the database
	for (NSDictionary *tagDictionary in localArray) {
		NSInteger theTagID = [DMHelper getNSIntegerValueForKey:@"TagID" fromDictionary:tagDictionary];
		NSString *theName = [DMHelper getNSStringValueForKey:@"Name" fromDictionary:tagDictionary];
		NSInteger theParentTagID = [DMHelper getNSIntegerValueForKey:@"ParentTagID" fromDictionary:tagDictionary];
		NSInteger theTagOrder = [DMHelper getNSIntegerValueForKey:@"TagOrder" fromDictionary:tagDictionary];
		NSInteger theTagDepth = [DMHelper getNSIntegerValueForKey:@"TagDepth" fromDictionary:tagDictionary];
        NSString *theTagColour = [DMHelper getNSStringValueForKey:@"Colour" fromDictionary:tagDictionary];
		
		Tag *tag = [[Tag alloc] initWithTagID:theTagID andName:theName
								andParentTagID:theParentTagID andTagOrder:theTagOrder
								   andTagDepth:theTagDepth
                                  andTagColour:theTagColour];// autorelease];
		
		[self.tags addObject:tag];
	}
	
	// Need to sort the array
	[self sortTags];
}

- (void)reloadAllTagsForParentTagID:(NSInteger)parentTagID {
	[self.tags removeAllObjects];
	
	// Get the array of all tags in the database
	NSArray *localArray = [BLTag getAllTagsWithParentTagID:parentTagID];
	
	// Create a new object for each tag taken from the database
	for (NSDictionary *tagDictionary in localArray) {
		NSInteger theTagID = [DMHelper getNSIntegerValueForKey:@"TagID" fromDictionary:tagDictionary];
		NSString *theName = [DMHelper getNSStringValueForKey:@"Name" fromDictionary:tagDictionary];
		NSInteger theParentTagID = [DMHelper getNSIntegerValueForKey:@"ParentTagID" fromDictionary:tagDictionary];
		NSInteger theTagOrder = [DMHelper getNSIntegerValueForKey:@"TagOrder" fromDictionary:tagDictionary];
		NSInteger theTagDepth = [DMHelper getNSIntegerValueForKey:@"TagDepth" fromDictionary:tagDictionary];
        NSString *theTagColour = [DMHelper getNSStringValueForKey:@"Colour" fromDictionary:tagDictionary];
		
		Tag *tag = [[Tag alloc] initWithTagID:theTagID andName:theName
								andParentTagID:theParentTagID andTagOrder:theTagOrder
								   andTagDepth:theTagDepth
                                  andTagColour:theTagColour];// autorelease];
		
		[self.tags addObject:tag];
	}
}

#pragma mark -
#pragma mark Sort Methods


// USE A DEPTH FIELD IN SQL TABLE
- (void)sortTags {
	
	// Depth is sorted
	
	// Need to get startIndex
	NSInteger startIndex = [self getStartIndex];
	
	// If startIndex is -1, it means table is sorted
	if (startIndex == -1) {
		return;
	}
	
	for (int i = startIndex; i < [self.tags count]; i++) {
		// Find each depth, no need, as it can be slotted in behind parent
		Tag *insertTag = [self.tags objectAtIndex:i];
		
		// From the starting index, we need to get the location of the parent tag ID
		NSInteger insertIndex = [self getIndexOfTagWithID:insertTag.parentTagID];
		
		// Need to check that there already isnt child tags
		insertIndex = [self getUpdatedChildTagsInsertIndex:insertIndex withFocusTagID:insertTag.tagID];

		Tag *copyTag = [insertTag copy];
		[self.tags removeObjectAtIndex:i];
		[self.tags insertObject:copyTag atIndex:insertIndex];
//		[copyTag release];
		
		// Then need to insert the object at the index
		// Each object needs to be inserted
	}
	
	// Get the largest
	
	/*for (int i = 0; i < [self.tags count] - 1; i++) {
		for (int j = 0; j < [self.tags count] - 1 - i; j++) {
			Tag *tag1 = [self.tags objectAtIndex:j];
			Tag *tag2 = [self.tags objectAtIndex:j + 1];
			
			if () {
				<#statements#>
			}
		}
	}*/
	
	/*NSInteger index = 0;
	
	// Get the updated index
	for (int i = 0; i < [self.tags count]; i++) {
		if ([[self.tags objectAtIndex:i] parentTagID] != -1) {
			index = i;
		}
	}
	
	if (index == 0) {
		return;
	}
	
	// Index is now set to the object that needs to be sorted
	// Need to get parent tag id
	NSInteger parentTagID = [[self.tags objectAtIndex:index] parentTagID];
	
	NSInteger foundIndex = [self getIndexOfTagWithID:parentTagID];
	
	*/
	
	
	//[self.tags replaceObjectAtIndex:<#(NSUInteger)index#> withObject:<#(id)anObject#>]
	
	
}

- (NSInteger)getStartIndex {
	for (int i = 0; i < [self.tags count]; i++) {
		if ([[self.tags objectAtIndex:i] tagDepth] != 0) {
			return i;
		}
	}
	
	return -1;
}

// Updated insert index for when there are already child tags.  Also need to ensure the
// focus Tag ID is presented so that the tag that needs to be swapped is not factored
- (NSInteger)getUpdatedChildTagsInsertIndex:(NSInteger)index withFocusTagID:(NSInteger)focusTagID {
	Tag *parentTag = [self.tags objectAtIndex:index];
	
	// Add 1 to index (need this regardless)
	if (index + 1 >= [self.tags count]) {
		return index;
	}
	
	index++;
	
	Tag *childTag = [self.tags objectAtIndex:index];
	
	while (childTag.parentTagID == parentTag.tagID && childTag.tagID != focusTagID) {
		if (index + 1 >= [self.tags count]) {
			return index;
		}
		index++;
		childTag = [self.tags objectAtIndex:index];
	}
	
	return index;
}


- (NSInteger)getFirstCaseOfChildTag {
	return 0;
}



#pragma mark -
#pragma mark Get Methods

- (NSString *)getNameForTagWithID:(NSInteger)theTagID {
	for (Tag *tag in self.tags) {
		if (tag.tagID == theTagID) {
			return tag.name;
		}
	}
	
	return @"";
}


- (NSInteger)getIndexOfTagWithID:(NSInteger)theTagID {
	NSInteger index = 0;
	for (Tag *tag in self.tags) {
		if (tag.tagID == theTagID) {
			return index;
		}
		index++;
	}
	
	// Not found
	return 0;
}

- (Tag *)getTagWithID:(NSInteger)theTagID {
	for (Tag *tag in self.tags) {
		if (tag.tagID == theTagID) {
			return tag;
		}
	}
	
	// Not found
	return nil;
}

// This is just tag depth now, no need to call
- (NSInteger)getIndentLevelOfTagWithID:(NSInteger)theTagID {
	// Need a form of recursion
	NSInteger index = [self getIndexOfTagWithID:theTagID];
	
	Tag *tag = [self.tags objectAtIndex:index];
	
	NSInteger indentLevel = 0;
	
	while (tag.parentTagID != -1) {
		
		index = [self getIndexOfTagWithID:tag.parentTagID];
		tag = [self.tags objectAtIndex:index];
		indentLevel++;
	}
	
	return indentLevel;
}

// Gets the new tag ID for us to use
- (NSInteger)getNewTagID {
	return [BLTag getNewTagID];
}

// Goes through the database and gets the next 'order' number.  Use -1 for top level tasks
- (NSInteger)getNextTagOrderForParentTagID:(NSInteger)parentTagID {
	return [BLTag getNextTagOrderForParentTagID:parentTagID];
}

- (NSInteger)getNumberOfChildTagsForParentTagID:(NSInteger)parentTagID {
	NSInteger numberOfChildTags = 0;
	
	for (Tag *tag in self.tags) {
		if (tag.parentTagID == parentTagID) {
			numberOfChildTags++;
			
			// Check whether this has any child tags
			numberOfChildTags = numberOfChildTags + [self getNumberOfChildTagsForParentTagID:tag.tagID];
		}
	}
	
	return numberOfChildTags;
}

- (NSMutableArray *)getArrayOfTagsToDeleteForParentTag:(Tag *)parentTag {
	NSMutableArray *tagsToDeleteArray = [[NSMutableArray alloc] init];// autorelease];
	
	[tagsToDeleteArray addObject:parentTag];
	
	for (Tag *tag in self.tags) {
		if (tag.parentTagID == parentTag.tagID) {
			[tagsToDeleteArray addObjectsFromArray:[self getArrayOfTagsToDeleteForParentTag:tag]];
		}
	}
	
	return tagsToDeleteArray;
}

- (NSMutableArray *)getFamilyOfTagsCopyForParentTag:(Tag *)parentTag {
	NSMutableArray *familyTagsArray = [[NSMutableArray alloc] init];// autorelease];
	
	[familyTagsArray addObject:parentTag];
	
	for (Tag *tag in self.tags) {
		if (tag.parentTagID == parentTag.tagID) {
			NSMutableArray *familyOfTagsCopy = [self getFamilyOfTagsCopyForParentTag:tag];// retain];
			[familyTagsArray addObjectsFromArray:familyOfTagsCopy];
//			[familyOfTagsCopy release];
		}
	}
	
	return familyTagsArray;
}

- (NSInteger)getTagIDOfTagWithName:(NSString *)tagName {
	for (Tag *tag in self.tags) {
		if ([[tag name] isEqualToString:tagName]) {
			return [tag tagID];
		}
	}
	
	return -1;
}

#pragma mark -
#pragma mark Delete Methods

- (void)deleteTagAtIndex:(NSInteger)index {
	Tag *tag = [self.tags objectAtIndex:index];
	
	// Delete from Tag table in database
	[BLTag deleteTagWithID:tag.tagID];
	
	// Delete all list item tags associated
	[BLListItemTag deleteAllListItemTagsWithTagID:tag.tagID];
	
	// Just delete the tag
	[self.tags removeObjectAtIndex:index];
}

#pragma mark -
#pragma mark Update Methods

- (void)updateChildrenTagDepthForParentTag:(Tag *)parentTag {
	for (Tag *tag in self.tags) {
		if (tag.parentTagID == parentTag.tagID) {

			tag.tagDepth = parentTag.tagDepth + 1;
			[tag updateSelfInDatabase];
			
			// Update any children of this tag
			[self updateChildrenTagDepthForParentTag:tag];
		}
	}
}


#pragma mark -
#pragma mark Insert Methods

- (void)insertNewTag:(Tag *)tag {
	// If parent tag id is -1, add to end of list
	if (tag.parentTagID == -1) {
		[self.tags addObject:tag];
		return;
	}
	
	// Get index of parent tag id
	NSInteger parentIndex = [self getIndexOfTagWithID:tag.parentTagID];
	
	// Check if this is the last index
	if (parentIndex == [self.tags count] - 1) {
		[self.tags addObject:tag];
		return;
	}
	
	NSInteger insertIndex = parentIndex + 1;
	for (int i = parentIndex + 1; i < [self.tags count]; i++) {
		Tag *checkTag = [self.tags objectAtIndex:i];
		
		if (checkTag.parentTagID != tag.parentTagID) {
			insertIndex = i;
			break;
		}
	}
	
	// Check if this is the last index
	if (insertIndex == [self.tags count] - 1) {
		[self.tags addObject:tag];
	} else {
		[self.tags insertObject:tag atIndex:insertIndex];
	}
}

#pragma mark -
#pragma mark Check Methods

- (BOOL)checkForAnyTagWithParentTagID:(NSInteger)theParentTagID {
	for (Tag *tag in self.tags) {
		if (tag.parentTagID == theParentTagID) {
			return TRUE;
		}
	}
	
	return FALSE;
}

#pragma mark -
#pragma mark Tag Order Methods

- (void)updateTagOrderFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
	// First tag tagOrder has already been set, so ignore it (but still update in database)
	[BLTag updateTagOrder:[[self.tags objectAtIndex:toIndex] tagOrder] 
				 forTagID:[[self.tags objectAtIndex:toIndex] tagID]];
	

	
	// Update the rest of the tags
	if (fromIndex < toIndex) {
		for (int i = fromIndex; i <= toIndex - 1; i++) {
			Tag *tag = [self.tags objectAtIndex:i];
			tag.tagOrder--;
			
			[BLTag updateTagOrder:tag.tagOrder 
						 forTagID:tag.tagID];
		}
	} else if (fromIndex > toIndex) {

		for (int i = fromIndex; i >= toIndex + 1; i--) {
			Tag *tag = [self.tags objectAtIndex:i];

			tag.tagOrder++;
			
			[BLTag updateTagOrder:tag.tagOrder 
						 forTagID:tag.tagID];
			
		}
	}
}

/*- (id)initWithAllTagsWithListItemID:(NSInteger)theListItemID {
 if (self = [super init]) {
 self.listItemTags = [[NSMutableArray alloc] init];
 self.tags = [[NSMutableArray alloc] init];
 
 NSArray *localTagTypesArray = [BLTag getAllTags];
 
 for (NSDictionary *tagDictionary in localArray) {
 NSInteger theTagID = [DMHelper getNSIntegerValueForKey:@"TagID" fromDictionary:tagDictionary];
 NSString *theName = [DMHelper getNSStringValueForKey:@"Name" fromDictionary:tagDictionary];
 
 Tag *tagType = [[[Tag alloc] initWithTagID:theTagID andName:theName] autorelease];
 
 [self.tags addObject:tagType];
 }
 
 
 NSArray *localTagsArray = [BLTag getListItemTagsWithListItemID:theListItemID];
 
 
 }
 return self;
 }
 */

/*
 - (id)initWithTagsForID:(NSInteger)theListItemID {
 if (self = [super init]) {
 self.tags = [[NSMutableArray alloc] init];
 
 NSArray *localTagsArray = [BLTag getAllTags];
 
 for (NSDictionary *tagDictionary in localTagsArray) {
 NSInteger theTagID = [DMHelper getNSIntegerValueForKey:@"TagID" fromDictionary:tagDictionary];
 NSString *theName = [DMHelper getNSStringValueForKey:@"Name" fromDictionary:tagDictionary];
 
 Tag *tag = [[[Tag alloc] initWithTagID:theTagID andName:theName] autorelease];
 
 [self.tags addObject:tag];
 }
 }
 return self;
 }*/

@end
