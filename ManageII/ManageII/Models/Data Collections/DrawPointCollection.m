//
//  DrawPointCollection.m
//  Manage
//
//  Created by Cliff Viegas on 23/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "DrawPointCollection.h"

// Data Models
#import "DrawPoint.h"


@implementation DrawPointCollection


@synthesize drawPoints;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[self.drawPoints release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		// Init the points array
		DrawPoint *drawPoint1 = [[DrawPoint alloc] init];
		DrawPoint *drawPoint2 = [[DrawPoint alloc] init];
		DrawPoint *drawPoint3 = [[DrawPoint alloc] init];
		DrawPoint *drawPoint4 = [[DrawPoint alloc] init];
		
		self.drawPoints = [NSArray arrayWithObjects:drawPoint1, drawPoint2, drawPoint3, drawPoint4, nil]; //drawPoint4, nil];
//		[drawPoint1 release];
//		[drawPoint2 release];
//		[drawPoint3 release];
//		[drawPoint4 release];
	}
	return self;
}

#pragma mark -
#pragma mark Class Helper Methods

- (void)rotatePointsWithNewPoint:(CGPoint)newPoint {
	DrawPoint *drawPoint1 = [self.drawPoints objectAtIndex:0];
	DrawPoint *drawPoint2 = [self.drawPoints objectAtIndex:1];
	DrawPoint *drawPoint3 = [self.drawPoints objectAtIndex:2];
	DrawPoint *drawPoint4 = [self.drawPoints objectAtIndex:3];
	
	drawPoint1.x = drawPoint2.x;
	drawPoint1.y = drawPoint2.y;
	drawPoint2.x = drawPoint3.x;
	drawPoint2.y = drawPoint3.y;
	drawPoint3.x = drawPoint4.x;
	drawPoint3.y = drawPoint4.y;
	drawPoint4.x = newPoint.x;
	drawPoint4.y = newPoint.y;
	
	/*drawPoint1.x = drawPoint4.x;
	drawPoint1.y = drawPoint4.y;
	drawPoint2.x = newPoint.x;
	drawPoint2.y = newPoint.y;
	drawPoint3.x = kNoPoint;
	drawPoint3.y = kNoPoint;
	drawPoint4.x = kNoPoint;
	drawPoint4.y = kNoPoint;*/
}

- (void)addPoint:(CGPoint)newPoint {
	for (DrawPoint *aDrawPoint in self.drawPoints) {
		if (aDrawPoint.x == kNoPoint && aDrawPoint.y == kNoPoint) {
			aDrawPoint.x = newPoint.x;
			aDrawPoint.y = newPoint.y;
			return;
		}
	}
}

- (BOOL)isPointsFull {
	BOOL pointsAreFull = TRUE;
	for (DrawPoint *aDrawPoint in self.drawPoints) {
		if (aDrawPoint.x == kNoPoint && aDrawPoint.y == kNoPoint) {
			pointsAreFull = FALSE;
		}
	}
	return pointsAreFull;
}

- (void)updateDrawPointAtIndex:(NSInteger)index withNewPoint:(CGPoint)newPoint {
	if (index < [self.drawPoints count]) {
		DrawPoint *selectedDrawPoint = [self.drawPoints objectAtIndex:index];
		selectedDrawPoint.x = newPoint.x;
		selectedDrawPoint.y = newPoint.y;
	}
	
}

- (void)clearDrawPoints {
	for (DrawPoint *aDrawPoint in self.drawPoints) {
		aDrawPoint.x = kNoPoint;
		aDrawPoint.y = kNoPoint;
	}
}

- (void)resetDrawPoints {
	DrawPoint *lastDrawPoint = [self.drawPoints objectAtIndex:3];
	CGPoint lastPoint = CGPointMake(lastDrawPoint.x, lastDrawPoint.y);
	
	for (DrawPoint *aDrawPoint in self.drawPoints) {
		aDrawPoint.x = kNoPoint;
		aDrawPoint.y = kNoPoint;
	}
	
	DrawPoint *firstDrawPoint = [self.drawPoints objectAtIndex:0];
	firstDrawPoint.x = lastPoint.x;
	firstDrawPoint.y = lastPoint.y;
}

@end
