//
//  AlarmCollection.m
//  Manage
//
//  Created by Cliff Viegas on 15/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "AlarmCollection.h"

// Data Models
#import "Alarm.h"

// Method Helper
#import "MethodHelper.h"

// Business Layer
#import "BLAlarm.h"

// Dictionary Methods helper
#import "DMHelper.h"

@implementation AlarmCollection

@synthesize alarms;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	//[self.alarms release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		// Init alarms
		self.alarms = [[NSMutableArray alloc] init];//autorelease
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

// This method will only load valid alarms (in future)
- (void)loadAlarmsWithListItemID:(NSInteger)listItemID {
	// Check to see if there are any alarms, if so remove them
	if ([self.alarms count] > 0) {
		[self.alarms removeAllObjects];
	}
	
	// Get a local array of all alarms with the list item id
	NSArray *localArray = [BLAlarm getAllAlarmsWithListItemID:listItemID];
	
	for (NSDictionary *alarmDictionary in localArray) {
		Alarm *alarm = [[Alarm alloc] init];
		alarm.listItemID = [DMHelper getNSIntegerValueForKey:@"ListItemID" fromDictionary:alarmDictionary];
		alarm.alarmGUID = [DMHelper getNSStringValueForKey:@"AlarmGUID" fromDictionary:alarmDictionary];
		alarm.fireDateTime = [DMHelper getNSStringValueForKey:@"FireDateTime" fromDictionary:alarmDictionary];
		alarm.delay = [DMHelper getNSIntegerValueForKey:@"Delay" fromDictionary:alarmDictionary];
		alarm.measurement = [DMHelper getNSStringValueForKey:@"Measurement" fromDictionary:alarmDictionary];
		
		// Add the object to our alarms array (if the firedate is not earlier than now)
		if ([MethodHelper isDateInPast:[MethodHelper dateFromString:alarm.fireDateTime usingFormat:K_DATETIME_FORMAT]] == FALSE) {
			[self.alarms addObject:alarm];
		} else {
			// Also need to remove any matching notification in queue
			[alarm removeFromNotificationQueue];
			
			// Need to remove this record from DB
			[BLAlarm deleteAlarmWithGUID:[alarm alarmGUID]];
		}
		
//		[alarm release];
	}
}

#pragma mark -
#pragma mark Get Methods

- (NSInteger)getIndexOfAlarmWithAlarmGUID:(NSString *)theAlarmGUID {
	for (int i = 0; i <= [self.alarms count]; i++) {
		Alarm *alarm = [self.alarms objectAtIndex:i];
		if ([alarm.alarmGUID isEqualToString:theAlarmGUID]) {
			return i;
		}
	}
	
	// Not found
	return -1;
} 

// Helper method used at application launch to check whether 'hasAlarms' field of listItem object
// needs to be updated due to there bing no more alarms left linked to that list item.
- (NSInteger)getNumberOfAlarmsWithListItemID:(NSInteger)listItemID {
	return [BLAlarm getNumberOfAlarmsWithListItemID:listItemID];
}

#pragma mark -
#pragma mark Delete Methods

- (void)deleteAlarmAtIndex:(NSInteger)alarmIndex {
	// Need to get the alarm guid as unique identifier for database deletion
	Alarm *alarm = [self.alarms objectAtIndex:alarmIndex];
	NSString *alarmGUID = alarm.alarmGUID;
	
	// Delete the alarm from the database
	[BLAlarm deleteAlarmWithGUID:alarmGUID];
	
	// Remove this alarm from the list of alarms
	[self.alarms removeObjectAtIndex:alarmIndex];
}


@end
