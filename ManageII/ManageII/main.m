//
//  main.m
//  ManageII
//
//  Created by Kerofrog on 11/21/13.
//  Copyright (c) 2013 Kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MAppDelegate class]));
    }
}
