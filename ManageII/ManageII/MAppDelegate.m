//
//  MAppDelegate.m
//  ManageII
//
//  Created by Kerofrog on 11/21/13.
//  Copyright (c) 2013 Kerofrog. All rights reserved.
//

#import "MAppDelegate.h"

// View Controllers
#import "MainViewController.h"

// Business Layer
#import "BLApplicationSetting.h"
#import "BLConfig.h"
#import "BLTag.h"

// Data Models
#import "Tag.h"
#import "TaskList.h"
#import "ApplicationSetting.h"
#import "ListItem.h"
#import "Alarm.h"
#import "UpdateDatabase.h"

// Data Collections
#import "TaskListCollection.h"
#import "AlarmCollection.h"

// Method Helper
#import "MethodHelper.h"

// Adjust constants for advertising
#import "MainConstants.h"
#import "ListConstants.h"

// Tracking app crash
#import "Crittercism.h"

// Tapstream
#import "TSTapstream.h"

#import "NotebookCollection.h"

#import "MGSplitViewController.h"

#import "TagCollection.h"

#define kUpdateDefaultChristmasTheme        @"kUpdateDefaultChristmasTheme"

NSString* const NSURLIsExcludedFromBackupKey = @"NSURLIsExcludedFromBackupKey"; //Support iOS < 5.1 to using dropbox framework
static NSString *const kTrackingId = @"UA-41514820-6"; //Google Analytics tracking ID

@implementation MAppDelegate

@synthesize navigationController;
@synthesize localNotifi;
@synthesize window;
@synthesize notebookCollection;
@synthesize splitViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Update database for old version database ( Add due time and alert time in List Item Table)
    [UpdateDatabase startUpdate];
    
    // Dropbox code
    DBAccountManager* accountMgr =
    [[DBAccountManager alloc] initWithAppKey:kAppDropboxKey secret:kAppDropboxSecret];
    [DBAccountManager setSharedManager:accountMgr];
	DBAccount *account = [[DBAccountManager sharedManager] linkedAccount];
	if (account) {
		self.dbFileSystem = [[DBFilesystem alloc] initWithAccount:account];
	}
    
	// Required before running anything
	[self databaseChecks];
	
	// Create any required resource folders
	[self createResourceFolders];
	
	// Set application will terminate delegate to self
	[[UIApplication sharedApplication] setDelegate:self];
	
	// NOTE: Important to do archive check before passing collection to main view controller
	
    // Create the window object
//    UIWindow *localWindow = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Assign the localWindow to the AppDelegate window, then release the local window
//    self.window = localWindow;
    
    
    // Override point for customization after application launch.
//    UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
//    UINavigationController *splitNavigationController = [splitViewController.viewControllers lastObject];
//    splitNavigationController.delegate = (id)splitNavigationController.topViewController;
    
    [self runStartupCode];
    
    // Load data for list view
    self.splitViewController = (MGSplitViewController *)self.window.rootViewController;
    
    ControlPanelViewController *controlPanelVC = [[ControlPanelViewController alloc] initWithTaskListCollection:refTaskListCollection notebookCollection:notebookCollection];
    _rootControlPanel = controlPanelVC;
    
//<<<<<<< HEAD
//    // reload hide competed task
//    [controlPanelVC reloadTaskListForHideCompletedTask];
//    
//    _listViewController = [[ListViewController alloc] initWithTaskList:refTaskList andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection andBackButtonTitle:nil];
//=======
    if ([refTaskListCollection.lists count] == 0) {
        _listViewController = nil;
        _noteViewController = nil;
    } else {
        
        _listViewController = [[ListViewController alloc] initWithTaskList:refTaskList andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection andBackButtonTitle:nil];
//>>>>>>> manage_2_nhan
    
        _noteViewController = [[NoteViewController alloc] initWithMasterTaskListCollection:refMasterTaskListCollection andTaskList:refTaskList andBackButtonTitle:nil];
    }
    
    controlPanelVC.listViewController = _listViewController;
    controlPanelVC.noteViewController = _noteViewController;
    
    UINavigationController *rootNav  = [[UINavigationController alloc]initWithRootViewController:controlPanelVC];
    self.splitViewController.masterViewController = rootNav;

    if (refTaskList.isNote == YES) {
        // Navigation to note
        UINavigationController *detailNoteNav = [[UINavigationController alloc]initWithRootViewController:_noteViewController];
        self.splitViewController.detailViewController = detailNoteNav;
        
    }else{
        // Navigation to list task
        UINavigationController *detailNav = [[UINavigationController alloc]initWithRootViewController:_listViewController];
        self.splitViewController.detailViewController = detailNav;

    }

    
//    UINavigationController *detailNav = (UINavigationController*)_splitViewController.detailViewController;
//    ListViewController *listController = (ListViewController *)detailNav.topViewController;
//    [listController loadListViewWithTaskList:refTaskList andTagCollection:nil andTaskListCollection:masterTaskListCollection andMasterTaskListCollection:masterTaskListCollection andBackButtonTitle:nil];
//    
//    // Load data for control panel view
//    UINavigationController *rootNav = (UINavigationController*)_splitViewController.masterViewController;
////    MainViewController *mainController = (MainViewController *)rootNav.topViewController;
////    [mainController runStartupCode];
//    ControlPanelViewController *controlPanelController = (ControlPanelViewController *)rootNav.topViewController;
//    [controlPanelController loadControlPanelWithTaskListCollection:masterTaskListCollection notebookCollection:notebookCollection];
    
    if (YES) { // whether to allow dragging the divider to move the split.
		self.splitViewController.splitWidth = 1.0; // make it wide enough to actually drag!
		self.splitViewController.allowsDraggingDivider = NO;
        self.splitViewController.showsMasterInLandscape = YES;
        self.splitViewController.showsMasterInPortrait = YES;
        //[self.splitViewController setDividerStyle:MGSplitViewDividerStylePaneSplitter animated:YES];
	}
    
    [window makeKeyAndVisible];
    
    // Add setting button
    settingsButton = [[UIButton alloc] init];
    
    if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])){
        [settingsButton setFrame:kSettingPortrait];
    }else{
        [settingsButton setFrame:kSettingLandscape];
    }
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        CGRect f = settingsButton.frame;
        f.origin.y -= 20;
        settingsButton.frame = f;
    }
    
    [settingsButton setBackgroundImage:[UIImage imageNamed:@"SettingsIcon.png"] forState:UIControlStateNormal];
    settingsButton.showsTouchWhenHighlighted = YES;
    
    // Add action for button
    [settingsButton addTarget:self action:@selector(settingsBarButtonItemAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.splitViewController.detailViewController.view addSubview:settingsButton];//self.splitViewController.detailViewController.view
    
    if (launchOptions != nil) {
        // Launched from local notification
        self.localNotifi = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    }
    
    return YES;
}


- (void)settingsBarButtonItemAction{
    if ([settingsPopoverController isPopoverVisible]) {
		// Do nothing
		[settingsPopoverController dismissPopoverAnimated:YES];
        settingsPopoverController = nil;
        //        [settingsPopoverController release];
		return;
	}
    
    if (settingsPopoverController != nil) {
        settingsPopoverController = nil;
        //        [settingsPopoverController release];
    }
	
	// Init the select view type controller4////////EnumPropertyCollectionApplicationList
	SettingsViewController *settingsViewController = [[SettingsViewController alloc] initWithDelegate:self andTagCollection:tagCollection
																			andPropertyCollectionType:EnumPropertyCollectionApplication
																				andTaskListCollection:refMasterTaskListCollection];
	
	// Init the navigation controller that will hold the edit list item view controller
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
    //	[settingsViewController release];
	
	// Allocate the pop over controller, that will host the nav controller and view controller
	settingsPopoverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
	[settingsPopoverController setDelegate:self];
    //	[navController release];
	
	// Present the popover using the calculated location to display the arrow
    if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])){
        
        [settingsPopoverController presentPopoverFromRect:kSettingPortrait inView:self.splitViewController.detailViewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }else{
        [settingsPopoverController presentPopoverFromRect:kSettingLandscape inView:self.splitViewController.detailViewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
	DBAccount *account = [[DBAccountManager sharedManager] handleOpenURL:url];
	if (account) {
        self.dbFileSystem = [DBFilesystem sharedFilesystem];
	} else {
        if ([self.appDelegate respondsToSelector:@selector(didLoginDropBoxWithAccount:)]) {
            [self.appDelegate didLoginDropBoxWithAccount:account];
        }
    }
	
	return YES;
}

#pragma mark - Handle local notification

// Handling a local notification when an application is already running
- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)localNotification {
    
    // Call handle local notification function
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
        // Active
        [self handleLocalNotification:localNotification andStatusActive:YES];
    }else{
        // Inactive
        [self handleLocalNotification:localNotification andStatusActive:NO];
    }
}

- (void)handleLocalNotification:(UILocalNotification *)localNotification andStatusActive:(BOOL)status{
    // Get local notification
    self.localNotifi = localNotification;
    
	if (status == YES) {
		// The app was already running
		
		// Get the correct alarm (no need, just need to remove from DB and)
		// local notification queue (which is already done).
		
		
		// Create alarm collection and count how many alarms for this listitemID
		
		// Update 'hasAlarms' setting if there are no more alarms left for list item
		// Can't update this setting, just remove the alarm
        
		// Just move the alarm, can only update 'hasAlarms' when opening the item
		// Just delete the alarm from the db
        
		// Oops, also need to check any currently open collections to update listitem
		// Solution is only update it on startup... this can be handled when opened.
		
		//AlarmCollection *alarmCollection = [[AlarmCollection alloc]
		
		// Need to create an alarm collection using the listitemID from this alarm
		
		// Need to call some business layer functions.  Can't be helped.
		// First get the alarm
        
        UIAlertView *alarmAlert = [[UIAlertView alloc] initWithTitle:@"Reminder" message:localNotification.alertBody delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"View", nil];
        alarmAlert.delegate = self;
        [alarmAlert show];
		
		
	} else {
        
        
		// The user tapped the 'action' button while the app was not running
		// Note: Look at not having an 'action' button and just use an ok button
		// Do nothing as alarm was already shown.  Cleanup can be done elsewhere... ack.  Just view it.
        
        // Get Alarm ID
        NSString *alarmItemID = [self.localNotifi.userInfo objectForKey:@"AlarmItemID"];
        
        // Save List Item ID
        [[NSUserDefaults standardUserDefaults] setInteger:[alarmItemID integerValue] forKey:kAlarmListItem];
        // Save index of Task List
        NSInteger indexOfTaskList = [refMasterTaskListCollection getIndexOfTaskListWithListItemID:[alarmItemID integerValue]];
        [[NSUserDefaults standardUserDefaults] setInteger:indexOfTaskList forKey:kAlarmTaskList];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAlarmViewListItem object:self userInfo:nil];
        
        self.localNotifi = nil;
	}
    
	// Remove the alarm from the DB, this 'should' be ok, but best to do testing anyways
	NSString *alarmGUID = [localNotification.userInfo objectForKey:@"AlarmGUID"];
	
    // Get alarm
	Alarm *alarm = [[Alarm alloc] initWithAlarmGUID:alarmGUID];
	
	// Delete the alarm from database
	[alarm deleteSelfFromDatabase];
	
    // [viewController displayItem:itemName];  // custom method
    // application.applicationIconBadgeNumber = notification.applicationIconBadgeNumber-1;
}

#pragma mark - UIAlertView Delegate

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    // When state of application is active and user press view button
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    // Get Alarm ID
    NSString *alarmItemID = [self.localNotifi.userInfo objectForKey:@"AlarmItemID"];
    
    // Save List Item ID
    [[NSUserDefaults standardUserDefaults] setInteger:[alarmItemID integerValue] forKey:kAlarmListItem];
    
    // Save index of Task List
    NSInteger indexOfTaskList = [refMasterTaskListCollection getIndexOfTaskListWithListItemID:[alarmItemID integerValue]];
    [[NSUserDefaults standardUserDefaults] setInteger:indexOfTaskList forKey:kAlarmTaskList];
    
    // Implement Snooze function
    if([title isEqualToString:@"View"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAlarmViewListItem object:self userInfo:nil];
    }
    
    self.localNotifi = nil;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    // Get number of list items with due date
	gNumberOfListItemsWithDueDate = [MethodHelper numberOfListItemsForBadgeNotification];
	
	// Update database if needed
	if ([refMasterTaskListCollection sortOrderUpdated] == TRUE) {
		[refMasterTaskListCollection updateListOrdersInDatabase];
	}
	
	// Add badge number to icon
	[[UIApplication sharedApplication] setApplicationIconBadgeNumber:gNumberOfListItemsWithDueDate];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    // Handling local notification
    if (self.localNotifi) {
        // Call function to handle local notification
        [self handleLocalNotification:self.localNotifi andStatusActive:NO];
    }
    
    [self archiveCheck];
    
    // Check for auto sync
    gRunToodledoAutoSync = FALSE;
    BOOL currentAutoSyncSetting = [[ApplicationSetting getSettingDataForName:@"ToodledoAutoSync"] boolValue];
    
    if (currentAutoSyncSetting == TRUE) {
        gRunToodledoAutoSync = TRUE;
    }
    
    // Fix bugs view is overlap by status bar when user open ads info screen and exit app after that reopen again
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.0") && SYSTEM_VERSION_LESS_THAN(@"6.0")) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Get number of list items with due date
	gNumberOfListItemsWithDueDate = [MethodHelper numberOfListItemsForBadgeNotification];
	
	// Update database if needed
	if ([refMasterTaskListCollection sortOrderUpdated] == TRUE) {
		[refMasterTaskListCollection updateListOrdersInDatabase];
	}
    
	// Add badge number to icon
	[[UIApplication sharedApplication] setApplicationIconBadgeNumber:gNumberOfListItemsWithDueDate];
}


- (void) application:(UIApplication *)application
willChangeStatusBarOrientation:(UIInterfaceOrientation)newStatusBarOrientation
            duration:(NSTimeInterval)duration
{
    if ([settingsPopoverController isPopoverVisible]) {
		// Do nothing
		[settingsPopoverController dismissPopoverAnimated:YES];
        settingsPopoverController = nil;
	}
    
    if (newStatusBarOrientation == UIInterfaceOrientationPortrait || newStatusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        [settingsButton setFrame:kSettingPortrait];
    }
    else
    {
        [settingsButton setFrame:kSettingLandscape];
    }
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        CGRect f = settingsButton.frame;
        f.origin.y -= 20;
        settingsButton.frame = f;
    }
}


#pragma mark -
#pragma mark Class Methods

// Create scribble and other folders at run-time
- (void)createResourceFolders {
	NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *documentsPath;
	
	// Add scribble folder to documents path
	documentsPath = [searchPaths objectAtIndex: 0];
	documentsPath = [NSString stringWithFormat:@"%@/Scribble/", documentsPath];
	[fileManager createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:NULL];
	
	// Add backup folder to documents path
	documentsPath = [searchPaths objectAtIndex: 0];
	documentsPath = [NSString stringWithFormat:@"%@/Backup/", documentsPath];
	[fileManager createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:NULL];
    
	// Add log folder to documents path
	documentsPath = [searchPaths objectAtIndex: 0];
	documentsPath = [NSString stringWithFormat:@"%@/Log/", documentsPath];
	[fileManager createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:NULL];
}

#pragma mark -
#pragma mark Custom Methods

- (void)archiveCheck {
	NSString *archiveItems = [ApplicationSetting getSettingDataForName:@"ArchiveItems"];
    
	if ([archiveItems isEqualToString:@"Manually"]) {
		// Do nothing
		return;
	}
	
	if ([archiveItems isEqualToString:@"On Application Launch"]) {
		// Archive now, do listitems first
		for (TaskList *taskList in refMasterTaskListCollection.lists) {
			[taskList archiveAllCompletedItems:FALSE];
		}
		
		[refMasterTaskListCollection archiveAllCompletedListsToArchiveList:nil];
        
        // put a flag to update database after archiving Task List on Application launch
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kNeedUpdateDatebaseForArchiveTaskList];
        
	} else if ([archiveItems isEqualToString:@"Daily"]) {
		// Archive now, do listitems first
		for (TaskList *taskList in refMasterTaskListCollection.lists) {
			[taskList archiveAllCompletedItemsUsingDuration:1];
		}
		
		[refMasterTaskListCollection archiveAllCompletedListsUsingDuration:1];
	} else if ([archiveItems isEqualToString:@"Weekly"]) {
		// Archive now, do listitems first
		for (TaskList *taskList in refMasterTaskListCollection.lists) {
			[taskList archiveAllCompletedItemsUsingDuration:7];
		}
		
		[refMasterTaskListCollection archiveAllCompletedListsUsingDuration:7];
	}
}

// Just used for updating of database schema if required
- (void)databaseChecks {
	// First get version
	NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
	
    // Alway check database before run app
    //	if ([[BLApplicationSetting getVersionNumber] isEqualToString:version]) {
    //		return;
    //	}
	
	// Look for application setting table, add if it does not exist
	if ([BLConfig checkIfTableExists:@"ApplicationSetting"] == FALSE) {
		//[BLConfig createTable:@"ApplicationSetting" withPrimaryKey:@"SettingID" ofType:@"INTEGER"];
		//[BLConfig addField:@"Name" toTable:@"ApplicationSetting" withType:@"VARCHAR"];
		//[BLConfig addField:@"Data" toTable:@"ApplicationSetting" withType:@"VARCHAR"];
		[BLConfig createApplicationSettingTable];
	}
	
	// Check if settings need to be updated
	[self updateSettings:version];
	
	// Look for the ListItemTag table
	if ([BLConfig checkIfTableExists:@"ListItemTag"] == FALSE) {
		// Create it as it doesnt exist
		[BLConfig createTable:@"ListItemTag" withPrimaryKey:@"ListItemID" andSecondaryKey:@"TagID" ofType:@"INTEGER"];
	}
	
	// Look for the alarm table for version 1.5, create it if it does not exist
	if ([BLConfig checkIfTableExists:@"Alarm"] == FALSE) {
		[BLConfig createAlarmTable];
	}
	
	// Look for the Tag table, add if it does not exist
	if ([BLConfig checkIfTableExists:@"Tag"] == FALSE) {
		[BLConfig createTable:@"Tag" withPrimaryKey:@"TagID" ofType:@"INTEGER"];
		[BLConfig addField:@"Name" toTable:@"Tag" withType:@"VARCHAR"];
		[BLConfig addField:@"TagOrder" toTable:@"Tag" withType:@"INTEGER"];
		[BLConfig addField:@"ParentTagID" toTable:@"Tag" withType:@"INTEGER"];
		[BLConfig addField:@"TagDepth" toTable:@"Tag" withType:@"INTEGER"];
        [BLConfig addField:@"Colour" toTable:@"Tag" withType:@"VARCHAR"];
		
		// Create records
		Tag *homeTag = [[Tag alloc] initWithTagID:1 andName:@"Home" andParentTagID:-1 andTagOrder:0 andTagDepth:0 andTagColour:NSLocalizedString(@"TAG_COLOR_BROWN", nil)];
		[homeTag insertSelfIntoDatabase];
		
		Tag *workTag = [[Tag alloc] initWithTagID:2 andName:@"Work" andParentTagID:-1 andTagOrder:1 andTagDepth:0 andTagColour:NSLocalizedString(@"TAG_COLOR_BROWN", nil)];
		[workTag insertSelfIntoDatabase];
	}
	
    // For 1.6, look for the notebook table
    if ([BLConfig checkIfTableExists:@"Notebook"] == FALSE) {
        [BLConfig createTable:@"Notebook" withPrimaryKey:@"NotebookID" ofType:@"INTEGER"];
        [BLConfig addField:@"Title" toTable:@"Notebook" withType:@"VARCHAR"];
        [BLConfig addField:@"CreationDate" toTable:@"Notebook" withType:@"VARCHAR"];
        [BLConfig addField:@"ItemOrder" toTable:@"Notebook" withType:@"INTEGER"];
        [BLConfig addField:@"Lists" toTable:@"Notebook" withType:@"VARCHAR"];
    }
    
    // For 1.5.2, look for tag colour integer in Tag table
    if ([BLConfig checkIfColumn:@"Colour" existsInTable:@"Tag"] == FALSE) {
        // Add the tag colour field
        [BLConfig addField:@"Colour" toTable:@"Tag" withType:@"VARCHAR"];
    }
    
	// Look for the completed date time in the List table
	if ([BLConfig checkIfColumn:@"CompletedDateTime" existsInTable:@"List"] == FALSE) {
		// Add the completed date time field
		[BLConfig addField:@"CompletedDateTime" toTable:@"List" withType:@"VARCHAR"];
	}
	
	// Add a completed field with a default of 0
	if ([BLConfig checkIfColumn:@"Completed" existsInTable:@"List"] == FALSE) {
		// Add the completed field
		[BLConfig addField:@"Completed" toTable:@"List" withType:@"INTEGER"
			   andNullable:NO andDefault:@"0"];
	}
	
	// Look to see if ModifiedDateTime field exists, if not, add it to ListItem table
	if ([BLConfig checkIfColumn:@"ModifiedDateTime" existsInTable:@"ListItem"] == FALSE) {
		// Add 'ModifiedDateTime' field
		[BLConfig addField:@"ModifiedDateTime" toTable:@"ListItem" withType:@"VARCHAR"];
	}
	
	// Add an archived field with a default of 0 to ListItem table
	if ([BLConfig checkIfColumn:@"Archived" existsInTable:@"ListItem"] == FALSE) {
		// Add 'Archived' field
		[BLConfig addField:@"Archived" toTable:@"ListItem" withType:@"INTEGER" andNullable:NO andDefault:@"0"];
	}
	
	// Add an archived date time field to ListItem table
	if ([BLConfig checkIfColumn:@"ArchivedDateTime" existsInTable:@"ListItem"] == FALSE) {
		// Add 'ArchivedDateTime' field
		[BLConfig addField:@"ArchivedDateTime" toTable:@"ListItem" withType:@"VARCHAR"];
	}
	
	// Added in 1.4.1, check if there is a 'HasAlarms' field in listitem table
	if ([BLConfig checkIfColumn:@"HasAlarms" existsInTable:@"ListItem"] == FALSE) {
		// Add 'HasAlarms' field
		[BLConfig addField:@"HasAlarms" toTable:@"ListItem" withType:@"INTEGER"];
	}
    
    // Added in 2.2, check if there is a "DueTime" field in listitem table
    if ([BLConfig checkIfColumn:@"DueTime" existsInTable:@"ListItem"] == FALSE) {
		// Add 'DueTime' field
		[BLConfig addField:@"DueTime" toTable:@"ListItem" withType:@"VARCHAR"];
	}
    
    // Added in 2.2, check if there is a "AlertTime" field in listitem table
    //    if ([BLConfig checkIfColumn:@"AlertTime" existsInTable:@"ListItem"] == FALSE) {
    //		// Add 'AlertTime' field
    //		[BLConfig addField:@"AlertTime" toTable:@"ListItem" withType:@"VARCHAR"];
    //	}
    
    // Added in 2.2, check if there is a "Sound" field in listitem table
    //    if ([BLConfig checkIfColumn:@"Sound" existsInTable:@"ListItem"] == FALSE) {
    //		// Add 'Sound' field
    //		[BLConfig addField:@"Sound" toTable:@"ListItem" withType:@"VARCHAR"];
    //	}
    
    // Added in 2.2, check if there is a "Snooze" field in listitem table
    //    if ([BLConfig checkIfColumn:@"Snooze" existsInTable:@"ListItem"] == FALSE) {
    //		// Add 'Snooze' field
    //		[BLConfig addField:@"Snooze" toTable:@"ListItem" withType:@"INTEGER"];
    //	}
    
	// Add an archived field with a default of 0 to List table
	if ([BLConfig checkIfColumn:@"Archived" existsInTable:@"List"] == FALSE) {
		// Add 'Archived' field
		[BLConfig addField:@"Archived" toTable:@"List" withType:@"INTEGER" andNullable:NO andDefault:@"0"];
	}
	
	// Add an archived date time field to List table
	if ([BLConfig checkIfColumn:@"ArchivedDateTime" existsInTable:@"List"] == FALSE) {
		// Add 'ArchivedDateTime' field
		[BLConfig addField:@"ArchivedDateTime" toTable:@"List" withType:@"VARCHAR"];
	}
	
	// Added in 1.5, Look for the ToodledoFolderID field in the list table
	if ([BLConfig checkIfColumn:@"ToodledoFolderID" existsInTable:@"List"] == FALSE) {
		// Add the 'ToodledoFolderID' field
		[BLConfig addField:@"ToodledoFolderID" toTable:@"List" withType:@"VARCHAR"];
	}
	
	if ([BLConfig checkIfColumn:@"CreationDateTime" existsInTable:@"List"] == FALSE) {
		// Create the creationDateTime field
		[BLConfig addField:@"CreationDateTime" toTable:@"List" withType:@"VARCHAR"];
	}
	
	// Look for the ListOrder field in the List table
	if ([BLConfig checkIfColumn:@"ListOrder" existsInTable:@"List"] == FALSE) {
		// Create the field
		[BLConfig addField:@"ListOrder" toTable:@"List" withType:@"INTEGER"];
		
		// Need to add list order values to the List table
		TaskListCollection *taskListCollection = [[TaskListCollection alloc] initWithAllLists];
		NSInteger i = [taskListCollection.lists	count] - 1;
		for (TaskList *list in taskListCollection.lists) {
			list.listOrder = i;
			[list updateDatabase];
			i--;
		}
	}
    
    // Added in 2.0, field for the is a note bool
    if ([BLConfig checkIfColumn:@"IsNote" existsInTable:@"List"] == FALSE) {
        // Add 'IsNote' field
        [BLConfig addField:@"IsNote" toTable:@"List" withType:@"INTEGER"];
    }
    
    // Added in 2.0, field for the location of note dictionary/drawing data (Scribble)
    if ([BLConfig checkIfColumn:@"Scribble" existsInTable:@"List"] == FALSE) {
        // Add 'Scribble' field
        [BLConfig addField:@"Scribble" toTable:@"List" withType:@"VARCHAR"];
    }
	
	// Added in 1.5, check if there is a 'ToodledoTaskID' field in listitem table
	if ([BLConfig checkIfColumn:@"ToodledoTaskID" existsInTable:@"ListItem"] == FALSE) {
		// Add 'toodledoTaskID' field
		[BLConfig addField:@"ToodledoTaskID" toTable:@"ListItem" withType:@"VARCHAR"];
	}
    
	// Look for the completedDateTime field in the list item table
	if ([BLConfig checkIfColumn:@"CompletedDateTime" existsInTable:@"ListItem"] == FALSE) {
		// Create completed date time field if not there
		[BLConfig addField:@"CompletedDateTime" toTable:@"ListItem" withType:@"VARCHAR"];
		
		// Need to go through any completed items and set the current time
		TaskListCollection *taskListCollection = [[TaskListCollection alloc] initWithAllLists];
		
		for (TaskList *list in taskListCollection.lists) {
			for (ListItem *listItem in list.listItems) {
				if ([listItem completed] == TRUE) {
					NSString *theCompletedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
					listItem.completedDateTime = theCompletedDateTime;
					[listItem updateDatabase];
				}
			}
		}
	}
	
	// Create recurring list item fields and table
	if ([BLConfig checkIfTableExists:@"RecurringListItem"] == FALSE) {
		[BLConfig createTable:@"RecurringListItem" withPrimaryKey:@"RecurringListItemID" ofType:@"INTEGER"];
		[BLConfig addField:@"Frequency" toTable:@"RecurringListItem" withType:@"INTEGER"];
		[BLConfig addField:@"FrequencyMeasurement" toTable:@"RecurringListItem" withType:@"VARCHAR"];
		[BLConfig addField:@"DaysOfTheWeek" toTable:@"RecurringListItem" withType:@"VARCHAR"];
		[BLConfig addField:@"UseCompletedDate" toTable:@"RecurringListItem" withType:@"INTEGER"];
	}
	
	// Also need to create a recurring list item ID field in 'ListItem' table
	if ([BLConfig checkIfColumn:@"RecurringListItemID" existsInTable:@"ListItem"] == FALSE) {
        [BLConfig addField:@"RecurringListItemID" toTable:@"ListItem" withType:@"INTEGER"
               andNullable:YES andDefault:@"-1"];
	}
	
	// Create DeletedListItem table
	if ([BLConfig checkIfTableExists:@"DeletedListItem"] == FALSE) {
		[BLConfig createDeletedListItemTable];
	}
	
	// Create DeletedList table
	if ([BLConfig checkIfTableExists:@"DeletedList"] == FALSE) {
		[BLConfig createDeletedListTable];
	}
    
	if ([BLConfig checkIfColumn:@"IsNote" existsInTable:@"DeletedList"] == FALSE) {
        [BLConfig addField:@"IsNote" toTable:@"DeletedList" withType:@"INTEGER"];
    }
    
	// Update the version number
	[BLApplicationSetting updateSetting:@"VersionNumber" withData:version];
	
}

// For checking what settings need to be updated
- (void)updateSettings:(NSString *)version {
	// 100 - 199 will be reserved for system settings
	// 200 - 299 will be for user settings
	if ([BLApplicationSetting checkIfSettingExists:@"VersionNumber"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"VersionNumber" andData:version];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"BadgeNotification"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"BadgeNotification" andData:@"Due and Overdue Tasks"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ShowCompleted"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ShowCompleted" andData:@"1"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
    /**
     Christmas theme
     **/
    //	if ([BLApplicationSetting checkIfSettingExists:@"SelectedTheme"] == FALSE) {
    //		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"SelectedTheme" andData:@"Christmas Wonderland"];// The first setup app will see Christmas themes (Classic)
    //		[BLApplicationSetting insertSetting:appSetting];
    //		[appSetting release];
    //
    //        // Set status isUpdate is YES
    //        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUpdateDefaultChristmasTheme];
    //
    //	}else{
    //        // Change theme to christmas wonderland
    //        BOOL isUpdateDefaultChristmas = [[NSUserDefaults standardUserDefaults] boolForKey:kUpdateDefaultChristmasTheme];
    //        if (isUpdateDefaultChristmas == NO) {
    //            [BLApplicationSetting updateSetting:@"SelectedTheme" withData:@"Christmas Wonderland"];
    //            // Set status isUpdate is YES
    //            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUpdateDefaultChristmasTheme];
    //        }
    //    }
    
    if ([BLApplicationSetting checkIfSettingExists:@"SelectedTheme"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"SelectedTheme" andData:@"Classic"];// The first setup app will see Christmas themes (Classic)
		[BLApplicationSetting insertSetting:appSetting];
	}
    
	
	if ([BLApplicationSetting checkIfSettingExists:@"ShowCompletedDuration"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ShowCompletedDuration" andData:@"One Day"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ArchiveItems"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ArchiveItems" andData:@"Daily"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"PocketReminder"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"PocketReminder" andData:@"1"];
		[BLApplicationSetting insertSetting:appSetting];
	}
    
    // Setting hide completed tasks
    if ([BLApplicationSetting checkIfSettingExists:@"HideCompletedTasks"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"HideCompletedTasks" andData:@"1"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"DefaultView"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"DefaultView" andData:@"List"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"NewListOrder"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"NewListOrder" andData:@"Beginning"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"NewTaskOrder"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"NewTaskOrder" andData:@"Beginning"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"DefaultSortOrder"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"DefaultSortOrder" andData:@"List"];
		[BLApplicationSetting insertSetting:appSetting];
	}
    
    // Setting for default panel view
    if ([BLApplicationSetting checkIfSettingExists:@"DefaultPanelView"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"DefaultPanelView" andData:@"Drawing Tools"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	// DATE & TIME FOR LAST EDIT, ADD AND DELETE TASK
	if ([BLApplicationSetting checkIfSettingExists:@"LastEditTaskDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastEditTaskDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"LastDeleteTaskDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastDeleteTaskDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"LastAddTaskDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastAddTaskDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	// DATE & TIME FOR LAST EDIT, ADD AND DELETE LIST
	if ([BLApplicationSetting checkIfSettingExists:@"LastEditListDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastEditListDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
    
	if ([BLApplicationSetting checkIfSettingExists:@"LastDeleteListDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastDeleteListDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
    
	if ([BLApplicationSetting checkIfSettingExists:@"LastAddListDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastAddListDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	// TOODLEDO settings
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoUserName"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoUserName" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoUserID"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoUserID" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoSessionToken"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoSessionToken" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoKey"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoKey" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoServiceStatus"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoServiceStatus" andData:@"Not Connected"];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoLastSyncDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoLastSyncDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoServerHasConflictPriority"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoServerHasConflictPriority" andData:@"Local"];
		[BLApplicationSetting insertSetting:appSetting];
	}
    
    if ([BLApplicationSetting checkIfSettingExists:@"ToodledoAutoSync"] == FALSE) {
        // 0 should be false
        ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoAutoSync" andData:@"0"];
        [BLApplicationSetting insertSetting:appSetting];
    }
}

#pragma mark - Run Start Up Code
#pragma mark

- (void) runStartupCode{
    
    // Load the master task list collection
	refMasterTaskListCollection = [[TaskListCollection alloc] init];
    [refMasterTaskListCollection reloadAllLists];
    
    // Do the archive check to archive any completed items that meet requirements - do this in main view controller
	[self archiveCheck];
    
    // refTaskListCollection
    refTaskListCollection = [[TaskListCollection alloc] init];
    [refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
    
    currentListItem = [[ListItem alloc]init];
    refTaskList = [[TaskList alloc] init];
    if ([refTaskListCollection.lists count] > 0) {
        refTaskList = [refTaskListCollection.lists objectAtIndex:0];
    }
    
    notebookCollection = [[NotebookCollection alloc] initWithAllNotebooks];
    
    // Init the tag collection
    tagCollection = [[TagCollection alloc] initWithAllTags];
}

#pragma mark -
#pragma mark Settings Delegate (and AutoSync)

- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData {
	if ([settingName isEqualToString:@"ShowCompleted"]) {
//		masterTaskListCollection.showCompletedTasks = [theData boolValue];//taskListCollection
//		// Update collection
//		[masterTaskListCollection reloadWithMasterTaskListCollection:masterTaskListCollection];
//        [self updateNotebooksUsingCurrentTaskListCollection];
//		if ([self currentViewType] == EnumViewTypeList) {
//			[self updateListScrollList];
//			[self scrollToListAtIndex:currentListIndex animated:YES];
//			[self listShaded:NO forIndex:currentListIndex];
//		} else if (currentViewType == EnumViewTypeTile) {
//			[tileScrollView updateScrollList];
//		}
	} else if ([settingName isEqualToString:@"ShowCompletedDuration"]) {
//		refMasterTaskListCollection.showCompletedTasksDuration = [refMasterTaskListCollection getShowCompletedDurationEnumForData:theData];
//		taskListCollection.showCompletedTasksDuration = [taskListCollection getShowCompletedDurationEnumForData:theData];
//		
//		// Update the other collections
//		[taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
//		[self updateNotebooksUsingCurrentTaskListCollection];
//        
//		if ([self currentViewType] == EnumViewTypeList) {
//			[self updateListScrollList];
//			[self scrollToListAtIndex:currentListIndex animated:YES];
//			[self listShaded:NO forIndex:currentListIndex];
//		} else if ([self currentViewType] == EnumViewTypeTile) {
//			[tileScrollView updateScrollList];
//		}
	} else if ([settingName isEqualToString:@"ArchiveItems"]) {
		refMasterTaskListCollection.archiveSetting = theData;
		refTaskListCollection.archiveSetting = theData;
	} else if ([settingName isEqualToString:@"PocketReminder"]) {
//		masterTaskListCollection.pocketReminder = [theData boolValue];
//		//taskListCollection.pocketReminder = [theData boolValue];
//		
//		// Now need a pocket reminder switch
//		if (masterTaskListCollection.pocketReminder == FALSE) {
//			[itemsDuePocket setHidden:YES];
//			[itemsDueTableView setHidden:YES];
//		} else if (refMasterTaskListCollection.pocketReminder == TRUE) {
//			[itemsDuePocket setHidden:NO];
//			[itemsDueTableView setHidden:NO];
//		}
	} else if ([settingName isEqualToString:@"NewListOrder"]) {
		refMasterTaskListCollection.theNewListOrder = theData;
		refTaskListCollection.theNewListOrder = theData;
	} else if ([settingName isEqualToString:@"DefaultSortOrder"]) {
		refMasterTaskListCollection.defaultSortOrder = theData ;
		refTaskListCollection.defaultSortOrder = theData;
	} else if ([settingName isEqualToString:@"NewTaskOrder"]) {
		refMasterTaskListCollection.theNewTaskOrder = theData;
		refTaskListCollection.theNewTaskOrder = theData;
	} else if ([settingName isEqualToString:@"SelectedTheme"]) {

		refMasterTaskListCollection.selectedTheme = theData;
		refTaskListCollection.selectedTheme = theData;
//		[self reloadTheme];
        
	} else if ([settingName isEqualToString:@"HideCompletedTasks"]) {
        // Set show completed tasks
        if ([theData boolValue] == TRUE) {// set the value
            theData = @"";
        }else {
            theData = @"1";
        }
        
        // Set show completed task follow by setting
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@", theData] forKey:showCompletedTasksSetting];
        
//        [self reloadPreviewTableViews];
    }
    
    // Make sure that settings button, viewtype control and help button are enabled
    [settingsButton setEnabled:YES];
//    [helpBarButtonItem setEnabled:YES];
//    [viewTypeSegmentedControl setUserInteractionEnabled:YES];
    
    //Reload view
    [_rootControlPanel refreshControlPanelWithTaskListCollection:refTaskListCollection notebookCollection:notebookCollection andTagCollection:tagCollection];

    TaskList *currentTaskList = [refTaskListCollection.lists objectAtIndex:_rootControlPanel.listViewController.currentTaskListIndex];

    [_rootControlPanel.listViewController refreshViewWithNewTaskList:currentTaskList andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection];
}

- (void)settingsPopoverCanClose:(BOOL)canClose {
//	settingsViewControllerCanClose = canClose;
}


- (void)closeSettingsPopover {
//	settingsViewControllerCanClose = YES;
    if (settingsPopoverController != nil) {
        [settingsPopoverController dismissPopoverAnimated:YES];
        settingsPopoverController = nil;
    }
    
    // Make sure that settings button, viewtype control and help button are enabled
    [settingsButton setEnabled:YES];
    
//    [helpBarButtonItem setEnabled:YES];
//    [viewTypeSegmentedControl setUserInteractionEnabled:YES];
}

- (void)updatesToDataComplete {
    // Duplicate the currently selected task list
//    progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
//    [self.navigationController.view addSubview:progressHUD];
//    progressHUD.delegate = self;
//    progressHUD.labelText = @"Updating local data...";
//    [progressHUD showWhileExecuting:@selector(updateLocalData) onTarget:self withObject:nil animated:YES];
    
    [_rootControlPanel updateLocalData];
    
}

- (void)replaceToodledoDataComplete {
//    [taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
//    [self updateNotebooksUsingCurrentTaskListCollection];
}

- (void)shopfrontSelected{
    
}

- (void)reloadAllTagsForTaskList{
    
    // reload all tag collection
    [tagCollection reloadAllTags];
    
    // Reload Tag for list view
    [_rootControlPanel refreshControlPanelWithTaskListCollection:refTaskListCollection notebookCollection:notebookCollection andTagCollection:tagCollection];
    
    TaskList *currentTaskList = [refTaskListCollection.lists objectAtIndex:_rootControlPanel.listViewController.currentTaskListIndex];
    
    [_rootControlPanel.listViewController refreshViewWithNewTaskList:currentTaskList andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection];
}




@end
