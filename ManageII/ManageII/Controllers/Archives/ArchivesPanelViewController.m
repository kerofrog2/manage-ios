    //
//  ArchivePanelViewController.m
//  Manage
//
//  Created by Cliff Viegas on 23/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ArchivesPanelViewController.h"

// View Controllers
#import "ArchivesViewController.h"

// Responders
#import "ArchivedListsTableViewResponder.h"

// Data Collections
#import "TaskListCollection.h"

// Constants
#define kViewControllerSize	CGSizeMake(256, 703)
#import "MainConstants.h"
#import "ArchiveConstants.h"

@implementation ArchivesPanelViewController

@synthesize refArchivesViewController;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[closeArchivesButton release];
//	[controlPanelImageView release];
//	[listPadTableView release];
//	[listPadImageView release];
//	[listPadHeaderLine1 release];
//	[listPadHeaderLine2 release];
//	[listPadFooterLine release];
//	[listPadTitleLabel release];
//    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithArchivedListsTableViewResponder:(ArchivedListsTableViewResponder *)responder 
				  andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection {
	if ((self = [super init])) {
		// Assign ref master task list collection to passed collection
		refMasterTaskListCollection = theMasterTaskListCollection;
		
		// Init ref archives view controller to nil
		refArchivesViewController = nil;
		
		// Set content size
		[self setContentSizeForViewInPopover:kViewControllerSize];
		[self.view setBackgroundColor:[UIColor darkGrayColor]];
		
		// Load the control panel
		controlPanelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPanel.png"]];
        CGRect fPanel = kMainControlPanelImageLandscape;
        fPanel.origin.x = (kViewControllerSize.width - fPanel.size.width) / 2;
		[controlPanelImageView setFrame:fPanel];
		[self.view addSubview:controlPanelImageView];
		
		// List paper image view
		listPadImageView = [[UIImageView alloc] init];
		[listPadImageView setImage:[UIImage imageNamed:@"ListPaper.png"]];
		[listPadImageView setFrame:kListPadImageLandscape];
		[self.view addSubview:listPadImageView];
		
		listPadTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
		[listPadTableView setDataSource:responder];
		[listPadTableView setDelegate:responder];
		[listPadTableView setBackgroundColor:[UIColor clearColor]];
		[listPadTableView setShowsVerticalScrollIndicator:NO];
		[listPadTableView setFrame:kArchiveListPadTableViewLandscape];
		[self.view addSubview:listPadTableView];
		
		// Load list pad header lines
		listPadHeaderLine1 = [[UIView alloc] initWithFrame:CGRectZero];
		[listPadHeaderLine1 setBackgroundColor:[UIColor lightGrayColor]];
		[listPadHeaderLine1 setAlpha:0.7f];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Landscape];
		[self.view addSubview:listPadHeaderLine1];
		
		listPadHeaderLine2 = [[UIView alloc] initWithFrame:CGRectZero];
		[listPadHeaderLine2 setBackgroundColor:[UIColor lightGrayColor]];
		[listPadHeaderLine2 setAlpha:0.7f];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Landscape];
		[self.view addSubview:listPadHeaderLine2];
		
		listPadFooterLine = [[UIView alloc] initWithFrame:CGRectZero];
		[listPadFooterLine setBackgroundColor:[UIColor lightGrayColor]];
		[listPadFooterLine setAlpha:0.7f];
		[listPadFooterLine setFrame:kListPadFooterLineLandscape];
		//[self.view addSubview:listPadFooterLine];
		
        // Remove CloseArchivesButton on panel
//		// Init close archives button
//		closeArchivesButton = [[UIButton alloc] init];
//		[closeArchivesButton setBackgroundImage:[UIImage imageNamed:@"ArchivesSelectedButton.png"] forState:UIControlStateNormal];
//		[closeArchivesButton addTarget:self action:@selector(closeArchivesButtonAction) forControlEvents:UIControlEventTouchUpInside];
//		[closeArchivesButton setFrame:kArchivesImageLandscape];
//		[self.view addSubview:closeArchivesButton];
		
		// Load list pad title label
		listPadTitleLabel = [[UILabel alloc] initWithFrame:kListPadTitleLabelLandscape];
		[listPadTitleLabel setBackgroundColor:[UIColor clearColor]];
		[listPadTitleLabel setTextColor:[UIColor lightGrayColor]];
		[listPadTitleLabel setTextAlignment:NSTextAlignmentCenter];
		[listPadTitleLabel setText:@"Archived Lists"];
		[listPadTitleLabel setFont:[UIFont fontWithName:@"MarkerFelt-Thin" size:23.0]];
		[self.view addSubview:listPadTitleLabel];
		
		listPadArchivedFooterLine = [[UIView alloc] initWithFrame:CGRectZero];
		[listPadArchivedFooterLine setBackgroundColor:[UIColor lightGrayColor]];
		[listPadArchivedFooterLine setAlpha:0.7f];
#ifdef IS_FREE_BUILD
		[listPadArchivedFooterLine setFrame:kListPadArchivedFooterLineLandscape];
#endif
		[self.view addSubview:listPadArchivedFooterLine];
		
		// Init archive completed lists button
		archiveCompletedListsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect] ;//retain];
		NSInteger numberOfCompletedLists = [refMasterTaskListCollection getNumberOfCompletedLists];
		NSString *buttonTitle = [NSString stringWithFormat:@"Archive %d completed lists", numberOfCompletedLists];
		if (numberOfCompletedLists == 1) {
			buttonTitle = @"Archive 1 completed list";
		}
		[archiveCompletedListsButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
		[archiveCompletedListsButton setTitle:buttonTitle forState:UIControlStateNormal];
		[archiveCompletedListsButton addTarget:self action:@selector(archiveCompletedListsButtonAction) 
							  forControlEvents:UIControlEventTouchUpInside];
#ifdef IS_FREE_BUILD
		[archiveCompletedListsButton setFrame:kArchiveCompletedListsButtonLandscape];
#endif
		[self.view addSubview:archiveCompletedListsButton];
		
		if (numberOfCompletedLists == 0 || [refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
			[self hideArchiveCompleteLists:YES];
			[listPadTableView setFrame:kArchiveListPadTableViewLandscape];
		} else {
			[self hideArchiveCompleteLists:NO];
			[listPadTableView setFrame:kArchiveListPadTableViewLandscape];
		}
	}
	return self;
}

#pragma mark -
#pragma mark Class Methods

- (void)hideArchiveCompleteLists:(BOOL)hide {
	if (hide == YES) {
		[archiveCompletedListsButton setAlpha:0.0f];
		[listPadArchivedFooterLine setAlpha:0.0f];
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
			[listPadTableView setFrame:kListPadTableViewPortrait];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
			[listPadTableView setFrame:kListPadTableViewLandscape];
		}
	} else {
		[archiveCompletedListsButton setAlpha:1.0f];
		[listPadArchivedFooterLine setAlpha:0.7f];
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
#ifdef IS_FREE_BUILD
			[listPadTableView setFrame:kListPadShortTableViewPortrait];
#endif
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
#ifdef IS_FREE_BUILD
			[listPadTableView setFrame:kListPadShortTableViewLandscape];
#endif
		}
	}
}

#pragma mark -
#pragma mark Button Actions

- (void)closeArchivesButtonAction {
	if (refArchivesViewController != nil) {
		[refArchivesViewController closeArchivesButtonAction];
	}
}

- (void)archiveCompletedListsButtonAction {
	// Call the completed lists button action in archives view controller
	[refArchivesViewController archiveCompletedListsButtonAction];
}


#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewDidLoad {
    NSLog(@"%@", NSStringFromCGRect(self.view.frame));
}



@end
