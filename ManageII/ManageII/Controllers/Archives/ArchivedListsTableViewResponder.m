//
//  ArchiveListsTableViewResponder.m
//  Manage
//
//  Created by Cliff Viegas on 10/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ArchivedListsTableViewResponder.h"

// Data Models
#import "ArchiveList.h"
#import "TaskList.h"

@implementation ArchivedListsTableViewResponder

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithArchiveList:(ArchiveList *)theArchiveList {
	if ((self = [super init])) {
		// Assign reference to passed archive list
		refArchiveList = theArchiveList;
	}
	return self;
}

#pragma mark -
#pragma mark TableView Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];//autorelease
	}
	
	// Set up the cell...
	TaskList *taskList = [refArchiveList.lists objectAtIndex:indexPath.row];
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.textColor = [UIColor grayColor];
	[cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:17.0]];
	cell.textLabel.text = taskList.title;
	
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return [refArchiveList.lists count];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	CGPoint offset = [tableView contentOffset];
	
	// Multiply selected row by 40, get center of tableview (use origin + halfwaypoint (22))
	CGFloat xOrigin = tableView.frame.origin.x + (tableView.frame.size.width - 8);
	CGFloat yOrigin = cell.frame.origin.y + 22 + tableView.frame.origin.y - offset.y;
	CGRect rectPopover = CGRectMake(xOrigin, yOrigin, 1, 1);
	
	// Get the selected task list
	TaskList *taskList = [refArchiveList.lists objectAtIndex:indexPath.row];
	
	// Call the delegate method
	[delegate taskListSelected:taskList atLocation:rectPopover withIndex:indexPath.row];
	
	// Return nil to select nothing
	return nil;
}


@end
