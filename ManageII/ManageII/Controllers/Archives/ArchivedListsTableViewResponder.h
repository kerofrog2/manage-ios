//
//  ArchiveListsTableViewResponder.h
//  Manage
//
//  Created by Cliff Viegas on 10/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class ArchiveList;
@class TaskList;

@protocol ArchivedListsDelegate
- (void)taskListSelected:(TaskList *)taskList atLocation:(CGRect)popoverRect withIndex:(NSInteger)index;
@end


@interface ArchivedListsTableViewResponder : NSObject <UITableViewDelegate, UITableViewDataSource> {
//	id <ArchivedListsDelegate>	delegate;				// Delegate for archived lists
	ArchiveList					*refArchiveList;		// Reference to the passed archive list
}

//@property (nonatomic, assign)		id		delegate;
@property (nonatomic)		NSObject<ArchivedListsDelegate>		*delegate;

#pragma mark Initialisation
- (id)initWithArchiveList:(ArchiveList *)theArchiveList;


@end
