//
//  ArchivesViewController.h
//  Manage
//
//  Created by Cliff Viegas on 3/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class ArchiveList;

// Data Collections
@class TagCollection;
@class TaskListCollection;

#ifdef IS_FREE_BUILD
#else
// Audio Toolbox
#import <AudioToolbox/AudioToolbox.h>
#endif

// Globals
#import "Globals.h"

// Pop Over Controllers (with required delegate link)
#import "MoveItemsViewController.h"

// Archived Lists Responder (for delegate)
#import "ArchivedListsTableViewResponder.h"

// Custom table view cell
#import "ListItemTableViewCell.h"

#ifdef IS_FREE_BUILD
// Advertising
#import "MPAdView.h"
#endif

typedef enum {
	EnumCurrentArchiveTypeTasks = 0,
	EnumCurrentArchiveTypeLists = 1
} EnumCurrentArchiveType;

@protocol ArchivesDelegate
@optional
- (void)archivesUpdated;
- (void)archiveListsUpdated;
@end

#ifdef IS_FREE_BUILD
@interface ArchivesViewController : GAITrackedViewController <MPAdViewDelegate, ArchivedListsDelegate, UIAlertViewDelegate, UIPopoverControllerDelegate, MoveItemsDelegate, UITableViewDelegate, UITableViewDataSource, ListItemDelegate, UIActionSheetDelegate, UITextFieldDelegate> {
	MPAdView                *_adView;
#else
    @interface ArchivesViewController : GAITrackedViewController <ArchivedListsDelegate, UIAlertViewDelegate, UIPopoverControllerDelegate, MoveItemsDelegate, UITableViewDelegate, UITableViewDataSource, ListItemDelegate, UIActionSheetDelegate, UITextFieldDelegate> {
#endif
//        id <ArchivesDelegate>			delegate;
        
        // Archive list
        ArchiveList						*archiveList;					// The list of archived list items
        // Selected Archive List
        NSMutableArray                  *selectedArchiveList;           // The list of archived items, user can delete and move it to other List.
        TaskListCollection				*refMasterTaskListCollection;
        TaskListCollection				*refTaskListCollection;// Needed for moving list items
        TagCollection					*refTagCollection;				// The passed reference to the tag collection
        BOOL							hasUpdatedListItems;			// For tracking when we have updated list items, need to make sure main view controller reloads
        
        // Background & Images
        UIImageView						*canvassImage;					// The canvass image
        UIImageView						*borderImage;					// The border image that sits over our page
        UIImageView						*pageImage;						// The page image that our tableview sits on
        
        // Popover Controllers
        UIPopoverController				*moveItemsPopoverController;	// The move items popover controller
        NSInteger						moveItemsSelectedRow;			// To check whether an item needs to be moved
        
        // List objects
        UITableView						*myTableView;					// Table view for displaying list items
        UIView							*headerLine1;					// One of the header lines on the page
        UIView							*headerLine2;					// One of the header lines on the page
        NSInteger						selectedRow;					// The currently selected row
        
        // Control Panel Objects
        UIImageView						*controlPanelImageView;			// The control panel
        UITableView						*listPadTableView;		// Table view to display archived lists
        ArchivedListsTableViewResponder	*archivedListsTableViewResponder;	// Responder for archived lists
        NSInteger						currentlySelectedList;			// The currently selected task list
        UIImageView						*listPadImageView;					// The list paper background image
        UIView							*listPadHeaderLine1;				// Header line 1 for the list pad
        UIView							*listPadHeaderLine2;				// Header line 2 for the list pad
        UIView							*listPadFooterLine;					// Footer line for the list pad
        UIView							*listPadArchivedFooterLine;		// Footer line to show when archived list button is visible
        UIPopoverController				*panelPopoverController;		// Popover for displaying the landscape view panel
        UILabel							*listPadTitleLabel;
        
        // Buttons
        UIButton						*archiveCompletedListsButton;	// For archiving any completed lists
        UIButton						*archiveCompletedItemsButton;	// For archiving any completed items
        UIBarButtonItem					*closeArchiveBarButtonItem;			// Used to return to the main view
        UIView							*archiveCompletedFooterLine;	// Footer line for when archive completed button is visible
        NSInteger						currentArchiveType;				// This will either be lists or tasks
        UIBarButtonItem					*panelBarButtonItem;            // Show popover to delete and move archive
        UIButton                        *editListButton;
        UIBarButtonItem                 *rightButton;
        //UIBarButtonItem					*archiveCheckBarButtonItem;		// For running method to check for any scheduled archiving
        UIBarButtonItem                 *helpBarButtonItem;
        
        // Header section (lower right)
        UILabel							*titleLabel;					// This will display either 'archived tasks' or lists
        UILabel							*scribblesOnOffLabel;			// Label for setting scribbles on or off
        UISwitch						*scribblesOnOffSwitch;			// Switch to turn scribbles on or off
		
        // Alert Views
        UIAlertView						*unableToArchiveTasksAlertView;	// Alert view when user can not archive some tasks (due to subtasks)
        UIAlertView						*archiveListsAlertView;			// Warning to tell the user that all tasks will be archived
        UIAlertView						*restoreTaskListAlertView;		// Warning before restoring a task list
        UIAlertView						*deleteArchivesAlertView;		// Confirm delete Archives
        UIActionSheet                   *editActionSheet;               // Show option which user choose edit function: delete, move.
        UIActionSheet                   *deleteAllArchivesActionSheet;  // Show option which user delete all archives.
        UIButton                        *deleteArchivesButton;          // OK button on deleteArchivesAlertView
        
        // Help menu options
        BOOL					isHelpSelected;
    }
    
    @property (assign)	id		delegate;
    
#pragma mark Initialisation
    - (id)initWithTagCollection:(TagCollection *)theTagCollection andTaskListCollection:(TaskListCollection *)theTaskListCollection andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection;
    
#pragma mark Public Button Actions
- (void)archiveCompletedListsButtonAction;
- (void)closeArchivesButtonAction;
    
@end
