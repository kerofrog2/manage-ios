//
//  ArchivesTaskTableViewCell.h
//  Manage
//
//  Created by Cliff Viegas on 3/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Parent Class
#import "ListItemTableViewCell.h"

@interface ArchivesTaskTableViewCell : ListItemTableViewCell {

}

@end
