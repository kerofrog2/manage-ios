//
//  NewTagViewController.m
//  Manage
//
//  Created by Cliff Viegas on 16/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "NewTagViewController.h"

// Data Models
#import "Tag.h"
#import "ApplicationSetting.h"
#import "PropertyDetail.h"

// Data Collections
#import "TagCollection.h"

// Custom View Objects
#import "QuartzTagView.h"

// Quartz
#import <QuartzCore/QuartzCore.h>
#import "GAIConstants.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)
#define kPopoverFrame		CGRectMake(0, 0, 320, 318)
#define kTextFieldTag		86551
#define kTextFieldFrame		CGRectMake(43, 12, 247, 23)

@interface NewTagViewController()
- (void)loadButtons;
- (void)loadTableView;
#pragma mark Helper Methods
- (UITextField *)getTextFieldForIndexPath:(NSIndexPath *)indexPath;
@end

@implementation NewTagViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[myTableView release];
//	[doneBarButtonItem release];
//    [tagColourSetting release];
//    [tagColourPropertyDetail release];
//    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

// This is used when managing tags from within a selected task
- (id)initWithTagCollection:(TagCollection *)theTagCollection andSelectedTagID:(NSInteger)theSelectedTagID {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_NewTagViewController;
#endif
		// Assign the tag collection
		refTagCollection = theTagCollection;
		
        // Setup tag colour setting
        tagColourSetting = [[ApplicationSetting alloc] init];
        tagColourSetting.name = @"TagColour";               // This doesn't matter
        tagColourSetting.data = NSLocalizedString(@"TAG_COLOR_BROWN", nil);         // Default tag colour
        
        // Setup the property detail
        tagColourPropertyDetail = [[PropertyDetail alloc] initWithName:@"TagColour" 
                                                       andFriendlyName:@"Tag Colour" 
                                                            andSection:@"" 
                                                       andPropertyType:EnumPropertyTypeList];
        NSArray *tagColourList = [NSArray arrayWithObjects:NSLocalizedString(@"TAG_COLOR_BROWN", nil), NSLocalizedString(@"TAG_COLOR_BLUE", nil), NSLocalizedString(@"TAG_COLOR_RED", nil), NSLocalizedString(@"TAG_COLOR_GREEN", nil), NSLocalizedString(@"TAG_COLOR_YELLOW", nil), NSLocalizedString(@"TAG_COLOR_ORANGE", nil), NSLocalizedString(@"TAG_COLOR_PURPLE", nil), NSLocalizedString(@"TAG_COLOR_PINK", nil),  nil];
        tagColourPropertyDetail.description = @"The colour to use for this tag";
        [tagColourPropertyDetail.list addObjectsFromArray:tagColourList];
        
		// Assign the selected tag ID
		selectedTagID = theSelectedTagID;
		
		// Set the title
		self.title = @"New Tag";
		
		// Load the table view
		[self loadTableView];
		
		// Load the buttons
		[self loadButtons];
		
		// Set popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
	}
	return self;
}

- (id)initWithDelegate:(NSObject<NewTagDelegate>*)theDelegate andTagCollection:(TagCollection *)theTagCollection andSelectedTagID:(NSInteger)theSelectedTagID {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_NewTagViewController;
#endif
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the tag collection
		refTagCollection = theTagCollection;
		
        // Setup tag colour setting
        tagColourSetting = [[ApplicationSetting alloc] init];
        tagColourSetting.name = @"TagColour";               // This doesn't matter
        tagColourSetting.data = NSLocalizedString(@"TAG_COLOR_BROWN", nil);         // Default tag colour
        
        // Setup the property detail
        tagColourPropertyDetail = [[PropertyDetail alloc] initWithName:@"TagColour" 
                                                       andFriendlyName:@"Tag Colour" 
                                                            andSection:@"" 
                                                       andPropertyType:EnumPropertyTypeList];
        NSArray *tagColourList = [NSArray arrayWithObjects:NSLocalizedString(@"TAG_COLOR_BROWN", nil), NSLocalizedString(@"TAG_COLOR_BLUE", nil), NSLocalizedString(@"TAG_COLOR_RED", nil), NSLocalizedString(@"TAG_COLOR_GREEN", nil), NSLocalizedString(@"TAG_COLOR_YELLOW", nil), NSLocalizedString(@"TAG_COLOR_ORANGE", nil), NSLocalizedString(@"TAG_COLOR_PURPLE", nil), NSLocalizedString(@"TAG_COLOR_PINK", nil),  nil];
        tagColourPropertyDetail.description = @"The colour to use for this tag";
        [tagColourPropertyDetail.list addObjectsFromArray:tagColourList];
        
		// Assign the selected tag ID
		selectedTagID = theSelectedTagID;
		
		// Set the title
		self.title = @"New Tag";
		
		// Load the table view
		[self loadTableView];
		
		// Load the buttons
		[self loadButtons];
		
		// Set popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadButtons {
	doneBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" 
														 style:UIBarButtonItemStyleDone 
														target:self 
														action:@selector(doneBarButtonItemAction)];
	[doneBarButtonItem setEnabled:FALSE];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:YES];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" 
																			style:UIBarButtonItemStyleBordered 
																		   target:self 
																		   action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:YES];
//	[cancelBarButtonItem release];
	
}

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	[myTableView setScrollEnabled:NO];
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	
	if ([myTextField.text length] > 0) {
		NSInteger newTagID = [refTagCollection getNewTagID];
		NSInteger newTagOrder = [refTagCollection getNextTagOrderForParentTagID:selectedTagID];
		NSString *newTagName = [myTextField text];
		
		NSInteger newTagDepth = 0;
		
		if (selectedTagID != -1) {
			Tag *parentTag = [refTagCollection getTagWithID:selectedTagID];
			if (parentTag == nil) {
				return;
			}
			
			
			// Increment tag depth by 1
			newTagDepth = parentTag.tagDepth + 1;
		}
		
		Tag *tag = [[Tag alloc] initWithTagID:newTagID andName:newTagName 
							   andParentTagID:selectedTagID 
								  andTagOrder:newTagOrder 
								  andTagDepth:newTagDepth
                                 andTagColour:tagColourSetting.data];
		[tag insertSelfIntoDatabase];
		
		// Have to add this at the correct location (insert)
		[refTagCollection insertNewTag:tag];
		
		if ([self.delegate respondsToSelector:@selector(newTagCreated:)]) {
			[self.delegate newTagCreated:tag];
		}
#if ENABLE_GOOGLE_ANALYTICS
        // track this event to Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker sendEventWithCategory:kCategory_event
                            withAction:kAction_tag
                             withLabel:[NSString stringWithFormat:@"%@ %@", kLabel_tag_create, newTagName]
                             withValue:0];
#endif
//		[tag release];
	}
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }
    
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];// autorelease];
	
		[cell.contentView addSubview:[self getTextFieldForIndexPath:indexPath]];
	}	
	
	// Get content view objects
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	
	// Default values
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	[cell setAccessoryType:UITableViewCellAccessoryNone];
	[cell.textLabel setText:@""];
	[cell.textLabel setTextColor:[UIColor darkTextColor]];
	cell.imageView.image = nil;
	
	if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [myTextField setHidden:NO];
            cell.imageView.image = [UIImage imageNamed:@"TagNoRightBuffer.png"];
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"Colour";
            [myTextField setHidden:YES];
            cell.detailTextLabel.text = tagColourSetting.data;
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            // Set the image view
            QuartzTagView *tagView = [[QuartzTagView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)
                                                              andTagColour:tagColourSetting.data];// autorelease];
            UIGraphicsBeginImageContext(tagView.bounds.size);
            [tagView.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            cell.imageView.image = viewImage;
        }
		
	} else {
		[myTextField setHidden:YES];
		[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		[cell.textLabel setTextColor:[UIColor colorWithRed:0.243 green:0.306 blue:0.435 alpha:1.0]];
		
		if (selectedTagID == -1) {
			[cell.textLabel setText:@"Tags"];
		} else {
			NSInteger tagIndex = [refTagCollection getIndexOfTagWithID:selectedTagID];
			cell.textLabel.text = [[refTagCollection.tags objectAtIndex:tagIndex] name];
		}
	}
	
	// Set cell properties
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 1) {
		SelectListItemTagViewController *controller = [[SelectListItemTagViewController alloc] initWithDelegate:self
																								andTagCollection:refTagCollection 
																								andSelectedTagID:selectedTagID];// autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if (indexPath.section == 0) {
        if (indexPath.row == 1) {
            ListSettingViewController *controller = [[ListSettingViewController alloc] initWithDelegate:self
                                                                                           andAppSetting:tagColourSetting 
                                                                                       andPropertyDetail:tagColourPropertyDetail];// autorelease];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	if (section == 0) {
		return 75.0;
	}
	return 0.0;
}

- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];// autorelease];
}

#pragma mark -
#pragma mark UIView Controller Delegates

- (void)viewWillAppear:(BOOL)animated {
	// Register for keyboard notifications
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) 
	//											 name:UITextFieldTextDidChangeNotification object:nil]; 
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	//[[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil]; 
	[super viewWillDisappear:animated];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	if ([myTextField isFirstResponder] == FALSE) {
		[myTextField becomeFirstResponder];
	}
	
	[super viewDidAppear:animated];
}

#pragma mark -
#pragma mark Select List Item Tag Delegates

- (void)tagIDSelected:(NSInteger)theSelectedTagID {
	selectedTagID = theSelectedTagID;
	[myTableView reloadData];
}

#pragma mark -
#pragma mark List Setting Delegates

- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData {
    tagColourSetting.data = theData;        // Copy property, auto-copy
    [myTableView reloadData];
}

#pragma mark -
#pragma mark UITextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	
	// To handle text field should return
	// Will pop view controller and store value
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	
	if ([myTextField isFirstResponder]) {
		[myTextField resignFirstResponder];
	}
	
	return YES;
}

//- (void)textFieldDidChange:(NSNotification *)theNotification {
	/*UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];

	
	if ([myTextField.text length] == 0) {
		[doneBarButtonItem setEnabled:FALSE];
	}
	
	[doneBarButtonItem setEnabled:TRUE];*/
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

	if (range.location == 0 && range.length == 0) {
		[doneBarButtonItem setEnabled:TRUE];
		return YES;
	} else if (range.location == 0 && range.length == [textField.text length]) {
		[doneBarButtonItem setEnabled:FALSE];
		return YES;
	}
	
	[doneBarButtonItem setEnabled:TRUE];

	return YES;
}

#pragma mark -
#pragma mark Helper Methods

- (UITextField *)getTextFieldForIndexPath:(NSIndexPath *)indexPath {
	UITextField *textField = [[UITextField alloc] initWithFrame:kTextFieldFrame];// autorelease];
	
	if (indexPath.section == 0) {
		[textField becomeFirstResponder];
	}
	
	[textField setBackgroundColor:[UIColor clearColor]];
	[textField setDelegate:self];
	[textField setText:@""];
	[textField setAutocorrectionType:UITextAutocorrectionTypeNo];
	[textField setTag:kTextFieldTag];
	[textField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19.0]];
	[textField setTextColor:[UIColor darkTextColor]];
	[textField setReturnKeyType:UIReturnKeyDone];
	//[textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
	
	return textField;
}


@end
