//
//  NewTagDelegate.h
//  Manage
//
//  Created by Cliff Viegas on 24/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "Tag.h"

@protocol NewTagDelegate
@optional
- (void)newTagCreated:(Tag *)theTag;
@end
