//
//  SelectListItemTagViewController.m
//  Manage
//
//  Created by Cliff Viegas on 23/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "SelectListItemTagViewController.h"

// Data Models
#import "Tag.h"

// Data Collections
#import "TagCollection.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@interface SelectListItemTagViewController()
- (void)loadTableView;
@end


@implementation SelectListItemTagViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[myTableView release];
//    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

// Need to select a set tag, will use a delegate for this
- (id)initWithDelegate:(NSObject<SelectListItemTagDelegate>*)theDelegate andTagCollection:(TagCollection *)theTagCollection
	  andSelectedTagID:(NSInteger)theSelectedTagID {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_SelectListItemTagViewController;
#endif
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the tag collections
		refTagCollection = theTagCollection ;//retain];
		
		// Assign the selected tag (may be nil)
		selectedTagID = theSelectedTagID;
		
		// Set the title
		self.title = @"Tags";
		
		// Load the tableview
		[self loadTableView];
		
		// Set popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
	}
	return self;
}



#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [refTagCollection.tags count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];// autorelease];
	}	
	
	// Set default values
	[cell setIndentationWidth:20.0];
	[cell setIndentationLevel:0];
	[cell setAccessoryType:UITableViewCellAccessoryNone];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.imageView.image = [UIImage imageNamed:@"TagNoRightBuffer.png"];
	
	// Handle displaying the cell
	if (indexPath.row == 0) {
		[cell.textLabel setText:@"Tags"];
		if (selectedTagID == -1) {
			[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
		}
		
	} else {
		Tag *tag = [refTagCollection.tags objectAtIndex:indexPath.row - 1];
		cell.textLabel.text = tag.name;
		cell.tag = tag.tagID;
		[cell setIndentationLevel:tag.tagDepth + 1];
		
		if (selectedTagID == tag.tagID) {
			[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
		}

		
	}
	
	// Set cell properties
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == 0) {
		selectedTagID = -1;
	} else {
		UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
		NSInteger tagIndex = [refTagCollection getIndexOfTagWithID:cell.tag];
		Tag *tag = [refTagCollection.tags objectAtIndex:tagIndex];
		selectedTagID = tag.tagID;
	}

	[tableView reloadData];
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewWillDisappear:(BOOL)animated {
	[delegate tagIDSelected:selectedTagID];
	[super viewWillDisappear:animated];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





@end
