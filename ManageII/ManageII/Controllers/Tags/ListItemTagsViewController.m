//
//  ListItemTagsViewController.m
//  Manage
//
//  Created by Cliff Viegas on 10/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "ListItemTagsViewController.h"

// Data Collections
#import "ListItemTagCollection.h"
#import "TagCollection.h"

// View Controllers
#import "ManageTagsViewController.h"

// Data Models
#import "ListItemTag.h"
#import "Tag.h"
#import "GAIConstants.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@interface ListItemTagsViewController () {
    BOOL updateTags;
}

@end

@implementation ListItemTagsViewController

@synthesize listItemID;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[originalListItemTags release];
//	[myTableView release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection 
	  andTagCollection:(TagCollection *)theTagCollection 
		 andListItemID:(NSInteger)theListItemID {
	if (self = [super init]) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_ListItemTagsViewController;
#endif
		// Set the title
		self.title = @"Tags";
		
		// Set is new list item to false
		isNewListItem = FALSE;
		
		// Set the list item ID
		self.listItemID = theListItemID;
		
		// Assign the reference to the list item tag collection
		refListItemTagCollection = theListItemTagCollection;
        
        // Set the original list item tag collection (as a copy)
		originalListItemTags = [refListItemTagCollection.listItemTags mutableCopy];
        
		// Assign the reference to the tag collection
		refTagCollection = theTagCollection;
		
		// Set popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Load the buttons
		[self loadButtons];
		
		// Load the tableview
		[self loadTableView];
	}
	return self;
}

- (id)initNewListItemWithListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection 
				   andTagCollection:(TagCollection *)theTagCollection 
					  andListItemID:(NSInteger)theListItemID {
	if (self = [super init]) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_ListItemTagsViewController;
#endif
		// Set the title
		self.title = @"Tags";
		
		// Set new list item to true
		isNewListItem = TRUE;
		
		// Set the list item ID
		self.listItemID = theListItemID;
		
		// Assign the reference to the list item tag collection
		refListItemTagCollection = theListItemTagCollection;
		
		// Set the original list item tag collection (as a copy)
		originalListItemTags = [refListItemTagCollection.listItemTags mutableCopy];
		
		// Assign the reference to the tag collection
		refTagCollection = theTagCollection;
		
		// Set popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Load the buttons
		[self loadButtons];
		
		// Load the tableview
		[self loadTableView];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

- (void)loadButtons {
	// Set the right bar button and the left bar button
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone 
																					   target:self 
																					   action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
//	[doneBarButtonItem release];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
																						 target:self 
																						 action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
//	[cancelBarButtonItem release];
	
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
#if ENABLE_GOOGLE_ANALYTICS
    // track this event to Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker sendEventWithCategory:kCategory_event
                        withAction:kAction_tag
                         withLabel:kLabel_tag_set
                         withValue:0];
#endif
    
    if ([_delegate respondsToSelector:@selector(didUpdateTags:)] && updateTags == YES) {
        [_delegate didUpdateTags:updateTags];
    }
    
	// List item tags already updated, just pop the view controller
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
	// Clear the list of tags before popping the view controller
	//[refListItemTagCollection.listItemTags removeAllObjects];
	
    [refListItemTagCollection removeAllTags];
    
	// Set the collection to our copied collection
	//refListItemTagCollection.listItemTags = [originalListItemTagCollection.listItemTags copy];
	//[refListItemTagCollection release];
	refListItemTagCollection.listItemTags = [originalListItemTags mutableCopy];//autorelease
	
    [refListItemTagCollection insertSelfIntoDatabase];
    
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark New Tag Delegates

- (void)newTagCreated:(Tag *)theTag {
	[refTagCollection.tags addObject:theTag];
	[myTableView reloadData];
}

#pragma mark -
#pragma mark Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
		return [refTagCollection.tags count];
	}
	return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];//autorelease
	}
	
	// Set default values
	[cell setIndentationWidth:20.0];
	[cell setIndentationLevel:0];
	[cell setAccessoryType:UITableViewCellAccessoryNone];
	[cell.textLabel setTextColor:[UIColor darkTextColor]];
	cell.imageView.image = nil;
	
	// Handle displaying the cell
	if (indexPath.section == 0) {
		
		Tag *tag = [refTagCollection.tags objectAtIndex:indexPath.row];
		cell.textLabel.text = tag.name;
		cell.tag = tag.tagID;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		// Image for tag
		cell.imageView.image = [UIImage imageNamed:@"TagNoRightBuffer.png"];
		
		// Need to know the level the tag is at.
		//NSInteger indentLevel = [refTagCollection getIndentLevelOfTagWithID:tag.tagID];
		
		// Set the indentation level
		[cell setIndentationLevel:tag.tagDepth];
		
		if ([refListItemTagCollection containsTagWithID:tag.tagID]) {
			cell.accessoryType = UITableViewCellAccessoryCheckmark;
			
			//UIView *localBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
			//cell.backgroundView = localBackgroundView;
			
			
			//[cell setBackgroundColor:[UIColor colorWithRed:204.0 / 255.0 green:174.0 / 255.0 blue:127.0 / 255.0 alpha:1.0]];
			//cell.textLabel.backgroundColor = [UIColor clearColor];
			//cell.detailTextLabel.backgroundColor = [UIColor clearColor];
		} else {
			cell.accessoryType = UITableViewCellAccessoryNone;
			//[cell setBackgroundColor:[UIColor whiteColor]];
			//cell.textLabel.backgroundColor = [UIColor whiteColor];
			//cell.detailTextLabel.backgroundColor = [UIColor whiteColor];
		}
		
		//CGContextSetRGBFillColor(context, 204.0 / 255.0, 174.0 / 255.0, 127.0 / 255.0, 1.0);
		
	} else if (indexPath.section == 1) {
		[cell.textLabel setTextColor:[UIColor colorWithRed:0.243 green:0.306 blue:0.435 alpha:1.0]];
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		if (indexPath.row == 0) {
			cell.textLabel.text = @"New Tag";
			cell.imageView.image = [UIImage imageNamed:@"TagNew.png"];
		} else if (indexPath.row == 1) {
			cell.textLabel.text = @"Manage Tags";
			cell.imageView.image = [UIImage imageNamed:@"TagEdit.png"];
		}
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Need to remove tag if it exists, and add if otherwise
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	if (indexPath.section == 0) {
		if ([refListItemTagCollection containsTagWithID:cell.tag]) {
			// Already exists, remove from collection/database
			[refListItemTagCollection removeTagWithID:cell.tag andIsNewListItem:isNewListItem];
		} else {
			// Does not exist, add it to collection/database
			// Need to cater for new list item where we dont have id yet
			
			[refListItemTagCollection addTagWithID:cell.tag andListItemID:self.listItemID andIsNewListItem:isNewListItem];
		}
		[tableView reloadData];
        updateTags = YES;
	} else if (indexPath.section == 1) {
		if ([cell.textLabel.text isEqualToString:@"Manage Tags"]) {
			// Manage tags
			ManageTagsViewController *controller = [[ManageTagsViewController alloc]
													 initWithTagCollection:refTagCollection];//autorelease
			[self.navigationController pushViewController:controller animated:YES];
		} else if ([cell.textLabel.text isEqualToString:@"New Tag"]) {
			// Add a new tag
			NewTagViewController *controller = [[NewTagViewController alloc] initWithTagCollection:refTagCollection 
																			  andSelectedTagID:-1];//autorelease
			[self.navigationController pushViewController:controller animated:YES];
		}
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewWillAppear:(BOOL)animated {
	[refTagCollection reloadAllTags];
	[myTableView reloadData];
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
	// Set popover size
	[self setContentSizeForViewInPopover:kPopoverSize];
	
	[super viewDidAppear:animated];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
