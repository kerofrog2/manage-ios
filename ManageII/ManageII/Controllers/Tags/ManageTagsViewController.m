//
//  ManageTagsViewController.m
//  Manage
//
//  Created by Cliff Viegas on 16/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "ManageTagsViewController.h"

// Data Collections
#import "TagCollection.h"

// View Controllers
#import "NewTagViewController.h"

// Data Models
#import "Tag.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 274)
#define kToolbarFrame		CGRectMake(0, 274, 320, 44)
#define kPopoverSize		CGSizeMake(320, 318)

// Private Methods
@interface ManageTagsViewController()
- (void)loadTableView;
- (void)loadButtons;
- (void)loadToolbar;
@end


@implementation ManageTagsViewController


#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[doneBarButtonItem release];
//	[newTagBarButtonItem release];
//	[editBarButtonItem release];
//	[myTableView release];
//	[tagCollection release];
//    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithTagCollection:(TagCollection *)theTagCollection {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_ManageTagsViewController;
#endif
		// Assign to ref tag collection
		refTagCollection = theTagCollection;
		
		// Init tag collection with Parent Tag ID
		tagCollection = [[TagCollection alloc] initWithAllTagsForParentTagID:-1];
		
		// Set the manage parent tag ID
		manageParentTagID = -1;
		
		// Set the title
		self.title = @"Manage Tags";
		
		// Set the manage tags depth
		manageTagsDepth = 0;
		
		// Init the selected tag id
		selectedTagID = -1;
		
		// Need to set the popover controller size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Dont show left bar button item
		[self.navigationItem setHidesBackButton:YES];
		
		// Need to load the tableview
		[self loadTableView];
		
		// Load the buttons
		[self loadButtons];
		
		// Load the toolbar
		[self loadToolbar];
	}
	return self;
}

- (id)initWithTagCollection:(TagCollection *)theTagCollection forParentTagID:(NSInteger)theParentTagID
		   andParentTagName:(NSString *)theParentTagName andSelectedTagID:(NSInteger)theSelectedTagID 
		 andManageTagsDepth:(NSInteger)theManageTagsDepth {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_ManageTagsViewController;
#endif
		// Assign to ref tag collection
		refTagCollection = theTagCollection;
		
		// Init tag collection with Parent Tag ID
		tagCollection = [[TagCollection alloc] initWithAllTagsForParentTagID:theParentTagID];
		
		// Set the manage parent tag id
		manageParentTagID = theParentTagID;
		
		// Set the manage tags depth
		manageTagsDepth = theManageTagsDepth;
		
		// Init selected tag ID
		selectedTagID = theSelectedTagID;
		
		// Set the title
		self.title = theParentTagName;
		
		// Need to set the popover controller size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Need to load the tableview
		[self loadTableView];
		
		// Load the buttons
		//[self loadButtons];
		
		// Load the toolbar
		[self loadToolbar];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame 
											   style:UITableViewStyleGrouped];
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

- (void)loadToolbar {
	myToolbar = [[UIToolbar alloc] initWithFrame:kToolbarFrame];
	[myToolbar setTintColor:[UIColor darkGrayColor]];
	//[myToolbar setTintColor:[UIColor colorWithRed:204.0 / 255.0 
											//green:174.0 / 255.0 
											// blue:127.0 / 255.0 alpha:1.0]];
	//(context, 204.0 / 255.0, 174.0 / 255.0, 127.0 / 255.0, 1.0)
	
	// Create an array to hold the buttons on the toolbar
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] init];
	
	// Edit Button
	//editBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit 
	//																				   target:self action:@selector(editBarButtonItemAction)];
	editBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" 
														 style:UIBarButtonItemStyleBordered 
														target:self action:@selector(editBarButtonItemAction)];
	[toolbarButtons addObject:editBarButtonItem];
	
	
	// Separator
	UIBarButtonItem *separator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace 
																			   target:nil action:nil];
	[toolbarButtons addObject:separator];
//	[separator release];
	
	
	// New Tag Button
	newTagBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"New Tag" 
																			style:UIBarButtonItemStyleBordered 
																		   target:self action:@selector(newTagBarButtonItemAction)];
	[toolbarButtons addObject:newTagBarButtonItem];
//	[newTagBarButtonItem release];
	
	[myToolbar setItems:toolbarButtons];
//	[toolbarButtons release];
	
	[self.view addSubview:myToolbar];
}

- (void)loadButtons {
	// Set the right bar button and the left bar button
	doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone 
																					   target:self 
																					   action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
    
    if ([_delegate respondsToSelector:@selector(reloadAllTags)]) {
        [_delegate reloadAllTags];
    }
    
	// Tags already updated, just pop the view controller
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)newTagBarButtonItemAction {
	NewTagViewController *controller = [[NewTagViewController alloc] initWithDelegate:(id)self
																		 andTagCollection:refTagCollection 
																		   andSelectedTagID:selectedTagID];//autorelease
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)editBarButtonItemAction {
	if ([[editBarButtonItem title] isEqualToString:@"Edit"]) {
		[editBarButtonItem setTitle:@"Done"];
		[editBarButtonItem setStyle:UIBarButtonItemStyleDone];
		[myTableView setEditing:YES animated:YES];
		[self.navigationItem setRightBarButtonItem:nil animated:YES];
		[newTagBarButtonItem setEnabled:NO];
	} else if ([[editBarButtonItem title] isEqualToString:@"Done"]) {
		[editBarButtonItem setTitle:@"Edit"];
		[editBarButtonItem setStyle:UIBarButtonItemStyleBordered];
		[myTableView setEditing:NO animated:YES];
		[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:YES];
		[newTagBarButtonItem setEnabled:YES];
	}

}

#pragma mark -
#pragma mark New Tag Delegates

- (void)newTagCreated:(Tag *)theTag {
	if (manageTagsDepth == theTag.tagDepth) {
		[tagCollection.tags addObject:theTag];
	}	
}

#pragma mark -
#pragma mark UITableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tagCollection.tags count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];//autorelease
	}
	
	// Default Cell values
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	cell.imageView.image = [UIImage imageNamed:@"TagNoRightBuffer.png"];
	
	// Display the cell
	Tag *tag = [tagCollection.tags objectAtIndex:indexPath.row];
	
	// Checks to see if there are any child tags
	if ([refTagCollection checkForAnyTagWithParentTagID:tag.tagID] == TRUE) {
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
	
	// Set the text label name
	cell.textLabel.text = tag.name;
	
	
	return cell;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	// If the user has not moved the page to a new row, exit and do nothing
	if (fromIndexPath.row == toIndexPath.row) {
		// Do nothing
		return;
	}
	
	// Create a copy of the tag
	Tag *tagCopy = [[tagCollection.tags objectAtIndex:fromIndexPath.row] copy];
	
	// Delete the old tag
	[tagCollection.tags removeObjectAtIndex:fromIndexPath.row];
	
	// Insert the new copy of the tag in the correct location
	[tagCollection.tags insertObject:tagCopy atIndex:toIndexPath.row];
//	[tagCopy release];
	
	
	// Update the tag order for the affected tag
	Tag *tag = [tagCollection.tags objectAtIndex:toIndexPath.row];
	
	// Now need to do the annoying part which is changing the tag order
	if (toIndexPath.row == 0 || fromIndexPath.row > toIndexPath.row) {
		tag.tagOrder = [[tagCollection.tags objectAtIndex:toIndexPath.row + 1] tagOrder];
	} else if (toIndexPath.row == [tagCollection.tags count] - 1 || fromIndexPath.row < toIndexPath.row) {
		tag.tagOrder = [[tagCollection.tags objectAtIndex:toIndexPath.row - 1] tagOrder];
	}
	
	// Then need to subtract (or add) 1 to every single tag order after the current one
	[tagCollection updateTagOrderFromIndex:fromIndexPath.row toIndex:toIndexPath.row];
	
	// Reload all tags
	[refTagCollection reloadAllTags];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if (cell.accessoryType != UITableViewCellAccessoryDisclosureIndicator) {
		return;
	}
	
	Tag *tag = [tagCollection.tags objectAtIndex:indexPath.row];
	ManageTagsViewController *controller = [[ManageTagsViewController alloc] initWithTagCollection:refTagCollection 
																					 forParentTagID:tag.tagID
																				   andParentTagName:tag.name
																				   andSelectedTagID:tag.tagID
																				 andManageTagsDepth:manageTagsDepth++];//autorelease
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		Tag *tag = [tagCollection.tags objectAtIndex:indexPath.row];
		
		NSInteger numberOfChildTags = [refTagCollection getNumberOfChildTagsForParentTagID:tag.tagID];
		
		if (numberOfChildTags > 0) {
			// Warning
			NSString *message = @"Deleting this tag will also delete";
			if (numberOfChildTags == 1) {
				message = [NSString stringWithFormat:@"%@ 1 subtag", message];
			} else {
				message = [NSString stringWithFormat:@"%@ %d subtags", message, numberOfChildTags];
			}

			UIAlertView *deleteAlert = [[UIAlertView alloc] initWithTitle:@"Warning: Tag has subtags" 
																  message:message 
																 delegate:self 
														cancelButtonTitle:@"Cancel" 
														otherButtonTitles:@"Ok", nil];
			[deleteAlert setTag:indexPath.row];
			[deleteAlert show];
//			[deleteAlert release];
		} else {
			// Delete
			NSInteger deleteTagIndex = [tagCollection getIndexOfTagWithID:tag.tagID];
			NSInteger deleteRefTagIndex = [refTagCollection getIndexOfTagWithID:tag.tagID];
			
			[tagCollection.tags removeObjectAtIndex:deleteTagIndex];
			[refTagCollection deleteTagAtIndex:deleteRefTagIndex];
			
			[myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]]  withRowAnimation:YES];
		}
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	Tag *selectedTag = [tagCollection.tags objectAtIndex:indexPath.row];
	
	EditTagViewController *controller = [[EditTagViewController alloc] initWithDelegate:self
																		   andTagCollection:refTagCollection 
																			   andEditingTag:selectedTag 
																			 andSelectedParentTagID:selectedTagID];//autorelease
	[self.navigationController pushViewController:controller animated:YES];
	
	// Need to reload all tags if there are changes made
}

#pragma mark -
#pragma mark Edit Tag Delegates

- (void)tagEdited:(Tag *)theTag {
	// Make changes to the tag (database only), as it will be reloaded
	[theTag updateSelfInDatabase];
	
	// Reload all tags
	[refTagCollection reloadAllTags];
	
	// Reload all tags in tag collection
	[tagCollection reloadAllTagsForParentTagID:manageParentTagID];
	
	// Reload the tableview
	[myTableView reloadData];
}

#pragma mark -
#pragma mark UIAlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	// Cancel
	if (buttonIndex == 0) {
		return;
	} else if (buttonIndex == 1) {
		// Ok
		Tag *tag = [tagCollection.tags objectAtIndex:alertView.tag];
		
		NSMutableArray *tagsToDeleteArray = [refTagCollection getArrayOfTagsToDeleteForParentTag:tag];
		
		for (Tag *tagToDelete in tagsToDeleteArray) {
			// Delete
			NSInteger deleteRefTagIndex = [refTagCollection getIndexOfTagWithID:tagToDelete.tagID];

			[refTagCollection deleteTagAtIndex:deleteRefTagIndex];
		}
		
		// Delete the parent tag
		[tagCollection.tags removeObjectAtIndex:alertView.tag];
		
		// Remove rows from the table
		[myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:alertView.tag inSection:0]] 
						   withRowAnimation:YES];
		
	}
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewWillAppear:(BOOL)animated {
	[tagCollection reloadAllTagsForParentTagID:manageParentTagID];
	[myTableView reloadData];
	[super viewWillAppear:animated];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
