//
//  ListItemTagsViewController.h
//  Manage
//
//  Created by Cliff Viegas on 10/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class ListItemTagCollection;
@class TagCollection;

// View Controllers (with Delegates)
#import "NewTagViewController.h"

@protocol ListItemTagsViewControllerDelegate <NSObject>

@optional
- (void) didUpdateTags:(BOOL) isUpdate;

@end

@interface ListItemTagsViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {	
	UITableView				*myTableView;
	ListItemTagCollection	*refListItemTagCollection;
	NSMutableArray			*originalListItemTags;
	TagCollection			*refTagCollection;
	
	NSInteger				listItemID;
	BOOL					isNewListItem;
}

@property (nonatomic, assign)	NSInteger	listItemID;
// The delegate will be notified
@property (nonatomic, assign) NSObject<ListItemTagsViewControllerDelegate> *delegate;

#pragma mark Initialisation
- (id)initWithListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection 
				   andTagCollection:(TagCollection *)theTagCollection 
					  andListItemID:(NSInteger)theListItemID;
- (id)initNewListItemWithListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection 
								 andTagCollection:(TagCollection *)theTagCollection 
									andListItemID:(NSInteger)theListItemID;
#pragma mark Load Methods
- (void)loadTableView;
- (void)loadButtons;

@end
