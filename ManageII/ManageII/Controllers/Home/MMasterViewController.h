//
//  RPNMasterViewController.h
//  SplitViewSliderDemo
//
//  Created by Rob on 18/02/2013.
//  Copyright (c) 2013 Rob Nadin. All rights reserved.
//

@class MDetailViewController;

@interface MMasterViewController : UITableViewController

@property (strong, nonatomic) MDetailViewController *detailViewController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
