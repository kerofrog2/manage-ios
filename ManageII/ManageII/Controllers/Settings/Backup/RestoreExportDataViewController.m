    //
//  RestoreExportDataViewController.m
//  Manage
//
//  Created by Cliff Viegas on 26/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "RestoreExportDataViewController.h"

// Method Helper
#import "MethodHelper.h"
#import "GAIConstants.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@interface RestoreExportDataViewController()
// Load Methods
- (void)loadTableView;
// Send mail methods
- (void)sendBackupDataMail:(NSString *)filePath andFileName:(NSString *)fileName;
@end

@implementation RestoreExportDataViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[myTableView release];
//	
//	[super dealloc];
}


#pragma mark -
#pragma mark Initialisation

- (id)initWithBackup:(NSString *)theBackupFile andBackupPath:(NSString *)theBackupPath {
	if (self = [super init]) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_RestoreExportDataViewController;
#endif
		// Assign the backup file
		backupFile = [theBackupFile copy];
		
		// Assign the backup path
		backupPath = [theBackupPath copy];
		
		// Get the time stamp string
		NSString *timeStampString = [MethodHelper getTimeStampStringFromFile:backupFile];
		
		NSTimeInterval timeInterval = [timeStampString doubleValue];
		NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
		
		backupDataDate = [MethodHelper localizedDateTimeFrom:date
											 usingDateStyle:NSDateFormatterMediumStyle 
                                                 andTimeStyle:NSDateFormatterMediumStyle];// retain];
		
		// Set the title
		self.title = @"Export or Restore";
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Load the tableview
		[self loadTableView];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];// autorelease];
	}
	
	// Default values
	cell.textLabel.textColor = [UIColor colorWithRed:86 / 255.0 green:104 / 255.0 blue:148 / 255.0 alpha:1.0];
	cell.textLabel.textAlignment = UITextAlignmentCenter;
	
	if (indexPath.section == 0) {
		cell.textLabel.text = @"Restore Backup Data";
	} else if (indexPath.section == 1) {
		cell.textLabel.text = @"Export Backup Data";
	}
	
	
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return 1;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	if (indexPath.section == 0) {
		// Warning alert box
		UIAlertView *restoreAlertView = [[UIAlertView alloc] initWithTitle:@"Warning" 
																   message:@"Restoring this backup data will replace your existing data.  Are you sure you want to do this?" 
																  delegate:self 
														 cancelButtonTitle:@"Cancel" 
														 otherButtonTitles:@"Ok", nil];
		[restoreAlertView show];
//		[restoreAlertView release];
#if ENABLE_GOOGLE_ANALYTICS
        // track this event to Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker sendEventWithCategory:kCategory_event
                            withAction:kAction_backup
                             withLabel:kLabel_restore_backup
                             withValue:0];
#endif
	} else if (indexPath.section == 1) {
		// Pop up the mail controller
		[self sendBackupDataMail:backupPath andFileName:backupFile];
#if ENABLE_GOOGLE_ANALYTICS
        // track this event to Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker sendEventWithCategory:kCategory_event
                            withAction:kAction_backup
                             withLabel:kLabel_export_backup
                             withValue:0];
#endif
	}
	
	[myTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	if (section == 0) {
		return @"Restore the selected backup data";
	} else if (section == 1) {
		return @"Export the selected backup data via email";
	}
	return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (section == 0) {
		NSString *headerTitle = [NSString stringWithFormat:@"Backup: %@", backupDataDate];
		return headerTitle;
	}
	
	return @"";
}


#pragma mark -
#pragma mark Send Mail Methods

- (void)sendBackupDataMail:(NSString *)filePath andFileName:(NSString *)fileName {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
//		[mailAlert release];
		return;
	}
	
	NSData *dataObj = [NSData dataWithContentsOfFile:filePath];
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	
	// Get the date
	NSString *timeStampString = [MethodHelper getTimeStampStringFromFile:fileName];
	NSTimeInterval timeInterval = [timeStampString doubleValue];
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
	NSString *dateString = [MethodHelper localizedDateTimeFrom:date 
						 usingDateStyle:NSDateFormatterMediumStyle 
						   andTimeStyle:NSDateFormatterMediumStyle];
	
	[controller setSubject:@"Manage Backup"];
	
	
	[controller setMessageBody:[NSString stringWithFormat:@"Backup was created on %@",
								dateString]	isHTML:NO];
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	[controller addAttachmentData:dataObj 
						 mimeType:@"application/zip" fileName:fileName];
	
	[self presentModalViewController:controller animated:YES];
//	[controller release];
}

#pragma mark -
#pragma mark Mail Composer View Delegates

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	NSString *message = @"";
	
	switch (result) {
		case MFMailComposeResultCancelled:
			message = @"Export Cancelled";
			break;
		case MFMailComposeResultSaved:
			message = @"Export Cancelled";
			break;
		case MFMailComposeResultSent:
			message = @"Export Sent";
			break;
		case MFMailComposeResultFailed:
			message = @"Export Failed";
			break;
		default:
			break;
	}
	
	// Display result from mail composer
	UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Mail Composer"
														message: message
													   delegate: nil
											  cancelButtonTitle: @"Ok"
											  otherButtonTitles: nil];
	[mailAlert show];
//	[mailAlert release];
	
	[self becomeFirstResponder];
	[self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UIAlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	if (buttonIndex == 0) {
		// Cancel
		return;
	} else if (buttonIndex == 1) {
		// Ok
		// Replace the existing database with the selected data...
		
		[MethodHelper restoreBackupData:backupFile backupPath:backupPath];
		
		[self.delegate closeSettingsPopover];
		
		[self.delegate restoreDataComplete];
		
		// Also need to replace any scribbles.
		
		// Then call method in Main View controller to reload everything
		
//        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
//            // AskingPoint event
//            [APManager addEventWithName:kAskingPointEventRestoreBackup];
//        }
	}
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end
