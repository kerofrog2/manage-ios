//
//  DatabaseBackupViewController.h
//  Manage
//
//  Created by Cliff Viegas on 16/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// View Controllers
#import "RestoreExportDataViewController.h"

// AppDelegate
#import "MAppDelegate.h"

@protocol DataBackupDelegate
- (void)dataSuccessfullyBackedUp;
- (void)restoreDataComplete;
@end


@interface DataBackupViewController : GAITrackedViewController <RestoreExportDataDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
//	id <DataBackupDelegate>	delegate;
	
	UITableView					*myTableView;
	
	NSArray						*backupListArray;
	NSArray						*importedBackupListArray;
}

//@property (nonatomic, assign)	id		delegate;
@property (nonatomic, assign)	NSObject<DataBackupDelegate>		*delegate;

@end
