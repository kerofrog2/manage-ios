    //
//  SyncToodledoViewController.m
//  Manage
//
//  Created by Cliff Viegas on 9/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "SynchronizationViewController.h"

// View Controllers
#import "NameAndPasswordViewController.h"

// Data Models
#import "ApplicationSetting.h"

// Data Collections
#import "TaskListCollection.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#define kTableViewFrame         CGRectMake(0, 0, 320, 318)
#define kTextFieldTag           38429
#define kTextFieldFrame         CGRectMake(13, 12, 277, 23)
#define kToodledoAlertViewTag   46338

@interface SynchronizationViewController()

@end


@implementation SynchronizationViewController

@synthesize delegate;
@synthesize refPopoverController;

@synthesize toodledoServiceStatus;
@synthesize toodledoErrorMessage;
@synthesize getAccountInfoOnceFlag;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[self.toodledoServiceStatus release];
	//[self.toodledoErrorMessage release];
	
//	[myTableView release];
//	[toodledoAccountInfo release];
//	toodledoAccountInfo = nil;
//    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithMasterTaskListCollection:(TaskListCollection *)theTaskListCollection {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_SynchronizationViewController;
#endif
		// Set the title
		self.title = @"Sync";
		
		// Init toodledo account info and authentication to nil
		toodledoAccountInfo = nil;
		toodledoAuthentication = nil;
		
		// Set status to connecting if we just came in
		if ([[ApplicationSetting getSettingDataForName:@"ToodledoServiceStatus"] isEqualToString:@"Not Connected"] == FALSE) {
			[ApplicationSetting updateSettingName:@"ToodledoServiceStatus" withData:@"Connecting..."];
		}
		
		// Assign the passed task list collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Get the current service status
		self.toodledoServiceStatus = @"";
	
		// Init the toodledo error message
		self.toodledoErrorMessage = @"";
		
		// Need to initialise the get account info once flag
		self.getAccountInfoOnceFlag = FALSE;
		
		// Init the toodledo account info
		toodledoAccountInfo = [[ToodledoAccountInfo alloc] init];
		toodledoAccountInfo.delegate = self;
		toodledoAccountInfo.toodledoKey = @"";
		
		// Load the tableview
		[self loadTableView];
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kSyncPopoverSize];
		
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];// autorelease];

	}
	
	// Default values
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.textLabel.text = @"";
	cell.detailTextLabel.text = @"";
	//cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// Set up the cell...
	if (indexPath.section == 0) {
		if (indexPath.row == 0) {
			cell.textLabel.text = @"Toodledo Service";
			cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
			cell.detailTextLabel.text = self.toodledoServiceStatus;
			
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		} 
	} else if (indexPath.section == 1) {
        if ([self.toodledoServiceStatus isEqualToString:@"Connected"]) {
            cell.textLabel.text = @"Sync with Toodledo";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        } else {
            cell.textLabel.text = @"Get a free Toodledo account";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
		
		// Also do a part-sync or update-sync
	}
	
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Assign service status
	self.toodledoServiceStatus = [ApplicationSetting getSettingDataForName:@"ToodledoServiceStatus"];
	
	// This will depend on whether or not we are connected
	/*NSInteger sectionCount = 1;
	if ([self.toodledoServiceStatus isEqualToString:@"Connected"]) {
		sectionCount = 2;
	}
	
	return sectionCount;*/
    return 2;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 1;
	} else if (section == 1) {
		return 1;
	}
	
	return 1;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	[myTableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if (indexPath.row == 0 && indexPath.section == 0) {
		NameAndPasswordViewController *controller = [[NameAndPasswordViewController alloc] init] ;//autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if (indexPath.row == 0 && indexPath.section == 1) {
        if ([self.toodledoServiceStatus isEqualToString:@"Connected"]) {
#if ENABLE_GOOGLE_ANALYTICS
            // track this event to Google Analytics
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker sendEventWithCategory:kCategory_event
                                withAction:kAction_sync_file
                                 withLabel:kLabel_sync_toodledo
                                 withValue:0];
#endif
            ToodledoSyncViewController *controller = [[ToodledoSyncViewController alloc] initWithTaskListCollection:refMasterTaskListCollection
                                                                                              andToodledoAccountInfo:toodledoAccountInfo] ;//autorelease];
            controller.delegate = self;
            [self.navigationController pushViewController:controller animated:YES];
        } else {
            // Connect to toodledo sign up... or no, call alert view
            UIAlertView *toodledoSignUpAlertView = [[UIAlertView alloc] initWithTitle:@"Toodledo Sign Up" 
                                                                              message:@"Manage will move into the background and a Safari browser window will open allowing you to create a new Toodledo account." 
                                                                             delegate:self 
                                                                    cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            toodledoSignUpAlertView.tag = kToodledoAlertViewTag;
            [toodledoSignUpAlertView show];
//            [toodledoSignUpAlertView release];
        }
		
	}
	
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	if (section == 0 && [self.toodledoErrorMessage length] > 0) {
		return self.toodledoErrorMessage;
	}
	
	return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (section == 0) {
		return @"Account Details";
	} else if (section == 1) {
		return @"Toodledo";
	}
	
	return @"";
}

#pragma mark -
#pragma mark ToodledoSyncViewController Delegates

- (void)settingsPopoverCanClose:(BOOL)canClose {
	[self.delegate settingsPopoverCanClose:canClose];
}

- (void)closeSettingsPopover {
	[self.delegate closeSettingsPopover];
}

- (void)replaceLocalTasksSyncComplete {
	[self.delegate replaceLocalTasksSyncComplete];
}

- (void)replaceToodledoDataComplete {
    [self.delegate replaceToodledoDataComplete];
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
	[myTableView reloadData];
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	
	if (toodledoAccountInfo) {
		[toodledoAccountInfo cancelConnection];
	}
	
	if (toodledoAuthentication) {
		[toodledoAuthentication cancelConnection];
	}
	
	[super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
	if ([self.toodledoServiceStatus isEqualToString:@"Not Connected"] == FALSE) {
		// Get the key
		NSString *key = [ApplicationSetting getSettingDataForName:@"ToodledoKey"];
		
		// Assign the key
		toodledoAccountInfo.toodledoKey = key;
		
		// Attempt to retrieve account info
		[toodledoAccountInfo connectToAccount];
	}
	[super viewDidAppear:animated];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ([alertView tag] == kToodledoAlertViewTag) {
        if (buttonIndex == 0) {
            // Cancel pressed - do nothing

        } else if (buttonIndex == 1) {
#if ENABLE_GOOGLE_ANALYTICS
            // track this event to Google Analytics
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker sendEventWithCategory:kCategory_event
                                withAction:kAction_sync_file
                                 withLabel:kLabel_sync_create_toodledo
                                 withValue:0];
#endif
            NSURL *url = [NSURL URLWithString:@"http://www.toodledo.com/signup.php"];
            
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}

#pragma mark -
#pragma mark ToodledoAccountInfo Delegates

- (void)accountInfoSucceeded {
	// Account info connected successfully.
	
	// Also display the time context or folder was last edited
	[ApplicationSetting updateSettingName:@"ToodledoServiceStatus" withData:@"Connected"];
	self.toodledoServiceStatus = @"Connected";
	[myTableView reloadData];
}

- (void)accountInfoFailedWithErrorCode:(NSInteger)errorCode andErrorDescription:(NSString *)errorDesc {
	
	if (errorCode == 2 && self.getAccountInfoOnceFlag == FALSE) {
		
		NSString *userID = [ApplicationSetting getSettingDataForName:@"ToodledoUserID"];
		
		toodledoAuthentication = [[ToodledoAuthentication alloc] initWithDelegate:self];
		[toodledoAuthentication assignDetail];		// Set the detail
		[toodledoAuthentication authenticateWithUserID:userID];
		
//		[toodledoAuthentication release];
		toodledoAuthentication = nil;
		
		self.getAccountInfoOnceFlag = TRUE;
	} else {
		// Update the connected status
		[ApplicationSetting updateSettingName:@"ToodledoServiceStatus" withData:@"Not Connected"];
		
		// Set the error message
		self.toodledoErrorMessage = errorDesc;
		
		[myTableView reloadData];
	}

}

- (void)accountInfoConnectionFailedWithError:(NSError *)error {
	// Update the connected status
	[ApplicationSetting updateSettingName:@"ToodledoServiceStatus" withData:@"Not Connected"];
	
	self.toodledoErrorMessage = [error localizedDescription];
	
	// Set the error message
	//self.toodledoErrorMessage = @"Error occurred when attempting to retrieve account info, please try updating your details";
	
	[myTableView reloadData];
}

#pragma mark -
#pragma mark Toodledo Authentication Delegates

- (void)authenticationSucceeded {
	// All good, can now load the account info properly
	// Get the key
	NSString *toodledoKey = [ApplicationSetting getSettingDataForName:@"ToodledoKey"];
	
	// Assign the key
	toodledoAccountInfo.toodledoKey = toodledoKey;
	
	// Attempt to connect a second time
	[toodledoAccountInfo connectToAccount];
}

- (void)authenticationFailedWithErrorCode:(NSInteger)errorCode andErrorDescription:(NSString *)errorDesc {
	// Update the connected status
	[ApplicationSetting updateSettingName:@"ToodledoServiceStatus" withData:@"Not Connected"];
	
	// Set the error message
	self.toodledoErrorMessage = errorDesc;
	
	[myTableView reloadData];
}

- (void)authenticationConnectionFailedWithError:(NSError *)error {
	// Update the connected status
	[ApplicationSetting updateSettingName:@"ToodledoServiceStatus" withData:@"Not Connected"];
	
	// Set the error message
	//self.toodledoErrorMessage = @"Connection error occurred, please try again later";
	self.toodledoErrorMessage = [error localizedDescription];
	
	[myTableView reloadData];
}

#pragma mark -
#pragma mark Helper Methods

- (UITextField *)getTextFieldForIndexPath:(NSIndexPath *)indexPath {
	UITextField *textField = [[UITextField alloc] initWithFrame:kTextFieldFrame];// autorelease];
	
	if (indexPath.section == 0) {
		[textField becomeFirstResponder];
	}
	
	[textField setBackgroundColor:[UIColor clearColor]];
	//[textField setDelegate:self];
	[textField setText:@""];
	[textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[textField setAutocorrectionType:UITextAutocorrectionTypeNo];
	[textField setTag:kTextFieldTag];
	[textField setFont:[UIFont fontWithName:@"Helvetica" size:15.0]];
	//[textField setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
	[textField setTextColor:[UIColor darkTextColor]];
	[textField setReturnKeyType:UIReturnKeyDone];
	//[textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
	
	return textField;
}



@end
