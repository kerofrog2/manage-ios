//
//  AutoSyncViewController.m
//  Manage
//
//  Created by Cliff Viegas on 24/04/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "AutoSyncViewController.h"

// View Controllers
#import "NameAndPasswordViewController.h"

// Data Models
#import "ApplicationSetting.h"

// Data Collections
#import "TaskListCollection.h"

// Method Helper
#import "MethodHelper.h"

@implementation AutoSyncViewController

#pragma mark -
#pragma mark Initialisation

- (id)initWithMasterTaskListCollection:(TaskListCollection *)theTaskListCollection {
	if ((self = [super init])) {
		// Set the title
		self.title = @"Auto Sync";
		
		// Init toodledo account info and authentication to nil
		toodledoAccountInfo = nil;
		toodledoAuthentication = nil;
		
		// Set status to connecting if we just came in
		if ([[ApplicationSetting getSettingDataForName:@"ToodledoServiceStatus"] isEqualToString:@"Not Connected"] == FALSE) {
			[ApplicationSetting updateSettingName:@"ToodledoServiceStatus" withData:@"Connecting..."];
		}
		
		// Assign the passed task list collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Get the current service status
		self.toodledoServiceStatus = @"";
        
		// Init the toodledo error message
		self.toodledoErrorMessage = @"";
		
		// Need to initialise the get account info once flag
		self.getAccountInfoOnceFlag = FALSE;
		
		// Init the toodledo account info
		toodledoAccountInfo = [[ToodledoAccountInfo alloc] init];
		toodledoAccountInfo.delegate = self;
		toodledoAccountInfo.toodledoKey = @"";
		
		// Load the tableview
		[self loadTableView];
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kSyncPopoverSize];
		
	}
	return self;
}


#pragma mark -
#pragma mark SyncToodledoNow Delegates

- (void)syncToodledoNowComplete {
    [self.delegate updatesToDataComplete];
}

- (void)closeSettingsPopover {
    [self.delegate closeAutoSyncPopover];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];// autorelease];
	}
	
	// Default values
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.textLabel.text = @"";
	cell.detailTextLabel.text = @"";
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// Set up the cell...
	if (indexPath.section == 0) {
		if (indexPath.row == 0) {
			cell.textLabel.text = @"Toodledo Service";
			cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
			cell.detailTextLabel.text = self.toodledoServiceStatus;

		} 
	}
	
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {	
	return 1;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	if (section == 0 && [self.toodledoErrorMessage length] > 0) {
        NSString *errorMessage = [NSString stringWithFormat:@"Error: %@\n\nPlease check your Toodledo account details in (Settings -> Synchronize), otherwise you can turn off Auto-Sync in (Settings -> Sync Settings).\n\nTap outside of this popover to close it.",
                                  self.toodledoErrorMessage];
		return errorMessage;
	}
	
	return @"";
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark -
#pragma mark ToodledoAccountInfo Delegates

- (void)accountInfoSucceeded {
    // Make sure this cant close
    [self.delegate autoSyncPopoverCanClose:FALSE];
    
	// Account info connected successfully.
	[super accountInfoSucceeded];
    
    SyncToodledoNowViewController *controller = [[SyncToodledoNowViewController alloc]
                                                  initWithTaskListCollection:refMasterTaskListCollection 
                                                  andToodledoAccountInfo:toodledoAccountInfo];// autorelease];
    controller.delegate = self;
    [controller setSyncNowTitle:@"Auto Sync"];
    [self.navigationController pushViewController:controller animated:NO];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}


@end
