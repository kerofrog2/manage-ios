//
//  AutoSyncViewController.h
//  Manage
//
//  Created by Cliff Viegas on 24/04/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// View Controllers
#import "SyncToodledoNowViewController.h"

// Master class
#import "SynchronizationViewController.h"

@protocol AutoSyncDelegate
- (void)updatesToDataComplete;
- (void)closeAutoSyncPopover;
- (void)autoSyncPopoverCanClose:(BOOL)canClose;
@end

@interface AutoSyncViewController : SynchronizationViewController <SyncToodledoNowDelegate> {
    
}

@end
