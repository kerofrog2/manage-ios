    //
//  NameAndPasswordViewController.m
//  Manage
//
//  Created by Cliff Viegas on 9/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "NameAndPasswordViewController.h"

// Keychain Access Wrapper
#import "KeychainWrapper.h"

// Data Models
#import "ApplicationSetting.h"

// Toodledo Authentication
//#import "ToodledoAuthentication.h"

// Constants
#define kTableViewFrame				CGRectMake(0, 0, 320, 318)
#define kPopoverSize				CGSizeMake(320, 318)
#define kTextFieldTag				19638
#define kTextFieldFrame				CGRectMake(11, 12, 283, 23)
#define kToodledoDetail				@"ToodledoManageDetail"
#define kToodledoUserName			@"ToodledoUserName"

#define kConnectingLabelFrame		CGRectMake(100, 140, 150, 21)
#define kActivityIndicatorViewFrame	CGRectMake(80, 140, 20, 21)
#define kConnectingTextViewFrame	CGRectMake(10, 140, 300, 200)

@interface NameAndPasswordViewController()
- (void)loadConnectingViews;
- (void)loadTableView;
- (UITextField *)getTextFieldForIndexPath:(NSIndexPath *)indexPath;
@end


@implementation NameAndPasswordViewController

@synthesize toodledoDetail;
@synthesize toodledoUserName;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[myTableView release];
//	[toodledoUserNameAppSetting release];
//	
//	[connectingLabel release];
//	[connectingActivityIndicatorView release];
//	[connectingTextView release];
//    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_NameAndPasswordViewController;
#endif
		// Set the title (will change depending on passed title)
		self.title = @"Toodledo Service";
		
		// Load the toodledo user name app settings
		toodledoUserNameAppSetting = [[ApplicationSetting alloc] init];
		[toodledoUserNameAppSetting loadApplicationSettingWithName:@"ToodledoUserName"];
		self.toodledoUserName = toodledoUserNameAppSetting.data;
		
		// Load the keychain wrapper
		keychainWrapper = [[KeychainWrapper alloc] init];
		
		NSData *data = [keychainWrapper searchKeychainCopyMatching:@"ToodledoManageDetail"];
		NSString *content = [[NSString alloc] initWithBytes:[data bytes] 
													 length:[data length] 
												   encoding:NSUTF8StringEncoding];
		
		if ([content length] > 0) {
			self.toodledoDetail = content;
		} else {
			self.toodledoDetail = @"";
		}
		
		//[data release];
//		[content release];
		
		// Load the tableview
		[self loadTableView];
		
		// Load the connecting views
		[self loadConnectingViews];
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];

		// Set the bar button item
		UIBarButtonItem *saveBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave 
																						   target:self 
																						   action:@selector(saveBarButtonItemAction)];
		// Set enabled to false
		[saveBarButtonItem setStyle:UIBarButtonItemStyleBordered];
		
		[self.navigationItem setRightBarButtonItem:saveBarButtonItem animated:YES];
//        [saveBarButtonItem release];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadConnectingViews {
	connectingLabel = [[UILabel alloc] initWithFrame:kConnectingLabelFrame];
	connectingLabel.text = @"Authenticating...";
	connectingLabel.textAlignment = UITextAlignmentCenter;
	connectingLabel.backgroundColor = [UIColor clearColor];
	// Initially this label is hidden
	connectingLabel.hidden = YES;
	[self.view addSubview:connectingLabel];
	
	connectingActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[connectingActivityIndicatorView setFrame:kActivityIndicatorViewFrame];
	//[connectingActivityIndicatorView startAnimating];
	// Initially the indicator view is hidden
	//connectingActivityIndicatorView.hidden = YES;
	[connectingActivityIndicatorView setHidesWhenStopped:YES];
	[self.view addSubview:connectingActivityIndicatorView];
	
	connectingTextView = [[UITextView alloc] initWithFrame:kConnectingTextViewFrame];
	[connectingTextView setBackgroundColor:[UIColor clearColor]];
	[connectingTextView setEditable:NO];
	[connectingTextView setFont:[UIFont fontWithName:@"Helvetica" size:15.0]];
	[connectingTextView setScrollEnabled:NO];
	[connectingTextView setShowsVerticalScrollIndicator:NO];
	[connectingTextView setHidden:YES];
	[connectingTextView setTextAlignment:NSTextAlignmentCenter];
	[self.view addSubview:connectingTextView];
}

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}


#pragma mark -
#pragma mark Action Methods

- (void)saveBarButtonItemAction {
#if ENABLE_GOOGLE_ANALYTICS
    // track this event to Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker sendEventWithCategory:kCategory_event
                        withAction:kAction_sync_file
                         withLabel:kLabel_login_toodledo
                         withValue:0];
#endif
	// Setup connecting views
	[connectingTextView setHidden:YES];
	[connectingLabel setHidden:NO];
	[connectingActivityIndicatorView startAnimating];
	
	// Need to update the connected setting in db
	ApplicationSetting *connectedSetting = [[ApplicationSetting alloc] init];
	connectedSetting.name = @"ToodledoServiceStatus";
	connectedSetting.data = @"Not Connected";
	[connectedSetting updateDatabase];
//	[connectedSetting release];
	
	// First save username to settings table and save password to correct section in keychain
	ApplicationSetting *appSetting = [[ApplicationSetting alloc] init];
	appSetting.name = kToodledoUserName;
	
	NSIndexPath *userNameIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
	UITableViewCell	*userNameCell = [myTableView cellForRowAtIndexPath:userNameIndexPath];
	UITextField *myUserNameTextField = (UITextField *)[userNameCell.contentView viewWithTag:kTextFieldTag];
	appSetting.data = [myUserNameTextField text];
	
	self.toodledoUserName = appSetting.data;
	
	[appSetting updateDatabase];
//	[appSetting release];
	
	
	// Save detail data
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
	UITableViewCell	*cell = [myTableView cellForRowAtIndexPath:indexPath];
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	
	self.toodledoDetail = [myTextField text];
	
	
	// Store in keychain
	[keychainWrapper deleteKeychainValue:@"ToodledoManageDetail"];
	[keychainWrapper createKeychainValue:self.toodledoDetail forIdentifier:@"ToodledoManageDetail"];
	
	// Authenticate with toodledo
	ToodledoAuthentication *toodledo = [[ToodledoAuthentication alloc] initWithDelegate:self];
	[toodledo authenticateWithAccount:self.toodledoUserName andDetail:self.toodledoDetail];
//	[toodledo release];
	
	
	/*ApplicationSetting *serviceStatus = [[ApplicationSetting alloc] init];
	[serviceStatus loadApplicationSettingWithName:@"ToodledoServiceStatus"];
	
	// If connection is succesful
	serviceStatus.data = @"Connected";
	[serviceStatus updateDatabase];
	[serviceStatus release];*/
}

#pragma mark -
#pragma mark ToodledoAuthentication Delegates 

- (void)authenticationSucceeded {
	// Need to update the connected setting in db
	ApplicationSetting *connectedSetting = [[ApplicationSetting alloc] init];
	connectedSetting.name = @"ToodledoServiceStatus";
	connectedSetting.data = @"Connected";
	[connectedSetting updateDatabase];
//	[connectedSetting release];	
	
	// Stop any authentication activity animating (which will also hide it)
	[connectingActivityIndicatorView stopAnimating];
	
	// Hide the connecting label
	[connectingLabel setHidden:YES];
	
	// Hide the text view
	[connectingTextView setHidden:YES];
	
	// YES, succeeded :)
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)authenticationFailedWithErrorCode:(NSInteger)errorCode andErrorDescription:(NSString *)errorDesc {
	// Stop any authentication activity animating (which will also hide it)
	[connectingActivityIndicatorView stopAnimating];
	
	// Hide the connecting label
	[connectingLabel setHidden:YES];
	
	// Show the text view and set text to be error description
	[connectingTextView setText:errorDesc];
	[connectingTextView setHidden:NO];
}

- (void)authenticationConnectionFailedWithError:(NSError *)error {
	// Stop any authentication activity animating (which will also hide it)
	[connectingActivityIndicatorView stopAnimating];

	// Hide the connecting label
	[connectingLabel setHidden:YES];
	
	// Show the text view and set text to be error description
	[connectingTextView setText:[NSString stringWithFormat:@"Connection failed: %@", [error localizedDescription]]];
	[connectingTextView setHidden:NO];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];// autorelease];
		
		[cell.contentView addSubview:[self getTextFieldForIndexPath:indexPath]];
	}
	
	// Get content view objects
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	
	// Default values
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.textLabel.text = @"";
	cell.detailTextLabel.text = @"";
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	// Dark blue colour
	//cell.textLabel.textColor = [UIColor colorWithRed:0.0245 green:0.0168 blue:0.568 alpha:1.0];
	
	[myTextField setTextColor:[UIColor colorWithRed:0.0245 green:0.0468 blue:0.398 alpha:1.0]];
	
	// Set up the cell...
	if (indexPath.row == 0) {
		[myTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
		[myTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
		[myTextField setPlaceholder:@"email@example.com"];
		if ([toodledoUserNameAppSetting.data length] > 0) {
			[myTextField setText:toodledoUserNameAppSetting.data];
		}
	} else if (indexPath.row == 1) {
		[myTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
		[myTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
		[myTextField setSecureTextEntry:YES];
		[myTextField setPlaceholder:@"password"];

		[myTextField setText:self.toodledoDetail];
	}
	
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return 2;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// We don't want any row to be selectable (yet)
	return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {	
	return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return @"Account Details";
}


#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark Helper Methods

- (UITextField *)getTextFieldForIndexPath:(NSIndexPath *)indexPath {
	UITextField *textField = [[UITextField alloc] initWithFrame:kTextFieldFrame];// autorelease];
	
	if (indexPath.section == 0 && indexPath.row == 0) {
		[textField becomeFirstResponder];
	}
	
	[textField setBackgroundColor:[UIColor clearColor]];
	//[textField setDelegate:self];
	[textField setText:@""];
	[textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[textField setAutocorrectionType:UITextAutocorrectionTypeNo];
	[textField setTag:kTextFieldTag];
	[textField setFont:[UIFont fontWithName:@"Helvetica" size:15.0]];
	[textField setClearButtonMode:UITextFieldViewModeWhileEditing];
	//[textField setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
	[textField setTextColor:[UIColor darkTextColor]];
	[textField setReturnKeyType:UIReturnKeyDone];
	//[textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
	
	return textField;
}


@end
