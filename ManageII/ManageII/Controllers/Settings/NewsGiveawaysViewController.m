//
//  NewsGiveawaysViewController.m
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 4/09/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "NewsGiveawaysViewController.h"

#define kPopoverSize		CGSizeMake(320, 318)
#define kWebviewFrame       CGRectMake(0, 0, 320, 318)

@implementation NewsGiveawaysViewController

#pragma mark - Memory Management

- (void)dealloc {
//    [myWebView release];
//    [super dealloc];
}

#pragma mark - Initialisation

- (id)init {
    self = [super init];
    if (self) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_NewsGiveawaysViewController;
#endif
        self.title = @"News & Giveaways";
        
        [self setContentSizeForViewInPopover:kPopoverSize];

        myWebView = [[UIWebView alloc] initWithFrame:kWebviewFrame];
        
        NSString *urlAddress = @"http://manage.kerofrog.com.au/newsgiveaways.html";
        
        //Create a URL object.
        NSURL *url = [NSURL URLWithString:urlAddress];
        
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        //Load the request in the UIWebView.
        [myWebView loadRequest:requestObj];
        
        [self.view addSubview:myWebView];
    }
    
    return self;
}

#pragma mark - UIWebViewDelegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView {
    UIActivityIndicatorView *actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIBarButtonItem *actItem = [[UIBarButtonItem alloc] initWithCustomView:actInd];
    
    [self.navigationItem setRightBarButtonItem:actItem animated:NO];
    
    [actInd startAnimating];
//    [actInd release];
//    [actItem release];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    // Show local html page
}


#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
