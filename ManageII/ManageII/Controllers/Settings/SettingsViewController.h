//
//  SettingsViewController.h
//  Manage
//
//  Created by Cliff Viegas on 6/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class ApplicationSettingCollection;
@class TagCollection;
@class TaskListCollection;

// For enums
#import "PropertyCollection.h"

// View Controllers (for delegates)
#import "SynchronizationViewController.h"
#import "ListSettingViewController.h"
//#import "DataBackupViewController.h"
#import "GADBannerViewDelegate.h"

// Constants
#import "MainConstants.h"

// Message Library
#import <MessageUI/MFMailComposeViewController.h>

#import "DataBackupViewController.h"

#import "ManageTagsViewController.h"

@class GADBannerView, GADRequest;

#define kNumberAdMob                1

// Delegate
@protocol SettingsDelegate
- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData;
- (void)settingsPopoverCanClose:(BOOL)canClose;
- (void)closeSettingsPopover;
- (void)updatesToDataComplete;
- (void)replaceToodledoDataComplete;
- (void)shopfrontSelected;
- (void)reloadAllTagsForTaskList;
@end

@interface SettingsViewController : GAITrackedViewController <SynchronizationDelegate, ListSettingDelegate, UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate, MFMailComposeViewControllerDelegate, DataBackupDelegate, ManageTagsViewControllerDelegate> {
//	id <SettingsDelegate>			delegate;
	
	ApplicationSettingCollection	*applicationSettingCollection;
	PropertyCollection				*propertyCollection;
	
	UITableView						*myTableView;
	TagCollection					*refTagCollection;
	
	TaskListCollection				*refMasterTaskListCollection;
	
//	UIPopoverController				*refPopoverController;
}

//@property (nonatomic, assign)	id						delegate;
@property (nonatomic, assign)	UIPopoverController		*refPopoverController;
@property (nonatomic, retain)   GADBannerView           *adBanner;
@property (nonatomic, assign)	NSObject<SettingsDelegate>			*delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(NSObject<SettingsDelegate>*)theDelegate andTagCollection:(TagCollection *)theTagCollection
			andPropertyCollectionType:(NSInteger)propertyCollectionType 
				andTaskListCollection:(TaskListCollection *)theTaskListCollection;
@end
