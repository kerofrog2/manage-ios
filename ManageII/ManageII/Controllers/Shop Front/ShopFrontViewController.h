//
//  ShopFrontViewController.h
//  Manage
//
//  Created by Cliff Viegas on 19/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Shop front table view cell
#import "ShopFrontTableViewCell.h"

@protocol ShopFrontDelegate 
- (void)previewTheme:(NSString *)themeName;
- (void)purchasedTheme:(NSString *)themeName;
@end

@interface ShopFrontViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, ShopFrontTableViewCellDelegate> {
    UITableView     *myTableView;
    NSArray         *titlesArray;
    NSArray         *imagesArray;
    NSString        *testString;
//    id <ShopFrontDelegate> delegate;
    UIActivityIndicatorView *activityIndicatorView;
}

@property (nonatomic)   NSObject <ShopFrontDelegate>  *delegate;

#pragma mark Initialisation
- (id)initWithSize:(CGSize)theSize;


#pragma mark - Class Methods
- (NSString *)getImageNameForProduct:(NSString *)productID;

@end
