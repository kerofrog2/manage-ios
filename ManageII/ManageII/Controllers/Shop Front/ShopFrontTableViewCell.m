//
//  ShopFrontTableViewCell.m
//  Manage
//
//  Created by Cliff Viegas on 20/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ShopFrontTableViewCell.h"

CGRect const kItemScreenshotFrame = {5, 5, 204, 192};
CGRect const kItemTypeLabelFrame = {212, 27, 145, 17};
CGRect const kItemTitleLabelFrame = {212, 41, 145, 21};
CGRect const kItemPriceLabelFrame = {212, 5, 145, 21};
CGRect const kItemDescriptionLabelFrame = {212,61, 145, 100};
CGRect const kItemPreviewButtonFrame = {212, 172, 70, 25};
CGRect const kItemPurchaseButtonFrame = {287, 172, 70, 25};

@implementation ShopFrontTableViewCell

@synthesize itemScreenshot;
@synthesize itemTypeLabel;
@synthesize itemTitleLabel;
@synthesize itemPriceLabel;
@synthesize itemDescriptionLabel;
@synthesize itemPreviewButton;
@synthesize itemPurchaseButton;
@synthesize itemProductID;
@synthesize delegate;

#pragma mark - Memory Management

- (void)dealloc {
    //[self.itemScreenshot release];
    //[self.itemTypeLabel release];
    //[self.itemPriceLabel release];
    //[self.itemDescriptionLabel release];
    //[self.itemPreviewButton release];
    //[self.itemPurchaseButton release];
    //[self.itemProductID release];
//    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.itemScreenshot = [[UIImageView alloc] initWithFrame:kItemScreenshotFrame];// autorelease];
        [self addSubview:itemScreenshot];
        
        self.itemProductID = @"";
        
        self.itemTypeLabel = [[UILabel alloc] initWithFrame:kItemTypeLabelFrame];// autorelease];
        [self.itemTypeLabel setBackgroundColor:[UIColor clearColor]];
        [self.itemTypeLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0]];
        [self addSubview:self.itemTypeLabel];
        
        self.itemTitleLabel = [[UILabel alloc] initWithFrame:kItemTitleLabelFrame];// autorelease];
        [self.itemTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.itemTitleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
        [self addSubview:self.itemTitleLabel];
        
        self.itemPriceLabel = [[UILabel alloc] initWithFrame:kItemPriceLabelFrame];// autorelease];
        [self.itemPriceLabel setBackgroundColor:[UIColor clearColor]];
        [self.itemPriceLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19.0]];
        [self addSubview:self.itemPriceLabel];
        
        self.itemDescriptionLabel = [[UILabel alloc] initWithFrame:kItemDescriptionLabelFrame];// autorelease];
        [self.itemDescriptionLabel setBackgroundColor:[UIColor clearColor]];
        [self.itemDescriptionLabel setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
        [self.itemDescriptionLabel setNumberOfLines:0];
        [self.itemDescriptionLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [self addSubview:self.itemDescriptionLabel];
        
        self.itemPreviewButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];// retain];// autorelease];
        [self.itemPreviewButton setTitle:@"Preview" forState:UIControlStateNormal];
        [self.itemPreviewButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
        [self.itemPreviewButton setFrame:kItemPreviewButtonFrame];
        [self.itemPreviewButton addTarget:self action:@selector(itemPreviewButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.itemPreviewButton];
        
        self.itemPurchaseButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];// retain] autorelease];
        [self.itemPurchaseButton setTitle:@"Buy" forState:UIControlStateNormal];
        [self.itemPurchaseButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
        [self.itemPurchaseButton setFrame:kItemPurchaseButtonFrame];
        [self.itemPurchaseButton addTarget:self action:@selector(itemPurchaseButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.itemPurchaseButton];
    }
    return self;
}

#pragma mark - Button Events

- (void)itemPreviewButtonAction {
    if ([delegate respondsToSelector:@selector(previewButtonPressed:)]) {
        [self.delegate previewButtonPressed:self.itemProductID];
    }
}

- (void)itemPurchaseButtonAction {
    if ([delegate respondsToSelector:@selector(purchaseButtonPressed:)]) {
        [self.delegate purchaseButtonPressed:self.itemProductID];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
