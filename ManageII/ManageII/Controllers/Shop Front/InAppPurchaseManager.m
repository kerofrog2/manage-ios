//
//  InAppPurchaseManager.m
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 21/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "InAppPurchaseManager.h"

//static InAppPurchaseManager *_sharedPurchaseManager = nil;
static InAppPurchaseManager *sharedPurchaseManager = nil;


@implementation InAppPurchaseManager

@synthesize products;

#pragma mark - Memory Management

// Need this here without super dealloc call so this never deallocs (over-ride)
- (void)dealloc {
    
}


#pragma mark - Singleton Methods

+ (id)sharedPurchaseManager {
   // @synchronized(self) {
    if (sharedPurchaseManager == nil) {
            sharedPurchaseManager = [[super allocWithZone:NULL] init];
    }
    //}
    return sharedPurchaseManager;
}
+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedPurchaseManager];//retain
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}
//- (id)retain {
//    return self;
//}
//- (unsigned)retainCount {
//    return UINT_MAX; //denotes an object that cannot be released
//}
//
//
//- (id)autorelease {
//    return self;
//}
- (id)init {
    if ((self = [super init])) {
        products = [[NSMutableArray alloc] init];
    }
    return self;
}


/*
+ (InAppPurchaseManager *)sharedPurchaseManager {
    @synchronized([InAppPurchaseManager class]) {
        if (!_sharedPurchaseManager) {
            [[self alloc] init];
            return _sharedPurchaseManager;
        }
    }
    return nil;
}

+ (id)alloc {
    @synchronized([InAppPurchaseManager class]) {
        NSAssert(_sharedPurchaseManager == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedPurchaseManager = [super alloc];
		return _sharedPurchaseManager;
    }
    return nil;
}*/

#pragma mark - Public methods

//
// call this method once on startup
//
- (void)loadStore {
    // restarts any purchases if they were interrupted last time the app was open
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    // get the product description (defined in early sections)
    [self requestProUpgradeProductData];
}

//
// call this before making a purchase
//
- (BOOL)canMakePurchases {
    return [SKPaymentQueue canMakePayments];
}

//
// kick off the upgrade transaction
//
- (void)purchaseUpgrade:(NSString *)productID {
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:productID];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)requestProUpgradeProductData {
    NSSet *productIdentifiers = [NSSet setWithObjects:kInAppPurchasePremiumThemePurpleHazeID,
                                 kInAppPurchasePremiumThemeNinjaBunniesID,
                                 kInAppPurchasePremiumThemeNitroBurnID,
                                 kInAppPurchasePremiumThemeOldTimerID,
                                 kInAppPurchasePremiumThemeSweetCravingsID, nil];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
    
    
    // we will release the request object in the delegate callback
}

- (void)requestRemoveAdvertisingData {
    NSSet *productIdentifiers = [NSSet setWithObjects:kInAppPurchaseRemoveAdvertisingID, nil];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

#pragma mark - Purchase helpers

//
// saves a record of the transaction by storing the receipt to disk
//
- (void)recordTransaction:(SKPaymentTransaction *)transaction {
    // Ninja Bunnies
    if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchasePremiumThemeNinjaBunniesID]) {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:kInAppPurchasePremiumThemeNinjaBunniesReceipt];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchasePremiumThemeNitroBurnID]) {
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:kInAppPurchasePremiumThemeNitroBurnReceipt];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchasePremiumThemeSweetCravingsID]) {
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:kInAppPurchasePremiumThemeSweetCravingsReceipt];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchasePremiumThemePurpleHazeID]) {
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:kInAppPurchasePremiumThemePurpleHazeReceipt];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchasePremiumThemeOldTimerID]) {
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:kInAppPurchasePremiumThemeOldTimerReceipt];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchaseRemoveAdvertisingID]) {
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:kInAppPurchaseRemoveAdvertisingReceipt];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

// Return the theme name of the premium theme
- (NSString *)getPremiumThemeName:(NSString *)productID {
    NSString *premiumName = @"Nitro Burn";
    if ([productID isEqualToString:kInAppPurchasePremiumThemeNinjaBunniesID]) {
        premiumName = @"Ninja Bunnies";
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemeNitroBurnID]) {
        premiumName = @"Nitro Burn";
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemeSweetCravingsID]) {
        premiumName = @"Sweet Cravings";
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemePurpleHazeID]) {
        premiumName = @"Purple Haze";
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemeOldTimerID]) {
        premiumName = @"Old Timer";
    }
    
    return premiumName;
}

// To check whether the content has been purchased
- (BOOL)contentPurchased:(NSString *)productID {
    BOOL productPurchased = FALSE;
    if ([productID isEqualToString:kInAppPurchasePremiumThemeNinjaBunniesID]) {
        productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:kInAppPurchasePremiumThemeNinjaBunniesProvideContent];
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemeNitroBurnID]) {
        productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:kInAppPurchasePremiumThemeNitroBurnProvideContent];
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemeSweetCravingsID]) {
        productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:kInAppPurchasePremiumThemeSweetCravingsProvideContent];
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemePurpleHazeID]) {
        productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:kInAppPurchasePremiumThemePurpleHazeProvideContent];
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemeOldTimerID]) {
        productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:kInAppPurchasePremiumThemeOldTimerProvideContent];
    } else if ([productID isEqualToString:kInAppPurchaseRemoveAdvertisingID]) {
        productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:kInAppPurchaseRemoveAdvertisingProvideContent];
    }
    
    return productPurchased;
}

//
// enable pro features
//
- (void)provideContent:(NSString *)productId {
    if ([productId isEqualToString:kInAppPurchasePremiumThemeNinjaBunniesID]) {
        // enable the pro features
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kInAppPurchasePremiumThemeNinjaBunniesProvideContent];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([productId isEqualToString:kInAppPurchasePremiumThemeNitroBurnID]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kInAppPurchasePremiumThemeNitroBurnProvideContent];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([productId isEqualToString:kInAppPurchasePremiumThemeSweetCravingsID]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kInAppPurchasePremiumThemeSweetCravingsProvideContent];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([productId isEqualToString:kInAppPurchasePremiumThemePurpleHazeID]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kInAppPurchasePremiumThemePurpleHazeProvideContent];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([productId isEqualToString:kInAppPurchasePremiumThemeOldTimerID]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kInAppPurchasePremiumThemeOldTimerProvideContent];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([productId isEqualToString:kInAppPurchaseRemoveAdvertisingID]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kInAppPurchaseRemoveAdvertisingProvideContent];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//
// removes the transaction from the queue and posts a notification with the transaction result
//
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful {
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:transaction, @"transaction" , nil];
    if (wasSuccessful) {
        // send out a notification that we’ve finished the transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionSucceededNotification object:self userInfo:userInfo];
    } else {
        // send out a notification for the failed transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionFailedNotification object:self userInfo:userInfo];
    }
}

//
// called when the transaction was successful
//
- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    [self recordTransaction:transaction];
    [self provideContent:transaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has been restored and and successfully completed
//
- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    [self recordTransaction:transaction.originalTransaction];
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has failed
//
- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    if (transaction.error.code != SKErrorPaymentCancelled) {
        // error!
        [self finishTransaction:transaction wasSuccessful:NO];
    } else {
        // this is fine, the user just cancelled, so don’t notify
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
}

#pragma mark -
#pragma mark SKPaymentTransactionObserver methods

//
// called when the transaction status is updated
//
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState)  {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

#pragma mark - SKProductsRequestDelegate methods

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSArray *localProducts = response.products;
    
    // Remove any existing listed products
    [self.products removeAllObjects];
    
    // Release any existing products
    //[self.products addObjectsFromArray:localProducts];
    
    for (SKProduct *product in localProducts) {
        //NSLog(@"Product title: %@" , product.localizedTitle);
       // NSLog(@"Product description: %@" , product.localizedDescription);
        
        //NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        //[numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        //[numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        //[numberFormatter setLocale:product.priceLocale];
        //NSString *formattedString = [numberFormatter stringFromNumber:product.price];
        //NSLog(@"Product price: %@" , formattedString);
        
       // NSLog(@"Product id: %@" , product.productIdentifier);
        
        [self.products addObject:product];
    }

    
    for (NSString *invalidProductId in response.invalidProductIdentifiers) {
        NSLog(@"Invalid product id: %@" , invalidProductId);
    }
    
    // finally release the reqest we alloc/init’ed in requestProUpgradeProductData
    //[productsRequest cancel];r
    //productsRequest.delegate = nil;
    //[[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    //productsRequest = nil;
//    [productsRequest release];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerProductsFetchedNotification object:self userInfo:nil];
}


#pragma mark - Restore Transactions

- (void) checkPurchasedItems {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}// Call This Function

//Then this delegate Funtion Will be fired
- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    for (SKPaymentTransaction *transaction in queue.transactions)
    {
        [self restoreTransaction:transaction];
    }
    
}

@end
