//
//  NoteConstants.h
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 13/09/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

// Footer Lines
#define kFooterLine1Portrait                    CGRectMake(10, 819, 520, 1)//CGRectMake(68, 790, 634, 1)
#define kFooterLine1Landscape                   CGRectMake(10, 589, 760, 1)//CGRectMake(324, 530, 634, 1)

// Header and Border Lines
#define kNoteHeaderLine1Portrait				CGRectMake(10, 44, 520, 1)
#define kNoteHeaderLine1Landscape				CGRectMake(10, 44, 760, 1)

#define kNoteLeftBorderLinePortrait             CGRectMake(10, 44, 1, 776)//CGRectMake(68, 84, 1, 706)
#define kNoteLeftBorderLineLandscape            CGRectMake(10, 44, 1, 546)// CGRectMake(324, 84, 1, 446)

#define kNoteRightBorderLinePortrait            CGRectMake(530, 44, 1, 776)//CGRectMake(702, 84, 1, 706)
#define kNoteRightBorderLineLandscape           CGRectMake(770, 44, 1, 546)// CGRectMake(958, 84, 1, 446)

// Drawing scroll view
#define kDrawingScrollViewRectP                 CGRectMake(11, 44, 519, 775)
#define kDrawingScrollViewRectL                 CGRectMake(11, 44, 760, 545)

#define kDrawingAreaRect                        CGRectMake(0, 0, 760, 775)
//#define kDrawingAreaRectL                       CGRectMake(0, 0, 773, 544)

// Buttons
#define kNoteGreenPenButtonPortrait             CGRectMake(170, 7, 40, 37)//CGRectMake(290, 44, 40, 37)
#define kNoteGreenPenButtonLandscape            CGRectMake(270, 7, 40, 37)//CGRectMake(556, 44, 40, 37)

#define kNoteRedPenButtonPortrait               CGRectMake(217, 7, 40, 37)
#define kNoteRedPenButtonLandscape              CGRectMake(320, 7, 40, 37)//CGRectMake(606, 44, 40, 37)

#define kNoteBluePenButtonPortrait              CGRectMake(264, 7, 40, 37)
#define kNoteBluePenButtonLandscape             CGRectMake(370, 7, 40, 37)//CGRectMake(656, 44, 40, 37)

#define kNoteBlackPenButtonPortrait             CGRectMake(311, 7, 40, 37)
#define kNoteBlackPenButtonLandscape            CGRectMake(420, 7, 40, 37)//CGRectMake(706, 44, 40, 37)

// Eraser and Clear
#define kNoteEraserButtonPortrait               CGRectMake(10, 7, 40, 37)//CGRectMake(70, 44, 40, 37)
#define kNoteEraserButtonLandscape              CGRectMake(10, 7, 40, 37)//CGRectMake(326, 67, 40, 37)

#define kNoteClearButtonPortrait                CGRectMake(57, 7, 40, 37)//CGRectMake(120, 44, 40, 37)
#define kNoteClearButtonLandscape               CGRectMake(60, 7, 40, 37)//  CGRectMake(376, 44, 40, 37)

// Redo and Undo
#define kNoteRedoButtonPortrait                 CGRectMake(447, 7, 40, 37)//CGRectMake(612, 67, 40, 37)
#define kNoteRedoButtonLandscape                CGRectMake(702, 7, 40, 37)//552

#define kNoteUndoButtonPortrait                 CGRectMake(494, 7, 40, 37)//CGRectMake(662, 67, 40, 37)
#define kNoteUndoButtonLandscape                CGRectMake(752, 7, 40, 37)//CGRectMake(918, 44, 40, 37)//602

// Line widths
#define kLineWidthsButtonPortrait               CGRectMake(400, 7, 40, 37)
#define kLineWidthsButtonLandscape              CGRectMake(652, 7, 40, 37)//CGRectMake(818, 44, 40, 37)

// Scroll View
#define kNoteScrollViewPortrait                 CGRectMake(68, 25, 634, 705)
#define kNoteScrollViewLandscape				CGRectMake(324, 25, 634, 445)//CGRectMake(324, 85, 634, 445)

/*
 
 // Divide by 5 from 405
 CGRect _ScrollControlFrame = {440, 193, 35, 81};
 */

// Divide by 5 from 405
//CGRect _NoteScrollControlFrame = {965, 238, 35, 81};


// Divide by 5 from 705
#define kNoteScrollControllWindowFrame                    CGRectMake(770, 178, 35, 155)
#define kNoteScrollControlFrame                           CGRectMake(770, 178, 35, 109.2)

//P
#define kNoteHorizontalScrollControllWindowFrame          CGRectMake(15, 825, 152, 35)
#define kNoteHorizontalScrollControlFrame                 CGRectMake(15, 825, 103.8, 35)

//#define kSortItemsSegmentedControlPortrait	CGRectMake(222, 47, 312, 29)
//#define kSortItemsSegmentedControlLandscape	CGRectMake(478, 46, 312, 29)

// Middle point in portrait is 378
// Middle point in landscape is 634

// #define kPenButtonPortrait					CGRectMake(319, 865, 35, 30)
// #define kPenButtonLandscape                 CGRectMake(575, 613, 35, 30)

// #define kDoneEditingButtonPortrait			CGRectMake(652, 46, 48, 29)
// #define kDoneEditingButtonLandscape			CGRectMake(908, 46, 48, 29)