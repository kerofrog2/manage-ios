//
//  ListViewController.m
//  Manage
//
//  Created by Cliff Viegas on 2/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "ListViewController.h"

// Responders
//#import "ItemsDueTableViewResponder.h"

// Custom Objects
#import "CustomSegmentedControl.h"
#import "MainViewController.h"

// Data Collections
#import "TagCollection.h"
#import "ListItemTagCollection.h"
#import "TaskListCollection.h"
#import "AlarmCollection.h"

// Data Models
#import "TaskList.h"
#import "ListItem.h"
#import "ListItemTag.h"
#import "Tag.h"

// Custom UIObjects
#import "HiddenToolbar.h"
#import "MoveArray.h"
#import "Alarm.h"

// View Controllers
#import "EditListItemDueViewController.h"
#import "FilterListItemTagViewController.h"

// Method Helper
#import "MethodHelper.h"

#import "MainConstants.h"
#import "GAIConstants.h"

#import "ApplicationSetting.h"

#import "MAppDelegate.h"

#define kNotesIconTag               8385
#define kHelpOverlayTag             7638


@interface ListViewController()<UISplitViewControllerDelegate>{
    
    
}


// Private methods
- (void)loadSearchObjects;
- (void)loadTableView;
- (void)loadTextFields;
- (void)loadItemsDueTableView;
- (void)loadFilterTagObjects;
- (void)loadNavigationBarButtons;
- (void)loadButtons;
- (void)loadControlPanelObjects;
- (void)loadControlPanelButtonObjects;

- (void)filterTagsClearButtonAction;

- (void)deselectAllHighlighters;
- (void)deselectAllPens;

// Search object helpers
- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope;
- (void)hideSearchObjects:(BOOL)isHidden;

// Class Helper Methods
- (void)reloadTableView;
- (void)updateListCompletedFrameForOrientation:(UIInterfaceOrientation)theInterfaceOrientation;
- (void)setSelectRowPrompt:(BOOL)newSetting;
- (UIImageView *)getCellNotesIcon;

// Update Collections
- (void)updateCollectionsListTitle:(NSString *)newListTitle;
- (void)removeFilteredListItemsUsingListItem:(ListItem *)listItem;
- (void)updateTaskListCollection:(TaskListCollection *)taskListCollection withListItem:(ListItem *)listItem;
- (void)updateMasterCollectionWithListItem:(ListItem *)listItem;
//- (void)updateCollectionsWithListItem:(ListItem *)listItem;
- (void)reloadCollections;

// Tag related object methods
- (BOOL)isFilterTagsMode;
- (void)hideFilterTagObjects:(BOOL)isHidden;
- (void)filterTagObjectsOrientation:(UIInterfaceOrientation)theOrientation;

- (void)reloadTheme;
@end


@implementation ListViewController

@synthesize tileViewFrame;
@synthesize isSearchMode;
@synthesize miniCalendarView;
@synthesize backButtonTitle;
@synthesize searchRangeType;
@synthesize delegate;
@synthesize arrayTaskList;
@synthesize timer;
@synthesize refTaskList;
@synthesize currentListItem;
@synthesize completedTasksArray;
@synthesize completedRepeatTasksArray;
@synthesize isCompletingRepeatTask;
@synthesize currentRepeatListItem;
@synthesize oldRepeatListItem;
@synthesize currentTaskListIndex;
@synthesize myTableView;

#pragma mark -
#pragma mark Memory Managements


- (void)dealloc {
//#ifdef IS_FREE_BUILD
//    [_adView release];
//#endif
//    
//    [arrayTaskList release];
//    [currentListItem release];
//    [completedTasksArray release];
//    
//	// Search objects
//	[filteredSearchTaskListCollection release];
//	[mySearchBar release];
//	[searchScopeSegmentedControl release];
//	[searchButton release];
//	[clearSearchObjectsButton release];
//    [searchTitleLabel release];
//	[searchDateRangeLabel release];
//    [searchDateRangeButton release];
//    [showAllTasksButton release];
//    [searchListRangeButton release];
//    [timer release];
//    
//    
//    if (editListItemPopOverController != nil) {
//        editListItemPopOverController = nil;
//    }
//    
//    
//	// Pop over controllers
//	//[newListItemPopOverController release];
//	//[drawPadPopOverController release];
//	[highlightersPopoverController release];
//	//[filterTagsPopoverController release];
//	//[settingsPopoverController release];
//	//[dateLabelPopoverController release];
//	//[dateRangePopoverController release];
//    
//	// Backgrounds
//	[canvassImage release];
//	[borderImage release];
//	[pageImage release];
//	
//	// Images
//	[listCompletedStampImage release];
//	
//	// List Objects
//	[myTableView release];
//	[headerLine1 release];
//	[headerLine2 release];
//	[listOptionsButton release];
//	[listOptionsActionSheet release];
//	
//	// Items due tableview
//	[itemsDueTableView release];
//	[itemsDuePocket release];
//	[itemsDueTableViewResponder release];
//	
//	// Buttons
//	[dateLabelButton release];
//	[editListButton release];
//	[newListItemButton release];
//	[doneEditingButton release];
//	[newScribbleListItemButton release];
//	[sortItemsSegmentedControl release];
//	[settingsBarButtonItem release];
//	
//	// Control panel objects
//	[controlPanelImage release];
//    [controlPanelTypeSegmentedControl release];
//	[pensContainerImageView release];
//    [calendarListsDividerImageView release];
//	[highlightersContainerImageView release];
//    [drawingToolsContainerView release];
//    [calendarListToolsContainerView release];
//    [toolsContainerView release];
//    [miniListPadTableViewResponder release];
//    [miniListPadTableView release];
//    [dayCalendarDateLabel release];
//    [dayCalendarImageView release];
//	
//    
//    
//	// Tag related properties/objects
//	[filterListItemTagCollection release];
//	//[filterTagsFooterLine release];
//	//[filterTagsLabel release];
//	//[filterTagsClearButton release];
//	[filterTagsTaskListCollection release];
//	
//	// Actual highlighter and pen buttons
//	//UIButton						*greenPenButton;
//	//UIButton						*redPenButton;
//	//UIButton						*bluePenButton;
//	//UIButton						*blackPenButton;
//	
//	//UIButton						*yellowHighlighterButton;
//	//UIButton						*redHighlighterButton;
//	//UIButton						*whiteHighlighterButton;
//	
//	// Pen and Highlighter Icon Buttons
//	//[penButton release];
//	//[highlighterButton release];
//	[selectTaskImageView release];
//    
//    // Calendar
//    //[self.miniCalendarView release];
//    [searchStartDateRange release];
//    [searchEndDateRange release];
    
    // Back button title property
    //[self.backButtonTitle release];
    
    // Remove our observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	
//    [super dealloc];
}


#pragma mark -
#pragma mark Initialisation

- (void)loadListViewWithTaskList:(TaskList *)theList andTagCollection:(TagCollection *)theTagCollection
   andTaskListCollection:(TaskListCollection *)theTaskListCollection andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection andBackButtonTitle:(NSString *)theBackButtonTitle{
    
    // Assign the task list to local reference object
    currentListItem = [[ListItem alloc]init];
    refTaskList = theList;
    
    currentTaskListID = refTaskList.listID;
    self.completedTasksArray = [[NSMutableArray alloc]init];
    self.completedRepeatTasksArray = [[NSMutableArray alloc]init];
    
    // Set initial search range type
    self.searchRangeType = EnumSearchRangeTypeAllLists;
    
    // Assign the back button title
    self.backButtonTitle = theBackButtonTitle;
    
    // Assign master task list collection to local reference object
    refMasterTaskListCollection = theMasterTaskListCollection;
    refMasterTaskListCollection.currentTaskListID = currentTaskListID;
    
    // Assign to the normal task list collection
    refTaskListCollection = theTaskListCollection;
    refTaskListCollection.currentTaskListID = currentTaskListID;
    
    // Create an observer for whenever application becomes active
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // Set title
    self.title = refTaskList.title;
    
    // Init the select row prompt to false
    selectRowPrompt = FALSE;
    
    // Set the selected row to 0
    selectedRow = 0;
    
    // Init items due selected to false
    itemsDueSelected = FALSE;
    
    // Init tile view frame to nil
    self.tileViewFrame = CGRectZero;
    
    // Set settings view controller can close to true
    settingsViewControllerCanClose = TRUE;
    
    // Init the tag collection
    refTagCollection = theTagCollection;
    
    // Empty init the filter tags task list collection
    filterTagsTaskListCollection = [[TaskListCollection alloc] init];
    
    // Init the filter list item tag collection
    filterListItemTagCollection = [[ListItemTagCollection alloc] init];
    
    // Init edit list item controller at base level
    editListItemViewControllerAtBaseLevel = TRUE;
    
    // Init date range for start and end date in search
    searchStartDateRange = @"";
    searchEndDateRange = @"";
    
    // Init the list options action sheet
    listOptionsActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil
                                           destructiveButtonTitle:nil otherButtonTitles:@"Email Text List", @"Email PDF List", nil];
    
    // Load the canvass and page images
    //		NSString *canvasImageName = [refMasterTaskListCollection getImageNameForSection:@"Canvas"];
    //		canvassImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:canvasImageName]];
    //		pageImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PaperPreview.png"]];
    //
    //		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
    //			[canvassImage setFrame:kCanvassImagePortrait];
    //			[pageImage setFrame:kPageImagePortrait];
    //		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
    //			[pageImage setFrame:kPageImageLandscape];
    //			[canvassImage setFrame:kCanvassImageLandscape];
    //		}
    
    //		[self.view addSubview:canvassImage];
    //		[self.view addSubview:pageImage];
    
    
    // Load the navigation bar buttons
    [self loadNavigationBarButtons];
    
    // Load the filter tag objects
    [self loadFilterTagObjects];
    
    // Add list table view
    [self loadTableView];
    
    // Load the completed stamp image
    listCompletedStampImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CompletedStamp.png"]];
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        [listCompletedStampImage setFrame:kListCompletedStampPortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        [listCompletedStampImage setFrame:kListCompletedStampLandscape];
    }
    
    [listCompletedStampImage setHidden:YES];
    if ([refTaskList completed]) {
        [listCompletedStampImage setHidden:NO];
    }
    [listCompletedStampImage setAlpha:0.75f];
    [self.view addSubview:listCompletedStampImage];
    
    // Add text related fields
    [self loadTextFields];
    
    // Set initial highlighter selected value
    highlighterSelected = EnumHighlighterColorNone;
    
    // Load the frame/border
    //		NSString *borderImageName = [refMasterTaskListCollection getImageNameForSection:@"Border"];
    //		borderImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:borderImageName]];
    
    //		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
    //			[borderImage setFrame:kBorderImagePortrait];
    //		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
    //			[borderImage setFrame:kBorderImageLandscape];
    //		}
    //
    //		[self.view addSubview:borderImage];
    
    // Load the search objects
    [self loadSearchObjects];
    
    // Load the buttons
    [self loadButtons];
    
    // Load the items due table view and pocket
    //		[self loadItemsDueTableView];
    
    // Load the control panel objects
    //		[self loadControlPanelObjects];
    
    // Load the control panel button objects
    //		[self loadControlPanelButtonObjects];
    
    // Load the highlighter cover view
    //	highlighterCoverView = [[UIImageView alloc] init]; //WithImage:[UIImage imageNamed:@"ToolSelectedBlackoutPortrait.png"]];
    //	[highlighterCoverView setAlpha:0.0f];
    
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        //	[highlighterCoverView setImage:[UIImage imageNamed:@"ToolSelectedBlackoutPortrait.png"]];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        //	[highlighterCoverView setImage:[UIImage imageNamed:@"ToolSelectedBlackoutLandscape.png"]];
    }
    
    [self.myTableView reloadData];

}

- (id)initWithTaskList:(TaskList *)theList andTagCollection:(TagCollection *)theTagCollection
 andTaskListCollection:(TaskListCollection *)theTaskListCollection andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection andBackButtonTitle:(NSString *)theBackButtonTitle {
	if ((self = [super init])) {

        [self.view setBackgroundColor:[UIColor whiteColor]];
		// Assign the task list to local reference object
        currentListItem = [[ListItem alloc]init];
		refTaskList = theList;
        _currentTaskList = theList;
        
		currentTaskListID = refTaskList.listID;
        self.completedTasksArray = [[NSMutableArray alloc]init];
        self.completedRepeatTasksArray = [[NSMutableArray alloc]init];
        
        // Set initial search range type
        self.searchRangeType = EnumSearchRangeTypeAllLists;
        
        // Assign the back button title
        self.backButtonTitle = theBackButtonTitle;
		
		// Assign master task list collection to local reference object
		refMasterTaskListCollection = theMasterTaskListCollection;
		refMasterTaskListCollection.currentTaskListID = currentTaskListID;
		
		// Assign to the normal task list collection
		refTaskListCollection = theTaskListCollection;
		refTaskListCollection.currentTaskListID = currentTaskListID;
		
        // Create an observer for whenever application becomes active
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        
		// Set title
		self.title = refTaskList.title;
		
		// Init the select row prompt to false
		selectRowPrompt = FALSE;
		
		// Set the selected row to 0
		selectedRow = 0;
		
		// Init items due selected to false
		itemsDueSelected = FALSE;
		
		// Init tile view frame to nil
		self.tileViewFrame = CGRectZero;
		
		// Set settings view controller can close to true
		settingsViewControllerCanClose = TRUE;
		
		// Init the tag collection
		refTagCollection = theTagCollection;
		
		// Empty init the filter tags task list collection
		filterTagsTaskListCollection = [[TaskListCollection alloc] init];
		
		// Init the filter list item tag collection
		filterListItemTagCollection = [[ListItemTagCollection alloc] init];
		
		// Init edit list item controller at base level
		editListItemViewControllerAtBaseLevel = TRUE;
		
        // Init date range for start and end date in search
        searchStartDateRange = @"";
        searchEndDateRange = @"";
        
		// Init the list options action sheet
		listOptionsActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil
											   destructiveButtonTitle:nil otherButtonTitles:@"Email Text List", @"Email PDF List", nil];
		
		// Load the canvass and page images
//		NSString *canvasImageName = [refMasterTaskListCollection getImageNameForSection:@"Canvas"];
//		canvassImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:canvasImageName]];
//		pageImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PaperPreview.png"]];
//        
//		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
//			[canvassImage setFrame:kCanvassImagePortrait];
//			[pageImage setFrame:kPageImagePortrait];
//		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
//			[pageImage setFrame:kPageImageLandscape];
//			[canvassImage setFrame:kCanvassImageLandscape];
//		}
        
//		[self.view addSubview:canvassImage];
//		[self.view addSubview:pageImage];
		
		
		// Load the navigation bar buttons
		[self loadNavigationBarButtons];
		
		// Load the filter tag objects
		[self loadFilterTagObjects];
        
		// Add list table view
		[self loadTableView];
		
		// Load the completed stamp image
		listCompletedStampImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CompletedStamp.png"]];
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
			[listCompletedStampImage setFrame:kListCompletedStampPortrait];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
			[listCompletedStampImage setFrame:kListCompletedStampLandscape];
		}
        
		[listCompletedStampImage setHidden:YES];
		if ([refTaskList completed]) {
			[listCompletedStampImage setHidden:NO];
		}
		[listCompletedStampImage setAlpha:0.75f];
		[self.view addSubview:listCompletedStampImage];
        
		// Add text related fields
		[self loadTextFields];
		
		// Set initial highlighter selected value
		highlighterSelected = EnumHighlighterColorNone;
		
		// Load the frame/border
//		NSString *borderImageName = [refMasterTaskListCollection getImageNameForSection:@"Border"];
//		borderImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:borderImageName]];
        
//		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
//			[borderImage setFrame:kBorderImagePortrait];
//		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
//			[borderImage setFrame:kBorderImageLandscape];
//		}
//        
//		[self.view addSubview:borderImage];
		
		// Load the search objects
		[self loadSearchObjects];
		
		// Load the buttons
		[self loadButtons];
		
		// Load the items due table view and pocket
//		[self loadItemsDueTableView];
		
		// Load the control panel objects
//		[self loadControlPanelObjects];
		
		// Load the control panel button objects
//		[self loadControlPanelButtonObjects];
		
		// Load the highlighter cover view
        //	highlighterCoverView = [[UIImageView alloc] init]; //WithImage:[UIImage imageNamed:@"ToolSelectedBlackoutPortrait.png"]];
        //	[highlighterCoverView setAlpha:0.0f];
		
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
            //	[highlighterCoverView setImage:[UIImage imageNamed:@"ToolSelectedBlackoutPortrait.png"]];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
            //	[highlighterCoverView setImage:[UIImage imageNamed:@"ToolSelectedBlackoutLandscape.png"]];
		}
		
		//[self.view addSubview:highlighterCoverView];
		
	}
	return self;
}

- (void) refreshViewWithNewTaskList:(TaskList *)theList andTagCollection:(TagCollection *)theTagCollection andTaskListCollection:(TaskListCollection *)theTaskListCollection andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection {

    // Assign the task list to local reference object
    currentListItem = [[ListItem alloc]init];
    refTaskList = theList;
    _currentTaskList = theList;
    
    currentTaskListID = refTaskList.listID;
    self.completedTasksArray = [[NSMutableArray alloc]init];
    self.completedRepeatTasksArray = [[NSMutableArray alloc]init];
    
    // Set initial search range type
    self.searchRangeType = EnumSearchRangeTypeAllLists;
    
    // Assign master task list collection to local reference object
    refMasterTaskListCollection = theMasterTaskListCollection;
    refMasterTaskListCollection.currentTaskListID = currentTaskListID;
    
    // Assign to the normal task list collection
    refTaskListCollection = theTaskListCollection;
    refTaskListCollection.currentTaskListID = currentTaskListID;
    
    // Set title
    self.title = refTaskList.title;
    
    // Init the select row prompt to false
    selectRowPrompt = FALSE;
    
    // Set the selected row to 0
    selectedRow = 0;
    
    // Init items due selected to false
    itemsDueSelected = FALSE;
    
    // Set settings view controller can close to true
    settingsViewControllerCanClose = TRUE;
    
    // Init the tag collection
    refTagCollection = theTagCollection;
    
    // Empty init the filter tags task list collection
    filterTagsTaskListCollection = [[TaskListCollection alloc] init];
    
    // Init the filter list item tag collection
    filterListItemTagCollection = [[ListItemTagCollection alloc] init];
    
    // Init edit list item controller at base level
    editListItemViewControllerAtBaseLevel = TRUE;
    
    // Init date range for start and end date in search
    searchStartDateRange = @"";
    searchEndDateRange = @"";
    
    // Init the list options action sheet
//    listOptionsActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil
//                                           destructiveButtonTitle:nil otherButtonTitles:@"Email Text List", @"Email PDF List", nil];
    
    
    // Load the navigation bar buttons
//    [self loadNavigationBarButtons];
    
    // Load the filter tag objects
//    [self loadFilterTagObjects];
    
    // Add list table view
//    [self loadTableView];
    
    // Load the completed stamp image
//    listCompletedStampImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CompletedStamp.png"]];
//    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
//        [listCompletedStampImage setFrame:kListCompletedStampPortrait];
//    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
//        [listCompletedStampImage setFrame:kListCompletedStampLandscape];
//    }
    
    [listCompletedStampImage setHidden:YES];
    if ([refTaskList completed]) {
        [listCompletedStampImage setHidden:NO];
    }
    [listCompletedStampImage setAlpha:0.75f];
//    [self.view addSubview:listCompletedStampImage];
    
    // Add text related fields
//    [self loadTextFields];
    
    // Set initial highlighter selected value
    highlighterSelected = EnumHighlighterColorNone;
    
    // Load the frame/border
    //		NSString *borderImageName = [refMasterTaskListCollection getImageNameForSection:@"Border"];
    //		borderImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:borderImageName]];
    
    //		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
    //			[borderImage setFrame:kBorderImagePortrait];
    //		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
    //			[borderImage setFrame:kBorderImageLandscape];
    //		}
    //
    //		[self.view addSubview:borderImage];
    
    // Load the search objects
//    [self loadSearchObjects];
    
    // Load the buttons
//    [self loadButtons];
    
    // Load the items due table view and pocket
    //		[self loadItemsDueTableView];
    
    // Load the control panel objects
    //		[self loadControlPanelObjects];
    
    // Load the control panel button objects
    //		[self loadControlPanelButtonObjects];
    
    // Load the highlighter cover view
    //	highlighterCoverView = [[UIImageView alloc] init]; //WithImage:[UIImage imageNamed:@"ToolSelectedBlackoutPortrait.png"]];
    //	[highlighterCoverView setAlpha:0.0f];
    
//    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
//        //	[highlighterCoverView setImage:[UIImage imageNamed:@"ToolSelectedBlackoutPortrait.png"]];
//    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
//        //	[highlighterCoverView setImage:[UIImage imageNamed:@"ToolSelectedBlackoutLandscape.png"]];
//    }
    
    // Check what to set the initial sort order to
	if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"List"]) {
		[sortItemsSegmentedControl setSelectedSegmentIndex:EnumSortByList];
	} else if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"Priority" ]) {
		[sortItemsSegmentedControl setSelectedSegmentIndex:EnumSortByPriority];
	} else if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"Date"]) {
		[sortItemsSegmentedControl setSelectedSegmentIndex:EnumSortByDueDate];
	} else if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"Completed" ]) {
		[sortItemsSegmentedControl setSelectedSegmentIndex:EnumSortByCompleted];
	} else if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"Last Selected"]) {
		// Update the sort order
		[sortItemsSegmentedControl setSelectedSegmentIndex:[refTaskList lastSortType]];
	} else {
		[sortItemsSegmentedControl setSelectedSegmentIndex:0];
	}
    
    [self.myTableView reloadData];
}

#pragma mark -
#pragma mark Load Methods

- (void)loadSearchObjects {
	// Empty init the filtered search task list collection
	filteredSearchTaskListCollection = [[TaskListCollection alloc] init];
    
	mySearchBar = [[UISearchBar alloc] initWithFrame:kSearchBarPortrait];
	[mySearchBar setDelegate:self];
	[mySearchBar setAlpha:0.0f];
	
	// Get rid of background
	for (UIView *view in mySearchBar.subviews) {
		if ([view isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
			[view removeFromSuperview];
		}
	}
	
	// Add a segmented control
	searchScopeSegmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Title", @"Notes", @"Tags", @"All", nil]];
	
	[searchScopeSegmentedControl setSegmentedControlStyle:UISegmentedControlStyleBar];
	[searchScopeSegmentedControl setSelectedSegmentIndex:0];
	[searchScopeSegmentedControl setAlpha:0.0f];
	[searchScopeSegmentedControl addTarget:self action:@selector(searchScopeSegmentedControlAction) forControlEvents:UIControlEventValueChanged];
	
	searchButton = [UIButton buttonWithType:UIButtonTypeCustom];// retain];
	[searchButton setShowsTouchWhenHighlighted:YES];
	[searchButton setBackgroundImage:[UIImage imageNamed:@"SearchButton.png"] forState:UIControlStateNormal];
	[searchButton addTarget:self action:@selector(searchButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[searchButton setFrame:CGRectZero];
	
	clearSearchObjectsButton = [UIButton buttonWithType:UIButtonTypeCustom];// retain];
	[clearSearchObjectsButton addTarget:self action:@selector(clearSearchObjectsButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[clearSearchObjectsButton setShowsTouchWhenHighlighted:YES];
	[clearSearchObjectsButton setBackgroundImage:[UIImage imageNamed:@"TagClearButton.png"] forState:UIControlStateNormal];
	[clearSearchObjectsButton setFrame:CGRectZero];
	[clearSearchObjectsButton setAlpha:0.0f];
	
    searchDateRangeButton = [UIButton buttonWithType:UIButtonTypeCustom];// retain];
    [searchDateRangeButton addTarget:self action:@selector(searchDateRangeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [searchDateRangeButton setShowsTouchWhenHighlighted:YES];
    [searchDateRangeButton setBackgroundImage:[UIImage imageNamed:@"CalendarButton.png"] forState:UIControlStateNormal];
    [searchDateRangeButton setFrame:CGRectZero];
    [searchDateRangeButton setAlpha:0.0f];
    
    searchListRangeButton = [UIButton buttonWithType:UIButtonTypeCustom];// retain];
    [searchListRangeButton addTarget:self action:@selector(searchListRangeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [searchListRangeButton setShowsTouchWhenHighlighted:YES];
    [searchListRangeButton setBackgroundImage:[UIImage imageNamed:@"SearchRangeThisListButton.png"] forState:UIControlStateNormal];
    [searchListRangeButton setBackgroundImage:[UIImage imageNamed:@"SearchRangeAllListsButton.png"] forState:UIControlStateSelected];
    [searchListRangeButton setFrame:CGRectZero];
    [searchListRangeButton setAlpha:0.0f];
    
    showAllTasksButton = [UIButton buttonWithType:UIButtonTypeCustom] ;//retain];
    [showAllTasksButton addTarget:self action:@selector(showAllTasksButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [showAllTasksButton setShowsTouchWhenHighlighted:YES];
    [showAllTasksButton setBackgroundImage:[UIImage imageNamed:@"ShowAllTasksButton.png"] forState:UIControlStateNormal];
    [showAllTasksButton setFrame:CGRectZero];
    [showAllTasksButton setAlpha:0.0f];
    
	searchTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	[searchTitleLabel setTextAlignment:NSTextAlignmentCenter];
    if ([self searchRangeType] == EnumSearchRangeTypeAllLists) {
        [searchTitleLabel setText:@"Search (All Lists)"];
    } else if ([self searchRangeType] == EnumSearchRangeTypeThisList) {
        [searchTitleLabel setText:@"Search (This List)"];
    }
	[searchTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19.0]];
	[searchTitleLabel setAlpha:0.0f];
	[searchTitleLabel setBackgroundColor:[UIColor clearColor]];
	[searchTitleLabel setTextColor:[UIColor darkGrayColor]];
	
    searchDateRangeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [searchDateRangeLabel setTextAlignment:NSTextAlignmentCenter];
    [searchDateRangeLabel setText:@"Date Range: None"];
	[searchDateRangeLabel setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
    [searchDateRangeLabel setAlpha:0.0f];
    [searchDateRangeLabel setBackgroundColor:[UIColor clearColor]];
    [searchDateRangeLabel setTextColor:[UIColor darkGrayColor]];
    
	// Check interface orientation
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
		[mySearchBar setFrame:kSearchBarPortrait];
		[searchScopeSegmentedControl setFrame:kSearchScopeSegmentPortrait];
		[searchButton setFrame:kSearchButtonPortrait];
		[clearSearchObjectsButton setFrame:kClearSearchButtonPortrait];
		[searchDateRangeButton setFrame:kSearchDateRangeButtonPortrait];
        [searchListRangeButton setFrame:kSearchListRangeButtonPortrait];
        [showAllTasksButton setFrame:kShowAllTasksButtonPortrait];
        [searchTitleLabel setFrame:kSearchTitleLabelPortrait];
        [searchDateRangeLabel setFrame:kSearchDateRangeLabelPortrait];
	} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
		[mySearchBar setFrame:kSearchBarLandscape];
		[searchScopeSegmentedControl setFrame:kSearchScopeSegmentLandscape];
		[searchButton setFrame:kSearchButtonLandscape];
		[clearSearchObjectsButton setFrame:kClearSearchButtonLandscape];
        [searchDateRangeButton setFrame:kSearchDateRangeButtonLandscape];
        [searchListRangeButton setFrame:kSearchListRangeButtonLandscape];
        [showAllTasksButton setFrame:kShowAllTasksButtonLandscape];
		[searchTitleLabel setFrame:kSearchTitleLabelLandscape];
        [searchDateRangeLabel setFrame:kSearchDateRangeLabelLandscape];
	}
	
	[self.view addSubview:mySearchBar];
	[self.view addSubview:searchScopeSegmentedControl];
	[self.view addSubview:searchButton];
	[self.view addSubview:clearSearchObjectsButton];
    [self.view addSubview:searchDateRangeButton];
    [self.view addSubview:searchListRangeButton];
    [self.view addSubview:showAllTasksButton];
	[self.view addSubview:searchTitleLabel];
    [self.view addSubview:searchDateRangeLabel];
	
	
	
	
	/*
	 filterTagsClearButton = [UIButton buttonWithType:UIButtonTypeCustom];
	 [filterTagsClearButton addTarget:self action:@selector(filterTagsClearButtonAction) forControlEvents:UIControlEventTouchUpInside];
	 [filterTagsClearButton setShowsTouchWhenHighlighted:YES];
	 [filterTagsClearButton setBackground
	 
	 */
	
	// Init is search mode to false
	self.isSearchMode = FALSE;
}

- (void)loadTableView {
	// Load/init related objects
	self.myTableView = [[UITableView alloc] initWithFrame:kListTableViewLandscape style:UITableViewStylePlain];
	[self.myTableView setBackgroundColor:[UIColor clearColor]];
	[self.myTableView setDataSource:self];
	[self.myTableView setDelegate:self];
	
	headerLine1 = [[UIView alloc] initWithFrame:kHeaderLine1Portrait];
	[headerLine1 setBackgroundColor:[UIColor lightGrayColor]];
	
	headerLine2 = [[UIView alloc] initWithFrame:kHeaderLine2Portrait];
	[headerLine2 setBackgroundColor:[UIColor lightGrayColor]];
	
	// Orientation calls
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[self.myTableView setFrame:kListTableViewPortrait];
		[headerLine1 setFrame:kHeaderLine1Portrait];
		[headerLine2 setFrame:kHeaderLine2Portrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[self.myTableView setFrame:kListTableViewLandscape];
		[headerLine1 setFrame:kHeaderLine1Landscape];
		[headerLine2 setFrame:kHeaderLine2Landscape];
	}
    
	// Add to the view
	[self.view addSubview:self.myTableView];
	[self.view addSubview:headerLine1];
	[self.view addSubview:headerLine2];
}

- (void)loadTextFields {
	// Load/init related objects
	titleTextField = [[UITextField alloc] initWithFrame:kTitleTextFieldPortrait];
	[titleTextField setBackgroundColor:[UIColor clearColor]];
	[titleTextField setTextAlignment:NSTextAlignmentRight];
	[titleTextField setFont:[UIFont fontWithName:@"Helvetica" size:30.0]];
	[titleTextField setTextColor:[UIColor darkTextColor]];
	[titleTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
	[titleTextField setAutocapitalizationType:UITextAutocapitalizationTypeWords];
	[titleTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
	[titleTextField setReturnKeyType:UIReturnKeyDone];
	[titleTextField setDelegate:self];
	[titleTextField setText:refTaskList.title];
	
	// Create the date label
	dateLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];// retain];
	[dateLabelButton setBackgroundColor:[UIColor clearColor]];
	[dateLabelButton setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
	[dateLabelButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
	[dateLabelButton addTarget:self action:@selector(dateLabelButtonAction) forControlEvents:UIControlEventTouchUpInside];
	NSDate *theDate = [MethodHelper dateFromString:refTaskList.creationDate usingFormat:K_DATEONLY_FORMAT];
	[dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES]
					 forState:UIControlStateNormal];
    
	listCompletedButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[listCompletedButton setImage:[UIImage imageNamed:@"ListBoxNotTicked.png"] forState:UIControlStateNormal];
	[listCompletedButton setImage:[UIImage imageNamed:@"ListBoxTicked.png"] forState:UIControlStateSelected];
	[listCompletedButton addTarget:self action:@selector(listCompletedButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[listCompletedButton setSelected:refTaskList.completed];
	
	// Orientation related updates
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[titleTextField setFrame:kTitleTextFieldPortrait];
		[dateLabelButton setFrame:kDateLabelPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[titleTextField setFrame:kTitleTextFieldLandscape];
		[dateLabelButton setFrame:kDateLabelLandscape];
	}
	
	[self updateListCompletedFrameForOrientation:[self interfaceOrientation]];
	
	// Add objects to the view
	[self.view addSubview:titleTextField];
	[self.view addSubview:dateLabelButton];
	[self.view addSubview:listCompletedButton];
}

// Load the items due tableView
- (void)loadItemsDueTableView {
	// Load the items due pocket image
	NSString *pocketImageName = [refMasterTaskListCollection getImageNameForSection:@"Pocket"];
	itemsDuePocket = [[UIImageView alloc] initWithImage:[UIImage imageNamed:pocketImageName]];
	//itemsDuePocket = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Pocket.png" ofType:nil]]];
	[itemsDuePocket setFrame:kItemsDuePocketPortrait];
	if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[itemsDuePocket setFrame:kItemsDuePocketLandscape];
        //	itemsDuePocket.frame = [MethodHelper moveToLandscapeLocation:itemsDuePocket.frame];
	}
	
	[self.view addSubview:itemsDuePocket];
	
	// Alloc and init the table view responder (delegate and datasource)
	itemsDueTableViewResponder = [[ItemsDueTableViewResponder alloc] initWithDelegate:self andParentListTitle:refTaskList.title
																	  andParentListID:refTaskList.listID
																andTaskListCollection:refMasterTaskListCollection];
	
	
	itemsDueTableView = [[UITableView alloc] initWithFrame:kItemsDueTableViewPortrait style:UITableViewStylePlain];
	if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[itemsDueTableView setFrame:kItemsDueTableViewLandscape];
        //	itemsDueTableView.frame = [MethodHelper moveToLandscapeLocation:itemsDueTableView.frame];
	}
	//[itemsDueTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLineEtched];
	[itemsDueTableView setSeparatorColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.3]];
	[itemsDueTableView setRowHeight:40.0f];
	[itemsDueTableView setBackgroundColor:[UIColor clearColor]];
	[itemsDueTableView setDataSource:itemsDueTableViewResponder];
	[itemsDueTableView setDelegate:itemsDueTableViewResponder];
	[self.view addSubview:itemsDueTableView];
	
	// Check whether to hide the pocket view
	if (refMasterTaskListCollection.pocketReminder == FALSE) {
		[itemsDuePocket setHidden:YES];
		[itemsDueTableView setHidden:YES];
	}
}

- (void)loadFilterTagObjects {
	filterTagsFooterLine = [[UIView alloc] initWithFrame:CGRectZero];
	[filterTagsFooterLine setBackgroundColor:[UIColor lightGrayColor]];
	
	filterTagsLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	[filterTagsLabel setBackgroundColor:[UIColor clearColor]];
	[filterTagsLabel setTextAlignment:NSTextAlignmentCenter];
	[filterTagsLabel setTextColor:[UIColor darkTextColor]];
	[filterTagsLabel setFont:[UIFont fontWithName:@"Helvetica" size:17.0]];
	
	filterTagsImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
	[filterTagsImageView setImage:[UIImage imageNamed:@"Tag_Big.png"]];// Change Tag -> Tag_Big
	
	// Filter tags clear button
	filterTagsClearButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[filterTagsClearButton addTarget:self action:@selector(filterTagsClearButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[filterTagsClearButton setShowsTouchWhenHighlighted:YES];
	[filterTagsClearButton setBackgroundImage:[UIImage imageNamed:@"TagClearButton.png"] forState:UIControlStateNormal];
	
	// Orientation related updates
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[filterTagsFooterLine setFrame:kFilterTagsFooterLinePortrait];
		[filterTagsLabel setFrame:kFilterTagsLabelPortrait];
		[filterTagsImageView setFrame:kFilterTagsImageViewPortrait];
		[filterTagsClearButton setFrame:kFilterTagsClearButtonPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[filterTagsFooterLine setFrame:kFilterTagsFooterLineLandscape];
		[filterTagsLabel setFrame:kFilterTagsLabelLandscape];
		[filterTagsImageView setFrame:kFilterTagsImageViewLandscape];
		[filterTagsClearButton setFrame:kFilterTagsClearButtonLandscape];
	}
	
	// Make them hidden first
	[self hideFilterTagObjects:YES];
	
	// Add to subview
	[self.view addSubview:filterTagsFooterLine];
	[self.view addSubview:filterTagsLabel];
	[self.view addSubview:filterTagsImageView];
	[self.view addSubview:filterTagsClearButton];
}

- (void)loadNavigationBarButtons {
	// Replace the left bar button item
//	UIBarButtonItem *listsButtonItem = [[UIBarButtonItem alloc] initWithTitle:self.backButtonTitle
//																		style:UIBarButtonItemStyleBordered
//																	   target:self action:@selector(listButtonItemAction)];
//	[self.navigationItem setLeftBarButtonItem:listsButtonItem animated:NO];
//	[listsButtonItem release];
    
	// Add a toolbar instead of a help button to the right bar button
	HiddenToolbar *toolbar = [[HiddenToolbar alloc] initWithFrame:CGRectMake(10, 0, 100, 44)];
	[toolbar setBarStyle: UIBarStyleBlack];
	toolbar.opaque = NO;
	toolbar.backgroundColor = [UIColor clearColor];
	toolbar.translucent = YES;
	
	// Array to hold the buttons in toolbar
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] init];
    
    
//    settingsBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
//    
//    UIButton *settingsButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 35)];
//    
//    [settingsButton setBackgroundImage:[UIImage imageNamed:@"SettingsIcon.png"] forState:UIControlStateNormal];
//    settingsButton.showsTouchWhenHighlighted = YES;
//    
//    // Add action for button
//    [settingsButton addTarget:self action:@selector(settingsBarButtonItemAction) forControlEvents:UIControlEventTouchUpInside];
//    
//    settingsBarButtonItem.customView = settingsButton;
	
	// Create a settings button
//	settingsBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SettingsIcon.png"]
//                                                             style:UIBarButtonItemStyleBordered
//                                                            target:self
//                                                            action:@selector(settingsBarButtonItemAction)];
//	
//	[settingsBarButtonItem setStyle:UIBarButtonItemStylePlain];
//    [toolbarButtons addObject:settingsBarButtonItem];
	
	// Add first spacer
	UIBarButtonItem *spacer1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
																			 target:nil action:nil];
	[toolbarButtons addObject:spacer1];
//	[spacer1 release];
//	
	// Add a help bar button item
	UIBarButtonItem *helpBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"?"
																		  style:UIBarButtonItemStyleBordered
																		 target:self action:@selector(helpBarButtonItemAction)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        NSDictionary *barButtonAppearanceDict = @{UITextAttributeFont : [UIFont boldSystemFontOfSize:18.0], UITextAttributeTextColor : [UIColor blackColor]};
        [helpBarButtonItem setTitleTextAttributes:barButtonAppearanceDict forState:UIControlStateNormal];
    }
	[helpBarButtonItem setStyle:UIBarButtonItemStylePlain];
	[toolbarButtons addObject:helpBarButtonItem];
//	[helpBarButtonItem release];
	
	// Place the buttons in the toolbar
	[toolbar setItems:toolbarButtons animated:NO];
	
//	[toolbarButtons release];
//	
	// Put the toolbar in the nav bar
	UIBarButtonItem *customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:toolbar];
	[self.navigationItem setRightBarButtonItem:customBarButtonItem animated:NO];
//	[toolbar release];
//	[customBarButtonItem release];
}

- (void)loadButtons {
	// Load the buttons
	editListButton = [UIButton buttonWithType:UIButtonTypeCustom];// retain];
	[editListButton setShowsTouchWhenHighlighted:YES];
	[editListButton setBackgroundImage:[UIImage imageNamed:@"EditButton.png"] forState:UIControlStateNormal];
	[editListButton addTarget:self action:@selector(editListButtonAction) forControlEvents:UIControlEventTouchUpInside];
	
	// List view options button
	listOptionsButton = [UIButton buttonWithType:UIButtonTypeCustom];// retain];
	[listOptionsButton setShowsTouchWhenHighlighted:YES];
	[listOptionsButton setBackgroundImage:[UIImage imageNamed:@"ExportListButton.png"] forState:UIControlStateNormal];
	[listOptionsButton addTarget:self action:@selector(listOptionsButtonAction) forControlEvents:UIControlEventTouchUpInside];
	
	// New list item button
	newListItemButton = [UIButton buttonWithType:UIButtonTypeCustom] ;//retain];
	[newListItemButton setShowsTouchWhenHighlighted:YES];
	[newListItemButton setBackgroundImage:[UIImage imageNamed:@"AddButton.png"] forState:UIControlStateNormal];
	[newListItemButton addTarget:self action:@selector(newListItemButtonAction) forControlEvents:UIControlEventTouchUpInside];
	
	// New scribble list item button
	newScribbleListItemButton = [UIButton buttonWithType:UIButtonTypeCustom];// retain];
	[newScribbleListItemButton setShowsTouchWhenHighlighted:YES];
	[newScribbleListItemButton setBackgroundImage:[UIImage imageNamed:@"AddScribbleTaskButton.png"] forState:UIControlStateNormal];
	[newScribbleListItemButton addTarget:self action:@selector(newScribbleListItemButtonAction) forControlEvents:UIControlEventTouchUpInside];
	
	// Done editing button
	doneEditingButton = [UIButton buttonWithType:UIButtonTypeCustom];/// retain];
	[doneEditingButton setShowsTouchWhenHighlighted:YES];
	[doneEditingButton setHidden:YES];
	[doneEditingButton setBackgroundImage:[UIImage imageNamed:@"DoneButton.png"] forState:UIControlStateNormal];
	[doneEditingButton addTarget:self action:@selector(doneEditingButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    
	// Create the KFSegmented Control
	
	//NSArray *buttonItems = [NSArray arrayWithObjects:button1, button2, button3, button4, nil];
    
	NSArray *imageItems = [NSArray arrayWithObjects:@"SortItemList.png", @"SortItemPriority.png", @"SortItemDate.png", @"SortItemCompleted.png", nil];
	NSArray *selectedImageItems = [NSArray arrayWithObjects:@"SortItemListSelected.png", @"SortItemPrioritySelected.png", @"SortItemDateSelected.png", @"SortItemCompletedSelected", nil];
	
	sortItemsSegmentedControl = [[KFSegmentedControl alloc] initWithItems:imageItems selectedItems:selectedImageItems
															  segmentSize:CGSizeMake(78, 29) delegate:self];
	
	//sortItemsSegmentedControl = [[KFSegmentedControl alloc] initWithButtonItems:buttonItems andDelegate:self];
	// Check what to set the initial sort order to
	if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"List"]) {
		[sortItemsSegmentedControl setSelectedSegmentIndex:EnumSortByList];
	} else if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"Priority" ]) {
		[sortItemsSegmentedControl setSelectedSegmentIndex:EnumSortByPriority];
	} else if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"Date"]) {
		[sortItemsSegmentedControl setSelectedSegmentIndex:EnumSortByDueDate];
	} else if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"Completed" ]) {
		[sortItemsSegmentedControl setSelectedSegmentIndex:EnumSortByCompleted];
	} else if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"Last Selected"]) {
		// Update the sort order
		[sortItemsSegmentedControl setSelectedSegmentIndex:[refTaskList lastSortType]];
	} else {
		[sortItemsSegmentedControl setSelectedSegmentIndex:0];
	}
	
    
	
	// Defaults to 0 already in CustomSegmentedControl class
	
	// Pen button
	penButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[penButton addTarget:self action:@selector(penButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[penButton setShowsTouchWhenHighlighted:YES];
	[penButton setBackgroundImage:[UIImage imageNamed:@"PenIcon.png"] forState:UIControlStateNormal];
	
	// Highlighter button
	highlighterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[highlighterButton addTarget:self action:@selector(highlighterButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[highlighterButton setShowsTouchWhenHighlighted:YES];
	[highlighterButton setBackgroundImage:[UIImage imageNamed:@"HighlighterIcon.png"] forState:UIControlStateNormal];
	
	// Filter tags button
	filterTagsButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[filterTagsButton addTarget:self action:@selector(filterTagsButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[filterTagsButton setShowsTouchWhenHighlighted:YES];
	[filterTagsButton setBackgroundImage:[UIImage imageNamed:@"FilterTagButton.png"] forState:UIControlStateNormal];
	
	// The select task image needs to be created here too
	selectTaskImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SelectTask.png"]];
	//[selectRowLabel setHidden:YES];
	[selectTaskImageView setAlpha:0.0f];
	
	// Orientation settings (useless here as this is called in init method)
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[listOptionsButton setFrame:kListOptionsButtonPortrait];
		[editListButton setFrame:kEditListButtonPortrait];
		[newListItemButton setFrame:kNewListItemButtonPortrait];
		[newScribbleListItemButton setFrame:kNewScribbleListItemButtonPortrait];
		[doneEditingButton setFrame:kDoneEditingButtonPortrait];
		[sortItemsSegmentedControl setFrame:kSortItemsSegmentedControlPortrait];
		[penButton setFrame:kPenButtonPortrait];
		[highlighterButton setFrame:kHighlighterButtonPortrait];
		[selectTaskImageView setFrame:kSelectTaskImageViewPortrait];
		[filterTagsButton setFrame:kFilterTagsButtonPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[listOptionsButton setFrame:kListOptionsButtonLandscape];
		[editListButton setFrame:kEditListButtonLandscape];
		[newListItemButton setFrame:kNewListItemButtonLandscape];
		[newScribbleListItemButton setFrame:kNewScribbleListItemButtonLandscape];
		[doneEditingButton setFrame:kDoneEditingButtonLandscape];
		[sortItemsSegmentedControl setFrame:kSortItemsSegmentedControlLandscape];
		[penButton setFrame:kPenButtonLandscape];
		[highlighterButton setFrame:kHighlighterButtonLandscape];
		[selectTaskImageView setFrame:kSelectTaskImageViewLandscape];
		[filterTagsButton setFrame:kFilterTagsButtonLandscape];
	}
	
	// Add to subview
	[self.view addSubview:listOptionsButton];
	[self.view addSubview:editListButton];
	[self.view addSubview:filterTagsButton];
	[self.view addSubview:newListItemButton];
	[self.view addSubview:newScribbleListItemButton];
	[self.view addSubview:doneEditingButton];
	[self.view addSubview:sortItemsSegmentedControl];
	[self.view addSubview:penButton];
	[self.view addSubview:highlighterButton];
	[self.view addSubview:selectTaskImageView];
}

- (void)loadControlPanelObjects {
	// Load the control panel image
	controlPanelImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPanel.png"]];
	
    // Create the segmented control panel type control
    controlPanelTypeSegmentedControl = [[UISegmentedControl alloc] init];
    // Create the items
    [controlPanelTypeSegmentedControl setSegmentedControlStyle:UISegmentedControlStyleBar];
    
    [controlPanelTypeSegmentedControl insertSegmentWithTitle:@"Calendar / Lists" atIndex:0 animated:NO];
    [controlPanelTypeSegmentedControl insertSegmentWithTitle:@"Drawing Tools" atIndex:0 animated:NO];
    
    // Set default panel view
    NSString *defaultPanelView = [ApplicationSetting getSettingDataForName:@"DefaultPanelView"];
    
    if ([defaultPanelView isEqualToString:@"Drawing Tools"]) {
        
        [controlPanelTypeSegmentedControl setSelectedSegmentIndex:EnumControlPanelTypeDrawingTools];//EnumControlPanelTypeDrawingTools
    } else if ([defaultPanelView isEqualToString:@"Calendar / Lists"]) {
        
        [controlPanelTypeSegmentedControl setSelectedSegmentIndex:EnumControlPanelTypeCalendarList];//EnumControlPanelTypeCalendarList
    }

    [controlPanelTypeSegmentedControl addTarget:self action:@selector(controlPanelTypeSegmentedControlAction) forControlEvents:UIControlEventValueChanged];
    
    // Create the tools container views
    toolsContainerView = [[UIView alloc] init];
    [toolsContainerView setClipsToBounds:YES];
    
	// Drawing Tools container view and calender list container view
	drawingToolsContainerView = [[UIView alloc] initWithFrame:kDrawingToolsContainerHide];
    
    calendarListToolsContainerView = [[UIView alloc] initWithFrame:kCalendarListToolsContainerHide];
    //[calendarListToolsContainerView setBackgroundColor:[UIColor redColor]];
    
    // Calender lists divider
    calendarListsDividerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CalendarListsDivider.png"]];
    [calendarListsDividerImageView setFrame:kCalenderListsDividerFrame];
    
    // Pens and highlighters container views
    pensContainerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PenContainer.png"]];
	highlightersContainerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"HighlighterContainer.png"]];
	
    // Day calendar view
    dayCalendarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DayCalendar.png"]];
    [dayCalendarImageView setFrame:kDayCalendarImageViewFrame];
    
    // Day calendar label
    dayCalendarDateLabel = [[UILabel alloc] initWithFrame:kDayCalendarLabelFrame];
    [dayCalendarDateLabel setText:[MethodHelper localizedDateFrom:[NSDate date] usingStyle:NSDateFormatterLongStyle withYear:YES]];
    [dayCalendarDateLabel setTextColor:[UIColor lightTextColor]];
    [dayCalendarDateLabel setTextAlignment:NSTextAlignmentCenter];
    [dayCalendarDateLabel setBackgroundColor:[UIColor clearColor]];
    
    // Calendar View
    self.miniCalendarView = [[MiniCalendarView alloc] initWithFrame:kMiniCalendarViewFrame andTaskListCollection:refMasterTaskListCollection];// autorelease];
    [self.miniCalendarView setSelectionDelegate:self];
    
    // Mini list pad tableview
    miniListPadTableViewResponder = [[MiniListPadTableViewResponder alloc] initWithTaskListCollection:refMasterTaskListCollection andDelegate:self];
	miniListPadTableView = [[UITableView alloc] initWithFrame:kMiniListPadTableViewFrame style:UITableViewStyleGrouped];
	//[listPadTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[miniListPadTableView setBackgroundView:[[UIView alloc] init]];// autorelease]];
    [miniListPadTableView setContentInset:UIEdgeInsetsMake(0, 0, 33, 0)];
    //[miniListPadTableView setBackgroundColor:[UIColor colorWithRed:20 / 255.0 green:20 / 255.0 blue:20 / 255.0 alpha:1.0]];  // #272324 -- back bit is #141414
    //[listPadTableView setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
    [miniListPadTableView setBackgroundColor:[UIColor clearColor]];
    [miniListPadTableView setShowsVerticalScrollIndicator:NO];
	[miniListPadTableView setDataSource:miniListPadTableViewResponder];
	[miniListPadTableView setDelegate:miniListPadTableViewResponder];
    
	// Orientation settings
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        [toolsContainerView setFrame:kToolsContainerViewPortrait];
		[controlPanelImage setFrame:kControlPanelImagePortrait];
        [controlPanelTypeSegmentedControl setFrame:kControlPanelTypeSegmentPortrait];
		[pensContainerImageView setFrame:kPensImagePortrait];
		[highlightersContainerImageView setFrame:kHighlightersImagePortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        [toolsContainerView setFrame:kToolsContainerViewLandscape];
		[controlPanelImage setFrame:kControlPanelImageLandscape];
        [controlPanelTypeSegmentedControl setFrame:kControlPanelTypeSegmentLandscape];
		[pensContainerImageView setFrame:kPensImageLandscape];
		[highlightersContainerImageView setFrame:kHighlightersImageLandscape];
	}
    
    if ([controlPanelTypeSegmentedControl selectedSegmentIndex] == EnumControlPanelTypeCalendarList) {
        [calendarListToolsContainerView setFrame:kCalendarListToolsContainerShow];
        [drawingToolsContainerView setFrame:kDrawingToolsContainerHide];
    } else if ([controlPanelTypeSegmentedControl selectedSegmentIndex] == EnumControlPanelTypeDrawingTools) {
        [calendarListToolsContainerView setFrame:kCalendarListToolsContainerHide];
        [drawingToolsContainerView setFrame:kDrawingToolsContainerShow];
    }
	
	// Add to subview
	[self.view addSubview:controlPanelImage];
    [self.view addSubview:controlPanelTypeSegmentedControl];
    [self.view addSubview:toolsContainerView];
    [toolsContainerView addSubview:calendarListToolsContainerView];
    [toolsContainerView addSubview:drawingToolsContainerView];
	[drawingToolsContainerView addSubview:pensContainerImageView];
	[drawingToolsContainerView addSubview:highlightersContainerImageView];
    [drawingToolsContainerView addSubview:dayCalendarImageView];
    [drawingToolsContainerView addSubview:dayCalendarDateLabel];
    [calendarListToolsContainerView addSubview:self.miniCalendarView];
    [calendarListToolsContainerView addSubview:miniListPadTableView];
    [calendarListToolsContainerView addSubview:calendarListsDividerImageView];
    
	// Force select the correct row
    NSInteger theSelectedRow = [refTaskListCollection getIndexOfTaskListWithID:refTaskList.listID];
    [miniListPadTableViewResponder tableView:miniListPadTableView forceSelectRowAtIndexPathRow:theSelectedRow andSection:0];
}


- (void)loadControlPanelButtonObjects {
	// Pen buttons
	greenPenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[greenPenButton addTarget:self action:@selector(greenPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[greenPenButton setShowsTouchWhenHighlighted:YES];
	[greenPenButton setImage:[UIImage imageNamed:@"GreenPen.png"] forState:UIControlStateNormal];
	[greenPenButton setImage:[UIImage imageNamed:@"Empty.png"] forState:UIControlStateSelected];
	
	blackPenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[blackPenButton addTarget:self action:@selector(blackPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[blackPenButton setShowsTouchWhenHighlighted:YES];
	[blackPenButton setImage:[UIImage imageNamed:@"BlackPen.png"] forState:UIControlStateNormal];
	[blackPenButton setImage:[UIImage imageNamed:@"Empty.png"] forState:UIControlStateSelected];
	
	redPenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[redPenButton addTarget:self action:@selector(redPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[redPenButton setShowsTouchWhenHighlighted:YES];
	[redPenButton setImage:[UIImage imageNamed:@"RedPen.png"] forState:UIControlStateNormal];
	[redPenButton setImage:[UIImage imageNamed:@"Empty.png"] forState:UIControlStateSelected];
	
	bluePenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[bluePenButton addTarget:self action:@selector(bluePenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[bluePenButton setShowsTouchWhenHighlighted:YES];
	[bluePenButton setImage:[UIImage imageNamed:@"BluePen.png"] forState:UIControlStateNormal];
	[bluePenButton setImage:[UIImage imageNamed:@"Empty.png"] forState:UIControlStateSelected];
	
	// Highlighter buttons
	redHighlighterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[redHighlighterButton addTarget:self action:@selector(redHighlighterButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[redHighlighterButton setShowsTouchWhenHighlighted:YES];
	[redHighlighterButton setImage:[UIImage imageNamed:@"RedHighlighter.png"] forState:UIControlStateNormal];
	[redHighlighterButton setImage:[UIImage imageNamed:@"Empty.png"] forState:UIControlStateSelected];
	
	yellowHighlighterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[yellowHighlighterButton addTarget:self action:@selector(yellowHighlighterButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[yellowHighlighterButton setShowsTouchWhenHighlighted:YES];
	[yellowHighlighterButton setImage:[UIImage imageNamed:@"YellowHighlighter.png"] forState:UIControlStateNormal];
	[yellowHighlighterButton setImage:[UIImage imageNamed:@"Empty.png"] forState:UIControlStateSelected];
	
	whiteHighlighterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[whiteHighlighterButton addTarget:self action:@selector(whiteHighlighterButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[whiteHighlighterButton setShowsTouchWhenHighlighted:YES];
	[whiteHighlighterButton setImage:[UIImage imageNamed:@"WhiteHighlighter.png"] forState:UIControlStateNormal];
	[whiteHighlighterButton setImage:[UIImage imageNamed:@"Empty.png"] forState:UIControlStateSelected];
	
	// Orientation settings
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[greenPenButton setFrame:kGreenPenButtonPortrait];
		[redPenButton setFrame:kRedPenButtonPortrait];
		[blackPenButton setFrame:kBlackPenButtonPortrait];
		[bluePenButton setFrame:kBluePenButtonPortrait];
		[redHighlighterButton setFrame:kRedHighlighterButtonPortrait];
		[yellowHighlighterButton setFrame:kYellowHighlighterButtonPortrait];
		[whiteHighlighterButton setFrame:kWhiteHighlighterButtonPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[greenPenButton setFrame:kGreenPenButtonLandscape];
		[redPenButton setFrame:kRedPenButtonLandscape];
		[blackPenButton setFrame:kBlackPenButtonLandscape];
		[bluePenButton setFrame:kBluePenButtonLandscape];
		[redHighlighterButton setFrame:kRedHighlighterButtonLandscape];
		[yellowHighlighterButton setFrame:kYellowHighlighterButtonLandscape];
		[whiteHighlighterButton setFrame:kWhiteHighlighterButtonLandscape];
	}
	
	// Add to subview
	[drawingToolsContainerView addSubview:greenPenButton];
	[drawingToolsContainerView addSubview:redPenButton];
	[drawingToolsContainerView addSubview:bluePenButton];
	[drawingToolsContainerView addSubview:blackPenButton];
	[drawingToolsContainerView addSubview:redHighlighterButton];
	[drawingToolsContainerView addSubview:yellowHighlighterButton];
	[drawingToolsContainerView addSubview:whiteHighlighterButton];
    
    
}

#ifdef IS_FREE_BUILD
#pragma mark - MPAdViewDelegate Methods

- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}

- (void)adViewDidLoadAd:(MPAdView *)view {
    CGSize size = [view adContentViewSize];
    CGRect newFrame = view.frame;
    
    newFrame.size = size;
    newFrame.origin.x = (self.view.bounds.size.width - size.width) / 2;
    view.frame = newFrame;
}
#endif

#pragma mark -
#pragma mark UIView Controller Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	// Get rid of help overlay if it is there
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
    
	if ([highlightersPopoverController isPopoverVisible]) {
		// Reset selected highlighter to none
		highlighterSelected = EnumHighlighterColorNone;
		
		[highlightersPopoverController dismissPopoverAnimated:NO];
        highlightersPopoverController = nil;
//        [highlightersPopoverController release];
	}
	
	// Deselect all pens and highlighters and set pen/highlighter variables to none
	penSelected = EnumPenColorNone;
	highlighterSelected = EnumHighlighterColorNone;
	
	[greenPenButton setSelected:NO];
	[redPenButton setSelected:NO];
	[blackPenButton setSelected:NO];
	[bluePenButton setSelected:NO];
	[yellowHighlighterButton setSelected:NO];
	[redHighlighterButton setSelected:NO];
	[whiteHighlighterButton setSelected:NO];
	
#ifdef IS_FREE_BUILD
    // Rotate ad view
	[_adView rotateToOrientation:toInterfaceOrientation];
#endif
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	
    if (newListItemPopOverController != nil) {
        if ([newListItemPopOverController isPopoverVisible]) {
            [newListItemPopOverController presentPopoverFromRect:newListItemButton.frame
                                                          inView:self.view
                                        permittedArrowDirections:UIPopoverArrowDirectionRight
                                                        animated:YES];
            [newListItemPopOverController setPopoverContentSize:CGSizeMake(320, 352) animated:NO];
            
        }
    }
    
	
	if ([filterTagsPopoverController isPopoverVisible]) {
		[filterTagsPopoverController presentPopoverFromRect:filterTagsButton.frame
                                                     inView:self.view
                                   permittedArrowDirections:UIPopoverArrowDirectionLeft
                                                   animated:YES];
		[filterTagsPopoverController setPopoverContentSize:CGSizeMake(320, 318) animated:NO];
	}
	
	
	if ([editListItemPopOverController isPopoverVisible]) {
		if (itemsDueSelected == TRUE) {
			[editListItemPopOverController dismissPopoverAnimated:NO];
			itemsDueSelected = FALSE;
			return;
		}
		
		[self.myTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]
                           atScrollPosition:UITableViewScrollPositionMiddle
                                   animated:NO];
		
		CGPoint offset = [self.myTableView contentOffset];
		
		UITableViewCell *cell = [self.myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow
                                                                                      inSection:0]];
		
		// Multiply selected row by 44, get center of tableview
		CGFloat xOrigin = self.myTableView.frame.origin.x + (self.myTableView.frame.size.width - 320 - 60);
		CGFloat yOrigin = cell.frame.origin.y + 22 + self.myTableView.frame.origin.y - offset.y;
		CGRect rectPopover = CGRectMake(xOrigin, yOrigin, 1, 1);
		
		[editListItemPopOverController setDelegate:self];
		
		[editListItemPopOverController presentPopoverFromRect:rectPopover
                                                       inView:self.view
                                     permittedArrowDirections:UIPopoverArrowDirectionLeft
                                                     animated:YES];
	}
	
	
	
    if (drawPadPopOverController != nil) {
        if ([drawPadPopOverController isPopoverVisible]) {
            [self.myTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]
                               atScrollPosition:UITableViewScrollPositionMiddle
                                       animated:NO];
            
            CGPoint offset = [self.myTableView contentOffset];
            
            UITableViewCell *cell = [self.myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow
                                                                                          inSection:0]];
            
            // Multiply selected row by 44, get center of tableview
            CGFloat xOrigin = self.myTableView.frame.origin.x + (self.myTableView.frame.size.width - 320);
            CGFloat yOrigin = cell.frame.origin.y + 22 + self.myTableView.frame.origin.y - offset.y;
            CGRect rectPopover = CGRectMake(xOrigin, yOrigin, 1, 1);
            
            UIPopoverArrowDirection direction = UIPopoverArrowDirectionUp;
            
            if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
                if (yOrigin > 350) {
                    direction = UIPopoverArrowDirectionDown;
                }
            } else if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
                if (yOrigin > 450) {
                    direction = UIPopoverArrowDirectionDown;
                }
            }
            
            [drawPadPopOverController presentPopoverFromRect:rectPopover
													  inView:self.view
									permittedArrowDirections:direction
													animated:YES];
        }
    }
    
#ifdef IS_FREE_BUILD
    // Obtain the new size of the ad content.
    CGSize size = [_adView adContentViewSize];
    // Repositioning example: keep the ad view centered horizontally.
    CGRect newFrame = _adView.frame;
    newFrame.size = size;
    newFrame.origin.x = (self.view.bounds.size.width - size.width) / 2;
    newFrame.origin.y = self.view.bounds.size.height - size.height;
    _adView.frame = newFrame;
#else
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		
    }
#endif
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#ifdef IS_FREE_BUILD
- (void)loadView {
    [super loadView];
    
    //agltb3B1Yi1pbmNyDQsSBFNpdGUY-tDVFAw
    _adView = [[MPAdView alloc] initWithAdUnitId:kMoPubAdUnitID size:MOPUB_LEADERBOARD_SIZE];
    _adView.delegate = self;
    
    CGRect frame = _adView.frame;
    CGSize size = [_adView adContentViewSize];
    frame.origin.y = self.view.bounds.size.height - size.height;
    frame.origin.x = (self.view.bounds.size.width - size.width) / 2;
    _adView.frame = frame;
    
    
    [self.view addSubview:_adView];
    [self.view bringSubviewToFront:_adView];
    
    [_adView loadAd];
}



#endif

- (void)viewDidLoad {
    
    // Update support iOS 7
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
            self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];
        }
    }
    
    self.navigationController.navigationBar.translucent = NO;
    
//    refMasterTaskListCollection = [[TaskListCollection alloc] init];
//    
//    [refMasterTaskListCollection reloadAllLists];
//    
//    currentListItem = [[ListItem alloc]init];
//    refTaskList = [refMasterTaskListCollection.lists objectAtIndex:currentTaskListIndex];
//    
//    ListViewController *list = [self initWithTaskList:refTaskList andTagCollection:nil andTaskListCollection:refMasterTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection andBackButtonTitle:@"Back"];
    
    [super viewDidLoad];
    self.arrayTaskList = [refTaskList.listItems mutableCopy];
    
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
    if (isShowCompletedTasks == 0) {
        TaskList *mainTaskList = [refTaskListCollection getTaskListWithID:currentTaskListID];
        for (ListItem *mainItem in mainTaskList.listItems) {
            for (ListItem *editItem in mainTaskList.listItems) { // arrayTaskList
                if (mainItem.listItemID == editItem.listItemID) {
                    if (editItem.hasJustComplete == YES) {
                        //mainItem.completed = editItem.hasJustComplete;
                        
                        if (editItem.parentListItemID != -1) {// Has parent task
                            
                            // If task has parent task and hascompleted is YES -> task is completed
                            mainItem.completed = YES;
                            
                        }else{// Has no parent task
                            
                            if ([mainTaskList hasSubtasksForListItemAtIndex:[mainTaskList.listItems indexOfObject:editItem]] == YES) {// Has subtask
                                BOOL isAllTasksCompleted = YES;
                                // Get all subtask and check
                                for (ListItem *subItem in mainTaskList.listItems) {
                                    if (subItem.parentListItemID == editItem.listItemID) {
                                        // Check subtask is completed
                                        if (subItem.hasJustComplete != YES) {
                                            isAllTasksCompleted = NO;
                                            break;
                                        }
                                    }
                                }
                                
                                // All subtask is completed
                                if (isAllTasksCompleted == YES && editItem.hasJustComplete == YES) {
                                    // This task is completed
                                    mainItem.completed = YES;
                                }else{
                                    mainItem.completed = NO;
                                    mainItem.hasJustComplete = YES;
                                }
                                
                            }else{// Has no subtask
                                
                                mainItem.completed = editItem.hasJustComplete;
                            }
                            
                        }
                        
                    }else{
                        
                        // Check for task is completed
                        if (editItem.parentListItemID != -1) {// Has parent task
                            if (editItem.completed == YES) {
                                mainItem.hasJustComplete = editItem.completed;
                            }
                        }else{// Has no parent task
                            
                            if ([mainTaskList hasSubtasksForListItemAtIndex:[mainTaskList.listItems indexOfObject:editItem]] == YES) { // Has subtask
                                BOOL isAllTasksCompleted = YES;
                                // Get all subtask and check
                                for (ListItem *subItem in mainTaskList.listItems) {
                                    if (subItem.parentListItemID == editItem.listItemID) {
                                        // Check subtask is completed
                                        if (subItem.completed != YES) {
                                            isAllTasksCompleted = NO;
                                            break;
                                        }
                                    }
                                }
                                
                                // All subtask is completed
                                if (isAllTasksCompleted == YES && editItem.completed == YES) {
                                    mainItem.hasJustComplete = YES;
                                    mainItem.completed = YES;
                                }
                                if (isAllTasksCompleted == NO && editItem.completed == YES) {
                                    mainItem.hasJustComplete = YES;
                                    mainItem.completed = NO;
                                }
                            }else{// Has no subtask
                                
                                if (editItem.completed == YES) {
                                    mainItem.hasJustComplete = YES;
                                    mainItem.completed = YES;
                                }
                            }
                        }
                        
                    }
                    
                }
            }
        }
    }
}


- (void)viewDidUnload {
    [super viewDidUnload];
    myTableView = nil;
    mySearchBar = nil;
    searchScopeSegmentedControl = nil;
    searchButton = nil;
    clearSearchObjectsButton = nil;
    searchTitleLabel = nil;
    searchDateRangeLabel = nil;
    searchDateRangeButton = nil;
    showAllTasksButton = nil;
    searchListRangeButton = nil;
    canvassImage = nil;
    borderImage = nil;
    pageImage = nil;
    listCompletedStampImage = nil;
    headerLine1 = nil;
    headerLine2 = nil;
    listOptionsButton = nil;
    titleTextField = nil;
    dateLabelButton = nil;
    listCompletedButton = nil;
    itemsDueTableView = nil;
    itemsDuePocket = nil;
    editListButton = nil;
    newListItemButton = nil;
    doneEditingButton = nil;
    newScribbleListItemButton = nil;
    sortItemsSegmentedControl = nil;
    controlPanelImage = nil;
    highlightersContainerImageView = nil;
    pensContainerImageView =  nil;
    calendarListsDividerImageView = nil;
    controlPanelTypeSegmentedControl = nil;
    drawingToolsContainerView = nil;
    calendarListToolsContainerView = nil;
    toolsContainerView = nil;
    miniListPadTableView = nil;
    dayCalendarImageView = nil;
    dayCalendarDateLabel = nil;
    greenPenButton = nil;
    redPenButton = nil;
    bluePenButton = nil;
    blackPenButton = nil;
    yellowHighlighterButton = nil;
    redHighlighterButton = nil;
    whiteHighlighterButton = nil;
    penButton = nil;
    highlighterButton = nil;
    selectTaskImageView = nil;
    highlighterCoverView = nil;
    filterTagsButton = nil;
    filterTagsClearButton = nil;
    filterTagsFooterLine = nil;
    filterTagsLabel = nil;
    filterTagsImageView = nil;
    settingsBarButtonItem = nil;
    miniCalendarView = nil;
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
}

- (void)viewDidAppear:(BOOL)animated {
	if ([[refMasterTaskListCollection defaultSortOrder] isEqualToString:@"Last Selected"]) {
		// Update the sort order
		[sortItemsSegmentedControl setSelectedSegmentIndex:[refTaskList lastSortType]];
	}
    
    // Get Alarm List Item
    NSInteger alarmListItemID = [[NSUserDefaults standardUserDefaults] integerForKey:kAlarmListItem];
    // Get Alarm Task List
    NSInteger alarmTaskList = [[NSUserDefaults standardUserDefaults] integerForKey:kAlarmTaskList];
    
    if (alarmListItemID > 0 && alarmTaskList > -1){
        
        [self.myTableView reloadData];
        // Get index of list item
        NSInteger indexOfListItem = 0;
        for (ListItem *item in arrayTaskList) {
            if (item.listItemID == alarmListItemID) {
                indexOfListItem = [arrayTaskList indexOfObject:item];
                break;
            }
        }
        
        if (indexOfListItem < 15) {
            // Show alarm detail list item
            [self.myTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexOfListItem inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        }else{
            // Show alarm detail list item
            [self.myTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexOfListItem inSection:0] animated:YES scrollPosition:UITableViewRowAnimationAutomatic];//UITableViewRowAnimationAutomatic
        }
        
        selectRowPrompt = FALSE;
        highlighterSelected = EnumHighlighterColorNone;
        [self tableView:self.myTableView willSelectRowAtIndexPath:[NSIndexPath indexPathForRow:indexOfListItem inSection:0]];
    }
    
	[super viewDidAppear:animated];
}

#pragma mark - Notification Application
#pragma mark


- (void)applicationDidBecomeActive {
    if ([self.navigationController.topViewController isEqual:self] == TRUE) {
        if (gRunToodledoAutoSync == TRUE) {
            [MethodHelper showAlertViewWithTitle:@"Auto Sync"
                                      andMessage:@"Manage wants to sync with Toodledo.  Please return to 'My Lists' to automatically run the sync. (Note: This setting can be changed in 'Sync Settings')"
                                  andButtonTitle:@"Ok"];
        }
        
        // To update database after archiving Task List on Application launch.
        BOOL needUpdateDatabase = [[NSUserDefaults standardUserDefaults]boolForKey:kNeedUpdateDatebaseForArchiveTaskList];
        
        if (needUpdateDatabase) {
            if (refTaskList.completed) {
                [self listButtonItemAction];
            }
        }
    }
    
    // Check if auto sync wants to run
    /* */
}

- (void)applicationDidEnterBackground {
    // Dismiss popover
    // Hide all popover on list view
    [self dismissEditListItemPopOver];
    
    //[self dismissNewListItemPopOver];
}

- (void)viewWillAppear:(BOOL)animated {
	[self reloadTheme];
    //self.ischeckCompletedTask = NO;
	[super viewWillAppear:animated];
    
    self.splitViewController.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated {
	// Get rid of help overlay if it is there
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
	
	// Set current task list ID to -1 so it doesnt show up in main section
	refMasterTaskListCollection.currentTaskListID = -1;
	refTaskListCollection.currentTaskListID = -1;
	
	// Get badge with number of list items
	//gNumberOfListItemsWithDueDate = [MethodHelper numberOfListItemsForBadgeNotification];
	
	[super viewWillDisappear:animated];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
	if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
		// Update our tileviewframe
        CGRect frame = self.tileViewFrame;
        NSString *barButtonItemTitle = self.navigationItem.leftBarButtonItem.title;
        if ([barButtonItemTitle isEqualToString:@"My Categories"] == FALSE) { //** Folders
            self.tileViewFrame = CGRectMake(frame.origin.x - 256, frame.origin.y, frame.size.width, frame.size.height);
        } else if ([barButtonItemTitle isEqualToString:@"My Categories"]) { //** Folders
            self.tileViewFrame = CGRectMake(frame.origin.x - 256, frame.origin.y + 98, frame.size.width, frame.size.height);
        }
        
        // backButtonTitle = @"My Notebooks";
        
        // Update the background
		[canvassImage setFrame:kCanvassImagePortrait];
		[borderImage setFrame:kBorderImagePortrait];
		[pageImage setFrame:kPageImagePortrait];
		[listCompletedStampImage setFrame:kListCompletedStampPortrait];
		
		// Update the list tableview
		if ([self isFilterTagsMode] == FALSE && [self isSearchMode] == FALSE) {
			[self.myTableView setFrame:kListTableViewPortrait];
		} else {
			[self.myTableView setFrame:kFilterTagsListTableViewPortrait];
		}
		[headerLine1 setFrame:kHeaderLine1Portrait];
		[headerLine2 setFrame:kHeaderLine2Portrait];
		
		// Update the buttons positions
		[editListButton setFrame:kEditListButtonPortrait];
		[newListItemButton setFrame:kNewListItemButtonPortrait];
		[newScribbleListItemButton setFrame:kNewScribbleListItemButtonPortrait];
		[doneEditingButton setFrame:kDoneEditingButtonPortrait];
		[sortItemsSegmentedControl setFrame:kSortItemsSegmentedControlPortrait];
		[selectTaskImageView setFrame:kSelectTaskImageViewPortrait];
		[filterTagsButton setFrame:kFilterTagsButtonPortrait];
		[listOptionsButton setFrame:kListOptionsButtonPortrait];
		[penButton setFrame:kPenButtonPortrait];
        [highlighterButton setFrame:kHighlighterButtonPortrait];
        
		// Search objects
		[mySearchBar setFrame:kSearchBarPortrait];
		[searchScopeSegmentedControl setFrame:kSearchScopeSegmentPortrait];
		[searchButton setFrame:kSearchButtonPortrait];
		[clearSearchObjectsButton setFrame:kClearSearchButtonPortrait];
        [searchDateRangeButton setFrame:kSearchDateRangeButtonPortrait];
        [searchListRangeButton setFrame:kSearchListRangeButtonPortrait];
        [showAllTasksButton setFrame:kShowAllTasksButtonPortrait];
		[searchTitleLabel setFrame:kSearchTitleLabelPortrait];
		[searchDateRangeLabel setFrame:kSearchDateRangeLabelPortrait];
        
		// Update items due tableview and pocket
		[itemsDueTableView setFrame:kItemsDueTableViewPortrait];
		[itemsDuePocket setFrame:kItemsDuePocketPortrait];
		
		// Update title and date elements
		if ([titleTextField isEditing]) {
			[titleTextField setFrame:kTitleTextFieldEditingPortrait];
            [titleTextField setBackgroundColor:[UIColor whiteColor]];
		} else {
			[titleTextField setFrame:kTitleTextFieldPortrait];
            [titleTextField setBackgroundColor:[UIColor clearColor]];
		}
		[self updateListCompletedFrameForOrientation:interfaceOrientation];
		[dateLabelButton setFrame:kDateLabelPortrait];
		
		// Control panel objects
		[controlPanelImage setFrame:kControlPanelImagePortrait];
        [controlPanelTypeSegmentedControl setFrame:kControlPanelTypeSegmentPortrait];
		[toolsContainerView setFrame:kToolsContainerViewPortrait];
        
		// Set filter tag objects orientaiton
		[self filterTagObjectsOrientation:self.interfaceOrientation];
		
	} else if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
        // Update our tileviewframe
        CGRect frame = self.tileViewFrame;
        NSString *barButtonItemTitle = self.navigationItem.leftBarButtonItem.title;
        if ([barButtonItemTitle isEqualToString:@"My Categories"] == FALSE) { //** Folders
            self.tileViewFrame = CGRectMake(frame.origin.x + 256, frame.origin.y, frame.size.width, frame.size.height);
        } else if ([barButtonItemTitle isEqualToString:@"My Categories"]) { //** Folders
            self.tileViewFrame = CGRectMake(frame.origin.x + 256, frame.origin.y - 98, frame.size.width, frame.size.height);
        }
        
		// Update the background
		//[borderImage setImage:[UIImage imageNamed:@"Border.png"]];
		[canvassImage setFrame:kCanvassImageLandscape];
		[borderImage setFrame:kBorderImageLandscape];
		[pageImage setFrame:kPageImageLandscape];
		[listCompletedStampImage setFrame:kListCompletedStampLandscape];
		
		// Update the list tableview
		// Update the list tableview
		if ([self isFilterTagsMode] == FALSE && [self isSearchMode] == FALSE) {
			[self.myTableView setFrame:kListTableViewLandscape];
		} else {
			[self.myTableView setFrame:kFilterTagsListTableViewLandscape];
		}
		[headerLine1 setFrame:kHeaderLine1Landscape];
		[headerLine2 setFrame:kHeaderLine2Landscape];
		
		// Update the buttons positions
		[editListButton setFrame:kEditListButtonLandscape];
		[newListItemButton setFrame:kNewListItemButtonLandscape];
		[newScribbleListItemButton setFrame:kNewScribbleListItemButtonLandscape];
		[doneEditingButton setFrame:kDoneEditingButtonLandscape];
		[sortItemsSegmentedControl setFrame:kSortItemsSegmentedControlLandscape];
		[selectTaskImageView setFrame:kSelectTaskImageViewLandscape];
		[filterTagsButton setFrame:kFilterTagsButtonLandscape];
		[listOptionsButton setFrame:kListOptionsButtonLandscape];
        [penButton setFrame:kPenButtonLandscape];
        [highlighterButton setFrame:kHighlighterButtonLandscape];
		
		// Search objects
		[mySearchBar setFrame:kSearchBarLandscape];
		[searchScopeSegmentedControl setFrame:kSearchScopeSegmentLandscape];
		[searchButton setFrame:kSearchButtonLandscape];
		[clearSearchObjectsButton setFrame:kClearSearchButtonLandscape];
        [searchDateRangeButton setFrame:kSearchDateRangeButtonLandscape];
        [searchListRangeButton setFrame:kSearchListRangeButtonLandscape];
        [showAllTasksButton setFrame:kShowAllTasksButtonLandscape];
		[searchTitleLabel setFrame:kSearchTitleLabelLandscape];
		[searchDateRangeLabel setFrame:kSearchDateRangeLabelLandscape];
        
		// Update items due tableview and pocket
		[itemsDueTableView setFrame:kItemsDueTableViewLandscape];
		[itemsDuePocket setFrame:kItemsDuePocketLandscape];
		
		// Update title and date elements
		if ([titleTextField isEditing]) {
			[titleTextField setFrame:kTitleTextFieldEditingLandscape];
            [titleTextField setBackgroundColor:[UIColor whiteColor]];
		} else {
			[titleTextField setFrame:kTitleTextFieldLandscape];
            [titleTextField setBackgroundColor:[UIColor clearColor]];
		}
		[self updateListCompletedFrameForOrientation:interfaceOrientation];
		[dateLabelButton setFrame:kDateLabelLandscape];
		
		// Control panel objects
		[controlPanelImage setFrame:kControlPanelImageLandscape];
        [controlPanelTypeSegmentedControl setFrame:kControlPanelTypeSegmentLandscape];
		[toolsContainerView setFrame:kToolsContainerViewLandscape];
        
		// Set filter tag objects orientaiton
		[self filterTagObjectsOrientation:self.interfaceOrientation];
	}
    
    [self.myTableView reloadData];
}

#pragma mark -
#pragma mark Button Events

- (void)controlPanelTypeSegmentedControlAction {
    // Need an animation
    [UIView beginAnimations:@"Control Panel Slide Animation" context:nil];
    [UIView setAnimationDuration:0.4f];
    
    
    if ([controlPanelTypeSegmentedControl selectedSegmentIndex] == EnumControlPanelTypeCalendarList) {
        [drawingToolsContainerView setFrame:kDrawingToolsContainerHide];
        [calendarListToolsContainerView setFrame:kCalendarListToolsContainerShow];
    } else if ([controlPanelTypeSegmentedControl selectedSegmentIndex] == EnumControlPanelTypeDrawingTools) {
        [drawingToolsContainerView setFrame:kDrawingToolsContainerShow];
        [calendarListToolsContainerView setFrame:kCalendarListToolsContainerHide];
    }
    
    [UIView commitAnimations];
}

- (void)searchScopeSegmentedControlAction {
	// Just need to call the filter control, with correct scope
	NSString *scope = @"";
	
    if ([self searchRangeType] == EnumSearchRangeTypeAllLists) {
        searchTitleLabel.text = @"Search (All Lists)";
    } else if ([self searchRangeType] == EnumSearchRangeTypeThisList) {
        searchTitleLabel.text = @"Search (This List)";
    }
    
	switch (searchScopeSegmentedControl.selectedSegmentIndex) {
		case 0:
			scope = @"Title";
			break;
		case 1:
			scope = @"Notes";
			break;
		case 2:
			scope = @"Tags";
			break;
		case 3:
			scope = @"All";
			break;
		default:
			break;
	}
    
	[self filterContentForSearchText:[mySearchBar text] scope:scope];
	
	[self.myTableView reloadData];
}

- (void)clearSearchObjectsButtonAction {
    //self.ischeckCompletedTask = NO;
	self.isSearchMode = FALSE;
	
	[UIView beginAnimations:@"Clear Search Objects Animation" context:nil];
	[UIView setAnimationDuration:0.3f];
	
	
	mySearchBar.text = @"";
	[self hideSearchObjects:YES];
	
	
	if ([mySearchBar isFirstResponder]) {
		[mySearchBar resignFirstResponder];
	}
	
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
		[self.myTableView setFrame:kListTableViewPortrait];
	} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
		[self.myTableView setFrame:kListTableViewLandscape];
	}
	
	[self.myTableView reloadData];
	[UIView commitAnimations];
}

- (void)searchDateRangeButtonAction {
    // Remove the keyboard responder if it is up
    if ([mySearchBar isFirstResponder]) {
        [mySearchBar resignFirstResponder];
    }
    
    // Popup the date controller
    DateRangeViewController *controller = [[DateRangeViewController alloc] initWithStartDate:searchStartDateRange andEndDate:searchEndDateRange];
    controller.delegate = self;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    
    dateRangePopoverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
    [dateRangePopoverController presentPopoverFromRect:searchDateRangeButton.frame inView:self.view
                              permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
//    [controller release];
//    [navController release];
}

- (void)searchListRangeButtonAction {
    NSString *scope = @"";
	
	switch (searchScopeSegmentedControl.selectedSegmentIndex) {
		case 0:
			scope = @"Title";
			break;
		case 1:
			scope = @"Notes";
			break;
		case 2:
			scope = @"Tags";
			break;
		case 3:
			scope = @"All";
			break;
		default:
			break;
	}
    
    if ([self searchRangeType] == EnumSearchRangeTypeAllLists) {
        self.searchRangeType = EnumSearchRangeTypeThisList;
        [searchListRangeButton setSelected:YES];
        [searchTitleLabel setText:@"Search (This List)"];
    } else if ([self searchRangeType] == EnumSearchRangeTypeThisList) {
        self.searchRangeType = EnumSearchRangeTypeAllLists;
        [searchListRangeButton setSelected:NO];
        [searchTitleLabel setText:@"Search (All Lists)"];
        
    }
    
    [self filterContentForSearchText:mySearchBar.text scope:scope];
    
    // Reload the table view
    [self.myTableView reloadData];
}

- (void)showAllTasksButtonAction {
    // Clear the date range
    [searchDateRangeLabel setText:@"Date Range: None"];
    
    self.searchRangeType = EnumSearchRangeTypeAllLists;
    [searchListRangeButton setSelected:NO];
    searchStartDateRange = @"";
    searchEndDateRange = @"";
    
    // Remove anything in the search field
    [mySearchBar setText:@""];
    
    NSString *scope = @"";
	
	switch (searchScopeSegmentedControl.selectedSegmentIndex) {
		case 0:
			scope = @"Title";
			break;
		case 1:
			scope = @"Notes";
			break;
		case 2:
			scope = @"Tags";
			break;
		case 3:
			scope = @"All";
			break;
		default:
			break;
	}
	
	[self filterContentForSearchText:@"SPECIAL: GET ALL FROM MANAGE! :)" scope:scope];
	
    searchTitleLabel.text = @"Showing All Tasks";
    
	// Reload the table view
	[self.myTableView reloadData];
    
}

- (void)searchButtonAction {
	[UIView beginAnimations:@"Search Button Animation" context:nil];
	[UIView setAnimationDuration:0.3f];
	
	if (self.isSearchMode == FALSE) {
		// Show search buttons
		[self hideSearchObjects:NO];
		self.isSearchMode = TRUE;
        
        [mySearchBar becomeFirstResponder];
        
		if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
			[self.myTableView setFrame:kFilterTagsListTableViewPortrait];
		} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
			[self.myTableView setFrame:kFilterTagsListTableViewLandscape];
		}
	} else if (self.isSearchMode == TRUE) {
		// Hide search buttons
		[self hideSearchObjects:YES];
		self.isSearchMode = FALSE;
		
		if ([mySearchBar isFirstResponder]) {
			[mySearchBar resignFirstResponder];
		}
		
		if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
			[self.myTableView setFrame:kListTableViewPortrait];
		} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
			[self.myTableView setFrame:kListTableViewLandscape];
		}
	}
	[self.myTableView reloadData];
	[UIView commitAnimations];
}

- (void)listOptionsButtonAction {
	[listOptionsActionSheet showFromRect:[listOptionsButton frame]
								  inView:self.view animated:NO];
}

- (void)dateLabelButtonAction {
	// Popup the date controller
	NewDueDateTableViewController *controller = [[NewDueDateTableViewController alloc] initWithDueDate:refTaskList.creationDate
                                                                                            andDelegate:self];// autorelease];
	
	// Allocate the pop over controller, that will host the nav controller and view controller
	
	dateLabelPopoverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:controller];
    
	// Present the popover using the calculated location to display the arrow
	[dateLabelPopoverController presentPopoverFromRect:dateLabelButton.frame inView:self.view
                              permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
	
    
}


/*
 if ([self isFilterTagsMode]) {
 TaskList *taskList = [filterTagsTaskListCollection.lists objectAtIndex:section];
 return [taskList.listItems count];
 } else if ([self isSearchMode]) {
 TaskList *taskList = [filteredSearchTaskListCollection.lists objectAtIndex:section];
 return [taskList.listItems count];
 }
 
 return [refTaskList.listItems count];
 */

- (void)newScribbleListItemButtonAction {
    //self.ischeckCompletedTask = NO;
    self.isShowWritingTasksPopover = YES;
    //NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
    //    if (isShowCompletedTasks == 0) {
    //        if ([arrayTaskList count] > 0) {
    //
    //            // Make sure we scroll to top of tableview first
    //            [myTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
    //                               atScrollPosition:UITableViewScrollPositionTop animated:NO];
    //        }
    //    }else{
    //
    //        // Make sure we scroll to top of tableview first
    //        if ([refTaskList.listItems count] > 0) {
    //            [myTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
    //                               atScrollPosition:UITableViewScrollPositionTop animated:NO];
    //        }
    //    }
    
    //Make sure we scroll to top of tableview first
    if ([refTaskList.listItems count] > 0) {
        [self.myTableView scrollToTop];
        //        [myTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
        //                           atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    
    
	// Create new list item
	ListItem *newListItem = [[ListItem alloc] initEmptyListItemWithListID:refTaskList.listID
														   andParentTitle:refTaskList.title
																 DBInsert:YES
															 emptyTagList:NO];
	
	// Reload the table view data before adding a new list
	[self.myTableView reloadData];
    
    // Insert the new list item
    [refTaskList.listItems insertObject:newListItem atIndex:0];
	
	// Also insert in the master task list
	TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:currentTaskListID];
	
	[masterTaskList.listItems insertObject:newListItem atIndex:0];
    
	
	// Do insert into table view
	NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [self.myTableView beginUpdates];
	[self.myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    [self.myTableView endUpdates];
    
	
	// Release the new list item copy we hold
//	[newListItem release];
	
	// Now popup draw pad view controller
	
	
	// Update the selected row
	selectedRow = 0;
	
	// Set items due selected to false
	itemsDueSelected = FALSE;
	
	// Get first cell
	ListItemTableViewCell *cell = (ListItemTableViewCell *)[self.myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0
																												 inSection:0]];
	
    
	// For correctly positioning the popover controller
	CGPoint offset = [self.myTableView contentOffset];
	
	// Multiply selected row by 44, get center of tableview
	CGFloat xOrigin = self.myTableView.frame.origin.x + (self.myTableView.frame.size.width - 320);
	CGFloat yOrigin = cell.frame.origin.y + 22 + self.myTableView.frame.origin.y - offset.y;
	CGRect rectPopover = CGRectMake(xOrigin, yOrigin, 1, 1);
	
	// Get the list item we just added to the task list
	ListItem *listItem = [refTaskList.listItems objectAtIndex:0];
	
	// Deselect all pens
	[greenPenButton setSelected:NO];
	[bluePenButton setSelected:NO];
	[blackPenButton setSelected:NO];
	[redPenButton setSelected:NO];
	
	DrawPadViewController *drawPadViewController = [[DrawPadViewController alloc] initWithDelegate:self
																						   andSize:kDrawPadViewControllerSize
																					   andListItem:listItem
																					   andPenColor:penSelected
                                                                                andNewScribbleItem:TRUE];
    
	//UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:drawPadViewController];
	
	drawPadPopOverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:drawPadViewController];
	
	[drawPadPopOverController setDelegate:self];
//	[drawPadViewController release];
	//[navController release];
	
	UIPopoverArrowDirection direction = UIPopoverArrowDirectionUp;
	
	if (rectPopover.origin.y > 450) {
		direction = UIPopoverArrowDirectionDown;
	}
	
	[drawPadPopOverController presentPopoverFromRect:rectPopover inView:self.view
							permittedArrowDirections:direction animated:YES];
	
	// Update the select row prompt setting
	//[self setSelectRowPrompt:FALSE];
	
	refTaskList.mainViewRequiresUpdate = TRUE;
    
    // Reload the list pad
    [miniListPadTableView reloadData];
}

- (void)editListButtonAction {
	// Hide edit and add button, disable other controls and show a 'done' button
	[editListButton setHidden:YES];
	[newListItemButton setHidden:YES];
	[newScribbleListItemButton setHidden:YES];
	[penButton setHidden:YES];
	[highlighterButton setHidden:YES];
	[filterTagsButton setHidden:YES];
	[searchButton setHidden:YES];
	
	// Show done button item
	[doneEditingButton setHidden:NO];
	
	// Set table view editing mode to YES
	[self.myTableView setEditing:YES animated:YES];
}

- (void)helpBarButtonItemAction {
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
		return;
	}
	
	// Show the help overlay
	isHelpSelected = TRUE;
	
	UIButton *helpOverlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[helpOverlayButton setAlpha:0.75f];
	helpOverlayButton.tag = kHelpOverlayTag;
	[helpOverlayButton addTarget:self action:@selector(helpOverlayButtonAction:) forControlEvents:UIControlEventTouchDown];
	
	
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        if ([self isSearchMode] == FALSE) {
            [helpOverlayButton setImage:[UIImage imageNamed:@"LIST-SCREEN-PORTRAIT-OVERLAY.png"] forState:UIControlStateNormal];
            [helpOverlayButton setFrame:CGRectMake(0, 0, 768, 960)];
        } else {
            [helpOverlayButton setImage:[UIImage imageNamed:@"LIST-SCREEN-SEARCH-MODE-PORTRAIT-OVERLAY.png"] forState:UIControlStateNormal];
            [helpOverlayButton setFrame:CGRectMake(0, 0, 768, 960)];
        }
		
	} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        if ([self isSearchMode]) {
            [helpOverlayButton setImage:[UIImage imageNamed:@"LIST-SCREEN-SEARCH-MODE-LANDSCAPE-OVERLAY.png"] forState:UIControlStateNormal];
            [helpOverlayButton setFrame:CGRectMake(0, 0, 1024, 704)];
        } else if ([controlPanelTypeSegmentedControl selectedSegmentIndex] == EnumControlPanelTypeCalendarList) {
            [helpOverlayButton setImage:[UIImage imageNamed:@"LIST-SCREEN-CALENDAR-VIEW-LANDSCAPE-OVERLAY.png"] forState:UIControlStateNormal];
            [helpOverlayButton setFrame:CGRectMake(0, 0, 1024, 704)];
        } else if ([controlPanelTypeSegmentedControl selectedSegmentIndex] == EnumControlPanelTypeDrawingTools) {
            [helpOverlayButton setImage:[UIImage imageNamed:@"LIST-SCREEN-DRAWING-TOOLS-VIEW-LANDSCAPE-OVERLAY.png"] forState:UIControlStateNormal];
            [helpOverlayButton setFrame:CGRectMake(0, 0, 1024, 704)];
        }
		
	}
	
	[self.view addSubview:helpOverlayButton];
}


- (void)settingsBarButtonItemAction {
	if ([settingsPopoverController isPopoverVisible]) {
		// Do nothing
		[settingsPopoverController dismissPopoverAnimated:YES];
        settingsPopoverController = nil;
//        [settingsPopoverController release];
		return;
	}
    
    if (settingsPopoverController != nil) {
        settingsPopoverController = nil;
//        [settingsPopoverController release];
    }
	
	// Init the select view type controller4////////EnumPropertyCollectionApplicationList
	SettingsViewController *settingsViewController = [[SettingsViewController alloc] initWithDelegate:self andTagCollection:refTagCollection
																			andPropertyCollectionType:EnumPropertyCollectionApplication
																				andTaskListCollection:refMasterTaskListCollection];
	
	// Init the navigation controller that will hold the edit list item view controller
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
//	[settingsViewController release];
	
	// Allocate the pop over controller, that will host the nav controller and view controller
	settingsPopoverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
	[settingsPopoverController setDelegate:self];
//	[navController release];
	
	// Present the popover using the calculated location to display the arrow
	[settingsPopoverController presentPopoverFromBarButtonItem:settingsBarButtonItem
									  permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	
}

- (void)helpOverlayButtonAction:(id)sender {
	isHelpSelected = FALSE;
	UIButton *helpOverlayButton = (UIButton *)sender;
	[helpOverlayButton removeFromSuperview];
}

- (void)newListItemButtonAction {
	NewListItemViewController *newListItemViewController = [[NewListItemViewController alloc] initWithDelegate:self
																									   andSize:kNewListItemViewControllerSize
																							  andTagCollection:refTagCollection];
	
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:newListItemViewController];
	
    
	newListItemPopOverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
	[newListItemViewController setPopoverController:newListItemPopOverController];
	[newListItemPopOverController setDelegate:self];
//	[newListItemViewController release];
//	[navController release];
	
	[newListItemPopOverController presentPopoverFromRect:[newListItemButton frame] inView:self.view
                                permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
	
	refTaskList.mainViewRequiresUpdate = TRUE;
}

- (void)penButtonAction {
	if (selectRowPrompt == TRUE) {
		[self setSelectRowPrompt:FALSE];
		return;
	}
	
	// Next task is to ask user to select a row in the table
	penSelected = EnumPenColorBlack;
	
	// Do nothing if there are no items
	if ([refTaskList.listItems count] == 0) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	if (selectRowPrompt == FALSE) {
		[self setSelectRowPrompt:TRUE];
	} else {
		[self setSelectRowPrompt:FALSE];
	}
#if ENABLE_GOOGLE_ANALYTICS
    // track this event to Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker sendEventWithCategory:kCategory_event
                        withAction:kAction_edit
                         withLabel:kLabel_edit_by_pen
                         withValue:0];
#endif
}

- (void)highlighterButtonAction {
	if ([refTaskList.listItems count] == 0) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		// Do nothing
		return;
	}
	
	// Init the edit list item view controller
	HighlightersViewController *highlightersViewController = [[HighlightersViewController alloc] initWithDelegate:self andSize:kHighlightersPopoverSize];
	
	// Init the nav controller
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:highlightersViewController];
	
	
	// Allocate the pop over controller, that will host the nav controller and view controller
	highlightersPopoverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
//	[highlightersViewController release];
//	[navController release];
	
	[highlightersPopoverController setPassthroughViews:[NSArray arrayWithObject:myTableView]];
	[highlightersPopoverController setDelegate:self];
	
	// Present the popover using the calculated location to display the arrow
	[highlightersPopoverController presentPopoverFromRect:[highlighterButton frame] inView:self.view
								 permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
#if ENABLE_GOOGLE_ANALYTICS
    // track this event to Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker sendEventWithCategory:kCategory_event
                        withAction:kAction_edit
                         withLabel:kLabel_edit_highlighter
                         withValue:0];
#endif
}

- (void)doneEditingButtonAction {
	// Set table view editing to false
	[self.myTableView setEditing:NO animated:YES];
	
	// Hide done editing and show other buttons
	[doneEditingButton setHidden:YES];
	
	// Show other buttons
	[newListItemButton setHidden:NO];
	[newScribbleListItemButton setHidden:NO];
	[editListButton setHidden:NO];
	[penButton setHidden:NO];
	[highlighterButton setHidden:NO];
	[filterTagsButton setHidden:NO];
	[searchButton setHidden:NO];
}

- (void)listButtonItemAction {
    
    if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
    
	if ([titleTextField isFirstResponder]) {
		[titleTextField resignFirstResponder];
	}
    
    
	
    
    //        for (ListItem *listItem in self.completedTasksArray) {
    //            if (listItem.completed == NO && listItem.hasJustComplete == YES) {
    //                listItem.completed = YES;
    //            }
    //        }
    
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
    if (isShowCompletedTasks == 0) {
        for (ListItem *item in self.arrayTaskList) {
            for (ListItem *reItem in refTaskList.listItems) {
                if (item.listItemID == reItem.listItemID) {
                    reItem.completed = item.hasJustComplete;
                }
            }
            item.completed = item.hasJustComplete;
        }
    }
    
    
    [self.myTableView reloadData];
    //
    //    if ([self.completedRepeatTasksArray count] > 0) {
    //        for (NSArray *array in self.completedRepeatTasksArray) {
    //            //[self deleteRowsOnListViewWithRepeatListItem:array];
    //        }
    //    }
    
	// Time to do animation
	// Shrink page image
	// Hide tableview and control panel and buttons
	//[sortItemsSegmentedControl setHidden:YES];
	[myTableView setHidden:YES];
	[titleTextField setHidden:YES];
	[listCompletedButton setHidden:YES];
	[dateLabelButton setHidden:YES];
	[penButton setHidden:YES];
    [sortItemsSegmentedControl setHidden:YES];
	[editListButton setHidden:YES];
	[newListItemButton setHidden:YES];
	[newScribbleListItemButton setHidden:YES];
	[highlighterButton setHidden:YES];
	[headerLine1 setHidden:YES];
	[headerLine2 setHidden:YES];
	[filterTagsButton setHidden:YES];
	[filterTagsClearButton setHidden:YES];
	[filterTagsLabel setHidden:YES];
	[filterTagsImageView setHidden:YES];
	[filterTagsFooterLine setHidden:YES];
	[doneEditingButton setHidden:YES];
	[listCompletedStampImage setHidden:YES];
	[selectTaskImageView setHidden:YES];
	
	// Hide search objects
	[searchScopeSegmentedControl setHidden:YES];
	[searchButton setHidden:YES];
	[searchTitleLabel setHidden:YES];
	[searchDateRangeLabel setHidden:YES];
    [mySearchBar setHidden:YES];
	[listOptionsButton setHidden:YES];
	[clearSearchObjectsButton setHidden:YES];
    [searchDateRangeButton setHidden:YES];
    [searchListRangeButton setHidden:YES];
    [showAllTasksButton setHidden:YES];
    
	// Resort taskList as list sort (if needed)
	if (refTaskList != nil && refTaskList.currentSortType != EnumSortByList) {
		for (TaskList *taskList in refMasterTaskListCollection.lists) {
			taskList.currentSortType = EnumSortByList;
			[taskList sortListWithSortType:EnumSortByList];
		}
		
		// Save the last sort type (and main view requires update) before reloading
		NSInteger lastSortType = refTaskList.lastSortType;
		BOOL mainViewRequiresUpdate = refTaskList.mainViewRequiresUpdate;
		
		[refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
		refTaskList = [refTaskListCollection getTaskListWithID:currentTaskListID];
        
		[[refMasterTaskListCollection getTaskListWithID:currentTaskListID] setLastSortType:lastSortType];
		refTaskList.mainViewRequiresUpdate = mainViewRequiresUpdate;
		refTaskList.lastSortType = lastSortType;
	}
    
    if (isShowCompletedTasks == 0) {
        TaskList *mainTaskList = [refTaskListCollection getTaskListWithID:currentTaskListID];
        for (ListItem *mainItem in mainTaskList.listItems) {
            for (ListItem *editItem in mainTaskList.listItems) { // arrayTaskList
                if (mainItem.listItemID == editItem.listItemID) {
                    if (editItem.hasJustComplete == YES) {
                        //mainItem.completed = editItem.hasJustComplete;
                        
                        if (editItem.parentListItemID != -1) {// Has parent task
                            
                            // If task has parent task and hascompleted is YES -> task is completed
                            mainItem.completed = YES;
                            
                        }else{// Has no parent task
                            
                            if ([mainTaskList hasSubtasksForListItemAtIndex:[mainTaskList.listItems indexOfObject:editItem]] == YES) {// Has subtask
                                BOOL isAllTasksCompleted = YES;
                                // Get all subtask and check
                                for (ListItem *subItem in mainTaskList.listItems) {
                                    if (subItem.parentListItemID == editItem.listItemID) {
                                        // Check subtask is completed
                                        if (subItem.hasJustComplete != YES) {
                                            isAllTasksCompleted = NO;
                                            break;
                                        }
                                    }
                                }
                                
                                // All subtask is completed
                                if (isAllTasksCompleted == YES && editItem.hasJustComplete == YES) {
                                    // This task is completed
                                    mainItem.completed = YES;
                                }else{
                                    mainItem.completed = NO;
                                    mainItem.hasJustComplete = YES;
                                }
                                
                            }else{// Has no subtask
                                
                                mainItem.completed = editItem.hasJustComplete;
                            }
                            
                        }
                        
                    }else{
                        
                        // Check for task is completed
                        if (editItem.parentListItemID != -1) {// Has parent task
                            if (editItem.completed == YES) {
                                mainItem.hasJustComplete = editItem.completed;
                            }
                        }else{// Has no parent task
                            
                            if ([mainTaskList hasSubtasksForListItemAtIndex:[mainTaskList.listItems indexOfObject:editItem]] == YES) { // Has subtask
                                BOOL isAllTasksCompleted = YES;
                                // Get all subtask and check
                                for (ListItem *subItem in mainTaskList.listItems) {
                                    if (subItem.parentListItemID == editItem.listItemID) {
                                        // Check subtask is completed
                                        if (subItem.completed != YES) {
                                            isAllTasksCompleted = NO;
                                            break;
                                        }
                                    }
                                }
                                
                                // All subtask is completed
                                if (isAllTasksCompleted == YES && editItem.completed == YES) {
                                    mainItem.hasJustComplete = YES;
                                    mainItem.completed = YES;
                                }
                                if (isAllTasksCompleted == NO && editItem.completed == YES) {
                                    mainItem.hasJustComplete = YES;
                                    mainItem.completed = NO;
                                }
                            }else{// Has no subtask
                                
                                if (editItem.completed == YES) {
                                    mainItem.hasJustComplete = YES;
                                    mainItem.completed = YES;
                                }
                            }
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    
	[UIView beginAnimations:@"Shrink Page Animation" context:nil];
	[UIView setAnimationDuration:0.5f];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView setAnimationDelegate:self];
	
	if (self.tileViewFrame.size.width > 0) {
		[pageImage setFrame:self.tileViewFrame];
	} else {
#ifdef IS_FREE_BUILD
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
			[pageImage setFrame:CGRectMake(177, 180, 414, 487)];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
			[pageImage setFrame:CGRectMake(433, 82, 414, 487)];
		}
#else
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
			[pageImage setFrame:CGRectMake(177, 180, 414, 551)];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
			[pageImage setFrame:CGRectMake(433, 82, 414, 551)];
		}
#endif
	}
	
	[UIView commitAnimations];
    
    
}

- (void)greenPenButtonAction {
	highlighterSelected = EnumHighlighterColorNone;
	[self deselectAllHighlighters];
	
	if ([greenPenButton isSelected]) {
		[greenPenButton setSelected:NO];
		penSelected = EnumPenColorNone;
		if (selectRowPrompt == TRUE) {
			[self setSelectRowPrompt:FALSE];
		}
		return;
	}
	
	[self deselectAllPens];
	
	// Select the pen color
	penSelected = EnumPenColorGreen;
	
	// Do nothing if there are no items
	if ([refTaskList.listItems count] == 0 && [self isFilterTagsMode] == FALSE && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	} else if ([self isFilterTagsMode] && [filterTagsTaskListCollection.lists count] == 0 && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	} else if ([self isSearchMode] == TRUE && [filteredSearchTaskListCollection.lists count] == 0) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	[greenPenButton setSelected:YES];
	
	//if (selectRowPrompt == FALSE) {
    [self setSelectRowPrompt:TRUE];
	//} else {
	//	[self setSelectRowPrompt:FALSE];
	//}
}

- (void)bluePenButtonAction {
	highlighterSelected = EnumHighlighterColorNone;
	[self deselectAllHighlighters];
	
	if ([bluePenButton isSelected]) {
		[bluePenButton setSelected:NO];
		penSelected = EnumPenColorNone;
		if (selectRowPrompt == TRUE) {
			[self setSelectRowPrompt:FALSE];
		}
		return;
	}
	
	[self deselectAllPens];
	
	// Select the pen color
	penSelected = EnumPenColorBlue;
	
	// Do nothing if there are no items
	if ([refTaskList.listItems count] == 0 && [self isFilterTagsMode] == FALSE && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	} else if ([self isFilterTagsMode] && [filterTagsTaskListCollection.lists count] == 0 && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	} else if ([self isSearchMode] == TRUE && [filteredSearchTaskListCollection.lists count] == 0) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	[bluePenButton setSelected:YES];
    
	//if (selectRowPrompt == FALSE) {
    [self setSelectRowPrompt:TRUE];
	//} else {
	//	[self setSelectRowPrompt:FALSE];
	//}
}

- (void)blackPenButtonAction {
	highlighterSelected = EnumHighlighterColorNone;
	[self deselectAllHighlighters];
	
	if ([blackPenButton isSelected]) {
		[blackPenButton setSelected:NO];
		penSelected = EnumPenColorNone;
		if (selectRowPrompt == TRUE) {
			[self setSelectRowPrompt:FALSE];
		}
		return;
	}
	
	[self deselectAllPens];
	
	// Select the pen color
	penSelected = EnumPenColorBlack;
	
	// Do nothing if there are no items
	if ([refTaskList.listItems count] == 0 && [self isFilterTagsMode] == FALSE && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	} else if ([self isFilterTagsMode] && [filterTagsTaskListCollection.lists count] == 0 && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	} else if ([self isSearchMode] == TRUE && [filteredSearchTaskListCollection.lists count] == 0) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	[blackPenButton setSelected:YES];
	
	//if (selectRowPrompt == FALSE) {
    [self setSelectRowPrompt:TRUE];
	//} else {
	//	[self setSelectRowPrompt:FALSE];
	//}
}

- (void)redPenButtonAction {
	highlighterSelected = EnumHighlighterColorNone;
	[self deselectAllHighlighters];
	
	if ([redPenButton isSelected]) {
		[redPenButton setSelected:NO];
		penSelected = EnumPenColorNone;
		if (selectRowPrompt == TRUE) {
			[self setSelectRowPrompt:FALSE];
		}
		return;
	}
	
	[self deselectAllPens];
	
	// Select the pen color
	penSelected = EnumPenColorRed;
	
	// Do nothing if there are no items
	if ([refTaskList.listItems count] == 0 && [self isFilterTagsMode] == FALSE && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	} else if ([self isFilterTagsMode] && [filterTagsTaskListCollection.lists count] == 0 && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	} else if ([self isSearchMode] == TRUE && [filteredSearchTaskListCollection.lists count] == 0) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Pens require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	[redPenButton setSelected:YES];
	
	//if (selectRowPrompt == FALSE) {
    [self setSelectRowPrompt:TRUE];
	//} else {
	//	[self setSelectRowPrompt:FALSE];
	//}
}

- (void)redHighlighterButtonAction {
	if ([refTaskList.listItems count] == 0 && [self isFilterTagsMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		// Do nothing
		return;
	} else if ([self isFilterTagsMode] && [filterTagsTaskListCollection.lists count] == 0 && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		// Do nothing
		return;
	} else if ([self isSearchMode] == TRUE && [filteredSearchTaskListCollection.lists count] == 0) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	if ([redHighlighterButton isSelected]) {
		[redHighlighterButton setSelected:NO];
		highlighterSelected = EnumHighlighterColorNone;
		return;
	}
	
	[self deselectAllHighlighters];
	
	[redHighlighterButton setSelected:YES];
	
	highlighterSelected = EnumHighlighterColorRed;
}

- (void)yellowHighlighterButtonAction {
	if ([refTaskList.listItems count] == 0 && [self isFilterTagsMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		// Do nothing
		return;
	} else if ([self isFilterTagsMode] && [filterTagsTaskListCollection.lists count] == 0 && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		// Do nothing
		return;
	} else if ([self isSearchMode] == TRUE && [filteredSearchTaskListCollection.lists count] == 0) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	if ([yellowHighlighterButton isSelected]) {
		[yellowHighlighterButton setSelected:NO];
		highlighterSelected = EnumHighlighterColorNone;
		return;
	}
	
	[self deselectAllHighlighters];
	
	[yellowHighlighterButton setSelected:YES];
	
	highlighterSelected = EnumHighlighterColorYellow;
}

- (void)whiteHighlighterButtonAction {
	if ([refTaskList.listItems count] == 0 && [self isFilterTagsMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		// Do nothing
		return;
	} else if ([self isFilterTagsMode] && [filterTagsTaskListCollection.lists count] == 0 && [self isSearchMode] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		// Do nothing
		return;
	} else if ([self isSearchMode] == TRUE && [filteredSearchTaskListCollection.lists count] == 0) {
		[MethodHelper showAlertViewWithTitle:@"Warning"
								  andMessage:@"Priority highlighters require at least one task"
							  andButtonTitle:@"Ok"];
		return;
	}
	
	if ([whiteHighlighterButton isSelected]) {
		[whiteHighlighterButton setSelected:NO];
		highlighterSelected = EnumHighlighterColorNone;
		return;
	}
	
	[self deselectAllHighlighters];
	
	[whiteHighlighterButton setSelected:YES];
	
	highlighterSelected = EnumHighlighterColorWhite;
}

- (void)deselectAllHighlighters {
	[yellowHighlighterButton setSelected:NO];
	[redHighlighterButton setSelected:NO];
	[whiteHighlighterButton setSelected:NO];
}

- (void)deselectAllPens {
	[bluePenButton setSelected:NO];
	[greenPenButton setSelected:NO];
	[redPenButton setSelected:NO];
	[blackPenButton setSelected:NO];
}

- (void)filterTagsButtonAction {
    if (filterTagsPopoverController != nil) {
        filterTagsPopoverController = nil;
//        [filterTagsPopoverController release];
    }
    
	FilterListItemTagViewController *filterTagController = [[FilterListItemTagViewController alloc] initWithFilterListItemTagCollection:filterListItemTagCollection
																													   andTagCollection:refTagCollection];
	
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:filterTagController];
	
	
    
    
	filterTagsPopoverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
	[filterTagsPopoverController setDelegate:self];
//	[filterTagController release];
//	[navController release];
	
	[filterTagsPopoverController presentPopoverFromRect:[filterTagsButton frame] inView:self.view
                               permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

- (void)filterTagsClearButtonAction {
	// Clear all filter tags
	[filterListItemTagCollection.listItemTags removeAllObjects];
	
	// Reposition elements
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
    
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
		[self.myTableView setFrame:kListTableViewPortrait];
	} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
		[self.myTableView setFrame:kListTableViewLandscape];
	}
	
	[UIView commitAnimations];
	// Hide objects
	[self hideFilterTagObjects:YES];
	[filterTagsLabel setText:@""];
	[self filterTagObjectsOrientation:self.interfaceOrientation];
	
	// Reload the table view
	[self.myTableView reloadData];
	
}

- (void)listCompletedButtonAction {
	if ([listCompletedButton isSelected] == YES) {
		[listCompletedButton setSelected:NO];
		[listCompletedStampImage setHidden:YES];
		[refTaskList setCompleted:NO];
	} else if ([listCompletedButton isSelected] == NO) {
		[listCompletedButton setSelected:YES];
		[listCompletedStampImage setHidden:NO];
		[refTaskList setCompleted:YES];
	}
    
    // Update completed stamp when user complete or uncomplete task list
    refTaskList.mainViewRequiresUpdate = YES;
	
	// Update completed status of the list
	
	// Get task list in master task list and update
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		if (taskList.listID == currentTaskListID) {
			taskList.completed = [listCompletedButton isSelected];
            
			if ([taskList completed] == TRUE) {
				taskList.completedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
				refTaskList.completedDateTime = taskList.completedDateTime;
                
                // If Task List marked to complete, All list item in it will remove alarm
                for (ListItem *item in taskList.listItems) {
                    
                    // Init any loaded alarms
                    AlarmCollection *alarmCollection = [[AlarmCollection alloc] init];
                    [alarmCollection loadAlarmsWithListItemID:item.listItemID];
                    
                    if (item.hasAlarms == TRUE) {
                        
                        // Set has alarm is false
                        item.hasAlarms = FALSE;
                        
                        // Update data for list item
                        [item updateDatabaseWithoutModifiedLog];
                        
                        for (Alarm *alarm in alarmCollection.alarms) {
                            // Remove notification
                            [alarm removeFromNotificationQueue];
                            
                            // Delete alarm in database
                            [alarm deleteSelfFromDatabase];
                        }
                        
                        // Update refTask List on view
                        [self updateTaskListCollection:refTaskListCollection withListItem:item];
                        
                    }
                    
//                    [alarmCollection release];
                }
                
			} else if ([taskList completed] == FALSE) {
                
				taskList.completedDateTime = @"";
				refTaskList.completedDateTime = @"";
			}
            
			[taskList updateDatabase];
		}
	}
	
	// Reload the collections
	//[self reloadCollections];
    [self.myTableView reloadData];
	
	// Set reftask list needs update
	refTaskList.mainViewRequiresUpdate = TRUE;
	
	// If list is showing in filter tags, then if its complete, put (Completed) next to title
    
    // Show an alert view
    if ([listCompletedButton isSelected] == YES) {
        UIAlertView *listCompletedAlertView = [[UIAlertView alloc] initWithTitle:@"List Completed"
                                                                         message:@"This list has now been marked complete.  Please note that this list (along with contained tasks) can now be moved to the archives."
                                                                        delegate:nil
                                                               cancelButtonTitle:@"Ok"
                                                               otherButtonTitles:nil];
        [listCompletedAlertView show];
//        [listCompletedAlertView release];
    }
}

#pragma mark -
#pragma mark UIAnimation Delegate Methods

// Animation has stopped
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
	if ([animationID isEqualToString:@"Shrink Page Animation"]) {
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:kHasAlarm] == YES){
            //do something
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kHasAlarm];
            [self.navigationController popToRootViewControllerAnimated:NO];
        }else{
            [self.navigationController popViewControllerAnimated:NO];
        }
	}
	
	if ([animationID isEqualToString:@"Clear Search Objects Animation"]) {
		[self.myTableView reloadData];
	}
	
    if ([animationID isEqualToString:@"Page Curl Animation"]) {
        // Send page image to back
        [self.view sendSubviewToBack:pageImage];
        [self.view sendSubviewToBack:canvassImage];
        [miniListPadTableView setUserInteractionEnabled:TRUE];
    }
    
    if ([animationID isEqualToString:@"Page to Note Curl Animation"]) {
        // Swapping with note
        [self.delegate swapWithNote:refTaskList usingFrame:self.tileViewFrame];
        
        [miniListPadTableView setUserInteractionEnabled:TRUE];
    }
    
    /*
     [UIView beginAnimations:@"Page to Note Curl Animation" context:nil];
     [UIView setAnimationDuration:1.0f];
     [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:pageImage cache:YES];
     [UIView setAnimationDelegate:self];
     [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
     [UIView commitAnimations];
     
     // Best to use a delegate from main view controller
     
     
     */
}


#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
	
	ListItem *listItem = nil;
	
    BOOL switchingToNoteView = FALSE;
	if ([self isFilterTagsMode] == FALSE && [self isSearchMode] == FALSE) {
        if ([refTaskList.listItems count] > 0) {
            listItem = [arrayTaskList objectAtIndex:indexPath.row];
        } else {
            switchingToNoteView = TRUE;
        }
	} else if ([self isFilterTagsMode]) {
		TaskList *taskList = [filterTagsTaskListCollection.lists objectAtIndex:indexPath.section];
        if (indexPath.row < taskList.listItems.count) {
            listItem = [taskList.listItems objectAtIndex:indexPath.row];
        }
        
	} else if ([self isSearchMode]) {
		TaskList *taskList = [filteredSearchTaskListCollection.lists objectAtIndex:indexPath.section];
        if (indexPath.row < taskList.listItems.count) {
            listItem = [taskList.listItems objectAtIndex:indexPath.row];
        }
	}
	
    ListItemTableViewCell *cell = (ListItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ListItemTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] ;//autorelease];
		//[cell addSubview:[self getCellNotesIcon]];
		
	}
    
//    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
//        cell.frame = CGRectMake(0, 0, 125, 21);
//    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
//        cell.frame = CGRectMake(0, 0, 855, 21);
//    }
	
    [cell changeResourceSize];
    
    if (switchingToNoteView == TRUE) {
        return cell;
    }
    
	if ([listItem.dueDate length] > 0) {
		/*NSDate *theDate = [MethodHelper dateFromString:listItem.dueDate usingFormat:K_DATEONLY_FORMAT];
         NSDate *today = [NSDate date];
         
         NSTimeInterval difference = [theDate timeIntervalSinceDate:today];*/
		
		NSString *dateToDisplay = @"";
		NSDate *theDate = [MethodHelper dateFromString:listItem.dueDate usingFormat:K_DATEONLY_FORMAT];
		
		// Past is negative, future is positive
		NSInteger difference = [listItem getDueDateDifferenceFromToday];
		
		// 86400 seconds in one day
		if (difference <= -1 && [listItem completed] == FALSE) {
			dateToDisplay = @"overdue";
		} else {
			dateToDisplay = [MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterMediumStyle withYear:NO];
		}
        
		cell.taskDate = dateToDisplay;
		//cell.detailTextLabel.text = dateToDisplay;
	} else {
		cell.taskDate = @"";
		//cell.detailTextLabel.text = @"";
	}
    
    // Set indent for task.
	if (refTaskList.currentSortType == EnumSortByList && ![self isFilterTagsMode] && ![self isSearchMode]) {
		[cell setIndentForListItem:listItem];
	} else {
		[cell setStandardIndent];
	}
    
    // Set up tags
	[cell loadTagsForListItem:listItem andTagCollection:refTagCollection];
	
    // Load scribble image (has to come straight after tags, to pick up taskItemFrame changes)
	[cell loadScribbleImageForListItem:listItem andIsPreviewList:NO];
    
    // Check to see if this is a repeating task
	if ([listItem recurringListItemID] != -1) {
		[cell loadRepeatingTaskImageForListItem:listItem];
	} else {
		cell.repeatingTaskImageView.image = nil;
	}
    
    // Check to see if this is a alarm task
    if (listItem.hasAlarms == TRUE) {
        
        // Init any loaded alarms
        AlarmCollection *alarmCollection = [[AlarmCollection alloc] init];
        [alarmCollection loadAlarmsWithListItemID:listItem.listItemID];
        
        // Update reflistItem data if no alarms found
        if ([alarmCollection.alarms count] == 0) {
            
            // Set has alarm is false
            listItem.hasAlarms = FALSE;
            [listItem updateDatabase];
            
            cell.alarmTaskImageView.image = nil;
            
        }else{
            if (listItem.completed != YES && listItem.hasJustComplete != YES) {
                [cell loadAlarmTaskImage:@"AlarmIcon.png" andListItem:listItem];
            }else{
                cell.alarmTaskImageView.image = nil;
            }
        }
        
    }else{
        cell.alarmTaskImageView.image = nil;
    }
    
    
	// Get the cell notes icon
    if ([listItem.notes length] > 0) {
        [cell loadCellNotesImage];
    } else {
        cell.cellNotesImageView.image = nil;
    }
	//UIImageView *cellNotesIcon = (UIImageView *)[cell viewWithTag:kNotesIconTag];
	
	cell.tag = listItem.listItemID;
	cell.delegate = self;
	cell.taskCompleted.tag = listItem.listItemID;
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	//cell.taskItem.text = [NSString stringWithFormat:@"%@", listItem.title];
    
    
	cell.taskItem = [NSString stringWithFormat:@"%@", listItem.title];
	
	//[cell.detailTextLabel setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
	//[cell.detailTextLabel setTextColor:[UIColor darkTextColor]];
	
	// Display cell notes icon if notes is not empty
//	if ([listItem.notes length] > 0) {
//		[cell loadCellNotesImage];
//	} else {
//		cell.cellNotesImageView.image = nil;
//	}
    
	
	// Display cell notes icon if notes is not empty
	/*if ([listItem.notes length] > 0) {
     UIFont *myFont = [UIFont fontWithName:@"Helvetica" size:14.0];
     // Get the width of a string ...
     
     CGSize size = [cell.taskItem sizeWithFont:myFont];
     
     if (size.width > cell.taskItemFrame.size.width) {
     size.width = cell.taskItemFrame.size.width;
     }
     
     //[cellNotesIcon setFrame:CGRectMake(cell.taskItem.frame.origin.x + cell.taskItem.frame.size.width + 35,
     //								   3, cellNotesIcon.frame.size.width, cellNotesIcon.frame.size.height)];
     
     [cellNotesIcon setFrame:CGRectMake(cell.taskItemFrame.origin.x + size.width,
     3, cellNotesIcon.frame.size.width, cellNotesIcon.frame.size.height)];
     
     [cellNotesIcon setHidden:NO];
     } else {
     [cellNotesIcon setHidden:YES];
     }*/
    
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
    if (isShowCompletedTasks == 1) {
        [cell.taskCompleted setSelected:listItem.completed];
        if (listItem.hasJustComplete == YES) {
            [cell.taskCompleted setSelected:listItem.hasJustComplete];
        }
    } else {
        [cell.taskCompleted setSelected:listItem.hasJustComplete];
    }
    
	//[cell.taskCompleted setSelected:listItem.completed];
	
    cell.priority = listItem.priority;
    
	if (listItem.hasJustComplete || listItem.completed) {
		[cell.tagsView setAlpha:0.3f];
        [cell.taskCompleted setAlpha:0.3f];
        [cell.scribbleImageView setAlpha:0.3f];
		[cell setHighlightStyle:EnumHighlighterColorNone];
	} else {
        [cell.taskCompleted setAlpha:1.0f];
        [cell.scribbleImageView setAlpha:1.0f];
        [cell.tagsView setAlpha:1.0f];
		[cell setHighlightStyle:listItem.priority];
	}
    
	
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    //NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
	if ([self isFilterTagsMode]) {
		return [filterTagsTaskListCollection.lists count];
	} else if ([self isSearchMode]) {
        return [filteredSearchTaskListCollection.lists count];
	}
	
    return 1;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
	if ([self isFilterTagsMode]) {
        
        TaskList *taskList = [filterTagsTaskListCollection.lists objectAtIndex:section];
        return [taskList.listItems count];
        
	} else if ([self isSearchMode]) {
        
        TaskList *taskList = [filteredSearchTaskListCollection.lists objectAtIndex:section];
        return [taskList.listItems count];
	}
    
    if (isShowCompletedTasks == 0) {
        
        self.arrayTaskList = [[NSMutableArray alloc] init];
        
        for (ListItem *item in refTaskList.listItems) {
            if (item.completed != YES) {
                [self.arrayTaskList addObject:item];
            }
        }
        
    }else{
        self.arrayTaskList = nil;
        self.arrayTaskList = [refTaskList.listItems mutableCopy];
    }
    
    return [self.arrayTaskList count];
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if ([self isFilterTagsMode]) {
		TaskList *taskList = [filterTagsTaskListCollection.lists objectAtIndex:section];
		NSString *listTitle = taskList.title;
		
		if ([taskList completed] == TRUE) {
			listTitle = [NSString stringWithFormat:@"(Completed) %@", listTitle];
		}
		return listTitle;
	} else if ([self isSearchMode]) {
		TaskList *taskList = [filteredSearchTaskListCollection.lists objectAtIndex:section];
		NSString *listTitle = taskList.title;
		
		if ([taskList completed] == TRUE) {
			listTitle = [NSString stringWithFormat:@"(Completed) %@", listTitle];
		}
		return listTitle;
	}
	
	return @"";
}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
	if ([titleTextField isFirstResponder]) {
		return indexPath;
	}
	
	// Update the selected row
	selectedRow = indexPath.row;
	
	// Set items due selected to false
	itemsDueSelected = FALSE;
    
    // Get Alarm List Item
    NSInteger alarmListItemID = [[NSUserDefaults standardUserDefaults] integerForKey:kAlarmListItem];
    // Get Alarm Task List
    NSInteger alarmTaskList = [[NSUserDefaults standardUserDefaults] integerForKey:kAlarmTaskList];
	
	ListItemTableViewCell *cell = (ListItemTableViewCell *)[self.myTableView cellForRowAtIndexPath:indexPath];
	
	// For correctly positioning the popover controller
	CGPoint offset = [self.myTableView contentOffset];
	
	// Multiply selected row by 44, get center of tableview
	CGFloat xOrigin = self.myTableView.frame.origin.x + (self.myTableView.frame.size.width - 320 - 60);
	CGFloat yOrigin = cell.frame.origin.y + 22 + self.myTableView.frame.origin.y - offset.y;
	CGRect rectPopover = CGRectMake(xOrigin, yOrigin, 1, 1);
	
	// Get the selected list item
	ListItem *listItem = nil;
	if ([self isFilterTagsMode]) {
		TaskList *taskList = [filterTagsTaskListCollection.lists objectAtIndex:indexPath.section];
        
        if (alarmListItemID > 0 && alarmTaskList > -1){
            listItem = [taskList.listItems objectAtIndex:indexPath.row];
        }else{
            listItem = [taskList.listItems objectAtIndex:[taskList getIndexOfListItemWithID:cell.tag]];
        }
        
		taskList.mainViewRequiresUpdate = TRUE;
	} else if ([self isSearchMode]) {
		TaskList *taskList = [filteredSearchTaskListCollection.lists objectAtIndex:indexPath.section];
        
        if (alarmListItemID > 0 && alarmTaskList > -1){
            listItem = [taskList.listItems objectAtIndex:indexPath.row];
        }else{
            listItem = [taskList.listItems objectAtIndex:[taskList getIndexOfListItemWithID:cell.tag]];
        }
		taskList.mainViewRequiresUpdate = TRUE;
	} else {
        
        //        if (isShowCompletedTasks == 0) {
        //            listItem = [arrayTaskList objectAtIndex:selectedRow];
        //        }else{
        //            listItem = [refTaskList.listItems objectAtIndex:[refTaskList getIndexOfListItemWithID:cell.tag]];
        //        }
        if (alarmListItemID > 0 && alarmTaskList > -1){
            listItem = [arrayTaskList objectAtIndex:indexPath.row];
        }else{
            listItem = [refTaskList.listItems objectAtIndex:[refTaskList getIndexOfListItemWithID:cell.tag]];
        }
        
        
		refTaskList.mainViewRequiresUpdate = TRUE;
	}
	
    // Update current selected list Item.
    self.currentListItem = listItem;
    
	if (selectRowPrompt == FALSE && highlighterSelected == EnumHighlighterColorNone) {
		// Init the edit list item view controller
		
		// For a sub task view as well
		/*CGSize editViewControllerSize = CGSizeZero;
         if (listItem.parentListItemID == -1) {
         editViewControllerSize = kEditListItemViewControllerSize;
         } else {
         editViewControllerSize = kEditSubListItemViewControllerSize;
         }*/
        
        if (alarmListItemID > 0 && alarmTaskList > -1){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kHasAlarm];;
            // Set Alarm List Item is -1
            [[NSUserDefaults standardUserDefaults] setInteger:-1 forKey:kAlarmListItem];
            // Set Alarm Task List is -1
            [[NSUserDefaults standardUserDefaults] setInteger:-1 forKey:kAlarmTaskList];
        }
		
		BOOL isFilterOrSearchMode = FALSE;
		
		if ([self isFilterTagsMode] || [self isSearchMode]) {
			isFilterOrSearchMode = TRUE;
		}
		
        //		EditListItemViewController *editListItemViewController = [[EditListItemViewController alloc] initWithDelegate:self andSize: CGSizeMake(320, 340)
        //                                                                                                          andListItem:listItem andTagCollection:refTagCollection
        //																								  andIsFilterTagsMode:isFilterOrSearchMode andTaskListCollection:refMasterTaskListCollection
        //                                                                                                didAppearFromListView:TRUE];
        
        EditListItemViewController *editListItemViewController = [[EditListItemViewController alloc] initWithDelegate:self andSize:kEditListItemViewControllerSize andListItem:currentListItem andTagCollection:refTagCollection andIsFilterTagsMode:isFilterOrSearchMode andTaskListCollection:refMasterTaskListCollection andTaskList:refTaskList didAppearFromListView:TRUE];
        
		editListItemViewController.popoverRect = rectPopover;
        
		// Init the navigation controller that will hold the edit list item view controller
		UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:editListItemViewController];
		
		
		// Allocate the pop over controller, that will host the nav controller and view controller
        if (editListItemPopOverController != nil) {
            editListItemPopOverController = nil;
        }
        
		editListItemPopOverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
		
        
		[editListItemViewController setPopoverController:editListItemPopOverController];
		
//		[editListItemViewController release];
//		[navController release];
		
		[editListItemPopOverController setDelegate:self];
		
		// Present the popover using the calculated location to display the arrow
		[editListItemPopOverController presentPopoverFromRect:rectPopover inView:self.view
									 permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
        
        
	} else if (selectRowPrompt == TRUE && highlighterSelected == EnumHighlighterColorNone) {
		// Deselect all pens
		[greenPenButton setSelected:NO];
		[bluePenButton setSelected:NO];
		[blackPenButton setSelected:NO];
		[redPenButton setSelected:NO];
		
        id drawPadViewController = [[DrawPadViewController alloc] initWithDelegate:self
                                                                           andSize:kDrawPadViewControllerSize
                                                                       andListItem:listItem
                                                                       andPenColor:penSelected
                                                                andNewScribbleItem:FALSE];
        
        UINavigationController *navController;
        
        BOOL isPNGScribble = [drawPadViewController isPNGScribble];
        if (isPNGScribble) {
            // Need to release the existing drawPadViewController and load the 'old' one
//            [drawPadViewController release];
            
            
            drawPadViewController = [[PngDrawPadViewController alloc] initWithDelegate:self
                                                                               andSize:kPngDrawPadViewControllerSize
                                                                           andListItem:listItem
                                                                           andPenColor:(EnumPngPenColor)penSelected
                                                                    andNewScribbleItem:FALSE];
            navController = [[UINavigationController alloc] initWithRootViewController:drawPadViewController];
        }
        
        //UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:drawPadViewController];
        
        
        if (isPNGScribble == FALSE) {
            drawPadPopOverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:drawPadViewController];
        } else {
            drawPadPopOverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
//            [navController release];
        }
        
        [drawPadPopOverController setDelegate:self];
//		[drawPadViewController release];
		//[navController release];
		
		UIPopoverArrowDirection direction = UIPopoverArrowDirectionUp;
		
		if (rectPopover.origin.y > 450) {
			direction = UIPopoverArrowDirectionDown;
		}
		
		[drawPadPopOverController presentPopoverFromRect:rectPopover inView:self.view
								permittedArrowDirections:direction animated:YES];
		
		// Update the select row prompt setting
		[self setSelectRowPrompt:FALSE];
		
		refTaskList.mainViewRequiresUpdate = TRUE;
	} else if (selectRowPrompt == FALSE && highlighterSelected != EnumHighlighterColorNone) {
		// Deselect all highlighters
		[redHighlighterButton setSelected:NO];
		[yellowHighlighterButton setSelected:NO];
		[whiteHighlighterButton setSelected:NO];
		
		// Exit and do nothing if list item is completed
		if ([listItem completed] == TRUE) {
			return nil;
		}
		
		//if (indexPath.row > [refTaskList.listItems count] - 1) {
        //			// Outside of range, change selected highlighter
        //			highlighterSelected = EnumHighlighterColorNone;
        //
        //			// Dismiss view controller
        //			if ([highlightersPopoverController isPopoverVisible]) {
        //				[highlightersPopoverController dismissPopoverAnimated:YES];
        //			}
        //
        //			// Exit
        //			return nil;
        //		}
		// A highlighter has been chosen, make changes
		listItem.priority = highlighterSelected;
		
		// Set highlighter color to none if the higlighter popover is not visible
		if ([highlightersPopoverController isPopoverVisible] == FALSE) {
			highlighterSelected = EnumHighlighterColorNone;
		}
		
		[listItem updateDatabase];
		
		// Also need to update other collections
		[self updateMasterCollectionWithListItem:listItem];
		
		[self.myTableView reloadData];
		
		refTaskList.mainViewRequiresUpdate = TRUE;
	}
	
	// Return nil to select nothing
	return nil;
}


// Tells the delegate that the table view is about to go into editing mode.
- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
	if (selectRowPrompt == FALSE && highlighterSelected != EnumHighlighterColorNone) {
		ListItemTableViewCell *cell = (ListItemTableViewCell *)[self.myTableView cellForRowAtIndexPath:indexPath];
		
		ListItem *listItem = nil;
		if ([self isFilterTagsMode]) {
			TaskList *taskList = [filterTagsTaskListCollection.lists objectAtIndex:indexPath.section];
			listItem = [taskList.listItems objectAtIndex:[taskList getIndexOfListItemWithID:cell.tag]];
			taskList.mainViewRequiresUpdate = TRUE;
		} else if ([self isSearchMode]) {
			TaskList *taskList = [filteredSearchTaskListCollection.lists objectAtIndex:indexPath.section];
			listItem = [taskList.listItems objectAtIndex:[taskList getIndexOfListItemWithID:cell.tag]];
			taskList.mainViewRequiresUpdate = TRUE;
		} else {
			listItem = [refTaskList.listItems objectAtIndex:[refTaskList getIndexOfListItemWithID:cell.tag]];
			refTaskList.mainViewRequiresUpdate = TRUE;
		}
		
		// Deselect all highlighters
		[redHighlighterButton setSelected:NO];
		[yellowHighlighterButton setSelected:NO];
		[whiteHighlighterButton setSelected:NO];
		
		// Exit if list item completed (must also reload tableview data to prevent delete showing)
		if ([listItem completed] == TRUE) {
			[self.myTableView reloadData];
			return;
		}
		
		listItem.priority = highlighterSelected;
		
		// Set highlighter color to none if the higlighter popover is not visible
		if ([highlightersPopoverController isPopoverVisible] == FALSE) {
			highlighterSelected = EnumHighlighterColorNone;
		}
		
		[listItem updateDatabase];
		
		// Also need to update other collections
		[self updateMasterCollectionWithListItem:listItem];
		
		[self.myTableView reloadData];
	}
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		// Get the index of the list item
		ListItemTableViewCell *cell = (ListItemTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
		
		// Can do the delete by always doing a lookup on complete task collection first and using that to
		// check if there are any subtasks, of course all lists get this list item deleted
		// Can get the list item from the found task list
		NSInteger listItemID = cell.tag;
		
		// Need to find which task list this is in
		for (TaskList *taskList in refMasterTaskListCollection.lists) {
			NSInteger listItemIndex = [taskList getIndexOfListItemWithID:listItemID];
			if (listItemIndex != -1) {
				//ListItem *listItem = [taskList.listItems objectAtIndex:listItemIndex];
				
				// Check if the list item has sub tasks
				if ([taskList hasSubtasksForListItemAtIndex:listItemIndex]) {
					// Has sub tasks
					// This task has sub tasks, need to warn the user
					UIAlertView *deleteAlert = [[UIAlertView alloc] initWithTitle:@"Warning: Task has subtasks"
																		  message:@"Deleting this task will also delete its subtasks"
																		 delegate:self
																cancelButtonTitle:@"Cancel"
																otherButtonTitles:@"Ok", nil];
					[deleteAlert setTag:listItemID];
					[deleteAlert show];
//					[deleteAlert release];
					
					// Exit this method
					return;
				} else {
					// Delete the list item
					[taskList deleteListItemAtIndex:listItemIndex permanent:NO];
					
					// Specify that task list requires update
					taskList.mainViewRequiresUpdate = YES;
					
                    // Reload collections
                    [self reloadCollections];
                    
                    // Deleting the currently selected task list
                    /* progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:progressHUD];
                     progressHUD.delegate = self;
                     progressHUD.labelText = @"Reloading task collections...";*/
                    
                    
                    
                    // This crashes it as it runs in a different thread
                    //[progressHUD showWhileExecuting:@selector(reloadCollections) onTarget:self withObject:nil animated:YES];
                    
					[self.myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
				}
				
			}
		}
	}
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	// If the user has not moved the page to a new row, exit and do nothing
	if (fromIndexPath.row == toIndexPath.row) {
		// Do nothing
		return;
	}
	
	TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:currentTaskListID];
	
	NSInteger fromListItemID = [[refTaskList.listItems objectAtIndex:fromIndexPath.row] listItemID];
	NSInteger toListItemID = [[refTaskList.listItems objectAtIndex:toIndexPath.row] listItemID];
	
	NSInteger fromIndex = [masterTaskList getIndexOfListItemWithID:fromListItemID];
	NSInteger toIndex = [masterTaskList getIndexOfListItemWithID:toListItemID];
	
	BOOL movingParentWithSubtasks = [masterTaskList moveListItemFrom:fromIndex to:toIndex];
    
	// Reloading collections
    [self reloadCollections];
    /*progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
     [self.navigationController.view addSubview:progressHUD];
     progressHUD.delegate = self;
     progressHUD.labelText = @"Reloading task collections...";
     [progressHUD showWhileExecuting:@selector(reloadCollections) onTarget:self withObject:nil animated:YES];*/
	
	if (movingParentWithSubtasks == TRUE) {
		[self.myTableView beginUpdates];
		
		[self.myTableView endUpdates];
		[self.myTableView reloadData];
	}
	
	refTaskList.mainViewRequiresUpdate = YES;
    self.arrayTaskList = [refTaskList.listItems mutableCopy];
}

// Whether to allow moving the selected row or not
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// No moving in filter tags mode (or in search mode)
	if ([self isFilterTagsMode] || [self isSearchMode]) {
		return NO;
	}
	
	//ListItem *listItem = [refTaskList.listItems objectAtIndex:indexPath.row];
	
	// Need to check whether next list item is a sub list item of this one and that we are not at last element
	/*if (listItem.parentListItemID == -1 && indexPath.row < [refTaskList.listItems count] - 1) {
     ListItem *nextListItem = [refTaskList.listItems objectAtIndex:indexPath.row + 1];
     if (nextListItem.parentListItemID != -1) {
     return NO;
     }
     }*/
	
	if (indexPath.row < [refTaskList.listItems count] && refTaskList.currentSortType == EnumSortByList) {
		return YES;
	}
	
	return NO;
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
	NSIndexPath *indexPath = proposedDestinationIndexPath;
	if(proposedDestinationIndexPath.row >= [refTaskList.listItems count]) {
		indexPath = [NSIndexPath indexPathForRow:[refTaskList.listItems count] - 1 inSection:0];
	}
	
	// If this is not the last row
	if (proposedDestinationIndexPath.row < [refTaskList.listItems count] - 1) {
		// Check if there is a sub task below this task
		NSInteger checkIndex = 0;
		NSInteger priorIndex = 1;
		
		if (proposedDestinationIndexPath.row > sourceIndexPath.row) {
			checkIndex = 1;
			priorIndex = 0;
		}
		
		ListItem *sourceListItem = [refTaskList.listItems objectAtIndex:sourceIndexPath.row];
		ListItem *checkListItem = [refTaskList.listItems objectAtIndex:proposedDestinationIndexPath.row + checkIndex];
		
		
		// Source is a sub task
		if (sourceListItem.parentListItemID != -1) {
			// The item previous must be a sub task or the main task
			
			// Make sure this is not the first row
			if (proposedDestinationIndexPath.row == 0) {
				return sourceIndexPath;
			}
			
			ListItem *priorListItem = [refTaskList.listItems objectAtIndex:proposedDestinationIndexPath.row - priorIndex];
            
			// Make sure that we are moving the sub task underneath the correct task
			if (priorListItem.parentListItemID != sourceListItem.parentListItemID &&
				priorListItem.listItemID != sourceListItem.parentListItemID) {
				return sourceIndexPath;
			}
		}
		
		// Source is a task and check item is a sub task (not allowed) (make sure not last row, allow if it is)
		if (checkListItem.parentListItemID != -1 && sourceListItem.parentListItemID == -1) {
			
			// In case of strange bug with subtask at end
			if (proposedDestinationIndexPath.row == [refTaskList.listItems count] - 2) {
				ListItem *lastListItem = [refTaskList.listItems objectAtIndex:[refTaskList.listItems count] - 1];
				
				if (lastListItem.parentListItemID != -1) {
					indexPath = [NSIndexPath indexPathForRow:[refTaskList.listItems count] - 1 inSection:0];
					return indexPath;
				}
			}
			
			// Do nothing, as there is a sub task below the moved task
			return sourceIndexPath;
		}
	}
	
	return indexPath;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	/*if (selectRowPrompt == TRUE) {
     if (indexPath.row % 2 == 0) {
     [cell setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:0.87 alpha:0.8]];
     } else {
     [cell setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:0.90 alpha:0.8]];
     }
     }*/
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    if ([self isFilterTagsMode] || [self isSearchMode]) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}

#pragma mark -
#pragma mark Alert View Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
	// Cancel
	if (buttonIndex == 0) {
		return;
	} else if (buttonIndex == 1) {
		// Ok
		// Need to get the task list that contains the list item id
		NSInteger listItemID = alertView.tag;
		
		for (TaskList *taskList in refMasterTaskListCollection.lists) {
			NSInteger listItemIndex = [taskList getIndexOfListItemWithID:listItemID];
			
			if (listItemIndex != -1) {
				// Delete any subtasks (only goes 1 rank deep)
				for (int i = [taskList.listItems count] - 1; i >= 0; i--) {
					if ([[taskList.listItems objectAtIndex:i] parentListItemID] == listItemID) {
						ListItem *deleteListItem = [taskList.listItems objectAtIndex:i];
                        
						// Now need to search if this row appears in the current tableview
						NSInteger cellSection = -1;
						NSInteger cellRow = -1;
						if ([self isFilterTagsMode]) {
							
							NSInteger sectionCount = 0;
							for (TaskList *filterTaskList in filterTagsTaskListCollection.lists) {
								
								NSInteger filterListItemIndex = [filterTaskList getIndexOfListItemWithID:deleteListItem.listItemID];
								if (filterListItemIndex != -1) {
									
									cellSection = sectionCount;
									cellRow = filterListItemIndex;
								}
								sectionCount++;
							}
						} else if ([self isSearchMode]) {
							NSInteger sectionCount = 0;
							for (TaskList *filterTaskList in filteredSearchTaskListCollection.lists) {
								
								NSInteger filterListItemIndex = [filterTaskList getIndexOfListItemWithID:deleteListItem.listItemID];
								if (filterListItemIndex != -1) {
									
									cellSection = sectionCount;
									cellRow = filterListItemIndex;
								}
								sectionCount++;
							}
						} else {
							cellSection = 0;
                            
                            // Get cellRow on task list
                            if (isShowCompletedTasks == 0) { // Hide completed is ON
                                
                                NSMutableArray *copyRefTaskList = [refTaskList.listItems copy];
                                for (ListItem *item in copyRefTaskList) {
                                    if (item.completed == YES) {
                                        [refTaskList.listItems removeObject:item];
                                    }
                                }
                                
                                cellRow = [refTaskList getIndexOfListItemWithID:deleteListItem.listItemID];
                                
                            }else{// Hide completed is OFF
                                
                                cellRow = [refTaskList getIndexOfListItemWithID:deleteListItem.listItemID];
                            }
						}
						
						
                        // Subtask found, delete it
                        [taskList deleteListItemAtIndex:i permanent:NO];
						
						// Task list requires update
						taskList.mainViewRequiresUpdate = YES;
						
						if (cellRow != -1 && cellSection != -1) {
							// Delete the row from the data source
							[refTaskList.listItems removeObjectAtIndex:cellRow];
                            
                            // Delete cell when delete list item
                            if (isShowCompletedTasks == 0) {// Hide completed is ON
                                
                                if (deleteListItem.completed == YES) {
                                    // No delete cell when task is hidden because it is completed
                                }else{
                                    
                                    // Delete row for cell is not completed
                                    [self.myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:cellRow inSection:cellSection]]
                                                       withRowAnimation:UITableViewRowAnimationFade];
                                }
                                
                                // Reload table view
                                [self.myTableView reloadData];
                                
                            }else{// Hide completed is OFF
                                
                                [self.myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:cellRow inSection:cellSection]]
                                                   withRowAnimation:UITableViewRowAnimationFade];
                            }
                            
                            
                            
                            
						}
					}
				}
				
				// Now need to find and delete the selected list item from the tableview and database
				NSInteger selectedListItemSection = 0;
				NSInteger selectedListItemRow = 0;
				if ([self isFilterTagsMode]) {
					
					NSInteger sectionCount = 0;
					for (TaskList *filterTaskList in filterTagsTaskListCollection.lists) {
						
						NSInteger filterListItemIndex = [filterTaskList getIndexOfListItemWithID:listItemID];
						if (filterListItemIndex != -1) {
							
							selectedListItemSection = sectionCount;
							selectedListItemRow = filterListItemIndex;
							
							[filterTaskList.listItems removeObjectAtIndex:filterListItemIndex];
						}
						sectionCount++;
					}
				} else if ([self isSearchMode]) {
					NSInteger sectionCount = 0;
					for (TaskList *filterTaskList in filteredSearchTaskListCollection.lists) {
						
						NSInteger filterListItemIndex = [filterTaskList getIndexOfListItemWithID:listItemID];
						if (filterListItemIndex != -1) {
							
							selectedListItemSection = sectionCount;
							selectedListItemRow = filterListItemIndex;
							
							[filterTaskList.listItems removeObjectAtIndex:filterListItemIndex];
						}
						sectionCount++;
					}
				} else {
					selectedListItemSection = 0;
					selectedListItemRow = [refTaskList getIndexOfListItemWithID:listItemID];
				}
				
				// Delete the selected item as well
				[taskList deleteListItemAtIndex:listItemIndex permanent:NO];
                
				// Delete the selected list item from the table view
				[refTaskList.listItems removeObjectAtIndex:selectedListItemRow];
				[self.myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:selectedListItemRow
																								inSection:selectedListItemSection]]
								   withRowAnimation:UITableViewRowAnimationFade];
				
				
			}
            
			
		}
		
		// Reload collections
        [self reloadCollections];
        /*progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
         [self.navigationController.view addSubview:progressHUD];
         progressHUD.delegate = self;
         progressHUD.labelText = @"Reloading task collections...";
         [progressHUD showWhileExecuting:@selector(reloadCollections) onTarget:self withObject:nil animated:YES];*/
	}
}

#pragma mark -
#pragma mark New List Item Delegate Methods

/*- (void)createNewListItemWithTitle:(NSString *)theTitle andNotes:(NSString *)theNotes andCompleted:(BOOL)completed {
 ListItem *newListItem = [[ListItem alloc] initNewListItemWithID:refTaskList.listID
 andTitle:theTitle
 andNotes:theNotes
 andCompleted:completed
 andParentTitle:refTaskList.title
 andParentListID:refTaskList.listID
 andDueDate:@""
 andListItemTagCollection:nil];
 //newListItem.completed = completed;
 [refTaskList.listItems insertObject:newListItem atIndex:0];
 
 // Update the database (dont do this, new list item does this)
 //[newListItem insertSelfIntoDatabase];
 
 [newListItem release];
 
 NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
 [listTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationLeft];
 
 // Release the popover
 [newListItemPopOverController dismissPopoverAnimated:YES];
 
 
 // Reload the tableview
 //[listTableView reloadData];
 
 }*/

// Returns the list item id of the sub list item that was created
- (NSInteger)createNewSubListItemWithTitle:(NSString *)theTitle andNotes:(NSString *)theNotes andCompleted:(BOOL)completed
                                andDueDate:(NSString *)theDate andDueTime:(NSString *)theTime andAlertTime:(NSString *)alertTime
                                  andSound:(NSString *)theSound andSnooze:(BOOL)theSnooze
                         andParentListItem:(ListItem *)parentListItem
                  andListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection {
	
    // Get status show completed tasks in setting
    //NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
	refTaskList.mainViewRequiresUpdate = TRUE;
	
	NSInteger subItemOrder = [parentListItem getNewSubItemOrder];
	
	NSString *theCompletedDateTime = @"";
	// If is completed
	if (completed) {
		theCompletedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	}
	// Create the sub list item
    // (update alerttime field)
	ListItem *newSubListItem = [[ListItem alloc] initNewSubListItemWithID:refTaskList.listID
                                                                 andTitle:theTitle
                                                                 andNotes:theNotes
                                                             andCompleted:completed
                                                           andParentTitle:refTaskList.title
                                                               andDueDate:theDate
                                                               andDueTime:theTime
                                                             andAlertTime:alertTime
                                                                 andSound:theSound
                                                                andSnooze:theSnooze
													  andParentListItemID:parentListItem.listItemID
															 andItemOrder:parentListItem.itemOrder
														  andSubItemOrder:subItemOrder
												 andListItemTagCollection:theListItemTagCollection
													 andCompletedDateTime:theCompletedDateTime];
	
	// Reload the table view data before adding a new sub list
	[self.myTableView reloadData];
	
	// Also insert in the master task list
	TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:currentTaskListID];
	NSInteger masterInsertIndex = [masterTaskList getIndexOfListItemWithID:parentListItem.listItemID];
	masterInsertIndex++;
	if (masterInsertIndex < [masterTaskList.listItems count]) {
		[masterTaskList.listItems insertObject:newSubListItem atIndex:masterInsertIndex];
	} else {
		[masterTaskList.listItems addObject:newSubListItem];
	}
	
	// Get index of parent list item, then increment by 1 to get correct insert index
	NSInteger insertIndex = [refTaskList getIndexOfListItemWithID:parentListItem.listItemID];
	insertIndex++;
	
	if (insertIndex < [refTaskList.listItems count]) {
        [refTaskList.listItems insertObject:newSubListItem atIndex:insertIndex];
		
	} else {
        [refTaskList.listItems addObject:newSubListItem];
	}
	
	
	//NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:insertIndex inSection:0]];
	//[myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
	
    // Reload main table with animation
    [UIView transitionWithView: myTableView
                      duration: 0.2f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void)
     {
         [self.myTableView reloadData];
     }
                    completion: ^(BOOL isFinished)
     {
         /* TODO: Whatever you want here */
     }];
    
	// Release the popover/s
	if ([newListItemPopOverController isPopoverVisible]) {
		[newListItemPopOverController dismissPopoverAnimated:YES];
        newListItemPopOverController = nil;
//        [newListItemPopOverController release];
	}
	if ([editListItemPopOverController isPopoverVisible]) {
		[editListItemPopOverController dismissPopoverAnimated:YES];
	}
    
	// Check if we need to update items due table view responder
	if ([newSubListItem.dueDate length] > 0 && newSubListItem.dueDate != nil) {
		[itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
												  andParentListID:refTaskList.listID andListItem:newSubListItem];
		[itemsDueTableView reloadData];
	}
	
    // Reload the calendar view
    [miniCalendarView reloadData];
    
    // Reload the list pad
    [miniListPadTableView reloadData];
    
    NSInteger subListItemID = newSubListItem.listItemID;
    
//	[newSubListItem release];
    
    if (newSubListItem.dueTime.length > 0) {
        // Add to local push
        [AlertTime addNewNotificationToQueue:newSubListItem];
    }
    
    return subListItemID;
}

- (void)createNewListItemWithTitle:(NSString *)theTitle andNotes:(NSString *)theNotes
					  andCompleted:(BOOL)completed
                        andDueDate:(NSString *)theDate
                        andDueTime:(NSString *)theTime
                      andAlertTime:(NSString *)alertTime
                          andSound:(NSString *)theSound
                         andSnooze:(BOOL)theSnooze
                       andPriority:(NSInteger)thePriority
		  andListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection {
    
	refTaskList.mainViewRequiresUpdate = TRUE;
	
	NSString *theCompletedDateTime = @"";
	// If is completed
	if (completed) {
		theCompletedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	}
	
    // Check to see if we want to add this new task to end of list
    BOOL addToEnd = FALSE;
    if ([refMasterTaskListCollection.theNewTaskOrder isEqualToString:@"End"]) {
		addToEnd = TRUE;
	}
    
    // (update alerttime field)
	ListItem *newListItem = [[ListItem alloc] initNewListItemWithID:refTaskList.listID
														   andTitle:theTitle
														   andNotes:theNotes
													   andCompleted:completed
													 andParentTitle:refTaskList.title
														 andDueDate:theDate
                                                         andDueTime:theTime
                                                       andAlertTime:alertTime
                                                           andSound:theSound
                                                          andSnooze:theSnooze
										   andListItemTagCollection:theListItemTagCollection
											   andCompletedDateTime:theCompletedDateTime
                                                        andPriority:thePriority
                                                           addToEnd:addToEnd];
	
	// Reload the table view data before adding a new list
	[self.myTableView reloadData];
    
	//newListItem.completed = completed;
	if ([refMasterTaskListCollection.theNewTaskOrder isEqualToString:@"Beginning"]) {
        [refTaskList.listItems insertObject:newListItem atIndex:0];
        
	} else if ([refMasterTaskListCollection.theNewTaskOrder isEqualToString:@"End"]) {
		[refTaskList.listItems addObject:newListItem];
	}
	
	
	// Also insert in the master task list
	TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:currentTaskListID];
	
	if ([refMasterTaskListCollection.theNewTaskOrder isEqualToString:@"Beginning"]) {
		[masterTaskList.listItems insertObject:newListItem atIndex:0];
	} else if ([refMasterTaskListCollection.theNewTaskOrder isEqualToString:@"End"]) {
		[masterTaskList.listItems addObject:newListItem];
	}
	
	// Update the database (DONT DO THIS, as init new list item does it)
	//[newListItem insertSelfIntoDatabase];
	if ([refMasterTaskListCollection.theNewTaskOrder isEqualToString:@"Beginning"]) {
		NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        [self.myTableView beginUpdates];
		[self.myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.myTableView endUpdates];
	} else if ([refMasterTaskListCollection.theNewTaskOrder isEqualToString:@"End"]) {
        
        NSInteger count = 0;
        for (ListItem *item in refTaskList.listItems) {
            if (item.completed == YES) {
                continue;
            }else{
                count ++;
            }
        }
        NSInteger lastIndex = count - 1;
        
		NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:lastIndex inSection:0]];
        [self.myTableView beginUpdates];
		[self.myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
		[self.myTableView endUpdates];
		[self.myTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
	}
	
	// Release the popover
	[newListItemPopOverController dismissPopoverAnimated:YES];
    newListItemPopOverController = nil;
//    [newListItemPopOverController release];
	
	// Check if we need to update items due table view responder
	if ([newListItem.dueDate length] > 0 && newListItem.dueDate != nil) {
		[itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
												  andParentListID:refTaskList.listID andListItem:newListItem];
		[itemsDueTableView reloadData];
	}
    
    // Scroll to top of table view
    [self.myTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
    // Yamikaze's change
    MAppDelegate *appDelegate = (MAppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.rootControlPanel.miniCalendarView reloadData];
    [appDelegate.rootControlPanel reloadListPadTableView];
	
    // Reload the calendar view
    [miniCalendarView reloadData];
    
    // Reload the list pad
    [miniListPadTableView reloadData];
    
//	[newListItem release];
    
    if (newListItem.dueTime.length > 0 ) {
        // Add to local push
        [AlertTime addNewNotificationToQueue:newListItem];
    }
}


- (void)dismissNewListItemPopOver {
	// Reload tableview data anyways (just in case)
	[self.myTableView reloadData];
	
	[newListItemPopOverController dismissPopoverAnimated:YES];
    newListItemPopOverController = nil;
//    [newListItemPopOverController release];
}

#pragma mark - Date Range Delegate Methods

- (void)dateRangeSelectedWithStartDate:(NSString *)theStartDate endDate:(NSString *)theEndDate {
    searchStartDateRange = [theStartDate copy];
    searchEndDateRange = [theEndDate copy];
    NSString *startDateString = @"";
    if ([searchStartDateRange length] > 0) {
        NSDate *startingDate = [MethodHelper dateFromString:searchStartDateRange usingFormat:K_DATEONLY_FORMAT];
        startDateString = [MethodHelper localizedDateFrom:startingDate usingStyle:NSDateFormatterMediumStyle withYear:YES];
    }
    
    NSString *endDateString = @"";
    if ([searchEndDateRange length] > 0) {
        NSDate *endDate = [MethodHelper dateFromString:searchEndDateRange usingFormat:K_DATEONLY_FORMAT];
        endDateString = [MethodHelper localizedDateFrom:endDate usingStyle:NSDateFormatterMediumStyle withYear:YES];
    }
    
    NSString *newDateRangeLabelText = @"Date Range: None";
    if ([endDateString length] > 0 && [startDateString length] > 0) {
        newDateRangeLabelText = [NSString stringWithFormat:@"Date Range: %@ to %@",
                                 startDateString, endDateString];
    } else if ([endDateString length] > 0 && [startDateString length] == 0) {
        newDateRangeLabelText = [NSString stringWithFormat:@"Date Range: Until %@", endDateString];
    } else if ([endDateString length] == 0 && [startDateString length] > 0) {
        newDateRangeLabelText = [NSString stringWithFormat:@"Date Range: From %@", startDateString];
    }
    
    [searchDateRangeLabel setText:newDateRangeLabelText];
    
    [self searchBar:mySearchBar textDidChange:mySearchBar.text];
}

#pragma mark -
#pragma mark List Item Delegate Methods

- (void)taskCompleted:(BOOL)isCompleted forListItemID:(NSInteger)theListItemID {
    ListItem *listItem = nil;
	ListItem *repeatingListItem = nil;
	BOOL didRepeatTask = FALSE;
	NSInteger filteredTaskListIndex = 0;
	
	if ([self isFilterTagsMode]) {
		for (TaskList *taskList in refTaskListCollection.lists) {
			NSInteger filterListItemIndex = [taskList getIndexOfListItemWithID:theListItemID];
			if (filterListItemIndex != -1) {
				taskList.mainViewRequiresUpdate = TRUE;
				listItem = [taskList.listItems objectAtIndex:filterListItemIndex];
                
                listItem.hasJustComplete = isCompleted;
                
				if (isCompleted == TRUE) {
					repeatingListItem = [taskList allocProcessRepeatingTaskAtIndex:filterListItemIndex];
					if (repeatingListItem != nil) {
                        
						[taskList.listItems insertObject:repeatingListItem atIndex:0];
						
						TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:taskList.listID];
						[masterTaskList.listItems insertObject:repeatingListItem atIndex:0];
						didRepeatTask = TRUE;
						
						filteredTaskListIndex = [filterTagsTaskListCollection getIndexOfTaskListWithID:taskList.listID];
						
						TaskList *filterTaskList = [filterTagsTaskListCollection getTaskListWithID:taskList.listID];
						[filterTaskList.listItems insertObject:repeatingListItem atIndex:0];
                        
						// Swap list item creation date with repeating list item
						NSString *tempListItemCreationDate = [listItem.creationDateTime copy];
						listItem.creationDateTime = repeatingListItem.creationDateTime;
						repeatingListItem.creationDateTime = tempListItemCreationDate;
						
                        [itemsDueTableViewResponder reloadDataWithListParentTitle:filterTaskList.title andTableView:itemsDueTableView
                                                                  andParentListID:filterTaskList.listID andListItem:repeatingListItem];
                        
						[repeatingListItem updateDatabase];
//						[repeatingListItem release];
						
					}
				}
			}
		}
	} else if ([self isSearchMode]) {
		for (TaskList *taskList in refTaskListCollection.lists) {
			NSInteger filterListItemIndex = [taskList getIndexOfListItemWithID:theListItemID];
			if (filterListItemIndex != -1) {
				taskList.mainViewRequiresUpdate = TRUE;
				listItem = [taskList.listItems objectAtIndex:filterListItemIndex];
                
                listItem.hasJustComplete = isCompleted;
                
				if (isCompleted == TRUE) {
					repeatingListItem = [taskList allocProcessRepeatingTaskAtIndex:filterListItemIndex];
					if (repeatingListItem != nil) {
                        
						[taskList.listItems insertObject:repeatingListItem atIndex:0];
						
						TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:taskList.listID];
						[masterTaskList.listItems insertObject:repeatingListItem atIndex:0];
						didRepeatTask = TRUE;
						
						filteredTaskListIndex = [filteredSearchTaskListCollection getIndexOfTaskListWithID:taskList.listID];
						
						TaskList *filterTaskList = [filteredSearchTaskListCollection getTaskListWithID:taskList.listID];
						[filterTaskList.listItems insertObject:repeatingListItem atIndex:0];
						
						// Swap list item creation date with repeating list item
						NSString *tempListItemCreationDate = [listItem.creationDateTime copy];
						listItem.creationDateTime = repeatingListItem.creationDateTime;
						repeatingListItem.creationDateTime = tempListItemCreationDate;
						
                        [itemsDueTableViewResponder reloadDataWithListParentTitle:filterTaskList.title andTableView:itemsDueTableView
                                                                  andParentListID:filterTaskList.listID andListItem:repeatingListItem];
                        
						[repeatingListItem updateDatabase];
//						[repeatingListItem release];
					}
				}
			}
		}
	} else {
		refTaskList.mainViewRequiresUpdate = TRUE;
		NSInteger listItemIndex = [refTaskList getIndexOfListItemWithID:theListItemID];
        if (listItemIndex != -1) {
            // Found list item
            listItem = [refTaskList.listItems objectAtIndex:listItemIndex];
            
            listItem.hasJustComplete = isCompleted;
            
            if (isCompleted == TRUE) {
                repeatingListItem = [refTaskList allocProcessRepeatingTaskAtIndex:listItemIndex];
                
                if (repeatingListItem != nil) {
                    
                    [refTaskList.listItems insertObject:repeatingListItem atIndex:0];
                    
                    // Also insert in the master task list
                    TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:currentTaskListID];
                    [masterTaskList.listItems insertObject:repeatingListItem atIndex:0];
                    didRepeatTask = TRUE;
                    
                    // Swap list item creation date with repeating list item
                    NSString *tempListItemCreationDate = listItem.creationDateTime;
                    listItem.creationDateTime = repeatingListItem.creationDateTime;
                    repeatingListItem.creationDateTime = tempListItemCreationDate;
                    
                    [itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
                                                              andParentListID:refTaskList.listID andListItem:repeatingListItem];
                    
                    [repeatingListItem updateDatabase];
//                    [repeatingListItem release];
                }
            }
        }
	}
	
	/*
	 [refTaskList.listItems insertObject:newListItem atIndex:0];
	 
	 // Also insert in the master task list
	 TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:currentTaskListID];
	 [masterTaskList.listItems insertObject:newListItem atIndex:0];
	 
	 */
	
	if (listItem == nil) {
		// Can not find list item, do nothing
		return;
	}
	
	listItem.completed = isCompleted;
    
	if ([listItem completed] == TRUE) {
		//NSString *completedDateTimeString = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		listItem.recurringListItemID = -1;
		listItem.completedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
        listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	} else if ([listItem completed] == FALSE) {
		listItem.completedDateTime = @"";
        listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	}
    
    
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
    // Set status completed is has just complete to save databse
    if (isShowCompletedTasks == 0) {
        listItem.hasJustComplete = isCompleted;
        if (listItem.completed == YES) {
            listItem.completed = listItem.hasJustComplete;
        }
    }
    
    // If List Item is repeat task and has alarm --> list item will remove alarm when check completed
    if ([listItem.dueTime length] > 0 && didRepeatTask == TRUE && listItem.completed == YES && listItem.hasJustComplete == YES){
        listItem.dueTime = @"";
    }
    
	// Update database
	[listItem updateDatabaseWithoutModifiedLog];
    
    
    // After save to databse. Set status of completed is NO and has just complete is isCompleted on view
    if (isShowCompletedTasks == 0) {
        if (listItem.completed == YES) {
            listItem.completed = NO;
        }
    }else{
        listItem.hasJustComplete = isCompleted;
    }
    
	
	// Assign list item due date
	NSString *listItemDueDate = [listItem dueDate];
	
	[self updateMasterCollectionWithListItem:listItem];
    
    
	//ListItem *listItemCopy = [[ListItem alloc] initWithListItemCopy:listItem];
	//[self removeFilteredListItemsUsingListItem:listItem];
	
	if ([listItemDueDate length] > 0) {
		// Update the items due table view responder
		[itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title
													 andTableView:itemsDueTableView
												  andParentListID:refTaskList.listID
													  andListItem:listItem];
	}
	
	if ([self isFilterTagsMode]) {
		[self updateTaskListCollection:filterTagsTaskListCollection withListItem:listItem];
	} else if ([self isSearchMode]) {
		[self updateTaskListCollection:filteredSearchTaskListCollection withListItem:listItem];
	}
	
	//[listItemCopy release];
	// Should insert row if needed
    
    // Init any loaded alarms
    AlarmCollection *alarmCollection = [[AlarmCollection alloc] init];
    [alarmCollection loadAlarmsWithListItemID:listItem.listItemID];
    
    // If task list is check to complete.
    if ([alarmCollection.alarms count] > 0 && (listItem.completed == YES || listItem.hasJustComplete == YES)) {
        
        // Set item has no alarm
        listItem.hasAlarms = FALSE;
        [listItem updateDatabase];
        
        for (Alarm *alarm in alarmCollection.alarms) {
            // Remove notification
            [alarm removeFromNotificationQueue];
            
            // Delete alarm in database
            [alarm deleteSelfFromDatabase];
        }
    }
    
//    [alarmCollection release];
    
	// Reload the table view
	if (didRepeatTask == FALSE) {
        
        [self.myTableView reloadData];
        
	} else if (didRepeatTask == TRUE) {
		// Do insert into table view
		if ([self isFilterTagsMode] || [self isSearchMode]) {
			NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:filteredTaskListIndex]];
			[self.myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
			[self performSelector:@selector(reloadTableView) withObject:nil afterDelay:0.5];
		} else {
			NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
			[self.myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
			[self performSelector:@selector(reloadTableView) withObject:nil afterDelay:0.5];
		}
	}
    // Reload the calendar view
//    [miniCalendarView reloadData];
    
    // Reload the list pad
//    [miniListPadTableView reloadData];
    
    // Yamikaze's change
    MAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.rootControlPanel.miniCalendarView reloadData];
    [appDelegate.rootControlPanel reloadListPadTableView];
}

- (void)deleteRowsOnListViewForSearchAndFilterWithListItem:(NSArray *) datas{
    
    // Get TaskList which have list item is checked
    TaskList *taskList = [datas objectAtIndex:0];
    // Get List item is checked
    ListItem *listItem = [datas objectAtIndex:1];
    // Get status, If it is 1, status is search. Otherwise is filter
    NSString *isSearch = [datas objectAtIndex:2];
    
    NSInteger filteredTaskListIndex = 0;
    TaskList *taskListNew = nil;
    
    if ([isSearch isEqualToString:@"0"]) {//isFilter
        // Get index of Task List
        filteredTaskListIndex = [filterTagsTaskListCollection getIndexOfTaskListWithID:taskList.listID];
        taskListNew = [filterTagsTaskListCollection getTaskListWithID:taskList.listID];
        
    } else {//is Search
        // Get index of TaskList
        filteredTaskListIndex = [filteredSearchTaskListCollection getIndexOfTaskListWithID:taskList.listID];
        taskListNew = [filteredSearchTaskListCollection getTaskListWithID:taskList.listID];
    }
    
    if (taskListNew != nil) {
        // Get index of list item
        NSInteger filterListItemIndex = [taskListNew.listItems indexOfObject:listItem];
        
        if (filterListItemIndex != NSNotFound && listItem.hasJustComplete == YES && (self.isFilterTagsMode == YES || self.isSearchMode == YES)) {
            // Remove list item at index in Task List
            [taskList.listItems removeObject:listItem];
            
            NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:filterListItemIndex inSection:filteredTaskListIndex]];
            
            listItem.completed = YES;
            
            // Delete row on table view at indexPaths
            [self.myTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
            
            // Reload the calendar view
            [miniCalendarView reloadData];
            
            // Reload the list pad
            [miniListPadTableView reloadData];
        }
        
        if (listItem.hasJustComplete == YES && self.isFilterTagsMode != YES && self.isSearchMode != YES) {
            listItem.completed = YES;
            [self.myTableView reloadData];
        }
    }
}

- (void)deleteRowsOnListViewWithListItem:(ListItem *) listItem{
    
    // Get index of list item
    NSInteger listItemIndex = [self.arrayTaskList indexOfObject:listItem];
    
    if (listItemIndex != NSNotFound && listItem.hasJustComplete == YES && self.isFilterTagsMode != YES && self.isSearchMode != YES && self.isShowWritingTasksPopover != YES) {
        
        listItem.completed = YES;
        
        NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:listItemIndex inSection:0]];
        [self.myTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        
    }
    if (listItem.hasJustComplete == YES && (self.isFilterTagsMode == YES || self.isSearchMode == YES || self.isShowWritingTasksPopover == YES)) {
        listItem.completed = YES;
        [self.myTableView reloadData];
    }
}

- (void)deleteRowsOnListViewForSearchAndFilterWithRepeatListItem:(NSArray *) arrayOfThingsToPassAlong {
    
    // Get TaskList which have list item is checked
    TaskList *taskList = [arrayOfThingsToPassAlong objectAtIndex:0];
    
    // Get List item is checked
    ListItem *listItem = [arrayOfThingsToPassAlong objectAtIndex:1];
    //    ListItem *repeatingListItem = [arrayOfThingsToPassAlong objectAtIndex:2];
    
    
    // Get status, If it is 1, status is search. Otherwise is filter
    NSString *isSearch = [arrayOfThingsToPassAlong objectAtIndex:3];
    
    NSInteger filteredTaskListIndex = 0;
    
    //
    //    [taskList.listItems insertObject:repeatingListItem atIndex:0];
    //
    //    TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:taskList.listID];
    //    [masterTaskList.listItems insertObject:repeatingListItem atIndex:0];
    //
    //
    //    filteredTaskListIndex = [filteredSearchTaskListCollection getIndexOfTaskListWithID:taskList.listID];
    //
    //    TaskList *filterTaskList = [filteredSearchTaskListCollection getTaskListWithID:taskList.listID];
    //    [filterTaskList.listItems insertObject:repeatingListItem atIndex:0];
    //
    //    // Swap list item creation date with repeating list item
    //    NSString *tempListItemCreationDate = [listItem.creationDateTime copy];
    //    listItem.creationDateTime = repeatingListItem.creationDateTime;
    //    repeatingListItem.creationDateTime = tempListItemCreationDate;
    //
    //    [itemsDueTableViewResponder reloadDataWithListParentTitle:filterTaskList.title andTableView:itemsDueTableView
    //                                              andParentListID:filterTaskList.listID andListItem:repeatingListItem];
    //
    //    [repeatingListItem updateDatabase];
    //
    //    NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
    //
    //    [myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    
    TaskList *taskListNew = nil;
    
    if ([isSearch isEqualToString:@"0"]) {//isFilter
        // Get index of Task List
        filteredTaskListIndex = [filterTagsTaskListCollection getIndexOfTaskListWithID:taskList.listID];
        taskListNew = [filterTagsTaskListCollection getTaskListWithID:taskList.listID];
        
    } else {//is Search
        // Get index of TaskList
        filteredTaskListIndex = [filteredSearchTaskListCollection getIndexOfTaskListWithID:taskList.listID];
        taskListNew = [filteredSearchTaskListCollection getTaskListWithID:taskList.listID];
    }
    
    
    if (taskListNew != nil) {
        // Get index of list item
        NSInteger filterListItemIndex = [taskListNew.listItems indexOfObject:listItem];
        
        if (filterListItemIndex != NSNotFound && listItem.hasJustComplete == YES && (self.isFilterTagsMode == YES || self.isSearchMode == YES)) {
            // Remove list item at index in Task List
            [taskList.listItems removeObject:listItem];
            
            NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:filterListItemIndex inSection:filteredTaskListIndex]];
            
            listItem.completed = YES;
            
            // Delete row on table view at indexPaths
            [self.myTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
            
            // Reload the calendar view
            [miniCalendarView reloadData];
            
            // Reload the list pad
            [miniListPadTableView reloadData];
        }
        
        if (listItem.hasJustComplete == YES && self.isFilterTagsMode != YES && self.isSearchMode != YES) {
            listItem.completed = YES;
            [self.myTableView reloadData];
        }
    }
}

- (void)deleteRowsOnListViewWithRepeatListItem:(NSArray *)arrayOfThingsToPassAlong {
    
    ListItem *listItem = [arrayOfThingsToPassAlong objectAtIndex:0];
    ListItem *repeatingListItem = [arrayOfThingsToPassAlong objectAtIndex:1];
    
    if (listItem.hasJustComplete == YES ) {
        
        [refTaskList.listItems insertObject:repeatingListItem atIndex:0];
        
        // Also insert in the master task list
        TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:currentTaskListID];
        [masterTaskList.listItems insertObject:repeatingListItem atIndex:0];
        
        // Swap list item creation date with repeating list item
        NSString *tempListItemCreationDate = listItem.creationDateTime;
        listItem.creationDateTime = repeatingListItem.creationDateTime;
        repeatingListItem.creationDateTime = tempListItemCreationDate;
        
        
        [itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
                                                  andParentListID:refTaskList.listID andListItem:repeatingListItem];
        
        //listItem.hasJustComplete = YES;
        
        [repeatingListItem updateDatabase];
        
        NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [self.myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
    // Get index of list item
    NSInteger listItemIndex = [self.arrayTaskList indexOfObject:listItem];
    
    if (listItemIndex != NSNotFound && listItem.hasJustComplete == YES && self.isFilterTagsMode != YES && self.isSearchMode != YES && self.isShowWritingTasksPopover != YES) {
        
        listItem.completed = YES;
        
        NSArray *indexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:listItemIndex inSection:0]];
        [self.myTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        
    }
    if (listItem.hasJustComplete == YES && (self.isFilterTagsMode == YES || self.isSearchMode == YES || self.isShowWritingTasksPopover == YES)) {
        listItem.completed = YES;
        [self.myTableView reloadData];
    }
    
//    [repeatingListItem release];
}

#pragma mark -
#pragma mark NewListItemDueDateDelegate
// Note: Technically this is truly 'NewListItemDueDateDelegate' but we are reusing it for list date

- (void)updateDueDate:(NSString *)newDueDate {
	// Update the task list date
	refTaskList.creationDate = newDueDate;
    
	[refTaskList updateDatabase];
	
	// We are going to update the list date with this date
	NSDate *theDate = [MethodHelper dateFromString:newDueDate usingFormat:K_DATEONLY_FORMAT];
	[dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES]
					 forState:UIControlStateNormal];
    
	// Dismiss the date label popover
	if ([dateLabelPopoverController isPopoverVisible]) {
		[dateLabelPopoverController dismissPopoverAnimated:YES];
//        [dateLabelPopoverController release];
	}
	
    // Reload the calendar view
    [miniCalendarView reloadData];
    
    // Reload the list pad
    [miniListPadTableView reloadData];
    
    // Yamikaze's change
//    MAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//    
//    UINavigationController *masterNav = (UINavigationController *)appDelegate.splitViewController.masterViewController;
//    ControlPanelViewController *rightPanel = [[masterNav viewControllers] firstObject];
//    [rightPanel reloadListPadTableView];
//    
//    [rightPanel.miniCalendarView reloadData];
}


#pragma mark -
#pragma mark KFSegmentedControl Delegate

- (void)touchUpInsideSegmentIndex:(NSInteger)segmentIndex {
    //self.ischeckCompletedTask = NO;
	if (sortItemsSegmentedControl.selectedSegmentIndex == refTaskList.currentSortType) {
		// Do nothing
		return;
	}
	
	refTaskList.currentSortType = sortItemsSegmentedControl.selectedSegmentIndex;
	
	refTaskList.lastSortType = sortItemsSegmentedControl.selectedSegmentIndex;
	
	
	// This needs to sort for all task lists
	
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		taskList.currentSortType = sortItemsSegmentedControl.selectedSegmentIndex;
		[taskList sortListWithSortType:sortItemsSegmentedControl.selectedSegmentIndex];
	}
	
	// Also sort the task list collection
	for (TaskList *taskList in refTaskListCollection.lists) {
		taskList.currentSortType = sortItemsSegmentedControl.selectedSegmentIndex;
		[taskList sortListWithSortType:sortItemsSegmentedControl.selectedSegmentIndex];
	}
	
	// Need to refresh the filter tags collection (if needed)
	if ([self isFilterTagsMode]) {
		[filterTagsTaskListCollection filterListsUsingListItemTagCollection:filterListItemTagCollection
											  andOriginalTaskListCollection:refTaskListCollection];
	}
	
	// Reload the table view
	[self.myTableView reloadData];
}

#pragma mark -
#pragma mark Sort Items Table View Delegate

- (void)updateSelectedSortType:(NSInteger)newSortType {
	[sortItemsSegmentedControl setSelectedSegmentIndex:newSortType];
}

#pragma mark -
#pragma mark Highlighters Popover Delegate

- (void)highlighterSelected:(NSInteger)selected {
	highlighterSelected = selected;
}

- (void)dismissHighlightersPopover {
	// Reset selected highlighter to none
	highlighterSelected = EnumHighlighterColorNone;
	
	[highlightersPopoverController dismissPopoverAnimated:YES];
    highlightersPopoverController = nil;
//    [highlightersPopoverController release];
}

#pragma mark -
#pragma mark UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	[textField setBorderStyle:UITextBorderStyleRoundedRect];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
		[textField setFrame:kTitleTextFieldEditingPortrait];
	} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
		[textField setFrame:kTitleTextFieldEditingLandscape];
	}
    [titleTextField setBackgroundColor:[UIColor whiteColor]];
	[UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	[textField setBorderStyle:UITextBorderStyleNone];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
		[textField setFrame:kTitleTextFieldPortrait];
	} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
		[textField setFrame:kTitleTextFieldLandscape];
	}
	
	// Update the title
	if ([textField.text length] > 0) {
		NSString *newTitle = [textField text];
		
		if ([newTitle isEqualToString:refTaskList.title] == FALSE) {
			if ([newTitle isEqualToString:@"Toodledo Tasks"] == TRUE || [textField.text isEqualToString:@"Toodledo Tasks"] == TRUE) {
				// Warn user that this is reserved list name
				[MethodHelper showAlertViewWithTitle:@"Reserved Name"
										  andMessage:@"This list title can not be changed and is reserved for Toodledo tasks with no folder"
									  andButtonTitle:@"Ok"];
				[textField setText:refTaskList.title];
			} else {
				// Update title
				[self updateCollectionsListTitle:newTitle];
			}
		}
	} else {
		[textField setText:refTaskList.title];
	}
	
	// Update list completed button
	[self updateListCompletedFrameForOrientation:[self interfaceOrientation]];
	
    [titleTextField setBackgroundColor:[UIColor clearColor]];
    
	[UIView commitAnimations];
    
    // Update control panel row title for task list
    ControlPanelViewController *controlPanelController = [(MAppDelegate *)[[UIApplication sharedApplication] delegate] rootControlPanel];
    [controlPanelController reloadTitle:refTaskList.title atRow:currentTaskListIndex andSection:0];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	// Update the title
	/*if ([textField.text length] > 0) {
     NSString *newTitle = [textField text];
     
     if ([newTitle isEqualToString:refTaskList.title] == FALSE) {
     if ([newTitle isEqualToString:@"Toodledo Tasks"] == TRUE || [textField.text isEqualToString:@"Toodledo Tasks"] == TRUE) {
     // Warn user that this is reserved list name
     [MethodHelper showAlertViewWithTitle:@"Reserved Name"
     andMessage:@"This list title can not be changed as it is reserved for Toodledo tasks with no folder"
     andButtonTitle:@"Ok"];
     } else {
     // Update title
     [self updateCollectionsListTitle:newTitle];
     }
     }
     } else {
     [textField setText:refTaskList.title];
     }*/
	
	// Update list completed button
	[self updateListCompletedFrameForOrientation:[self interfaceOrientation]];
	
	// This will call text field did end editing
	[textField resignFirstResponder];
	return YES;
}

#pragma mark -
#pragma mark EditListItemDelegate Methods

- (void)editScribbleForListItem:(ListItem *)listItem usingRect:(CGRect)rect {
    if ([editListItemPopOverController isPopoverVisible]) {
        [editListItemPopOverController dismissPopoverAnimated:YES];
    }
    
    // Deselect all pens
    [greenPenButton setSelected:NO];
    [bluePenButton setSelected:NO];
    [blackPenButton setSelected:NO];
    [redPenButton setSelected:NO];
    
    id drawPadViewController = [[DrawPadViewController alloc] initWithDelegate:self
                                                                       andSize:kDrawPadViewControllerSize
                                                                   andListItem:listItem
                                                                   andPenColor:EnumPenColorBlack
                                                            andNewScribbleItem:FALSE];
    
    UINavigationController *navController;
    
    BOOL isPNGScribble = [drawPadViewController isPNGScribble];
    if (isPNGScribble) {
        // Need to release the existing drawPadViewController and load the 'old' one
//        [drawPadViewController release];
        
        drawPadViewController = [[PngDrawPadViewController alloc] initWithDelegate:self
                                                                           andSize:kPngDrawPadViewControllerSize
                                                                       andListItem:listItem
                                                                       andPenColor:(EnumPngPenColor)penSelected
                                                                andNewScribbleItem:FALSE];
        navController = [[UINavigationController alloc] initWithRootViewController:drawPadViewController];
    }
    
    //UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:drawPadViewController];
    
    
    if (isPNGScribble == FALSE) {
        drawPadPopOverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:drawPadViewController];
    } else {
        drawPadPopOverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
//        [navController release];
    }
    
    
    [drawPadPopOverController setDelegate:self];
//    [drawPadViewController release];
    //[navController release];
    
    UIPopoverArrowDirection direction = UIPopoverArrowDirectionUp;
    
    if (rect.origin.y > 450) {
        direction = UIPopoverArrowDirectionDown;
    }
    
    [drawPadPopOverController presentPopoverFromRect:rect inView:self.view
                            permittedArrowDirections:direction animated:YES];
    
    // Update the select row prompt setting
    [self setSelectRowPrompt:FALSE];
    
    refTaskList.mainViewRequiresUpdate = TRUE;
}

- (void)editListItemAtBaseLevel:(BOOL)isBaseLevel {
	editListItemViewControllerAtBaseLevel = isBaseLevel;
}

- (void)dismissEditListItemPopOver {
	[editListItemPopOverController dismissPopoverAnimated:YES];
}

- (void)listItem:(ListItem *)listItem wasUpdated:(BOOL)updated completedStatusChanged:(BOOL)completeChanged {
    
    // Yamikaze's change
    MAppDelegate *appDelegate = (MAppDelegate *)[UIApplication sharedApplication].delegate;
    UINavigationController *masterNav = (UINavigationController *)appDelegate.splitViewController.masterViewController;
    ControlPanelViewController *rightPanel = [[masterNav viewControllers] firstObject];
    
	if (updated) {
		// Update the items due table if required
		//if (listItem.completed == FALSE &&
		//	([listItem.dueDate length] > 0 || listItem.dueDate == nil)) {
		// Check other collections for update
        
        if (completeChanged) {
            [self taskCompleted:listItem.completed forListItemID:listItem.listItemID];
        } else {
            [self updateMasterCollectionWithListItem:listItem];
            
            //if ([self isFilterTagsMode]) {
			// Just need to update reftask collection
            [self updateTaskListCollection:refTaskListCollection withListItem:listItem];
            
            [itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
                                                      andParentListID:refTaskList.listID andListItem:listItem];
            
            [self.myTableView reloadData];
            
            // Reload the calendar view
            [miniCalendarView reloadData];
            
            // Yamikaze's change
            [rightPanel.miniCalendarView reloadData];
        }
	}
	
    // Reload the list pad
    [miniListPadTableView reloadData];
    
    // Yamikaze's change
    [rightPanel reloadListPadTableView];
    
	// Release the popover
	//[editListItemPopOverController dismissPopoverAnimated:YES];
}

- (void)listItemDue:(ListItem *)listItem wasUpdated:(BOOL)updated completedStatusChanged:(BOOL)completeChanged {
    
    MAppDelegate *appDelegate = (MAppDelegate *)[UIApplication sharedApplication].delegate;
    
    UINavigationController *masterNav = (UINavigationController *)appDelegate.splitViewController.masterViewController;
    ControlPanelViewController *rightPanel = [[masterNav viewControllers] firstObject];
    
	if (updated) {
		// Need to see whether this list item is in the task list.. shit
		// We loaded up all details at start, will just need to refresh everything on close
		/*NSInteger itemIndex = [refTaskList indexOfListItemWithID:listItem.listItemID];
         
         // -1 means not found
         if (itemIndex != -1) {
         // Found, need to update the list item
         ListItem *foundListItem = [refTaskList.listItems objectAtIndex:itemIndex];
         
         // Check if there is a need to refresh the items due table view
         if (foundListItem.completed != listItem.completed ||
         [foundListItem.dueDate isEqualToString:listItem.dueDate] == FALSE) {*/
        
        //}
		
        //[foundListItem updateSelfWithListItem:listItem];
		//}
        
        if (completeChanged) {
            [self taskCompleted:listItem.completed forListItemID:listItem.listItemID];
        } else {
            // Need to update the correct listItem if this is filter tags or search mode
            if ([self isFilterTagsMode]) {
                [filterTagsTaskListCollection findAndUpdateListItem:listItem];
            } else if ([self isSearchMode]) {
                [filteredSearchTaskListCollection findAndUpdateListItem:listItem];
            }
            
            // Update if needed
            [self updateMasterCollectionWithListItem:listItem];
            
            // Also update the task list collection
            [self updateTaskListCollection:refTaskListCollection withListItem:listItem];
            
            //if ([self isFilterTagsMode]) {
			// Just need to update reftask collection
            //	[self updateTaskListCollection:refTaskListCollection withListItem:listItem];
            //}
            
            // Reload items due table
            [itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
                                                      andParentListID:refTaskList.listID andListItem:listItem];
            
            [self.myTableView reloadData];
            
            // Reload the calendar view
            [miniCalendarView reloadData];
            
            [rightPanel.miniCalendarView reloadData];
        }
	}
    
    // Reload the list pad
    [miniListPadTableView reloadData];
    
    [rightPanel reloadListPadTableView];
    
	// Release the popover
	//[editListItemPopOverController dismissPopoverAnimated:YES];
}

- (void)listItemWasMoved {
	// Close edit pop over
	if ([editListItemPopOverController isPopoverVisible]) {
		[editListItemPopOverController dismissPopoverAnimated:YES];
	}
    
	// Reload collections
    [self reloadCollections];
    /*progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
     [self.navigationController.view addSubview:progressHUD];
     progressHUD.delegate = self;
     progressHUD.labelText = @"Reloading task collections...";
     [progressHUD showWhileExecuting:@selector(reloadCollections) onTarget:self withObject:nil animated:YES];*/
	
    // Refresh Search screen
    if (isSearchMode) {
        [self searchBar:mySearchBar textDidChange:mySearchBar.text];
    }
    
	// Reload tableview
	[self.myTableView reloadData];
    
    // Yamikaze's change
    MAppDelegate *appDelegate = (MAppDelegate *)[UIApplication sharedApplication].delegate;
    UINavigationController *masterNav = (UINavigationController *)appDelegate.splitViewController.masterViewController;
    ControlPanelViewController *rightPanel = [[masterNav viewControllers] firstObject];
    [rightPanel reloadListPadTableView];
}

// Add new handwritten subtask for list item with id?
- (void)newHandwrittenSubtaskCreatedForParentListItem:(ListItem *)parentListItem {
    
    //NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
    if ([editListItemPopOverController isPopoverVisible]) {
        [editListItemPopOverController dismissPopoverAnimated:YES];
    }
    
    // (update alerttime field)
    NSInteger newSubListItemID = [self createNewSubListItemWithTitle:@"" andNotes:@"" andCompleted:NO
                                                          andDueDate:@""
                                                          andDueTime:@""
                                                        andAlertTime:@""
                                                            andSound:@""
                                                           andSnooze:NO
                                                   andParentListItem:parentListItem
                                            andListItemTagCollection:parentListItem.listItemTagCollection];
    
    // So we can display the scribble pad
    selectRowPrompt = TRUE;
    
    // Now show the scribble pad
    NSInteger listItemIndex = [refTaskList getIndexOfListItemWithID:newSubListItemID];
    
    ListItem *subListItem = nil;
    //    if (isShowCompletedTasks == 0) {
    //        subListItem = [arrayTaskList objectAtIndex:listItemIndex];
    //    }else{
    //        subListItem = [refTaskList.listItems objectAtIndex:listItemIndex];
    //    }
    subListItem = [refTaskList.listItems objectAtIndex:listItemIndex];
    
    NSInteger numberOfRowsInSection = [self.myTableView numberOfRowsInSection:0];
    
    // Update new item index to set frame for DrawPadView
    if (listItemIndex > numberOfRowsInSection) {
        selectedRow = numberOfRowsInSection;
    } else {
        selectedRow = listItemIndex;
    }
    
	// Set items due selected to false
	itemsDueSelected = FALSE;
	
	ListItemTableViewCell *cell = (ListItemTableViewCell *)[self.myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]];
	
	// For correctly positioning the popover controller
	CGPoint offset = [self.myTableView contentOffset];
	
	// Multiply selected row by 44, get center of tableview
	CGFloat xOrigin = self.myTableView.frame.origin.x + (self.myTableView.frame.size.width - 320);
	CGFloat yOrigin = cell.frame.origin.y + 22 + self.myTableView.frame.origin.y - offset.y;
	CGRect rectPopover = CGRectMake(xOrigin, yOrigin, 1, 1);
    
    DrawPadViewController *drawPadViewController = [[DrawPadViewController alloc] initWithDelegate:self
                                                                                           andSize:kDrawPadViewControllerSize
                                                                                       andListItem:subListItem
                                                                                       andPenColor:EnumPenColorBlack
                                                                                andNewScribbleItem:TRUE];
    
    // UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:drawPadViewController];
    
    drawPadPopOverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:drawPadViewController];
    
    
    [drawPadPopOverController setDelegate:self];
//    [drawPadViewController release];
    //[navController release];
    
    UIPopoverArrowDirection direction = UIPopoverArrowDirectionUp;
    
    if (rectPopover.origin.y > 450) {
        direction = UIPopoverArrowDirectionDown;
    }
    
    [drawPadPopOverController presentPopoverFromRect:rectPopover inView:self.view
                            permittedArrowDirections:direction animated:YES];
    
    // Update the select row prompt setting
    [self setSelectRowPrompt:FALSE];
    
    refTaskList.mainViewRequiresUpdate = TRUE;
    
}

#pragma mark -
#pragma mark PreviewListItemViewControllerDelegate

- (void)updateTableViewAfterMovingTasks {
    if ([editListItemPopOverController isPopoverVisible]) {
        [editListItemPopOverController dismissPopoverAnimated:YES];
    }
    
    // Reload main table with animation
    [UIView transitionWithView: self.myTableView
                      duration: 0.2f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void)
     {
         [self.myTableView reloadData];
     }
                    completion: ^(BOOL isFinished)
     {
         /* TODO: Whatever you want here */
     }];
}

#pragma mark -
#pragma mark Update Collections

- (void)updateCollectionsListTitle:(NSString *)newListTitle {
	// Get task list in master task list and update
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		if (taskList.listID == currentTaskListID) {
			taskList.title = newListTitle;
            for (ListItem *listItem in taskList.listItems) {
                listItem.parentListTitle = newListTitle;
            }
			self.title = newListTitle;
			[taskList updateDatabase];
		}
	}
    
    // Also need to update the items due collection of list titles
	
	// Reload collections
    [self reloadCollections];
    /*progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
     [self.navigationController.view addSubview:progressHUD];
     progressHUD.delegate = self;
     progressHUD.labelText = @"Reloading task collections...";
     [progressHUD showWhileExecuting:@selector(reloadCollections) onTarget:self withObject:nil animated:YES];*/
}


- (void)removeFilteredListItemsUsingListItem:(ListItem *)listItem {
	// Can exit the method if show completed tasks is true or listItem is not completed
	if ([refTaskListCollection showCompletedTasks] == TRUE || [listItem completed] == FALSE) {
		return;
	}
	
	NSInteger listIndex = [refTaskListCollection getIndexOfTaskListWithID:listItem.listID];
	
	// Get the correct task List
	TaskList *taskList = nil;
	if (listIndex != -1) {
		taskList = [refTaskListCollection.lists objectAtIndex:listIndex];
	} else {
		// Task list not found, exit
		return;
	}
	
	NSInteger listItemIndex = [refTaskList getIndexOfListItemWithID:listItem.listItemID];
	
	if ([listItem parentListItemID] != -1) {
		// Sub task, it does need to be removed because show completed tasks is off
		if (listItemIndex != -1) {
			NSInteger parentListItemID = [listItem parentListItemID];
			[taskList.listItems removeObjectAtIndex:listItemIndex];
			
			// Find the parent subtask to check if it needs to be removed as well
			NSInteger parentListItemIndex = [taskList getIndexOfListItemWithID:parentListItemID];
			if (parentListItemIndex != -1) {
				if ([taskList hasIncompleteSubtasksForListItemAtIndex:parentListItemIndex
												 forCompletedDuration:[refTaskListCollection showCompletedTasksDuration]
											   andShowCompletedStatus:[refTaskListCollection showCompletedTasks]]) {
					// Return, dont delete
					return;
				} else {
					[taskList.listItems removeObjectAtIndex:parentListItemIndex];
				}
                
			}
			
		}
	} else if ([listItem parentListItemID] == -1) {
		// First need to check if any there are any incomplete subtasks before removing
		if ([taskList hasIncompleteSubtasksForListItemAtIndex:listItemIndex
										 forCompletedDuration:[refTaskListCollection showCompletedTasksDuration]
									   andShowCompletedStatus:[refTaskListCollection showCompletedTasks]]) {
			// Return, dont delete
			return;
		} else {
			[taskList.listItems removeObjectAtIndex:listItemIndex];
		}
	}
}

// Update a specified collection
- (void)updateTaskListCollection:(TaskListCollection *)taskListCollection withListItem:(ListItem *)listItem {
	NSInteger listIndex = [taskListCollection getIndexOfTaskListWithID:listItem.listID];
	
	NSInteger listItemIndex = -1;
	TaskList *taskList = nil;
	if (listIndex != -1) {
		taskList = [taskListCollection.lists objectAtIndex:listIndex];
		taskList.mainViewRequiresUpdate = TRUE;
		listItemIndex = [taskList getIndexOfListItemWithID:listItem.listItemID];
	}
	
	if (listItemIndex != -1 && taskList != nil) {
		ListItem *taskListItem = [taskList.listItems objectAtIndex:listItemIndex];
		[taskListItem updateSelfWithListItem:listItem];
	}
}

// Update the list item in the master collection
- (void)updateMasterCollectionWithListItem:(ListItem *)listItem {
	NSInteger listIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:listItem.listID];
	
	NSInteger listItemIndex = -1;
	TaskList *masterTaskList = nil;
	if (listIndex != -1) {
		masterTaskList = [refMasterTaskListCollection.lists objectAtIndex:listIndex];
		listItemIndex = [masterTaskList getIndexOfListItemWithID:listItem.listItemID];
	}
	
	if (listItemIndex != -1 && masterTaskList != nil) {
		ListItem *masterListItem = [masterTaskList.listItems objectAtIndex:listItemIndex];
		[masterListItem updateSelfWithListItem:listItem];
	}
	
	// Need to look/remove from collection if is not filter/tag mode
}

- (void)reloadCollections {
	// Collections:		refTaskListCollection
	//					refTaskList
	//					filterTagsTaskListCollection
	//					itemsDueCollection
	
	// RefTaskList should be updated automatically when refTaskListCollection changes (and vice-versa)
    
	// Keep the last sort type
	NSInteger lastSortType = refTaskList.lastSortType;
    
    [refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
    
	refTaskList = [refTaskListCollection getTaskListWithID:currentTaskListID];
	refTaskList.lastSortType = lastSortType;
    
	// Reload items due table
	[itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
											  andParentListID:refTaskList.listID andListItem:nil];
    
	[filterTagsTaskListCollection filterListsUsingListItemTagCollection:filterListItemTagCollection
										  andOriginalTaskListCollection:refTaskListCollection];
    
    // Reload the calendar view
    [miniCalendarView reloadData];
    
    // Reload the list pad
    [miniListPadTableView reloadData];
}

#pragma mark -
#pragma mark UIActionSheetDelegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ([actionSheet isEqual:listOptionsActionSheet]) {
		NSString *trackerLabel = kAction_sharing;
		if (buttonIndex == 0) {
			// Email text list
            trackerLabel = kLabel_sharing_text;
			if ([self isSearchMode] == FALSE && [self isFilterTagsMode] == FALSE) {
				[self sendTextMailForTaskList:refTaskList];
			} else if ([self isSearchMode] == TRUE) {
				[self sendTextMailForTaskListCollection:filteredSearchTaskListCollection];
			} else if ([self isFilterTagsMode] == TRUE) {
				[self sendTextMailForTaskListCollection:filterTagsTaskListCollection];
			}
			
		} else if (buttonIndex == 1) {
			// Email pdf list
            trackerLabel = kLabel_sharing_pdf;
			if ([self isSearchMode] == FALSE && [self isFilterTagsMode] == FALSE) {
				[self sendPDFMailForTaskList:refTaskList];
			} else if ([self isSearchMode] == TRUE) {
				[self sendPDFMailForTaskListCollection:filteredSearchTaskListCollection];
			} else if ([self isFilterTagsMode] == TRUE) {
				[self sendPDFMailForTaskListCollection:filterTagsTaskListCollection];
			}
		}
#if ENABLE_GOOGLE_ANALYTICS
        // track this event to Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker sendEventWithCategory:kCategory_event
                            withAction:kAction_sharing
                             withLabel:trackerLabel
                             withValue:0];
#endif
	}
}

#pragma mark -
#pragma mark Items Due Responder Delegates

- (void)listItemSelected:(ListItem *)selectedListItem atLocation:(CGRect)popOverRect {
	// Set items due selected to true
	itemsDueSelected = TRUE;
	
	BOOL tagsOrSearchModeOn = FALSE;
	
	if ([self isFilterTagsMode] || [self isSearchMode]) {
		tagsOrSearchModeOn = TRUE;
	}
    
    EditListItemDueViewController *editListItemDueViewController = [[EditListItemDueViewController alloc] initWithDelegate:self andSize:kEditListItemViewControllerSize andListItem:selectedListItem andTagCollection:refTagCollection andIsFilterTagsMode:tagsOrSearchModeOn andTaskListCollection:refMasterTaskListCollection andTaskList:refTaskList didAppearFromListView:FALSE];
	
    //	// Init the edit list item view controller
    //	EditListItemDueViewController *editListItemDueViewController = [[EditListItemDueViewController alloc] initWithDelegate:self
    //																												   andSize:kEditListItemViewControllerSize
    //																											   andListItem:selectedListItem
    //																										  andTagCollection:refTagCollection
    //																									   andIsFilterTagsMode:tagsOrSearchModeOn
    //																									 andTaskListCollection:refMasterTaskListCollection
    //                                                                                                     didAppearFromListView:FALSE];
	
    editListItemDueViewController.popoverRect = popOverRect;
    editListItemDueViewController.isDueDateViewController = YES;
    
	// Init the navigation controller that will hold the edit list item view controller
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:editListItemDueViewController];
	
	// Allocate the pop over controller, that will host the nav controller and view controller
    if (editListItemPopOverController != nil) {
        editListItemPopOverController = nil;
    }
    
    
	editListItemPopOverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
	
//	[navController release];
	
	[editListItemPopOverController setDelegate:self];
	
	[editListItemDueViewController setPopoverController:editListItemPopOverController];
//	[editListItemDueViewController release];
	
	// Present the popover using the calculated location to display the arrow
	[editListItemPopOverController presentPopoverFromRect:popOverRect inView:self.view
								 permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

#pragma mark -
#pragma mark Mini Calendar Delegate

- (void)didSelectDate:(NSDate *)theDate {
    // Open up search bar
    if ([self isSearchMode] == FALSE) {
        [self searchButtonAction];
        
        if ([mySearchBar isFirstResponder]) {
			[mySearchBar resignFirstResponder];
		}
		
    }
    
    // Set the date range
    NSString *stringDate = [MethodHelper stringFromDate:theDate usingFormat:K_DATEONLY_FORMAT];
    
    searchStartDateRange = [stringDate copy];
    searchEndDateRange = [stringDate copy];
    
    NSString *newDateRangeLabelText = [NSString stringWithFormat:@"Date Range: %@ to %@",
                                       stringDate, stringDate];
    
    [searchDateRangeLabel setText:newDateRangeLabelText];
    
    // Refresh the search
    [self searchBar:mySearchBar textDidChange:mySearchBar.text];
    
}

#pragma mark -
#pragma mark Settings Delegate

- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData {
	if ([settingName isEqualToString:@"ShowCompleted"]) {
		refMasterTaskListCollection.showCompletedTasks = [theData boolValue];
		refTaskListCollection.showCompletedTasks = [theData boolValue];
		// Update collection
		// Reload collections
        [self reloadCollections];
        /*progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
         [self.navigationController.view addSubview:progressHUD];
         progressHUD.delegate = self;
         progressHUD.labelText = @"Reloading task collections...";
         [progressHUD showWhileExecuting:@selector(reloadCollections) onTarget:self withObject:nil animated:YES];*/
        
		[self.myTableView reloadData];
	} else if ([settingName isEqualToString:@"ShowCompletedDuration"]) {
		refMasterTaskListCollection.showCompletedTasksDuration = [refMasterTaskListCollection getShowCompletedDurationEnumForData:theData];
		refTaskListCollection.showCompletedTasksDuration = [refTaskListCollection getShowCompletedDurationEnumForData:theData];
		
		// Update the other collections
		// Reload collections
        [self reloadCollections];
        /*progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
         [self.navigationController.view addSubview:progressHUD];
         progressHUD.delegate = self;
         progressHUD.labelText = @"Reloading task collections...";
         [progressHUD showWhileExecuting:@selector(reloadCollections) onTarget:self withObject:nil animated:YES];*/
		[self.myTableView reloadData];
	} else if ([settingName isEqualToString:@"ArchiveItems"]) {
		refMasterTaskListCollection.archiveSetting = theData;
		refTaskListCollection.archiveSetting = theData;
	} else if ([settingName isEqualToString:@"PocketReminder"]) {
		refMasterTaskListCollection.pocketReminder = [theData boolValue];
		refTaskListCollection.pocketReminder = [theData boolValue];
		
		// Now need a pocket reminder switch
		if (refMasterTaskListCollection.pocketReminder == FALSE) {
			[itemsDuePocket setHidden:YES];
			[itemsDueTableView setHidden:YES];
		} else if (refMasterTaskListCollection.pocketReminder == TRUE) {
			[itemsDuePocket setHidden:NO];
			[itemsDueTableView setHidden:NO];
		}
	} else if ([settingName isEqualToString:@"NewListOrder"]) {
		refMasterTaskListCollection.theNewListOrder = theData;
		refTaskListCollection.theNewListOrder = theData;
	} else if ([settingName isEqualToString:@"DefaultSortOrder"]) {
		refMasterTaskListCollection.defaultSortOrder = theData;
		refTaskListCollection.defaultSortOrder = theData;
	} else if ([settingName isEqualToString:@"NewTaskOrder"]) {
		refMasterTaskListCollection.theNewTaskOrder = theData;
		refTaskListCollection.theNewTaskOrder = theData;
	} else if ([settingName isEqualToString:@"SelectedTheme"]) {
		refMasterTaskListCollection.selectedTheme = theData;
		refTaskListCollection.selectedTheme = theData;
		[self reloadTheme];
	}
}

- (void)settingsPopoverCanClose:(BOOL)canClose {
	settingsViewControllerCanClose = canClose;
}

- (void)closeSettingsPopover {
	
}

- (void)updatesToDataComplete {
	// Do nothing as we should not be able to sync from list view
}

- (void)replaceToodledoDataComplete {
    
    // Do nothing as we can not sync from list view
}

- (void)shopfrontSelected {
    // Do nothing
}

#pragma mark -
#pragma mark UIPopoverControllerDelegate Methods

/*
 Called on the delegate when the popover controller will dismiss the popover. Return NO to prevent the dismissal of the view.
 */
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
	if ([popoverController isEqual:drawPadPopOverController]) {
		return NO;
	}
	
	// Prevent new list item pop over controller being dismissed
	if ([popoverController isEqual:newListItemPopOverController]) {
		return NO;
	}
	
	if ([popoverController isEqual:settingsPopoverController]) {
		if (settingsViewControllerCanClose == FALSE) {
			return NO;
		} else {
			// Reload collections
            [self reloadCollections];
            /*progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
             [self.navigationController.view addSubview:progressHUD];
             progressHUD.delegate = self;
             progressHUD.labelText = @"Reloading task collections...";
             [progressHUD showWhileExecuting:@selector(reloadCollections) onTarget:self withObject:nil animated:YES];*/
			[self.myTableView reloadData];
		}
        
	}
	
	// If this is edit pop over controller, then need to get the updated list item and update to database
	if ([popoverController isEqual:editListItemPopOverController]) {
		if (editListItemViewControllerAtBaseLevel == FALSE) {
			return NO;
		}
		
		// Reload items due table view
		//[itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
		//										  andParentListID:refTaskList.listID];
		
		//[myTableView reloadData];
		return YES;
	}
	
	return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	if ([popoverController isEqual:settingsPopoverController]) {
		[self.myTableView reloadData];
	}
	
	// Do something if this is the highlighters popover
	if ([popoverController isEqual:highlightersPopoverController]) {
		// Reset selected highlighter to none
		highlighterSelected = EnumHighlighterColorNone;
	}
	
    
	// Resize table view if filter tags has content
	if ([popoverController isEqual:filterTagsPopoverController]) {
		// Create animation scope
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.3f];
		if ([filterListItemTagCollection.listItemTags count] == 0) {
			// Remove all objects from the filtertags task collection
			[filterTagsTaskListCollection.lists removeAllObjects];
			
			// Hide objects
			[self hideFilterTagObjects:YES];
			if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
				[self.myTableView setFrame:kListTableViewPortrait];
			} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
				[self.myTableView setFrame:kListTableViewLandscape];
			}
			[filterTagsLabel setText:@""];
		} else {
			// Filter the filterTagsTaskCollection
			[filterTagsTaskListCollection filterListsUsingListItemTagCollection:filterListItemTagCollection
												  andOriginalTaskListCollection:refTaskListCollection];
			
			// Set text of tags label
			BOOL firstRun = TRUE;
			NSString *tagString = @"";
			for (ListItemTag *listItemTag in filterListItemTagCollection.listItemTags) {
                
				NSInteger tagIndex = [refTagCollection getIndexOfTagWithID:listItemTag.tagID];
				NSString *tagName = [[refTagCollection.tags objectAtIndex:tagIndex] name];
				//tagString = [NSString stringWithFormat:@"%@
				
				if (firstRun) {
					tagString = [NSString stringWithFormat:@"\"%@\"", tagName];
					firstRun = FALSE;
				} else {
					tagString = [NSString stringWithFormat:@"%@, \"%@\"", tagString, tagName];
				}
				
			}
			[filterTagsLabel setText:tagString];
			
			// Show filter tag objects
			[self hideFilterTagObjects:NO];
			if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
				[self.myTableView setFrame:kFilterTagsListTableViewPortrait];
			} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
				[self.myTableView setFrame:kFilterTagsListTableViewLandscape];
			}
		}
		[UIView commitAnimations];
		[self filterTagObjectsOrientation:self.interfaceOrientation];
		
		// Reload table view
		[self.myTableView reloadData];
	}
	
}

#pragma mark -
#pragma mark DrawPadDelegate Methods

- (void)dismissDrawPadPopOver:(BOOL)newScribbleItem {
	[drawPadPopOverController dismissPopoverAnimated:YES];
    drawPadPopOverController = nil;
//    [drawPadPopOverController release];
    
    if (newScribbleItem == TRUE) {
        // We need to remove the last added item
        // Get the selected row
        if (selectedRow >= [refTaskList.listItems count] || selectedRow < 0) {
            return;
        }
        
        // Get the list item, we need to delete it
        //- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedRow inSection:0];
        
        // Get the index of the list item
		ListItemTableViewCell *cell = (ListItemTableViewCell *)[self.myTableView cellForRowAtIndexPath:indexPath];
		
		// Can do the delete by always doing a lookup on complete task collection first and using that to
		// check if there are any subtasks, of course all lists get this list item deleted
		// Can get the list item from the found task list
		NSInteger listItemID = cell.tag;
        
        NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
        
        // Need to find which task list this is in
        for (TaskList *taskList in refMasterTaskListCollection.lists) {
            
            NSInteger listItemIndex = [taskList getIndexOfListItemWithID:listItemID];
            if (listItemIndex != -1) {
                
                if (isShowCompletedTasks == 0) {
                    // Delete the list item
                    [taskList deleteListItemAtIndex:listItemIndex permanent:NO];
                    [refTaskList.listItems removeObjectAtIndex:listItemIndex];
                    [self.myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    return;
                }else{
                    // Delete the list item
                    [taskList deleteListItemAtIndex:listItemIndex permanent:NO];
                }
                
                // Specify that task list requires update
                taskList.mainViewRequiresUpdate = YES;
                
                // Reload collections
                [self reloadCollections];
                
                [self.myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                
                self.isShowWritingTasksPopover = NO;
            }
        }
    }
}

- (void)drawPadUpdatedForListItem:(ListItem *)listItem {
    // Need to handle updating the current 'reftasklist' as well
	[self updateMasterCollectionWithListItem:listItem];
	
    if ([[listItem dueDate] length] > 0) {
        // Refresh the itemsDueTableView
        [itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
                                                  andParentListID:refTaskList.listID andListItem:listItem];
        [itemsDueTableView reloadData];
        
        if ([self isFilterTagsMode] == FALSE && isSearchMode == FALSE) {
            [self updateTaskListCollection:refTaskListCollection withListItem:listItem];
        } else if (isSearchMode == TRUE) {
            [self updateTaskListCollection:filteredSearchTaskListCollection withListItem:listItem];
        } else if ([self isFilterTagsMode] == TRUE) {
            [self updateTaskListCollection:filterTagsTaskListCollection withListItem:listItem];
        }
    }
    
	refTaskList.mainViewRequiresUpdate = TRUE;
	
    // Reload main table with animation
    [UIView transitionWithView: self.myTableView
                      duration: 0.2f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void)
     {
         [self.myTableView reloadData];
     }
                    completion: ^(BOOL isFinished)
     {
         /* TODO: Whatever you want here */
     }];
	
	[drawPadPopOverController dismissPopoverAnimated:YES];
    self.isShowWritingTasksPopover = NO;
    drawPadPopOverController = nil;
//    [drawPadPopOverController release];
    
    // Yamikaze's change
    // Update left panel
    MAppDelegate *appDelagate = (MAppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelagate.rootControlPanel.miniCalendarView reloadData];
    [appDelagate.rootControlPanel reloadListPadTableView];
}

#pragma mark -
#pragma mark Send Mail Methods

- (void)sendTextMailForTaskList:(TaskList *)theTaskList {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
//		[mailAlert release];
		return;
	}
	
	BOOL showSubtasks = TRUE;
	if ([sortItemsSegmentedControl selectedSegmentIndex] != 0) {
		showSubtasks = FALSE;
	}
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	[controller setMessageBody:[theTaskList getTextMailMessageWithTagCollection:refTagCollection showSubtasks:showSubtasks] isHTML:NO];
	
	[controller setSubject:[NSString stringWithFormat:@"Manage Task List: %@", theTaskList.title]];
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
    
	[self presentModalViewController:controller animated:YES];
//	[controller release];
}

- (void)sendTextMailForTaskListCollection:(TaskListCollection *)theTaskListCollection {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
//		[mailAlert release];
		return;
	}
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	[controller setMessageBody:[theTaskListCollection getTextMailMessageWithTagCollection:refTagCollection showSubtasks:NO] isHTML:NO];
	
	[controller setSubject:@"Manage Task Lists"];
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	[self presentModalViewController:controller animated:YES];
//	[controller release];
}

- (void)sendPDFMailForTaskList:(TaskList *)theTaskList {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
//		[mailAlert release];
		return;
	}
	
	BOOL showSubtasks = TRUE;
	
	if ([sortItemsSegmentedControl selectedSegmentIndex] != 0) {
		showSubtasks = FALSE;
	}
	
	NSString *pdfPath = [theTaskList createPDFMessageWithTagCollection:refTagCollection showSubtasks:showSubtasks];
	
	NSData *dataObj = [NSData dataWithContentsOfFile:pdfPath];
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	
	[controller setSubject:[NSString stringWithFormat:@"Manage Task List: %@", theTaskList.title]];
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	[controller addAttachmentData:dataObj
						 mimeType:@"application/pdf" fileName:@"Manage Task List.pdf"];
	
	[self presentModalViewController:controller animated:YES];
//	[controller release];
}

- (void)sendPDFMailForTaskListCollection:(TaskListCollection *)theTaskListCollection {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
//		[mailAlert release];
		return;
	}
	
	/*BOOL showSubtasks = TRUE;
     
     if ([sortItemsSegmentedControl selectedSegmentIndex] != 0) {
     showSubtasks = FALSE;
     }*/
	
	NSString *pdfPath = [theTaskListCollection createPDFMessageWithTagCollection:refTagCollection showSubtasks:NO];
	
	NSData *dataObj = [NSData dataWithContentsOfFile:pdfPath];
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	
	[controller setSubject:@"Manage Task Lists"];
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	[controller addAttachmentData:dataObj
						 mimeType:@"application/pdf" fileName:@"Manage Task Lists.pdf"];
	
	[self presentModalViewController:controller animated:YES];
//	[controller release];
}

#pragma mark -
#pragma mark Mail Composer View Delegates

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	NSString *message = @"";
	
	switch (result) {
		case MFMailComposeResultCancelled:
			message = @"Email Cancelled";
			
			break;
		case MFMailComposeResultSaved:
			message = @"Email Cancelled";
			break;
		case MFMailComposeResultSent:
			message = @"Email Sent";
			break;
		case MFMailComposeResultFailed:
			message = @"Email Failed";
			break;
		default:
			break;
	}
	
	// Display result from mail composer
	UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Mail Composer"
														message: message
													   delegate: nil
											  cancelButtonTitle: @"Ok"
											  otherButtonTitles: nil];
	[mailAlert show];
//	[mailAlert release];
	
	[self becomeFirstResponder];
	[self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark ListPadDelegate Methods

// Conformance with the list pad delegate methods
- (void)listSelected:(NSInteger)theSelectedRow {
    if (theSelectedRow < 0 || theSelectedRow >= [refTaskListCollection.lists count]) {
        return;
    }
    
    currentTaskListIndex = theSelectedRow;
    
    [miniListPadTableView setUserInteractionEnabled:FALSE];
    
    // Close the search bar if it is open
    if ([self isSearchMode]) {
        [self clearSearchObjectsButtonAction];
    }
    
    if ([self isFilterTagsMode]) {
        [self filterTagsClearButtonAction];
    }
    
    // Bring the page image to front
    [self.view bringSubviewToFront:pageImage];
    [self.view bringSubviewToFront:borderImage];
    [self.view bringSubviewToFront:controlPanelImage];
    [self.view bringSubviewToFront:controlPanelTypeSegmentedControl];
    [self.view bringSubviewToFront:toolsContainerView];
    [self.view bringSubviewToFront:itemsDuePocket];
    [self.view bringSubviewToFront:itemsDueTableView];
    
    // Assign the task list to local reference object
    refTaskList = [refTaskListCollection.lists objectAtIndex:theSelectedRow];
    _currentTaskList = [refTaskListCollection.lists objectAtIndex:theSelectedRow];
    
    // Need to jump out here if this is a note
    if ([refTaskList isNote]) {
        [UIView beginAnimations:@"Page to Note Curl Animation" context:nil];
        [UIView setAnimationDuration:1.0f];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:pageImage cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
        [UIView commitAnimations];
        return;
    }
    
    currentTaskListID = refTaskList.listID;
    
    // Assign the current task list id to the master task list collection
    refMasterTaskListCollection.currentTaskListID = currentTaskListID;
    
    // Assign to the normal task list collection
    refTaskListCollection.currentTaskListID = currentTaskListID;
    
    // Set title
    self.title = refTaskList.title;
    
    // Init the select row prompt to false
    selectRowPrompt = FALSE;
    
    // Set the selected row to 0
    selectedRow = 0;
    
    // Set settings view controller can close to true
    settingsViewControllerCanClose = TRUE;
    
    // Init date range for start and end date in search
    searchStartDateRange = @"";
    searchEndDateRange = @"";
    
    // Update title text field, date and list completed
    [titleTextField setText:refTaskList.title];
    NSDate *theDate = [MethodHelper dateFromString:refTaskList.creationDate usingFormat:K_DATEONLY_FORMAT];
    [dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES] forState:UIControlStateNormal];
    [listCompletedButton setSelected:refTaskList.completed];
    [self updateListCompletedFrameForOrientation:[self interfaceOrientation]];
    [self.myTableView reloadData];
    
    /*
     pageImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PaperPreview.png"]];
     if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
     [canvassImage setFrame:kCanvassImagePortrait];
     [pageImage setFrame:kPageImagePortrait];
     } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
     [pageImage setFrame:kPageImageLandscape];
     [canvassImage setFrame:kCanvassImageLandscape];
     }
     [self.view addSubview:canvassImage];
     [self.view addSubview:pageImage];
     */
    
    [miniListPadTableView setUserInteractionEnabled:FALSE];
    [UIView beginAnimations:@"Page Curl Animation" context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:pageImage cache:YES];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView commitAnimations];
}

- (void)loadingListsSelected {
    // Do nothing
}

- (void)notebookSelected:(NSInteger)theSelectedRow {
    
}

- (void)archivesSelected {
    // [self archivesButtonAction];
}

- (void)shopfrontSelectedWithFrame:(CGRect)theFrame {
    
}

- (void)deleteRowsAtIndexPaths:(NSArray *)indexPaths {
    [miniListPadTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
}

- (void)insertRowsAtIndexPaths:(NSArray *)indexPaths {
    [miniListPadTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
}

- (void)reloadListPadTableView {
    // No need to do anything
}

#pragma mark -
#pragma mark UISearchBarDelegate Methods

// Tells the delegate that the user changed the search text.
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	if ([self searchRangeType] == EnumSearchRangeTypeAllLists) {
        searchTitleLabel.text = @"Search (All Lists)";
    } else if ([self searchRangeType] == EnumSearchRangeTypeThisList) {
        searchTitleLabel.text = @"Search (This List)";
    }
    
    NSString *scope = @"";
    
	switch (searchScopeSegmentedControl.selectedSegmentIndex) {
		case 0:
			scope = @"Title";
			break;
		case 1:
			scope = @"Notes";
			break;
		case 2:
			scope = @"Tags";
			break;
		case 3:
			scope = @"All";
			break;
		default:
			break;
	}
	
	
	[self filterContentForSearchText:searchText scope:scope];
	
	// Reload the table view
	[self.myTableView reloadData];
}

// Tells the delegate that the user finished editing the search text.
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
	//[myTableView reloadData];
}

// Tells the delegate when the user begins editing the search text.
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
	// Set selected index to 0 (title)
	searchScopeSegmentedControl.selectedSegmentIndex = 0;
	
	//[myTableView reloadData];
}


#pragma mark -
#pragma mark Search Object Helpers

- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope {
	// Release all objects in filtered search task list collection
	if ([filteredSearchTaskListCollection.lists count] > 0) {
		[filteredSearchTaskListCollection.lists removeAllObjects];
	}
	
    BOOL dateRangeExists = FALSE;
    if ([searchEndDateRange length] > 0 || [searchStartDateRange length] > 0) {
        dateRangeExists = TRUE;
    }
	
    if (searchText == nil) {
        return;
    }
    if ([searchText length] == 0 && dateRangeExists == FALSE) {
		return;
	}
    
    // Special string to return all
    if ([searchText isEqualToString:@"SPECIAL: GET ALL FROM MANAGE! :)"]) {
        searchText = @"";
    }
    
    NSString *type = @"this list";
    if ([self searchRangeType] == EnumSearchRangeTypeAllLists) {
        type = @"all list";
    }
    NSString *hasDate = @"NO";
    if (dateRangeExists == TRUE) {
        hasDate = @"YES";
    }
#if ENABLE_GOOGLE_ANALYTICS
    // track this event to Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker sendEventWithCategory:kCategory_event
                        withAction:kAction_search
                         withLabel:[NSString stringWithFormat:kLabel_search_format, searchText, scope, type, hasDate]
                         withValue:0];
#endif
	// Need to go through every collection comparing
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		TaskList *newTaskList = [[TaskList alloc] initWithBasicTaskListCopy:taskList];
		
        // Skip if we are only searching current list
        if ([self searchRangeType] == EnumSearchRangeTypeThisList && newTaskList.listID != refTaskList.listID) {
            continue;
        }
        
		for (ListItem *listItem in taskList.listItems) {
            
            
            NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
            
            if (isShowCompletedTasks == 1) {
                
                BOOL isWithinDateRange = TRUE;
                NSRange resultTitleRange = NSMakeRange(0, 0);
                NSRange resultNotesRange = NSMakeRange(0, 0);
                NSRange	resultTagsRange;
                
                if (dateRangeExists == TRUE && [[listItem dueDate] length] == 0) {
                    continue; // Not going to add this one at all to filter list
                }
                
                if ([scope isEqualToString:@"All"] || [scope isEqualToString:@"Title"]) {
                    resultTitleRange = [listItem.title rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [listItem.title length]) locale:[NSLocale currentLocale]];
                    
                    // Check to see if we need to filter by date too
                    // Check range of date
                    
                    
                    //resultTitle = [listItem.title compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length]) locale:[NSLocale currentLocale]];
                }
                
                if ([scope isEqualToString:@"All"] || ([scope isEqualToString:@"Notes"] && resultTitleRange.length == 0)) {
                    //resultNotes = [listItem.title compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length]) locale:[NSLocale currentLocale]];
                    resultNotesRange = [listItem.notes rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [listItem.notes length]) locale:[NSLocale currentLocale]];
                    
                }
                
                if (dateRangeExists == TRUE) {
                    if ([[listItem dueDate] length] == 0) {
                        isWithinDateRange = FALSE;
                    } else if ([searchEndDateRange length] > 0 && [searchStartDateRange length] > 0) {
                        NSComparisonResult beforeResult = [listItem.dueDate compare:searchEndDateRange];
                        NSComparisonResult afterResult = [listItem.dueDate compare:searchStartDateRange];
                        
                        if ((beforeResult == NSOrderedAscending || beforeResult == NSOrderedSame) &&
                            (afterResult == NSOrderedDescending || afterResult == NSOrderedSame)) {
                            isWithinDateRange = TRUE;
                        } else {
                            isWithinDateRange = FALSE;
                        }
                    } else if ([searchEndDateRange length] == 0 && [searchStartDateRange length] > 0) {
                        NSComparisonResult afterResult = [listItem.dueDate compare:searchStartDateRange];
                        if (afterResult == NSOrderedDescending || afterResult == NSOrderedSame) {
                            isWithinDateRange = TRUE;
                        } else {
                            isWithinDateRange = FALSE;
                        }
                    } else if ([searchEndDateRange length] > 0 && [searchStartDateRange length] == 0) {
                        NSComparisonResult beforeResult = [listItem.dueDate compare:searchEndDateRange];
                        if (beforeResult == NSOrderedAscending || beforeResult == NSOrderedSame) {
                            isWithinDateRange = TRUE;
                        } else {
                            isWithinDateRange = FALSE;
                        }
                    }
                }
                
                // Need to do it this way, as range will be changed when there is a following tag that does not have the letter.
                BOOL resultsTagFound = FALSE;
                if (([scope isEqualToString:@"All"] || [scope isEqualToString:@"Tags"]) && (resultTitleRange.length == 0 && resultNotesRange.length == 0)) {
                    for (ListItemTag *listItemTag in listItem.listItemTagCollection.listItemTags) {
                        NSString *tagName = [refTagCollection getNameForTagWithID:[listItemTag tagID]];
                        //resultTags = [tagName compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length]) locale:[NSLocale currentLocale]];
                        resultTagsRange = [tagName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [tagName length]) locale:[NSLocale currentLocale]];
                        
                        if (resultTagsRange.length > 0) {
                            resultsTagFound = TRUE;
                        }
                    }
                }
                
                if (resultTitleRange.length > 0 || resultNotesRange.length > 0 || resultsTagFound == TRUE || [searchText length] == 0) {// || resultNotes == NSOrderedSame || resultTags == NSOrderedSame) {
                    // Now need to add this object to the filtered list
                    if (dateRangeExists == TRUE && isWithinDateRange == FALSE) {
                        // Do nothing
                    } else {
                        ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
                        [newTaskList.listItems addObject:newListItem];
//                        [newListItem release];
                    }
                    
                }
                
            }else{
                if (listItem.completed != YES) {
                    
                    BOOL isWithinDateRange = TRUE;
                    NSRange resultTitleRange = NSMakeRange(0, 0);
                    NSRange resultNotesRange = NSMakeRange(0, 0);
                    NSRange	resultTagsRange;
                    
                    if (dateRangeExists == TRUE && [[listItem dueDate] length] == 0) {
                        continue; // Not going to add this one at all to filter list
                    }
                    
                    if ([scope isEqualToString:@"All"] || [scope isEqualToString:@"Title"]) {
                        resultTitleRange = [listItem.title rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [listItem.title length]) locale:[NSLocale currentLocale]];
                        
                        // Check to see if we need to filter by date too
                        // Check range of date
                        
                        
                        //resultTitle = [listItem.title compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length]) locale:[NSLocale currentLocale]];
                    }
                    
                    if ([scope isEqualToString:@"All"] || ([scope isEqualToString:@"Notes"] && resultTitleRange.length == 0)) {
                        //resultNotes = [listItem.title compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length]) locale:[NSLocale currentLocale]];
                        resultNotesRange = [listItem.notes rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [listItem.notes length]) locale:[NSLocale currentLocale]];
                        
                    }
                    
                    if (dateRangeExists == TRUE) {
                        if ([[listItem dueDate] length] == 0) {
                            isWithinDateRange = FALSE;
                        } else if ([searchEndDateRange length] > 0 && [searchStartDateRange length] > 0) {
                            NSComparisonResult beforeResult = [listItem.dueDate compare:searchEndDateRange];
                            NSComparisonResult afterResult = [listItem.dueDate compare:searchStartDateRange];
                            
                            if ((beforeResult == NSOrderedAscending || beforeResult == NSOrderedSame) &&
                                (afterResult == NSOrderedDescending || afterResult == NSOrderedSame)) {
                                isWithinDateRange = TRUE;
                            } else {
                                isWithinDateRange = FALSE;
                            }
                        } else if ([searchEndDateRange length] == 0 && [searchStartDateRange length] > 0) {
                            NSComparisonResult afterResult = [listItem.dueDate compare:searchStartDateRange];
                            if (afterResult == NSOrderedDescending || afterResult == NSOrderedSame) {
                                isWithinDateRange = TRUE;
                            } else {
                                isWithinDateRange = FALSE;
                            }
                        } else if ([searchEndDateRange length] > 0 && [searchStartDateRange length] == 0) {
                            NSComparisonResult beforeResult = [listItem.dueDate compare:searchEndDateRange];
                            if (beforeResult == NSOrderedAscending || beforeResult == NSOrderedSame) {
                                isWithinDateRange = TRUE;
                            } else {
                                isWithinDateRange = FALSE;
                            }
                        }
                    }
                    
                    // Need to do it this way, as range will be changed when there is a following tag that does not have the letter.
                    BOOL resultsTagFound = FALSE;
                    if (([scope isEqualToString:@"All"] || [scope isEqualToString:@"Tags"]) && (resultTitleRange.length == 0 && resultNotesRange.length == 0)) {
                        for (ListItemTag *listItemTag in listItem.listItemTagCollection.listItemTags) {
                            NSString *tagName = [refTagCollection getNameForTagWithID:[listItemTag tagID]];
                            //resultTags = [tagName compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length]) locale:[NSLocale currentLocale]];
                            resultTagsRange = [tagName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [tagName length]) locale:[NSLocale currentLocale]];
                            
                            if (resultTagsRange.length > 0) {
                                resultsTagFound = TRUE;
                            }
                        }
                    }
                    
                    if (resultTitleRange.length > 0 || resultNotesRange.length > 0 || resultsTagFound == TRUE || [searchText length] == 0) {// || resultNotes == NSOrderedSame || resultTags == NSOrderedSame) {
                        // Now need to add this object to the filtered list
                        if (dateRangeExists == TRUE && isWithinDateRange == FALSE) {
                            // Do nothing
                        } else {
                            ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
                            [newTaskList.listItems addObject:newListItem];
//                            [newListItem release];
                        }
                        
                    }
                }
            }
		}
		
		// Add the new task list if needed
		if ([newTaskList.listItems count] > 0) {
			[filteredSearchTaskListCollection.lists addObject:newTaskList];
		}
//		[newTaskList release];
	}
	
}

- (void)hideSearchObjects:(BOOL)isHidden {
	if (isHidden) {
		[searchScopeSegmentedControl setAlpha:0.0f];
		[filterTagsFooterLine setAlpha:0.0f];
		[mySearchBar setAlpha:0.0f];
		[clearSearchObjectsButton setAlpha:0.0f];
		[searchDateRangeButton setAlpha:0.0f];
        [searchListRangeButton setAlpha:0.0f];
        [showAllTasksButton setAlpha:0.0f];
        [searchTitleLabel setAlpha:0.0f];
		[searchDateRangeLabel setAlpha:0.0f];
        [sortItemsSegmentedControl setAlpha:1.0f];
		
		[filterTagsButton setEnabled:YES];
		[newListItemButton setEnabled:YES];
		[newScribbleListItemButton setEnabled:YES];
		[editListButton setEnabled:YES];
		
		if ([refTaskList completed]) {
			[listCompletedStampImage setHidden:NO];
		}
		
		[listCompletedButton setHidden:NO];
		
	} else {
		[searchScopeSegmentedControl setAlpha:1.0f];
		[filterTagsFooterLine setAlpha:1.0f];
		[mySearchBar setAlpha:1.0f];
		[clearSearchObjectsButton setAlpha:1.0f];
		[searchDateRangeButton setAlpha:1.0f];
        [searchListRangeButton setAlpha:1.0f];
        [showAllTasksButton setAlpha:1.0f];
        [searchTitleLabel setAlpha:1.0f];
		[searchDateRangeLabel setAlpha:1.0f];
        
		[sortItemsSegmentedControl setAlpha:0.0f];
		
		[filterTagsButton setEnabled:NO];
		[newListItemButton setEnabled:NO];
		[newScribbleListItemButton setEnabled:NO];
		[editListButton setEnabled:NO];
		
		// Also need to hide completed stamp
		[listCompletedStampImage setHidden:YES];
		
		// Also hide tick completed icon
		[listCompletedButton setHidden:YES];
	}
}

#pragma mark -
#pragma mark Class Helper Methods

// Class method to reload table view after delay
- (void)reloadTableView {
	[self.myTableView reloadData];
}



- (void)updateListCompletedFrameForOrientation:(UIInterfaceOrientation)theInterfaceOrientation {
	NSString *titleString = [titleTextField text];
	CGSize size = [titleString sizeWithFont:[titleTextField font]];
    
	if (size.width > (titleTextField.frame.size.width -8)) {
		size.width = (titleTextField.frame.size.width -8);
	}
	
	CGRect titleFrame = [titleTextField frame];
	
	if ([titleTextField isEditing]) {
		if (UIInterfaceOrientationIsPortrait(theInterfaceOrientation)) {
			titleFrame = kTitleTextFieldPortrait;
		} else if (UIInterfaceOrientationIsLandscape(theInterfaceOrientation)) {
			titleFrame = kTitleTextFieldLandscape;
		}
	}
	
	CGFloat rightBorder = titleFrame.origin.x + titleFrame.size.width;
	[listCompletedButton setFrame:CGRectMake(rightBorder - (size.width + 45),
											 titleFrame.origin.y,
											 36, 36)];
}

/*
 UIFont *myFont = [UIFont fontWithName:@"Helvetica" size:14.0];
 // Get the width of a string ...
 NSString *taskItemString = [cell.taskItem text];
 CGSize size = [taskItemString sizeWithFont:myFont];
 
 */

- (void)setSelectRowPrompt:(BOOL)newSetting {
	selectRowPrompt = newSetting;
	
	if (newSetting == TRUE) {
		// Disable buttons and controls
		[newListItemButton setEnabled:FALSE];
		[newScribbleListItemButton setEnabled:FALSE];
		//[penButton setEnabled:FALSE];
		[highlighterButton setEnabled:FALSE];
		[editListButton setEnabled:FALSE];
		[sortItemsSegmentedControl setEnabled:FALSE];
		[titleTextField setEnabled:FALSE];
		[listCompletedButton setEnabled:FALSE];
		[itemsDueTableView setUserInteractionEnabled:FALSE];
		[filterTagsButton setEnabled:FALSE];
		// Enable pens and highlighters
		//[greenPenButton setEnabled:FALSE];
		//[bluePenButton setEnabled:FALSE];
		//[redPenButton setEnabled:FALSE];
		//[blackPenButton setEnabled:FALSE];
		[redHighlighterButton setEnabled:FALSE];
		[yellowHighlighterButton setEnabled:FALSE];
		[whiteHighlighterButton setEnabled:FALSE];
		
		[selectTaskImageView setAlpha:0.0f];
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.5f];
		[selectTaskImageView setAlpha:0.5f];
		[UIView commitAnimations];
	} else {
		// Enable buttons and controls
		if ([self isFilterTagsMode] == FALSE && [self isSearchMode] == FALSE) {
			[newListItemButton setEnabled:TRUE];
			[newScribbleListItemButton setEnabled:TRUE];
			[editListButton setEnabled:TRUE];
		}
		
		//[penButton setEnabled:TRUE];
		[highlighterButton setEnabled:TRUE];
		
		[sortItemsSegmentedControl setEnabled:TRUE];
		[titleTextField setEnabled:TRUE];
		[listCompletedButton setEnabled:TRUE];
		[itemsDueTableView setUserInteractionEnabled:TRUE];
		[filterTagsButton setEnabled:TRUE];
		// Enable pens and highlighters
		//[greenPenButton setEnabled:TRUE];
		//[bluePenButton setEnabled:TRUE];
		//[redPenButton setEnabled:TRUE];
		//[blackPenButton setEnabled:TRUE];
		[redHighlighterButton setEnabled:TRUE];
		[yellowHighlighterButton setEnabled:TRUE];
		[whiteHighlighterButton setEnabled:TRUE];
		
		
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.5f];
		[selectTaskImageView setAlpha:0.0f];
		[UIView commitAnimations];
	}
    
	
	[self.myTableView reloadData];
}


- (UIImageView *)getCellNotesIcon {
	UIImage *notesIconImage = [MethodHelper scaleImage:[UIImage imageNamed:@"NotesIcon.png"]
											  maxWidth:22 maxHeight:22];
	UIImageView *cellNotesIcon = [[UIImageView alloc] initWithImage:notesIconImage];// autorelease];
	cellNotesIcon.tag = kNotesIconTag;
	[cellNotesIcon setHidden:YES];
	return cellNotesIcon;
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
    [progressHUD removeFromSuperview];
//    [progressHUD release];
}

#pragma mark -
#pragma mark Tag Object Methods

- (BOOL)isFilterTagsMode {
	if ([filterListItemTagCollection.listItemTags count] > 0) {
		return TRUE;
	}
	return FALSE;
}

- (void)filterTagObjectsOrientation:(UIInterfaceOrientation)theOrientation {
	UIFont *myFont = [UIFont fontWithName:@"Helvetica" size:17.0];
	// Get the width of a string ...
	NSString *filterTagString = [filterTagsLabel text];
	CGSize size = [filterTagString sizeWithFont:myFont];
	
    
	//CGRect tagLabelRect = CGRectZero;
	CGRect tagImageRect = CGRectZero;
	
	if (UIInterfaceOrientationIsPortrait(theOrientation)) {
		[filterTagsLabel setFrame:kFilterTagsLabelPortrait];
		[filterTagsFooterLine setFrame:kFilterTagsFooterLinePortrait];
		[filterTagsClearButton setFrame:kFilterTagsClearButtonPortrait];
		//tagLabelRect = kFilterTagsLabelPortrait;
		tagImageRect = kFilterTagsImageViewPortrait;
	} else if (UIInterfaceOrientationIsLandscape(theOrientation)) {
		[filterTagsLabel setFrame:kFilterTagsLabelLandscape];
		[filterTagsFooterLine setFrame:kFilterTagsFooterLineLandscape];
		[filterTagsClearButton setFrame:kFilterTagsClearButtonLandscape];
		//tagLabelRect = kFilterTagsLabelLandscape;
		tagImageRect = kFilterTagsImageViewLandscape;
	}
	
	if (size.width > filterTagsLabel.frame.size.width) {
		size.width = filterTagsLabel.frame.size.width;
	}
	
	//CGFloat labelMiddlePoint = tagLabelRect.origin.x + (tagLabelRect.size.width / 2.0);
	tagImageRect = CGRectMake(tagImageRect.origin.x - (size.width / 2.0),
							  tagImageRect.origin.y, tagImageRect.size.width, tagImageRect.size.height);
	[filterTagsImageView setFrame:tagImageRect];
	
}

- (void)hideFilterTagObjects:(BOOL)isHidden {
	if (isHidden) {
		[filterTagsLabel setAlpha:0.0f];
		[filterTagsFooterLine setAlpha:0.0f];
		[filterTagsImageView setAlpha:0.0f];
		[filterTagsClearButton setAlpha:0.0f];
		[newListItemButton setEnabled:YES];
		[newScribbleListItemButton setEnabled:YES];
		[editListButton setEnabled:YES];
		[searchButton setEnabled:YES];
		
		if ([refTaskList completed]) {
			[listCompletedStampImage setHidden:NO];
		}
		
		[listCompletedButton setHidden:NO];
		
	} else {
		[filterTagsLabel setAlpha:1.0f];
		[filterTagsFooterLine setAlpha:1.0f];
		[filterTagsImageView setAlpha:1.0f];
		[filterTagsClearButton setAlpha:1.0f];
		[newListItemButton setEnabled:NO];
		[newScribbleListItemButton setEnabled:NO];
		[editListButton setEnabled:NO];
		[searchButton setEnabled:NO];
		
		// Also need to hide completed stamp
		[listCompletedStampImage setHidden:YES];
		
		// Also hide tick completed icon
		[listCompletedButton setHidden:YES];
	}
	
}


- (void)reloadTheme {
	NSString *canvasImageName = [refMasterTaskListCollection getImageNameForSection:@"Canvas"];
	UIImage *newCanvasImage = [UIImage imageNamed:canvasImageName];
	[canvassImage setImage:newCanvasImage];
	
	NSString *borderImageName = [refMasterTaskListCollection getImageNameForSection:@"Border"];
	UIImage *newBorderImage = [UIImage imageNamed:borderImageName];
	[borderImage setImage:newBorderImage];
	
	NSString *pocketImageName = [refMasterTaskListCollection getImageNameForSection:@"Pocket"];
	UIImage *newPocketImage = [UIImage imageNamed:pocketImageName];
	[itemsDuePocket setImage:newPocketImage];
	
	itemsDueTableViewResponder.textLabelColour = [refMasterTaskListCollection getColourForSection:@"ItemsDueTextLabel"];
	itemsDueTableViewResponder.detailTextLabelColour = [refMasterTaskListCollection getColourForSection:@"ItemsDueDetailTextLabel"];
	itemsDueTableViewResponder.listLabelColour = [refMasterTaskListCollection getColourForSection:@"ItemsDueListLabel"];
	
	[itemsDueTableViewResponder reloadTableView:itemsDueTableView];
}

-(BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    if (UIInterfaceOrientationIsPortrait(orientation))
        return NO;
    else
        return NO;
}


#pragma mark -
#pragma mark Split view support


- (void)splitViewController:(MGSplitViewController*)svc
	 willHideViewController:(UIViewController *)aViewController
		  withBarButtonItem:(UIBarButtonItem*)barButtonItem
	   forPopoverController: (UIPopoverController*)pc
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
	
//	if (barButtonItem) {
//		barButtonItem.title = @"Popover";
//		NSMutableArray *items = [[toolbar items] mutableCopy];
//		[items insertObject:barButtonItem atIndex:0];
//		[toolbar setItems:items animated:YES];
//	}
//    self.popoverController = pc;
}


// Called when the view is shown again in the split view, invalidating the button and popover controller.
- (void)splitViewController:(MGSplitViewController*)svc
	 willShowViewController:(UIViewController *)aViewController
  invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
	
//	if (barButtonItem) {
//		NSMutableArray *items = [[toolbar items] mutableCopy];
//		[items removeObject:barButtonItem];
//		[toolbar setItems:items animated:YES];
//	}
//    self.popoverController = nil;
}


- (void)splitViewController:(MGSplitViewController*)svc
		  popoverController:(UIPopoverController*)pc
  willPresentViewController:(UIViewController *)aViewController
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
}


- (void)splitViewController:(MGSplitViewController*)svc willChangeSplitOrientationToVertical:(BOOL)isVertical
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
}


- (void)splitViewController:(MGSplitViewController*)svc willMoveSplitToPosition:(float)position
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
}


- (float)splitViewController:(MGSplitViewController *)svc constrainSplitPosition:(float)proposedPosition splitViewSize:(CGSize)viewSize
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
	return proposedPosition;
}

@end
