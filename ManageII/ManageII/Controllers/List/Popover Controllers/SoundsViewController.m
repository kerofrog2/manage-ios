//
//  SoundsViewController.m
//  Manage
//
//  Created by Kerofrog on 7/5/13.
//
//

#import "SoundsViewController.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 370)
#define kPopoverSize		CGSizeMake(320, 318)

@interface SoundsViewController ()

@end

@implementation SoundsViewController

@synthesize delegate;
@synthesize currentSound;
@synthesize soundArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Memory Management

- (void)dealloc {
//    [myTableView release];
//    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithDelegate:(NSObject<SoundsViewControllerDelegate>*)theDelegate withSound:(Sound *)sound{
    if (self = [super init]) {
        // Set the title
		self.title = @"Sounds";
        self.delegate = theDelegate;
        self.currentSound = sound;
        [self setContentSizeForViewInPopover:kPopoverSize];
        
        [self loadDataSourceForTableView];
        
        [self loadTableView];
    }
    return self;
}


#pragma mark - Load Methods

- (void)loadDataSourceForTableView {
    
    self.soundArray = [Sound createDataSource];
}

- (void)loadTableView {
    myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark - UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.soundArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];//autorelease
	}
	
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    NSInteger row = indexPath.row;
    
    Sound *s  = nil;
    if (row < [self.soundArray count]) {
        s = [self.soundArray objectAtIndex:row];
    }
    cell.textLabel.text = s.textSound;
    
    if (s.textSound == currentSound.textSound) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger row = indexPath.row;
    UITableViewCell *cell = [myTableView cellForRowAtIndexPath:indexPath];
    Sound *s = [self.soundArray objectAtIndex:row];
    s.isSelected = YES;
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    
    // Update current sound
    [self.delegate updatedCurrentWithSound:s];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

@end
