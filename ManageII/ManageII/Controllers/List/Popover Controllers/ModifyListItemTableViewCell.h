//
//  PopOverTableViewCell.h
//  Manage
//
//  Created by Cliff Viegas on 6/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TagCollection;
@class ListItemTagCollection;

// Data Models
@class ListItem;

// GL Drawing
#import "GLDrawES2ViewController.h"


@interface ModifyListItemTableViewCell : UITableViewCell {
	UIButton		*detailButton;
	UILabel			*customTextLabel;
	UILabel			*notesTextLabel;
	
	// Tags View
	UIView			*tagsView;
	UIFont			*tagsFont;
	
	//CGFloat			tagsRowHeight;
    //GLDrawES2ViewController     *drawingLayer;
    UIImageView                 *scribbleImageView;
    ListItem                    *refListItem;
}

@property (nonatomic, retain)		UIButton		*detailButton;
@property (nonatomic, retain)		UILabel			*customTextLabel;
@property (nonatomic, retain)		UILabel			*notesTextLabel;

@property (nonatomic, retain)		UIView			*tagsView;
@property (nonatomic, retain)		UIFont			*tagsFont;
//@property (nonatomic, assign)		CGFloat			tagsRowHeight;

//@property (nonatomic, retain)   GLDrawES2ViewController     *drawingLayer;
@property (nonatomic, retain)   UIImageView                 *scribbleImageView;

#pragma mark Class Methods
- (void)loadScribbleImageForListItem:(ListItem *)listItem;
- (void)loadDrawingLayerForListItem:(ListItem *)listItem;
- (void)clearTagsView;
- (void)loadTagsFrom:(ListItemTagCollection *)listItemTagCollection andTagCollection:(TagCollection *)tagCollection;

@end
