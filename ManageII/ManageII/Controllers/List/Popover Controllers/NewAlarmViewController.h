//
//  NewAlarmViewController.h
//  Manage
//
//  Created by Cliff Viegas on 18/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class Alarm;
@class ListItem;

// Data Collections
@class AlarmCollection;

@interface NewAlarmViewController : GAITrackedViewController {
	UIDatePicker			*alarmPicker;
//	UIPopoverController		*refPopoverController;	// Reference to the popover controller
	NSString				*existingAlarmGUID;		// This is a 'copy' property, so ok to pass it
	AlarmCollection			*refAlarmCollection;	// Reference to alarm collection
	ListItem				*refCurrentListItem;	// Reference to the current list item
}

@property	(nonatomic, assign)	UIPopoverController		*refPopoverController;
@property	(nonatomic, copy)   NSString				*existingAlarmGUID;

#pragma mark Initialisation
- (id)initWithCurrentListItem:(ListItem *)theCurrentListItem andAlarmCollection:(AlarmCollection *)theAlarmCollection;
- (id)initWithExistingAlarmGUID:(NSString *)alarmGUID andCurrentListItem:(ListItem *)theCurrentListItem
			 andAlarmCollection:(AlarmCollection *)theAlarmCollection;
@end
