//
//  EditListItemDueViewController.m
//  Manage
//
//  Created by Cliff Viegas on 15/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "EditListItemDueViewController.h"

// Data Models
#import "ListItem.h"

@implementation EditListItemDueViewController

#pragma mark -
#pragma mark Button Actions

// DEPRECATED
/*- (void)doneBarButtonItemAction {
	BOOL updated = TRUE;
	
	//	if (originalDueDate == [NSNull null]) {
	//		originalDueDate = @"";
	//	}
	//	
	//	if (refListItem.dueDate == [NSNull null]) {
	//		refListItem.dueDate = @"";
	//	}
	
	if ([listItemTitle isEqualToString:originalListItemTitle] &&
		[listItemNotes isEqualToString:originalListItemNotes] &&
		[originalDueDate isEqualToString:refListItem.dueDate] &&
		originalCompleted == boxIsTicked) {
		updated = FALSE;
	}
	
	// Update the db
	if (updated) {
		//listItemTitle = [listItemTitle stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
		refListItem.title = listItemTitle;
		refListItem.notes = listItemNotes;
		[refListItem updateDatabase];
	}
	
	// Need to update the collection for the list item before copying
	//[refListItem.listItemTagCollection updateTagCollectionWithListItemID:<#(NSInteger)theListItemID#>
	
	
	// Pass a new list item
	ListItem *passedListItem = [[ListItem alloc] initWithListItemCopy:refListItem];
	
	// Notify the delegate on whether the table view needs to be refreshed or not
	[delegate listItemDue:passedListItem wasUpdated:updated];
	[passedListItem release];
}*/

#pragma mark -
#pragma mark Over-ride methods

- (void)viewWillDisappear:(BOOL)animated {
	[self.delegate editListItemAtBaseLevel:FALSE];

        
    if ((editListItemWillExit == TRUE && wasListItemMoved == FALSE && needDBUpdate == TRUE)
            || repeatingTaskWasUpdated == TRUE) {
		refListItem.title = listItemTitle;
		refListItem.notes = listItemNotes;
		[refListItem updateDatabase];
		
		ListItem *passedListItem = [[ListItem alloc] initWithListItemCopy:refListItem];
		
		// Notify the delegate on whether the table view needs to be refreshed or not
        BOOL completedChanged = FALSE;
        if ([refListItem completed] != originalCompleted) {
            completedChanged = TRUE;
        }
        
		[self.delegate listItemDue:passedListItem wasUpdated:YES completedStatusChanged:completedChanged];
//		[passedListItem release];
	}
	
	// Update database
	//[refListItem updateDatabase]
	
	// Update database
	//[refListItem updateDatabase];
	
	// Pass a new list item
	
}

// Over-ridden to prevent text view becoming first responder
- (void)viewDidAppear:(BOOL)animated {
	if (didAppearFromListView == TRUE) {
		didAppearFromListView = FALSE;
	} else if (didAppearFromListView == FALSE) {
		if (refPopoverController) {
		//	[refPopoverController setPopoverContentSize:kPopoverSize animated:YES];
		}
	}
	
	editListItemWillExit = TRUE;
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}



@end
