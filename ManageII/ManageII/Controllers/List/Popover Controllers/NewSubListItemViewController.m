    //
//  NewSubListItemViewController.m
//  Manage
//
//  Created by Cliff Viegas on 13/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "NewSubListItemViewController.h"

// List Constants
#import "ListConstants.h"

// Data Collections
#import "ListItemTagCollection.h"
#import "TagCollection.h"

// Data Models
#import "ListItem.h"
#import "GAIConstants.h"

// Constants
#define kPopoverSize				CGSizeMake(320, 318)

@implementation NewSubListItemViewController

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(NSObject<NewListItemDelegate>*)theDelegate andListItem:(ListItem *)theListItem andTagCollection:(TagCollection *)theTagCollection {
	if (self = [super init]) {
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign existing list item
		refListItem = theListItem;
		
		// Init box is ticked to false
		boxIsTicked = FALSE;
		
		// Set current due date
		currentDueDate = @"";

		// Init the list item tag collection
		listItemTagCollection = [[ListItemTagCollection alloc] init];
		
		refTagCollection = theTagCollection;
		
		// Set the title
		self.title = @"New Subtask";
		
		// Set did appear from edit list view as true
		didAppearFromEditListView = TRUE;
		
		// Load the left and right bar buttons
		[self loadButtons];
		
		// Load the table view
		[self loadTableView];
	}
	return self;
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	// Create a new list item and add it to the list item collection (task list)
	// Will need a delegate for this! bah
	// Need to create a createnewsublistitem delegate method
	//[delegate createNewListItemWithTitle:listItemTitle andNotes:listItemNotes andCompleted:boxIsTicked];
	//[delegate createNewSubListItemWithTitle:@"test" andNotes:@"notes" andCompleted:YES andParentListItem:refListItem];
	[self.delegate createNewSubListItemWithTitle:listItemTitle andNotes:listItemNotes
							   andCompleted:boxIsTicked andDueDate:currentDueDate andDueTime:currentDueTime
                               andAlertTime:self.alertTime.timeAlert
                                   andSound:self.currentSound.textSound
                                  andSnooze:self.currrentSnooze
						  andParentListItem:refListItem
				   andListItemTagCollection:listItemTagCollection];
    
#if ENABLE_GOOGLE_ANALYTICS
    // track this event to Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker sendEventWithCategory:kCategory_event
                        withAction:kAction_new_task
                         withLabel:[NSString stringWithFormat:@"%@ %@", kLabel_new_sub_task_with_name, listItemTitle]
                         withValue:0];
#endif
    
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
//        // AskingPoint event
//        [APManager addEventWithName:kAskingPointEventNewSubTask andData:@{kAskingPointEventDataKeyType : kAskingPointEventDataTyping}];
//    }
//    
}

// Over-ride super cancel bar button
- (void)cancelBarButtonItemAction {
	// Do nothing
	[self.navigationController popViewControllerAnimated:YES];
	//[delegate dismissNewListItemPopOver];
}

#pragma mark -
#pragma mark UIViewController Delegates

// Over-riding these methods so they do nothing
//- (void)viewWillAppear:(BOOL)animated {}
//- (void)viewWillDisappear:(BOOL)animated {}


- (void)viewDidAppear:(BOOL)animated {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	[self setContentSizeForViewInPopover:kNewSubListItemViewControllerSize];
	[UIView commitAnimations];
	
	if (didAppearFromEditListView == TRUE) {
		UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
		UITextView *titleTextView = (UITextView *)[cell.contentView viewWithTag:kTextViewTag];
		[titleTextView becomeFirstResponder];
		
		didAppearFromEditListView = FALSE;
	}
	
	
	if (popoverController) {
		[popoverController setPopoverContentSize:kPopoverSize animated:YES];
	}
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}


@end
