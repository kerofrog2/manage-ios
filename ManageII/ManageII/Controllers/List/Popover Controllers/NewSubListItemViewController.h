//
//  NewSubListItemViewController.h
//  Manage
//
//  Created by Cliff Viegas on 13/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Subclass new list item view controller
#import "NewListItemViewController.h"

// Data Models
@class ListItem;

@interface NewSubListItemViewController : NewListItemViewController {
	ListItem	*refListItem;
	
	BOOL		didAppearFromEditListView;
}

#pragma mark Initialisation
- (id)initWithDelegate:(NSObject<NewListItemDelegate>*)theDelegate andListItem:(ListItem *)theListItem andTagCollection:(TagCollection *)theTagCollection;
	
@end
