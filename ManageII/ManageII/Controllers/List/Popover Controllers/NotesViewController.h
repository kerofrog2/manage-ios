//
//  NotesViewController.h
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NotesDelegate
- (void)taskNotesUpdated:(NSString *)updatedNotes;
@end


@interface NotesViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
//	id <NotesDelegate> delegate;
	
	NSString *notes;
	
	UITableView *myTableView;
}

@property (nonatomic, copy)			NSString	*notes;
//@property (nonatomic, assign)		id			delegate;
@property (nonatomic, assign) NSObject<NotesDelegate>    *delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(NSObject<NotesDelegate>*)theDelegate andNotes:(NSString *)taskNotes;

#pragma mark Load Methods
- (void)loadTableView;
- (void)loadButtons;

#pragma mark Helper Methods
- (UITextView *)getTextView; 

@end
