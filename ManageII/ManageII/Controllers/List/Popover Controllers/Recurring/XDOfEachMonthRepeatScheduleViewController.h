//
//  CustomOfEachMonthRepeatScheduleViewController.h
//  Manage
//
//  Created by Cliff Viegas on 22/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class RecurringListItem;


@interface XDOfEachMonthRepeatScheduleViewController : GAITrackedViewController <UIPickerViewDelegate, UIPickerViewDataSource> {
	RecurringListItem	*refRecurringListItem; // Reference to recurring list item
	
	NSArray				*daysArray;
	NSArray				*measurementArray;
	UILabel				*pickerLabel;
	NSInteger			initialFrequency;
	NSString			*initialDayOfTheWeek;
	NSString			*initialFrequencyMeasurement;
}

#pragma mark Initialisation
- (id)initWithRecurringListItem:(RecurringListItem *)theRecurringListItem;

@end
