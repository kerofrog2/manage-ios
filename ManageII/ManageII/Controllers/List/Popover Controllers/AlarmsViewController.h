//
//  AlarmsViewController.h
//  Manage
//
//  Created by Cliff Viegas on 16/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class AlarmCollection;

// Data Models
@class ListItem;

@protocol AlarmsViewControllerDelegate

- (void)didUpdateWithAlarmsListItemID:(ListItem *)item;

@end

@interface AlarmsViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
	UITableView				*myTableView;				// Tableview for displaying content
	AlarmCollection			*refAlarmCollection;		// Passed alarm collection
//	UIPopoverController		*refPopoverController;		// Reference to popover controller
	ListItem				*refCurrentListItem;		// Reference to current list item
//    id <AlarmsViewControllerDelegate>	delegate;
}

//@property (nonatomic, assign) id					delegate;
@property	(nonatomic, assign)		UIPopoverController		*refPopoverController;
@property	(nonatomic, assign)		NSInteger				currentListItemID;
@property (nonatomic, assign) NSObject<AlarmsViewControllerDelegate>	*delegate;

#pragma mark Initialisation
- (id)initWithAlarmCollection:(AlarmCollection *)theAlarmCollection andCurrentListItem:(ListItem *)theCurrentListItem;

@end
