//
//  SoundsViewController.h
//  Manage
//
//  Created by Kerofrog on 7/5/13.
//
//

#import <UIKit/UIKit.h>
#import "Sound.h"

@protocol SoundsViewControllerDelegate
- (void)updatedCurrentWithSound:(Sound *)newSound;
@end

@interface SoundsViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
//    id <SoundsViewControllerDelegate>       delegate;
    Sound   *currentSound;
    
    UITableView                 *myTableView;
    NSMutableArray              *timeStringArray;
}

//@property (nonatomic, assign)		id			delegate;
@property (nonatomic, retain)		NSMutableArray    *soundArray;
@property (nonatomic, retain)       Sound   *currentSound;
@property (nonatomic, assign) NSObject<SoundsViewControllerDelegate>			*delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(NSObject<SoundsViewControllerDelegate>*)theDelegate withSound:(Sound *)sound;

#pragma mark Load Methods
- (void)loadTableView;

@end