//
//  RepeatStartDateTableViewController.h
//  Manage
//
//  Created by Cliff Viegas on 10/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Master class
#import "DueDateTableViewController.h"

// Data Modles
@class RecurringListItem;

@protocol RepeatStartDateDelegate
- (void)didSelectNewRepeatStartDate;
@end


@interface RepeatStartDateTableViewController : DueDateTableViewController <UIAlertViewDelegate> {
//	id <RepeatStartDateDelegate>	delegate;
	RecurringListItem				*refRecurringListItem;
	
	// Reference to the popover controller
//	UIPopoverController				*refPopoverController;
	NSDate							*warningSavedDate;
}

//@property (nonatomic, assign) id					delegate;
@property (nonatomic, assign) NSObject<RepeatStartDateDelegate>         *delegate;
@property (nonatomic, assign) UIPopoverController	*refPopoverController;

#pragma mark Initialisation
- (id)initWithRecurringListItem:(RecurringListItem *)theRecurringListItem; 

@end
