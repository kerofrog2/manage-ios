//
//  NewDueDateTableViewController.m
//  Manage
//
//  Created by Cliff Viegas on 22/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "NewDueDateTableViewController.h"

// Method Helper
#import "MethodHelper.h"

@implementation NewDueDateTableViewController

@synthesize delegate;
@synthesize dueDate;
@synthesize refPopoverController;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[dueDate release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDueDate:(NSString *)theDate andDelegate:(NSObject<NewListItemDueDateDelegate>*)theDelegate {
	if ((self = [super init])) {
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the date to due date
		self.dueDate = theDate;
		
		// Set title
		self.title = @"Due Date";
		
		// Create clear bar button item
		UIBarButtonItem *clearBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear" 
																			   style:UIBarButtonItemStyleBordered 
																			  target:self 
																			  action:@selector(clearBarButtonItemAction)];
		[self.navigationItem setRightBarButtonItem:clearBarButtonItem animated:NO];
//		[clearBarButtonItem release];
	}
	return self;
}

- (id)initWithListDate:(NSString *)theDate andDelegate:(NSObject<NewListItemDueDateDelegate>*)theDelegate {
	if ((self = [super init])) {
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the date to due date
		self.dueDate = theDate;
		
		// Set title
		self.title = @"Date";
	}
	return self;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	//	[self.monthView reload];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
    [refPopoverController setPopoverContentSize:self.monthView.frame.size animated:YES];
	[self setContentSizeForViewInPopover:CGSizeMake(self.monthView.frame.size.width, 
													self.monthView.frame.size.height)];
	[UIView commitAnimations];
	
	[self.monthView reload];
	
	if (self.dueDate != nil && [self.dueDate length] > 0) {
        NSDate *theDate = [MethodHelper dateGMTFromString:self.dueDate usingFormat:K_DATEONLY_FORMAT];
		[self.monthView selectDate:theDate];
	}
	
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	[self setContentSizeForViewInPopover:CGSizeMake(self.monthView.frame.size.width, 
													self.monthView.frame.size.height)];
	
	if (self.dueDate != nil && [self.dueDate length] > 0) {
        NSDate *theDate = [MethodHelper dateGMTFromString:self.dueDate usingFormat:K_DATEONLY_FORMAT];
		[self.monthView selectDate:theDate];
	}	
}

#pragma mark -
#pragma mark Tapku Calendar Delegates

- (void)calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)d {
	// Update the date
	self.dueDate = [MethodHelper stringGMTFromDate:d usingFormat:K_DATEONLY_FORMAT];
    
	[self.delegate updateDueDate:self.dueDate];
	
	[self performSelector:@selector(popViewDelay) withObject:nil afterDelay:0.2f];
}

- (void) popViewDelay{
    
}

#pragma mark -
#pragma mark Button Events

- (void)clearBarButtonItemAction {
	// Clear date field
	self.dueDate = @"";
	[self.delegate updateDueDate:self.dueDate];
	
	[self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}


@end

