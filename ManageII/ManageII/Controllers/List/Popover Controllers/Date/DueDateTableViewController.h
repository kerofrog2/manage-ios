//
//  DueDateTableViewController.h
//  Manage
//
//  Created by Cliff Viegas on 12/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Tapku  (for calendar item)
#import "TapkuLibrary.h"

// Data Models
@class ListItem;

@protocol DueDatePickerDelegate <NSObject>

@optional
- (void) didUpdateDueDate;

@end
@interface DueDateTableViewController : TKCalendarMonthTableViewController {
	ListItem *refListItem;
}

@property (weak, nonatomic) id<DueDatePickerDelegate> delegate;

#pragma mark Initialisation
- (id)initWithListItem:(ListItem *)theListItem;

@end
