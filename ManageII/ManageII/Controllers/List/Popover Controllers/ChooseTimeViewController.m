//
//  ChooseTimeViewController.m
//  Manage
//
//  Created by Cliff Viegas on 6/6/13.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ChooseTimeViewController.h"
#import "MethodHelper.h"
#import "AlertTimeViewController.h"
#import "SoundsViewController.h"

#define kViewControllerSize					CGSizeMake(320, 210)
#define	kPickerSize                         CGSizeMake(320, 210)
#define kTableViewFrame						CGRectMake(0, 0, 320, 0)
#define kSwitchTag			100000

@implementation ChooseTimeViewController

@synthesize refPopoverController;
@synthesize delegate;
@synthesize sound;
@synthesize alertTime;
@synthesize snooze;

#pragma mark -
#pragma mark Memory Management

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
	if (refPopoverController) {
		[refPopoverController setPopoverContentSize:kViewControllerSize animated:YES];
	}
}

- (void)dealloc {
//	[datePicker release];
//    [alertTime release];
//    [sound release];
//	[super dealloc];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithCurrentTime:(NSDate *)currentTime {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_ChooseTimeViewController;
#endif
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Set the title
		self.title = @"Reminders";
		
        // set Time value for date picker
        if (currentTime == nil) {
            currentDate = [NSDate date];
        } else {
            currentDate = currentTime;
        }
		
		// Load the table view
		[self loadTableView];
        
        // Load time picker view
        [self loadDatePicker];
		
		// Load bar button items
		[self loadBarButtonItems];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadBarButtonItems {
	// Cancel button if it is new alarm, otherwise display a clear button
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
										  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
										  target:self
										  action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
//	[doneBarButtonItem release];
	
    UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                            target:self
                                            action:@selector(cancelBarButtonItemAction)];
    [self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
//    [cancelBarButtonItem release];
}

- (void)loadDatePicker {
    
	datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
	[datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.date = currentDate;
    datePicker.minimumDate = [NSDate date];
    
	[self.view addSubview:datePicker];
}

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];

	[myTableView setDataSource:self];
	[myTableView setDelegate:self];
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark UIpicker Delegate

- (void)datePickerValueChanged:(id) sender{
    
    // Set date of date picker is today if date of picker is past date
    datePicker.minimumDate = [NSDate date];
    if ([datePicker.date compare:[NSDate date]] != NSOrderedDescending) {
        datePicker.date = [NSDate date];
    }
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
    
    // update current date
    currentDate = datePicker.date;
    
    // update data for NewListItemViewController
    [delegate updatedCurrentTimeString:[MethodHelper stringFromDate:currentDate usingFormat:K_DATETIME_FORMAT] repeat:self.alertTime sound:self.sound andSnooze:mySwitch.isOn];
    
	[self.navigationController popViewControllerAnimated:YES];
}

// Do nothing
- (void)cancelBarButtonItemAction {
    
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UISwitch Button

- (UISwitch *)getSwitch {
	UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];//autorelease
	switchView.tag = kSwitchTag;
	// We dont want user interaction, clicking on table view cell will change this value
	[switchView setFrame:CGRectMake(210, 8, 0, 0)];
	switchView.userInteractionEnabled = FALSE;
	[switchView setHidden:YES];
	return switchView;
}

#pragma mark -
#pragma mark TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];//autorelease
        
        [cell.contentView addSubview:[self getSwitch]];
	}
    
    switch (indexPath.row) {
        case 0:// Repeat record
        {
            cell.textLabel.text = @"Repeat";
            if (alertTime == nil) {
                cell.detailTextLabel.text = @"None";
            }else{
                cell.detailTextLabel.text = alertTime.text;
            }
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
            break;
//        case 1:// Sound record
//        {
//            cell.textLabel.text = @"Sound";
//            if (self.sound == nil) {
//                cell.detailTextLabel.text = @"None";
//            }else{
//                cell.detailTextLabel.text = self.sound.textSound;
//            }
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        }
            break;
        case 1:// Snooze record
        {
            cell.textLabel.text = @"Snooze";
            // Get relevant data
            mySwitch = (UISwitch *)[cell.contentView viewWithTag:kSwitchTag];
            [mySwitch setHidden:NO];
            [mySwitch setOn:self.snooze];
        }
            break;
            
        default:
            break;
    }
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
        {
            // Push Alert Time View Controller
            AlertTimeViewController *controller = [[AlertTimeViewController alloc] initWithDelegate:self withNumberOfMinuteForAlert:nil];//autorelease
            controller.delegate = self;
            controller.currentAlertTime = self.alertTime;
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
//        case 1:
//        {
//            // Push Sound View Controller
//            SoundsViewController *controller = [[[SoundsViewController alloc] initWithDelegate:self withSound:nil]autorelease];
//            controller.delegate = self;
//            controller.currentSound = self.sound;
//            [self.navigationController pushViewController:controller animated:YES];
//        }
//            break;
            
        default:
            break;
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    // Get switch object
	mySwitch = (UISwitch *)[cell.contentView viewWithTag:kSwitchTag];
    
    if (indexPath.row == 1) {
        BOOL switchStatus;
        // Change switch value
        if ([mySwitch isOn]) {
            [mySwitch setOn:NO animated:YES];
            switchStatus = NO;
        } else {
            [mySwitch setOn:YES animated:YES];
            switchStatus = YES;
        }
        
        return nil;
    }
    
    return indexPath;
}

#pragma mark -
#pragma mark AlertTimeViewControllerDelegate
- (void)updatedCurrentNumberOfMinuteForAlert:(AlertTime *)newAlertTime {
    alertTime = newAlertTime;
    [myTableView reloadData];
}

#pragma mark -
#pragma mark AlertTimeViewControllerDelegate
- (void)updatedCurrentWithSound:(Sound *)newSound {
    self.sound = newSound;
    [myTableView reloadData];
}


@end
