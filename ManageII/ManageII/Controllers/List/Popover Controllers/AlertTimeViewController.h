//
//  AlertTimeViewController.h
//  Manage
//
//  Created by Cliff Viegas on 6/6/13.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertTime.h"

@protocol AlertTimeViewControllerDelegate
- (void)updatedCurrentNumberOfMinuteForAlert:(AlertTime *)newAlertTime;
@end

@interface AlertTimeViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
//    id <AlertTimeViewControllerDelegate>       delegate;
    AlertTime   *currentAlertTime;
    
    UITableView                 *myTableView;
    NSMutableArray              *timeStringArray;
}

//@property (nonatomic, assign)		id			delegate;
@property (nonatomic, retain)		NSMutableArray    *timeStringArray;
@property (nonatomic, retain)       AlertTime   *currentAlertTime;
@property (nonatomic, assign)		NSObject<AlertTimeViewControllerDelegate>			*delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(NSObject<AlertTimeViewControllerDelegate>*)theDelegate withNumberOfMinuteForAlert:(AlertTime *)currentNumberOfMinuteForAlert;

#pragma mark Load Methods
- (void)loadTableView;

@end
