//
//  FastTableViewCell.m
//  Manage
//
//  Created by Cliff Viegas on 17/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "FastTableViewCell.h"

@interface FastTableViewCellView : UIView
@property (nonatomic, assign) FastTableViewCell *superCell;
@end

@implementation FastTableViewCellView

- (void)drawRect:(CGRect)r
{
//    if (_superCell.retainCount > 0) {
//        
//    }
    [_superCell drawContentView:r];
}

@end

@implementation FastTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
		contentView = [[FastTableViewCellView alloc] init];
		contentView.opaque = YES;
        contentView.superCell = self;
		[self.contentView addSubview:contentView];

//		[contentView release];
    }
    return self;
}


- (void)dealloc
{
//	[super dealloc];
}

- (void)setFrame:(CGRect)f
{
	[super setFrame:f];
	CGRect b = [self bounds];
	b.size.height -= 1; // leave room for the seperator line
	[contentView setFrame:b];
}

- (void)setNeedsDisplay
{
	[super setNeedsDisplay];
    if (contentView != nil) {
        [contentView setNeedsDisplay];
    }
}

- (void)drawContentView:(CGRect)r
{
	// subclasses should implement this
}

@end
