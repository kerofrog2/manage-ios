//
//  MiniCalendarView.m
//  Manage
//
//  Created by Cliff Viegas on 7/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "MiniCalendarView.h"

// Kal
#import "KalViewController.h"

// Data Models
#import "TaskList.h"
#import "ListItem.h"

// Data Collections
#import "TaskListCollection.h"

// Method Helper
#import "MethodHelper.h"

// Macro methods
static BOOL IsDateBetweenInclusive(NSDate *date, NSDate *begin, NSDate *end) {
    return [date compare:begin] != NSOrderedAscending && [date compare:end] != NSOrderedDescending;
}

// Private Methods
@interface MiniCalendarView()
- (NSArray *)getTaskDatesFrom:(NSDate *)fromDate to:(NSDate *)toDate;
- (BOOL)foundDate:(NSDate *)theDate;
@end

@implementation MiniCalendarView

@synthesize selectionDelegate;
@synthesize didChangeMonth;

#pragma mark - Memory Management

- (void)dealloc {
//    [kalCalendar release];
//    [calendarItems release];
//    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithFrame:(CGRect)frame andTaskListCollection:(TaskListCollection *)theTaskListCollection {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        // Assign reference to task list collection
        refTaskListCollection = theTaskListCollection;
        
        // Load the calendar
        kalCalendar = [[KalViewController alloc] initWithFrame:frame];
        kalCalendar.title = @"Calendar";
        
        // Init did change month to false
        self.didChangeMonth = FALSE;
        
        // Init our calendar items
        calendarItems = [[NSMutableArray alloc] init];
        
        // Need to setup datasource
        kalCalendar.dataSource = self;

        // Setup the navigation stack to display the calendar
        [self addSubview:kalCalendar.view];
        
    }
    return self;
}

#pragma mark - Get Methods

// Get the list of dates to return as an array

- (NSArray *)getTaskDatesFrom:(NSDate *)fromDate to:(NSDate *)toDate {
    NSMutableArray *matches = [NSMutableArray array];
    
    for (TaskList *taskList in refTaskListCollection.lists) {
        for (ListItem *listItem in taskList.listItems) {
            if ([[listItem dueDate] length] == 0) {
                continue;
            }
            NSDate *taskDate = [MethodHelper dateFromString:[listItem dueDate] usingFormat:K_DATEONLY_FORMAT];
            if (IsDateBetweenInclusive(taskDate, fromDate, toDate)) {
                [matches addObject:taskDate];
            }
        }
    }
    return  matches;
}

- (BOOL)foundDate:(NSDate *)theDate {
    for (TaskList *taskList in refTaskListCollection.lists) {
        for (ListItem *listItem in taskList.listItems) {
            if ([[listItem dueDate] length] == 0) {
                continue;
            }
            NSDate *taskDate = [MethodHelper dateFromString:[listItem dueDate] usingFormat:K_DATEONLY_FORMAT];
            if (IsDateBetweenInclusive(taskDate, theDate, theDate)) {
                return TRUE;
            }
        }
    }
    return FALSE;
}

#pragma mark - Reload Methods

- (void)reloadData {
    [kalCalendar reloadData];
}

#pragma mark - KalDataSource protocol conformance

- (void)presentingDatesFrom:(NSDate *)fromDate to:(NSDate *)toDate delegate:(id<KalDataSourceCallbacks>)delegate {
    self.didChangeMonth = TRUE;
    callback = delegate;
    [callback loadedDataSource:self];
}

- (NSArray *)markedDatesFrom:(NSDate *)fromDate to:(NSDate *)toDate {
    return [self getTaskDatesFrom:fromDate to:toDate];
}

- (void)loadItemsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate monthChanged:(BOOL)changed {
    if (changed == TRUE) {
        return;
    }
    
    // This is where we do a callback on the list view controller
    if ([self didChangeMonth] == TRUE) {
        self.didChangeMonth = FALSE;
        return;
    }
    
    if ([self foundDate:fromDate]) {
        [self.selectionDelegate didSelectDate:fromDate];
    }
    
    //[calendarItems addObjectsFromArray:[self getTaskDatesFrom:fromDate to:toDate]];

}

- (void)removeAllItems {
    [calendarItems removeAllObjects];
}

#pragma mark - UITableViewDataSource protocol conformance

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];// autorelease];
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

@end
