//
//  DateRangeTableViewController.h
//  Manage
//
//  Created by Cliff Viegas on 8/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Master class
#import "DueDateTableViewController.h"

@protocol SelectDateRangeDelegate
- (void)updateStartDate:(NSString *)newStartDate;
- (void)updateEndDate:(NSString *)newEndDate;
@end

@interface DateRangeTableViewController : DueDateTableViewController {
//	id <SelectDateRangeDelegate> delegate;
	
	NSString                    *selectedDate;
    NSInteger                   selectedDateType;
}

//@property (nonatomic, assign)		id				delegate;
@property (nonatomic, copy)			NSString		*selectedDate;
@property (nonatomic, assign)       NSInteger       selectedDateType;
@property (nonatomic, assign)		NSObject<SelectDateRangeDelegate>				*delegate;

#pragma mark Initialisation
- (id)initWithStartDate:(NSString *)theStartDate andDelegate:(NSObject<SelectDateRangeDelegate>*)theDelegate;
- (id)initWithEndDate:(NSString *)theEndDate andDelegate:(NSObject<SelectDateRangeDelegate>*)theDelegate;
@end