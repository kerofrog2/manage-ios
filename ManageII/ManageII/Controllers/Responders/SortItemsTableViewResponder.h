//
//  SortItemsTableViewDelegate.h
//  Manage
//
//  Created by Cliff Viegas on 4/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

@protocol SortItemsTableViewDelegate
// No need, we have the link to the list item, this just needs to update table
- (void)updateSelectedSortType:(NSInteger)newSortType;
@end

@interface SortItemsTableViewResponder : NSObject <UITableViewDelegate, UITableViewDataSource> {
//	id <SortItemsTableViewDelegate> delegate;
	
	NSArray			*sortItemsArray;
	NSInteger		selectedSortType;
}

//@property (nonatomic, assign)		id		delegate;
@property (nonatomic)		NSObject<SortItemsTableViewDelegate>		*delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(NSObject<SortItemsTableViewDelegate>*)theDelegate;

#pragma mark External Class Helper Methods
- (void)setSelectedSortType:(NSInteger)newSortType forTableView:(UITableView *)theTableView;

@end
