//
//  SortItemsTableViewDelegate.m
//  Manage
//
//  Created by Cliff Viegas on 4/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "SortItemsTableViewResponder.h"

// Data Models (for sort enums)
#import "TaskList.h"

@interface SortItemsTableViewResponder()
- (UIImage *)scaleImage:(UIImage *)image maxWidth:(float)maxWidth maxHeight:(float)maxHeight;

@end


@implementation SortItemsTableViewResponder

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[sortItemsArray release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(NSObject<SortItemsTableViewDelegate>*)theDelegate {
	if (self = [super init]) {
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Init the sort items array
		sortItemsArray = [[NSArray alloc] initWithObjects:@"List", @"Priority", @"Date", @"Completed", nil];
		
		// Init sort type to list
		selectedSortType = EnumSortByList;
	}
	return self;
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];// autorelease];
	}
	
	// Set up the cell...
	cell.textLabel.text = [sortItemsArray objectAtIndex:indexPath.row];
	cell.backgroundColor = [UIColor colorWithRed:0.24313 green:0.22352 blue:0.22352 alpha:1.0];
	cell.textLabel.textColor = [UIColor lightTextColor];

	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	//[cell.selectedBackgroundView setBackgroundColor:[UIColor greenColor]];

	cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
	cell.textLabel.highlightedTextColor = [UIColor darkTextColor];
	
	if (indexPath.row == selectedSortType) {
		//[cell setSelected:YES];
		[tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
	} else {
		//[cell setSelected:NO];
	}

	
	// 62 57 57
	switch (indexPath.row) {
		case 0:
			//cell.imageView.image = [self scaleImage:[UIImage imageNamed:@"EnteredIcon.png"];
			cell.imageView.image = [UIImage imageNamed:@"EnteredIcon.png"];
			//cell.imageView.image = [self scaleImage:[UIImage imageNamed:@"EnteredIcon.png"] 
			//												   maxWidth:20 maxHeight:20];
			break;
		case 1:
			cell.imageView.image = [UIImage imageNamed:@"PriorityIcon.png"];
			//cell.imageView.image = [self scaleImage:[UIImage imageNamed:@"PriorityIcon.png" ]
			//												   maxWidth:20 maxHeight:20];
			break;
		case 2:
			cell.imageView.image = [UIImage imageNamed:@"DateIcon.png"];
		//	cell.imageView.image = [self scaleImage:[UIImage imageNamed:@"DateIcon.png" ]
		//								   maxWidth:20 maxHeight:20];
			break;
		case 3:
			cell.imageView.image = [UIImage imageNamed:@"BoxTicked.png"];
		//	cell.imageView.image = [self scaleImage:[UIImage imageNamed:@"CompletedIcon.png" ]
		//								   maxWidth:20 maxHeight:20];
			break;
		default:
			break;
	}
	
	cell.imageView.frame = CGRectMake(cell.imageView.frame.origin.x, 
									  cell.imageView.frame.origin.y, 
									  10, 10);
	
    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return 4;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	selectedSortType = indexPath.row;

	[self.delegate updateSelectedSortType:selectedSortType];
}

#pragma mark -
#pragma mark Helper Methods

- (void)setSelectedSortType:(NSInteger)newSortType forTableView:(UITableView *)theTableView {
	selectedSortType = newSortType;
	
	[theTableView reloadData];
}

// Scale image
- (UIImage *)scaleImage:(UIImage *)image maxWidth:(float)maxWidth maxHeight:(float)maxHeight {
	CGImageRef imgRef = image.CGImage;
	
	CGFloat width = CGImageGetWidth(imgRef);
	CGFloat height = CGImageGetHeight(imgRef);
	
	if (width <= maxWidth && height <= maxHeight)
	{
		return image;
	}
	
	CGAffineTransform transform = CGAffineTransformIdentity;
	CGRect bounds = CGRectMake(0, 0, width, height);
	if (width > maxWidth || height > maxHeight)
	{
		CGFloat ratio = width/height;
		if (ratio > 1)
		{
			bounds.size.width = maxWidth;
			bounds.size.height = bounds.size.width / ratio;
		}
		else
		{
			bounds.size.height = maxHeight;
			bounds.size.width = bounds.size.height * ratio;
		}
	}
	
	CGFloat scaleRatio = bounds.size.width / width;
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextScaleCTM(context, scaleRatio, -scaleRatio);
	CGContextTranslateCTM(context, 0, -height);
	
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return imageCopy;
}


@end
