//
//  ToodledoResultsTableViewResponder.h
//  Manage
//
//  Created by Cliff Viegas on 14/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ToodledoResultsTableViewResponder : NSObject <UITableViewDataSource, UITableViewDelegate> {
	
}

@end
