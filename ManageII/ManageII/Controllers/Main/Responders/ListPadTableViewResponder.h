//
//  ListPadTableViewResponder.h
//  Manage
//
//  Created by Cliff Viegas on 19/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Collections
@class TaskListCollection;
@class NotebookCollection;

// Custom Table View Cells
@class ListPadTableViewCell;

// Constants
#define kHeaderViewFrame    CGRectMake(0, 15, 192, 45)
#define kHeaderImageViewFrame   CGRectMake(0, 15, 192, 45)
#define kFooterViewFrame    CGRectMake(0, 0, 192, 11)
#define kHeaderLabelFrame   CGRectMake(0, 25, 192, 35)


@protocol ListPadDelegate
- (void)listSelected:(NSInteger)theSelectedRow;
- (void)notebookSelected:(NSInteger)theSelectedRow;
- (void)loadingListsSelected;
- (void)archivesSelected;
- (void)shopfrontSelectedWithFrame:(CGRect)theFrame;
- (void)deleteRowsAtIndexPaths:(NSArray *)indexPaths;
- (void)insertRowsAtIndexPaths:(NSArray *)indexPaths;
- (void)reloadListPadTableView;
@end


@interface ListPadTableViewResponder : NSObject <UITableViewDelegate, UITableViewDataSource> {
//	id <ListPadDelegate>	delegate;
	TaskListCollection		*refTaskListCollection;
	NotebookCollection      *refNotebookCollection;
    
	NSInteger				selectedRow;
    NSInteger               selectedSection;
    
    BOOL                    listsCollapsed;         // If lists are collapsed
    BOOL                    notebooksCollapsed;     // If notebooks are collapsed
    UIImageView             *taskListArrowImageView;
    UIImageView             *noteBookArrowImageView;

}

//@property (nonatomic, assign)	id		delegate;
@property (nonatomic, assign)   BOOL    listsCollapsed;
@property (nonatomic, assign)   BOOL    notebooksCollapsed;
@property (nonatomic, assign)	NSObject<ListPadDelegate>		*delegate;
@property (weak, nonatomic) UITableView *tableView;


#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection andDelegate:(NSObject<ListPadDelegate>*)theDelegate;

#pragma mark Button Actions
- (void)updateSection:(NSInteger)section forNumberOfRows:(NSInteger)rows;

#pragma mark Class Methods
- (void)tableView:(UITableView *)tableView forceSelectRowAtIndexPathRow:(NSInteger)theSelectedRow andSection:(NSInteger)theSelectedSection;

- (void) setSelectedRow:(NSInteger) row andSection:(NSInteger) section;
- (void) reloadDataAtRow:(NSInteger) row andSection:(NSInteger) section;
@end
