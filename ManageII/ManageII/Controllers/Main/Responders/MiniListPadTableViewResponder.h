//
//  MiniListPadTableViewResponder.h
//  Manage
//
//  Created by Cliff Viegas on 15/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Master class
#import "ListPadTableViewResponder.h"

// Data Collections
@class TaskListCollection;

@interface MiniListPadTableViewResponder : ListPadTableViewResponder {
    
}

#pragma mark - Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection andDelegate:(NSObject<ListPadDelegate>*)theDelegate;

@end
