//
//  ListPadTableViewCell.h
//  Manage
//
//  Created by Cliff Viegas on 19/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Constants

#define kListTitleFrame			CGRectMake(20, 7, 119, 21)
#define kListCompletedFrame		CGRectMake(2, 2, 40, 40)
// Width of tableview is 192
#define kRightBorderFrame       CGRectMake(180, 0, 12, 38)
#define kLeftBorderFrame        CGRectMake(0, 0, 11, 38)
#define kSeparatorViewFrame     CGRectMake(10, 34, 200, 1)
#define kSeparatorViewFrameiOS7 CGRectMake(-10, 34, 220, 1)

@interface ListPadTableViewCell : UITableViewCell {
	UILabel			*listTitle;             // The list title
    UIImageView     *rightBorder;           // Control pad right image
    UIImageView     *leftBorder;            // Control pad left image
    UIView          *separatorView;         // Used for lines between rows
    UIImageView     *taskCountImageView;    // Task count image view
    
    // Subviews of task count image view
    UILabel         *taskCountLabel;
    UILabel         *overdueCountLabel;
}

@property	(nonatomic, retain)		UILabel			*listTitle;
@property   (nonatomic, retain)     UIImageView     *rightBorder;
@property   (nonatomic, retain)     UIImageView     *leftBorder;
@property   (nonatomic, retain)     UIView          *separatorView;
@property   (nonatomic, retain)     UIImageView     *taskCountImageView;

@property   (nonatomic, retain)     UILabel         *taskCountLabel;
@property   (nonatomic, retain)     UILabel         *overdueCountLabel;

#pragma mark - Public Methods
- (void)updateTaskCount:(NSInteger)taskCount andOverdueCount:(NSInteger)overdueCount note:(BOOL)isNote;

@end
