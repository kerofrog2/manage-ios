//
//  MiniListPadTableViewResponder.m
//  Manage
//
//  Created by Cliff Viegas on 15/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "MiniListPadTableViewResponder.h"

// Data Collections
#import "TaskListCollection.h"

// Data Models
#import "TaskList.h"

// Custom Table View Cells
#import "ListPadTableViewCell.h"

// Constants
//const CGRect kMiniNumberCaseFrame = { 14.f, 33.f, 42.f, 19.f };


@implementation MiniListPadTableViewResponder

#pragma mark - Initialisation

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection andDelegate:(NSObject<ListPadDelegate>*)theDelegate {
    self = [super init];
    if (self) {
        // Assign reference to passed task list collection
		refTaskListCollection = theTaskListCollection;
        
        // Init collapsed bools
        self.listsCollapsed = FALSE;
        
        // Assign the delegate
		self.delegate = theDelegate;
        
        // Init selected row and section to 0
		selectedRow = 0;
        selectedSection = 0;
    }
    return self;
}

#pragma mark - Button Events

- (void)headerButtonAction:(id)sender {
    UIButton *headerButton = (UIButton *)sender;
    NSInteger section = headerButton.tag;
    
    if (section == 0) {
        // Lists
        if ([self listsCollapsed] == TRUE && selectedSection == section) {
            self.listsCollapsed = FALSE;
        } else if ([self listsCollapsed] == FALSE && selectedSection == section) {
            self.listsCollapsed = TRUE;
            
        } 
        
        [self updateSection:section forNumberOfRows:[refTaskListCollection.lists count]];
    } 
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    ListPadTableViewCell *cell = (ListPadTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ListPadTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];// autorelease];
		//[cell.contentView addSubview:[self getTextFieldForIndexPath:indexPath]];
	}	
    
    // Get the counts
    NSInteger taskCount = 0;
    NSInteger overdueCount = 0;
    BOOL isNote = FALSE;
    if (indexPath.section == 0 && [refTaskListCollection.lists count] > 0) {
        TaskList *taskList = [refTaskListCollection.lists objectAtIndex:indexPath.row];
        isNote = [taskList isNote];
        taskCount = [taskList getCountOfNonOverdueTasks];
        overdueCount = [taskList getCountOfOverdueTasks];
    }
    
    [cell updateTaskCount:taskCount andOverdueCount:overdueCount note:isNote];
    
	
	// Default values
	cell.accessoryType = UITableViewCellAccessoryNone;
	[cell.listTitle setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
    [cell.listTitle setHighlightedTextColor:[UIColor darkTextColor]];
    [cell.listTitle setTextColor:[UIColor whiteColor]];
    //[cell.accessoryView setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
    
    
    
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.backgroundView = [[UIView alloc] init];// autorelease];
    //cell.backgroundView.backgroundColor = [UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0];
    cell.backgroundView.backgroundColor = [UIColor colorWithRed:69 / 255.0 green:65 / 255.0 blue:66 / 255.0 alpha:1.0];
    
    //[cell.contentView setBackgroundColor:[UIColor greenColor]];
    //[cell setBackgroundColor:[UIColor clearColor]];
    //[cell setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
    //[cell.contentView setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
    //cell.listTitle.text = @"";
    
    if ([indexPath section] == 0) {
        // Setup the cell
        TaskList *taskList = [refTaskListCollection.lists objectAtIndex:indexPath.row];
        
        //cell.imageView.image = [UIImage imageNamed:@"BoxTicked.png"];
        if (indexPath.row == selectedRow && indexPath.section == selectedSection) {
            // [cell.listTitle setTextColor:[UIColor lightGrayColor]];
            //cell.backgroundView.backgroundColor = [UIColor colorWithRed:69 / 255.0 green:65 / 255.0 blue:66 / 255.0 alpha:1.0];
            cell.backgroundView.backgroundColor = [UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0];
            //[cell.listTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
        }
        
        cell.listTitle.text = taskList.title;
    } 
	
	
    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    NSInteger rowCount = 0;
    if (section == 0) {
        if ([self listsCollapsed] == TRUE) {
            rowCount = 0;
        } else if ([self listsCollapsed] == FALSE) {
            rowCount = [refTaskListCollection.lists count];
        }
    }
    
	return rowCount;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == selectedRow) {
        // Do nothing
        return;
    }
    
    if ([indexPath section] == 0) {
        selectedRow = indexPath.row;
        selectedSection = indexPath.section;
        [tableView reloadData];
        [self.delegate listSelected:indexPath.row];
    } 
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:kFooterViewFrame];
    // Add a subview imageView
    UIImageView *footerImageView = [[UIImageView alloc] initWithFrame:kFooterViewFrame];
    [footerImageView setBackgroundColor:[UIColor clearColor]];
    [footerImageView setImage:[UIImage imageNamed:@"ControlPadFooter.png"]];
    [footerImageView setOpaque:YES];
    [footerView addSubview:footerImageView];
    [footerView setOpaque:YES];
//    [footerImageView release];
    
//    return [footerView autorelease];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 11.0;
}


@end
