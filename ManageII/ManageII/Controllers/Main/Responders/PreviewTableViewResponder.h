//
//  PreviewTableViewResponder.h
//  Manage
//
//  Created by Cliff Viegas on 20/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class TaskList;

// Data Collections
@class TagCollection;

@interface PreviewTableViewResponder : NSObject <UITableViewDelegate, UITableViewDataSource> {
//	TaskList		*refTaskList;
	TagCollection	*refTagCollection;
//    NSMutableArray *arrayTaskList;
}

@property (nonatomic, strong) NSMutableArray *arrayTaskList;
@property (nonatomic, strong) TaskList		*refTaskList;
@property (nonatomic) BOOL shouldShowTags;

#pragma mark Initialisation
- (id)initWithTaskList:(TaskList *)theTaskList andTagCollection:(TagCollection *)theTagCollection;

@end
