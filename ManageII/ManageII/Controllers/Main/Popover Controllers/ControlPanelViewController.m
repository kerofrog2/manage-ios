    //
//  ControlPanelViewController.m
//  Manage
//
//  Created by Cliff Viegas on 23/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ControlPanelViewController.h"

// Table View Responder
#import "ListPadTableViewResponder.h"

// Constants
#import "MainConstants.h"

// Task List Collection
#import "TaskListCollection.h"

// Notebook collection
#import "NotebookCollection.h"

#import "MainViewController.h"

#import "HiddenToolbar.h"

#import "NoteViewController.h"

#import "ListViewController.h"

#import "NotebookTileScrollView.h"

#import "TagCollection.h"

#import "NotebookListsViewController.h"

#import "Notebook.h"


// Constants
#define kViewControllerSize	CGSizeMake(256, 703)
#define kScrollObjWidth				414
#define kScrollObjBuffer			70

@interface ControlPanelViewController () <ArchivesDelegate>

@end

@implementation ControlPanelViewController{
    UIBarButtonItem *addButton;
    UIBarButtonItem *deleteButton;
    BOOL					newListInserted;
    
    // Data Collections
    TaskListCollection		*refMasterTaskListCollection;		// The master task list collection which includes all completed items
    NotebookCollection      *notebookCollection;                // The collection of notebooks
}

@synthesize refMainViewController;
@synthesize noteViewController;
@synthesize currentSection;
@synthesize miniCalendarView;
@synthesize controlPanelImageView;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[controlPanelImageView release];
//    [listPadTableViewResponder release];
//	[listPadTableView release];
//    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

/*
 - (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection andDelegate:(id<ListPadDelegate>)theDelegate {

 
 */

- (void)loadControlPanelWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection{
    
    // Assign references to notebook and tasklist collection
    refTaskListCollection = theTaskListCollection;
    refNotebookCollection = theNotebookCollection;
    
    // Create our list pad table view responder
    listPadTableViewResponder = [[ListPadTableViewResponder alloc] initWithTaskListCollection:refTaskListCollection notebookCollection:refNotebookCollection andDelegate:self];
    listPadTableView = [[UITableView alloc] initWithFrame:kListPadTableViewPortrait style:UITableViewStylePlain];
    [listPadTableView setBackgroundView:[[UIView alloc] init]];//autorelease
    [listPadTableView setContentInset:UIEdgeInsetsMake(0, 0, 33, 0)];
    [listPadTableView setBackgroundColor:[UIColor clearColor]];
    [listPadTableView setShowsHorizontalScrollIndicator:YES];
    [listPadTableView setShowsVerticalScrollIndicator:YES];
    [listPadTableView setDataSource:listPadTableViewResponder];
    [listPadTableView setDelegate:listPadTableViewResponder];
    
    [listPadTableViewResponder setDelegate:self];
    listPadTableViewResponder.tableView = listPadTableView;
    
    // Init ref main view controller to nil
    refMainViewController = nil;
    _listViewController = nil;
    noteViewController = nil;
    
    // Set content size
    [self setContentSizeForViewInPopover:kViewControllerSize];
    
    //		[self.view setBackgroundColor:[UIColor darkGrayColor]];
    
    // Load the control panel
    controlPanelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPanel.png"]];
    
    // Load the list pad table view
    /*listPadTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
     [listPadTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
     [listPadTableView setBackgroundColor:[UIColor clearColor]];
     [listPadTableView setShowsVerticalScrollIndicator:NO];
     [listPadTableView setDataSource:listPadTableViewResponder];
     [listPadTableView setDelegate:listPadTableViewResponder];*/
    
    [controlPanelImageView setFrame:kMainControlPanelImageLandscape];
    [listPadImageView setFrame:kListPadImageLandscape];
    [listPadTableView setFrame:kListPadTableViewLandscape];
    //[searchTableView setFrame:kSearchTableViewLandscape];
    //[searchBar setFrame:kSearchBarLandscape];
    
    [self.view addSubview:controlPanelImageView];
    [self.view addSubview:listPadImageView];
    [self.view addSubview:listPadTableView];
}

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_ControlPanelViewController;
#endif
        // Assign references to notebook and tasklist collection
        refTaskListCollection = theTaskListCollection;
        refNotebookCollection = theNotebookCollection;
        
        // Init the tag collection
        tagCollection = [[TagCollection alloc] initWithAllTags];
        
        // Create our list pad table view responder
        listPadTableViewResponder = [[ListPadTableViewResponder alloc] initWithTaskListCollection:refTaskListCollection notebookCollection:refNotebookCollection andDelegate:self];
        listPadTableView = [[UITableView alloc] initWithFrame:kListPadTableViewPortrait style:UITableViewStyleGrouped];
        [listPadTableView setBackgroundView:[[UIView alloc] init]];//autorelease
        [listPadTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [listPadTableView setContentInset:UIEdgeInsetsMake(0, 0, 33, 0)];
        [listPadTableView setBackgroundColor:[UIColor clearColor]];
        [listPadTableView setShowsVerticalScrollIndicator:NO];
        [listPadTableView setScrollEnabled:YES];
        [listPadTableView setDataSource:listPadTableViewResponder];
        [listPadTableView setDelegate:listPadTableViewResponder];
        
        [listPadTableViewResponder setDelegate:self];
        listPadTableViewResponder.tableView = listPadTableView;
		
        // Init ref main view controller to nil
		refMainViewController = nil;
        _listViewController = nil;
        noteViewController = nil;
		
		// Set content size
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
//		[self.view setBackgroundColor:[UIColor darkGrayColor]];
		
		// Load the control panel
//		controlPanelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPanel.png"]];
		
		// Load the list pad table view
		/*listPadTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [listPadTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
		[listPadTableView setBackgroundColor:[UIColor clearColor]];
		[listPadTableView setShowsVerticalScrollIndicator:NO];
		[listPadTableView setDataSource:listPadTableViewResponder];
		[listPadTableView setDelegate:listPadTableViewResponder];*/

//		[controlPanelImageView setFrame:kMainControlPanelImageLandscape];

        if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
            [listPadTableView setFrame:kListPadTableViewPortrait];
        } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
            [listPadTableView setFrame:kListPadTableViewLandscape];
        }

        
        [listPadImageView setFrame:kListPadImageLandscape];


			//[searchTableView setFrame:kSearchTableViewLandscape];
			//[searchBar setFrame:kSearchBarLandscape];
        
        refMasterTaskListCollection = [[TaskListCollection alloc] init];
        [refMasterTaskListCollection reloadAllLists];
//        refMasterTaskListCollection = theTaskListCollection;
		
        // Calendar View
        self.miniCalendarView = [[MiniCalendarView alloc] initWithFrame:kMiniCalendarViewFrame andTaskListCollection:refMasterTaskListCollection];
        [self.miniCalendarView setSelectionDelegate:self];
        
        calendarListToolsContainerView = [[UIView alloc] initWithFrame:kCalendarListToolsContainerShow];
        [calendarListToolsContainerView setFrame:kCalendarListToolsContainerShow];
        

        [calendarListToolsContainerView addSubview:self.miniCalendarView];

        [self.view addSubview:calendarListToolsContainerView];
//        [self.view addSubview:controlPanelImageView];
        [self.view addSubview:listPadTableView];
        [self.view addSubview:listPadImageView];
        
        // Load toolbar
        [self loadToolbar];
        
        // Load left navigation
//        [self loadLeftNavigationBarButtons];
        // Create an observer for alarm to view task
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alarmViewListItemOnTaskList)
                                                     name:kNotificationAlarmViewListItem
                                                   object:nil];
	}
	return self;
}

#pragma mark -
#pragma mark Button Actions

- (void)archivesButtonAction {
	if (refMainViewController != nil) {
		[refMainViewController archivesSelected];
	}
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // To update database after archiving Task List on Application launch.
//    BOOL needUpdateDatabase = [[NSUserDefaults standardUserDefaults]boolForKey:kNeedUpdateDatebaseForArchiveTaskList];
//    
//    if (needUpdateDatabase) {
//        [self updateLocalData];
//        [listPadTableView reloadData];
//        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kNeedUpdateDatebaseForArchiveTaskList];
//    }
    
    if (archivesHaveBeenUpdated == TRUE && archiveListsUpdated == TRUE) {
		[refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
        [refMainViewController updateNotebooksUsingCurrentTaskListCollection];
        
        // Update title and date
        //[self updateTitleAndDateForSelectedIndex];
        
		archivesHaveBeenUpdated = FALSE;
        
	} else if (archivesHaveBeenUpdated == TRUE && archiveListsUpdated == FALSE) {
		
		[refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
        
		//[self updateNotebooksUsingCurrentTaskListCollection];
        
		archivesHaveBeenUpdated = FALSE;
	}
}


- (void)viewDidLoad{
    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
//        self.preferredContentSize = CGSizeMake(320, 600);
//    }else{
//        self.contentSizeForViewInPopover = CGSizeMake(320, 600);
//    }
    
    // Update support iOS 7
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
            self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];
        }
    }
    
    self.navigationController.navigationBar.translucent = YES;
    

    [self loadActionSheets];
    
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;
//    
//    self.navigationItem.rightBarButtonItem = addButton;
    
    [super viewDidLoad];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    // Set allowsMultipleSelectionDuringEditing to YES only while
    // editing. This gives us the golden combination of swipe-to-delete
    // while out of edit mode and multiple selections while in it.
    
    [super setEditing:editing animated:animated];
    
    if (editing) {
        self.navigationItem.rightBarButtonItem = deleteButton;
    } else {
        self.navigationItem.rightBarButtonItem = addButton;
    }
}

#pragma mark -
#pragma mark Load Methods

// Load the hidden toolbar which displays the + and delete buttons
- (void)loadLeftNavigationBarButtons {
    
	// Add a toolbar instead of a help button to the right bar button
	HiddenToolbar *toolbar = [[HiddenToolbar alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
	[toolbar setBarStyle: UIBarStyleBlack];
	toolbar.opaque = NO;
	toolbar.backgroundColor = [UIColor redColor];
	toolbar.translucent = YES;
	
	// Array to hold the buttons in toolbar
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] init];
    
    settingsBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    UIButton *settingsButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 35)];
    
    [settingsButton setBackgroundImage:[UIImage imageNamed:@"SettingsIcon.png"] forState:UIControlStateNormal];
    settingsButton.showsTouchWhenHighlighted = YES;
    
    // Add action for button
    [settingsButton addTarget:self action:@selector(settingsBarButtonItemAction) forControlEvents:UIControlEventTouchUpInside];
    
    settingsBarButtonItem.customView = settingsButton;
	

    [toolbarButtons addObject:settingsBarButtonItem];
	
	// Add first spacer
	UIBarButtonItem *spacer1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
																			 target:nil action:nil];
	[toolbarButtons addObject:spacer1];

	
	// Place the buttons in the toolbar
	[toolbar setItems:toolbarButtons animated:NO];
	
	// Put the toolbar in the nav bar
	UIBarButtonItem *customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:toolbar];
	[self.navigationItem setLeftBarButtonItem:customBarButtonItem animated:NO];
    
}

- (void)loadToolbar {
	// Create the hidden toolbar
	bottomToolbar = [[HiddenToolbar alloc] init]; //WithFrame:kHiddenToolbarPortrait];
	bottomToolbar.opaque = NO;
	bottomToolbar.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1];
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[bottomToolbar setFrame:kBottomToolbarPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[bottomToolbar setFrame:kBottomToolbarLandscape];//720
	}
	
	// Create an array to hold the buttons on the toolbar
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] init];
	
	// Seperator
	UIBarButtonItem *seperator0 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                target:nil action:nil];
	seperator0.width = 20.0;
	[toolbarButtons addObject:seperator0];
    //	[seperator0 release];
	
	newListBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
																		 target:self
																		 action:@selector(newListBarButtonItemAction:)];
	[toolbarButtons addObject:newListBarButtonItem];
	
	
	
	// Seperator
	UIBarButtonItem *seperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
																			   target:nil action:nil];
	seperator.width = 20.0;
	[toolbarButtons addObject:seperator];
    //	[seperator release];
	
	deleteListBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
																			target:self
																			action:@selector(deleteListBarButtonItemAction:)];
	[toolbarButtons addObject:deleteListBarButtonItem];
	
	// Seperator
	UIBarButtonItem *seperator2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                target:nil action:nil];
	seperator2.width = 20.0;
	[toolbarButtons addObject:seperator2];
    //	[seperator2 release];
	
	//emailBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
	//																   target:self
	//																   action:@selector(emailBarButtonItemAction:)];
    UIImage *optionsButtonImage = [UIImage imageNamed:@"ExportListIcon.png"];
    if ([self currentSection] == EnumCurrentSectionNotebooks) {
        optionsButtonImage = [UIImage imageNamed:@"OptionsIcon.png"];
    }
    
	optionsBarButtonItem = [[UIBarButtonItem alloc] initWithImage:optionsButtonImage
                                                            style:UIBarButtonItemStylePlain
                                                           target:self action:@selector(optionsBarButtonItemAction:)];
	//[emailBarButtonItem set
	[toolbarButtons addObject:optionsBarButtonItem];
	
	// Add the buttons to the toolbar
	[bottomToolbar setItems:toolbarButtons];
    //	[toolbarButtons release];
	
	
	[self.view addSubview:bottomToolbar];
    [self.view bringSubviewToFront:bottomToolbar];
}

- (void)updateImageOptionButton{
    
    UIImage *optionsButtonImage = [UIImage imageNamed:@"ExportListIcon.png"];
    if ([self currentSection] == EnumCurrentSectionNotebooks) {
        optionsButtonImage = [UIImage imageNamed:@"OptionsIcon.png"];
    }
    
    [optionsBarButtonItem setImage:optionsButtonImage];
}


- (void)loadActionSheets {
    
    // New task list
    newListActionSheet = [[UIActionSheet alloc] initWithTitle:nil
													 delegate:self
											cancelButtonTitle:nil
									   destructiveButtonTitle:nil
											otherButtonTitles:@"New List", @"New Note", @"Duplicate List/Note", nil];
    
    // Delete note/list
    deleteListActionSheet = [[UIActionSheet alloc] initWithTitle:nil
														delegate:self
											   cancelButtonTitle:nil
										  destructiveButtonTitle:@"Delete List/Note"
											   otherButtonTitles:nil];
	
    // Send mail text/pdf task list
	optionsListActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:nil
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:@"Email Text List", @"Email PDF List", nil];
    
    // Send mail pdf note list
    exportNoteActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                        delegate:self
                                               cancelButtonTitle:nil
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:@"Email PDF", nil];
    
    newNotebookActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:nil
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:@"New Category", nil];//** Folder
    
    deleteNotebookActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                              destructiveButtonTitle:@"Delete Category"//** Folders
                                                   otherButtonTitles:nil];
}


#pragma mark -
#pragma mark Button Actions

// New list bar button item actoin
- (void)newListBarButtonItemAction:(id)sender {
    
    // Dismiss delete
    if (deleteListActionSheet) {
        [deleteListActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    // Dismiss send email
    if (exportNoteActionSheet) {
        [exportNoteActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    if (optionsListActionSheet) {
        [optionsListActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    // Dimiss delete notebook
    if (deleteNotebookActionSheet) {
        [deleteNotebookActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    // Didmiss add new task popover
    if (notebookListsPopoverController) {
        [notebookListsPopoverController dismissPopoverAnimated:YES];
    }
    
    if (currentSection == EnumCurrentSectionLists) {
        // Show add list popover
        UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
        [newListActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    }else{
        // Show add category popover
        UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
        [newNotebookActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    }
}

// Delete list bar button item actoin
- (void)deleteListBarButtonItemAction:(id)sender {
    
    // Dismiss add new list
    if (newListActionSheet) {
        [newListActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    // Dismiss send email
    if (exportNoteActionSheet) {
        [exportNoteActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    if (optionsListActionSheet) {
        [optionsListActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    // Dimiss new notebook
    if (newNotebookActionSheet) {
        [newNotebookActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    // Dismiss add new task popover
    if (notebookListsPopoverController) {
        [notebookListsPopoverController dismissPopoverAnimated:YES];
    }
    
    if (currentSection == EnumCurrentSectionLists) {
        // Show delete task list popover
        UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
        [deleteListActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    }else{
        // Show delete task list popover
        UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
        [deleteNotebookActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    }
}

// Send email list bar button item actoin
- (void)optionsBarButtonItemAction:(id)sender {
    
    UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
    
    // Dismiss add new list
    if (newListActionSheet) {
        [newListActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    // Dismiss delete
    if (deleteListActionSheet) {
        [deleteListActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    // Dismiss add new category
    if (newNotebookActionSheet) {
        [newNotebookActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    // Dismiss delete category
    if (deleteNotebookActionSheet) {
        [deleteNotebookActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    switch ([self currentSection]) {
        case EnumCurrentSectionNotebookLists:
        case EnumCurrentSectionLists://EnumCurrentSectionNotebookLists
        {
            if ([refTaskListCollection.lists count] == 0) {
                // do nothing
                return;
            }
            
            if (currentListIndex < 0 || currentListIndex >= [refMasterTaskListCollection.lists count]) {
                return;
            }
            TaskList *checkTaskList = [refMasterTaskListCollection.lists objectAtIndex:currentListIndex];
            if ([checkTaskList isNote] == FALSE) {
                [optionsListActionSheet showFromBarButtonItem:barButtonItem animated:NO];
            } else if ([checkTaskList isNote] == TRUE) {
                [exportNoteActionSheet showFromBarButtonItem:barButtonItem animated:NO];
            }
            
            
            break;
        }
        case EnumCurrentSectionNotebooks:
        {
            if ([refNotebookCollection.notebooks count] == 0) {
                // do nothing
                return;
            }
            
            if ([self currentSection] == EnumCurrentSectionNotebooks) {
                if (currentListIndex >= 0 && currentListIndex < [refNotebookCollection.notebooks count]) {
                    Notebook *notebook = [refNotebookCollection.notebooks objectAtIndex:currentListIndex];
                    
                    NotebookListsViewController *controller = [[NotebookListsViewController alloc] initWithNotebook:notebook
                                                                                        andMasterTaskListCollection:refMasterTaskListCollection] ;//autorelease];
                    // Init the navigation controller that will hold the edit list item view controller
                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];// autorelease];
                    notebookListsPopoverController = [[UIPopoverController alloc] initWithContentViewControllerWithAutoDismiss:navController];
                    notebookListsPopoverController.delegate = self;
                    [notebookListsPopoverController presentPopoverFromBarButtonItem:optionsBarButtonItem
                                                           permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
                }
            }
            break;
        }
        default:
            break;
    }
    
}

- (void)settingsBarButtonItemAction{
    
}

#pragma mark -
#pragma mark Action Sheet Delegates

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ([actionSheet isEqual:newListActionSheet]) {
		if (buttonIndex == 0) {
			// Set new list inserted to true
			newListInserted = TRUE;
			
			// New List pressed
            //Son Nguyen fix leaks
			TaskList *newMasterList = [[TaskList alloc] initNewList];
            
			// Insert the new list in the database and view
			[self insertNewTaskList:newMasterList];
            
		} else if (buttonIndex == 1) {
            // Create a new note
            newListInserted = TRUE;
            //Son Nguyen fix leaks
            // New Note pressed
            TaskList *newMasterNote = [[TaskList alloc] initNewNote];
            
            // Insert the new note in the database and view
            [self insertNewTaskListDrawing:newMasterNote];
            
        } else if (buttonIndex == 2) {
            // Duplicate the currently selected task list
            [self duplicateTheCurrentTaskList];
		}
	} else if ([actionSheet isEqual:deleteListActionSheet]) {
		// Delete selected list
		if (buttonIndex == 0) {
            
            // Delete list or note
            [self deleteListAtCurrentIndex];
            
    }
	} else if ([actionSheet isEqual:optionsListActionSheet]) {
        
		// Email list
		if (buttonIndex == 0) {
			[self sendTextMailForTaskList:[refTaskListCollection.lists objectAtIndex:currentListIndex]];
		} else if (buttonIndex == 1) {
			TaskList *theTaskList = [refTaskListCollection.lists objectAtIndex:currentListIndex];
			[self sendPDFMailForTaskList:theTaskList];
		}
        
	} else if ([actionSheet isEqual:exportNoteActionSheet]) {
        
        if (buttonIndex == 0) {
            // Email pdf version
            TaskList *theTaskList = [refMasterTaskListCollection.lists objectAtIndex:currentListIndex];
			[self sendPDFMailForTaskList:theTaskList];
        }
        
    } else if ([actionSheet isEqual:newNotebookActionSheet]) {

        // Need to add new notebook
        if (buttonIndex == 0) {
            
            Notebook *notebook = [[Notebook alloc] initNewNotebook];
            
            // Insert notebook on list pad table view
            [refNotebookCollection.notebooks insertObject:notebook atIndex:0];
            
            // Insert notebook on main view
            [refMainViewController insertNewNotebook:notebook];
            
            // Reload table view
            [self reloadListPadTableView];
        }
        
    } else if ([actionSheet isEqual:deleteNotebookActionSheet]) {
        // Need to delete the current notebook
        if (buttonIndex == 0) {
            
            // Delete notebook on main view
            [refMainViewController deleteCategory];
            
            // Delete notebook on list pad table view
            [self deleteListAtCurrentIndex];
            
            // Reload table view
            [self reloadListPadTableView];
        }
    }
    
	// Set toolbar user interaction to true
	[leftToolbar setUserInteractionEnabled:TRUE];
}

#pragma mark - Task List Methods

- (void)deleteListAtCurrentIndex {
    // Will also need to look for instances of the list in any notebooks
    
    NSInteger index = currentListIndex;
    BOOL isLastIndex = FALSE;
    
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
        {
            // TODO: Check if this list is contained in any notebooks
            if (index == [refTaskListCollection.lists count] - 1) {
                isLastIndex = TRUE;
            }
            
            TaskList *taskList = [refTaskListCollection.lists objectAtIndex:currentListIndex];
            NSInteger masterListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:taskList.listID];
            
            if (masterListIndex != -1 && [refMasterTaskListCollection.lists count] > 0) {
                [refMasterTaskListCollection.lists removeObjectAtIndex:masterListIndex];
                [refTaskListCollection deleteListAtIndex:currentListIndex permanent:NO];
                
                if ([taskList isNote] == YES) {
                    MAppDelegate *appDelegate = (MAppDelegate *)[UIApplication sharedApplication].delegate;
                    [appDelegate.noteViewController.drawingLayer clearView];
//                    appDelegate.noteViewController = nil;
                }
            }
            
            // Load right panel
            if (isLastIndex) {
                
                [self listSelected:currentListIndex - 1];
                [listPadTableViewResponder setSelectedRow:currentListIndex andSection:EnumCurrentSectionLists];
            }else{
                [self listSelected:currentListIndex];
                [listPadTableViewResponder setSelectedRow:currentListIndex andSection:EnumCurrentSectionLists];
            }
            
//            [self listSelected:currentListIndex]
            
            break;
        }
        case EnumCurrentSectionNotebooks:
        {
            if (index == [refNotebookCollection.notebooks count] - 1) {
                isLastIndex = TRUE;
            }
            
            [refNotebookCollection deleteNotebookAtIndex:currentListIndex];
            break;
        }
        case EnumCurrentSectionNotebookLists:
        {
            // TODO: The list needs to be removed from the notebook as well
            break;
        }
        default:
            break;
    }

    
    // Reload list pad
    [listPadTableView reloadData];
}


- (void)duplicateTheCurrentTaskList {
    // Do nothing if there is no lists in the task list collection
    if ([refTaskListCollection.lists count] == 0) {
        UIAlertView *noListsAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                   message:@"No lists present, please add a new list"
                                                                  delegate:nil cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil];
        [noListsAlertView show];
        //        [noListsAlertView release];
        return;
    }
    
    // Duplicate list pressed
    newListInserted = TRUE;
    
    TaskList *originalTaskList = [refTaskListCollection.lists objectAtIndex:currentListIndex];
    
    TaskList *newMasterList = [[TaskList alloc] initWithDuplicateTaskList:originalTaskList];// autorelease];
    
    // Insert the new list in the database and view
    
    if (newMasterList.isNote) {
        // Insert drawing
        [self insertNewTaskListDrawing:newMasterList];
    }else{
        [self insertNewTaskList:newMasterList];
    }
    
}

- (void)insertNewTaskList:(TaskList *)newMasterList {
    
	if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		[refMasterTaskListCollection.lists insertObject:newMasterList atIndex:0];
	} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
		[refMasterTaskListCollection.lists addObject:newMasterList];
		
		// Need to update the list order (as we have added to end)
		[refMasterTaskListCollection commitListOrderChanges];
		[refMasterTaskListCollection updateListOrdersInDatabase];
	}
	
	
	TaskList *newTaskList = [[TaskList alloc] initWithTaskListCopy:newMasterList];
    int index = 0;
	if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		[refTaskListCollection.lists insertObject:newTaskList atIndex:0];
        index = 0;
	} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
		[refTaskListCollection.lists addObject:newTaskList];
        index = [refTaskListCollection.lists count] - 1;
	}
    
//    NSInteger scrollIndex = 0;
//	if (newListInserted == TRUE && [refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
//		scrollIndex++;
//        //		newListInserted = FALSE;
//	}
//	
//	CGFloat newXLoc = (kScrollObjWidth + kScrollObjBuffer) * scrollIndex;
//	[listScrollView setContentOffset:CGPointMake(newXLoc, 0)];
    
    [listPadTableViewResponder setSelectedRow:index andSection:0];
    [self reloadListPadTableView];
//<<<<<<< HEAD
//    /*
//     When a row is selected, set the detail view controller's detail item to the item associated with the selected row.
//     */
//    MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
//    UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
//    
//    _listViewController = appDelegate.listViewController;
//    
//    if (_listViewController != nil) {
//        [_listViewController refreshViewWithNewTaskList:newTaskList andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection];
//    } else {
//    
//        _listViewController = [[ListViewController alloc] initWithTaskList:newTaskList andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection andBackButtonTitle:nil];
//    }
//=======
    
    [self listSelected:index];
//>>>>>>> manage_2_nhan
    
//    UITableViewCell *cell = [listPadTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
//    [cell setSelected:YES animated:YES];
    
//<<<<<<< HEAD
//    [nav setViewControllers:[NSArray arrayWithObject:_listViewController] animated:NO];
//=======
    /*
     When a row is selected, set the detail view controller's detail item to the item associated with the selected row.
     */
//    MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
//    UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
//    
//    ListViewController *listVC = appDelegate.listViewController;
//    
//    if (listVC != nil) {
//        [listVC refreshViewWithNewTaskList:newTaskList andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection];
//    } else {
//    
//        listVC = [[ListViewController alloc] initWithTaskList:newTaskList andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection andBackButtonTitle:nil];
//    }
//    
//    
//    [nav setViewControllers:[NSArray arrayWithObject:listVC] animated:NO];
//>>>>>>> manage_2_nhan
    
    
    
    // Update at right home screen
    // code from MasterView

    
//    [self.navigationController pushViewController:listVC animated:YES];
    
//    [listVC loadListViewWithTaskList:newTaskList andTagCollection:nil andTaskListCollection:taskListCollection andMasterTaskListCollection:refMasterTaskListCollection andBackButtonTitle:nil];
    
    //[self.storyboard instantiateViewControllerWithIdentifier:@"ListViewController"];
    
    // gets tabbar controllers
//    MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
//    
//    NSMutableArray *viewControllerArray = [[NSMutableArray alloc] initWithArray:[[appDelegate.splitViewController.viewControllers objectAtIndex:1] viewControllers]];
//    [viewControllerArray addObject:listVC];
//    
////    [[appDelegate.splitViewController.viewControllers objectAtIndex:1] pushViewController:listVC animated:YES];
//    
//    [[appDelegate.splitViewController.viewControllers objectAtIndex:1] setViewControllers:viewControllerArray animated:YES];
    
	
    // Insert the new list button
//    [self insertNewListButton];
//    
//    [self listShaded:YES forIndex:currentListIndex];
//	
//	if ([self currentViewType] == EnumViewTypeList) {
//		// Scroll to the new list
//		if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
//			[self scrollToListAtIndex:0 animated:YES];
//		} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
//			NSInteger location = [taskListCollection.lists count] - 1;
//			[self scrollToListAtIndex:location animated:YES];
//		}
//		
//		if ([taskListCollection.lists count] == 1) {
//			[self updateTitleAndDateForSelectedIndex];
//		}
//        
//	} else if ([self currentViewType] == EnumViewTypeTile) {
//		if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
//			tileScrollView.currentListIndex = tileScrollView.currentListIndex + 1;
//		}
//		[tileScrollView updateScrollList];
//	}
    
}

- (void)insertNewTaskListDrawing:(TaskList *)newMasterList {
    
	if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		[refMasterTaskListCollection.lists insertObject:newMasterList atIndex:0];
	} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
		[refMasterTaskListCollection.lists addObject:newMasterList];
		
		// Need to update the list order (as we have added to end)
		[refMasterTaskListCollection commitListOrderChanges];
		[refMasterTaskListCollection updateListOrdersInDatabase];
	}
	
	
	TaskList *newTaskList = [[TaskList alloc] initWithTaskListCopy:newMasterList];
    int index = 0;
	if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		[refTaskListCollection.lists insertObject:newTaskList atIndex:0];
        index = 0;
	} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
		[refTaskListCollection.lists addObject:newTaskList];
        index = [refTaskListCollection.lists count] - 1;
	}
    
    //    NSInteger scrollIndex = 0;
    //	if (newListInserted == TRUE && [refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
    //		scrollIndex++;
    //        //		newListInserted = FALSE;
    //	}
    //
    //	CGFloat newXLoc = (kScrollObjWidth + kScrollObjBuffer) * scrollIndex;
    //	[listScrollView setContentOffset:CGPointMake(newXLoc, 0)];
    
    
    [listPadTableViewResponder setSelectedRow:index andSection:0];
    [self reloadListPadTableView];
    
    [self listSelected:index];
    /*
     When a row is selected, set the detail view controller's detail item to the item associated with the selected row.
     */
//    MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
//    
//    NoteViewController *noteVC = appDelegate.noteViewController;
//    
//    if (noteVC != nil) {
//        [noteVC refreshViewWithNewTaskListCollection:refMasterTaskListCollection andTaskList:newTaskList];
//    } else {
//        
//        noteVC = [[NoteViewController alloc] initWithMasterTaskListCollection:refMasterTaskListCollection andTaskList:newTaskList andBackButtonTitle:nil];
//    }
//    
////    NoteViewController *noteVC = [[NoteViewController alloc] initWithMasterTaskListCollection:refMasterTaskListCollection andTaskList:newTaskList andBackButtonTitle:nil];
////    
//    UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
//    
//    [noteVC listSelected:index];
//    
//    [nav setViewControllers:[NSArray arrayWithObject:noteVC] animated:NO];

    
}


#pragma mark -
#pragma mark UIViewController Delegates

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {

    if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        
        [listPadImageView setFrame:kListPadImagePortrait];
        
        [listPadTableView setFrame:kListPadTableViewPortrait];
        // Bottom bar
        [bottomToolbar setFrame:kBottomToolbarPortrait];
        

        
    }else if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
        
        [listPadImageView setFrame:kListPadImageLandscape];
        
        [listPadTableView setFrame:kListPadTableViewLandscape];
         // Bottom bar
        [bottomToolbar setFrame:kBottomToolbarLandscape];//720
    }
}


#pragma mark -
#pragma mark List Pad Delegates

- (void)listSelected:(NSInteger)theSelectedRow {
    
    self.currentSection = EnumCurrentSectionLists;
    
    refMainViewController = nil;
    
    MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (theSelectedRow == -1 || [refMasterTaskListCollection.lists count] <= 0) {
//        UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
//        [nav animated:NO];
        UINavigationController *newNav = [[UINavigationController alloc] initWithRootViewController:nil];
        appDelegate.splitViewController.detailViewController = newNav;
        _listViewController = nil;
        noteViewController = nil;
        appDelegate.noteViewController = nil;
        appDelegate.listViewController = nil;
        
        // reload toolbar
        [self updateImageOptionButton];
        
        return;
    }
    
    // Set current list index
    currentListIndex = theSelectedRow;
    
    //[refMainViewController listSelected:theSelectedRow];
    
//    [refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
    if ([[refMasterTaskListCollection.lists objectAtIndex:theSelectedRow] isNote] == YES) {
        
        // Go to note view
        
        UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
        
//        UINavigationController *masterNav = (UINavigationController*)appDelegate.splitViewController.masterViewController;
        
        NoteViewController *noteVC;
        
        noteVC = appDelegate.noteViewController;
        
        if (noteVC != nil) {
            
            if (noteVC.currentTaskList == [refMasterTaskListCollection.lists objectAtIndex:theSelectedRow]) {
                
                
                // Reload toolbar
//                [self loadToolbar];

            } else {
                if ([[nav viewControllers] count] > 0) {
                    id currentView = [[nav viewControllers] lastObject];
                    if ([currentView isKindOfClass:[NoteViewController class]]) {
//                        MBProgressHUD *progressHUD = [[MBProgressHUD alloc] initWithView:noteVC.view];
//                        [noteVC.view addSubview:progressHUD];
////                        progressHUD.delegate = self;
//                        progressHUD.labelText = @"Saving...";
//                        [progressHUD showWhileExecuting:@selector(updateDrawingData) onTarget:noteVC withObject:nil animated:YES];
                        [noteVC updateDrawingData];
                    }
                }
                
                [noteVC refreshViewWithNewTaskListCollection:refMasterTaskListCollection andTaskList:[refMasterTaskListCollection.lists objectAtIndex:theSelectedRow]];
            }
            
        } else {
            noteVC = [[NoteViewController alloc] initWithMasterTaskListCollection:refMasterTaskListCollection andTaskList:[refTaskListCollection.lists objectAtIndex:theSelectedRow] andBackButtonTitle:nil];
            appDelegate.noteViewController = noteVC;

        }
        
        if ([[nav viewControllers] count] > 0) {
            id currentView = [[nav viewControllers] lastObject];
            if ([currentView isKindOfClass:[NoteViewController class]]) {

                if (currentView == noteVC) {
//                    [noteVC listSelected:theSelectedRow];
                    
                } else {
                    
                    [nav setViewControllers:[NSArray arrayWithObject:noteVC] animated:NO];
                    [noteVC willAnimateRotationToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.2];
                }
            } else {
                
                [nav setViewControllers:[NSArray arrayWithObject:noteVC] animated:NO];
                [noteVC willAnimateRotationToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.2];
            }
        } else {
            [nav setViewControllers:[NSArray arrayWithObject:noteVC] animated:NO];
        }
        
//        if ([[masterNav viewControllers] count] > 0) {
//            for (id VC in [masterNav viewControllers]) {
//                if ([VC isKindOfClass:[ControlPanelViewController class]]) {
//                    
//                    ControlPanelViewController *controlPanel = VC;
//                    noteVC = controlPanel.noteViewController;
//                    
//                    if (noteVC.currentTaskList == [refTaskListCollection.lists objectAtIndex:theSelectedRow]) {
//                        
//                        [noteVC listSelected:theSelectedRow];
//                        // Reload toolbar
//                        [self loadToolbar];
//                        return;
//                    } else {
//                        [noteVC updateDrawingData];
//                        
//                        [noteVC refreshViewWithNewTaskListCollection:refMasterTaskListCollection andTaskList:[refTaskListCollection.lists objectAtIndex:theSelectedRow]];
//                    }
//                    
//                    break;
//                }
//            }
//            if (noteVC == nil) {
//                noteVC = [[NoteViewController alloc] initWithMasterTaskListCollection:refMasterTaskListCollection andTaskList:[refTaskListCollection.lists objectAtIndex:theSelectedRow] andBackButtonTitle:nil];
//            }
//        }
        
//        // Get current detail view to save.
        
        
        
        [noteVC listSelected:theSelectedRow];
        
        // Reload toolbar
        [self updateImageOptionButton];//[self loadToolbar];
        
    }else{
        
        // Update task list for hide completed task
        [self reloadHideCompletedTaskWithTaskIndex:theSelectedRow];
        
        
        // Go to list view
        MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
        
        UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
        
//        _listViewController = appDelegate.listViewController;
        
        if (_listViewController != nil) {
            
//            [nav setViewControllers:[NSArray arrayWithObject:listVC] animated:NO];
            
            if (_listViewController.currentTaskList == [refTaskListCollection.lists objectAtIndex:theSelectedRow]) {
                
//                [listVC listSelected:theSelectedRow];
                // Reload toolbar
//                [self loadToolbar];
            } else {
                [_listViewController refreshViewWithNewTaskList:[refTaskListCollection.lists objectAtIndex:theSelectedRow] andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection];
            }
            
        } else {
            
            _listViewController = [[ListViewController alloc] initWithTaskList:[refTaskListCollection.lists objectAtIndex:theSelectedRow]  andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection andBackButtonTitle:nil];
            appDelegate.listViewController = _listViewController;
        }
        
        if ([[nav viewControllers] count] > 0) {
            id currentView = [[nav viewControllers] lastObject];
            if ([currentView isKindOfClass:[ListViewController class]]) {
                if (currentView == _listViewController) {
//                    [listVC listSelected:theSelectedRow];
                } else {
                    
                    [nav setViewControllers:[NSArray arrayWithObject:_listViewController] animated:NO];
                    [_listViewController willAnimateRotationToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
                }
            } else {
                [nav setViewControllers:[NSArray arrayWithObject:_listViewController] animated:NO];
                [_listViewController willAnimateRotationToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
            }
        } else {
            [nav setViewControllers:[NSArray arrayWithObject:_listViewController] animated:NO];
        }
        
        [_listViewController listSelected:theSelectedRow];
        
        // UpdateImage options button
        [self updateImageOptionButton];
    }
    
    [self reloadListPadTableView];
    
}

- (void)loadingListsSelected {
//    [refMainViewController loadingListsSelected];
}

- (void)notebookSelected:(NSInteger)theSelectedRow {

    self.currentSection = EnumCurrentSectionNotebooks;
    
    // Set current category index
    currentListIndex = theSelectedRow;
    
    CGRect theNotebookImageFrame = CGRectZero;
    
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        theNotebookImageFrame = CGRectMake(142 + 35, 180, 414, 551);
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        theNotebookImageFrame = CGRectMake(398 + 35, 82, 414, 551);
    }
    
    if (refMainViewController == nil) {
        
        MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
        
        MainViewController *mainVC = [[MainViewController alloc] initWithMasterTaskListCollection:refMasterTaskListCollection andManageAppDelegate:nil];
        [mainVC setCurrentListIndex:theSelectedRow];
        
        UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
        
        [nav setViewControllers:[NSArray arrayWithObject:mainVC] animated:NO];
        
        refMainViewController = mainVC;
    }

    [refMainViewController notebookSelected:theSelectedRow];
    
    // Update image options button
    [self updateImageOptionButton];
}

- (void)archivesSelected {
//    [refMainViewController archivesButtonAction];
    
    refMainViewController = nil;
    
    //tagCollection
	MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	ArchivesViewController *controller = [[ArchivesViewController alloc] initWithTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection] ;//autorelease];
	
	controller.delegate = self;
    
    UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
    [nav setViewControllers:[NSArray arrayWithObject:controller] animated:NO];    
}

- (void)shopfrontSelectedWithFrame:(CGRect)theFrame {
    [refMainViewController shopfrontSelectedWithFrame:theFrame];
}

- (void)deleteRowsAtIndexPaths:(NSArray *)indexPaths {
    [listPadTableView deleteRowsAtIndexPaths:indexPaths
                            withRowAnimation:UITableViewRowAnimationTop];
}

- (void)insertRowsAtIndexPaths:(NSArray *)indexPaths {
    [listPadTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
}

- (void)reloadListPadTableView {
    [listPadTableView reloadData];
}

#pragma mark -
#pragma mark Mini Calendar Delegate

- (void)didSelectDate:(NSDate *)theDate {
    
    refMainViewController = nil;
    
    // Go to list view
    MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _listViewController = [[ListViewController alloc] initWithTaskList:[refTaskListCollection.lists objectAtIndex:currentListIndex]  andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection andBackButtonTitle:nil];
    
    UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
    
    [nav setViewControllers:[NSArray arrayWithObject:_listViewController] animated:NO];
    
    [_listViewController didSelectDate:theDate];
}

#pragma mark -
#pragma mark Send Mail Methods

- (void)sendTextMailForTaskList:(TaskList *)theTaskList {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
		return;
	}
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	[controller setMessageBody:[theTaskList getTextMailMessageWithTagCollection:tagCollection showSubtasks:TRUE] isHTML:NO];
	
	[controller setSubject:[NSString stringWithFormat:@"Manage Task List: %@", theTaskList.title]];
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	
	[self presentModalViewController:controller animated:YES];
}

- (void)sendPDFMailForTaskList:(TaskList *)theTaskList {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
		return;
	}
	
	NSString *pdfPath = @"";
    NSString *initialTitle = @"Manage Task List";
    
    if ([theTaskList isNote] == FALSE) {
        pdfPath = [theTaskList createPDFMessageWithTagCollection:tagCollection showSubtasks:YES];
    } else if ([theTaskList isNote]) {
        initialTitle = @"Manage Note";
        MAppDelegate *appDelegate = (MAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate.noteViewController updateDrawingData];
        
        pdfPath = [theTaskList createPDFMessageFromNote];
    }
    
	
	NSData *dataObj = [NSData dataWithContentsOfFile:pdfPath];
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	
	[controller setSubject:[NSString stringWithFormat:@"%@: %@", initialTitle, theTaskList.title]];
	
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	[controller addAttachmentData:dataObj
						 mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"%@.pdf", initialTitle]];
	
	[self presentModalViewController:controller animated:YES];
}

- (void)sendPDFMailForNote:(TaskList *)theTaskList {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
		return;
	}
    
    
	
	NSString *pdfPath = [theTaskList createPDFMessageFromNote];
	
	NSData *dataObj = [NSData dataWithContentsOfFile:pdfPath];
    
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	
	[controller setSubject:[NSString stringWithFormat:@"Manage Note: %@", theTaskList.title]];
	
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	[controller addAttachmentData:dataObj
						 mimeType:@"application/pdf" fileName:@"Manage Note.pdf"];
	
	[self presentModalViewController:controller animated:YES];
}


#pragma mark -
#pragma mark Mail Composer View Delegates

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	NSString *message = @"";
	
	switch (result) {
		case MFMailComposeResultCancelled:
			message = @"Email Cancelled";
			
			break;
		case MFMailComposeResultSaved:
			message = @"Email Cancelled";
			break;
		case MFMailComposeResultSent:
			message = @"Email Sent";
			break;
		case MFMailComposeResultFailed:
			message = @"Email Failed";
			break;
		default:
			break;
	}
	
	// Display result from mail composer
	UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Mail Composer"
														message: message
													   delegate: nil
											  cancelButtonTitle: @"Ok"
											  otherButtonTitles: nil];
	[mailAlert show];
	
	[self becomeFirstResponder];
	[self dismissModalViewControllerAnimated:YES];
}


- (void)updateLocalData {
    
    //Check database before restore database (Fix bug crash add new folder after resotre database)
    MAppDelegate *mainDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
    [mainDelegate databaseChecks];
    
    // Reload all the lists
    [refMasterTaskListCollection reloadAllLists];
    
    // Do an archive check
    [mainDelegate archiveCheck];
    
    // Init the notebook collection
    notebookCollection = [[NotebookCollection alloc] initWithAllNotebooks];
    
	// Reload tag collection
	[tagCollection reloadAllTags];
	
	[refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
    
    // Init the current list index to 0
    currentListIndex = 0;
    
    // update hide completed task with task list
    [self reloadTaskListForHideCompletedTask];
    
    // Init current section to lists
    self.currentSection = EnumCurrentSectionLists;
    
    mainDelegate.noteViewController = nil;
    mainDelegate.listViewController = nil;
    _listViewController = nil;
    
    [self listSelected:currentListIndex];
    
    [listPadTableView reloadData];
}

#pragma mark -
#pragma mark PopOver Controller Delegates

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    [refMainViewController addListTaskToCategory:refNotebookCollection];
    
    [self reloadListPadTableView];
}

/*
 Called on the delegate when the popover controller will dismiss the popover. Return NO to prevent the dismissal of the view.
 */
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    // Dismiss delete and add new category
    if (newNotebookActionSheet) {
        [newNotebookActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    if (deleteNotebookActionSheet) {
        [deleteNotebookActionSheet dismissWithClickedButtonIndex:4 animated:YES];
    }
    
    return YES;
}


-(void)setSelectedNotebookIndex:(NSInteger)noteBookIndex {
    currentListIndex = noteBookIndex;
    // Section = 1 is category, if change category section index need update here to set correct selected row
    [listPadTableViewResponder setSelectedRow:noteBookIndex andSection:1];
}

-(void)reloadTitle:(NSString *)title atRow:(NSInteger)row andSection:(NSInteger)section {
    switch (section) {
        case 0:
        {
            // Update task list title
            if ([refTaskListCollection.lists count] > 0) {
                TaskList *taskList = [refTaskListCollection.lists objectAtIndex:row];
                taskList.title = title;
            }
        }
            break;
            
        case 1:
        {
            // Update notebook title
            if ([refNotebookCollection.notebooks count] > 0) {
                Notebook *notebook = [refNotebookCollection.notebooks objectAtIndex:row];
                notebook.title = title;
            }
        }
            break;
            
        default:
            break;
    }
    
    [listPadTableViewResponder reloadDataAtRow:row andSection:section];
}

#pragma mark - === Handle view reminder task ===
- (void)alarmViewListItemOnTaskList {
    
    // Get Alarm List Item
    NSInteger alarmListItemID = [[NSUserDefaults standardUserDefaults] integerForKey:kAlarmListItem];
    // Get Alarm Task List
    NSInteger alarmTaskList = [[NSUserDefaults standardUserDefaults] integerForKey:kAlarmTaskList];
    
    if (alarmListItemID > 0 && alarmTaskList > -1) {
        
        // Show detail list item
        currentListIndex = alarmTaskList;
        
        if (alarmTaskList < [refTaskListCollection.lists count]) {
            TaskList *taskList = [refTaskListCollection.lists objectAtIndex:alarmTaskList];
            
            // Go to list view
            MAppDelegate *appDelegate = (MAppDelegate *)[[UIApplication sharedApplication] delegate];
            
            UINavigationController *nav = (UINavigationController*)appDelegate.splitViewController.detailViewController;
            
            _listViewController = [[ListViewController alloc] initWithTaskList:taskList  andTagCollection:tagCollection andTaskListCollection:refTaskListCollection andMasterTaskListCollection:refMasterTaskListCollection andBackButtonTitle:nil];
            
            [nav setViewControllers:[NSArray arrayWithObject:_listViewController] animated:NO];
            [_listViewController willAnimateRotationToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
        }
        
        refMainViewController = nil;
        
        [listPadTableViewResponder setSelectedRow:currentListIndex andSection:0];
    }
}

#pragma mark - Refresh View Control Panel -

- (void)refreshControlPanelWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection andTagCollection:(TagCollection*)theTagCollection{
    // Assign references to notebook and tasklist collection
    refTaskListCollection = theTaskListCollection;
    refNotebookCollection = theNotebookCollection;
    
    tagCollection = theTagCollection;
    
    [refMasterTaskListCollection reloadAllLists];
    
    // Init ref main view controller to nil
    refMainViewController = nil;
    noteViewController = nil;
    
    // update hide completed task with task list
    [self reloadTaskListForHideCompletedTask];
    
    [self reloadListPadTableView];
}

#pragma mark - Hide Completed Task - 
#pragma mark

- (void)reloadHideCompletedTaskWithTaskIndex:(NSInteger)index{
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        //Hide completed task is ON -> SHow completed task = 0 -> An nhung task completed
        NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
        
         if (isShowCompletedTasks == 0) {
             TaskList *mainTaskList = [refMasterTaskListCollection.lists objectAtIndex:index];
             TaskList *editTaskList = [refTaskListCollection.lists objectAtIndex:index];
             
             for (ListItem *mainItem in mainTaskList.listItems) {
                 for (ListItem *editItem in editTaskList.listItems) {
                     if (mainItem.listItemID == editItem.listItemID) {
                         if (editItem.hasJustComplete == YES) {
                             //mainItem.completed = editItem.hasJustComplete;
                             
                             if (editItem.parentListItemID != -1) {// Has parent task
                                 // If task has parent task and hascompleted is YES -> task is completed
                                 mainItem.completed = YES;
                                 
                             }else{// Has no parent task
                                 
                                 if ([editTaskList hasSubtasksForListItemAtIndex:[editTaskList.listItems indexOfObject:editItem]] == YES) {// Has subtask
                                     BOOL isAllTasksCompleted = YES;
                                     // Get all subtask and check
                                     for (ListItem *subItem in editTaskList.listItems) {
                                         if (subItem.parentListItemID == editItem.listItemID) {
                                             // Check subtask is completed
                                             if (subItem.hasJustComplete != YES && subItem.completed != YES) {
                                                 isAllTasksCompleted = NO;
                                                 break;
                                             }
                                         }
                                     }
                                     
                                     // All subtask is completed
                                     if (isAllTasksCompleted == YES && editItem.hasJustComplete == YES) {
                                         // This task is completed
                                         mainItem.completed = YES;
                                     }else{
                                         mainItem.completed = NO;
                                         mainItem.hasJustComplete = YES;
                                     }
                                     
                                 }else{// Has no subtask
                                     
                                     mainItem.completed = editItem.hasJustComplete;
                                     editItem.completed = editItem.hasJustComplete;
                                 }
                                 
                             }
                         }else{
                             
                             if (editItem.parentListItemID != -1) {// Has parent task
                                 if (editItem.completed == YES) {
                                     mainItem.hasJustComplete = editItem.completed;
                                 }
                             }else{// Has no parent task
                                 
                                 if ([editTaskList hasSubtasksForListItemAtIndex:[editTaskList.listItems indexOfObject:editItem]] == YES) { // Has subtask
                                     BOOL isAllTasksCompleted = YES;
                                     // Get all subtask and check
                                     for (ListItem *subItem in editTaskList.listItems) {
                                         if (subItem.parentListItemID == editItem.listItemID) {
                                             // Check subtask is completed
                                             if (subItem.completed != YES) {
                                                 isAllTasksCompleted = NO;
                                                 break;
                                             }
                                         }
                                     }
                                     
                                     // All subtask is completed
                                     if (isAllTasksCompleted == YES && editItem.completed == YES) {
                                         mainItem.hasJustComplete = YES;
                                         mainItem.completed = YES;
                                     }
                                     if (isAllTasksCompleted == NO && editItem.completed == YES) {
                                         mainItem.hasJustComplete = YES;
                                         mainItem.completed = NO;
                                     }
                                 }else{// Has no subtask
                                     
                                     if (editItem.completed == YES) {
                                         mainItem.hasJustComplete = YES;
                                         mainItem.completed = YES;
                                     }
                                 }
                             }
                             
                         }
                     }
                 }
             }
         }
    }
}


- (void)reloadTaskListForHideCompletedTask{
    
    // set data
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        
        for (NSInteger i = 0; i < [refMasterTaskListCollection.lists count]; i++) {
            
            //Hide completed task is ON -> SHow completed task = 0 -> An nhung task completed
            NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
            
            if (isShowCompletedTasks == 0) {
                
                TaskList *mainTaskList = [refMasterTaskListCollection.lists objectAtIndex:i];
                TaskList *editTaskList = [refTaskListCollection.lists objectAtIndex:i];
                
                for (ListItem *mainItem in mainTaskList.listItems) {
                    for (ListItem *editItem in editTaskList.listItems) {
                        if (mainItem.listItemID == editItem.listItemID) {
                            if (editItem.hasJustComplete == YES) {
                                //mainItem.completed = editItem.hasJustComplete;
                                
                                if (editItem.parentListItemID != -1) {// Has parent task
                                    // If task has parent task and hascompleted is YES -> task is completed
                                    mainItem.completed = YES;
                                    
                                }else{// Has no parent task
                                    
                                    if ([editTaskList hasSubtasksForListItemAtIndex:[editTaskList.listItems indexOfObject:editItem]] == YES) {// Has subtask
                                        BOOL isAllTasksCompleted = YES;
                                        // Get all subtask and check
                                        for (ListItem *subItem in editTaskList.listItems) {
                                            if (subItem.parentListItemID == editItem.listItemID) {
                                                // Check subtask is completed
                                                if (subItem.hasJustComplete != YES && subItem.completed != YES) {
                                                    isAllTasksCompleted = NO;
                                                    break;
                                                }
                                            }
                                        }
                                        
                                        // All subtask is completed
                                        if (isAllTasksCompleted == YES && editItem.hasJustComplete == YES) {
                                            // This task is completed
                                            mainItem.completed = YES;
                                        }else{
                                            mainItem.completed = NO;
                                            mainItem.hasJustComplete = YES;
                                        }
                                        
                                    }else{// Has no subtask
                                        
                                        mainItem.completed = editItem.hasJustComplete;
                                    }
                                    
                                }
                            }else{
                                
                                if (editItem.parentListItemID != -1) {// Has parent task
                                    if (editItem.completed == YES) {
                                        mainItem.hasJustComplete = editItem.completed;
                                    }
                                }else{// Has no parent task
                                    
                                    if ([editTaskList hasSubtasksForListItemAtIndex:[editTaskList.listItems indexOfObject:editItem]] == YES) { // Has subtask
                                        BOOL isAllTasksCompleted = YES;
                                        // Get all subtask and check
                                        for (ListItem *subItem in editTaskList.listItems) {
                                            if (subItem.parentListItemID == editItem.listItemID) {
                                                // Check subtask is completed
                                                if (subItem.completed != YES) {
                                                    isAllTasksCompleted = NO;
                                                    break;
                                                }
                                            }
                                        }
                                        
                                        // All subtask is completed
                                        if (isAllTasksCompleted == YES && editItem.completed == YES) {
                                            mainItem.hasJustComplete = YES;
                                            mainItem.completed = YES;
                                        }
                                        if (isAllTasksCompleted == NO && editItem.completed == YES) {
                                            mainItem.hasJustComplete = YES;
                                            mainItem.completed = NO;
                                        }
                                    }else{// Has no subtask
                                        
                                        if (editItem.completed == YES) {
                                            mainItem.hasJustComplete = YES;
                                            mainItem.completed = YES;
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    }
}

#pragma mark -
#pragma mark Archives Delegates

- (void) archivesUpdated {
    archivesHaveBeenUpdated = TRUE;
    
    [self reloadListPadTableView];
}

- (void)archiveListsUpdated {
	archiveListsUpdated = TRUE;
    
    [self refreshControlPanelWithTaskListCollection:refTaskListCollection notebookCollection:notebookCollection andTagCollection:tagCollection];
}


@end
