//
//  ControlPanelViewController.h
//  Manage
//
//  Created by Cliff Viegas on 23/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Table View Responder
#import "ListPadTableViewResponder.h"

// Calendar (for delegate)
#import "MiniCalendarView.h"

#import "TagCollection.h"

// For email
#import <MessageUI/MessageUI.h>

#import "TileScrollView.h"

// Main View Controller Link
@class MainViewController;
@class ListViewController;
@class NoteViewController;

// Data Collections
@class TaskListCollection;
@class NotebookCollection;
// Hidden Toolbar
@class HiddenToolbar;

@interface ControlPanelViewController : GAITrackedViewController <ListPadDelegate, UIActionSheetDelegate, MiniCalendarDelegate, MFMailComposeViewControllerDelegate, UIPopoverControllerDelegate>{
//	MainViewController              *refMainViewController;
    ListPadTableViewResponder       *listPadTableViewResponder;
    TaskListCollection              *refTaskListCollection;
    NotebookCollection              *refNotebookCollection;
	
	UIImageView				*controlPanelImageView;				// The rounded rectangle control panel image
	UIImageView				*listPadImageView;					// The list paper background image
	UITableView				*listPadTableView;					// The list pad table view that sits on list pad image view
    
    HiddenToolbar			*leftToolbar;
    
    HiddenToolbar           *bottomToolbar;
    
    // Buttons
    UIBarButtonItem			*newListBarButtonItem;
    UIBarButtonItem			*deleteListBarButtonItem;
    UIBarButtonItem			*optionsBarButtonItem;
    UIBarButtonItem			*settingsBarButtonItem;
    
    // Action sheet
    UIActionSheet           *newListActionSheet;
    UIActionSheet			*deleteListActionSheet;
    UIActionSheet			*optionsListActionSheet;
    UIActionSheet           *exportNoteActionSheet;
    UIActionSheet           *newNotebookActionSheet;
    UIActionSheet           *deleteNotebookActionSheet;
    
    
    UIPopoverController     *notebookListsPopoverController;
    
    // Current Section variables
    NSInteger               currentSection;
    
    UIView                  *calendarListToolsContainerView;
    // Calendar
    MiniCalendarView        *miniCalendarView;
    
    NSInteger				currentListIndex;
    
    // Tag Collection
    TagCollection			*tagCollection;						// The collection of tags
    
    
    // Tile Scroll View
    TileScrollView			*tileScrollView;
    
    BOOL					archiveListsUpdated;
    BOOL					archivesHaveBeenUpdated;
}

@property (nonatomic, assign)	MainViewController	*refMainViewController;
@property (nonatomic, strong)	ListViewController	*listViewController;
@property (nonatomic, strong)   UIImageView				*controlPanelImageView;
@property (nonatomic)           NSInteger               currentSection;
@property (nonatomic, retain)   MiniCalendarView            *miniCalendarView;
@property (nonatomic, assign)	NoteViewController	*noteViewController;

#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection;
- (void)loadControlPanelWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection;

- (void) setSelectedNotebookIndex:(NSInteger) noteBookIndex;
- (void) reloadTitle:(NSString *) title atRow:(NSInteger) row andSection:(NSInteger) section;
- (void)refreshControlPanelWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection andTagCollection:(TagCollection*)theTagCollection;
- (void) reloadTaskListForHideCompletedTask;


- (void)updateLocalData ;
@end
