//
//  NotebookTileScrollView.h
//  Manage
//
//  Created by Cliff Viegas on 28/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TaskListCollection;

// Data Models
@class Notebook;
@class TaskList;

// View Controllers
@class MainViewController;

@interface NotebookTileScrollView : UIScrollView {
    TaskListCollection      *refTaskListCollection;
    TaskListCollection      *refMasterTaskListCollection;
    MainViewController      *refMainViewController;
//    Notebook                *refNotebook;
    CGRect                  notebookImageFrame;
}

@property   (nonatomic, assign)     Notebook    *refNotebook;
@property   (assign)                CGRect      notebookImageFrame;

#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection andMainViewController:(MainViewController *)theController
     andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection andNotebook:(Notebook *)theNotebook 
           andNotebookImageFrame:(CGRect)theNotebookImageFrame;

#pragma mark Update Methods
- (void)updateScrollList;
- (void)loadIconsInTile:(UIImageView *)tileImageView forList:(TaskList *)taskList; 
- (void)loadCompletedStampForTile:(UIImageView *)tile;

#pragma mark Dispatch Touch Methods
- (void)dispatchTouchAtPoint:(CGPoint)touchPoint forEvent:(UIEvent *)event;

#pragma mark Helper Methods
- (void)resizeLabelFrame:(UILabel *)titleLabel forTitle:(NSString *)titleString;
- (UILabel *)getListTitleLabelCopy;

@end
