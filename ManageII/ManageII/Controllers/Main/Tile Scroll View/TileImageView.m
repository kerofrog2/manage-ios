//
//  TileImageView.m
//  Manage
//
//  Created by Cliff Viegas on 2/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "TileImageView.h"


@implementation TileImageView

@synthesize expanded;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		// Init is expanded to false
		self.expanded = FALSE;
	}
	return self;
}





@end
