//
//  AdmobViewController.m
//  Manage Free
//
//  Created by Kerofrog on 6/14/13.
//
//

#import "AdmobViewController.h"
#import "GADBannerView.h"
#import "GADRequest.h"
#import "MainConstants.h"
#import "MBProgressHUD.h"

// Constants
#define kPopoverSize				CGSizeMake(320, 318)
#define kViewControllerSize         CGSizeMake(320, 103)
#define kTableViewFrame				CGRectMake(0, 0, 320, 100)
#define kNumberInAdMob                2

@interface AdmobViewController ()

//Array admob
@property (nonatomic, strong) NSMutableArray *arrayAdmob;

@end

@implementation AdmobViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    
//	if (popoverController) {
//		[popoverController setPopoverContentSize:kPopoverSize animated:YES];
//	}
}

#pragma mark -
#pragma mark Public Set Methods

- (void)setPopoverController:(UIPopoverController *)thePopoverController {
	popoverController = thePopoverController;
}

- (void)viewDidLoad
{
    self.arrayAdmob = [[NSMutableArray alloc] init];
    
    // Set content size
    [self setContentSizeForViewInPopover:kViewControllerSize];
    
    [self loadTableView];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"";
    
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
//        // AskingPoint tag
//        [APManager requestCommandsWithTag:kAskingPointTagMore];
//        
//        // AskingPoint event
//        [APManager addEventWithName:kAskingPointEventMoreButton];
//    }
}


- (void)loadTableView {
	
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStylePlain];
    [myTableView setBackgroundColor:[UIColor blackColor]];
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
    [myTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	
	[self.view addSubview:myTableView];
	[myTableView reloadData];
    
    
}

- (void)loadAdmob:(NSInteger)index{
    
    CGPoint origin = CGPointMake(0.0, 0.0);
    
    // Use predefined GADAdSize constants to define the GADBannerView.
    self.adBanner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner
                                                    origin:origin];//autorelease
    
    // Note: Edit SampleConstants.h to provide a definition for kSampleAdUnitID
    // before compiling.
    if (index == 0) {
        self.adBanner.adUnitID = kManageAdmob;
    }else{
        self.adBanner.adUnitID = kKikiFarmAnimalAdmob;
    }

    self.adBanner.delegate = self;
    [self.adBanner setRootViewController:self];
    [self.adBanner loadRequest:[self createRequest]];
}


#pragma mark -
#pragma mark Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    
	return kNumberInAdMob;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    // Load admob
    [self loadAdmob:indexPath.row];
    
    //Add admob to cell
    [cell addSubview:self.adBanner];

	
    return cell;
}
    

- (void)dealloc {
//    adBanner_.delegate = nil;
//    [adBanner_ release];
//    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark GADRequest generation

// Here we're creating a simple GADRequest and whitelisting the application
// for test ads. You should request test ads during development to avoid
// generating invalid impressions and clicks.
- (GADRequest *)createRequest {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as
    // well as any devices you want to receive test ads.
//    request.testDevices =
//    [NSArray arrayWithObjects: 
//     // TODO: Add your device/simulator test identifiers here. They are
//     // printed to the console when the app is launched.
//     nil];
    return request;
}

#pragma mark GADBannerViewDelegate impl

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"Received ad successfully");
    // Hide HUD
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)adView:(GADBannerView *)view
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
    // Hide HUD
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


@end


