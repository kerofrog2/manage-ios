//
//  MethodHelper.h
//  Manage
//
//  Created by Cliff Viegas on 19/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAppDelegate.h"
#import "ListItem.h"

// Date Formats
#define K_DATESORT_FORMAT			@"yyyyMMdd"
#define K_DATEONLY_FORMAT			@"yyyy-MM-dd"
#define K_TIMEONLY_FORMAT			@"HH:mm:ss"
#define K_DATETIME_FORMAT			@"yyyy-MM-dd HH:mm:ss"
#define kTimeFormat                 @"hh:mm a"
#define kDateTimeUSAFormat          @"MMM dd yyyy, hh:mm:ss a"
#define kDateTimeNotUSAFormat       @"dd MMM yyyy, hh:mm:ss a"
#define kDateTimeUSAFormatWithoutSecond          @"MMM dd yyyy, hh:mm a"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface MethodHelper : NSObject {

}

+ (NSString *)escapeStringsForSQL:(NSString *)inputString;

#pragma mark Date Helper Methods
+ (NSString *)getShortWeekdayForDate:(NSDate *)theDate;
+ (NSString *)stringGMTFromDate:(NSDate *)theDate usingFormat:(NSString *)theFormat;
+ (NSString *)stringFromDate:(NSDate *)theDate usingFormat:(NSString *)theFormat;
+ (NSDate *)dateFromString:(NSString *)dateString usingFormat:(NSString *)theFormat;
+ (NSDate *)dateGMTFromString:(NSString *)dateString usingFormat:(NSString *)theFormat;
+ (NSString *)localizedDateTimeFrom:(NSDate *)theDate usingDateStyle:(NSDateFormatterStyle)dateStyle andTimeStyle:(NSDateFormatterStyle)timeStyle;
+ (NSString *)localizedDateFrom:(NSDate *)theDate usingStyle:(NSDateFormatterStyle)style withYear:(BOOL)showYear;
+ (BOOL)isCurrentYearForDate:(NSDate *)theDate;
+ (BOOL)isToday:(NSDate *)theDate;
+ (NSInteger)getDateDifferenceFromTodayForDate:(NSDate *)theDate;
+ (BOOL)isDateInPast:(NSDate *)theDate;
+ (NSString *)getDateTimeStringFromTimeInterval:(NSTimeInterval)timeInterval usingFormat:(NSString *)format;

#pragma mark Rotation Placement Helpers
+ (void)adjustX:(CGFloat)x andY:(CGFloat)y forView:(UIView *)theView;
+ (CGRect)moveToLandscapeLocation:(CGRect)originalFrame;
+ (CGRect)moveToPortraitLocation:(CGRect)originalFrame;

#pragma mark Image Manipulation Methods
+ (UIImage *)cropImage:(UIImage *)largeImage usingCropRect:(CGRect)cropRect;
+ (UIImage *)scaleImage:(UIImage *)image maxWidth:(float)maxWidth maxHeight:(float)maxHeight;

#pragma mark Database File Methods
+ (void)removeOldBackups;
+ (NSArray *)getSortedListOfBackupFiles:(NSString *)path;
+ (NSString *)createBackup;
+ (void)createLogFile:(NSString *)log fromBackupPath:(NSString *)backupPath;
+ (void)createLogFile:(NSString *)log;
+ (NSString *)createDatabaseBackup;
+ (NSString *)getTimeStampStringFromFile:(NSString *)theFile;
+ (void)restoreBackupData:(NSString *)backupFile backupPath:(NSString *)backupPath;
+ (NSString *)getBackupFileFromBackupPath:(NSString *)backupPath;

#pragma mark - String Manipulation Methods
+ (NSString *)concatenatedString:(NSString *)sourceString font:(UIFont *)stringFont maxSize:(CGSize)maxSize lineBreakMode:(UILineBreakMode)lineBreakMode;

#pragma mark Class Helper Methods
+ (CGFloat)getIOSVersion;
+ (void)reportMemory;
+ (NSData *)encodeObject:(NSDictionary *)dicData withKey:(NSString *)key;
+ (NSDictionary *)decodeObject:(NSData *)objectData andKey:(NSString *)key;
+ (NSString *)getCopyOfScribble:(NSString *)originalScribble;
+ (NSString *)getPngScribblePathFromGLSScribble:(NSString *)originalScribble;
+ (CGFloat)distanceBetween:(CGPoint)point1 and:(CGPoint)point2;
+ (NSString *)getDatabasePath;
+ (NSString *)getHomePath;
+ (NSString *)getBackupPath;
+ (NSString *)getScribblePath;
+ (NSString *)getLogPath;
+ (NSString *)getUniqueGUID;
+ (NSString *)getUniqueFileName;
+ (NSInteger)numberOfListItemsForBadgeNotification;
+ (void)showAlertViewWithTitle:(NSString *)title andMessage:(NSString *)message andButtonTitle:(NSString *)buttonTitle;
+ (NSString *)replaceHttpSpecialCharactersForString:(NSString *)theString;
+ (BOOL)stringObject:(NSString *)theString existsInArray:(NSArray *)theArray;
+ (BOOL)stringObject:(NSString *)theString existsInToodledoTagArray:(NSArray *)theArray;
+ (NSString *)getTimeStampStringFromDate:(NSDate *)theDate;
+ (NSDate *)convertDateToUTCTimezone:(NSDate *)sourceDate forDestinationZone:(NSString *)theZone;

+ (BOOL)listItemIsExistInArray:(NSMutableArray *)array withListItem:(ListItem *)listItem;

+ (BOOL)isOlderThanToDayOneMonthWithDayString:(NSString *) dayString;

+ (GADBannerView *)sharedAdsBanner;

#pragma mark Check Network Connection
+ (BOOL)connectedToInternet;

#pragma mark BuzzCity
+ (NSString *)ODIN1;


@end
