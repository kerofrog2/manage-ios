//
//  AskingPointConstants.h
//  Manage
//
//  Created by Phi Tran on 10/29/13.
//
//

#ifndef Manage_AskingPointConstants_h
#define Manage_AskingPointConstants_h

// Use test app key when build debug
#ifdef DEBUG
    #define kAskingPointAppKey              @"5gDKADcEUm3cy0caflBqlna2BZsRDqXN9ceFq28fHd8="
#else
    #define kAskingPointAppKey              @"pQCJAG8EUm38ziju525W2bvEBOFXrfF79NDJnzuJJnA="
#endif

// Define AskingPoint Tags
#define kAskingPointTagAppLauched           @"app_lauched"
#define kAskingPointTagMainScreen           @"main_screen"
#define kAskingPointTagSetting              @"setting_screen"
#define kAskingPointTagMore                 @"more_screen"


// Define AskingPoint events
// Common event
#define kAskingPointEventMoreButton         @"press_more_button"

// Have 2 type: typing & handwriting
#define kAskingPointEventNewTask            @"new_task"
#define kAskingPointEventNewSubTask         @"new_sub_task"

#define kAskingPointEventNewList            @"new_list"
#define kAskingPointEventNewNote            @"new_note"
#define kAskingPointEventNewReminder        @"new_reminder"

// Settings
// Parse theme name when send to server
#define kAskingPointEventChangeTheme        @"change_theme"
#define kAskingPointEventNewBackup          @"new_backup"
#define kAskingPointEventRestoreBackup      @"restore_backup"
#define kAskingPointEventLinkDropbox        @"link_dropbox"
#define kAskingPointEventUnLinkDropbox      @"unlink_dropbox"

// Define data of event
#define kAskingPointEventDataKeyType        @"type"
#define kAskingPointEventDataHandwriting    @"handwriting"
#define kAskingPointEventDataTyping         @"typing"

#endif
