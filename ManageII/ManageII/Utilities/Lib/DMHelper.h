//
//  DMHelper.h
//  iDesign
//
//  Created by Cliff Viegas on 25/02/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// DMHelper stands for Dictionary Method Helper
// It is used to assist in extracting non-string values from a dictionary object.  It
// is also used to cater for cases where the value in the dictionary object is null.

@interface DMHelper : NSObject {

}

+ (NSString *)getNSStringValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary;
+ (NSInteger)getNSIntegerValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary;
+ (int)getIntegerValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary;
+ (double)getDoubleValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary;
+ (float)getFloatValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary;
+ (BOOL)getBoolValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary;
+ (SInt64)getSInt64ValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary;

@end
