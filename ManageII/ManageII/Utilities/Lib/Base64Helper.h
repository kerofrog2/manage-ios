//
//  Base64Helper.h
//  Manage
//
//  Created by Cliff Viegas on 18/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Base64Helper : NSObject {

}

+ (NSString *)encodeBase64WithString:(NSString *)strData;
+ (NSString *)encodeBase64WithData:(NSData *)objData;
+ (NSData *)decodeBase64WithString:(NSString *)strBase64;

@end
