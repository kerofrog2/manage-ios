//
//  MethodHelper.m
//  Manage
//
//  Created by Cliff Viegas on 19/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "MethodHelper.h"

// Business Layer
#import "BLListItem.h"
#import "BLApplicationSetting.h"

// Data Models
#import "ApplicationSetting.h"

// Zip Archive
#import "ZipArchive.h"

// DM Helper
#import "DMHelper.h"

// For checking memory
#import "mach/mach.h"

// For check connection
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

// BuzzCity
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonHMAC.h>

#include <sys/sysctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h> 
#include <sys/param.h>
#include <sys/file.h>

#include <net/if.h>
#include <net/ethernet.h>
#include <net/if_dl.h>
#include <netinet/in.h>

#import "GADBannerView.h"



@implementation MethodHelper

// Adds escape characters for strings that have inverted commas
+ (NSString *)escapeStringsForSQL:(NSString *)inputString {
	if([inputString isEqualToString:@""]){
		return @"null";
	}
	NSString *returnValue = [inputString stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
	returnValue = [NSString stringWithFormat:@"'%@'", returnValue];
	return returnValue;
}

#pragma mark -
#pragma mark Date Helper Methods

+ (NSString *)getShortWeekdayForDate:(NSDate *)theDate {
    // Get our array of short weekdays symbols
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];//autorelease
	NSArray *weekdays = [dateFormatter shortWeekdaySymbols];
	
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents  *components = [calendar components:NSWeekdayCalendarUnit fromDate:theDate];
	
	return [weekdays objectAtIndex:[components weekday]];
}

+ (NSString *)stringGMTFromDate:(NSDate *)theDate usingFormat:(NSString *)theFormat {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
	[dateFormat setDateFormat:theFormat];
	
	NSString *dateString = [dateFormat stringFromDate:theDate];
	
//	[dateFormat release];
	
	return dateString;
}

+ (NSString *)stringFromDate:(NSDate *)theDate usingFormat:(NSString *)theFormat {
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:theFormat];
	
	NSString *dateString = [dateFormat stringFromDate:theDate];
	
//	[dateFormat release];
	
	return dateString;
}

+ (NSDate *)dateFromString:(NSString *)dateString usingFormat:(NSString *)theFormat {
	//NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	
	// Convert string to date object
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:theFormat];
	//[dateFormat setTimeZone:timeZone];
	
	NSDate *date = [dateFormat dateFromString:dateString];
//	[dateFormat release];
	
	return date;
}

+ (NSDate *)dateGMTFromString:(NSString *)dateString usingFormat:(NSString *)theFormat {
	//NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	
	// Convert string to date object
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
	[dateFormat setDateFormat:theFormat];
	//[dateFormat setTimeZone:timeZone];
	
	NSDate *date = [dateFormat dateFromString:dateString];
//	[dateFormat release];
	
	return date;
}

// In future might need to give with year option if expanding time to tasks
+ (NSString *)localizedDateTimeFrom:(NSDate *)theDate usingDateStyle:(NSDateFormatterStyle)dateStyle andTimeStyle:(NSDateFormatterStyle)timeStyle {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeStyle:timeStyle];
	[dateFormatter setDateStyle:dateStyle];
	
	NSString *stringDate = [dateFormatter stringFromDate:theDate];
//	[dateFormatter release];
	
	return stringDate;
}

+ (NSString *)localizedDateFrom:(NSDate *)theDate usingStyle:(NSDateFormatterStyle)style withYear:(BOOL)showYear {
	//NSCalendar *calendar = [NSCalendar currentCalendar];
	
	//NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
	//NSDateComponents *components = [calendar components:unitFlags fromDate:theDate];

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	[dateFormatter setDateStyle:style];
	
	if (style == NSDateFormatterMediumStyle) {
		
	}
	
	// Return full date if requested
	if (showYear == YES) {
		NSString *stringDate = [dateFormatter stringFromDate:theDate];
//		[dateFormatter release];
		return stringDate;
	}
	
	// Return full date if it is not the current year for passed date
	if ([self isCurrentYearForDate:theDate] == FALSE) {
		NSString *stringDate = [dateFormatter stringFromDate:theDate];
//		[dateFormatter release];
		return stringDate;
	}
	
	// Read out the format string
	//NSString *format = [dateFormatter dateFormat];
	
	NSString *format = @"MMM d";
	
	/*NSLog(@"format is %@", format);
	format = [format stringByReplacingOccurrencesOfString:@"/y" withString:@""];
	format = [format stringByReplacingOccurrencesOfString:@", y" withString:@""];
	format = [format stringByReplacingOccurrencesOfString:@"y," withString:@""];
	format = [format stringByReplacingOccurrencesOfString:@",y" withString:@""];
	format = [format stringByReplacingOccurrencesOfString:@" y" withString:@""];
	format = [format stringByReplacingOccurrencesOfString:@"y" withString:@""];*/
	[dateFormatter setDateFormat:format];
	
	//NSString *dateComponents = @"MMMd";
//	NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:dateComponents options:0 locale:[NSLocale currentLocale]];
	
	
	//NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:dateComponents options:0 locale:@"en_GB"];
	
	//[dateFormatter setDateFormat:dateFormat];
	
	//NSDateFormatter
	
	NSString *stringDate = [dateFormatter stringFromDate:theDate];
//	[dateFormatter release];
	return stringDate;
	
	//NSString *format = [dateFormatter dateFormat];
	
	//NSString *dateComponents = @"MMMd";
	
	
	
	/*
	 NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
	 NSLocale *gbLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
	 
	 NSString *dateFormat;
	 NSString *dateComponents = @"yMMMMd";
	 
	 dateFormat = [NSDateFormatter dateFormatFromTemplate:dateComponents options:0 locale:usLocale];

	 dateFormat = [NSDateFormatter dateFormatFromTemplate:dateComponents options:0 locale:gbLocale];

	 // Output:
	 // Date format for English (United States): MMMM d, y
	 // Date format for English (United Kingdom): d MMMM y
	 
	 */
	
	//return format;
}

+ (BOOL)isCurrentYearForDate:(NSDate *)theDate {
	NSCalendar *calendar = [NSCalendar currentCalendar];
	
	//NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
	NSDateComponents *dateYearComponent = [calendar components:NSYearCalendarUnit fromDate:theDate];
	NSDateComponents *currentYearComponent = [calendar components:NSYearCalendarUnit fromDate:[NSDate date]];
	
	if ([dateYearComponent year] == [currentYearComponent year]) {
		return YES;
	}
	
	return NO;
}

+ (BOOL)isToday:(NSDate *)theDate {
	NSCalendar *gregorian = [[NSCalendar alloc]
							  initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components =
    [gregorian components:
	 (NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit)
                 fromDate:[NSDate date]];
	NSDate *today = [gregorian dateFromComponents:components];
	
	components = [gregorian components:
				  (NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit)
							  fromDate:theDate];
	NSDate *compareDate = [gregorian dateFromComponents:components];
	
//	[gregorian release];
	
	if ([today isEqualToDate:compareDate]) {
		return TRUE;
	}
	
	return FALSE;
}

// Due date in future = positive, past = negative
+ (NSInteger)getDateDifferenceFromTodayForDate:(NSDate *)theDate {
	NSString *todayDateString = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATEONLY_FORMAT];
	NSDate *todaysDate = [MethodHelper dateFromString:todayDateString usingFormat:K_DATEONLY_FORMAT];
	NSTimeInterval lastDiff = [theDate timeIntervalSinceNow];
	NSTimeInterval todaysDiff = [todaysDate timeIntervalSinceNow];
	NSTimeInterval dateDiff = lastDiff - todaysDiff;
	NSInteger dayDifference = (int)(dateDiff / 86400.0f);
	return dayDifference;
}

// Time difference greater than zero is future, less than zero is past
+ (BOOL)isDateInPast:(NSDate *)theDate {
    
	NSTimeInterval timeDifference = [theDate timeIntervalSinceNow];
	
	if (timeDifference > 0) {
		return FALSE;
	}
	return TRUE;
}


+ (NSString *)getDateTimeStringFromTimeInterval:(NSTimeInterval)timeInterval usingFormat:(NSString *)format {
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
	return [self stringFromDate:date usingFormat:format];
}


/*
 I've got a timestamp as a string like:
 
 Thu, 21 May 09 19:10:09 -0700
 
 and I'd like to convert it to a relative time stamp like '20 minutes ago' or '3 days ago'.
 
 What's the best way to do this using Objective-C for the iPhone?
 
 ANSWER
 
 -(NSString *)dateDiff:(NSString *)origDate {
 NSDateFormatter *df = [[NSDateFormatter alloc] init];
 [df setFormatterBehavior:NSDateFormatterBehavior10_4];
 [df setDateFormat:@"EEE, dd MMM yy HH:mm:ss VVVV"];
 NSDate *convertedDate = [df dateFromString:origDate];
 [df release];
 NSDate *todayDate = [NSDate date];
 double ti = [convertedDate timeIntervalSinceDate:todayDate];
 ti = ti * -1;
 if(ti < 1) {
 return @"never";
 } else      if (ti < 60) {
 return @"less than a minute ago";
 } else if (ti < 3600) {
 int diff = round(ti / 60);
 return [NSString stringWithFormat:@"%d minutes ago", diff];
 } else if (ti < 86400) {
 int diff = round(ti / 60 / 60);
 return[NSString stringWithFormat:@"%d hours ago", diff];
 } else if (ti < 2629743) {
 int diff = round(ti / 60 / 60 / 24);
 return[NSString stringWithFormat:@"%d days ago", diff];
 } else {
 return @"never";
 }   
 }
 
 
 */

#pragma mark -
#pragma mark Rotation Placement Helpers

+ (void)adjustX:(CGFloat)x andY:(CGFloat)y forView:(UIView *)theView {
	CGRect newFrame = CGRectMake(theView.frame.origin.x + x, 
								 theView.frame.origin.y + y, 
								 theView.frame.size.width, 
								 theView.frame.size.height);
	[theView setFrame:newFrame];
}

+ (CGRect)moveToLandscapeLocation:(CGRect)originalFrame {
	CGRect newFrame = CGRectMake(originalFrame.origin.x + 256, 
								 originalFrame.origin.y - 128, 
								 originalFrame.size.width, 
								 originalFrame.size.height);
	return newFrame;
}

+ (CGRect)moveToPortraitLocation:(CGRect)originalFrame {
	CGRect newFrame = CGRectMake(originalFrame.origin.x - 256, 
								 originalFrame.origin.y + 128, 
								 originalFrame.size.width, 
								 originalFrame.size.height);
	return newFrame;
}

#pragma mark -
#pragma mark Image Manipulation Methods

// Crop image
+ (UIImage *)cropImage:(UIImage *)largeImage usingCropRect:(CGRect)cropRect {
	CGImageRef imageRef = CGImageCreateWithImageInRect([largeImage CGImage], cropRect);
	
	
	UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
	return croppedImage;
	
	// or use the UIImage wherever you like
	//[UIImageView setImage:[UIImage imageWithCGImage:imageRef]]; 
	//CGImageRelease(imageRef);
}

// Scale image
+ (UIImage *)scaleImage:(UIImage *)image maxWidth:(float)maxWidth maxHeight:(float)maxHeight {
	CGImageRef imgRef = image.CGImage;
	
	CGFloat width = CGImageGetWidth(imgRef);
	CGFloat height = CGImageGetHeight(imgRef);
	
	if (width <= maxWidth && height <= maxHeight)
	{
		return image;
	}
	
	CGAffineTransform transform = CGAffineTransformIdentity;
	CGRect bounds = CGRectMake(0, 0, width, height);
	if (width > maxWidth || height > maxHeight)
	{
		CGFloat ratio = width/height;
		if (ratio > 1)
		{
			bounds.size.width = maxWidth;
			bounds.size.height = bounds.size.width / ratio;
		}
		else
		{
			bounds.size.height = maxHeight;
			bounds.size.width = bounds.size.height * ratio;
		}
	}
	
	CGFloat scaleRatio = bounds.size.width / width;
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextScaleCTM(context, scaleRatio, -scaleRatio);
	CGContextTranslateCTM(context, 0, -height);
	
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return imageCopy;
}

#pragma mark -
#pragma mark Database File Methods

// This method will remove old backups (up to 50 backups can be saved)
+ (void)removeOldBackups {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSInteger maxCount = 25;
	
	// Get the sorted array (descending)
	NSArray *backupsArray = [fileManager contentsOfDirectoryAtPath:[MethodHelper getBackupPath] 
															 error:nil];;
	//NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:nil ascending:NO selector:@selector(localizedCaseInsensitiveCompare:)];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:NO selector:@selector(localizedCaseInsensitiveCompare:)];
	
	NSArray *sortedArray = [backupsArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	if ([sortedArray count] > maxCount) {
		NSFileManager *fileManager = [NSFileManager defaultManager];
		for (int i = [sortedArray count] - 1; i > (maxCount - 1); i--) {
			NSString *backupName = [sortedArray objectAtIndex:i];
			
			NSString *backupDBPath = [NSString stringWithFormat:@"%@%@",
									  [MethodHelper getBackupPath],
									  backupName];
			
			if ([fileManager fileExistsAtPath:backupDBPath]) {
				NSError *deleteFileError = nil;
				[fileManager removeItemAtPath:backupDBPath error:&deleteFileError];
				
				if (deleteFileError) {
					NSLog(@"%@", [deleteFileError localizedDescription]);
				}
			}
		}
	}
	
	// Get log sorted array
	backupsArray = [fileManager contentsOfDirectoryAtPath:[MethodHelper getLogPath] 
													error:nil];
	sortedArray = [backupsArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	if ([sortedArray count] > maxCount) {
		NSFileManager *fileManager = [NSFileManager defaultManager];
		for (int i = [sortedArray count] - 1; i > (maxCount - 1); i--) {
			NSString *logName = [sortedArray objectAtIndex:i];
			
			NSString *backupLogPath = [NSString stringWithFormat:@"%@%@",
									  [MethodHelper getLogPath],
									  logName];
			
			if ([fileManager fileExistsAtPath:backupLogPath]) {
				NSError *deleteFileError = nil;
				[fileManager removeItemAtPath:backupLogPath error:&deleteFileError];
				
				if (deleteFileError) {
					NSLog(@"%@", [deleteFileError localizedDescription]);
				}
			}
		}
	}
	
//	[sortDescriptor release];
}

// Return sorted list of backup files
+ (NSArray *)getSortedListOfBackupFiles:(NSString *)path {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSArray *backupsArray = [fileManager contentsOfDirectoryAtPath:path 
															 error:nil];;
	//NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:nil ascending:NO selector:@selector(localizedCaseInsensitiveCompare:)];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:NO selector:@selector(localizedCaseInsensitiveCompare:)];
	
	NSArray *sortedArray = [backupsArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	NSMutableArray *filterArray = [NSMutableArray arrayWithArray:sortedArray];
	
	for (int i = [filterArray count] - 1; i >= 0; i--) {
		NSString *theFile = [filterArray objectAtIndex:i];
		
		if ([theFile rangeOfString:@"_"].length == 0 ||
			([theFile rangeOfString:@".db"].length == 0 && 
			 [theFile rangeOfString:@".txt"].length == 0 &&
			 [theFile rangeOfString:@".zip"].length == 0)) {
			[filterArray removeObjectAtIndex:i];
		}
	}
	
	sortedArray = (NSArray *)filterArray;
	
//	[sortDescriptor release];
	
	return sortedArray;
}

/*
 
 
 - (NSString *)createZipFile {
 // Init the file manager
 NSFileManager *fileManager = [[NSFileManager alloc] init];
 
 // Get the temp directory path
 NSString *tempDappPath = [self getTempDocumentsPath];
 
 // Get the list of files in the temp directory (do this before creating zip file path)
 NSArray *files = [fileManager subpathsAtPath:tempDappPath];
 
 // Get the zip file path
 NSString *zipFilePath = [NSString stringWithFormat:@"%@%@.zip", 
 [self getTempDocumentsPath], wireframeTitle];
 
 // Init the zip archive
 ZipArchive *zipArchive = [[ZipArchive alloc] init];
 [zipArchive CreateZipFile2:zipFilePath];
 
 // For each file in the folder, add it to the zip file
 for (NSString *file in files) {
 NSString *longPath = [NSString stringWithFormat:@"%@%@",
 [self getTempDocumentsPath], file];
 [zipArchive addFileToZip:longPath newname:file];
 }
 
 BOOL successCompressing = [zipArchive CloseZipFile2];
 
 [fileManager release];
 
 NSString *returnZipFileName = @"";
 
 // If success
 if (successCompressing) {
 NSLog(@"Compressed successfully");
 returnZipFileName = [NSString stringWithFormat:@"%@.zip", wireframeTitle];
 } else {
 NSLog(@"Compression failed");
 }
 
 
 return returnZipFileName;
 }
 
 */

// Return the time stamp string
+ (NSString *)createBackup {
	// Create our time stamp string
	NSTimeInterval currentTimeStamp = [[NSDate date] timeIntervalSince1970];
	NSString *currentTimeStampString = [NSString stringWithFormat:@"%f", currentTimeStamp];
	NSRange decimalRange = [currentTimeStampString rangeOfString:@"."];
	currentTimeStampString = [currentTimeStampString substringToIndex:decimalRange.location];
	
	// Init the file manager
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// Need copy of log file, DB and all images/scribbles
	
	// Get the zip file path
	NSString *zipFilePath = [NSString stringWithFormat:@"%@ManageBackup_%@.zip",
							 [self getBackupPath], currentTimeStampString];
	
	// Init the zip archive
	ZipArchive *zipArchive = [[ZipArchive alloc] init];
	[zipArchive CreateZipFile2:zipFilePath Password:@"0ECCB59F-FE90-4B40-B068-24F857BB475B"];
	
	// Get the list of files (scribbles)
	NSArray *scribbles = [fileManager contentsOfDirectoryAtPath:[MethodHelper getScribblePath] 
														  error:nil];
	
	// First lets get scribbles (and log file)
	for (NSString *scribble in scribbles) {
		// Ignore .pdf file
		NSRange pdfRange = [scribble rangeOfString:@"pdf"];
		NSRange txtRange = [scribble rangeOfString:@"txt"];
		if (pdfRange.length == 0 && txtRange.length == 0) {
			NSString *longPath = [NSString stringWithFormat:@"%@%@",
								  [self getScribblePath], scribble];
			[zipArchive addFileToZip:longPath newname:scribble];
		}
	}
	
	// Add the database
	NSString *dbLongPath = [NSString stringWithFormat:@"%@/%@",
							[self getDatabasePath], @"ManageDB.db"];
	NSString *newDBName = [NSString stringWithFormat:@"ManageDB_%@.db",
						   currentTimeStampString];
	[zipArchive addFileToZip:dbLongPath newname:newDBName];
	
	// Compress
	BOOL successCompressing = [zipArchive CloseZipFile2];
	
//	[zipArchive release];
	
	NSString *returnZipFilePath = @"";
	
	
	
	if (successCompressing) {
		returnZipFilePath = [NSString stringWithFormat:@"%@ManageBackup_%@.zip", 
							 [self getBackupPath], currentTimeStampString];
	} else {
		NSLog(@"Compression failed");
	}

	// Remove any old backups (anything over 25)
	[MethodHelper removeOldBackups];
	
	// Also add ManageDB backup
	// Also add log backup
	return returnZipFilePath;
}

// Adds log file to the backup zip
+ (void)createLogFile:(NSString *)log fromBackupPath:(NSString *)backupPath {
	NSRange endIndex = [backupPath rangeOfString:@".zip"];
	NSString *timeStampString = [backupPath substringToIndex:endIndex.location];
	NSRange startIndex = [timeStampString rangeOfString:@"_"];
	timeStampString = [timeStampString substringFromIndex:startIndex.location];
	timeStampString = [timeStampString stringByReplacingOccurrencesOfString:@"_" 
																 withString:@""];
	
	// Add the log file..
	NSString *logPath = [NSString stringWithFormat:@"%@log_%@.txt",
						 [MethodHelper getLogPath], timeStampString];
	[log writeToFile:logPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

+ (void)createLogFile:(NSString *)log {
	NSString *timeStampString = [MethodHelper getTimeStampStringFromDate:[NSDate date]];
	
	// Add the log file..
	NSString *logPath = [NSString stringWithFormat:@"%@log_%@.txt",
						 [MethodHelper getLogPath], timeStampString];
	[log writeToFile:logPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

// Create database backup
+ (NSString *)createDatabaseBackup {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSString *originalDBPath = [NSString stringWithFormat:@"%@/%@", 
						[MethodHelper getDatabasePath], @"ManageDB.db"];
	
	// Now make a copy of the database, and need to put timestamp on end... ahhh.. timestamp :)
	NSTimeInterval currentTimeStamp = [[NSDate date] timeIntervalSince1970];
	NSString *currentTimeStampString = [NSString stringWithFormat:@"%f", currentTimeStamp];

	// Strip everything after the decimal place
	NSRange decimalRange = [currentTimeStampString rangeOfString:@"."];
	currentTimeStampString = [currentTimeStampString substringToIndex:decimalRange.location];
	
	NSString *newDBPath = [NSString stringWithFormat:@"%@/ManageDB_%@.db", 
						   [MethodHelper getBackupPath], currentTimeStampString];
	
	NSError *error;
	
	BOOL success = [fileManager copyItemAtPath:originalDBPath toPath:newDBPath error:&error];
						
	if (!success) {
		NSLog(@"DB Backup error = %@", error);
	}
	
	return newDBPath;
}

+ (NSString *)getTimeStampStringFromFile:(NSString *)theFile {
	NSRange decimalRange = [theFile rangeOfString:@"."];
	
	if (decimalRange.length == 0) {
		return @"";
	}
	
	theFile = [theFile substringToIndex:decimalRange.location];
	
	NSRange underscoreRange = [theFile rangeOfString:@"_"];
	
	if (underscoreRange.length == 0) {
		return @"";
	}
	
	theFile = [theFile substringFromIndex:underscoreRange.location];
	theFile = [theFile stringByReplacingOccurrencesOfString:@"_" withString:@""];
	
	return theFile;
}

+ (void)restoreBackupData:(NSString *)backupFile backupPath:(NSString *)backupPath {
	// Delete all existing scribbles
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *files = [fileManager contentsOfDirectoryAtPath:[MethodHelper getScribblePath] 
													  error:nil];
	for (NSString *file in files) {
		NSString *filePath = [NSString stringWithFormat:@"%@%@",
							  [MethodHelper getScribblePath], file];
		
		if ([fileManager fileExistsAtPath:filePath]) {
			NSError *deleteFileError = nil;
			[fileManager removeItemAtPath:filePath error:&deleteFileError];
			
			if (deleteFileError) {
				NSLog(@"%@", [deleteFileError localizedDescription]);
			}
		}
	}
	
	// Unzip scribbles and DB to 'Scribble' folder
	ZipArchive *zipArchive = [[ZipArchive alloc] init];
	NSString *zipFile = backupPath;
	[zipArchive UnzipOpenFile:zipFile Password:@"0ECCB59F-FE90-4B40-B068-24F857BB475B"];
	[zipArchive UnzipFileTo:[MethodHelper getScribblePath] overWrite:YES];
	[zipArchive UnzipCloseFile];
//	[zipArchive release];
	
	// Delete the original DB
	NSString *originalDBPath = [NSString stringWithFormat:@"%@/%@", 
								[MethodHelper getDatabasePath], @"ManageDB.db"];
	if ([fileManager fileExistsAtPath:originalDBPath]) {
		NSError *deleteFileError = nil;
		[fileManager removeItemAtPath:originalDBPath error:&deleteFileError];
		
		if (deleteFileError) {
			NSLog(@"%@", [deleteFileError localizedDescription]);
		}
	}
	
	// Rename the backup database
	backupFile = [backupFile stringByReplacingOccurrencesOfString:@".zip" withString:@".db"];
	backupFile = [backupFile stringByReplacingOccurrencesOfString:@"Backup" withString:@"DB"];
	NSString *newDBPath = [NSString stringWithFormat:@"%@%@",
						   [MethodHelper getScribblePath], backupFile];
	
	NSString *renamedDBPath = [NSString stringWithFormat:@"%@%@",
							   [MethodHelper getDatabasePath], @"ManageDB.db"];

	// Move backup database to correct location
	NSError *error = nil;
	BOOL result = [fileManager moveItemAtPath:newDBPath toPath:renamedDBPath error:&error];
	if (!result) {
		NSLog(@"Error: %@", [error description]);
	}
	
	// Delete the old backup path zip file
//	if ([fileManager fileExistsAtPath:backupPath]) {
//		NSError *deleteFileError = nil;
//		[fileManager removeItemAtPath:backupPath error:&deleteFileError];
//		
//		if (deleteFileError) {
//			NSLog(@"%@", [deleteFileError localizedDescription]);
//		}
//	}
}

+ (NSString *)getBackupFileFromBackupPath:(NSString *)backupPath {
	NSString *backupFile = @"";
	
	NSRange backupFileNameRange = [backupPath rangeOfString:@"ManageBackup"];
	NSRange oldBackupFileNameRange = [backupPath rangeOfString:@"ProtaskinoteBackup"];
    
	if (backupFileNameRange.length == 0 && oldBackupFileNameRange.length == 0) {
		NSLog(@"Unable to find ManageBackup in %@", backupPath);
        
		return @"";
	}
	
    if (backupFileNameRange.length > 0) {
        backupFile = [backupPath substringFromIndex:backupFileNameRange.location];
    } else if (oldBackupFileNameRange.length > 0) {
        backupFile = [backupPath substringFromIndex:oldBackupFileNameRange.location];
    }
	
	return backupFile;
}

#pragma mark - String Manipulation Methods

+ (NSString *)concatenatedString:(NSString *)sourceString font:(UIFont *)stringFont maxSize:(CGSize)maxSize lineBreakMode:(NSLineBreakMode)lineBreakMode {
    NSString *originalString = [sourceString copy];//autorelease
    NSString *newString = [originalString copy];//autorelease
    
    CGSize newSize = [originalString sizeWithFont:stringFont constrainedToSize:CGSizeMake(maxSize.width, 600) lineBreakMode:lineBreakMode];
    
    while (newSize.height > maxSize.height) {
        // Update the new string (take 3 letters away from other string... no..)
        newString = [newString substringToIndex:([newString length] - 1)];
        
        newSize = [newString sizeWithFont:stringFont constrainedToSize:CGSizeMake(maxSize.width, 600) lineBreakMode:lineBreakMode];
    }
    
    return newString;
}

// Write a function that will incrementally take away the last character until it fits. Then
// Return our leftover string, print this on a new page and do the test again
// Do it in binary form, so that we take away half and so forth
/*
 CGSize notesSize = [notesString sizeWithFont:notesFont
 constrainedToSize:CGSizeMake(taskItemFrame.size.width, thisMaxRowHeight) lineBreakMode:UILineBreakModeWordWrap];
 
 // Make sure the notes don't go past the end of the page, if they do, then we need to split them up
 NSLog(@"notesize.height: %f + taskListhHeight: %d - totalTaskSectionHeight: %d",
 notesSize.height, self.m_totalHeight, kTotalTaskSectionHeight);
 if (notesSize.height + self.m_totalHeight >= kTotalTaskSectionHeight) {
 notesSize.height = kTotalTaskSectionHeight - self.m_totalHeight;
 
 // Write a function that will incrementally take away the last character until it fits (-3 for truncation dots). Then
 // Return our leftover string, print this on a new page and do the test again
 // Do it in binary form, so that we take away half and so forth
 
 }
 
 
 */

#pragma mark -
#pragma mark Class Helper Methods

+ (CGFloat)getIOSVersion {
    CGFloat iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    return iOSVersion;
}

+ (void)reportMemory {
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                   TASK_BASIC_INFO,
                                   (task_info_t)&info,
                                   &size);
    if( kerr == KERN_SUCCESS ) {
        NSLog(@"Memory in use (in bytes): %u", info.resident_size);
    } else {
        NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
    }
}

+ (NSData *)encodeObject:(NSDictionary *)dicData withKey:(NSString *)key {
    NSMutableDictionary *mutableDicData = [[NSMutableDictionary alloc] init];
    [mutableDicData setObject:dicData forKey:key];
    

    NSData *objectData = [NSKeyedArchiver archivedDataWithRootObject:mutableDicData];
//    [mutableDicData release];
    return objectData;
}

+ (NSDictionary *)decodeObject:(NSData *)objectData andKey:(NSString *)key {
    //NSMutableDictionary *mutableDicData;
    id decodedObject;
    @try {
        decodedObject = [NSKeyedUnarchiver unarchiveObjectWithData:objectData];
    }
    @catch (NSException *exception) {
        decodedObject = nil;
    }
    @finally {
    }
    
    if (decodedObject == nil) {
        return nil;
    } 
                
    NSDictionary *dic = (NSMutableDictionary *)[decodedObject objectForKey:key];
    
    return dic;
}

// Creates a copy of the passed scribble and returns the name of the copy that was created
+ (NSString *)getCopyOfScribble:(NSString *)originalScribble {
	NSString *fileSource = [[self getScribblePath] stringByAppendingPathComponent:originalScribble];
	
	NSString *newScribble = [self getUniqueFileName];
    
    NSString *pngScribble = @"";
    NSString *originalPngScribble = @"";
    
    NSRange glsRange = [originalScribble rangeOfString:@".gls"];

    if (glsRange.location == NSNotFound) {
        newScribble = [NSString stringWithFormat:@"%@.png", newScribble];
    } else {
        pngScribble = [NSString stringWithFormat:@"%@.png", newScribble];
        newScribble = [NSString stringWithFormat:@"%@.gls", newScribble];
        
        originalPngScribble = [originalScribble substringToIndex:glsRange.location];
        originalPngScribble = [NSString stringWithFormat:@"%@.png", originalPngScribble];
        
        //pngFileSource = [[self getScribblePath] stringByAppendingPathComponent:pngScribble];
    }
//	[[newScribble retain] autorelease];
	NSString *fileDestination = [[self getScribblePath] stringByAppendingPathComponent:newScribble];
	
	// Create file manager object
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// Check if the copy operation is successful
	if ([fileManager copyItemAtPath:fileSource toPath:fileDestination error:nil] == TRUE) {
        if ([pngScribble length] > 0) {
            fileDestination = [[self getScribblePath] stringByAppendingFormat:@"%@",pngScribble];
            fileSource = [[self getScribblePath] stringByAppendingPathComponent:originalPngScribble];
            
            [fileManager copyItemAtPath:fileSource toPath:fileDestination error:nil];
        }
        
		return newScribble;
	} 

	
	
	return @"";
}

+ (NSString *)getPngScribblePathFromGLSScribble:(NSString *)originalScribble {
    NSRange glsRange = [originalScribble rangeOfString:@".gls"];
    NSString *pngScribble = @"";
    
    if (glsRange.length > 0) {
        pngScribble = [originalScribble substringToIndex:glsRange.location];
        pngScribble = [NSString stringWithFormat:@"%@.png", pngScribble];
    } else {
        return @"";
    }
    
    NSString *scribblePath = [NSString stringWithFormat:@"%@%@",
                              [MethodHelper getScribblePath],
                              pngScribble];
    
    return scribblePath;
}

+ (CGFloat)distanceBetween:(CGPoint)point1 and:(CGPoint)point2 {
	CGFloat d1 = point2.x - point1.x;
	d1 = d1 * d1;
	
	CGFloat d2 = point2.y - point1.y;
	d2 = d2 * d2;
	
	return sqrt(d1 + d2);
}

+ (NSString *)getDatabasePath {
	NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *databasePath = [searchPaths objectAtIndex: 0];
	databasePath = [NSString stringWithFormat:@"%@/Manage/", databasePath];
	
	return databasePath;
}

+ (NSString *)getHomePath {
	NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *homePath = [searchPaths objectAtIndex: 0];
	homePath = [NSString stringWithFormat:@"%@/", homePath];
	
	return homePath;
}

+ (NSString *)getBackupPath {
	NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *backupPath = [searchPaths objectAtIndex: 0];
	backupPath = [NSString stringWithFormat:@"%@/Backup/", backupPath];
	
	return backupPath;
}

+ (NSString *)getScribblePath {
	NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *newDocumentsPath = [searchPaths objectAtIndex: 0];
	newDocumentsPath = [NSString stringWithFormat:@"%@/Scribble/", newDocumentsPath];
	
	return newDocumentsPath;
}

+ (NSString *)getLogPath {
	NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *newDocumentsPath = [searchPaths objectAtIndex: 0];
	newDocumentsPath = [NSString stringWithFormat:@"%@/Log/", newDocumentsPath];
	
	return newDocumentsPath;
}

+ (NSString *)getUniqueGUID {
	// Add new UUID file name to documents path
	CFUUIDRef newCFUUID = CFUUIDCreate(kCFAllocatorDefault);
	
	// create a new CFStringRef (toll-free bridged to NSString)
	// that you own
	NSString *uniqueUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, newCFUUID));
	
	// Transfer ownership of the string to the autorelease pool
//	[uniqueUUID autorelease];
	
	// Release the newCFUUID
	CFRelease(newCFUUID);
	
	return uniqueUUID;
}

+ (NSString *)getUniqueFileName {
	// Add new UUID file name to documents path
	CFUUIDRef newCFUUID = CFUUIDCreate(kCFAllocatorDefault);
	
	// create a new CFStringRef (toll-free bridged to NSString)
	// that you own
	NSString *scribbleUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, newCFUUID));
	
	// Transfer ownership of the string to the autorelease pool
//	[scribbleUUID autorelease];
	
	// Release the newCFUUID
	CFRelease(newCFUUID);
	
	return scribbleUUID;
}

+ (NSInteger)numberOfListItemsForBadgeNotification {
	// Need to see what type of badge notification setting has been placed
	NSString *badgeSettingData = [BLApplicationSetting getSettingData:@"BadgeNotification"];
	
	if ([badgeSettingData isEqualToString:@"None"]) {
		return 0;
	}
	
	NSArray *localArray = [BLListItem getDateDifferenceForAllListItemsWithDueDate];
	
	NSInteger dueCount = 0;
	NSInteger overdueCount = 0;
	
	for (NSDictionary *dataDictionary in localArray) {
		NSInteger dateDifference = [DMHelper getNSIntegerValueForKey:@"DateDifference" 
													  fromDictionary:dataDictionary];
		
		// Greater than zero means overdue
		if (dateDifference > 0) {
			overdueCount++;
			dueCount++;
		} else {
			dueCount++;
		}
	}
	
	if ([badgeSettingData isEqualToString:@"Due and Overdue Tasks"]) {
		return dueCount;
	} else if ([badgeSettingData isEqualToString:@"Overdue Tasks"]) {
		return overdueCount;
	}
	
	return 0;
}


// Show an alert view with specified parameters
+ (void)showAlertViewWithTitle:(NSString *)title andMessage:(NSString *)message andButtonTitle:(NSString *)buttonTitle {
	// Duplicate found, warn user and exit this method
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: title
														message: message
													   delegate: nil
											  cancelButtonTitle: buttonTitle
											  otherButtonTitles: nil];
	[alertView show];
//	[alertView release];
}

+ (NSString *)replaceHttpSpecialCharactersForString:(NSString *)theString {
	theString = [theString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
	theString = [theString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
	theString = [theString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
	theString = [theString stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
	theString = [theString stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
	
	return theString;
}

+ (BOOL)stringObject:(NSString *)theString existsInArray:(NSArray *)theArray {
	for (NSString *localString in theArray) {
		if ([localString isEqualToString:theString]) {
			return TRUE;
		}
	}
	return FALSE;
}

+ (BOOL)stringObject:(NSString *)theString existsInToodledoTagArray:(NSArray *)theArray {
	for (NSString *localString in theArray) {
        NSString *aLocalString = [localString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		
		if ([aLocalString isEqualToString:theString]) {
			return TRUE;
		}
	}
	return FALSE;
}

// Gets the time stamp string (generally for use with toodledo) from date
+ (NSString *)getTimeStampStringFromDate:(NSDate *)theDate {
	// Need to convert the date to UTC time first?
	// The date in your source timezone (eg. EST)
	NSDate *destinationDate = [MethodHelper convertDateToUTCTimezone:theDate forDestinationZone:@"Toodledo"];
	
	NSTimeInterval timeInterval = [destinationDate timeIntervalSince1970];
	
	// Need to time interval for current region and add/minus it
	
	NSString *timeStampString = [NSString stringWithFormat:@"%f", timeInterval];
	
	NSRange decimalRange = [timeStampString rangeOfString:@"."];
	if (decimalRange.length == 0) {
		return timeStampString;
	} else {
		timeStampString = [timeStampString substringToIndex:decimalRange.location];
	}

	return timeStampString;
}

+ (NSDate *)convertDateToUTCTimezone:(NSDate *)sourceDate forDestinationZone:(NSString *)theZone {
	NSTimeZone *sourceTimeZone = [NSTimeZone systemTimeZone];
	NSTimeZone *destinationTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	
	NSTimeInterval interval = 0;
	if ([theZone isEqualToString:@"Toodledo"]) {
		interval = sourceGMTOffset - destinationGMTOffset;
	} else if ([theZone isEqualToString:@"Local"]) {
		interval = destinationGMTOffset - sourceGMTOffset;
	}
	
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];//autorelease

	return destinationDate;
}

+ (BOOL)listItemIsExistInArray:(NSMutableArray *)array withListItem:(ListItem *)listItem {
    
    for (NSInteger i=0; i < [array count]; i++) {
        if ([array objectAtIndex:i] == listItem) {
            return  YES;
        }
    }
    
    return NO;
}

+ (BOOL)isOlderThanToDayOneMonthWithDayString:(NSString *) dayString {
    
    NSDate *currentDate = [self dateFromString:dayString usingFormat:K_DATETIME_FORMAT];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [NSDateComponents new];
    comps.month = -1;
    NSDate *lastMonthDate = [calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
//    [comps release];
    
    if ([currentDate compare:lastMonthDate] == NSOrderedAscending) {
        return YES;
    }
    
    return NO;
}

+ (GADBannerView *)sharedAdsBanner {
    MAppDelegate *app = [[UIApplication sharedApplication] delegate];
    return app.adBanner;
}

+ (BOOL)connectedToInternet{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

#pragma mark BuzzCity
+ (NSString *)ODIN1{
    // Step 1: Get MAC address
    int mib[6];
    size_t len;
    char *buf;
    unsigned char *ptr;
    struct if_msghdr *ifm;
    struct sockaddr_dl *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        return nil;
    }
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        return nil;
    }
    if ((buf = malloc(len)) == NULL) {
        return nil;
    }
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        return nil;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)ether_ntoa((const struct ether_addr *)LLADDR(sdl));
    
    // Step 2: Take the SHA-1 of the MAC address
    CFDataRef data = CFDataCreate(NULL, (uint8_t*)ptr, 6);
    unsigned char messageDigest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(CFDataGetBytePtr((CFDataRef)data), CFDataGetLength((CFDataRef)data), messageDigest);
    CFMutableStringRef string = CFStringCreateMutable(NULL, 40);
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        CFStringAppendFormat(string, NULL, (CFStringRef)@"%02X", messageDigest[i]);
    }
    CFStringLowercase(string, CFLocaleGetSystem());
    free(buf);
    NSString *odinstring = [[NSString alloc] initWithString:(__bridge  NSString*)string];
    CFRelease(data);
    CFRelease(string);
    return odinstring;
}


@end
