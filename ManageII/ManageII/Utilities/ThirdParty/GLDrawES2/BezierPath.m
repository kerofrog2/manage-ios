//
//  BezierPath.m
//  
//
//  Created by Adam Lockhart on 7/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BezierPath.h"

@implementation BezierPath

@synthesize controlPoints;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
        controlPoints = [[NSMutableArray alloc] init];
    }
    
    return self;
}


-(void) dealloc {
    [controlPoints release];
    [super dealloc];
}

-(void) interpolate: (NSMutableArray*) segmentPoints withScale: (NSInteger) scale {
    [controlPoints removeAllObjects];
    if ([segmentPoints count] < 2) {
        return;
    }
    
    for (int i = 0; i < [segmentPoints count]; i++)
    {
        if (i == 0) // is first
        {
            Vector3 *p1 = [segmentPoints objectAtIndex:i];
            Vector3 *p2 = [segmentPoints objectAtIndex:i + 1];                
            
            Vector3 *tangent = [p2 minus: p1];
            Vector3 *q1 = [p1 plus: [tangent scaled:scale]];
            
            [controlPoints addObject: p1];
            [controlPoints addObject: q1];
        }
        else if (i == [segmentPoints count] - 1) //last
        {
            Vector3 *p0 = [segmentPoints objectAtIndex:i - 1]; 
            Vector3 *p1 = [segmentPoints objectAtIndex:i];
            Vector3 *tangent = [p1 minus: p0];
            Vector3 *q0 = [p1 minus: [tangent scaled:scale]];
            
            [controlPoints addObject: q0];
            [controlPoints addObject: p1 ];
        }
        else
        {
            Vector3 *p0 = [segmentPoints objectAtIndex:i - 1];
            Vector3 *p1 = [segmentPoints objectAtIndex:i];
            Vector3 *p2 = [segmentPoints objectAtIndex:i + 1];
            Vector3 *tangent = [[p2 minus: p0] normalized];
            Vector3 *q0 = [p1 minus: [[tangent scaled:scale] scaled: ((Vector3*)[p1 minus: p0]).magnitude]];
            Vector3 *q1 = [p1 plus: [[tangent scaled:scale] scaled: ((Vector3*)[p2 minus: p1]).magnitude]];
            
            [controlPoints addObject: q0];
            [controlPoints addObject: p1];
            [controlPoints addObject: q1];
        }
    }
    
    curveCount = ([controlPoints count] - 1) / 3;
}
-(void) samplePoints: (NSMutableArray*) sourcePoints andMinSqrDistance: (float) minSqrDistance andMaxSqrDistance: (float) maxSqrDistance andScale: (float) scale {
    if([sourcePoints count] < 2)
    {
        return;
    }
    
    NSMutableArray *samplePoints =[[[NSMutableArray alloc] init] autorelease];
    
    [samplePoints addObject: [sourcePoints objectAtIndex: 0]];
    
    Vector3 *potentialSamplePoint = [sourcePoints objectAtIndex: 1];
    
    for (int i = 2; i < [sourcePoints count]; i++ )
    {
        if(
           ([[potentialSamplePoint minus: [sourcePoints objectAtIndex: i]] sqrMagnitude] > minSqrDistance) &&
           ([[[samplePoints lastObject] minus: [sourcePoints objectAtIndex: i]] sqrMagnitude] > maxSqrDistance))
        {
            [samplePoints addObject: potentialSamplePoint];
        }
        
        potentialSamplePoint = [sourcePoints objectAtIndex: i];
    }
    
    //now handle last bit of curve
    Vector3 *p1 = [samplePoints lastObject]; //last sample point
    [samplePoints removeLastObject];
    Vector3 *p0 = [samplePoints lastObject]; //second last sample point
    Vector3 *tangent = [p0 minus: [potentialSamplePoint normalized]];
    float d2 = [potentialSamplePoint minus: p1].magnitude;
    float d1 = [p1 minus: p0].magnitude;
    p1 = [[p1 plus: tangent] scaled: ((d1 - d2)/2)];
    
    [samplePoints addObject: p1];
    [samplePoints addObject: potentialSamplePoint];
    
    
    [self interpolate: [NSMutableArray arrayWithArray: samplePoints] withScale: scale];
}

-(Vector3*) calculateBezierPoint: (int) curveIndex : (float) t {
    int nodeIndex = curveIndex * 3;
    
    Vector3 *p0 = [controlPoints objectAtIndex: nodeIndex];
    Vector3 *p1 = [controlPoints objectAtIndex: nodeIndex + 1];
    Vector3 *p2 = [controlPoints objectAtIndex: nodeIndex + 2];
    Vector3 *p3 = [controlPoints objectAtIndex: nodeIndex + 3];
    
    return [self calculateBezierPoint2:t andp0:p0 andp1:p1 andp2:p2 andp3:p3];
}

-(NSMutableArray*) getDrawingPoints0 {
    NSMutableArray *drawingPoints = [NSMutableArray array];
    
    for (int curveIndex = 0; curveIndex < curveCount; curveIndex++)
    {
        if (curveIndex == 0) //Only do this for the first end point. 
            //When i != 0, this coincides with the 
            //end point of the previous segment,
        {
            [drawingPoints addObject: [self calculateBezierPoint: curveIndex: 0]];
        }
        
        for (int j = 1; j <= SEGMENTS_PER_CURVE; j++)
        {
            float t = j / (float)SEGMENTS_PER_CURVE;
            [drawingPoints addObject: [self calculateBezierPoint: curveIndex: t]];
        }
    }
    
    return drawingPoints;
}

-(NSMutableArray*) getDrawingPoints1 {
    NSMutableArray *drawingPoints = [NSMutableArray array];
    for (int i = 0; i < [controlPoints count] - 3; i += 3)
    {
        Vector3 *p0 = [controlPoints objectAtIndex: i];
        Vector3 *p1 = [controlPoints objectAtIndex: i + 1];
        Vector3 *p2 = [controlPoints objectAtIndex: i + 2];
        Vector3 *p3 = [controlPoints objectAtIndex: i + 3];
        
        if (i == 0) //only do this for the first end point. When i != 0, this coincides with the end point of the previous segment,
        {
            [drawingPoints addObject:[self calculateBezierPoint2:0 andp0:p0 andp1:p1 andp2:p2 andp3:p3]];
        }
        
        for (int j = 1; j <= SEGMENTS_PER_CURVE; j++)
        {
            float t = j / (float)SEGMENTS_PER_CURVE;
            [drawingPoints addObject:[self calculateBezierPoint2:t andp0:p0 andp1:p1 andp2:p2 andp3:p3]];
        }
    }
    return drawingPoints;
}

-(NSMutableArray*) getDrawingPoints2 {
    NSMutableArray *drawingPoints = [NSMutableArray array];
    for (int curveIndex = 0; curveIndex < curveCount; curveIndex++)
    {
        NSMutableArray *bezierCurveDrawingPoints = [self findDrawingPoints: curveIndex];
        
        if (curveIndex != 0)
        {
            //remove the fist point, as it coincides with the last point of the previous Bezier curve.
            [bezierCurveDrawingPoints removeObjectAtIndex: 0];
        }
        
        [drawingPoints addObjectsFromArray: bezierCurveDrawingPoints];
    }
    return drawingPoints;
}

-(NSMutableArray*) findDrawingPoints: (int) curveIndex {
    NSMutableArray* pointList = [NSMutableArray array];
    
    Vector3 *left = [self calculateBezierPoint: curveIndex: 0];
    Vector3 *right = [self calculateBezierPoint: curveIndex: 1];
    
    [pointList addObject: left];
    [pointList addObject: right];
    
    [self findDrawingPoints2:curveIndex andt0:0 andt1:1 andPointList:pointList andInsertionIndex:1];
    
    return pointList;
}

-(int) findDrawingPoints2: (int) curveIndex andt0: (float) t0 andt1: (float) t1 andPointList: (NSMutableArray*) pointList andInsertionIndex: (int) insertionIndex {
    Vector3 *left = [self calculateBezierPoint:curveIndex: t0];
    Vector3 *right = [self calculateBezierPoint:curveIndex: t1];
    
    if ([left minus: right].sqrMagnitude < MINIMUM_SQR_DISTANCE)
    {
        return 0;
    }
    
    float tMid = (t0 + t1) / 2;
    Vector3 *mid = [self calculateBezierPoint:curveIndex: tMid];
    
    Vector3 *leftDirection = [[left minus: mid] normalized];
    Vector3 *rightDirection = [[right minus: mid] normalized];
    
    if ([leftDirection dot: rightDirection] > DIVISION_THRESHOLD || ABS(tMid - 0.5f) < 0.0001f)
    {
        int pointsAddedCount = 0;
        
        pointsAddedCount += [self findDrawingPoints2:curveIndex andt0:t0 andt1:tMid andPointList:pointList andInsertionIndex:insertionIndex];
        [pointList insertObject:mid atIndex: insertionIndex + pointsAddedCount ];
        pointsAddedCount++;
        
        pointsAddedCount += [self findDrawingPoints2:curveIndex andt0:tMid andt1:t1 andPointList:pointList andInsertionIndex:insertionIndex + pointsAddedCount];
        
        return pointsAddedCount;
    }
    
    return 0;
}

-(Vector3*) calculateBezierPoint2:(float)t andp0:(Vector3*)p0 andp1:(Vector3*)p1 andp2:(Vector3*)p2 andp3:(Vector3*)p3 {
    float u = 1 - t;
    float tt = t * t;
    float uu = u * u;
    float uuu = uu * u;
    float ttt = tt * t;
    
    Vector3 *p = [p0 scaled:uuu]; //first term
    
    p = [p plus: [p1 scaled: 3*uu*t]]; //second term
    p = [p plus: [p2 scaled: 3*u*tt]]; //third term
    p = [p plus: [p3 scaled: ttt]]; //fourth term
    
    return p;
}


@end
