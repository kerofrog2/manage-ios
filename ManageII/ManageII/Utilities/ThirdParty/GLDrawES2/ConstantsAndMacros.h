//
//  ConstantsAndMacros.h
//

#define DATABASE_VERSION_2_1  @"2.1.1"

#define MAX_LINE_WIDTH 2.f
#define MIN_LINE_WIDTH 1.f

#define kReloadTableAnimationTime 2.5f
#define kFadeOutTime 7.0f

#define kNeedUpdateDatebaseForArchiveTaskList @"NeedUpdateDatebaseForArchiveTaskList"

// buffer settings
#define OUTLIER_THRESHOLD 150

// smallest unit to be made into vector
#define MIN_MAGNITUDE_THRESHOLD 1.f

// bezier sttings
#define BEZIER_THRESHOLD 10f
#define BEZIER_MIN_ANGLE 0.125f
#define BEZIER_MAX_ANGLE 1.57f

//  2pi/20 = circle divided into 20 pieces
#define PI 3.1415927410125732421875f;

#define AVG(X, Y) (((X)+(Y))/2)
#define _ABS(X) ((X)<0?-(X):(X))
#define SQUARED(X) ((X)*(X))
#define CUBED(X) ((X)*(X)*(X))

enum {
    BLACK,
    BLUE,
    RED,
    GREEN
};

