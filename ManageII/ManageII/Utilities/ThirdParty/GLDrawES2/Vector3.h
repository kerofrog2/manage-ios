//
//  Vector3.h
//  GLDrawES2
//
//  Created by Adam Lockhart on 7/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vector3 : NSObject {
    float x,y,z, magnitude;
}

@property (assign) float x;
@property (assign) float z;
@property (assign) float y;
@property (assign) float magnitude;

+(Vector3*) vector;

-(Vector3*) minus: (Vector3*) operand;
-(Vector3*) minusf: (float) operand;
-(Vector3*) plus: (Vector3*) operand;
-(Vector3*) scaled: (float) operand;
-(Vector3*) normalized;
-(float) sqrMagnitude;
-(float) dot: (Vector3*) v;


-(void) setxyz: (float)xx andY:(float)yy andZ:(float)zz;

@end
