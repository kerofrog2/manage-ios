//
//  ToodledoAccountInfo.h
//  Manage
//
//  Created by Cliff Viegas on 11/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ToodledoAccountInfoDelegate
- (void)accountInfoSucceeded;
- (void)accountInfoFailedWithErrorCode:(NSInteger)errorCode andErrorDescription:(NSString *)errorDesc;
- (void)accountInfoConnectionFailedWithError:(NSError *)error;
@end


@interface ToodledoAccountInfo : NSObject {
//	id	<ToodledoAccountInfoDelegate>	delegate;
	
	// The connection
	NSURLConnection						*toodledoConnection;
	
	NSString							*toodledoKey;
	NSMutableData						*responseData;
	
	NSString							*userID;
	NSString							*alias;
	BOOL								pro;
	NSInteger							dateFormat;
	NSInteger							timeZone;
	NSInteger							hideMonths;
	NSInteger							hotListPriority;
	NSInteger							hotListDueDate;
	BOOL								hotListStar;
	BOOL								hotListStatus;
	BOOL								showTabNums;
	NSString							*lastEditFolder;
	NSString							*lastEditContext;
	NSString							*lastEditGoal;
	NSString							*lastEditLocation;
	NSString							*lastEditTask;
	NSString							*lastDeleteTask;
	NSString							*lastEditNotebook;
	NSString							*lastDeleteNotebook;
}

//@property	(nonatomic, assign)		id			delegate;
@property	(nonatomic, assign)		NSObject<ToodledoAccountInfoDelegate>			*delegate;

@property	(nonatomic, copy)		NSString	*toodledoKey;

@property	(nonatomic, copy)		NSString	*userID;
@property	(nonatomic, copy)		NSString	*alias;
@property	(nonatomic, assign)		BOOL		pro;
@property	(nonatomic, assign)		NSInteger	dateFormat;
@property	(nonatomic, assign)		NSInteger	timeZone;
@property	(nonatomic, assign)		NSInteger	hideMonths;
@property	(nonatomic, assign)		NSInteger	hotListPriority;
@property	(nonatomic, assign)		NSInteger	hotListDueDate;
@property	(nonatomic, assign)		BOOL		hotListStar;
@property	(nonatomic, assign)		BOOL		hotListStatus;
@property	(nonatomic, assign)		BOOL		showTabNums;
@property	(nonatomic, copy)		NSString	*lastEditFolder;
@property	(nonatomic, copy)		NSString	*lastEditContext;
@property	(nonatomic, copy)		NSString	*lastEditGoal;
@property	(nonatomic, copy)		NSString	*lastEditLocation;
@property	(nonatomic, copy)		NSString	*lastEditTask;
@property	(nonatomic, copy)		NSString	*lastDeleteTask;
@property	(nonatomic, copy)		NSString	*lastEditNotebook;
@property	(nonatomic, copy)		NSString	*lastDeleteNotebook;

#pragma mark Initialisation
- (id)initWithDelegate:(NSObject<ToodledoAccountInfoDelegate>*)theDelegate andKey:(NSString *)theKey;

#pragma mark Public Connection Methods
- (void)connectToAccount;
- (void)cancelConnection;

@end
