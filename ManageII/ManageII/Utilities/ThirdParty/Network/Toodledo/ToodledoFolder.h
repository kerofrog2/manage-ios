//
//  ToodledoFolder.h
//  Manage
//
//  Created by Cliff Viegas on 13/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	EnumToodledoFolderActionNone = 0,
	EnumToodledoFolderActionUpdate = 1,
	EnumToodledoFolderActionDelete = 2,
	EnumToodledoFolderActionAdd = 3
} EnumToodledoFolderAction;

@protocol ToodledoFolderDelegate
- (void)folderActionSuccessful:(id)sender;
- (void)folderActionConnectionFailedWithError:(NSError *)error;
- (void)folderActionFailedWithErrorCode:(NSInteger)errorCode andErrorDescription:(NSString *)errorDesc;
@end


@interface ToodledoFolder : NSObject {
//	id <ToodledoFolderDelegate>		delegate;
	
	NSMutableData					*responseData;
	
	NSInteger						folderID;
	NSString						*name;
	BOOL							folderPrivate;
	BOOL							archived;
	NSInteger						order;
	
	// Used for tracking what action was performed on the folder
	NSInteger						actionType;	// deprecated

}

//@property	(nonatomic, assign)		id				delegate;

@property	(nonatomic, assign)		NSInteger		folderID;
@property	(nonatomic, copy)		NSString		*name;
@property	(nonatomic, assign)		BOOL			folderPrivate;
@property	(nonatomic, assign)		BOOL			archived;
@property	(nonatomic, assign)		NSInteger		order;

@property	(nonatomic, assign)		NSInteger		actionType;
@property	(nonatomic, assign)		NSObject<ToodledoFolderDelegate>				*delegate;


#pragma mark Initialisation
- (id)initWithName:(NSString *)theName andID:(NSInteger)theFolderID andPrivate:(BOOL)isPrivate andArchived:(BOOL)isArchived andOrder:(NSInteger)theOrder;

#pragma mark Action Methods
- (BOOL)addFolderWithName:(NSString *)folderName usingKey:(NSString *)key responseString:(NSString **)responseString error:(NSError **)error;
- (NSString *)deleteFolderWithID:(NSInteger)theFolderID andOriginalName:(NSString *)originalName usingKey:(NSString *)key;
- (NSString *)updateFolderUsingKey:(NSString *)key andOldName:(NSString *)originalName;

@end
