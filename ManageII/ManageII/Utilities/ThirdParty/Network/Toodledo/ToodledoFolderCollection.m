//
//  ToodledoFolders.m
//  Manage
//
//  Created by Cliff Viegas on 13/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ToodledoFolderCollection.h"

// Data Models
#import "ToodledoFolder.h"

// JSON
#import	"JSON.h"

// DMHelper
#import "DMHelper.h"

// Private methods
@interface ToodledoFolderCollection()
- (void)loadFoldersUsingArray:(NSArray *)responseArray;
@end


@implementation ToodledoFolderCollection

@synthesize folders;
@synthesize toodledoKey;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[self.folders release];
//	//[self.toodledoKey release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		// Init the folders array
		self.folders = [NSMutableArray array];
		self.toodledoKey = @"";
	}
	return self;
}

- (id)initUsingKey:(NSString *)theToodledoKey {
	if (self = [super init]) {
		// Assign the passed key
		self.toodledoKey = theToodledoKey;
		
		// Init the folders array
		self.folders = [NSMutableArray array];

		// Init toodledo connection to nil
		toodledoConnection = nil;
	}
	return self;
}

#pragma mark -
#pragma mark Public Methods

- (NSString *)collectFoldersFromServer {
	NSError *error = nil;
	
	// Get the json data.. need to print this first to see what data is returned, possibly an array
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/folders/get.php?key=%@",
							self.toodledoKey];
	
	//responseData = [[NSMutableData data] retain];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	
	// Get the data
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error ];
	NSString *jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
//	[jsonString release];
//	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
	NSString *responseString = @"";
	if (error) {
		// NSError
		responseString = [error description];
	} else if (responseDictionary != nil) {
		if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
			responseString = [NSString stringWithFormat:@"Toodledo Error: %@",
							  [responseDictionary valueForKey:@"errorDesc"]];
		} else {
			responseString = @"Toodledo Error: Unknown issue occured";
		}

	} else {
		// Update self with dictionary
		responseString = @"Toodledo Server: Successfully collected folders";
		
		[self loadFoldersUsingArray:responseArray];
	}
	
	
	
	return responseString;
}

- (void)cancelConnection {
	if (toodledoConnection) {
		NSLog(@"ToodledoFolderCollection connection cancelled");
		[toodledoConnection cancel];
	}
}

#pragma mark -
#pragma mark Delete Methods

- (void)deleteAllFoldersUsingResponseString:(NSMutableString **)mutableResponseString {
	if ([self.folders count] == 0) {
		[*mutableResponseString setString:@"Toodledo Server: No folders found\n"];
		return;
	}
	
	for (ToodledoFolder *folder in self.folders) {
		NSString *returnValue = [folder deleteFolderWithID:folder.folderID 
										   andOriginalName:folder.name
												  usingKey:self.toodledoKey];
		
		if ([returnValue length] > 10) {
			[*mutableResponseString appendString:[NSString stringWithFormat:@"%@\n", 
												  returnValue]];
		} else {
			[*mutableResponseString appendString:[NSString stringWithFormat:@"Toodledo Server: Folder (%@) deleted\n",
												  returnValue]];
		}

	}
	
	
}

#pragma mark -
#pragma mark Private Methods

- (void)loadFoldersUsingArray:(NSArray *)responseArray {
	// Clear any existing folders in array first
	[self.folders removeAllObjects];

	// Create new folder for every dictionary in the array
	for (NSDictionary *folderDictionary in responseArray) {
		ToodledoFolder *folder = [[ToodledoFolder alloc] init];// autorelease];
		
		folder.folderID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:folderDictionary];
		folder.name = [DMHelper getNSStringValueForKey:@"name" fromDictionary:folderDictionary];
		folder.folderPrivate = [DMHelper getBoolValueForKey:@"private" fromDictionary:folderDictionary];
		folder.archived = [DMHelper getBoolValueForKey:@"archived" fromDictionary:folderDictionary];
		folder.order = [DMHelper getNSIntegerValueForKey:@"ord" fromDictionary:folderDictionary];
		
		if ([folder archived] == FALSE) {
			// Only add the folder if it was archived
			[self.folders addObject:folder];
		}
		
	}
	
	// Notify the delegate that we have finished loading the folders
	//[self.delegate finishedCollectingFolders];
}

#pragma mark -
#pragma mark Get Methods

- (NSInteger)getIDOfFolderWithName:(NSString *)folderName {
	NSInteger folderID = 0;
	
	for (ToodledoFolder *folder in self.folders) {
		if ([folder.name isEqualToString:folderName]) {
			folderID = folder.folderID;
			return folderID;
		}
	}
	
	return folderID;
}

- (NSString *)getNameOfFolderWithID:(NSInteger)theFolderID {
	NSString *folderName = @"";
	
	for (ToodledoFolder *folder in self.folders) {
		if (folder.folderID == theFolderID) {
			folderName = folder.name;
		}
	}
	
	return folderName;
}

// Check if folder with name exists
- (BOOL)doesFolderExistWithName:(NSString *)folderName {
	for (ToodledoFolder *folder in self.folders) {
		if ([folder.name isEqualToString:folderName]) {
			return YES;
		}
	}
	return NO;
}

// Check if folder with ID exists
- (BOOL)doesFolderExistWithID:(NSInteger)theFolderID {
	for (ToodledoFolder *folder in self.folders) {
		if (folder.folderID == theFolderID) {
			return YES;
		}
	}
	return NO;
}

#pragma mark -
#pragma mark Test Methods

- (void)testDisplayAllFolders {
	NSLog(@"\n= TOODLEDO FOLDERS =");
	for (ToodledoFolder *folder in self.folders) {
		NSLog(@"FolderID: %d | Name: %@", folder.folderID, folder.name);
	}
	NSLog(@"= END =\n");
}


@end
