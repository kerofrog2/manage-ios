//
//  ToodledoSync.h
//  Manage
//
//  Created by Cliff Viegas on 9/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ToodledoAuthenticationDelegate
- (void)authenticationSucceeded;
- (void)authenticationFailedWithErrorCode:(NSInteger)errorCode andErrorDescription:(NSString *)errorDesc;
- (void)authenticationConnectionFailedWithError:(NSError *)error;
@end

@interface ToodledoAuthentication : NSObject {
//	id	<ToodledoAuthenticationDelegate>	delegate;
	
	NSMutableData							*responseData;
	NSString								*tdDetail;
	
	NSURLConnection							*toodledoConnection;
}

//@property (nonatomic, assign)	id			delegate;
@property (nonatomic, copy)		NSString	*tdDetail;
@property (nonatomic, assign)	NSObject<ToodledoAuthenticationDelegate>			*delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(NSObject<ToodledoAuthenticationDelegate>*)theDelegate;

#pragma mark Public methods
- (void)authenticateWithAccount:(NSString *)account andDetail:(NSString *)detail;
- (void)authenticateWithUserID:(NSString *)userID;
- (void)assignDetail;
- (void)cancelConnection;

@end
