//
//  ToodledoFolder.m
//  Manage
//
//  Created by Cliff Viegas on 13/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ToodledoFolder.h"

// JSON
#import	"JSON.h"

// Method Helper
#import "MethodHelper.h"

// DMHelper
#import "DMHelper.h"

@interface ToodledoFolder()
// Class Methods
- (void)updateSelfWithDictionary:(NSDictionary *)responseDictionary;
@end


@implementation ToodledoFolder

@synthesize delegate;

@synthesize folderID;
@synthesize name;
@synthesize folderPrivate;
@synthesize archived;
@synthesize order;

@synthesize actionType;
// TODO: Also need to add a JsonDATA field in list table to store the original or 'grabbed' JSON data from toodledo for this folder

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		self.folderID = 0;
		self.name = @"";
		self.folderPrivate = FALSE;
		self.archived = FALSE;
		self.order = 0;
		
		self.actionType = EnumToodledoFolderActionNone;
	}
	return self;
}

- (id)initWithName:(NSString *)theName andID:(NSInteger)theFolderID andPrivate:(BOOL)isPrivate 
	   andArchived:(BOOL)isArchived andOrder:(NSInteger)theOrder {
	if (self = [super init]) {
		self.folderID = theFolderID;
		self.name = theName;
		self.folderPrivate = isPrivate;
		self.archived = isArchived;
		self.order = theOrder;
		
		self.actionType = EnumToodledoFolderActionNone;
	}
	return self;
}

#pragma mark -
#pragma mark Class Methods

- (void)updateSelfWithDictionary:(NSDictionary *)responseDictionary {
	self.folderID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDictionary];
	self.name = [DMHelper getNSStringValueForKey:@"name" fromDictionary:responseDictionary];
	self.folderPrivate = [DMHelper getBoolValueForKey:@"private" fromDictionary:responseDictionary];
	self.archived = [DMHelper getBoolValueForKey:@"archived" fromDictionary:responseDictionary];
	self.order = [DMHelper getNSIntegerValueForKey:@"ord" fromDictionary:responseDictionary];
}

#pragma mark -
#pragma mark Action Methods

// Return success if this succeeds
- (BOOL)addFolderWithName:(NSString *)folderName usingKey:(NSString *)key responseString:(NSString **)responseString error:(NSError **)error {
	// Update the action type
	self.actionType = EnumToodledoFolderActionAdd;
	
	// Keep copy of original passed folder name
	NSString *originalFolderName = [folderName copy];
	
	// Replace any special characters in folder name (like space)
	//folderName = [folderName stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
	//folderName = [folderName stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
	folderName = [folderName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	// Get the json data.. need to print this first to see what data is returned, possibly an array
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/folders/add.php?name=%@;key=%@",
							folderName, key];
	
	//responseData = [[NSMutableData data] retain];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	//[[NSURLConnection alloc] initWithRequest:request delegate:self];	
	
	// Get the data
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:error ];
	NSString *jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	//*responseString = [jsonString copy];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	//id object = [json objectWithString:jsonString error:error];
	//NSLog(@"object type is: %@", NSStringFromClass([object class]));
	
	id object = [json objectWithString:jsonString];
//	[jsonString release];
	
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
		responseDictionary = [responseArray objectAtIndex:0];
	}
	
	//for (id arrayObject in responseArray) {
	//	NSLog(@"array object type is: %@", NSStringFromClass([arrayObject class]));
	//}
	
	//NSDictionary *responseDictionary = (NSDictionary *)[json objectWithString:jsonString error:error];
	
	BOOL status = FALSE;
	
	// Check for errorDesc
	if (*error) {
		// NSError
		*responseString = [NSString stringWithFormat:@"Connection Error: %@", [*error localizedDescription]];
	} else if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
		// ERROR
		NSInteger errorCode = [[responseDictionary valueForKey:@"errorCode"] integerValue];
		
		if (errorCode == 5) {
			// Same folder already exists
			*responseString = [NSString stringWithFormat:@"Toodledo Error: %@ (%@)", 
							   [responseDictionary valueForKey:@"errorDesc"], originalFolderName];
		} else {
			*responseString = [NSString stringWithFormat:@"Toodledo Error: %@", 
							   [responseDictionary valueForKey:@"errorDesc"]];
		}

		
	}  else {
		status = TRUE;
		
		// Update self with dictionary
		[self updateSelfWithDictionary:responseDictionary];
		
		*responseString = [NSString stringWithFormat:@"Toodledo Server: Added folder (%@)", self.name];
	}

//	[originalFolderName release];
	return status;
}

- (NSString *)deleteFolderWithID:(NSInteger)theFolderID andOriginalName:(NSString *)originalName usingKey:(NSString *)key {
	// Return blank string if folder ID is either 0 or -1
	if (theFolderID == 0 || theFolderID == -1) {
		return @"";
	}
	
	// Get the json data.. need to print this first to see what data is returned, possibly an array
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/folders/delete.php?id=%d;key=%@",
							theFolderID, key];
	responseData = [NSMutableData data];// retain];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	//[[NSURLConnection alloc] initWithRequest:request delegate:self];	
	
	NSError *error = nil;
	
	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
	NSString *jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	
	// Assign the json object
	NSDictionary *responseDictionary = [json objectWithString:jsonString];
	
//	[jsonString release];
	
	NSString *responseString = @"";
	
	if (error) {
		responseString = [NSString stringWithFormat:@"Connection Error: %@",
						  [error localizedDescription]];
		return responseString;
	} else if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
		// Toodledo error
		NSInteger errorCode = [[responseDictionary valueForKey:@"errorCode"] integerValue];
		if (errorCode == 4) {
			responseString = [NSString stringWithFormat:@"Toodledo Error: %@ (%@)",
							  [responseDictionary valueForKey:@"errorDesc"],
							  originalName];
		} else {
			responseString = [NSString stringWithFormat:@"Toodledo Error: %@",
							  [responseDictionary valueForKey:@"errorDesc"]];
		}

		
		return responseString;
	} else if ([responseDictionary objectForKey:@"deleted"] != nil) {
		responseString = [NSString stringWithFormat:@"Toodledo Server: Deleted folder (%@) (%@)",
						  originalName,
						  [responseDictionary valueForKey:@"deleted"]];
		return responseString;
	}
	
	
	return @"";
}

// Will update the folder
- (NSString *)updateFolderUsingKey:(NSString *)key andOldName:(NSString *)originalName {
	// Update the action type
	self.actionType = EnumToodledoFolderActionUpdate;
	
	// Replace any special characters in folder name (like space)
	NSString *folderName = [self name];
	folderName = [MethodHelper replaceHttpSpecialCharactersForString:folderName];
	
	// Make sure not to change name to one that already exists, and not same values
	
	// Get the json data.. need to print this first to see what data is returned, possibly an array
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/folders/edit.php?id=%d;name=%@;archived=%d;key=%@",
							self.folderID, folderName, self.archived, key];
	responseData = [NSMutableData data];// retain];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	//[[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	NSError *error = nil;
	
	// Get the data
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error ];
	NSString *jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new] ;//autorelease];
	
	id object = [json objectWithString:jsonString];
//	[jsonString release];
	
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
		responseDictionary = [responseArray objectAtIndex:0];
	}
	
	//NSDictionary *responseDictionary = [json objectWithString:jsonString];
	
	NSString *responseString = @"";
	
	// Check for error desc
	if (error) {
		responseString = [NSString stringWithFormat:@"Connection Error: %@", [error localizedDescription]];
	} else if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
		NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
		responseString = [NSString stringWithFormat:@"Toodledo Error: %@", errorDesc];
	} else if ([responseDictionary objectForKey:@"name"] != nil) {
		responseString = [NSString stringWithFormat:@"Toodledo Server: Updated folder (%@) with local list title (%@)", originalName, self.name];
	}
	
	return responseString;
}

@end
