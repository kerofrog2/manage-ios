//
//  ToodledoTaskCollection.m
//  Manage
//
//  Created by Cliff Viegas on 14/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ToodledoTaskCollection.h"

// Data Models
#import "ToodledoTask.h"
#import "TaskList.h"
#import "ListItem.h"
#import "RecurringListItem.h"
#import "DeletedListItem.h"
#import "ArchiveList.h"

// Data Collections
#import "TagCollection.h"
#import "ToodledoFolderCollection.h"
#import "DeletedListItemCollection.h"
#import "TaskListCollection.h"

// JSON
#import "JSON.h"

// DMHelper
#import "DMHelper.h"

// Method Helper
#import "MethodHelper.h"


// Constants
#import "ListConstants.h"

// Private methods
@interface ToodledoTaskCollection()
- (NSDictionary *)convertListItemIntoDictionaryCopy:(ListItem *)listItem 
							 usingTagCollection:(TagCollection *)tagCollection 
					andToodledoFolderCollection:(ToodledoFolderCollection *)toodledoFolderCollection;
- (BOOL)loadTasksUsingArray:(NSArray *)responseArray;
- (void)loadDeletedTasksUsingArray:(NSArray *)responseArray;
@end


@implementation ToodledoTaskCollection

@synthesize toodledoKey;
@synthesize tasks;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
//	[self.tasks release];
//	//[self.toodledoKey release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if ((self = [super init])) {
		self.tasks = [NSMutableArray array];
		self.toodledoKey = @"";
	}
	return self;
}

- (id)initUsingKey:(NSString *)theToodledoKey {
	if ((self = [super init])) {
		// Assign the passed key
		self.toodledoKey = theToodledoKey;
		
		// Init toodledoconnection to nil
		toodledoConnection = nil;
		
		// Init the tasks array
		self.tasks = [NSMutableArray array];
	}
	return self;
}

#pragma mark -
#pragma mark Get Methods

- (NSString *)getTasksFromServerWithModafter:(NSString *)modAfter andFields:(NSString *)fields andStart:(NSInteger)start {
	NSError *error = nil;
	
	// Get the json data
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/get.php?key=%@;fields=%@;start=%d;num=%d",
							self.toodledoKey, fields, start, (start + 1000)];
	
	if ([modAfter isEqualToString:@"0"] == FALSE) {
		httpString = [NSString stringWithFormat:@"%@;modafter=%@", httpString,modAfter];
	}

	//responseData = [[NSMutableData data] retain];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	
	// Get the data
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error ];
	NSString *jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];

	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
    
	NSArray *responseArray = [json objectWithString:jsonString];
//	[jsonString release];
    
	NSString *responseString = @"";
	if (error) {
		// NSError
		responseString = [error description];
	} else if ([responseArray count] > 990) {
        responseString = @"Toodledo Server: Collecting tasks...";
    } else {
		// Update self with dictionary
		responseString = @"Toodledo Server: Successfully collected tasks";
	}
	
    [self loadTasksUsingArray:responseArray];
    
	return responseString;
}

- (NSString *)getDeletedTasksFromServerWithModafter:(NSString *)modAfter {
	NSError *error = nil;
	
	// Get the json data
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/deleted.php?key=%@",
							self.toodledoKey];
	
	if ([modAfter isEqualToString:@"0"] == FALSE) {
		httpString = [NSString stringWithFormat:@"%@;modafter=%@", httpString,modAfter];
	}
	
	//responseData = [[NSMutableData data] retain];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	
	// Get the data
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error ];
	NSString *jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	
	NSArray *responseArray = [json objectWithString:jsonString];
//	[jsonString release];
	
	NSString *responseString = @"";
	if (error) {
		// NSError
		responseString = [error description];
	} else {
		// Update self with dictionary
		responseString = @"Toodledo Server: Successfully collected deleted tasks";
	}
	
	[self loadDeletedTasksUsingArray:responseArray];
	
	return responseString;
}

- (NSArray *)getListOfTagsCopy {
	NSMutableArray *tagsArray = [[NSMutableArray alloc] init];// autorelease];
	
	for (ToodledoTask *task in self.tasks) {
		NSArray *tags = [task.tags componentsSeparatedByString:@","];
		
		for (NSString *tag in tags) {
            NSString *aTag = [tag stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

			if ([aTag length] == 0) {
				continue;
			}
			
			// Need to see whether this string exists in array
			if ([MethodHelper stringObject:aTag existsInArray:tagsArray] == FALSE) {
				[tagsArray addObject:aTag];
			}
		}
	}
	return tagsArray;
}

// Recurring method, used to get correct parent task for an imported toodledo task
- (SInt64)getTopLevelLocalTaskIDFromToodledoParentTaskID:(SInt64)initialParentID {
	SInt64 topLevelID = -1;
	
	for (ToodledoTask *task in self.tasks) {
		if ([task serverID] == initialParentID) {
			if ([task parentID] == 0) {
				// return local task ID
				topLevelID = [task serverID];
				return topLevelID;
			} else {
				return [self getTopLevelLocalTaskIDFromToodledoParentTaskID:[task serverID]];
			}

		}
	}
	
	return topLevelID;
}

// Return the index of the task with the passed server ID
- (NSInteger)getIndexOfTaskWithTaskID:(SInt64)serverTaskID {
	NSInteger i = 0;
	for (ToodledoTask *task in self.tasks) {
		if ([task serverID] == serverTaskID) {
			return i;
		}
		i++;
	}
	
	return -1;
}

// Return the index of the task with the 'meta' listItemID
- (NSInteger)getIndexOfTaskWithListItemID:(NSInteger)listItemID {
	NSInteger foundIndex = -1;
	NSInteger i = 0;
	
	for (ToodledoTask *task in self.tasks) {
		// Process data
		SBJsonParser *json = [SBJsonParser new];// autorelease];
		NSDictionary *metaDictionary = [json objectWithString:task.meta];
		
		NSInteger taskListItemID = [DMHelper getNSIntegerValueForKey:@"listitemid" fromDictionary:metaDictionary];
		
		if (listItemID == taskListItemID) {
			foundIndex = i;
			break;
		}
		i++;
	}
	
	return foundIndex;
}

#pragma mark -
#pragma mark Update Methods

// Update tasks using array of list items...


// Update meta data on all toodledo tasks
- (BOOL)postUpdatesToodledoMetaWithResponseString:(NSMutableString **)mutableResponseString section:(NSInteger)section error:(NSError **)error {
	NSMutableArray *postUpdatesArray = [[NSMutableArray alloc] init];
    
	BOOL updatesComplete = TRUE;
	NSInteger i = 1;
	NSInteger maxEntries = 45;
    
    NSInteger startCount = 1 + (45 * section);
    NSInteger progressiveCount = 1;
	
	for (ToodledoTask *task in self.tasks) {
		if ([task needsUpdate] == TRUE && i <= maxEntries && progressiveCount >= startCount) {
			// Update this task
			NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
			[newDictionary setValue:[NSString stringWithFormat:@"%qi", task.serverID] forKey:@"id"];
			[newDictionary setValue:[NSString stringWithFormat:@"%@", task.meta] forKey:@"meta"];
			
			[postUpdatesArray addObject:newDictionary];
//			[newDictionary release];
			
			task.needsUpdate = FALSE;
			i++;
		} else if (i > maxEntries) {
			updatesComplete = FALSE;
			break;
		}
        progressiveCount++;
	}
	
	NSString *jsonString = @"";
	
	// Update if we found some tasks that need to be updated
	if ([postUpdatesArray count] > 0) {
		//SBJsonWriter *json = [SBJsonWriter alloc];
		jsonString = [postUpdatesArray JSONRepresentation]; //[json stringWithObject:postUpdatesArray];
		
		// Replace json string
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
		jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	} else {
		// No updates to be made, return complete
//		[postUpdatesArray release];
		return YES;
	}
	
	
//	[postUpdatesArray release];
	
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/edit.php?key=%@;tasks=%@",
							self.toodledoKey, jsonString];
	
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	
	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error:error ];
	jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new] ;//autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
//	[jsonString release];
	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
	if (*error) {
		// NSError occured
		[*mutableResponseString setString:[*error description]];
	} else {
		// Go through the array
		if (responseArray != nil) {
			for (NSDictionary *responseDictionary in responseArray) {
				if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
					// ERROR
					NSString *toodledoError = [responseDictionary objectForKey:@"errorDesc"];
					[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
				} else {
					// Success
					SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDictionary];
					[*mutableResponseString appendFormat:@"Toodledo Server: Toodledo task (%qi) linked\n", taskID];
				}
				
			}
		} else if (responseDictionary != nil) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
				[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
			}
		}
		
	}
	
	return updatesComplete;
}

// To do updates, we just update the collection and then set a needsUpdate flag on task
- (BOOL)postUpdatesToTaskFolderNamesWithResponseString:(NSMutableString **)mutableResponseString 
                                               section:(NSInteger)section
                                                 error:(NSError **)error {
	NSMutableArray *postUpdatesArray = [[NSMutableArray alloc] init];
    
	BOOL updatesComplete = TRUE;
	NSInteger i = 1;
	NSInteger maxEntries = 45;
    
    NSInteger startCount = 1 + (45 * section);
    NSInteger progressiveCount = 1;
	
	for (ToodledoTask *task in self.tasks) {
		if ([task needsUpdate] == TRUE && i <= maxEntries && progressiveCount >= startCount) {
			// Update this task
			NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
			[newDictionary setValue:[NSString stringWithFormat:@"%qi", task.serverID] forKey:@"id"];
			[newDictionary setValue:[NSString stringWithFormat:@"%lld", task.folderID] forKey:@"folder"];
				
			[postUpdatesArray addObject:newDictionary];
//			[newDictionary release];
				
			task.needsUpdate = FALSE;
			i++;
		} else if (i > maxEntries) {
			updatesComplete = FALSE;
			break;
		}
        progressiveCount++;
	}
	
	NSString *jsonString = @"";
	
	// Update if we found some tasks that need to be updated
	if ([postUpdatesArray count] > 0) {
		//SBJsonWriter *json = [SBJsonWriter alloc];
		jsonString = [postUpdatesArray JSONRepresentation]; //[json stringWithObject:postUpdatesArray];
		
		// Replace json string
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
		jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	} else {
		// No updates to be made, return complete
//		[postUpdatesArray release];
		return YES;
	}

	
//	[postUpdatesArray release];
	
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/edit.php?key=%@;tasks=%@;fields=folder,star",
							self.toodledoKey, jsonString];

	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];

	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error:error ];
	jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
//	[jsonString release];
	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
	if (*error) {
		// NSError occured
		[*mutableResponseString setString:[*error description]];
	} else {
		// Go through the array
		if (responseArray != nil) {
			for (NSDictionary *responseDictionary in responseArray) {
				if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
					// ERROR
					NSString *toodledoError = [responseDictionary objectForKey:@"errorDesc"];
					[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
				} else {
					// Success
					SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDictionary];
					NSInteger folderID = [DMHelper getNSIntegerValueForKey:@"folder" fromDictionary:responseDictionary];
					[*mutableResponseString appendFormat:@"Toodledo Server: Toodledo task (%qi) updated with new folder (%d)\n", taskID, folderID];
				}
				
			}
		} else if (responseDictionary != nil) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
				[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
			}
		}
		
	}

	return updatesComplete;
}


// To do updates, we just update the collection and then set a needsUpdate flag on task
- (BOOL)postUpdatesToSubTasksWithResponseString:(NSMutableString **)mutableResponseString section:(NSInteger)section error:(NSError **)error {
	NSMutableArray *postUpdatesArray = [[NSMutableArray alloc] init];
    
	BOOL updatesComplete = TRUE;
	NSInteger i = 1;
	NSInteger maxEntries = 45;
    
    NSInteger startCount = 1 + (45 * section);
    NSInteger progressiveCount = 1;
	
	for (ToodledoTask *task in self.tasks) {
		if ([task needsUpdate] == TRUE && i <= maxEntries && progressiveCount >= startCount) {
			// Update this task
			NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
			[newDictionary setValue:[NSString stringWithFormat:@"%qi", task.serverID] forKey:@"id"];
			[newDictionary setValue:[NSString stringWithFormat:@"%qi", task.parentID] forKey:@"parent"];
			
			[postUpdatesArray addObject:newDictionary];
//			[newDictionary release];
			
			task.needsUpdate = FALSE;
			i++;
		} else if (i > maxEntries) {
			updatesComplete = FALSE;
			break;
		}
        progressiveCount++;
	}
	
	NSString *jsonString = @"";
	
	// Update if we found some tasks that need to be updated
	if ([postUpdatesArray count] > 0) {
		//SBJsonWriter *json = [SBJsonWriter alloc];
		jsonString = [postUpdatesArray JSONRepresentation]; //[json stringWithObject:postUpdatesArray];
		
		// Replace json string
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
		jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	} else {
		// No updates to be made, return complete
//		[postUpdatesArray release];
		return YES;
	}
	
	
//	[postUpdatesArray release];
	
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/edit.php?key=%@;tasks=%@;fields=parent",
							self.toodledoKey, jsonString];
	
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	
	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error:error ];
	jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
//	[jsonString release];
	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
	if (*error) {
		// NSError occured
		[*mutableResponseString setString:[*error description]];
	} else {
		// Go through the array
		if (responseArray != nil) {
			for (NSDictionary *responseDictionary in responseArray) {
				if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
					// ERROR
					NSString *toodledoError = [responseDictionary objectForKey:@"errorDesc"];
					[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
				} else {
					// Success
					SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDictionary];
					SInt64 parentID = [DMHelper getSInt64ValueForKey:@"parent" fromDictionary:responseDictionary];
					[*mutableResponseString appendFormat:@"Toodledo Server: Toodledo subtask (%qi) updated with new parent (%qi)\n", taskID, parentID];
				}
				
			}
		} else if (responseDictionary != nil) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
				[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
			}
		}
		
	}
	
	return updatesComplete;
}

- (BOOL)postUpdatesToTasksFromTaskListCollection:(TaskListCollection *)taskListCollection
							  usingTagCollection:(TagCollection *)tagCollection
					 andToodledoFolderCollection:(ToodledoFolderCollection *)toodledoFolderCollection
						  andAlreadyUpdatedTasks:(NSMutableArray *)alreadyUpdatedLocalTasks
										 section:(NSInteger)section
						   mutableResponseString:(NSMutableString **)mutableResponseString
										   error:(NSError **)error {
	// Return if no lists found
	NSMutableArray *updateTasksArray = [[NSMutableArray alloc] init];
	BOOL updateTasksComplete = TRUE;
	NSInteger i = 1;
	NSInteger maxEntries = 45;
	
	NSInteger startCount = 1 + (45 * section);
	NSInteger progressiveCount = 1;
	
	for (TaskList *taskList in taskListCollection.lists) {
		if (updateTasksComplete == FALSE) {
			break;
		}
		for (ListItem *listItem in taskList.listItems) {
			// First need to make sure we haven't already edited this task
			BOOL found = FALSE;
			for (NSString *alreadyEditedID in alreadyUpdatedLocalTasks) {
				if (listItem.listItemID == [alreadyEditedID integerValue]) {
					found = TRUE;
				}
			}
			// Continue to next listItem if found is true
			if (found == TRUE) {
				continue;
			}
            
			if (i <= maxEntries && progressiveCount >= startCount) {
				// Make sure list item parent title is correct
				listItem.parentListTitle = taskList.title;
				
				// Convert the list item to a dictionary
				NSDictionary *editDictionary = [self convertListItemIntoDictionaryCopy:listItem
																usingTagCollection:tagCollection 
                                                            andToodledoFolderCollection:toodledoFolderCollection];// retain];
				
				if (listItem.toodledoTaskID == -1) {
//					[editDictionary release];
					continue;
				}
				
				[editDictionary setValue:[NSString stringWithFormat:@"%qi", listItem.toodledoTaskID] forKey:@"id"];
				
				[updateTasksArray addObject:editDictionary];
//                [editDictionary release];
                i++;
				//[editDictionary release];
			} else if (i > maxEntries) {
				updateTasksComplete = FALSE;
				break;
			}
				
			progressiveCount++;
		}
	}
	
	// Send these tasks to toodledo to update
	NSString *jsonString = @"";
    
	if ([updateTasksArray count] > 0) {
		jsonString = [updateTasksArray JSONRepresentation];
		
		// Replace json string using correct encoding
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
		jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	} else {
		// No tasks to be added
//		[updateTasksArray release];
		return YES;
	}
	
//	[updateTasksArray release];
	
	// The extra fields to return
	NSString *fields = @"tag,folder,parent,duedate,repeat,repeatfrom,priority,note";
	// Create the http string
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/edit.php?key=%@;tasks=%@;fields=%@",
							self.toodledoKey, jsonString, fields];
	
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	
	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:error ];
	jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
//	[jsonString release];
	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
	if (*error) {
		// NSError occured
		[*mutableResponseString setString:[*error description]];
		NSLog(@"Connection Error: %@", [*error localizedDescription]);
	} else {
		// Go through the array
		if (responseArray != nil) {
			// Load these tasks into this collection
			[self loadTasksUsingArray:responseArray];
			
			for (NSDictionary *responseDict in responseArray) {
				if ([responseDict objectForKey:@"errorDesc"] != nil) {
					// ERROR
					NSString *toodledoError = [responseDict objectForKey:@"errorDesc"];
					[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
				} else {
					// Success
					SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDict];
					[*mutableResponseString appendFormat:@"Toodledo Server: Task (%qi) updated\n", taskID];
				}
				
			} 
		} else if (responseDictionary != nil) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
				[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
			}
		}
	}
	
	// Now need to update any tasks that are not subtasks but need to be... or those that used to be subtasks
	// but no longer are... sheez... When we return the old 'parent' hasn't changed, so we just need to compare
	// each toodledo task to it's equivelent list item, set requires update to true and call update subtasks :)
	
	// If toodledo task had been changed to have parent and local does not have parent.. local has precedent, so change toodledotask to no parent
	// If toodledo task had changed to no parent and local does have parent, then update toodledotask with new parent folder
	
	
	return updateTasksComplete;
}


// To do updates, we just update the collection and then set a needsUpdate flag on task
- (BOOL)postUpdatesToTaskFolderNamesFromTaskList:(TaskList *)taskList 
                         usingTaskListCollection:(TaskListCollection *)refTaskListCollection 
                                  responseString:(NSMutableString **)mutableResponseString 
                                         section:(NSInteger)section
                                           error:(NSError **)error {
    if ([taskList.listItems count] == 0) {
		return YES;
	}
	
	NSMutableArray *postUpdatesArray = [[NSMutableArray alloc] init];
    
	BOOL updatesComplete = TRUE;
	NSInteger i = 1;
	NSInteger maxEntries = 45;
    
    NSInteger startCount = 1 + (45 * section);
    NSInteger progressiveCount = 1;
	
	for (ListItem *listItem in taskList.listItems) {
		if (i <= maxEntries && progressiveCount >= startCount) {
			NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
			[newDictionary setValue:[NSString stringWithFormat:@"%qi", listItem.toodledoTaskID] forKey:@"id"];
			
			// Need to get the correct folder id
			NSInteger listIndex = [refTaskListCollection getIndexOfTaskListWithID:listItem.listID];
			
			if (listIndex == -1) {
//				[newDictionary release];
				continue;
			}
			TaskList *refTaskList = [refTaskListCollection.lists objectAtIndex:listIndex];
			
			if ([refTaskList toodledoFolderID] == -1) {
//				[newDictionary release];
				continue;
			}
			
			[newDictionary setValue:[NSString stringWithFormat:@"%d", refTaskList.toodledoFolderID] forKey:@"folder"];
			
			[postUpdatesArray addObject:newDictionary];
//			[newDictionary release];
			
			i++;
		} else if (i > maxEntries) {
			updatesComplete = FALSE;
			break;
		}
        progressiveCount++;
	}
	
	NSString *jsonString = @"";
	
	// Update if we found some tasks that need to be updated
	if ([postUpdatesArray count] > 0) {
		//SBJsonWriter *json = [SBJsonWriter alloc];
		jsonString = [postUpdatesArray JSONRepresentation]; //[json stringWithObject:postUpdatesArray];
		
		// Replace json string
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
		jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	} else {
		// No updates to be made, return complete
//		[postUpdatesArray release];
		return YES;
	}
	
	
//	[postUpdatesArray release];
	
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/edit.php?key=%@;tasks=%@;fields=folder,star",
							self.toodledoKey, jsonString];
	
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];	
	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error:error ];
	jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
//	[jsonString release];
	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
	if (*error) {
		// NSError occured
		[*mutableResponseString setString:[*error description]];
	} else {
		// Go through the array
		if (responseArray != nil) {
			for (NSDictionary *responseDictionary in responseArray) {
				if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
					// ERROR
					NSString *toodledoError = [responseDictionary objectForKey:@"errorDesc"];
					[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
				} else {
					// Success
					SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDictionary];
					NSInteger folderID = [DMHelper getNSIntegerValueForKey:@"folder" fromDictionary:responseDictionary];
					[*mutableResponseString appendFormat:@"Toodledo Server: Toodledo task (%qi) updated with new folder (%d)\n", taskID, folderID];
				}
				
			}
		} else if (responseDictionary != nil) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
				[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
			}
		}
		
	}
	
	return updatesComplete;
}


// To do updates, we just update the collection and then set a needsUpdate flag on task
- (BOOL)postUpdatesToTaskFolderNamesFromTaskList:(TaskList *)taskList 
                                   usingFolderID:(SInt64)folderID 
                                  responseString:(NSMutableString **)mutableResponseString 
                                         section:(NSInteger)section
                                           error:(NSError **)error {
    if ([taskList.listItems count] == 0) {
		return YES;
	}
	
	NSMutableArray *postUpdatesArray = [[NSMutableArray alloc] init];
    
	BOOL updatesComplete = TRUE;
	NSInteger i = 1;
	NSInteger maxEntries = 45;
    
    NSInteger startCount = 1 + (45 * section);
    NSInteger progressiveCount = 1;
	
	for (ListItem *listItem in taskList.listItems) {
		if (i <= maxEntries && progressiveCount >= startCount) {
			NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
			[newDictionary setValue:[NSString stringWithFormat:@"%qi", listItem.toodledoTaskID] forKey:@"id"];
			[newDictionary setValue:[NSString stringWithFormat:@"%lld", folderID] forKey:@"folder"];
			
			[postUpdatesArray addObject:newDictionary];
//			[newDictionary release];
			
			i++;
		} else if (i > maxEntries) {
			updatesComplete = FALSE;
			break;
		}
        progressiveCount++;
	}
	
	NSString *jsonString = @"";
	
	// Update if we found some tasks that need to be updated
	if ([postUpdatesArray count] > 0) {
		//SBJsonWriter *json = [SBJsonWriter alloc];
		jsonString = [postUpdatesArray JSONRepresentation]; //[json stringWithObject:postUpdatesArray];
		
		// Replace json string
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
		jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	} else {
		// No updates to be made, return complete
//		[postUpdatesArray release];
		return YES;
	}
	
	
//	[postUpdatesArray release];
	
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/edit.php?key=%@;tasks=%@;fields=folder,star",
							self.toodledoKey, jsonString];
	
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	
	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error:error ];
	jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
//	[jsonString release];
	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
	if (*error) {
		// NSError occured
		[*mutableResponseString setString:[*error description]];
	} else {
		// Go through the array
		if (responseArray != nil) {
			for (NSDictionary *responseDictionary in responseArray) {
				if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
					// ERROR
					NSString *toodledoError = [responseDictionary objectForKey:@"errorDesc"];
					[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
				} else {
					// Success
					SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDictionary];
					NSInteger folderID = [DMHelper getNSIntegerValueForKey:@"folder" fromDictionary:responseDictionary];
					[*mutableResponseString appendFormat:@"Toodledo Server: Toodledo task (%qi) updated with new folder (%d)\n", taskID, folderID];
				}
				
			}
		} else if (responseDictionary != nil) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
				[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
			}
		}
		
	}
	
	return updatesComplete;
}

/*
- (BOOL)addNewToodledoTasksFromTaskList:(TaskList *)taskList 
					 usingTagCollection:(TagCollection *)tagCollection
			andToodledoFolderCollection:(ToodledoFolderCollection *)toodledoFolderCollection
								section:(NSInteger)section
				  mutableResponseString:(NSMutableString **)mutableResponseString
								  error:(NSError **)error {
	
	// Return if no tasks found
	if ([taskList.listItems count] == 0) {
		[*mutableResponseString setString:[NSString stringWithFormat:@"Local: List (%@) has no tasks",
										   taskList.title]];
		return YES;
	}
	
	NSMutableArray *addNewTasksArray = [[NSMutableArray alloc] init];
	BOOL addTasksComplete = TRUE;
	NSInteger i = 1;
	NSInteger maxEntries = 50;
	
	NSInteger startCount = 1 + (50 * section);
	NSInteger progressiveCount = 1;
	
	for (ListItem *listItem in taskList.listItems) {
		if (i <= maxEntries && progressiveCount >= startCount) {
			// Need to convert list item into a dictionary object, and then to json string
			NSDictionary *newDictionary = [self convertListItemIntoDictionary:listItem 
														   usingTagCollection:tagCollection
												  andToodledoFolderCollection:toodledoFolderCollection];
			[addNewTasksArray addObject:newDictionary];
			[newDictionary release];
		} else if (i > maxEntries) {
			addTasksComplete = FALSE;
			break;
		}
		progressiveCount++;
	}
	----------------------------------------------------
	NSString *jsonString = @"";
	
	if ([addNewTasksArray count] > 0) {
		jsonString = [addNewTasksArray JSONRepresentation];
		
		// Replace json string using correct encoding
		jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	} else {
		// No tasks to be added
		return YES;
	}
	
	[addNewTasksArray release];
	-----------------------------
	// The extra fields to return
	NSString *fields = @"tag,folder,parent,duedate,repeat,repeatfrom,priority,note,meta";
	
	// Create the http string
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/add.php?key=%@;tasks=%@;fields=%@",
							self.toodledoKey, jsonString, fields];
	
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString]];
	
	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:error ];
	jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [[SBJsonParser new] autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
	[jsonString release];
	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
	if (*error) {
		// NSError occured
		[*mutableResponseString setString:[*error description]];
		NSLog(@"Connection Error: %@", [*error localizedDescription]);
	} else {
		// Go through the array
		if (responseArray != nil) {
			// Load these tasks into this collection
			[self loadTasksUsingArray:responseArray];
			
			for (NSDictionary *responseDict in responseArray) {
				if ([responseDict objectForKey:@"errorDesc"] != nil) {
					// ERROR
					NSString *toodledoError = [responseDict objectForKey:@"errorDesc"];
					[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
				} else {
					// Success
					SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDict];
					[*mutableResponseString appendFormat:@"Toodledo Server: Task (%qi) created\n", taskID];
					
					// Update our local list item toodledoTaskID
					if ([responseDict objectForKey:@"ref"] != nil) {
						NSString *refString = [responseDict valueForKey:@"ref"];
						NSInteger theListItemID = [refString integerValue];
						NSInteger listItemIndex = [taskList getIndexOfListItemWithID:theListItemID];
						if (listItemIndex != -1) {
							ListItem *listItem = [taskList.listItems objectAtIndex:listItemIndex];
							listItem.toodledoTaskID = taskID;
							[listItem updateDatabaseWithoutLastEditTaskDateTime];
						}
					}
					
					
					
				}
				
			} 
		} else if (responseDictionary != nil) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
				[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
			}
		}
	}
	
	
	return addTasksComplete;
}
*/

#pragma mark -
#pragma mark Add Methods

- (BOOL)addNewToodledoTasksFromTaskList:(TaskList *)taskList 
					 usingTagCollection:(TagCollection *)tagCollection
			andToodledoFolderCollection:(ToodledoFolderCollection *)toodledoFolderCollection
								section:(NSInteger)section
				  mutableResponseString:(NSMutableString **)mutableResponseString
								  error:(NSError **)error 
			   masterTaskListCollection:(TaskListCollection *)masterTaskListCollection 
                            archiveList:(ArchiveList *)archiveList {
	// Return if no tasks found
    if (taskList != nil) {
        if ([taskList.listItems count] == 0) {
            [*mutableResponseString setString:[NSString stringWithFormat:@"Local: List (%@) has no tasks",
                                               taskList.title]];
            return YES;
        }
    }
	
	
	NSMutableArray *addNewTasksArray = [[NSMutableArray alloc] init];
	BOOL addTasksComplete = TRUE;
	NSInteger i = 1;
	NSInteger maxEntries = 40;
	
	NSInteger startCount = 1 + (40 * section);
	NSInteger progressiveCount = 1;
	
    if (taskList != nil) {
        for (ListItem *listItem in taskList.listItems) {
            if (i <= maxEntries && progressiveCount >= startCount) {
                // Make sure list item parent title is correct
                listItem.parentListTitle = taskList.title;
                
                // Need to convert list item into a dictionary object, and then to json string
                NSDictionary *newDictionary = [self convertListItemIntoDictionaryCopy:listItem
                                                                   usingTagCollection:tagCollection
                                                           andToodledoFolderCollection:toodledoFolderCollection];// retain];
                [addNewTasksArray addObject:newDictionary];
//                [newDictionary release];
                i++;
                //[newDictionary release];
            } else if (i > maxEntries) {
                addTasksComplete = FALSE;
                break;
            }
            progressiveCount++;
        } 
    } else if (archiveList != nil) {
        // Go through and send these up
        for (ListItem *listItem in archiveList.listItems) {
            if (i <= maxEntries && progressiveCount >= startCount) {
                // We have already correctly set the parent list item title
                
                // Need to convert list item into a dictionary object, and then to json string
                NSDictionary *newDictionary = [self convertListItemIntoDictionaryCopy:listItem
                                                                   usingTagCollection:tagCollection
                                                           andToodledoFolderCollection:toodledoFolderCollection];// retain];
                [addNewTasksArray addObject:newDictionary];
//                [newDictionary release];
                i++;
                //[newDictionary release];
            } else if (i > maxEntries) {
                addTasksComplete = FALSE;
                break;
            }
            progressiveCount++;
        } 
    }
	

	NSString *jsonString = @"";
	
	if ([addNewTasksArray count] > 0) {
		jsonString = [addNewTasksArray JSONRepresentation];
		
		// Replace json string using correct encoding
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
		jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

	} else {
		// No tasks to be added
//		[addNewTasksArray release];
		return YES;
	}
	
//	[addNewTasksArray release];
	
	// The extra fields to return
	NSString *fields = @"tag,folder,parent,duedate,repeat,repeatfrom,priority,note,meta";
	
	// Create the http string
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/add.php?key=%@;tasks=%@;fields=%@",
							self.toodledoKey, jsonString, fields];
	
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	
	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:error ];
	jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new] ;//autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
//	[jsonString release];
	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
    
	if (*error) {
		// NSError occured
		[*mutableResponseString setString:[*error description]];
		NSLog(@"Connection Error: %@", [*error localizedDescription]);
	} else {
		// Go through the array
		if (responseArray != nil) {
			// Load these tasks into this collection
			[self loadTasksUsingArray:responseArray];
			
			for (NSDictionary *responseDict in responseArray) {
				if ([responseDict objectForKey:@"errorDesc"] != nil) {
					// ERROR
					NSString *toodledoError = [responseDict objectForKey:@"errorDesc"];
					[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
				} else {
					// Success
					SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDict];
					[*mutableResponseString appendFormat:@"Toodledo Server: Task (%qi) created\n", taskID];
					
					// Update our local list item toodledoTaskID (might need to update archive list)
					if ([responseDict objectForKey:@"ref"] != nil) {
						NSString *refString = [responseDict valueForKey:@"ref"];
						NSInteger theListItemID = [refString integerValue];
                        if (taskList != nil) {
                            NSInteger listItemIndex = [taskList getIndexOfListItemWithID:theListItemID];
                            if (listItemIndex != -1) {
                                ListItem *listItem = [taskList.listItems objectAtIndex:listItemIndex];
                                listItem.toodledoTaskID = taskID;
                                [listItem updateDatabaseWithoutLastEditTaskDateTime];
                                
                                // Also need to update the master task list collection
                                [masterTaskListCollection findAndUpdateListItem:listItem];
                            }
                        } else if (archiveList != nil) {
                            // Update in case of archived list item
                            NSInteger archiveListItemIndex = [archiveList getIndexOfListItemWithID:theListItemID];
                            if (archiveListItemIndex != -1) {
                                ListItem *archiveListItem = [archiveList.listItems objectAtIndex:archiveListItemIndex];
                                archiveListItem.toodledoTaskID = taskID;
                                [archiveListItem updateDatabaseWithoutLastEditTaskDateTime];
                            }
                        }
						
					}
					
					
					
				}

			} 
		} else if (responseDictionary != nil) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
				[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
			}
		}
	}

	
	return addTasksComplete;
}



#pragma mark -
#pragma mark Delete Methods

- (BOOL)deleteAllTasksFromSection:(NSInteger)section mutableResponseString:(NSMutableString **)mutableResponseString
							error:(NSError **)error deletedListItemCollection:(DeletedListItemCollection *)deletedListItemCollection {
	NSMutableArray *deleteTasksArray = [[NSMutableArray alloc] init];
	
	BOOL deleteTasksComplete = TRUE;
	NSInteger i = 1;
	NSInteger maxEntries = 50;
	
	NSInteger startCount = 1 + (50 * section);
	NSInteger progressiveCount = 1;
	
	if (deletedListItemCollection == nil) {
		if ([self.tasks count] == 0) {
			[*mutableResponseString setString:@"Toodledo Server: No tasks found\n"];
//			[deleteTasksArray release];
			return TRUE;
		}
	} else {
		if ([deletedListItemCollection.deletedListItems count] == 0) {
			[*mutableResponseString setString:@"Toodledo Server: No tasks found\n"];
//			[deleteTasksArray release];
			return TRUE;
		}
	}
	
	if (deletedListItemCollection == nil) {
		for (ToodledoTask *task in self.tasks) {
			if (i <= maxEntries && progressiveCount >= startCount) {
				// Delete this task
				[deleteTasksArray addObject:[NSString stringWithFormat:@"%qi", task.serverID]];
				
				i++;
			} else if (i > maxEntries) {
				deleteTasksComplete = FALSE;
				break;
			}
			
			progressiveCount++;
		}
	} else {
		for (DeletedListItem *deletedListItem in deletedListItemCollection.deletedListItems) {
			if (i <= maxEntries && progressiveCount >= startCount) {
				// Delete this task
				if (deletedListItem.toodledoTaskID != -1) {
					[deleteTasksArray addObject:[NSString stringWithFormat:@"%qi", deletedListItem.toodledoTaskID]];
					i++;
				}
			} else if (i > maxEntries) {
				deleteTasksComplete = FALSE;
				break;
			}
					 
			progressiveCount++;
		}
	}

	
	NSString *jsonString = @"";
	
	// Delete if we found some tasks that need to be deleted
	if ([deleteTasksArray count] > 0) {
		//SBJsonWriter *json = [SBJsonWriter alloc];
		jsonString = [deleteTasksArray JSONRepresentation]; //[json stringWithObject:postUpdatesArray];
		
		// Replace json string
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
		jsonString = [jsonString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
		jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	} else {
		// No deletions to be made, return complete
//		[deleteTasksArray release];
		return YES;
	}
	
	
//	[deleteTasksArray release];
	
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/delete.php?key=%@;tasks=%@",
							self.toodledoKey, jsonString];
	
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:20];
	
	// Data returned by WebService
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:error ];
	jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];
	
	// Process data
	SBJsonParser *json = [SBJsonParser new];// autorelease];
	
	// Assign the json object
	id object = [json objectWithString:jsonString];
//	[jsonString release];

	
	// Create the responseArray and responseDictionary
	NSArray *responseArray = nil;
	NSDictionary *responseDictionary = nil;
	
	if ([object isKindOfClass:[NSDictionary class]]) {
		responseDictionary = (NSDictionary *)object;
	} else if ([object isKindOfClass:[NSArray class]]) {
		responseArray = (NSArray *)object;
	}
	
	if (*error) {
		// NSError occured
		[*mutableResponseString setString:[*error description]];
		NSLog(@"Connection Error: %@", [*error localizedDescription]);
	} else {
		// Go through the array
		if (responseArray != nil) {
			for (NSDictionary *responseDict in responseArray) {
				if ([responseDict objectForKey:@"errorDesc"] != nil) {
					// ERROR
					NSString *toodledoError = [responseDict objectForKey:@"errorDesc"];
					[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
				} else {
					// Success
					SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDict];
					[*mutableResponseString appendFormat:@"Toodledo Server: Task (%qi) deleted\n", taskID];
				}
				
			}
		} else if (responseDictionary != nil) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
				[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
			}
		}
		
	}
	
	return deleteTasksComplete;

}

#pragma mark -
#pragma mark Cancel Methods

- (void)cancelConnection {
	if (toodledoConnection) {
		[toodledoConnection cancel];
	}
}

#pragma mark -
#pragma mark Private Methods

- (NSDictionary *)convertListItemIntoDictionaryCopy:(ListItem *)listItem 
							 usingTagCollection:(TagCollection *)tagCollection 
					andToodledoFolderCollection:(ToodledoFolderCollection *)toodledoFolderCollection {
	NSMutableDictionary *taskDictionary = [[NSMutableDictionary alloc] init];// autorelease];
	
	// Set the title
	NSString *title = listItem.title;
	
	// Specific to toodledo need to encode & as %26 and ; as %3B
	//title = [title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	if ([title length] == 0) {
		title = @"[no title]";
	}
	[taskDictionary setValue:title forKey:@"title"];
	
	// Get tag names
	NSString *tags = [listItem getToodledoStringOfTagsUsingTagCollection:tagCollection];
	if ([tags length] > 0) {
		[taskDictionary setValue:tags forKey:@"tag"];
	}
	
	// Get folder id
	NSInteger folderID = [toodledoFolderCollection getIDOfFolderWithName:listItem.parentListTitle];

	NSString *folderIDString = [NSString stringWithFormat:@"%d", folderID];
	[taskDictionary setValue:folderIDString forKey:@"folder"];
	
	// Get parent id (cant, need to do this after).. don't edit this if we updating
	// Yes edit, but only if list item is not a subtask
	if (listItem.parentListItemID == -1) {
		[taskDictionary setValue:@"0" forKey:@"parent"];
	}
	
	// Get duedate timestamp
	if ([listItem.dueDate length] > 0) {
		NSDate *theDate = [MethodHelper dateFromString:listItem.dueDate usingFormat:K_DATEONLY_FORMAT];
		NSString *timeStampString = [MethodHelper getTimeStampStringFromDate:theDate];
		[taskDictionary setValue:timeStampString forKey:@"duedate"];
	}
	
	// Get recurring toodledo string
	if (listItem.recurringListItemID != -1) {
		RecurringListItem *recurringListItem = [[RecurringListItem alloc] initWithRecurringListItemID:listItem.recurringListItemID];
		NSString *repeatString = [recurringListItem getToodledoRepeatString];
//		[recurringListItem release];
		[taskDictionary setValue:repeatString forKey:@"repeat"];
	}
	
	// Get the priority
	NSInteger priority = 0;
	if (listItem.priority == EnumHighlighterColorYellow) {
		priority = 2;
	} else if (listItem.priority == EnumHighlighterColorRed) {
		priority = 3;
	}
	NSString *priorityString = [NSString stringWithFormat:@"%d", priority];
	[taskDictionary setValue:priorityString forKey:@"priority"];
	
	// Completed timestamp
	NSString *completed = @"0";
	if ([listItem completed] == TRUE) {
		NSDate *theDate = [MethodHelper dateFromString:[listItem completedDateTime] 
										   usingFormat:K_DATETIME_FORMAT]; 
		completed = [MethodHelper getTimeStampStringFromDate:theDate];
	}
	[taskDictionary setValue:completed forKey:@"completed"];
	
	// Added timestamp
	NSString *addedString = @"";
	NSDate *theDate = [MethodHelper dateFromString:[listItem creationDateTime] 
									   usingFormat:K_DATETIME_FORMAT]; 
	addedString = [MethodHelper getTimeStampStringFromDate:theDate];
	[taskDictionary setValue:addedString forKey:@"added"];
	
	// Note
	if ([[listItem notes] length] > 0) {
		[taskDictionary setValue:[listItem notes] forKey:@"note"];
	}
	
	
	// {"value": "New", "onclick": "CreateNewDoc()"
	// Meta - store scribble, list id and listitem id
	/*NSString *meta = [NSString stringWithFormat:@"{\"listitemid\":\"%d\",\"listid\":\"%d\"", 
					  listItem.listItemID, listItem.listID];
	if ([listItem.scribble length] > 0) {
		meta = [NSString stringWithFormat:@"%@,\"scribble\":\"%@\"", meta, [listItem scribble]];
	}
	meta = [NSString stringWithFormat:@"%@}", meta];
	[taskDictionary setValue:meta forKey:@"meta"];*/
	
	// Ref - can use this to store an id number that is sent back
	NSString *refString = [NSString stringWithFormat:@"%d", listItem.listItemID];
	[taskDictionary setValue:refString forKey:@"ref"];
	
	return taskDictionary;
}

- (BOOL)loadTasksUsingArray:(NSArray *)responseArray {
	// Create new toodledo task for every dictionary in the array
	for (NSDictionary *taskDictionary in responseArray) {
		// Continue if this is the first dictionary, which contains 'num' and 'total' entries
		if ([taskDictionary objectForKey:@"num"] != nil) {
			continue;
		}
		
		// Jump out if there is an error description dictionary
		if ([taskDictionary objectForKey:@"errorDesc"] != nil) {
			continue;
		}
		
		// Jump out if we don't find an 'id' field
		if ([taskDictionary objectForKey:@"id"] == nil) {
			continue;
		}
		
		ToodledoTask *task = [[ToodledoTask alloc] init];// autorelease];
		
		// Always going to be returned
		task.serverID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:taskDictionary];
		task.title = [DMHelper getNSStringValueForKey:@"title" fromDictionary:taskDictionary];
		task.modified = [DMHelper getSInt64ValueForKey:@"modified" fromDictionary:taskDictionary];
		task.completed = [DMHelper getSInt64ValueForKey:@"completed" fromDictionary:taskDictionary];
		
		// Test other fields
		// BOOL containsKey = ([dictionary objectForKey:foo] != nil);
		
		if ([taskDictionary objectForKey:@"tag"] != nil) {
			task.tags = [DMHelper getNSStringValueForKey:@"tag" fromDictionary:taskDictionary];
		}
		
		if ([taskDictionary objectForKey:@"folder"] != nil) {
			task.folderID = [DMHelper getSInt64ValueForKey:@"folder" fromDictionary:taskDictionary];
		}
		
		if ([taskDictionary objectForKey:@"parent"] != nil) {
			task.parentID = [DMHelper getSInt64ValueForKey:@"parent" fromDictionary:taskDictionary];
		}
		
		if ([taskDictionary objectForKey:@"duedate"] != nil) {
			task.dueDate = [DMHelper getSInt64ValueForKey:@"duedate" fromDictionary:taskDictionary];
		}
		
		if ([taskDictionary objectForKey:@"repeat"] != nil) {
			task.repeat = [DMHelper getNSStringValueForKey:@"repeat" fromDictionary:taskDictionary];
		}
		
		if ([taskDictionary objectForKey:@"repeatfrom"] != nil) {
			task.repeatFrom = [DMHelper getNSIntegerValueForKey:@"repeatfrom" fromDictionary:taskDictionary];
		}
		
		if ([taskDictionary objectForKey:@"priority"] != nil) {
			task.priority = [DMHelper getNSIntegerValueForKey:@"priority" fromDictionary:taskDictionary];
		}
		
		if ([taskDictionary objectForKey:@"note"] != nil) {
			task.note = [DMHelper getNSStringValueForKey:@"note" fromDictionary:taskDictionary];
		}
		
		if ([taskDictionary objectForKey:@"meta"] != nil) {
			task.meta = [DMHelper getNSStringValueForKey:@"meta" fromDictionary:taskDictionary];
		}
		
		[self.tasks addObject:task];
	}
    
    return TRUE;
}

- (void)loadDeletedTasksUsingArray:(NSArray *)responseArray {
	// Create new toodledo task for every dictionary in the array
	for (NSDictionary *taskDictionary in responseArray) {
		// Continue if this is the first dictionary, which contains 'num' and 'total' entries
		if ([taskDictionary objectForKey:@"num"] != nil) {
			continue;
		}
		
		// Jump out if there is an error description dictionary
		if ([taskDictionary objectForKey:@"errorDesc"] != nil) {
			continue;
		}
		
		// Jump out if we don't find an 'id' field
		if ([taskDictionary objectForKey:@"id"] == nil) {
			continue;
		}
		
		ToodledoTask *task = [[ToodledoTask alloc] init];// autorelease];
		
		// Always going to be returned
		task.serverID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:taskDictionary];
		
		[self.tasks addObject:task];
	}
	
}

@end
