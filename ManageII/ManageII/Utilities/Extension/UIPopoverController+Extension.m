//
//  UIPopoverController+Extension.m
//  Manage
//
//  Created by Thuong Nguyen on 7/19/13.
//
//

#import "UIPopoverController+Extension.h"
#import "MainConstants.h"
#import "DrawPadViewController.h"

@implementation UIPopoverController (Extension)

-(id)initWithContentViewControllerWithAutoDismiss:(UIViewController *)viewController {
    self = [self initWithContentViewController:viewController];
    
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(autoDismissWithReminderAlert:) name:kNotificationAlarmViewListItem object:nil];
    }
    
    return self;
}

- (void) autoDismissWithReminderAlert:(NSNotification *) notification {
    if ([self isPopoverVisible] == YES) {
        if ([self.contentViewController isKindOfClass:[DrawPadViewController class]]) {
            // Always cancel drawpad item if user want to view reminder task
            DrawPadViewController *drawPadController = (DrawPadViewController *)self.contentViewController;
            [drawPadController cancelButtonAction];
        }
        // Only dismiss popover is visible
        [self dismissPopoverAnimated:YES];
    }
}

@end
