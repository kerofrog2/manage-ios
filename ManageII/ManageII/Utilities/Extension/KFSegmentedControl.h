//
//  KFSegmentedControl.h
//  Manage
//
//  Created by Cliff Viegas on 11/04/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KFSegmentedControlDelegate
- (void)touchUpInsideSegmentIndex:(NSInteger)segmentIndex;
@end


@interface KFSegmentedControl : UIView {
    //id <KFSegmentedControlDelegate>		delegate
	
	NSMutableArray						*items;
	NSInteger							selectedSegmentIndex;
	BOOL								enabled;
}

//@property (nonatomic, assign)	id				delegate;
@property (nonatomic, retain)	NSMutableArray	*items;
@property (nonatomic, assign)	NSInteger		selectedSegmentIndex;
@property (nonatomic, assign)	BOOL			enabled;
@property (nonatomic, assign) NSObject <KFSegmentedControlDelegate> *delegate;

#pragma mark Initialisation
- (id)initWithItems:(NSArray *)imageItems selectedItems:(NSArray *)selectedImageItems segmentSize:(CGSize)segmentSize delegate:(id<KFSegmentedControlDelegate>)theDelegate;
- (id)initWithButtonItems:(NSArray *)buttonItems andDelegate:(NSObject<KFSegmentedControlDelegate>*)theDelegate;

#pragma mark Set Methods
- (void)setSelectedSegmentIndex:(NSInteger)index;
- (void)setEnabled:(BOOL)isEnabled;

@end
