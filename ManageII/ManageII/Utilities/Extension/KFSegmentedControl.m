//
//  KFSegmentedControl.m
//  Manage
//
//  Created by Cliff Viegas on 11/04/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "KFSegmentedControl.h"

@interface KFSegmentedControl()
// Class Methods
- (void)dimAllButtonsExceptSelectedIndex;
@end


@implementation KFSegmentedControl

@synthesize delegate;
@synthesize items;
@synthesize selectedSegmentIndex;
@synthesize enabled;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//Tan Nguyen
//    [self.items release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithItems:(NSArray *)imageItems selectedItems:(NSArray *)selectedImageItems segmentSize:(CGSize)segmentSize delegate:(NSObject<KFSegmentedControlDelegate>*)theDelegate {
	if ((self = [super init])) {
		// Init array to store buttons
        //Tan Nguyen
        if (items != nil) {
//            [items release];
            items = nil;
        }
		self.items = [NSMutableArray array];
		
		
		// Init the delegate
		self.delegate = theDelegate;
		
		// Number of segments
		NSInteger segmentCount = [imageItems count];
		
		// Set our frame size
		self.frame = CGRectMake(0, 0, segmentCount * segmentSize.width, segmentSize.height);
		
		// Create all our buttons
		CGFloat horizontalOffset = 0;
		NSInteger i = 0;
		for (NSString *imageName in imageItems) {
			NSString *selectedImageName = [selectedImageItems objectAtIndex:i];
			
			UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];// retain
			[button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
			[button setImage:[UIImage imageNamed:selectedImageName] forState:UIControlStateSelected];
			[button setImage:[UIImage imageNamed:selectedImageName] forState:UIControlStateHighlighted];
			[button setFrame:CGRectMake(horizontalOffset, 0.0, segmentSize.width, segmentSize.height)];
			[button addTarget:self action:@selector(touchDownAction:) forControlEvents:UIControlEventTouchDown];
			
			// Increment horizontal offset
			horizontalOffset += segmentSize.width;
			
			[self.items addObject:button];
			
			[self addSubview:button];
//			[button release];
            
			i++;
		}
		
	}
	
	return self;
}

- (id)initWithButtonItems:(NSArray *)buttonItems andDelegate:(NSObject<KFSegmentedControlDelegate>*)theDelegate {
	if ((self = [super init])) {
		// Init array to store buttons
		self.items = [NSMutableArray array];
		
		// Init the delegate
		self.delegate = theDelegate;
		
		// Number of segments
		NSInteger segmentCount = [buttonItems count];
		
		CGFloat totalWidth = 0;
		CGFloat segmentWidth = 0;
		
		// Get first button item
		for (UIButton *button in buttonItems) {
			totalWidth += button.frame.size.width;
			
			segmentWidth = button.frame.size.width;
			
			[self.items addObject:button];
		}
		
		// Release the passed button items
//		[buttonItems release];
		
		// Set width (Need buttons to overlap somewhat)
		self.frame = CGRectMake(0, 0, totalWidth - (segmentCount - 1), segmentWidth);
		
		CGFloat horizontalOffset = 0;
		
		// Go through setting frame and button targets
		for (UIButton *button in self.items) {
			// Register for touch events
			[button addTarget:self action:@selector(touchUpInsideAction:) forControlEvents:UIControlEventTouchUpInside];
			[button addTarget:self action:@selector(otherTouchesAction:) forControlEvents:UIControlEventTouchUpOutside];
			[button addTarget:self action:@selector(otherTouchesAction:) forControlEvents:UIControlEventTouchDragOutside];
			[button addTarget:self action:@selector(otherTouchesAction:) forControlEvents:UIControlEventTouchDragInside];
			
			// Set button position (minus horizontal offset so we get slight overlap)
			if (horizontalOffset > 0) {
				horizontalOffset--;
			}
			
			// Set button frame
			button.frame = CGRectMake(horizontalOffset, 0.0, button.frame.size.width, button.frame.size.height);
			
			// Increment horizontal offset
			horizontalOffset += button.frame.size.width;
			
			[self addSubview:button];
		}
	}
	
	return self;
}

#pragma mark -
#pragma mark Class Methods

- (void)dimAllButtonsExceptSelectedIndex {
	NSInteger i = 0;
	for (UIButton *button in self.items) {
		if (i == self.selectedSegmentIndex) {
			button.selected = YES;
			//button.highlighted = YES;
		} else {
			button.selected = NO;
			//button.highlighted = NO;
		}
		
		i++;
	}
}

#pragma mark -
#pragma mark Set Methods

- (void)setSelectedSegmentIndex:(NSInteger)index {
	//self.selectedSegmentIndex = index;
	selectedSegmentIndex = index;
	
	[self dimAllButtonsExceptSelectedIndex];
	
	if ([self.delegate respondsToSelector:@selector(touchUpInsideSegmentIndex:)]) {
		[self.delegate touchUpInsideSegmentIndex:0];
	}
}

- (void)setEnabled:(BOOL)isEnabled {
	enabled = isEnabled;
	
	if ([self enabled] == TRUE) {
		for (UIButton *button in self.items) {
			[button setEnabled:TRUE];
		}
	} else {
		for (UIButton *button in self.items) {
			[button setEnabled:FALSE];
		}
	}

}

#pragma mark -
#pragma mark Button Actions


- (void)touchDownAction:(UIButton *)button {
	NSInteger i = 0;
	for (UIButton *localButton in self.items) {
		if (localButton == button) {
			break;
		}
		i++;
	}
	
	[self setSelectedSegmentIndex:i];

}

- (void)touchUpInsideAction:(UIButton *)button {
	//[self dimAllButtonsExcept:button];
	NSInteger i = 0;
	for (UIButton *localButton in self.items) {
		if ([localButton isEqual:button]) {
			break;
		}
		i++;
	}
	
	// Error
	if (i >= [self.items count]) {
		return;
	}
	
	self.selectedSegmentIndex = i;
	
	[self dimAllButtonsExceptSelectedIndex];
	
	if ([self.delegate respondsToSelector:@selector(touchUpInsideSegmentIndex:)]) {
		[self.delegate touchUpInsideSegmentIndex:0];
	}
}

- (void)otherTouchesAction:(UIButton *)button {
	[self dimAllButtonsExceptSelectedIndex];
}

@end
