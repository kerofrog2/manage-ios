//
//  MAppDelegate.h
//  ManageII
//
//  Created by Kerofrog on 11/21/13.
//  Copyright (c) 2013 Kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Google Analytics lib
#import "GAI.h"

// Dropbox
#import <dropbox/dropbox.h>

// Globals
#import "Globals.h"

// Code for Pro build
#import <AudioToolbox/AudioToolbox.h>

// Admob
#import "GADBannerViewDelegate.h"

#import "ControlPanelViewController.h"

#import "ListViewController.h"

#import "NoteViewController.h"

#import "TaskList.h"

#import "SettingsViewController.h"

// Data Collections
@class TaskListCollection;
@class MGSplitViewController;

@protocol MAppDelegate <NSObject>

-(void) didLoginDropBoxWithAccount:(DBAccount *)account;

@end

@interface MAppDelegate : NSObject <UIApplicationDelegate,SettingsDelegate> {
    
    UIWindow					*window;
	UINavigationController		*navigationController;
    
    UILocalNotification         *localNotifi;
    
    TaskListCollection		*refMasterTaskListCollection;
    TaskList                *refTaskList;
    NotebookCollection      *notebookCollection;
    TaskListCollection      *refTaskListCollection;
    
    
    // Tag Collection
    TagCollection			*tagCollection;						// The collection of tags
    ListItem                *currentListItem;
    
    MGSplitViewController *splitViewController;
    
    UIPopoverController				*settingsPopoverController;
    UIButton *settingsButton;
}

@property (strong, nonatomic) UIWindow *window;
// Admob's variable
@property (nonatomic, retain) GADBannerView *adBanner;

@property (nonatomic, retain) UINavigationController *navigationController;

@property (nonatomic, strong) UILocalNotification         *localNotifi;

// Google Analytics Tracker
@property(nonatomic, retain) id<GAITracker> tracker;

// Dropbox's variable.
@property (nonatomic, retain) DBFilesystem *dbFileSystem;

@property (nonatomic, strong) id<MAppDelegate> appDelegate;

@property (nonatomic, strong)  MGSplitViewController *splitViewController;

// Left panel
@property (strong, nonatomic)  ControlPanelViewController *rootControlPanel;

// Note
@property (strong, nonatomic) NotebookCollection *notebookCollection;

// Note View
@property (strong, nonatomic) NoteViewController *noteViewController;
@property (strong, nonatomic) ListViewController *listViewController;

#pragma mark Class Methods
- (void)createResourceFolders;

#pragma mark Custom Methods
- (void)archiveCheck;
- (void)databaseChecks;
- (void)updateSettings:(NSString *)version;

@end
