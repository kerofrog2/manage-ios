//
//  HitTestContainerView.m
//  Manage
//
//  Created by Cliff Viegas on 16/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "HitTestContainerView.h"


@implementation HitTestContainerView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		self.frame = frame;
		
		// Set ref parent view to nil
		refParentView = nil;
    }
    return self;
}

- (void)setParentView:(UIView *)parentView {
	refParentView = parentView;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
	// Need to know where this lands and whether that is where a 'button' is
	if ([self pointInside:point withEvent:event] && [self isUserInteractionEnabled]) {
		if (refParentView != nil) {
			return refParentView;
		}
	} 
	return nil;
}

- (void)dealloc {
//    [super dealloc];
}


@end

