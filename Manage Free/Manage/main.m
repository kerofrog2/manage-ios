//
//  main.m
//  Manage
//  Created by Cliff Viegas on 16/06/10.
//  Copyright kerofrog 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"ManageAppDelegate");
    [pool release];
    return retVal;
    
}
