//
//  GLDrawES2ViewController.h
//  GLDrawES2
//
//  Created by Adam Lockhart on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <OpenGLES/EAGL.h>

#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import "Structures.h"

@protocol GLDrawViewDelegate <NSObject>
-(void)infoUpdatedWithData: (NSMutableDictionary*) data;
-(void)infoUpdated;
-(void)viewCleared;
@end

@class  EAGLView;

@interface GLDrawES2ViewController : UIViewController {
@private
    GLuint program;
    
    BOOL eraserSelected;
    
    CGRect rect;
    
    vertex p0,p1,p2,p3,p4;
    GLfloat eraseRadius, regionSize;
    
    vertex lastGoodPoint;
    vertex drawVertex;
    tangent t1, t2;
    GLfloat width2,width3,width4,width5;
    
    NSUndoManager *undoManager;
    
    GLfloat xc, yc, scale, strokeWidth, widthFactor;
    
    GLfloat mvp[16]; // model view projection matrix
    GLfloat left, right, top, bottom; // variables for view volume
    
    int eraseRegionMessage, // describe erase region -- see enum
        strokeCount; // number of vertices in new stroke need; can start drawing after 3
    
    rgba myColor;
    
    NSMutableIndexSet *removals;    // Indices for dots to be erased.
    NSMutableArray *neighbors;      // Used to store keys for regions surrounding scissor erase region
    NSMutableDictionary *regionMap;
    NSMutableDictionary *tempDic;
    
    NSMutableArray *knotRemovals;
    
    dispatch_queue_t eraseQueue;
    
    CGFloat maxLineWidth;
    CGFloat minLineWidth;
    CGFloat strokeStep;
}

@property (nonatomic, retain) NSUndoManager *undoManager;
@property (assign) id <GLDrawViewDelegate> delegate;
@property (assign) BOOL eraserSelected;
@property (nonatomic, retain) UIColor *selectedColour;
@property (assign) BOOL passThroughTouches;

// getter is atomic but setter is not
@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) GLfloat scale;
@property ( assign) GLfloat removal;

@property (assign)  CGFloat maxLineWidth;
@property (assign)  CGFloat minLineWidth;
@property (assign)  CGFloat strokeStep;

-(void)initialFrame: (CGRect) frame;

-(NSDictionary*) drawingData;
-(void) clearView;

// Erase toggle - toggles erase and returns a value for the erase button
-(BOOL) eraseToggle;
-(void) drawFrame;

-(void) setDrawingData: (NSDictionary*) data;
-(void) setCenter: (CGPoint) center;
-(void) setColour: (UIColor*) newColor;
-(void) restoreState: (NSMutableDictionary*) state;
-(void) undo;
-(void) redo;
-(void) updateDrawingWithData: (NSMutableDictionary*) data;

-(void) changeWidth;
-(UIImage *) UIImage;
- (UIImage *)getUIImage;

@end
