//
//  Shader.fsh
//  GLDrawES2
//
//  Created by Adam Lockhart on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

varying lowp vec4 colorVarying;

void main() {
    // perform premuliplication
    gl_FragColor = vec4(colorVarying.rgb*colorVarying.a,colorVarying.a);
}
