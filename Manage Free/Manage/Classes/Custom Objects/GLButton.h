//
//  GLButton.h
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 25/09/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// GLDrawView Controller
#import "GLDrawES2ViewController.h"

@interface GLButton : UIButton {
   // GLDrawES2ViewController     *drawingLayer;
}

//@property (nonatomic, retain) GLDrawES2ViewController *drawingLayer;

@end
