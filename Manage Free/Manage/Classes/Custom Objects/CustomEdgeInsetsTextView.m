//
//  CustomTextView.m
//  Manage
//
//  Created by Cliff Viegas on 10/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "CustomEdgeInsetsTextView.h"

@implementation CustomEdgeInsetsTextView

// Over-riding edge insets
- (UIEdgeInsets) contentInset { 
    return UIEdgeInsetsZero; 
};

@end
