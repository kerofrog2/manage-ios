//
//  ToDoTableViewCell.m
//  Manage
//
//  Created by Cliff Viegas on 5/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ListItemTableViewCell.h"

// Quartzcore
#import <QuartzCore/QuartzCore.h>

// Method Helper
#import "MethodHelper.h"

// Data Models
#import "ListItem.h"
#import "Tag.h"
#import "ListItemTag.h"

// Data Collections
#import "TagCollection.h"
#import "ListItemTagCollection.h"

// Views
#import "QuartzTagView.h"

// #define kPreviewTableViewPortrait				CGRectMake(15, 10, 380, 530)
// #define kPageImagePortrait					CGRectMake(43, 30, 684, 891)


// Private Methods
@interface ListItemTableViewCell()
- (UIImage *)getRandomHighlighterImageForColor:(NSInteger)theColor andSide:(NSInteger)theSide;
@end

@implementation ListItemTableViewCell

@synthesize delegate;
@synthesize taskItem;
@synthesize taskDate;
@synthesize taskCompleted;
@synthesize scribbleImageView;
@synthesize repeatingTaskImageView;
@synthesize highlighterView;
@synthesize leftHighlighterImageView;
@synthesize rightHighlighterImageView;
@synthesize tagsView;
@synthesize tagsFont;

// Updated properties
@synthesize cellNotesImageView;
@synthesize taskItemFrame;
@synthesize isSubtask;
@synthesize isPreview;
@synthesize isNonListSort;
@synthesize hasTags;
@synthesize isArchived;

//@synthesize drawingLayer;

static UIFont *taskItemFont = nil;
static UIFont *taskDateFont = nil;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    //Tan Nguyen fix leaks
	[taskItem release];
	[taskDate release];
 //   [drawingLayer release];
	[taskCompleted release];
	[scribbleImageView release];
	[highlighterView release];
//	if (nil != leftHighlighterImageView)[leftHighlighterImageView release];
//	if ([rightHighlighterImageView retainCount]> 0)[rightHighlighterImageView release];
	[tagsView release];
	[tagsFont release];
	[cellNotesImageView release];
    [repeatingTaskImageView release];
	
    [super dealloc];
}


+ (void)initialize
{
	if(self == [ListItemTableViewCell class])
	{
		taskItemFont = [[UIFont fontWithName:@"Helvetica" size:14.0] retain];
		taskDateFont = [[UIFont fontWithName:@"Helvetica" size:11.0] retain];

		// this is a good spot to load any graphics you might be drawing in -drawContentView:
		// just load them and retain them here (ONLY if they're small enough that you don't care about them wasting memory)
		// the idea is to do as LITTLE work (e.g. allocations) in -drawContentView: as possible
	}
}

#pragma mark -
#pragma mark Override Setters

- (void)setTaskItem:(NSString *)s {
	[taskItem release];
	taskItem = [s copy];
	[self setNeedsDisplay];
}

- (void)setTaskDate:(NSString *)s {
	[taskDate release];
	taskDate = [s copy];
	[self setNeedsDisplay];
}

#pragma mark -
#pragma mark Draw Content View

- (void)drawContentView:(CGRect)r {
	CGContextRef context = UIGraphicsGetCurrentContext();

	UIColor *backgroundColor = [UIColor colorWithRed:242.0 / 255.0 
											   green:242.0 / 255.0 
												blue:242.0 / 255.0 
											   alpha:1.0];
    //UIColor *backgroundColor = [UIColor clearColor];
	UIColor *redOverdueColor = [UIColor colorWithRed:0.8274509 green:0.0235294 blue:0.0 alpha:1.0];
	UIColor *textColor = [UIColor darkTextColor];
	
	// Draw Background
	[backgroundColor set];
	CGContextFillRect(context, r);
	
	// Draw tags if needed
	// Draw tags view
	if ([self hasTags]) {
		if (self.repeatingTaskImageView.image != nil) {
			self.tagsView.frame = repeatingTaskTagsViewFrame;
		}
		
		UIGraphicsBeginImageContext(self.tagsView.bounds.size);
		[tagsView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *tagsViewImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		[tagsViewImage drawInRect:self.tagsView.frame];
	}
	
	// Draw text after setting text color
	[textColor set];
	
	if ([self.taskCompleted isSelected] == TRUE || [self isArchived]) {
		CGContextSetAlpha(context, 0.3f);
	} 
	[taskItem drawInRect:taskItemFrame withFont:taskItemFont lineBreakMode:UILineBreakModeTailTruncation alignment:UITextAlignmentLeft];
	
	if (!self.editing) {
		CGRect newTaskDateFrame = taskDateFrame;
		
		if (self.repeatingTaskImageView.image != nil) {
			newTaskDateFrame = CGRectMake(taskDateFrame.origin.x - 25, taskDateFrame.origin.y, 
										taskDateFrame.size.width, taskDateFrame.size.height);
		}
		
		if ([taskDate isEqualToString:@"overdue"]) {
			[redOverdueColor set];
		}
		[taskDate drawInRect:newTaskDateFrame withFont:taskDateFont lineBreakMode:UILineBreakModeClip alignment:UITextAlignmentRight];
	}
	[textColor set];
	// [cell.detailTextLabel setTextColor:[UIColor colorWithRed:0.8274509 green:0.0235294 blue:0.0 alpha:1.0]];
	
	CGContextSetAlpha(context, 1.0f);
	
	
	[backgroundColor set];
	
	// Draw the highlighters
	if (self.highlighterView.frame.size.width > 0 && [self isArchived] == FALSE) {
		UIGraphicsBeginImageContext(self.highlighterView.bounds.size);
		[highlighterView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *highlighterViewImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		[highlighterViewImage drawInRect:self.highlighterView.frame];
	}
	
	// Draw left highlighter
	if (self.leftHighlighterImageView.image != nil && [self isArchived] == FALSE) {
		UIGraphicsBeginImageContext(self.leftHighlighterImageView.bounds.size);
		[self.leftHighlighterImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *leftHighlighterImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		[leftHighlighterImage drawInRect:self.leftHighlighterImageView.frame];
	}
	
	// Draw right highlighter
	if (self.rightHighlighterImageView.image != nil && [self isArchived] == FALSE) {
		UIGraphicsBeginImageContext(self.rightHighlighterImageView.bounds.size);
		[self.rightHighlighterImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *rightHighlighterImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		[rightHighlighterImage drawInRect:self.rightHighlighterImageView.frame];
	}
	
	// Draw notes image
	if (self.cellNotesImageView.image != nil) {
		UIGraphicsBeginImageContext(self.cellNotesImageView.bounds.size);
		[self.cellNotesImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *cellNotesImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		[cellNotesImage drawInRect:self.cellNotesImageView.frame];
	}
	
	// Draw scribble
	if (self.scribbleImageView.image != nil) {
		UIGraphicsBeginImageContext(self.scribbleImageView.bounds.size);
		[self.scribbleImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *scribbleImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		[scribbleImage drawInRect:self.scribbleImageView.frame];
	}
    


	
	// Draw repeating tasks image view
	if (self.repeatingTaskImageView.image != nil) {
		UIGraphicsBeginImageContext(self.repeatingTaskImageView.bounds.size);
		[self.repeatingTaskImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *repeatingTaskImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		[repeatingTaskImage drawInRect:self.repeatingTaskImageView.frame];
	}
}

- (void)loadDrawingLayerForListItem:(ListItem *)listItem {
/*    refListItem = listItem;
    drawingLayer = [GLDrawES2ViewController alloc];
    [drawingLayer initialFrame:CGRectMake(1500, 0, 1500, 132)];

    [drawingLayer.view setUserInteractionEnabled:FALSE];

    
    [drawingLayer drawFrame];
    drawingLayer.passThroughTouches = YES;
    //[self addSubview:drawingLayer.view];
    
    UIImage *myImage = [drawingLayer getUIImage];
    myImage = [MethodHelper scaleImage:myImage maxWidth:500 maxHeight:44];

    //UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
    
    [self.scribbleImageView setImage:myImage];
    
    if (isSubtask == FALSE && isPreview == FALSE) {
        [self.scribbleImageView setFrame:kListItemScribbleRect];
    } else if (isPreview) {
        [self.scribbleImageView setFrame:kListItemPreviewScribbleRect];
    } else if (isSubtask) {
        [self.scribbleImageView setFrame:kSubListItemScribbleRect];
    } 
    
    //[self addSubview:imageView];
    //[imageView release];
    //[drawingLayer.view removeFromSuperview];
    [drawingLayer.view removeFromSuperview];
    [drawingLayer release];
    drawingLayer = nil; */
    //[self addSubview:drawingLayer.view];
        
        

        //[drawingLayer drawFrame];
        //[self performSelector:@selector(showDrawingFrame) withObject:nil afterDelay:0.0000001];
   // }
}

- (void)loadGLDrawingLayerForListItem:(ListItem *)listItem {
   /* refListItem = listItem;
    drawingLayer = [GLDrawES2ViewController alloc];
    
    if (isSubtask == FALSE && isPreview == FALSE) {
        [drawingLayer initialFrame:kListItemScribbleRect];
    } else if (isPreview) {
        [drawingLayer initialFrame:kListItemPreviewScribbleRect];
    } else if (isSubtask) {
        [drawingLayer initialFrame:kSubListItemScribbleRect];
    } 
    
    
    [drawingLayer.view setUserInteractionEnabled:FALSE];

    
    
    drawingLayer.passThroughTouches = YES;
    //[self addSubview:drawingLayer.view];
    
    self.scribbleImageView = nil;
    
    [self addSubview:drawingLayer.view];
    [drawingLayer setScale:3.f];
    [drawingLayer drawFrame];*/
    //[drawingLayer release];
    

}

- (void)showDrawingFrame {

}

- (void)layoutSubviews {
	CGRect b = [self bounds];
	//b.size.height -= 1; // leave room for the separator line
   // b.size.width += 30; // allow extra width to slide for editing
   // b.origin.x -= (self.editing) ? 0 : 30; // start 30px left unless editing
	if ([self isPreview] == FALSE && [self isSubtask] == FALSE) {
		[self.taskCompleted setFrame:kTaskCompletedFrame];
	} else if ([self isPreview] == FALSE && [self isSubtask] == TRUE) {
		[self.taskCompleted setFrame:kSubTaskCompletedFrame];
	} else if ([self isPreview] == TRUE && [self isSubtask] == FALSE) {
		[self.taskCompleted setFrame:kPreviewTaskCompletedFrame];
	} else if ([self isPreview] == TRUE && [self isSubtask] == TRUE) {
		[self.taskCompleted setFrame:kPreviewSubTaskCompletedFrame];
	}
	
	if ([self isNonListSort]) {
		[self.taskCompleted setFrame:kTaskCompletedFrame];
	}
	
	if ([self isEditing] && !self.showingDeleteConfirmation) {
		b.origin.x += 30;
		self.taskCompleted.frame = CGRectMake(self.taskCompleted.frame.origin.x + 30, 
											  self.taskCompleted.frame.origin.y,
											  self.taskCompleted.frame.size.width,
											  self.taskCompleted.frame.size.height);
	} 
    [contentView setFrame:b];
    [super layoutSubviews];
}

#pragma mark -
#pragma mark Initialisation


- (void)newValuesForInitWithStyle {
    if (self.tagsView == nil) {
        UIView *tagsViewTemp = [[UIView alloc] initWithFrame:kTagsViewFrame];
        self.tagsView = tagsViewTemp;
        [tagsViewTemp release];
    }
    
    if (self.cellNotesImageView == nil) {
        UIImageView *cellNotesImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
        self.cellNotesImageView = cellNotesImageViewTemp;
        [cellNotesImageViewTemp release];
    }
    
    if (self.highlighterView == nil) {
        UIView *highlighterViewTemp = [[UIView alloc] initWithFrame:CGRectZero];
        self.highlighterView = highlighterViewTemp;
        [highlighterView release];
    }
    
    if (self.leftHighlighterImageView == nil) {
        UIImageView *leftHighlighterImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.leftHighlighterImageView = leftHighlighterImageViewTemp;
        [leftHighlighterImageViewTemp release];
    }
    
    if (self.rightHighlighterImageView == nil) {
        UIImageView *rightHighlighterImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.rightHighlighterImageView = rightHighlighterImageViewTemp;
        [rightHighlighterImageViewTemp release];
    }
    
    if (self.scribbleImageView == nil) {
        UIImageView *scribbleImageViewTemp = [[UIImageView alloc] initWithFrame:kListItemScribbleRect];
        self.scribbleImageView = scribbleImageViewTemp;
        [scribbleImageViewTemp release];
    }
    
    if (self.repeatingTaskImageView == nil) {
        UIImageView *repeatingTaskImageViewTemp = [[UIImageView alloc] initWithFrame:kPreviewTaskRepeatingFrame];
        self.repeatingTaskImageView = repeatingTaskImageViewTemp;
        [repeatingTaskImageViewTemp release];
    }

    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        //Tan Nguyen fix leaks
        // Initialization code
        UIView * tagsViewTemp = [[UIView alloc] initWithFrame:kTagsViewFrame];
		self.tagsView = tagsViewTemp;
		[self.tagsView setBackgroundColor:[UIColor colorWithRed:242.0 / 255.0 
														  green:242.0 / 255.0 
														   blue:242.0 / 255.0 
														  alpha:1.0]];
        [tagsViewTemp release];

		self.taskCompleted = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
		[self.taskCompleted setFrame:kTaskCompletedFrame];
		[self.taskCompleted addTarget:self action:@selector(taskCompletedAction) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:self.taskCompleted];
		//[self.taskCompleted release];
		
		// Load the notes image
//        if (self.cellNotesImageView != nil) {
//            [self.cellNotesImageView release];
//            self.cellNotesImageView = nil;
//            
//        }
        UIImageView * cellNotesImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)] ;
        self.cellNotesImageView = cellNotesImageViewTemp;
		self.cellNotesImageView.image = nil;
        [cellNotesImageViewTemp release];
//		self.cellNotesImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
		self.cellNotesImageView.image = nil;
		
		// Init the highlighter views
//        if (self.highlighterView != nil) {
//            [self.highlighterView release];
//            self.highlighterView = nil;
//        }

		UIView * highlighterViewTemp = [[UIView alloc] initWithFrame:CGRectZero];
        self.highlighterView = highlighterViewTemp;
        [highlighterViewTemp release];
        
//        if (self.leftHighlighterImageView != nil) {
//            [self.leftHighlighterImageView release];
//            self.leftHighlighterImageView = nil;
//        }
        
		UIImageView * leftHighlighterImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.leftHighlighterImageView = leftHighlighterImageViewTemp;
        self.leftHighlighterImageView.image = nil;
        [leftHighlighterImageViewTemp release];
        
		
//		if (self.rightHighlighterImageView != nil) {
//            [self.rightHighlighterImageView release];
//            self.rightHighlighterImageView = nil;
//        }
        
        UIImageView * rightHighlighterImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.rightHighlighterImageView = rightHighlighterImageViewTemp;
		self.rightHighlighterImageView.image = nil;
        [rightHighlighterImageViewTemp release];
		
		// Init the scribble image view
//        if (self.scribbleImageView != nil) {
//            [self.scribbleImageView release];
//            self.scribbleImageView = nil;
//        }
		UIImageView * scribbleImageViewTemp = [[UIImageView alloc] initWithFrame:kListItemScribbleRect];
        self.scribbleImageView = scribbleImageViewTemp;
		[self.scribbleImageView setContentMode:UIViewContentModeLeft];
		self.scribbleImageView.image = nil;
        [scribbleImageViewTemp release];
		
		// Init the tags font
		self.tagsFont = [UIFont fontWithName:@"Helvetica" size:11.0];
		
		// Set task item and date frame
		taskItemFrame = kTaskItemFrame;
		taskDateFrame = kTaskDateFrame;
		
		// Init task repeating frame
//        if (self.repeatingTaskImageView != nil) {
//            [self.repeatingTaskImageView release];
//            self.repeatingTaskImageView = nil;
//        }
		UIImageView * repeatingTaskImageViewTemp = [[UIImageView alloc] initWithFrame:kTaskRepeatingFrame];
        self.repeatingTaskImageView = repeatingTaskImageViewTemp;
		self.repeatingTaskImageView.image = nil;
        [repeatingTaskImageViewTemp release];
		
		// Set is preview
		self.isPreview = FALSE;
		self.isNonListSort = FALSE;
		self.hasTags = FALSE;
		self.isArchived = FALSE;
    }
    return self;
}

//- (void) newValuesForInitWithPreviewStyle{
//    if (tagsView == nil) {
//        tagsView = [[UIView alloc] initWithFrame:kTagsViewFrame];
//    }
//    if (cellNotesImageView == nil) {
//        cellNotesImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
//    }
//    if (highlighterView == nil) {
//        highlighterView = [[UIView alloc] initWithFrame:CGRectZero];
//    }
//    if (leftHighlighterImageView == nil) {
//        leftHighlighterImageView = [[UIView alloc] initWithFrame:CGRectZero];
//    }
//    if (rightHighlighterImageView == nil) {
//        rightHighlighterImageView = [[UIView alloc] initWithFrame:CGRectZero];
//    }
//    
//    if (scribbleImageView == nil) {
//        scribbleImageView = [[UIImageView alloc] initWithFrame:kListItemScribbleRect];
//    }
//    if (repeatingTaskImageView == nil) {
//        self.repeatingTaskImageView = [[UIImageView alloc] initWithFrame:kPreviewTaskRepeatingFrame];
//    }
//    
//    
//}

- (id)initWithPreviewStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
        //Tan Nguyen fix leaks
//        if (self.tagsView != nil){
//            [self.tagsView release];
//            self.tagsView = nil;
//        }
        UIView * tagsViewTemp = [[UIView alloc] initWithFrame:kTagsViewFrame];
		self.tagsView = tagsViewTemp;
		//self.tagsView = [[UIView alloc] initWithFrame:kTagsViewFrame];
		[self.tagsView setBackgroundColor:[UIColor colorWithRed:242.0 / 255.0 
														  green:242.0 / 255.0 
														   blue:242.0 / 255.0 
														  alpha:1.0]];
        [tagsViewTemp release];
		
		self.taskCompleted = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
		[self.taskCompleted setFrame:kPreviewTaskCompletedFrame];
		[self.taskCompleted addTarget:self action:@selector(taskCompletedAction) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:self.taskCompleted];
		//[self.taskCompleted release];
		
		// Load the notes image
//        if (self.cellNotesImageView != nil) {
//            [self.cellNotesImageView release];
//            self.cellNotesImageView = nil;
//            
//        }
		UIImageView * cellNotesImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
        self.cellNotesImageView = cellNotesImageViewTemp;
		self.cellNotesImageView.image = nil;
        [cellNotesImageViewTemp release];
		
		// Init the highlighter views
//        if (self.highlighterView != nil) {
//            [self.highlighterView release];
//            self.highlighterView = nil;
//            
//        }
		UIView * highlighterViewTemp = [[UIView alloc] initWithFrame:CGRectZero];
        self.highlighterView = highlighterViewTemp;
        [highlighterViewTemp release];
        
        //        if (self.leftHighlighterImageView != nil) {
        //            [self.leftHighlighterImageView release];
        //            self.leftHighlighterImageView = nil;
        //        }
        
		UIImageView * leftHighlighterImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.leftHighlighterImageView = leftHighlighterImageViewTemp;
        self.leftHighlighterImageView.image = nil;
        [leftHighlighterImageViewTemp release];
        
		
        //		if (self.rightHighlighterImageView != nil) {
        //            [self.rightHighlighterImageView release];
        //            self.rightHighlighterImageView = nil;
        //        }
        
        UIImageView * rightHighlighterImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.rightHighlighterImageView = rightHighlighterImageViewTemp;
		self.rightHighlighterImageView.image = nil;
        [rightHighlighterImageViewTemp release];
		
		// Init the scribble image view
        //        if (self.scribbleImageView != nil) {
        //            [self.scribbleImageView release];
        //            self.scribbleImageView = nil;
        //        }
		UIImageView * scribbleImageViewTemp = [[UIImageView alloc] initWithFrame:kListItemScribbleRect];
        self.scribbleImageView = scribbleImageViewTemp;
		[self.scribbleImageView setContentMode:UIViewContentModeLeft];
		self.scribbleImageView.image = nil;
        [scribbleImageViewTemp release];
		
		// Init the tags font
		self.tagsFont = [UIFont fontWithName:@"Helvetica" size:11.0];
		
		// Set task item and date frame
		taskItemFrame = kPreviewTaskItemFrame;
		taskDateFrame = kPreviewTaskDateFrame;
		
		// Init task repeating frame
        UIImageView * repeatingTaskImageViewTemp = [[UIImageView alloc] initWithFrame:kPreviewTaskRepeatingFrame];
        self.repeatingTaskImageView = repeatingTaskImageViewTemp;
		self.repeatingTaskImageView.image = nil;
        [repeatingTaskImageViewTemp release];
		
		// Set tagsview and font to nil
		//self.tagsView = nil;
		//self.tagsFont = nil;
		
		// Set is preview
		self.isPreview = TRUE;
		self.isNonListSort = FALSE;
		self.hasTags = FALSE;
		self.isArchived = FALSE;
    }
    return self;
}

#pragma mark -
#pragma mark Editing Mode Methods

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	[self.tagsView setHidden:editing];
	//[self.taskDate setHidden:editing];
	[self setNeedsDisplay];
	[super setEditing:editing animated:animated];
}

#pragma mark -
#pragma mark Class Methods

- (void)loadTagsForListItem:(ListItem *)listItem andTagCollection:(TagCollection *)tagCollection {
	// Remove all subviews first
	for (int i = [[self.tagsView subviews] count] - 1; i >= 0; i--) {
		[[[self.tagsView subviews] objectAtIndex:i] removeFromSuperview];
	}
	
	// Exit if there are no tags
	if ([listItem.listItemTagCollection.listItemTags count] == 0) {
		// Set task date frame back to standard
		if (![self isPreview]) {
			taskDateFrame = kTaskDateFrame;
		} else if ([self isPreview]) {
			taskDateFrame = kPreviewTaskDateFrame;
		}
		
		return;
	}
	
	self.hasTags = TRUE;
	
	if ([listItem.dueDate length] > 0 && [self isArchived] == FALSE) {
		// Shift both frames
		if (![self isPreview]) {
			taskDateFrame = kShiftedTaskDateFrame;
			[self.tagsView setFrame:kShiftedTagsViewFrame];
		} else if ([self isPreview]) {
			taskDateFrame = kPreviewShiftedTaskDateFrame;
			[self.tagsView setFrame:kPreviewShiftedTagsViewFrame];
		}
	} else {
		if (![self isPreview]) {
			[self.tagsView setFrame:kTagsViewFrame];
		} else if ([self isPreview]) {
			[self.tagsView setFrame:kPreviewTagsViewFrame];
		}
		
	}

	
	CGFloat tagBuffer = 10.0;
	CGFloat tagBorder = 8.0;
	
	CGFloat totalBuffer = 0.0; //+ (tagBuffer / 2);
	
	NSInteger extraTagCount = 0;
	NSInteger indexCount = 0;
	
	CGFloat widthCheck = 595.0;
	if ([self isPreview]) {
		widthCheck = 305.0;
	}
	
	//for (ListItemTag *listItemTag in listItem.listItemTagCollection.listItemTags) {
	for (Tag *theTag in tagCollection.tags) {
		if ([listItem.listItemTagCollection containsTagWithID:theTag.tagID] == FALSE) {
			continue;
		}
	
		NSString *tagName = theTag.name; //[tagCollection getNameForTagWithID:listItemTag.tagID];
        
		CGSize tagSize = [tagName sizeWithFont:self.tagsFont];
		
		// Need to make sure total buffer is less than the right border of text view (add 100 to allow it to cut partway through)
		if ((widthCheck + 100) - (totalBuffer + tagSize.width + tagBorder) < taskItemFrame.origin.x + taskItemFrame.size.width) {
			extraTagCount++;
			if (indexCount == [listItem.listItemTagCollection.listItemTags count] - 1) {
				// Last index
				tagName = [NSString stringWithFormat:@"+%d", extraTagCount];
				tagSize = [tagName sizeWithFont:self.tagsFont];
			} else {
				indexCount++;
				continue;
			}
		}

		// Add the label to the tag view
		UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.tagsView.frame.size.width - totalBuffer - tagBorder - tagSize.width - 5, 
																	  0, 
																	  tagSize.width + tagBorder, self.tagsView.frame.size.height)];
		
		totalBuffer = totalBuffer + tagLabel.frame.size.width + tagBuffer;
		
		// Increase size of tags view if total buffer exceeds width
		/*if (totalBuffer > self.tagsView.frame.size.width) {
			CGFloat difference = totalBuffer - self.tagsView.frame.size.width;
			[self.tagsView setFrame:CGRectMake(tagsView.frame.origin.x - difference,
													tagsView.frame.origin.y,
													tagsView.frame.size.width + difference,
													tagsView.frame.size.height)];
			//[self.tagsView setFrame:self.tagsView.frame.origin]
		}*/
		
		[tagLabel setBackgroundColor:[UIColor clearColor]];
		[tagLabel setTextAlignment:UITextAlignmentCenter];
		[tagLabel setTextColor:[UIColor darkTextColor]];
		[tagLabel setText:tagName];
		[tagLabel setFont:self.tagsFont];
		
		QuartzTagView *quartzTagView = [[QuartzTagView alloc] initWithFrame:CGRectMake(tagLabel.frame.origin.x - (tagBorder / 2), 
																					   0, tagLabel.frame.size.width + tagBorder, 
																					   20)
                                        andTagColour:theTag.colour];
		// If this goes over task item then make it transparent
		[quartzTagView setAlpha:0.9f];
		[tagLabel setAlpha:0.9f];
		
		[self.tagsView addSubview:quartzTagView];
		[quartzTagView release];
		
		if (listItem.completed || listItem.archived) {
			[self.tagsView setAlpha:0.3f];
		} else {
			[self.tagsView setAlpha:1.0f];
		}

		
		repeatingTaskTagsViewFrame = CGRectMake(self.tagsView.frame.origin.x - 25, self.tagsView.frame.origin.y, 
												self.tagsView.frame.size.width, self.tagsView.frame.size.height);
		
		//[self.tagsView addSubview:tagLabel];
		[self.tagsView addSubview:tagLabel];
		[tagLabel release];
		
		// Increment index count
		indexCount++;
		
		// Position each label and reposition rest of row
	}
	
	// Reposition task item frame as needed
	if ([self isPreview]) {
		if (totalBuffer > 50) {
			CGFloat cutAmount = totalBuffer - 45;
			taskItemFrame = CGRectMake(taskItemFrame.origin.x, taskItemFrame.origin.y, 
									   taskItemFrame.size.width - cutAmount, taskItemFrame.size.height);
			
		}
		return;
	}
	
	if (widthCheck - totalBuffer < taskItemFrame.origin.x + taskItemFrame.size.width) {
		CGFloat cutAmount = (taskItemFrame.origin.x + taskItemFrame.size.width) - (widthCheck - totalBuffer);
		cutAmount += (tagBuffer * 2);
		
		taskItemFrame = CGRectMake(taskItemFrame.origin.x, taskItemFrame.origin.y, 
								   taskItemFrame.size.width - cutAmount, taskItemFrame.size.height);
	}

}

- (void)setHighlightStyle:(EnumHighlighterColor)highlighter {
	UIFont *myFont = [UIFont fontWithName:@"Helvetica" size:14.0];
	// Get the width of a string ...
	//NSString *taskItemString = [self.taskItem text];
	CGSize size = [self.taskItem sizeWithFont:myFont];
	
	if (size.width > taskItemFrame.size.width) {
		size.width = taskItemFrame.size.width;
	}
	
	if (size.width < 35) {
		size.width = 35;
	}
	
	if (highlighter == EnumHighlighterColorWhite || highlighter == EnumHighlighterColorNone) {
		[self.highlighterView setFrame:CGRectZero];
		self.leftHighlighterImageView.image = nil;
		self.rightHighlighterImageView.image = nil;
		
	} else if (highlighter == EnumHighlighterColorYellow) {
		[self.highlighterView setFrame:CGRectMake(taskItemFrame.origin.x, 
												  taskItemFrame.origin.y, 
												  size.width, 
												  taskItemFrame.size.height)];
		[self.highlighterView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:0.2 alpha:1.0]];
		[self.highlighterView setAlpha:0.3f];
		
		// Show the left highlighter view
		[self.leftHighlighterImageView setImage:[self getRandomHighlighterImageForColor:EnumHighlighterColorYellow 
																				andSide:EnumHighlightSideLeft]];
		[self.leftHighlighterImageView setFrame:CGRectMake(taskItemFrame.origin.x - 10, 
														   taskItemFrame.origin.y, 
														   10, 21)];
		//leftHighlighterFrame = CGRectMake(taskItemFrame.origin.x - 10, taskItemFrame.origin.y, 10, 21);
		
		[self.leftHighlighterImageView setAlpha:0.3f];
		 

		 // Show the right highlighter view
		[self.rightHighlighterImageView setImage:[self getRandomHighlighterImageForColor:EnumHighlighterColorYellow 
																				andSide:EnumHighlightSideRight]];
		[self.rightHighlighterImageView setFrame:CGRectMake(taskItemFrame.origin.x + size.width, 
														   taskItemFrame.origin.y, 
															10, 21)];

		//rightHighlighterFrame = CGRectMake(taskItemFrame.origin.x + size.width, taskItemFrame.origin.y, 10, 21);
		[self.rightHighlighterImageView setAlpha:0.3f];
		
	} else if (highlighter == EnumHighlighterColorRed) {
		[self.highlighterView setFrame:CGRectMake(taskItemFrame.origin.x, 
												  taskItemFrame.origin.y, 
												  size.width,
												  taskItemFrame.size.height)];
		[self.highlighterView setBackgroundColor:[UIColor colorWithRed:0.92941176 green:0.10980392 blue:0.14117647 alpha:1.0]];
		[self.highlighterView setAlpha:0.3f];
	
		// Show the left highlighter view
		[self.leftHighlighterImageView setImage:[self getRandomHighlighterImageForColor:EnumHighlighterColorRed 
																				andSide:EnumHighlightSideLeft]];
		[self.leftHighlighterImageView setFrame:CGRectMake(taskItemFrame.origin.x - 10, 
														   taskItemFrame.origin.y, 
														   10, 21)];
		//leftHighlighterFrame = CGRectMake(taskItemFrame.origin.x - 10, taskItemFrame.origin.y, 10, 21);
		
		[self.leftHighlighterImageView setAlpha:0.3f];
		
		// Show the right highlighter view
		[self.rightHighlighterImageView setImage:[self getRandomHighlighterImageForColor:EnumHighlighterColorRed 
																				 andSide:EnumHighlightSideRight]];
		[self.rightHighlighterImageView setFrame:CGRectMake(taskItemFrame.origin.x + size.width, 
															taskItemFrame.origin.y, 
															10, 21)];
		//rightHighlighterFrame = CGRectMake(taskItemFrame.origin.x + size.width, taskItemFrame.origin.y, 10, 21);
		
		[self.rightHighlighterImageView setAlpha:0.3f];
	
	}
}

- (void)setIndentLevel:(NSInteger)indent {
	CGFloat indentAmount = indent * self.indentationWidth;
	
	taskItemFrame = CGRectMake(taskItemFrame.origin.x + indentAmount, 
							   taskItemFrame.origin.y, 
							   taskItemFrame.size.width - indentAmount, 
							   taskItemFrame.size.height);
	
	
	[self.taskCompleted setFrame:CGRectMake(self.taskCompleted.frame.origin.x + indentAmount, 
											self.taskCompleted.frame.origin.y, 
											self.taskCompleted.frame.size.width, 
											self.taskCompleted.frame.size.height)];
}


//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

#pragma mark -
#pragma mark Button Actions

- (void)taskCompletedAction {
	// Will also need some sort of delegate here, bugger, to update state
	// Not ... needed, as the view controller will set a tag for the button.  Just need to retrieve this on call.
	// But must have delegate... still..
	
	// Do nothing if this is the archived section
	if ([self isArchived]) {
		return;
	}
	
	if ([self.taskCompleted isSelected] == FALSE) {
		[self.taskCompleted setSelected:YES];
		[self.tagsView setAlpha:0.3f];
	} else if ([self.taskCompleted isSelected] == TRUE) {
		[self.taskCompleted setSelected:NO];
		[self.tagsView setAlpha:1.0f];
	}
	
	[self.delegate taskCompleted:[self.taskCompleted isSelected] forListItemID:self.taskCompleted.tag]; 
	 // - (void)taskCompleted:(BOOL)isCompleted forListID:(NSInteger)theListID;
}

#pragma mark -
#pragma mark Scribble Image Methods

- (void)loadScribbleImageForListItem:(ListItem *)listItem andIsPreviewList:(BOOL)isPreviewList {
	// Display image if needed
	if ([[listItem scribble] length] > 0 && [listItem scribble] != nil) {
		self.scribbleImageView.image = nil;
        
        //if (self.drawingLayer != nil) {
        //    [self.drawingLayer.view removeFromSuperview];
       //     self.drawingLayer = nil;
        //}
		
        if ([listItem completed] == TRUE || [listItem archived] == TRUE) {
			[self.scribbleImageView setAlpha:0.2f];
		} else {
			[self.scribbleImageView setAlpha:1.0f];
		}
        
        NSString *scribblePath;
        
        NSRange glsRange = [listItem.scribble rangeOfString:@".gls"];
        
        if (glsRange.location != NSNotFound) { 
            
            NSString *pngScribble = [listItem.scribble substringToIndex:glsRange.location];
            pngScribble = [NSString stringWithFormat:@"%@.png", pngScribble];
            
            scribblePath = [NSString stringWithFormat:@"%@%@",
                            [MethodHelper getScribblePath],
                            pngScribble];
            
            //[self loadDrawingLayerForListItem:listItem];
        } else {
            scribblePath = [NSString stringWithFormat:@"%@%@",
                            [MethodHelper getScribblePath],
                            listItem.scribble];
        }
        
        UIImage *scribbleImage = [UIImage imageWithContentsOfFile:scribblePath];
        
        if (glsRange.location == NSNotFound) {
            scribbleImage = [MethodHelper scaleImage:scribbleImage 
                                            maxWidth:500 maxHeight:44];
        }
        
		
		if (isPreviewList == TRUE) {
			
			//[self.scribbleImageView setFrame:kListItemPreviewScribbleRect];
			
			scribbleImage = [MethodHelper cropImage:scribbleImage 
									  usingCropRect:CGRectMake(0, 0, 
															   self.taskItemFrame.size.width, 44)];
            [self.scribbleImageView setFrame:kListItemPreviewScribbleRect];
            

		} else if (self.taskItemFrame.size.width < 450) {
			
			scribbleImage = [MethodHelper cropImage:scribbleImage 
									  usingCropRect:CGRectMake(0, 0, 
															   self.taskItemFrame.size.width, 44)];
            [self.scribbleImageView setFrame:kListItemScribbleRect];
		}
		
		[self.scribbleImageView setImage:scribbleImage];
	} else {
		self.scribbleImageView.image = nil;
	}
}



#pragma mark -
#pragma mark Cell Notes Icon Methods

- (void)loadCellNotesImage {
	UIImage *notesIconImage = [MethodHelper scaleImage:[UIImage imageNamed:@"NotesIcon.png"] 
											  maxWidth:22 maxHeight:22];
	cellNotesImageView.image = notesIconImage; 
	
	CGSize size = [taskItem sizeWithFont:taskItemFont];
	if (size.width > taskItemFrame.size.width) {
		size.width = taskItemFrame.size.width;
	}
	
	[cellNotesImageView setFrame:CGRectMake(taskItemFrame.origin.x + size.width + 3, 
											0, cellNotesImageView.frame.size.width, 
											cellNotesImageView.frame.size.height)];
}

#pragma mark -
#pragma mark Repeating Task Image Methods

- (void)loadRepeatingTaskImage {
	UIImage *repeatingTaskImage = [UIImage imageNamed:@"RepeatActiveIcon.png"];
	repeatingTaskImageView.image = repeatingTaskImage;
}

#pragma mark -
#pragma mark Class Helper Methods

- (void)setStandardIndent {
	// Standard Task
	self.isNonListSort = TRUE;
	taskItemFrame = kTaskItemFrame;
	[self.taskCompleted setFrame:kTaskCompletedFrame];
	[self.scribbleImageView setFrame:kListItemScribbleRect];
}

- (void)setIndentForListItem:(ListItem *)listItem {
	self.isNonListSort = FALSE;
	if (listItem.parentListItemID == -1) {
		// Standard Task
		taskItemFrame = kTaskItemFrame;
		[self.taskCompleted setFrame:kTaskCompletedFrame];
		[self.scribbleImageView setFrame:kListItemScribbleRect];
		self.isSubtask = FALSE;
	} else {
		// Sub Task
		taskItemFrame = kSubTaskItemFrame;
		[self.taskCompleted setFrame:kSubTaskCompletedFrame];
		[self.scribbleImageView setFrame:kSubListItemScribbleRect];
		self.isSubtask = TRUE;
	}
}

- (void)setIndentForPreviewListItem:(ListItem *)previewListItem {
	if (previewListItem.parentListItemID == -1) {
		// Standard task
		taskItemFrame = kPreviewTaskItemFrame;
		[self.taskCompleted setFrame:kPreviewTaskCompletedFrame];
		[self.scribbleImageView setFrame:kListItemPreviewScribbleRect];
		self.isSubtask = FALSE;
	} else {
		// Sub task
		taskItemFrame = kPreviewSubTaskItemFrame;
		[self.taskCompleted setFrame:kPreviewSubTaskCompletedFrame];
		[self.scribbleImageView setFrame:kSubListItemPreviewScribbleRect];
		self.isSubtask = TRUE;
	}
}


- (UIImage *)getRandomHighlighterImageForColor:(NSInteger)theColor andSide:(NSInteger)theSide {
	//NSInteger randomNumber = (arc4random() % 5) + 1;
	NSInteger randomNumber = 1;

	if (theColor == EnumHighlighterColorRed) {
		if (theSide == EnumHighlightSideLeft) {
			NSString *imageName = [NSString stringWithFormat:@"RedHighLeft%d.png", randomNumber];
			return [UIImage imageNamed:imageName];
		} else if (theSide == EnumHighlightSideRight) {
			NSString *imageName = [NSString stringWithFormat:@"RedHighRight%d.png", randomNumber];
			return [UIImage imageNamed:imageName];
		}
		
	} else if (theColor == EnumHighlighterColorYellow) {
		if (theSide == EnumHighlightSideLeft) {
			NSString *imageName = [NSString stringWithFormat:@"YellowHighLeft%d.png", randomNumber];
			return [UIImage imageNamed:imageName];
		} else if (theSide == EnumHighlightSideRight) {
			NSString *imageName = [NSString stringWithFormat:@"YellowHighRight%d.png", randomNumber];
			return [UIImage imageNamed:imageName];
		}
	}
	
	return nil;
}

@end
