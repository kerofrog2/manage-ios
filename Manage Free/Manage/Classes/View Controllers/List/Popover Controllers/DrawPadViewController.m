//
//  DrawPadViewController.m
//  Manage
//
//  Created by Cliff Viegas on 14/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "DrawPadViewController.h"

// Data Models
#import "ListItem.h"
#import "DrawPoint.h"

// Data collections
#import "DrawPointCollection.h"

// Constants
#import "ListConstants.h"

// Method Helper
#import "MethodHelper.h"

// Quartzcore
#import <QuartzCore/QuartzCore.h>

// Constants
#define kLineWidth              5.0
#define kPreviewLayerSize       CGSizeMake(150, 132)

// Divide by 10 from 600
CGRect _ScrollControlFrame = {440, 193, 60, 35};


@interface DrawPadViewController()
// Private Methods
- (void)loadButtons;
- (void)loadScrollView;
- (void)loadLineWidthsSegmentedControl;
- (void)loadScrollControlWindow;
// For deselecting all buttons
- (void)deselectAll;
@end


@implementation DrawPadViewController

@synthesize delegate;
@synthesize isNewScribbleItem;
@synthesize drawingLayer;
//@synthesize previewLayer;
@synthesize dataStorage = _dataStorage;
@synthesize isPNGScribble;
@synthesize glScribbleDictionary;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[bluePenButton release];
//	[greenPenButton release];
//	[redPenButton release];
//	[blackPenButton release];
//	[dragButton release];
//	[eraserButton release];
	
   // [bluePenButton release];
   // [greenPenButton release];
    //[redPenButton release];
    //[blackPenButton release];
    //[dragButton release];
    //[undoButton release];
    //[redoButton release];
   // [eraserButton release];
   // [clearButton release];
    
    //[scrollControlWindow release];
    //[scrollControl release];
    
    
	// Scroll view objects
	[drawPadScrollView release];
	
    //[self.previewLayer release];
    //[_dataStorage release];
    [self.drawingLayer release];
    [clearActionSheet release];
    [lineWidthsSegmentedControl release];
    [dataStorage release];
    [self.glScribbleDictionary release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<DrawPadDelegate>)theDelegate andSize:(CGSize)theSize 
		   andListItem:(ListItem *)theListItem andPenColor:(EnumPenColor)thePenColor andNewScribbleItem:(BOOL)newScribbleItem {
	if ((self = [super init])) {
		// Assign the delegate
		self.delegate = theDelegate;
		
        // Assign is new scribble item
        self.isNewScribbleItem = newScribbleItem;
        
		// Set a background color
		/*UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iPadGroupTableViewBackground.png"]];
		[backgroundImageView setFrame:CGRectMake(0, 0, theSize.width, theSize.height)];
		[self.view addSubview:backgroundImageView];
		[backgroundImageView release];*/

		// 21  29  49
		// 21 28 47
		
		// The pen color needs to be passed in (sometimes.. but default it to black)
		penColor = thePenColor;
        
        self.glScribbleDictionary = [NSDictionary dictionary];
		
		// Assign the passed list item
		refListItem = theListItem;
        
        
        // Load the list item dictionary
        NSRange glsRange = [refListItem.scribble rangeOfString:@"gls"];
        if (glsRange.location != NSNotFound) {
            NSString *fileSource = [[MethodHelper getScribblePath] stringByAppendingPathComponent:refListItem.scribble];
            NSData *glScribbleData = [[NSData alloc] initWithContentsOfFile:fileSource];
            
            self.glScribbleDictionary = [MethodHelper decodeObject:glScribbleData andKey:@"GLScribbleData"];
            [glScribbleData release];
        }
        
        scribblePadContentSize = theSize;
        
        // Need a delegate call to say that this is the wrong 'item' to be calling if this is a png
        NSRange pngRange = [[refListItem scribble] rangeOfString:@"png"];
        if ([[refListItem scribble] length] > 0 && pngRange.length > 0) {
            // This is an old png scribble, handle it!
            self.isPNGScribble = TRUE;
        } else {
            self.isPNGScribble = FALSE;
        }
        
		//55, 77, 96
	}
	return self;
}

- (void)viewDidLoad {
    // Set the background color
    [self.view setBackgroundColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0]];
    //[self.view setBackgroundColor:[UIColor colorWithRed:0.08235294 green:0.10980392 blue:0.18431372 alpha:1.0]];
    
    // Set content size for view in popover
    [self setContentSizeForViewInPopover:scribblePadContentSize];
    
    // Set title
    self.title = @"Scribble Pad";
    
    // Load the buttons
    [self loadButtons];
    
    // Load the scroll view
    [self loadScrollView];
    
    [self loadLineWidthsSegmentedControl];
    
    [self loadScrollControlWindow];
    
    [self loadDrawingLayer];
    
    // Init the list options action sheet
    clearActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" 
                                           destructiveButtonTitle:@"Clear Drawing" 
                                          otherButtonTitles:nil];
    
    
    // Deselect all
    [self deselectAll];
    
    [drawPadScrollView setScrollEnabled:FALSE];

    /*
     
     - (void)greenPenButtonAction {
     [drawPadScrollView setScrollEnabled:NO];
     penColor = EnumPenColorGreen;
     
     
     // Deselect all
     [self deselectAll];
     
     // Select this pen
     [greenPenButton setSelected:YES];
     }
     
     - (void)redPenButtonAction {
     [drawPadScrollView setScrollEnabled:NO];
     penColor = EnumPenColorRed;
     
     
     // Deselect all
     [self deselectAll];
     
     // Select this pen
     [redPenButton setSelected:YES];
     }
     
     - (void)bluePenButtonAction {
     [drawPadScrollView setScrollEnabled:NO];
     penColor = EnumPenColorBlue;
     
     
     // Deselect all
     [self deselectAll];
     
     // Select this pen
     [bluePenButton setSelected:YES];
     }
     
     - (void)blackPenButtonAction {
     [drawPadScrollView setScrollEnabled:NO];
     penColor = EnumPenColorBlack;
     
     */
    
    switch (penColor) {
        case EnumPenColorNone:
            break;
        case EnumPenColorRed:
            [redPenButton setSelected:YES];
            [drawingLayer setColour:[UIColor colorWithRed:0.8274509 green:0.0235294 blue:0.0 alpha:1.0]];
            break;
        case EnumPenColorBlue:
            [bluePenButton setSelected:YES];
            [drawingLayer setColour:[UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:1.0]];
            break;
        case EnumPenColorBlack:
            [blackPenButton setSelected:YES];
            [drawingLayer setColour:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]];
            break;
        case EnumPenColorGreen:
            [greenPenButton setSelected:YES];
            [drawingLayer setColour:[UIColor colorWithRed:0.0 green:0.411764 blue:0.21568627 alpha:1.0]];
            break;
            
        default:
            break;
    }
    
}


#pragma mark -
#pragma mark Load Methods

- (void)loadDrawingLayer { 
    if (drawingLayer == nil) {
        drawingLayer = [GLDrawES2ViewController alloc];
        [drawingLayer initialFrame:kDrawPadImageViewRect];
        [drawingLayer setDelegate:self];
        //[drawingLayer drawFrame];
        [drawPadScrollView addSubview:drawingLayer.view];
        
        [drawingLayer setScale:1.0f];
        //[drawingLayer drawFrame];
        [self performSelector:@selector(showDrawingFrame) withObject:nil afterDelay:0.0000001];
    }
}

- (void)loadLineWidthsSegmentedControl {
	lineWidthsSegmentedControl = [[UISegmentedControl alloc] init];
    
    [lineWidthsSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"LineWidthNine.png"] 
                                               atIndex:0 animated:NO];
    [lineWidthsSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"LineWidthSeven.png"] 
                                               atIndex:0 animated:NO];
    [lineWidthsSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"LineWidthFive.png"] 
                                               atIndex:0 animated:NO];
    
    [lineWidthsSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"LineWidthThree.png"] 
                                                                   atIndex:0 animated:NO];
    
    [lineWidthsSegmentedControl setSelectedSegmentIndex:0];
    
    [lineWidthsSegmentedControl setFrame:CGRectMake(200, 8, 200, 37)];

    [lineWidthsSegmentedControl addTarget:self action:@selector(lineWidthsSegmentedControlAction) forControlEvents:UIControlEventValueChanged];
    
	[self.view addSubview:lineWidthsSegmentedControl];

}

- (void)showDrawingFrame {
    [drawingLayer setDrawingData:self.glScribbleDictionary];
    [drawingLayer drawFrame];
}

- (void)loadScrollView {
	drawPadScrollView = [[UIScrollView alloc] init];
	
	[drawPadScrollView setFrame:kDrawPadScrollViewRect];
	drawPadScrollView.delegate = self;
    drawPadScrollView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    drawPadScrollView.layer.borderWidth = 1.0f;
	[drawPadScrollView setBackgroundColor:[UIColor whiteColor]];
	[drawPadScrollView setContentSize:kDrawPadScrollViewContentSize];
    
	UILabel *magnifiedListItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 33, 1350, 63)];
	// If this is sub task, modify label position and size
	//if (refListItem.parentListItemID != -1) {
	//	[magnifiedListItemLabel setFrame:CGRectMake(84, 33, 1281, 63)];
	//}
	
	//[magnifiedListItemLabel setFont:[UIFont fontWithName:@"Helvetica" size:42.0]];
	[magnifiedListItemLabel setFont:[UIFont fontWithName:@"Helvetica" size:42.5]];
	
	[magnifiedListItemLabel setBackgroundColor:[UIColor whiteColor]];
	[magnifiedListItemLabel setText:refListItem.title];
	[magnifiedListItemLabel setTextColor:[UIColor darkTextColor]];
	[drawPadScrollView addSubview:magnifiedListItemLabel];
	[magnifiedListItemLabel release];
	
    
    
	[self.view addSubview:drawPadScrollView];
	
}

- (void)loadButtons {
	// Set the right bar button and the left bar button
	UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton addTarget:self action:@selector(doneButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [doneButton setImage:[UIImage imageNamed:@"DoneDrawPadButton.png"] forState:UIControlStateNormal];
    [doneButton setFrame:CGRectMake(536, 8, 54, 37)];
    [self.view addSubview:doneButton];                                            

    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(cancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[UIImage imageNamed:@"CancelDrawPadButton.png"] forState:UIControlStateNormal];
    [cancelButton setFrame:CGRectMake(10, 8, 68, 37)];
    [self.view addSubview:cancelButton];   
	
	undoButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[undoButton setFrame:CGRectMake(486, 8, 40, 37)];
	[undoButton setImage:[UIImage imageNamed:@"UndoButton.png"] forState:UIControlStateNormal];
	[undoButton addTarget:self action:@selector(undoButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [undoButton setShowsTouchWhenHighlighted:YES];
	[self.view addSubview:undoButton];
    
    redoButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[redoButton setFrame:CGRectMake(436, 8, 40, 37)];
	[redoButton setImage:[UIImage imageNamed:@"RedoButton.png"] forState:UIControlStateNormal];
	[redoButton addTarget:self action:@selector(redoButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [redoButton setShowsTouchWhenHighlighted:YES];
	[self.view addSubview:redoButton];
	// 614 width (306)
	
	// Init the pen buttons
	greenPenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[greenPenButton setFrame:CGRectMake(206, 192, 40, 37)];
	[greenPenButton setImage:[UIImage imageNamed:@"Green.png"] forState:UIControlStateNormal];
	[greenPenButton setImage:[UIImage imageNamed:@"GreenSelected.png"] forState:UIControlStateSelected];
	[greenPenButton addTarget:self action:@selector(greenPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:greenPenButton];
	
	redPenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[redPenButton setFrame:CGRectMake(256, 192, 40, 37)];
	[redPenButton setImage:[UIImage imageNamed:@"Red.png"] forState:UIControlStateNormal];
	[redPenButton setImage:[UIImage imageNamed:@"RedSelected.png"] forState:UIControlStateSelected];
	[redPenButton addTarget:self action:@selector(redPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:redPenButton];
	
    bluePenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[bluePenButton setFrame:CGRectMake(306, 192, 40, 37)];
	[bluePenButton setImage:[UIImage imageNamed:@"Blue.png"] forState:UIControlStateNormal];
	[bluePenButton setImage:[UIImage imageNamed:@"BlueSelected.png"] forState:UIControlStateSelected];
	[bluePenButton addTarget:self action:@selector(bluePenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:bluePenButton];
    
	blackPenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[blackPenButton setFrame:CGRectMake(356, 192, 40, 37)];
	[blackPenButton setImage:[UIImage imageNamed:@"Black.png"] forState:UIControlStateNormal];
	[blackPenButton setImage:[UIImage imageNamed:@"BlackSelected.png"] forState:UIControlStateSelected];
	[blackPenButton addTarget:self action:@selector(blackPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:blackPenButton];
	
	eraserButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[eraserButton setFrame:CGRectMake(10, 192, 40, 37)];
	[eraserButton setImage:[UIImage imageNamed:@"Eraser.png"] forState:UIControlStateNormal];
	[eraserButton setImage:[UIImage imageNamed:@"EraserSelected.png"] forState:UIControlStateSelected];
	[eraserButton addTarget:self action:@selector(eraserButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:eraserButton];
    
    
    clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[clearButton setFrame:CGRectMake(60, 192, 40, 37)];
	[clearButton setImage:[UIImage imageNamed:@"ClearDrawPadButton.png"] forState:UIControlStateNormal];
    [clearButton setShowsTouchWhenHighlighted:YES];
	[clearButton addTarget:self action:@selector(clearDrawPadButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:clearButton];
    
    /*
     
     dragButton = [UIButton buttonWithType:UIButtonTypeCustom];
     [dragButton setFrame:CGRectMake(564, 152, 40, 37)];
     [dragButton setImage:[UIImage imageNamed:@"Hand.png"] forState:UIControlStateNormal];
     [dragButton setImage:[UIImage imageNamed:@"HandSelected.png"] forState:UIControlStateSelected];
     [dragButton addTarget:self action:@selector(dragButtonAction) forControlEvents:UIControlEventTouchUpInside];
     [self.view addSubview:dragButton];
     */
    
}

- (void)loadScrollControlWindow {
    //scrollControlWindow = [[UIView alloc] initWithFrame:kScrollControllWindowFrame];
    //scrollControlWindow.layer.borderColor = [UIColor blackColor].CGColor;
    //scrollControlWindow.layer.borderWidth = 1.0f;
    //[scrollControlWindow setBackgroundColor:[UIColor whiteColor]];
    
    scrollControlWindow = [[UIImageView alloc] initWithFrame:kScrollControllWindowFrame];
    [scrollControlWindow setImage:[UIImage imageNamed:@"HorizontalScrollControlWindow.png"]];
    [self.view addSubview:scrollControlWindow];
    
    
    scrollControl = [[UIImageView alloc] initWithFrame:_ScrollControlFrame];
    [scrollControl setImage:[UIImage imageNamed:@"HorizontalScrollControl.png"]];
    
    
    
    //scrollControl = [[UIView alloc] initWithFrame:_ScrollControlFrame];
    //[scrollControl setBackgroundColor:[UIColor whiteColor]];
    //scrollControl.layer.borderColor = [UIColor grayColor].CGColor;
    //scrollControl.layer.borderWidth = 3.0f;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"HorizontalMoveLines.png"]];
    [imageView setFrame:CGRectMake(18, 6, 23, 23)];
    [scrollControl addSubview:imageView];
    [imageView release];
    [self.view addSubview:scrollControl];
 
    scrollControlDragging = FALSE;
}

#pragma mark -
#pragma mark Touches Handling


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	// Enumerate through all the touch objects.
    CGPoint touchPosition = CGPointMake(0, 0);
	for (UITouch *touch in touches) {
        touchPosition = [touch locationInView:self.view];
        
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		//[self dispatchFirstTouchAtPoint:[touch locationInView:self] forEvent:nil];
        
	}	

    if (CGRectContainsPoint([scrollControl frame], touchPosition)) {
        // Within bounds
        scrollControlDragging = TRUE;
        
        scrollOffset = touchPosition.x - scrollControl.frame.origin.x;
    } else {
        scrollControlDragging = FALSE;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	// If ref touched view not nil, shrink
	for (UITouch *touch in touches) {
        
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		//[self dispatchTouchAtPoint:[touch locationInView:self] forEvent:nil];
	}	
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	[self touchesEnded:touches withEvent:event];
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (scrollControlDragging) {
        // Move it along it's y axis
        CGPoint touchPosition = CGPointZero;
        for (UITouch *touch in touches) {
            touchPosition = [touch locationInView:self.view];
        }	
        
        
        // Move the scroll control up to a maximum, minumum.
        if (touchPosition.x != 0) {
            CGFloat newPosition = touchPosition.x - scrollOffset;
            
            if (newPosition >= (scrollControlWindow.frame.origin.x) && 
                (newPosition + scrollControl.frame.size.width) <= (scrollControlWindow.frame.origin.x + scrollControlWindow.frame.size.width)) {
                [scrollControl setFrame:CGRectMake(newPosition, 
                                                   scrollControl.frame.origin.y, 
                                                   scrollControl.frame.size.width, 
                                                   scrollControl.frame.size.height)];
            } else if (newPosition < scrollControlWindow.frame.origin.x) {
                newPosition = scrollControlWindow.frame.origin.x;
                [scrollControl setFrame:CGRectMake(newPosition, 
                                                   scrollControl.frame.origin.y, 
                                                   scrollControl.frame.size.width, 
                                                   scrollControl.frame.size.height)];
            } else if ((newPosition + scrollControl.frame.size.width) > (scrollControlWindow.frame.origin.x + scrollControlWindow.frame.size.width)) {
                newPosition = (scrollControlWindow.frame.origin.x + scrollControlWindow.frame.size.width) - scrollControl.frame.size.width;
                [scrollControl setFrame:CGRectMake(newPosition, 
                                                   scrollControl.frame.origin.y, 
                                                   scrollControl.frame.size.width, 
                                                   scrollControl.frame.size.height)];
            }
        }
        
        // Now need to adjust the scrollview, the multiple is 10
        CGFloat newScrollViewOffset = (scrollControl.frame.origin.x - (scrollControlWindow.frame.origin.x)) * 10.0f;

        [drawPadScrollView scrollRectToVisible:CGRectMake(newScrollViewOffset, 0, 600, 132) animated:NO];
    }
}

#pragma mark -
#pragma mark Button Actions

- (void)lineWidthsSegmentedControlAction {
    switch ([lineWidthsSegmentedControl selectedSegmentIndex]) {
        case 0:
            drawingLayer.minLineWidth = 1.f;
            drawingLayer.maxLineWidth = 2.f;
            drawingLayer.strokeStep = 0.65f;
           // drawingLayer.
            [drawingLayer changeWidth];
            break;
        case 1:
            drawingLayer.minLineWidth = 3.f;
            drawingLayer.maxLineWidth = 4.f;
            drawingLayer.strokeStep = 0.55f;
            [drawingLayer changeWidth];
            break;
        case 2:
            drawingLayer.minLineWidth = 5.f;
            drawingLayer.maxLineWidth = 6.f;
             drawingLayer.strokeStep = 0.45f;
            [drawingLayer changeWidth];
            break;
        case 3:
            drawingLayer.minLineWidth = 7.f;
            drawingLayer.maxLineWidth = 8.f;
            drawingLayer.strokeStep = 0.35f;
            [drawingLayer changeWidth];
            break;
        default:
            break;
    }
}

- (void)deselectAll {
	[greenPenButton setSelected:NO];
	[redPenButton setSelected:NO];
	[bluePenButton setSelected:NO];
	[blackPenButton setSelected:NO];
	[dragButton setSelected:NO];
	[eraserButton setSelected:NO];
    
    if ([drawingLayer eraserSelected] == TRUE) {
        [drawingLayer eraseToggle];
    }
}

/*
 case EnumPenColorBlack:
 CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
 break;
 case EnumPenColorBlue:
 CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 1.0, 1.0);
 break;
 case EnumPenColorGreen:
 CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.411764, 0.21568627, 1.0);
 break;
 case EnumPenColorRed:
 CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.8274509, 0.0235294, 0.0, 1.0);
 break;
 
 */

- (void)greenPenButtonAction {
	[drawPadScrollView setScrollEnabled:NO];
	penColor = EnumPenColorGreen;
	[drawingLayer setColour:[UIColor colorWithRed:0.0 green:0.411764 blue:0.21568627 alpha:1.0]];
    
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[greenPenButton setSelected:YES];
}

- (void)redPenButtonAction {
	[drawPadScrollView setScrollEnabled:NO];
	penColor = EnumPenColorRed;
    [drawingLayer setColour:[UIColor colorWithRed:0.8274509 green:0.0235294 blue:0.0 alpha:1.0]];
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[redPenButton setSelected:YES];
}

- (void)bluePenButtonAction {
	[drawPadScrollView setScrollEnabled:NO];
	penColor = EnumPenColorBlue;
    [drawingLayer setColour:[UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:1.0]];
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[bluePenButton setSelected:YES];
}

- (void)blackPenButtonAction {
	[drawPadScrollView setScrollEnabled:NO];
	penColor = EnumPenColorBlack;
    [drawingLayer setColour:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]];
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[blackPenButton setSelected:YES];
}



- (void)eraserButtonAction {
	[drawPadScrollView setScrollEnabled:NO];
	penColor = EnumPenColorEraser;
	
	// Deselect all
	[self deselectAll];
	
    if ([drawingLayer eraserSelected] == FALSE) {
        [drawingLayer eraseToggle];
    }
    
	// Select this pen
	[eraserButton setSelected:YES];
    
}

- (void)doneButtonAction {
    // Need to save the drawing data
    //[self.glScribbleDictionary release];
    //NSDictionary *newDictionary = [[NSDictionary alloc] initWithDictionary:[drawingLayer drawingData]];

    self.glScribbleDictionary = [NSDictionary dictionaryWithDictionary:[drawingLayer drawingData]];

    NSString *newScribble;
    
    if ([refListItem.scribble length] > 0) {
        newScribble = refListItem.scribble;
    } else {
        newScribble = [MethodHelper getUniqueFileName];
        newScribble = [newScribble stringByAppendingString:@".gls"];
        refListItem.scribble = newScribble;
    }
    
	NSString *fileDestination = [[MethodHelper getScribblePath] stringByAppendingPathComponent:newScribble];
  //  NSLog(@"%@", fileDestination);
  //  NSLog(@"%@", self.glScribbleDictionary);
    NSData *glScribbleData = [MethodHelper encodeObject:self.glScribbleDictionary withKey:@"GLScribbleData"];
    
    
    if ([glScribbleData writeToFile:fileDestination atomically:YES] == NO) {
        NSLog(@"NSData write failed");
    }
    
	// Update the database
	[refListItem updateDatabase];
    
    
    NSRange glsRange = [newScribble rangeOfString:@".gls"];
    if (glsRange.length == 0) {
        [delegate drawPadUpdatedForListItem:refListItem];
    }
    
    NSString *pngScribble = [newScribble substringToIndex:glsRange.location];
    pngScribble = [NSString stringWithFormat:@"%@.png", pngScribble];
    
    // Get the file destination
    NSString *pngFileDestination = [[MethodHelper getScribblePath] stringByAppendingPathComponent:pngScribble];
    
    // Write the image to the database
    UIImage *myImage = [self.drawingLayer getUIImage];
    myImage = [MethodHelper scaleImage:myImage maxWidth:500 maxHeight:44];
    
    [UIImagePNGRepresentation(myImage) writeToFile:pngFileDestination atomically:YES];
    
	[delegate drawPadUpdatedForListItem:refListItem];
	
}

- (void)cancelButtonAction {
	[delegate dismissDrawPadPopOver:self.isNewScribbleItem];
}

- (void)undoButtonAction {
    [drawingLayer undo];
}

- (void)redoButtonAction {
    [drawingLayer redo];
}

- (void)clearDrawPadButtonAction {
    [clearActionSheet showFromRect:[clearButton frame] 
								  inView:self.view animated:NO];
}

#pragma mark -
#pragma mark UIViewController Delegate Methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - UIActionSheetDelegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ([actionSheet isEqual:clearActionSheet]) {
		
		if (buttonIndex == 0) {
			// Clear the page
            [drawingLayer clearView];
		}
	}
}

#pragma mark - GVViewDelegate

-(void)infoUpdatedWithData:(NSMutableDictionary *)data {
    [drawingLayer updateDrawingWithData:data];
}

-(void) infoUpdated {
    // drawingData returns a deep mutable copy, which _dataStorage retains.
    _dataStorage = [drawingLayer drawingData];
    // setDrawing data makes a deep mutable copy of _dataStorage.
    //[previewLayer setDrawingData:_dataStorage];
    // redraw everything with new data.
    //[previewLayer drawFrame];
}

-(void) viewCleared {
    
}

- (void)drawingTouchesEnded:(CGPoint)touchLocation {
    //NSLog(@"%@", NSStringFromCGPoint(touchLocation));
    /*CGPoint contentOffset = [drawPadScrollView contentOffset];
    
    CGFloat rightBorder = drawPadScrollView.frame.size.width + contentOffset.x;
    
    if ((rightBorder - touchLocation.x) < 100) {
        previewLayer = [GLDrawES2ViewController alloc];
        CGSize previewSize = kPreviewLayerSize;
        [previewLayer initialFrame:CGRectMake(contentOffset.x, 0, previewSize.width, previewSize.height)];
        //previewLayer.view.backgroundColor = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:0.3];
        //[previewLayer setCenter:CGPointMake(20, 66)];
        //[previewLayer setDrawingData:[drawingLayer drawingData]];
        //
        [drawPadScrollView addSubview:previewLayer.view];
        [previewLayer setScale:3.f];
        previewLayer.view.userInteractionEnabled = NO;
        [self performSelector:@selector(infoUpdated) withObject:nil afterDelay:0.0000001];
    } else {
        NSLog(@"pixels distance: %f", rightBorder - touchLocation.x);
    }
    */
    
   // [previewLayer set
}

- (NSDictionary *)copyDictionary:(NSDictionary *)theDictionary {
    NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
    for (id key in [theDictionary allKeys]) {
        [newDictionary setObject:(NSArray*)[theDictionary objectForKey:key] forKey:key];
    }
    
    return newDictionary;
}

@end
