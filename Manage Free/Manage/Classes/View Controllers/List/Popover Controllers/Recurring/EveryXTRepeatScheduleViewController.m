    //
//  CustomRepeatScheduleViewController.m
//  Manage
//
//  Created by Cliff Viegas on 10/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "EveryXTRepeatScheduleViewController.h"

// Data Models
#import "RecurringListItem.h"

// Constants
#define kViewControllerSize					CGSizeMake(320, 318)

@interface EveryXTRepeatScheduleViewController()
// Private methods
- (void)loadPickerView;
- (void)loadBarButtonItems;
- (void)updatePickerLabel;
- (NSInteger)getArrayIndexForCurrentFrequency;
- (NSInteger)getArrayIndexForCurrentFrequencyMeasurement;
@end


@implementation EveryXTRepeatScheduleViewController

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[pickerLabel release];
	[numbersArray release];
	[measurementArray release];
	[initialFrequencyMeasurement release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithRecurringListItem:(RecurringListItem *)theRecurringListItem {
	if (self = [super init]) {
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Set the title
		self.title = @"Custom Schedule";
		
		// Set the background color (silver group table view bg color)
		[self.view setBackgroundColor:[UIColor colorWithRed:222 / 255.0 green:223 / 255.0 blue:228 / 255.0 alpha:1.0]];
		
		// Load the numbers and months arrays
		numbersArray = [[NSArray alloc] initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10",
						@"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23",
						@"24", @"25", @"26", @"27", @"28", @"29", @"30", @"31", @"32", @"33", @"34", @"35", @"36",
						@"37", @"38", @"39", @"40", @"41", @"42", @"43", @"44", @"45", @"46", @"47", @"48", @"49", @"50",
						@"51", @"52", nil];
		measurementArray = [[NSArray alloc] initWithObjects:@"Days", @"Weeks", @"Months", @"Years", nil];
		
		
		// Assign the recurring list item
		refRecurringListItem = theRecurringListItem;
		
		// Load the bar button items
		[self loadBarButtonItems];
		
		pickerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, 300, 31)];
		[pickerLabel setBackgroundColor:[UIColor clearColor]];
		[pickerLabel setTextColor:[UIColor darkTextColor]];
		[pickerLabel setTextAlignment:UITextAlignmentCenter];
		[pickerLabel setFont:[UIFont fontWithName:@"Helvetica" size:19.0]];
		[self.view addSubview: pickerLabel];
		
		// Set initial values
		initialFrequency = refRecurringListItem.frequency;
		initialFrequencyMeasurement = [refRecurringListItem.frequencyMeasurement copy];
		
		[self updatePickerLabel];
		
		// Load the picker views
		[self loadPickerView];
		
		}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadBarButtonItems {
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
										  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
										  target:self
										  action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]
											initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
											target:self 
											action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
	[cancelBarButtonItem release];
}

- (void)loadPickerView {
	UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 102, 320, 216)];
	
	[pickerView setDelegate:self];
	[pickerView setDataSource:self];
	[self.view addSubview:pickerView];
	
	[pickerView selectRow:[self getArrayIndexForCurrentFrequency] inComponent:0 animated:NO];
	[pickerView selectRow:[self getArrayIndexForCurrentFrequencyMeasurement] inComponent:1 animated:NO];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	// Call a delegate as well
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
	// Reset to initial values
	refRecurringListItem.frequency = initialFrequency;
	refRecurringListItem.frequencyMeasurement = initialFrequencyMeasurement;
	
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UIPickerView Delegates

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	if (component == 0) {
		return [numbersArray objectAtIndex:row];
	}
	
	return [measurementArray objectAtIndex:row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	if (component == 0) {
		return [numbersArray count];
	} 
	
	return [measurementArray count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	if (component == 0) {
		NSInteger newFrequency = [[numbersArray objectAtIndex:row] integerValue];
		refRecurringListItem.frequency = newFrequency;
		[self updatePickerLabel];
	} else if (component == 1) {
		NSString *newFrequencyMeasurement = [measurementArray objectAtIndex:row];
		refRecurringListItem.frequencyMeasurement = newFrequencyMeasurement;
		[self updatePickerLabel];
	}
}

#pragma mark -
#pragma mark Class Methods

- (void)updatePickerLabel {
	if (refRecurringListItem.frequency == 0 || [refRecurringListItem.frequencyMeasurement isEqualToString:@""]) {
		pickerLabel.text = @"Every Day";
		refRecurringListItem.frequency = 1;
		refRecurringListItem.frequencyMeasurement = @"Days";
	} else if (refRecurringListItem.frequency == 1 && [refRecurringListItem.frequencyMeasurement isEqualToString:@""] == FALSE) {
		if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Days"]) {
			pickerLabel.text = @"Every Day";
		} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Weeks"]) {
			pickerLabel.text = @"Every Week";
		} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Months"]) {
			pickerLabel.text = @"Every Month";
		} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Years"]) {
			pickerLabel.text = @"Every Year";
		}
	} else {
		pickerLabel.text = [NSString stringWithFormat:@"Every %d %@", refRecurringListItem.frequency, refRecurringListItem.frequencyMeasurement];
	}
}

- (NSInteger)getArrayIndexForCurrentFrequency {
	NSInteger i = 0;
	
	for (NSString *stringFrequency in numbersArray) {
		if ([stringFrequency integerValue] == refRecurringListItem.frequency) {
			return i;
		}
		i++;
	}
	
	return 0;
}

- (NSInteger)getArrayIndexForCurrentFrequencyMeasurement {
	if ([refRecurringListItem.frequencyMeasurement isEqualToString:@""] || [refRecurringListItem.frequencyMeasurement isEqualToString:@"Days"]) {
		return 0;
	} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Weeks"]) {
		return 1;
	} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Months"]) {
		return 2;
	} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Years"]) {
		return 3;
	}

	return 0;
}

#pragma mark -
#pragma mark UIViewController Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
