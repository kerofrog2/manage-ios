//
//  CustomRepeatScheduleViewController.h
//  Manage
//
//  Created by Cliff Viegas on 10/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class RecurringListItem;

@interface EveryXTRepeatScheduleViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource> {
	RecurringListItem	*refRecurringListItem; // Reference to recurring list item
	
	NSArray				*numbersArray;
	NSArray				*measurementArray;
	UILabel				*pickerLabel;
	NSInteger			initialFrequency;
	NSString			*initialFrequencyMeasurement;
}

#pragma mark Initialisation
- (id)initWithRecurringListItem:(RecurringListItem *)theRecurringListItem;

@end
