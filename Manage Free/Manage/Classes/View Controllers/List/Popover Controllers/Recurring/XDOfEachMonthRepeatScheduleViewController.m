    //
//  CustomOfEachMonthRepeatScheduleViewController.m
//  Manage
//
//  Created by Cliff Viegas on 22/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "XDOfEachMonthRepeatScheduleViewController.h"

// Data Models
#import "RecurringListItem.h"

// Constants
#define kViewControllerSize					CGSizeMake(320, 318)

@interface XDOfEachMonthRepeatScheduleViewController()

// Private methods
- (void)loadPickerView;
- (void)loadBarButtonItems;
- (void)updatePickerLabel;
- (NSInteger)getArrayIndexForCurrentDayOfTheWeek;
- (NSInteger)getArrayIndexForCurrentFrequencyMeasurement;
@end


@implementation XDOfEachMonthRepeatScheduleViewController

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[pickerLabel release];
	[daysArray release];
	[measurementArray release];
	[initialDayOfTheWeek release];
	[initialFrequencyMeasurement release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithRecurringListItem:(RecurringListItem *)theRecurringListItem {
	if (self = [super init]) {
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Set the title
		self.title = @"Custom Schedule";
		
		// Set the background color (silver group table view bg color)
		[self.view setBackgroundColor:[UIColor colorWithRed:222 / 255.0 green:223 / 255.0 blue:228 / 255.0 alpha:1.0]];
		
		// Load the numbers and months arrays
		daysArray = [[NSArray alloc] initWithObjects:@"Monday", @"Tuesday", @"Wednesday",
												@"Thursday", @"Friday", @"Saturday", @"Sunday", nil];
		measurementArray = [[NSArray alloc] initWithObjects:@"First", @"Second", @"Third",
							@"Fourth", @"Last", nil];
		
		
		// Assign the recurring list item
		refRecurringListItem = theRecurringListItem;
		
		// Load the bar button items
		[self loadBarButtonItems];
		
		pickerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, 300, 31)];
		[pickerLabel setBackgroundColor:[UIColor clearColor]];
		[pickerLabel setTextColor:[UIColor darkTextColor]];
		[pickerLabel setTextAlignment:UITextAlignmentCenter];
		[pickerLabel setFont:[UIFont fontWithName:@"Helvetica" size:19.0]];
		[self.view addSubview: pickerLabel];
		
		// Set initial values
		initialFrequency = refRecurringListItem.frequency;
		initialFrequencyMeasurement = [refRecurringListItem.frequencyMeasurement copy];
		initialDayOfTheWeek = [refRecurringListItem.daysOfTheWeek copy];
		
		[self updatePickerLabel];
		
		// Load the picker views
		[self loadPickerView];
		
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadBarButtonItems {
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
										  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
										  target:self
										  action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]
											initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
											target:self 
											action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
	[cancelBarButtonItem release];
}

- (void)loadPickerView {
	UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 102, 320, 216)];
	
	[pickerView setDelegate:self];
	[pickerView setDataSource:self];
	[self.view addSubview:pickerView];
	
	[pickerView selectRow:[self getArrayIndexForCurrentFrequencyMeasurement] inComponent:0 animated:NO];
	[pickerView selectRow:[self getArrayIndexForCurrentDayOfTheWeek] inComponent:1 animated:NO];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	// Call a delegate as well
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
	// Reset to initial values
	refRecurringListItem.frequency = initialFrequency;
	refRecurringListItem.daysOfTheWeek = initialDayOfTheWeek;
	refRecurringListItem.frequencyMeasurement = initialFrequencyMeasurement;
	
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UIPickerView Delegates

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	if (component == 0) {
		return [measurementArray objectAtIndex:row];
	}
	
	return [daysArray objectAtIndex:row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	if (component == 0) {
		return [measurementArray count];
	} 
	
	return [daysArray count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	if (component == 0) {
		NSString *newFrequency = [measurementArray objectAtIndex:row];
		refRecurringListItem.frequencyMeasurement = newFrequency;
		[self updatePickerLabel];
	} else if (component == 1) {
		NSString *newDayOfTheWeek = [daysArray objectAtIndex:row];
		refRecurringListItem.daysOfTheWeek = newDayOfTheWeek;
		[self updatePickerLabel];
	}
}

#pragma mark -
#pragma mark Class Methods

- (void)updatePickerLabel {
	// If this isn't set to a current frequency measuremtn for x of d each month
	if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"First"] == FALSE
		&& [refRecurringListItem.frequencyMeasurement isEqualToString:@"Second"] == FALSE
		&& [refRecurringListItem.frequencyMeasurement isEqualToString:@"Third"] == FALSE
		&& [refRecurringListItem.frequencyMeasurement isEqualToString:@"Fourth"] == FALSE
		&& [refRecurringListItem.frequencyMeasurement isEqualToString:@"Last"] == FALSE) {
		pickerLabel.text = @"First Monday of each month";
		refRecurringListItem.frequency = 0;
		refRecurringListItem.frequencyMeasurement = @"First";
		refRecurringListItem.daysOfTheWeek = @"Monday";
	} else {
		pickerLabel.text = [NSString stringWithFormat:@"%@ %@ of each month",
							refRecurringListItem.frequencyMeasurement,
							refRecurringListItem.daysOfTheWeek];
	}
}

- (NSInteger)getArrayIndexForCurrentDayOfTheWeek {
	if ([refRecurringListItem.daysOfTheWeek isEqualToString:@"Monday"]) {
		return 0;
	} else if ([refRecurringListItem.daysOfTheWeek isEqualToString:@"Tuesday"]) {
		return 1;
	} else if ([refRecurringListItem.daysOfTheWeek isEqualToString:@"Wednesday"]) {
		return 2;
	} else if ([refRecurringListItem.daysOfTheWeek isEqualToString:@"Thursday"]) {
		return 3;
	} else if ([refRecurringListItem.daysOfTheWeek isEqualToString:@"Friday"]) {
		return 4;
	} else if ([refRecurringListItem.daysOfTheWeek isEqualToString:@"Saturday"]) {
		return 5;
	} else if ([refRecurringListItem.daysOfTheWeek isEqualToString:@"Sunday"]) {
		return 6;
	} 
	
	return 0;
}

- (NSInteger)getArrayIndexForCurrentFrequencyMeasurement {
	if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"First"] || [refRecurringListItem.frequencyMeasurement isEqualToString:@"Days"]) {
		return 0;
	} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Second"]) {
		return 1;
	} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Third"]) {
		return 2;
	} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Fourth"]) {
		return 3;
	} else if ([refRecurringListItem.frequencyMeasurement isEqualToString:@"Last"]) {
		return 4;
	}
	
	return 0;
}

#pragma mark -
#pragma mark UIViewController Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

