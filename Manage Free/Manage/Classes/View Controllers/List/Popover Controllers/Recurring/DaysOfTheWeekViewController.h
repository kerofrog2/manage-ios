//
//  DaysOfTheWeekViewController.h
//  Manage
//
//  Created by Cliff Viegas on 10/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class RecurringListItem;


@interface DaysOfTheWeekViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	NSString				*initialDaysOfTheWeek;
	RecurringListItem		*refRecurringListItem;
	UITableView				*myTableView;
	NSMutableArray			*weekDaysArray;
}

#pragma mark Initialisation
- (id)initWithRecurringListItem:(RecurringListItem *)theRecurringListItem;

@end
