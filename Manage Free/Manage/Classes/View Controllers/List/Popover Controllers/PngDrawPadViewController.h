//
//  DrawPadViewController.h
//  Manage
//
//  Created by Cliff Viegas on 14/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class ListItem;

// Data Collections
@class DrawPointCollection;

@protocol PngDrawPadDelegate
- (void)dismissDrawPadPopOver:(BOOL)newScribbleItem;
- (void)drawPadUpdatedForListItem:(ListItem *)listItem;
@end

typedef enum {
	EnumPngPenColorBlack = 0,
	EnumPngPenColorBlue = 1,
	EnumPngPenColorRed = 2,
	EnumPngPenColorGreen = 3,
	EnumPngPenColorEraser = 4,
	EnumPngPenColorNone = 5
} EnumPngPenColor;

@interface PngDrawPadViewController : UIViewController <UIScrollViewDelegate> {
	id <PngDrawPadDelegate>	delegate;
	ListItem				*refListItem;
	
	// Scroll view objects
	UIScrollView			*drawPadScrollView;
	UIImageView				*drawPadImageView;
	
	// Buttons
	UIButton				*bluePenButton;
	UIButton				*greenPenButton;
	UIButton				*redPenButton;
	UIButton				*blackPenButton;
	UIButton				*dragButton;
	UIButton				*eraserButton;
	
	// Drawing objects
	CGPoint					lastPoint;
	BOOL					mouseSwiped;
	BOOL					lockDrawing;				// If the user is currently in the settings view, used to prevent touches
	BOOL					imageHasChanged;			// If the image has changed since the view was entered
	BOOL					isEditMode;				// editMode == NO when the scribble is to be added, YES if it is being edited
	NSInteger				mouseMoved;
	EnumPngPenColor			penColor;
	
	// Drawing curve objects
	DrawPointCollection		*drawPointCollection;
	NSTimer					*timer;
	BOOL					isDraggingFinger;
    BOOL                    isNewScribbleItem;
}

@property (nonatomic, assign)	id			delegate;
@property (nonatomic, retain)	NSTimer		*timer;
@property (nonatomic, assign)   BOOL        isNewScribbleItem;

#pragma mark Initialisation
- (id)initWithDelegate:(id<PngDrawPadDelegate>)theDelegate andSize:(CGSize)theSize 
		   andListItem:(ListItem *)theListItem andPenColor:(EnumPngPenColor)thePenColor andNewScribbleItem:(BOOL)newScribbleItem;

@end
