    //
//  NewAlarmViewController.m
//  Manage
//
//  Created by Cliff Viegas on 18/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "NewAlarmViewController.h"

// Data Models
#import "Alarm.h"
#import "ListItem.h"

// Method Helper
#import "MethodHelper.h"

// EKEvent Classes
//#import <EventKit/EventKit.h>

// Data Collections
#import "AlarmCollection.h"

// Constants
#define kViewControllerSize					CGSizeMake(320, 216)
#define	kAlarmPickerSize					CGSizeMake(320, 216)

@interface NewAlarmViewController()
- (void)loadBarButtonItems;
- (void)loadAlarmPicker;

// Private methods
- (NSDate *)clearSecondsFromDate:(NSDate *)selectedDate;
- (void)addNewNotificationToQueue:(Alarm *)newAlarm;
- (BOOL)isGreaterThanIOSVersion4;

@end

@implementation NewAlarmViewController

@synthesize refPopoverController;
@synthesize	existingAlarmGUID;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[alarmPicker release];
	[existingAlarmGUID release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithCurrentListItem:(ListItem *)theCurrentListItem andAlarmCollection:(AlarmCollection *)theAlarmCollection {
	if ((self = [super init])) {
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Set the title
		self.title = @"New Alarm";
		
		// Assign reference to current list item
		refCurrentListItem = theCurrentListItem;
		
		// Assign the alarm collection
		refAlarmCollection = theAlarmCollection;
		
		// Load the alarm picker
		[self loadAlarmPicker];
		
		// Load bar button items
		[self loadBarButtonItems];
		
		// Set existing alarm guid to blank
		self.existingAlarmGUID = @"";
	}
	return self;
}

- (id)initWithExistingAlarmGUID:(NSString *)alarmGUID andCurrentListItem:(ListItem *)theCurrentListItem
			 andAlarmCollection:(AlarmCollection *)theAlarmCollection {
	if ((self = [super init])) {
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Set the title
		self.title = @"Edit Alarm";
		
		// Assign the passed list item
		refCurrentListItem = theCurrentListItem;
		
		// Assign the alarm collection
		refAlarmCollection = theAlarmCollection;
		
		// Assign passed alarm GUID (copy property)
		self.existingAlarmGUID = alarmGUID;
		
		[self loadAlarmPicker]; // Need to load with datetime
		
		// Update alarm picker date/time
		NSInteger alarmIndex = [refAlarmCollection getIndexOfAlarmWithAlarmGUID:self.existingAlarmGUID];
		
		if (alarmIndex != -1 && alarmIndex < [refAlarmCollection.alarms count]) {
			Alarm *alarm = [refAlarmCollection.alarms objectAtIndex:alarmIndex];
			alarmPicker.date = [MethodHelper dateFromString:alarm.fireDateTime usingFormat:K_DATETIME_FORMAT];
		}
		
		
		[self loadBarButtonItems]; // Need different buttons, a (clear) button and (done)
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadBarButtonItems {
	// Cancel button if it is new alarm, otherwise display a clear button
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
										  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
										  target:self
										  action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	if ([self.existingAlarmGUID length] == 0) {
		UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]
												initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
												target:self 
												action:@selector(cancelBarButtonItemAction)];
		[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
		[cancelBarButtonItem release];
	} else {
		UIBarButtonItem *clearBarButtonItem = [[UIBarButtonItem alloc] 
											   initWithTitle:@"Clear" 
											   style:UIBarButtonItemStyleBordered 
											   target:self action:@selector(clearBarButtonItemAction)];
		[self.navigationItem setLeftBarButtonItem:clearBarButtonItem animated:NO];
		[clearBarButtonItem release];
	}

	
}

- (void)loadAlarmPicker {
	alarmPicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
	[alarmPicker setDatePickerMode:UIDatePickerModeDateAndTime];
	
	[self.view addSubview:alarmPicker];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	// Do nothing if this is not at least version 4 of iOS
	if ([self isGreaterThanIOSVersion4] == FALSE) {
		[MethodHelper showAlertViewWithTitle:@"Warning" andMessage:@"Please update your iOS version.  Alarms require iOS version 4.0 and above." andButtonTitle:@"Ok"];
		return;
	}
	
	// Only move on to next stage if the alarm picker date is after the current time
	if ([alarmPicker.date compare:[NSDate date]] == NSOrderedAscending) {
		// This means the reciever (alarmPicker.date) is earlier than the compared date ([NSDate date])		
		[MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"The chosen alarm time is earlier than the current time" andButtonTitle:@"Ok"];
		return;
	}
	
	// Only create new alarm if refExistingAlarm is nil
	if ([self.existingAlarmGUID length] == 0) {
		Alarm *newAlarm = [[Alarm alloc] initWithListItemID:refCurrentListItem.listItemID];
		
		// Note: Can't use delay/measurement until we add time feature
		NSDate *selectedDate = [self clearSecondsFromDate:alarmPicker.date];
		
		newAlarm.fireDateTime = [MethodHelper stringFromDate:selectedDate usingFormat:K_DATETIME_FORMAT];
		newAlarm.delay = -1;
		newAlarm.measurement = @"";
		
		[refAlarmCollection.alarms addObject:newAlarm];

		// Insert the new alarm in the database
		[newAlarm insertSelfIntoDatabase];
		
		[self addNewNotificationToQueue:newAlarm];
		[newAlarm release];
	} else {
		// Get the existing alarm using alarmGUID
		NSInteger alarmIndex = [refAlarmCollection getIndexOfAlarmWithAlarmGUID:self.existingAlarmGUID];
		
		Alarm *foundAlarm = nil;
		
		if (alarmIndex >= 0) {
			// Found
			foundAlarm = [refAlarmCollection.alarms objectAtIndex:alarmIndex];
			[foundAlarm removeFromNotificationQueue];
		} else {
			// Not found (not good)
			[MethodHelper showAlertViewWithTitle:@"Warning" andMessage:@"Unable to find existing alarm to update.  Please report this issue to support@kerofrog.com.au" andButtonTitle:@"Ok"];
			[self.navigationController popViewControllerAnimated:YES];
		}

		if (foundAlarm != nil) {
			NSDate *selectedDate = [self clearSecondsFromDate:alarmPicker.date];
			
			foundAlarm.fireDateTime = [MethodHelper stringFromDate:selectedDate usingFormat:K_DATETIME_FORMAT];
			foundAlarm.delay = -1;
			foundAlarm.measurement = @"";
			
			// Update database
			[foundAlarm updateDatabase];
			
			// Create new notification
			[self addNewNotificationToQueue:foundAlarm];
		}
	}

	// Update has alarms if needed
	if ([refAlarmCollection.alarms count] > 0) {
		if ([refCurrentListItem hasAlarms] == FALSE) {
			refCurrentListItem.hasAlarms = TRUE;
			[refCurrentListItem updateDatabase];
		}
	}
	
	// Might also require that entries in database that are too old get deleted
	[self.navigationController popViewControllerAnimated:YES];
}

// Do nothing
- (void)cancelBarButtonItemAction {
	[self.navigationController popViewControllerAnimated:YES];
}

// Clear the current alarm
- (void)clearBarButtonItemAction {
	// There should be a string guid that was passed
	NSInteger alarmIndex = [refAlarmCollection getIndexOfAlarmWithAlarmGUID:self.existingAlarmGUID];
	
	if (alarmIndex == -1) {
		[MethodHelper showAlertViewWithTitle:@"Warning" andMessage:@"Unable to find existing alarm to clear. Please report this issue to support@kerofrog.com.au" andButtonTitle:@"Ok"];
	} else {
		// Found the alarm
		Alarm *foundAlarm = [refAlarmCollection.alarms objectAtIndex:alarmIndex];
		
		// Attempt to remove from notification queue first
		[foundAlarm removeFromNotificationQueue];
		
		// Delete the alarm from the database (which also removes object)
		[refAlarmCollection deleteAlarmAtIndex:alarmIndex];
	}

	// Update current list item if it doesn't have alarms
	if ([refAlarmCollection.alarms count] == 0) {
		if ([refCurrentListItem hasAlarms] == YES) {
			refCurrentListItem.hasAlarms = NO;
			[refCurrentListItem updateDatabase];
		}
	}
	
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Helper Methods

- (NSDate *)clearSecondsFromDate:(NSDate *)selectedDate {
	NSDate *returnDate = selectedDate;
	
	NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:returnDate];
	
	// Get the number of seconds
	NSInteger seconds = [dateComponents second];
	
	// Round down the seconds field
	returnDate = [returnDate dateByAddingTimeInterval:-seconds];
	
	return returnDate;

}

// Will create a new notification to add to queue.  Need to pass alarm
- (void)addNewNotificationToQueue:(Alarm *)newAlarm {
	/*UILocalNotification *alarmNotification = [[UILocalNotification alloc] init];
	
	if (alarmNotification == nil) {
		[MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Unable to create local notification (alarm).  Please report this issue to support@kerofrog.com.au" 
							  andButtonTitle:@"Ok"];
		return;
	}
	
	alarmNotification.timeZone = [NSTimeZone defaultTimeZone];
	alarmNotification.fireDate = [MethodHelper dateFromString:newAlarm.fireDateTime usingFormat:K_DATETIME_FORMAT];
	
	alarmNotification.alertBody = refCurrentListItem.title;
	alarmNotification.alertAction = @"View Task";
	alarmNotification.soundName = UILocalNotificationDefaultSoundName;
	
	// Need to create key using AlarmGUID for reference
	NSDictionary *infoDict = [NSDictionary dictionaryWithObject:newAlarm.alarmGUID forKey:@"AlarmGUID"];
	alarmNotification.userInfo = infoDict;
	
	// Schedule the local notification
	[[UIApplication sharedApplication] scheduleLocalNotification:alarmNotification];
	[alarmNotification release];*/
	
	// Need to check if this is ios 4 or above, otherwise display warning
	
}

// Checks to see if first number in version number is greater or equal to 4
- (BOOL)isGreaterThanIOSVersion4 {
	NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
	
	// Get the first number
	NSInteger firstNumber = [[currentVersion substringToIndex:1] integerValue];
	
	if (firstNumber >= 4) {
		return TRUE;
	}
	
	return FALSE;
}

#pragma mark -
#pragma mark UIViewController Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidAppear:(BOOL)animated {
	if (refPopoverController) {
		[refPopoverController setPopoverContentSize:kViewControllerSize animated:YES];
	}
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end
