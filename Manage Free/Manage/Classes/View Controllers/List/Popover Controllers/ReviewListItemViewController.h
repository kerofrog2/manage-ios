//
//  ReviewListItemViewController.h
//  Manage
//
//  Created by Cliff Viegas on 13/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// This is going to inherit from edit list item view controller but will remove
// any editing abilities

#import <UIKit/UIKit.h>

// Inherited Class
#import "EditListItemViewController.h"

@interface ReviewListItemViewController : EditListItemViewController {

}

@end
