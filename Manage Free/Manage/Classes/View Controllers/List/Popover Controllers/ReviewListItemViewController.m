    //
//  ReviewListItemViewController.m
//  Manage
//
//  Created by Cliff Viegas on 13/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ReviewListItemViewController.h"

// Custom Table View Cells
#import "ModifyListItemTableViewCell.h"

// Data Collections
#import "ListItemTagCollection.h"

// Data Models
#import "ListItem.h"

// Custom Objects
#import "CustomEdgeInsetsTextView.h"

// Method Helper
#import "MethodHelper.h"

@implementation ReviewListItemViewController

#pragma mark -
#pragma mark UIViewController Delegates

// Moving to view did load so we can specify settings before the view is loaded
- (void)viewDidLoad {
	// Load the table view
	[self loadTableView];
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewDidAppear:(BOOL)animated {
	
}

- (void)viewWillAppear:(BOOL)animated {
	self.title = @"Review (Read Only)";
	
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	// Do nothing
}

#pragma mark -
#pragma mark Helper Methods

- (CustomEdgeInsetsTextView *)getTextView {
	CustomEdgeInsetsTextView *textView = [[[CustomEdgeInsetsTextView alloc] initWithFrame:CGRectZero] autorelease];
	textView.tag = kTextViewTag;
	[textView setBackgroundColor:[UIColor clearColor]];
	
	[textView setDelegate:self];
	[textView setText:@""];
	[textView setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0]];
	[textView setTextColor:[UIColor darkTextColor]];
	[textView setDirectionalLockEnabled:YES];
	[textView setShowsVerticalScrollIndicator:NO];
	[textView setAlwaysBounceVertical:NO];
	[textView setUserInteractionEnabled:NO];
	
	//[textView set
	
	return textView;
}

- (UILabel *)getLabel {
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	[label setBackgroundColor:[UIColor clearColor]];
	[label setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
	[label setTextColor:[UIColor darkTextColor]];
	[label setLineBreakMode:UILineBreakModeWordWrap];
	[label setTag:kLabelTag];
	
	return label;
}

#pragma mark -
#pragma mark Table View Delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Do nothing
	return;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Only give option of creating sub task to standard tasks
	if (section == 0) {
		return 2;
	} 
    
	return 5;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    ModifyListItemTableViewCell *cell = (ModifyListItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ModifyListItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		//[cell.contentView addSubview:[self getSwitch]];
		[cell.contentView addSubview:[self getTextView]];
	}
	
	CustomEdgeInsetsTextView *myTextView = (CustomEdgeInsetsTextView *)[cell.contentView viewWithTag:(kTextViewTag)];
	
	// Set default cell values
	[cell clearTagsView];
	
	[cell.detailButton setUserInteractionEnabled:NO];
	cell.textLabel.text = @"";
	cell.customTextLabel.text = @"";
	cell.notesTextLabel.text = @"";
	cell.imageView.image = nil;
	[cell.detailButton setImage:nil forState:UIControlStateNormal];
	[cell.detailButton setImage:nil forState:UIControlStateSelected];
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	/*
     
     
     */
    
	/*if (indexPath.section == 0) {
		[cell.detailButton setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
		[cell.detailButton setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
		[cell.detailButton addTarget:self action:@selector(detailButtonAction:) forControlEvents:UIControlEventTouchUpInside];
		
		//cell.detailButton.image = [UIImage imageNamed:@"BoxNotTicked.png"];
		[myTextView setFrame:CGRectMake(28, 4, 268, 73)];
		
		if (boxIsTicked == TRUE) {
			[cell.detailButton setSelected:TRUE];
		} else {
			[cell.detailButton setSelected:FALSE];
		}
		
		if ([listItemTitle length] == 0) {
			[myTextView setTextColor:[UIColor grayColor]];
			[myTextView setText:@"Title"];
		} else if ([listItemTitle length] > 0) {
			[myTextView setTextColor:[UIColor darkTextColor]];
			[myTextView setText:listItemTitle];
		}
	}*/
    
    if (indexPath.section == 0 && indexPath.row == 0) {
		[myTextView setHidden:NO];
		[cell.detailButton setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
		[cell.detailButton setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
		[cell.detailButton addTarget:self action:@selector(detailButtonAction:) forControlEvents:UIControlEventTouchUpInside];
		
		//cell.detailButton.image = [UIImage imageNamed:@"BoxNotTicked.png"];
		// This frame needs to adjust itself based on size of text... hmm.. Ok, can do this... means in textdidchange we just need to come back here
        
        
        // 16 is for the padding added in by Apple
        CGSize theSize = [listItemTitle sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0] 
                                   constrainedToSize:CGSizeMake(kTitleTextViewWidth - 16, 20000) 
                                       lineBreakMode:UILineBreakModeWordWrap];
        
        if (theSize.height == 0) {
            theSize.height = 18;
        }
        
        [myTextView setFrame:CGRectMake(28, 4, 268, theSize.height + 11)];
        
        // [myTextView setFrame:CGRectMake(28, 4, 268, 73)];
		
		if (boxIsTicked == TRUE) {
			[cell.detailButton setSelected:TRUE];
		} else {
			[cell.detailButton setSelected:FALSE];
		}
		
		if ([listItemTitle length] == 0) {
			[myTextView setTextColor:[UIColor grayColor]];
			[myTextView setText:@"Title"];
		} else if ([listItemTitle length] > 0) {
			[myTextView setTextColor:[UIColor darkTextColor]];
			[myTextView setText:listItemTitle];
		}
        
	} else if (indexPath.section == 0 && indexPath.row == 1) {
        [cell.detailButton setBackgroundImage:[UIImage imageNamed:@"EnteredIcon.png"] forState:UIControlStateNormal];
        
        // Need to display an 'imageView' in here... use itemsDueTableViewResponder as example
        [cell loadScribbleImageForListItem:refListItem];
    } else if (indexPath.section == 2) {
		[cell.detailButton setBackgroundImage:[UIImage imageNamed:@"EnteredIcon.png"] forState:UIControlStateNormal];
		cell.customTextLabel.textColor = [UIColor darkTextColor];
		cell.customTextLabel.text = @"Add a Subtask";
	} else {
		switch (indexPath.row) {
			case 0: // Tag
				[cell.detailButton setImage:[UIImage imageNamed:@"Tag.png"] forState:UIControlStateNormal];
				
				// Need to track tags now...
				if ([refListItem.listItemTagCollection.listItemTags count] == 0) {
					cell.customTextLabel.text = @"Tags";
				} else {
					cell.customTextLabel.text = @"";
				}
				
				[cell.customTextLabel setTextColor:[UIColor grayColor]];
				
				[cell loadTagsFrom:refListItem.listItemTagCollection andTagCollection:refTagCollection];
				
				break;
			case 1: // Notes
				[cell.detailButton setImage:[UIImage imageNamed:@"NotesIcon.png"] forState:UIControlStateNormal];
				
				CGSize theSize = [listItemNotes sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0]  
										   constrainedToSize:CGSizeMake(kNotesLabelWidth, kMaximumNotesHeight) lineBreakMode:UILineBreakModeWordWrap];
				NSInteger height = theSize.height;
				if (height < 20) {
					height = 20;
				}
				
				[cell.notesTextLabel setFrame:CGRectMake(cell.notesTextLabel.frame.origin.x, 
														 cell.notesTextLabel.frame.origin.y, 
														 cell.notesTextLabel.frame.size.width,
														 height)];
				
				[cell.notesTextLabel setLineBreakMode:UILineBreakModeWordWrap];
				cell.notesTextLabel.text = listItemNotes;
				
				
				if ([listItemNotes length] == 0) {
					[cell.notesTextLabel setTextColor:[UIColor grayColor]];
					[cell.notesTextLabel setText:@"Notes"];
				} else if ([listItemNotes length] > 0) {
					[cell.notesTextLabel setTextColor:[UIColor darkTextColor]];
					[cell.notesTextLabel setText:listItemNotes];
					
				}
				break;
			case 2: // Due date
				[cell.detailButton setImage:[UIImage imageNamed:@"DateIcon.png"] forState:UIControlStateNormal];
				//cell.imageView.image = [UIImage imageNamed:@"DateIcon.png"];
				[myTextView setFrame:CGRectZero];
				[myTextView setHidden:YES];
				
				if ([refListItem.dueDate length] > 0 && refListItem.dueDate != nil) {
					NSDate *theDate = [MethodHelper dateFromString:refListItem.dueDate usingFormat:K_DATEONLY_FORMAT];
					[cell.customTextLabel setTextColor:[UIColor darkTextColor]];
					cell.customTextLabel.text = [MethodHelper localizedDateFrom:theDate 
																	 usingStyle:NSDateFormatterMediumStyle 
																	   withYear:YES];
				} else {
					[cell.customTextLabel setTextColor:[UIColor grayColor]];
					cell.customTextLabel.text = @"Due Date";
				}
				
				break;
            case 3: // Priority
                [cell.detailButton setImage:[UIImage imageNamed:@"PriorityIcon.png"] forState:UIControlStateNormal];
                [myTextView setFrame:CGRectZero];
                [myTextView setHidden:YES];
                
                if ([refListItem priority] == 0 || [refListItem priority] == 3) {
                    [cell.customTextLabel setTextColor:[UIColor darkGrayColor]];
                    [cell.customTextLabel setText:@"Normal"];
                } else if ([refListItem priority] == 1) {
                    [cell.customTextLabel setTextColor:[UIColor orangeColor]];
                    [cell.customTextLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
                    [cell.customTextLabel setText:@"Important"];
                } else if ([refListItem priority] == 2) {
                    [cell.customTextLabel setTextColor:[UIColor colorWithRed:204 / 255.0 green:0 blue:0 alpha:1.0]];
                    [cell.customTextLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
                    [cell.customTextLabel setText:@"Critical"];
                }

                break;
            case 4: // Completion date
                [cell.detailButton setImage:[UIImage imageNamed:@"ArchivedDateTimeIcon.png"] forState:UIControlStateNormal];
				//cell.imageView.image = [UIImage imageNamed:@"DateIcon.png"];
				[myTextView setFrame:CGRectZero];
				[myTextView setHidden:YES];
                
				NSDate *theDate = [MethodHelper dateFromString:refListItem.archivedDateTime usingFormat:K_DATETIME_FORMAT];
                
                [cell.customTextLabel setTextColor:[UIColor darkTextColor]];
                cell.customTextLabel.text = [MethodHelper localizedDateTimeFrom:theDate usingDateStyle:NSDateFormatterMediumStyle 
                                                                   andTimeStyle:NSDateFormatterMediumStyle];
              
                
                
				
				break;
			default:
				break;
		}
	}

	
    return cell;
}

@end
