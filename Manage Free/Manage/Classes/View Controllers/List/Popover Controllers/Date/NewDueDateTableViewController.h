//
//  NewDueDateTableViewController.h
//  Manage
//
//  Created by Cliff Viegas on 22/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Master class
#import "DueDateTableViewController.h"

@protocol NewListItemDueDateDelegate
- (void)updateDueDate:(NSString *)newDueDate;
@end

@interface NewDueDateTableViewController : DueDateTableViewController {
	id <NewListItemDueDateDelegate> delegate;
	
	NSString		*dueDate;
}

@property (nonatomic, assign)		id				delegate;
@property (nonatomic, copy)			NSString		*dueDate;

#pragma mark Initialisation
- (id)initWithDueDate:(NSString *)theDate andDelegate:(id<NewListItemDueDateDelegate>)theDelegate;

@end
