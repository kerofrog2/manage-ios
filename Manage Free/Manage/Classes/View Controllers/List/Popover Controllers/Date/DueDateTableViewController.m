//
//  DueDateTableViewController.m
//  Manage
//
//  Created by Cliff Viegas on 12/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header file
#import "DueDateTableViewController.h"

// Data Models
#import "ListItem.h"

// Method Helper
#import "MethodHelper.h"

@implementation DueDateTableViewController

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    [super dealloc];
}


#pragma mark -
#pragma mark Initialisation

- (id)initWithListItem:(ListItem *)theListItem {
	if ((self = [super init])) {
		// Set title
		self.title = @"Due Date";
		
		// Init the list item
		refListItem = theListItem;
		

		// Create clear bar button item
		UIBarButtonItem *clearBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear" 
																			   style:UIBarButtonItemStyleBordered 
																			  target:self 
																			  action:@selector(clearBarButtonItemAction)];
		[self.navigationItem setRightBarButtonItem:clearBarButtonItem animated:NO];
		[clearBarButtonItem release];
		
	}
	return self;
}


#pragma mark -
#pragma mark View lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
//	[self.monthView reload];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	[self setContentSizeForViewInPopover:CGSizeMake(self.monthView.frame.size.width, 
													self.monthView.frame.size.height)];
	[UIView commitAnimations];

	[self.monthView reload];

	if (refListItem.dueDate != nil && [refListItem.dueDate length] > 0) {
		NSDate *theDate = [MethodHelper dateGMTFromString:refListItem.dueDate usingFormat:K_DATEONLY_FORMAT];
		[self.monthView selectDate:theDate];
	}
}


- (void)viewDidLoad {
	[super viewDidLoad];
	
	if (refListItem.dueDate != nil && [refListItem.dueDate length] > 0) {
        NSDate *theDate = [MethodHelper dateGMTFromString:refListItem.dueDate usingFormat:K_DATEONLY_FORMAT];
		[self.monthView selectDate:theDate];
	}	
}

#pragma mark -
#pragma mark Tapku Calendar Delegates

- (void)calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)d {
	// Update the date
	refListItem.dueDate = [MethodHelper stringGMTFromDate:d usingFormat:K_DATEONLY_FORMAT];
	
	[self performSelector:@selector(popViewDelay) withObject:nil afterDelay:0.2f];
	
}

- (void) calendarMonthView:(TKCalendarMonthView*)monthView monthDidChange:(NSDate*)month animated:(BOOL)animated {
	[super updateTableOffset:animated];
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4f];
    [UIView setAnimationDelay:0.1f];
    [self setContentSizeForViewInPopover:CGSizeMake(self.monthView.frame.size.width, 
													self.monthView.frame.size.height)];
    [UIView commitAnimations];
}


- (void)popViewDelay {
	[self.navigationController popViewControllerAnimated:YES];
	
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	//NSArray *localArray = [
    return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	/*
	
     // ...
     // Pass the selected object to the new view controller.
	 [self.navigationController pushViewController:detailViewController animated:YES];
	 [detailViewController release];
	 */
}

#pragma mark -
#pragma mark Button Events

- (void)clearBarButtonItemAction {
	// Clear date field
	refListItem.dueDate = @"";
	
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}





@end

