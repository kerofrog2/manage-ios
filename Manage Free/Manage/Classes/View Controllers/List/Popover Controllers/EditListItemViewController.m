//
//  NewListItemPopOver.m
//  Manage
//
//  Created by Cliff Viegas on 5/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "EditListItemViewController.h"

// Custom Table View Cells
#import "ModifyListItemTableViewCell.h"

// View Controllers
#import "DueDateTableViewController.h"
#import "NewSubListItemViewController.h"
#import "ListItemTagsViewController.h"
#import "AlarmsViewController.h"

// EKEvent Classes
//#import <EventKit/EventKit.h>

// Data Collections
#import "TagCollection.h"
#import "ListItemTagCollection.h"
#import "TaskListCollection.h"
#import "AlarmCollection.h"

// Data Models
#import "ListItem.h"
#import "RecurringListItem.h"

// Custom Objects
#import "HiddenToolbar.h"
#import "CustomEdgeInsetsTextView.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#import "ListConstants.h"

// Tags for retrieving content views
#define kTableViewFrame				CGRectMake(0, 0, 320, 318)
#define kTextViewHeightBuffer       11

//#define kTableViewFrame				CGRectMake(0, 0, 320, 372)
//#define kCompressedTableViewFrame	CGRectMake(0, 0, 320, 322)

@interface EditListItemViewController()
// Private Methods

@end

@implementation EditListItemViewController

@synthesize delegate;
@synthesize isArchivesMode;
@synthesize popoverRect;
@synthesize isDueDateViewController;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	if (recurringListItem != nil) {
		[recurringListItem release];
	}
	
	[repeatBarButtonItem release];
	[repeatActiveBarButtonItem release];
	[alarmBarButtonItem release];
	[alarmActiveBarButtonItem release];
	//[myTableView release];
	[originalListItemTitle release];
	[originalListItemNotes release];
	[listItemTitle release];
	[listItemNotes release];
	[originalDueDate release];
	[currentDueDate release];
	[listItemTagCollection release];
	//[alarmCollection release];			// Remove loaded alarms
	//[topRightToolbar release];			// Top right toolbar for holding repeat and alarm buttons
	
    [newSubtaskActionSheet release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<EditListItemDelegate>)theDelegate andSize:(CGSize)theSize andListItem:(ListItem *)theListItem 
	  andTagCollection:(TagCollection *)theTagCollection andIsFilterTagsMode:(BOOL)filterTagsMode
 andTaskListCollection:(TaskListCollection *)theTaskListCollection didAppearFromListView:(BOOL)listViewAppear {
	if ((self = [super init])) {
		// Assign the delegate
		self.delegate = theDelegate;
        
        // Init is due date view controller to false
        self.isDueDateViewController = FALSE;

		// Assign the passed list item reference
		refListItem = theListItem;
		
		// Assign the passed task list collection
		refTaskListCollection = theTaskListCollection;
		
		// Set is filter tag mode bool
		isFilterTagsMode = filterTagsMode;
		
		// Set did appear from list view
		didAppearFromListView = listViewAppear;
		
		// Set did appear from move item view
		didAppearFromMoveItemView = FALSE;
		
		// Init recurring list item
		if (refListItem.recurringListItemID == -1) {
			recurringListItem = nil;
		} else {
			recurringListItem = [[RecurringListItem alloc] initWithRecurringListItemID:refListItem.recurringListItemID];
		}
		
		// Init whether repeating task was updated to prompt for redisplay in list view
		repeatingTaskWasUpdated = FALSE;
		
		// Set edit list item will exit to true
		editListItemWillExit = TRUE;
		
		// Init was list item moved to false
		wasListItemMoved = FALSE;
		
		// Init needs database update to false
		needDBUpdate = FALSE;
		
		// Init is archives mode to false
		self.isArchivesMode = FALSE;
		
		// Init box is ticked to false
		if ([refListItem completed] == TRUE) {
			boxIsTicked = TRUE;
		} else {
			boxIsTicked = FALSE;
		}
		
		// Set the current date
		currentDueDate = [[refListItem dueDate] copy];
		
		// Assign the reference to the tag collection
		refTagCollection = theTagCollection;
		
		// Set the original completed status
		originalCompleted = refListItem.completed;
		
		// Set the original due date
		originalDueDate = [refListItem.dueDate copy];
		
		// Init list item title and notes
		listItemTitle = [refListItem.title copy];
		listItemNotes = [refListItem.notes copy];
		
		// Init original list item title and notes
		originalListItemTitle = [refListItem.title copy];
		originalListItemNotes = [refListItem.notes copy];
		
        
        
        newSubtaskActionSheet = [[UIActionSheet alloc] initWithTitle:@"New Subtask"
                                                            delegate:self 
                                                   cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Add a Subtask", @"Add a Handwritten Subtask", nil];

        
        
		// Init any loaded alarms
		/*alarmCollection = [[AlarmCollection alloc] init];
		if ([refListItem hasAlarms]) {
			
			[alarmCollection loadAlarmsWithListItemID:refListItem.listItemID];
			
			// Update reflistItem data if no alarms found
			if ([alarmCollection.alarms count] == 0) {
				refListItem.hasAlarms = FALSE;
				[refListItem updateDatabase];
			}
		}*/
		
		// Set content size for view in popover
		[self setContentSizeForViewInPopover:theSize];

		// Set the title
		self.title = @"Details";
	}
	return self;
}

// Moving to view did load so we can specify settings before the view is loaded
- (void)viewDidLoad {
	// Load the left bar button
	[self loadButtons];
	
	// Load the table view
	[self loadTableView];
}

#pragma mark -
#pragma mark Load Methods

- (void)loadButtons {
    
    topLeftToolbar = [[HiddenToolbar alloc] initWithFrame:CGRectMake(0, 0, 85, 44)];
    [topLeftToolbar setBarStyle:UIBarStyleBlackTranslucent];
    [topLeftToolbar setOpaque:NO];
    [topLeftToolbar setBackgroundColor:[UIColor clearColor]];
    [topLeftToolbar setTranslucent:YES];
    
    // Array to hold the buttons in toolbar
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] init];
    
    
	// Set the left bar button item, which is for moving tasks
	//UIBarButtonItem *moveBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Move" 
			//															  style:UIBarButtonItemStyleBordered
			//															 target:self action:@selector(moveBarButtonItemAction)];
	
    
    if (refListItem.parentListItemID == -1 && isFilterTagsMode == FALSE && didAppearFromListView == TRUE) {
		UIBarButtonItem *subtaskBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd 
                                                                                              target:self 
                                                                                              action:@selector(subtaskBarButtonItemAction)];
        [toolbarButtons addObject:subtaskBarButtonItem];
        [subtaskBarButtonItem release];
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace 
                                                                                       target:nil action:nil];
        [toolbarButtons addObject:flexibleSpace];
        [flexibleSpace release];
	}
    
    
    
    UIBarButtonItem *moveBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction 
                                                                                       target:self 
                                                                                       action:@selector(moveBarButtonItemAction)];
    
    
    [toolbarButtons addObject:moveBarButtonItem];
    //[self.navigationItem setLeftBarButtonItem:moveBarButtonItem animated:NO];
	[moveBarButtonItem release];
    
    
    [topLeftToolbar setItems:toolbarButtons];
    [self.navigationItem setLeftBarButtonItem:[[[UIBarButtonItem alloc] initWithCustomView:topLeftToolbar] autorelease]];
    [topLeftToolbar release];
    [toolbarButtons release];
    
	
	// Set the right bar button item, which is for repeating tasks
	repeatBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"RepeatIcon.png"] style:UIBarButtonItemStyleBordered target:self 
														  action:@selector(repeatBarButtonItemAction)];
	
	
	[repeatBarButtonItem setImageInsets:UIEdgeInsetsMake(0, 0, 5, 0)];
	//repeatBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Repeat" 
	//																		style:UIBarButtonItemStyleBordered 
	//																	   target:self action:@selector (repeatBarButtonItemAction)];
	
	repeatActiveBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"RepeatActiveIcon.png"] style:UIBarButtonItemStyleBordered 
																target:self action:@selector(repeatBarButtonItemAction)];
	
	[repeatActiveBarButtonItem setImageInsets:UIEdgeInsetsMake(0, 0, 5, 0)];							 
	
	if (recurringListItem == nil) {
		[self.navigationItem setRightBarButtonItem:repeatBarButtonItem animated:NO];
	} else if (recurringListItem != nil) {
		[self.navigationItem setRightBarButtonItem:repeatActiveBarButtonItem animated:NO];
	}
	
	
    
}

/*- (void)loadButtons {
	topRightToolbar = [[HiddenToolbar alloc] initWithFrame:CGRectMake(0, 0, 70, 44)];
	[topRightToolbar setBarStyle:UIBarStyleBlack];
	//[topRightToolbar setTintColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.7 alpha:1.0]];
	topRightToolbar.opaque = NO;
	topRightToolbar.backgroundColor = [UIColor clearColor];
	topRightToolbar.translucent = YES;
	
	// Array to hold the buttons in toolbar
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] init];
	
	// Set the left bar button item, which is for moving tasks
	UIBarButtonItem *moveBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Move" 
																		  style:UIBarButtonItemStyleBordered
																		 target:self action:@selector(moveBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:moveBarButtonItem animated:NO];
	[moveBarButtonItem release];
	
	// Set the right bar button item, which is for repeating tasks
	repeatBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"RepeatIcon.png"] style:UIBarButtonItemStyleBordered target:self 
														  action:@selector(repeatBarButtonItemAction)];
	[repeatBarButtonItem setImageInsets:UIEdgeInsetsMake(0, 0, 5, 0)];
	
	
	// Setup active version of alarm
	UIImage *alarmActiveImage = [UIImage imageNamed:@"AlarmActiveIcon.png"];
	UIButton *alarmActiveButton = [UIButton buttonWithType:UIButtonTypeCustom];
	alarmActiveButton.bounds = CGRectMake(0, 0, alarmActiveImage.size.width, alarmActiveImage.size.height);
	[alarmActiveButton setImage:alarmActiveImage forState:UIControlStateNormal];
	[alarmActiveButton addTarget:self action:@selector(alarmBarButtonItemAction) forControlEvents:UIControlEventTouchUpInside];
	alarmActiveBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:alarmActiveButton];
	[alarmActiveButton release];
	
	//alarmActiveBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"RepeatIcon.png"]
	//															style:UIBarButtonItemStyleBordered target:self 
	//														   action:@selector(alarmBarButtonItemAction)];
	// TODO: Later on need to do test to see whether alarm or alarm active is shown
	//[toolbarButtons addObject:alarmActiveBarButtonItem];
	
	// Setup alarm bar button item
	UIImage *alarmImage = [UIImage imageNamed:@"AlarmIcon.png"];
	UIButton *alarmButton = [UIButton buttonWithType:UIButtonTypeCustom];
	alarmButton.bounds = CGRectMake(0, 0, alarmImage.size.width, alarmImage.size.height);
	[alarmButton setImage:alarmImage forState:UIControlStateNormal];
	[alarmButton addTarget:self action:@selector(alarmBarButtonItemAction) forControlEvents:UIControlEventTouchUpInside];
	alarmBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:alarmButton];
	[alarmButton release];
	
	// Check whether an alarm has been set or not
	
	// Initialise repeat bar button item
	UIImage *repeatImage = [UIImage imageNamed:@"RepeatIcon.png"];
	UIButton *repeatButton = [UIButton buttonWithType:UIButtonTypeCustom];
	repeatButton.bounds = CGRectMake(0, 0, repeatImage.size.width, repeatImage.size.height);
	[repeatButton setImage:repeatImage forState:UIControlStateNormal];
	[repeatButton addTarget:self action:@selector(repeatBarButtonItemAction) forControlEvents:UIControlEventTouchUpInside];
	repeatBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:repeatButton];
	[repeatBarButtonItem setStyle:UIBarButtonItemStyleBordered];
	[repeatButton release];
	
	repeatActiveBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"RepeatActiveIcon.png"] style:UIBarButtonItemStyleBordered 
																					target:self action:@selector(repeatBarButtonItemAction)];
	[repeatActiveBarButtonItem setImageInsets:UIEdgeInsetsMake(0, 0, 5, 0)];	
	
	
	// Initialise repeat active bar button item
	UIImage *repeatActiveImage = [UIImage imageNamed:@"RepeatActiveIcon.png"];
	UIButton *repeatActiveButton = [UIButton buttonWithType:UIButtonTypeCustom];
	repeatActiveButton.bounds = CGRectMake(0, 0, repeatActiveImage.size.width, repeatActiveImage.size.height);
	[repeatActiveButton setImage:repeatActiveImage forState:UIControlStateNormal];
	[repeatActiveButton addTarget:self action:@selector(repeatBarButtonItemAction) forControlEvents:UIControlEventTouchUpInside];
	repeatActiveBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:repeatActiveButton];
	[repeatActiveButton release];
	
	//[self.navigationItem setRightBarButtonItem:alarmBarButtonItem];
	
	if (recurringListItem == nil) {
		[toolbarButtons addObject:repeatBarButtonItem];
	//	[self.navigationItem setRightBarButtonItem:repeatBarButtonItem animated:NO];
	} else if (recurringListItem != nil) {
		[toolbarButtons addObject:repeatActiveBarButtonItem];
	//	[self.navigationItem setRightBarButtonItem:repeatActiveBarButtonItem animated:NO];
	}
	
	if ([refListItem hasAlarms] == TRUE) {
		[toolbarButtons addObject:alarmActiveBarButtonItem];
	} else {
		[toolbarButtons addObject:alarmBarButtonItem];
	}
	
	// Set up the toolbar
	[topRightToolbar setItems:toolbarButtons animated:NO];
	[toolbarButtons release];
	
	UIBarButtonItem *customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:topRightToolbar];
	[self.navigationItem setRightBarButtonItem:customBarButtonItem animated:NO];
	[customBarButtonItem release];
	
	//[self.navigationItem setRightBarButtonItem:topRightToolbar];
	
}*/

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	myTableView.showsVerticalScrollIndicator = FALSE;
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Notes Delegate

- (void)taskNotesUpdated:(NSString *)updatedNotes {
	listItemNotes = [updatedNotes copy];
	refListItem.notes = updatedNotes;
	[myTableView reloadData];
}

#pragma mark -
#pragma mark UIActionSheetDelegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
        // New subtask
        // New sub task selected
        editListItemWillExit = FALSE;
        // New sub task selected
        NewSubListItemViewController *controller = [[[NewSubListItemViewController alloc] initWithDelegate:self.delegate 
                                                                                               andListItem:refListItem
                                                                                          andTagCollection:refTagCollection] autorelease];
        if (refPopoverController) {
            [controller setPopoverController:refPopoverController];
        }
        
        [self.navigationController pushViewController:controller animated:YES];
    } else if (buttonIndex == 1) {
        
        // New handwritten subtask
        [self.delegate newHandwrittenSubtaskCreatedForParentListItem:refListItem];
    } else if (buttonIndex == 2) {
        // Cancel
    }
}

#pragma mark -
#pragma mark Button Actions

// DEPRECATED
/*- (void)doneBarButtonItemAction {
	BOOL updated = TRUE;
	
//	if (originalDueDate == [NSNull null]) {
//		originalDueDate = @"";
//	}
//	
//	if (refListItem.dueDate == [NSNull null]) {
//		refListItem.dueDate = @"";
//	}
	
	if ([listItemTitle isEqualToString:originalListItemTitle] &&
		[listItemNotes isEqualToString:originalListItemNotes] &&
		[originalDueDate isEqualToString:refListItem.dueDate] &&
		originalCompleted == boxIsTicked) {
		updated = FALSE;
	}
	
	// Update the db
	if (updated) {
		//fixCarriageReturns = [fixCarriageReturns stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
		//listItemTitle = [listItemTitle stringByReplacingOccurrencesOfString:@"\n" withString:@""];
		//listItemTitle = [listItemTitle stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
		
		refListItem.title = [listItemTitle copy];
		refListItem.notes = [listItemNotes copy];
		[refListItem updateDatabase];
	}
	
	// Notify the delegate on whether the table view needs to be refreshed or not
	[delegate listItem:refListItem wasUpdated:updated];
}

// DEPRECATED
- (void)cancelBarButtonItemAction {
	// Re-assign original values
	refListItem.title = originalListItemTitle;
	refListItem.notes = originalListItemNotes;
	refListItem.dueDate = originalDueDate;
	
	[delegate dismissEditListItemPopOver];
}*/

- (void)detailButtonAction:(id)sender {
	UIButton *detailButton = (UIButton *)sender;
	
	if (boxIsTicked == FALSE) {
		boxIsTicked = TRUE;
		refListItem.completedDateTime = @"";
		[detailButton setSelected:TRUE];
		//	[detailButton setBackgroundImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlState
	} else if (boxIsTicked == TRUE) {
		boxIsTicked = FALSE;
		refListItem.completedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		[detailButton setSelected:FALSE];
	}
	
	needDBUpdate = TRUE;
	refListItem.completed = boxIsTicked;
}

- (void)moveBarButtonItemAction {
	MoveItemsViewController *controller = [[[MoveItemsViewController alloc] initWithTaskListCollection:refTaskListCollection
																						   andListItem:refListItem
																					  andNavigationBar:YES] autorelease];
	controller.delegate = self;
	controller.modalPresentationStyle = UIModalPresentationCurrentContext;
	[self.navigationController presentModalViewController:controller animated:YES];
}

- (void)subtaskBarButtonItemAction {
    [newSubtaskActionSheet showInView:self.view];
}

- (void)repeatBarButtonItemAction {
	RecurringDetailsViewController *controller = [[[RecurringDetailsViewController alloc] initWithListItem:refListItem] autorelease];
	controller.delegate = self;
	controller.refPopoverController = refPopoverController;
	//controller.modalPresentationStyle = UIModalPresentationCurrentContext;
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)alarmBarButtonItemAction {

	/*AlarmsViewController *controller = [[[AlarmsViewController alloc] initWithAlarmCollection:alarmCollection
																		 andCurrentListItem:refListItem] autorelease];
	controller.refPopoverController = refPopoverController;
	[self.navigationController pushViewController:controller animated:YES];*/

	//EKAlarm *test = [EKAlarm alarmWithRelativeOffset:1];
	
	/*EKEventStore *myStore = [[EKEventStore alloc] init];
	
	EKEvent *myEvent = [EKEvent eventWithEventStore:myStore];
	myEvent.title = @"Test Event";
	myEvent.startDate = [[NSDate alloc] init];
	myEvent.endDate = [[NSDate alloc] initWithTimeInterval:600 sinceDate:myEvent.startDate];
	myEvent.calendar = myStore.defaultCalendarForNewEvents;
	
	NSError *err;
	[myStore saveEvent:myEvent span:EKSpanThisEvent error:&err];
	
	[myStore release];*/
	
/*	UILocalNotification *alarmNotification = [[UILocalNotification alloc] init];
	
	if (alarmNotification == nil) {
		return;
	}
	
	alarmNotification.timeZone = [NSTimeZone defaultTimeZone];
	alarmNotification.fireDate = [[NSDate alloc] initWithTimeInterval:20 sinceDate:[NSDate date]];
	
	alarmNotification.alertBody = @"Alert body";
	alarmNotification.alertAction = @"View ToDo";
	alarmNotification.soundName = UILocalNotificationDefaultSoundName;
	
	
	// Will need to pass key as being the listItem id.  Use this to perhaps go to the item.
	//NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"Dict Title" forKey:800];
	//alarmNotification.userInfo = infoDict;
	
	[[UIApplication sharedApplication] scheduleLocalNotification:alarmNotification];
	[alarmNotification release];
	
	NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
	
	for (UILocalNotification *notification in localNotifications) {
		NSLog(@"Notification: %@, fireDate: %@", notification.alertBody, [MethodHelper stringFromDate:notification.fireDate usingFormat:K_DATEONLY_FORMAT]);
	}*/
	
	// TODO: check if this is ios 4, if earlier, show message
	//NSTimeInterval offset = 20;
	//EKAlarm *testAlarm = [EKAlarm alarmWithRelativeOffset:offset];
	//[testAla
}

#pragma mark -
#pragma mark Recurring Details Delegate 

- (void)didUpdateWithRecurringListItemID:(NSInteger)recurringListItemID {
	// Assign the recurring list id
	refListItem.recurringListItemID = recurringListItemID;
	
	// Set to true that repeating task was updated
	repeatingTaskWasUpdated = TRUE;
	
	// Means recurring list item has changed from being assigned to not, update
	if (refListItem.recurringListItemID == -1) {
		if (recurringListItem != nil) {
			recurringListItem = nil;
			[self.navigationItem setRightBarButtonItem:nil animated:NO];
			[self.navigationItem setRightBarButtonItem:repeatBarButtonItem animated:NO];
			//[self updateTopRightToolbarWithAlarmStatus:YES andRepeatStatus:NO];
			//[self.navigationItem setRightBarButtonItem:topRightToolbar animated:NO];
		}
	} else {
		// Means that there was already a recurring list item, but now this has been changed, refetch.
		if (recurringListItem != nil) {
			// Release first, as we need to re-fetch from db
			[recurringListItem release];
		}
		
		recurringListItem = [[RecurringListItem alloc] initWithRecurringListItemID:refListItem.recurringListItemID];
		
		[self.navigationItem setRightBarButtonItem:nil animated:NO];
		[self.navigationItem setRightBarButtonItem:repeatActiveBarButtonItem animated:NO];
		
		//[self updateTopRightToolbarWithAlarmStatus:YES andRepeatStatus:YES];
		
		//[self.navigationItem setRightBarButtonItem:topRightToolbar animated:NO];
	}
	
	
	
	// Now reset the repeatBarButtonItem with correct 'status', best idea would be to use 'DoneBarButtonItem'
}

#pragma mark -
#pragma mark Move Items Delegates

- (void)didDismissMoveItemsView {
	[self.navigationController dismissModalViewControllerAnimated:YES];
	didAppearFromMoveItemView = TRUE;
}

- (void)didUpdateMoveItemsView {
	// List item was moved
	wasListItemMoved = TRUE;
	
	// Need to do any updates by informing the list view controller
	[self.navigationController dismissModalViewControllerAnimated:YES];
	
	// Need a delegate we can call
	[delegate listItemWasMoved];
}

#pragma mark - PriorityDelegate Methods

- (void)priorityUpdated:(NSInteger)newPriority {
    editListItemWillExit = TRUE;
    
    refListItem.priority = newPriority;
}

#pragma mark -
#pragma mark TableView Delegates

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	if (recurringListItem != nil) {
		if (refListItem.parentListItemID == -1 && isFilterTagsMode == FALSE) {
			if (section == 2) {
				//footerTitle = [NSString stringWithFormat:@"Task is scheduled to repeat every %@", 
				return [self getRepeatingHeaderTitle];
			}
		} else {
			if (section == 1) {
				return [self getRepeatingHeaderTitle];
			}
		}
	}
	
	return @"";
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Only give option of creating sub task to standard tasks
	if (section == 0) {
		return 2;
	} else if (section == 2) {
        return 1;
    }
	
    // Section 1
    
	return 4;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    ModifyListItemTableViewCell *cell = (ModifyListItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ModifyListItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		//[cell.contentView addSubview:[self getSwitch]];
		[cell.contentView addSubview:[self getTextView]];
	}
	
	CustomEdgeInsetsTextView *myTextView = (CustomEdgeInsetsTextView *)[cell.contentView viewWithTag:(kTextViewTag)];
	
	// Set default cell values
	[cell clearTagsView];
	
	cell.textLabel.text = @"";
	cell.customTextLabel.text = @"";
	cell.notesTextLabel.text = @"";
	cell.imageView.image = nil;
	[cell.detailButton setImage:nil forState:UIControlStateNormal];
	[cell.detailButton setImage:nil forState:UIControlStateSelected];
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	
	if (indexPath.section == 0 && indexPath.row == 0) {
		[myTextView setHidden:NO];
		[cell.detailButton setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
		[cell.detailButton setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
		[cell.detailButton addTarget:self action:@selector(detailButtonAction:) forControlEvents:UIControlEventTouchUpInside];
		
		//cell.detailButton.image = [UIImage imageNamed:@"BoxNotTicked.png"];
		// This frame needs to adjust itself based on size of text... hmm.. Ok, can do this... means in textdidchange we just need to come back here
        
        
        // 16 is for the padding added in by Apple
        CGSize theSize = [listItemTitle sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0] 
                                   constrainedToSize:CGSizeMake(kTitleTextViewWidth - 16, 20000) 
                                       lineBreakMode:UILineBreakModeWordWrap];
        
        if (theSize.height == 0) {
            theSize.height = 18;
        }
        
        [myTextView setFrame:CGRectMake(28, 4, 268, theSize.height + kTextViewHeightBuffer)];
        
       // [myTextView setFrame:CGRectMake(28, 4, 268, 73)];
		
		if (boxIsTicked == TRUE) {
			[cell.detailButton setSelected:TRUE];
		} else {
			[cell.detailButton setSelected:FALSE];
		}
		
		if ([listItemTitle length] == 0) {
			[myTextView setTextColor:[UIColor grayColor]];
			[myTextView setText:@"Title"];
		} else if ([listItemTitle length] > 0) {
			[myTextView setTextColor:[UIColor darkTextColor]];
			[myTextView setText:listItemTitle];
		}
        
	} else if (indexPath.section == 0 && indexPath.row == 1) {
        [cell.detailButton setBackgroundImage:[UIImage imageNamed:@"EnteredIcon.png"] forState:UIControlStateNormal];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        // Need to display an 'imageView' in here... use itemsDueTableViewResponder as example
        [cell loadScribbleImageForListItem:refListItem];
    } else if (indexPath.section == 2) {
        
        
        /*if (indexPath.row == 0) {
            [cell.detailButton setBackgroundImage:[UIImage imageNamed:@"NewSubtaskIcon.png"] forState:UIControlStateNormal];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.customTextLabel.textColor = [UIColor darkTextColor];
            cell.customTextLabel.text = @"Add a Subtask";
        } else if (indexPath.row == 1) {
            [cell.detailButton setBackgroundImage:[UIImage imageNamed:@"EnteredIcon.png"] forState:UIControlStateNormal];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.customTextLabel.textColor = [UIColor darkTextColor];
            cell.customTextLabel.text = @"Add a Handwritten Subtask";
        }*/
		
	} else {
		switch (indexPath.row) {
			case 0: // Tag
				[cell.detailButton setImage:[UIImage imageNamed:@"Tag.png"] forState:UIControlStateNormal];
				
				// Need to track tags now...
				if ([refListItem.listItemTagCollection.listItemTags count] == 0) {
					cell.customTextLabel.text = @"Tags";
				} else {
					cell.customTextLabel.text = @"";
				}
				
				cell.selectionStyle = UITableViewCellSelectionStyleBlue;
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				[cell.customTextLabel setTextColor:[UIColor grayColor]];
				
				[cell loadTagsFrom:refListItem.listItemTagCollection andTagCollection:refTagCollection];
				
				break;
			case 1: // Notes
				[cell.detailButton setImage:[UIImage imageNamed:@"NotesIcon.png"] forState:UIControlStateNormal];
				
				CGSize theSize = [listItemNotes sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0]  
										   constrainedToSize:CGSizeMake(kNotesLabelWidth, kMaximumNotesHeight) lineBreakMode:UILineBreakModeWordWrap];
				NSInteger height = theSize.height;
				if (height < 20) {
					height = 20;
				}
				
				[cell.notesTextLabel setFrame:CGRectMake(cell.notesTextLabel.frame.origin.x, 
														 cell.notesTextLabel.frame.origin.y, 
														 cell.notesTextLabel.frame.size.width,
														 height)];
				
				[cell.notesTextLabel setLineBreakMode:UILineBreakModeWordWrap];
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				cell.selectionStyle = UITableViewCellSelectionStyleBlue;
				cell.notesTextLabel.text = listItemNotes;
				
				
				if ([listItemNotes length] == 0) {
					[cell.notesTextLabel setTextColor:[UIColor grayColor]];
					[cell.notesTextLabel setText:@"Notes"];
				} else if ([listItemNotes length] > 0) {
					[cell.notesTextLabel setTextColor:[UIColor darkTextColor]];
					[cell.notesTextLabel setText:listItemNotes];
					
				}
				break;
			case 2: // Due date
				[cell.detailButton setImage:[UIImage imageNamed:@"DateIcon.png"] forState:UIControlStateNormal];
				cell.selectionStyle = UITableViewCellSelectionStyleBlue;
				//cell.imageView.image = [UIImage imageNamed:@"DateIcon.png"];
				[myTextView setFrame:CGRectZero];
				[myTextView setHidden:YES];
				
				if ([refListItem.dueDate length] > 0 && refListItem.dueDate != nil) {
					NSDate *theDate = [MethodHelper dateFromString:refListItem.dueDate usingFormat:K_DATEONLY_FORMAT];
					[cell.customTextLabel setTextColor:[UIColor darkTextColor]];
					cell.customTextLabel.text = [MethodHelper localizedDateFrom:theDate 
																	 usingStyle:NSDateFormatterMediumStyle 
																	   withYear:YES];
				} else {
					[cell.customTextLabel setTextColor:[UIColor grayColor]];
					cell.customTextLabel.text = @"Due Date";
				}
				
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				break;
            case 3: // Priority
                [cell.detailButton setImage:[UIImage imageNamed:@"PriorityIcon.png"] forState:UIControlStateNormal];
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                [myTextView setFrame:CGRectZero];
                [myTextView setHidden:YES];
                if ([refListItem priority] == 0 || [refListItem priority] == 3) {
                    [cell.customTextLabel setTextColor:[UIColor darkGrayColor]];
                    [cell.customTextLabel setText:@"Normal"];
                } else if ([refListItem priority] == 1) {
                    [cell.customTextLabel setTextColor:[UIColor orangeColor]];
                    [cell.customTextLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
                    [cell.customTextLabel setText:@"Important"];
                } else if ([refListItem priority] == 2) {
                    [cell.customTextLabel setTextColor:[UIColor colorWithRed:204 / 255.0 green:0 blue:0 alpha:1.0]];
                    [cell.customTextLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
                    [cell.customTextLabel setText:@"Critical"];
                }
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            default:
				break;
		}
	}
	
	/*switch (indexPath.section) {
		case 0:
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
			[cell.detailButton setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
			[cell.detailButton setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
			[cell.detailButton addTarget:self action:@selector(detailButtonAction:) forControlEvents:UIControlEventTouchUpInside];

			[myTextView setFrame:CGRectMake(28, 4, 268, 73)];
			
			if (boxIsTicked == TRUE) {
				[cell.detailButton setSelected:TRUE];
			} else {
				[cell.detailButton setSelected:FALSE];
			}

			
			
			if ([listItemTitle length] == 0) {
				[myTextView setTextColor:[UIColor lightGrayColor]];
				[myTextView setText:@"Title"];
			} else if ([listItemTitle length] > 0) {
				[myTextView setTextColor:[UIColor darkTextColor]];
				[myTextView setText:listItemTitle];
			}
			break;
		case 1:
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
			[cell.detailButton setBackgroundImage:[UIImage imageNamed:@"NotesIcon.png"] forState:UIControlStateNormal];
			
			[myTextView setFrame:CGRectMake(28, 4, 268, 143)];
			
			if ([listItemNotes length] == 0) {
				[myTextView setTextColor:[UIColor lightGrayColor]];
				[myTextView setText:@"Notes"];
			} else if ([listItemNotes length] > 0) {
				[myTextView setTextColor:[UIColor darkTextColor]];
				[myTextView setText:listItemNotes];
			}
			break;
		case 2:
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
			
			//cell.imageView.image = [UIImage imageNamed:@"DateIcon.png"];
			[myTextView setFrame:CGRectZero];
			[myTextView setHidden:YES];
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			
			if (indexPath.row == 0) {
				[cell.detailButton setBackgroundImage:[UIImage imageNamed:@"DateIcon.png"] forState:UIControlStateNormal];
				if ([refListItem.dueDate length] > 0 && refListItem.dueDate != nil) {
					NSDate *theDate = [MethodHelper dateFromString:refListItem.dueDate usingFormat:K_DATEONLY_FORMAT];
					
					cell.customTextLabel.text = [MethodHelper localizedDateFrom:theDate 
																	 usingStyle:NSDateFormatterMediumStyle 
																	   withYear:YES];
				} else {
					cell.customTextLabel.text = @"Due Date";
					//cell.textLabel.text = @"Due Date";
				}
				
			} else if (indexPath.row == 1) {
				[cell.detailButton setBackgroundImage:[UIImage imageNamed:@"EnteredIcon.png"] forState:UIControlStateNormal];
				cell.customTextLabel.text = @"Add a Subtask";
			}
			
			break;
		default:
			break;
	}*/

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat height = 44;
	
	if (indexPath.section == 0 && indexPath.row == 0) {
		// Title
        //NSLog(@"title height is %f", height);

        CGSize theSize = [listItemTitle sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0] 
                                   constrainedToSize:CGSizeMake(kTitleTextViewWidth - 16, 20000) 
                                       lineBreakMode:UILineBreakModeWordWrap];
        if (theSize.height == 0) {
            theSize.height = 17;
        } else if (theSize.height < 20) {
            height = theSize.height + 25;
        } else {
            height = theSize.height + 22;
        }
        
		
        
        
	} else if (indexPath.section == 0 && indexPath.row == 1) {
        height = 44;
    } else if (indexPath.section == 2) {
		// Add sub task
		height = 44;
	} else {
		switch (indexPath.row) {
			case 0:  // Tags
				height = [refListItem.listItemTagCollection heightOfRowForTagCollection:refTagCollection];
				break;
			case 1:  // Notes
				// Need to work out a variable/dynamic height for notes
				//	CGSize theSize = [listItemNotes sizeWithFont: 
				height = 44;
				
				CGSize theSize = [listItemNotes sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0]  
										   constrainedToSize:CGSizeMake(kNotesLabelWidth, kMaximumNotesHeight) lineBreakMode:UILineBreakModeWordWrap];
				
				if (theSize.height + 20 > 44) {
					height = theSize.height + 20;
				} 
				
				break;
			case 2: // Due date
				height = 44;
				break;
			default:
				break;
		}
	}
	
	return height;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Don't resign responder if notes is selected
	//if (indexPath.section != 1 || indexPath.row != 1) {
		UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
		CustomEdgeInsetsTextView *titleTextView = (CustomEdgeInsetsTextView *)[cell.contentView viewWithTag:kTextViewTag];
		if ([titleTextView isFirstResponder]) {
			[titleTextView resignFirstResponder];
		}
	//}
	
	// Set needs db update
	needDBUpdate = TRUE;
	
    // Scribble selected
    if (indexPath.section == 0 && indexPath.row == 1) {
        if ([self isDueDateViewController] == TRUE && [[refListItem dueDate] length] == 0) {
            [self.delegate dismissEditListItemPopOver];
            return;
        }
        [self.delegate editScribbleForListItem:refListItem usingRect:self.popoverRect];
    }
    
	// Notes selected
	if (indexPath.section == 1 && indexPath.row == 1) {
		editListItemWillExit = FALSE;
		NotesViewController *controller = [[[NotesViewController alloc] initWithDelegate:self andNotes:listItemNotes] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	}
	
	// Due date selected
	if (indexPath.section == 1 && indexPath.row == 2) {
		editListItemWillExit = FALSE;
		DueDateTableViewController *controller = [[[DueDateTableViewController alloc] initWithListItem:refListItem] autorelease];
		[self.navigationController pushViewController:controller animated:YES];

	}
	
	// Tags selected
	if (indexPath.section == 1 && indexPath.row == 0) {
		editListItemWillExit = FALSE;
		ListItemTagsViewController *controller = [[[ListItemTagsViewController alloc] initWithListItemTagCollection:refListItem.listItemTagCollection 
																											  andTagCollection:refTagCollection 
																												 andListItemID:refListItem.listItemID] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	}
	
    
    // Priority selected
    if (indexPath.section == 1 && indexPath.row == 3) {
		editListItemWillExit = FALSE;
        PriorityViewController *controller = [[[PriorityViewController alloc] initWithDelegate:self Priority:refListItem.priority] autorelease];
        [self.navigationController pushViewController:controller animated:YES];
	}
	
}


#pragma mark -
#pragma mark Updating Height of Section Footers and Headers

- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
	if (recurringListItem != nil) {
		if (section == 0) {
			return 40.0;
		}
	}
	
	if (section == 0) {
		return 10.0;
	}
    return 5.0;
}


- (CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section {
	/*if (recurringListItem != nil) {
		if (refListItem.parentListItemID == -1 && isFilterTagsMode == FALSE) {
			if (section == 2) {
				//footerTitle = [NSString stringWithFormat:@"Task is scheduled to repeat every %@",
				return 40.0;
			}
		} else {
			if (section == 1) {
				return 40.0;
			}
		}
	}*/
	
	if (section == 2) {
		return 10.0;
	}
    return 5.0;
}


- (UIView *)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
	if (recurringListItem != nil) {
		
		if (section == 0) {
			UILabel *headerLabel = [[[UILabel alloc] initWithFrame:CGRectMake(25, 0, 270, 35)] autorelease];
			[headerLabel setBackgroundColor:[UIColor clearColor]];
			[headerLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
			[headerLabel setLineBreakMode:UILineBreakModeTailTruncation];
			[headerLabel setTextAlignment:UITextAlignmentCenter];
			[headerLabel setNumberOfLines:2];
			headerLabel.text = [NSString stringWithFormat:@"Task is scheduled to repeat %@.", [self getRepeatingHeaderTitle]];
			return headerLabel;
		}
		
	/*	if (refListItem.parentListItemID == -1 && isFilterTagsMode == FALSE) {
			if (section == 2) {
				//footerTitle = [NSString stringWithFormat:@"Task is scheduled to repeat every %@",
				footerLabel.text = [NSString stringWithFormat:@"Task is scheduled to repeat %@.", [self getRepeatingFooterTitle]];
				return footerLabel;
			}
		} else {
			if (section == 1) {
				footerLabel.text = [self getRepeatingFooterTitle];
				return footerLabel;
			}
		}*/
	}
	
    return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
}

- (UIView *)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section {
    return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
}




#pragma mark -
#pragma mark UIViewController Delegates

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidDisappear:(BOOL)animated {
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	CustomEdgeInsetsTextView *titleTextView = (CustomEdgeInsetsTextView *)[cell.contentView viewWithTag:kTextViewTag];
	[titleTextView resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated {
	if (didAppearFromListView == TRUE) {
		//UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
		//CustomEdgeInsetsTextView *titleTextView = (CustomEdgeInsetsTextView *)[cell.contentView viewWithTag:kTextViewTag];
		//[titleTextView becomeFirstResponder];
		//didAppearFromListView = FALSE;
	} else if (didAppearFromMoveItemView == TRUE) {
		// Do nothing
		didAppearFromMoveItemView = FALSE;
	} else {
		if (refPopoverController) {
			[refPopoverController setPopoverContentSize:kPopoverSize animated:YES];
		}
	}
	
	editListItemWillExit = TRUE;
	
	[super viewDidAppear:animated];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
	// Register for keyboard notifications
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) 
	//											 name:UIKeyboardDidShowNotification object:self.view.window]; 
	//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) 
	//											 name:UIKeyboardWillHideNotification object:self.view.window]; 
	
	[delegate editListItemAtBaseLevel:TRUE];
	
	if (wasListItemMoved != TRUE) {
		[myTableView reloadData];
	}
	
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	[delegate editListItemAtBaseLevel:FALSE];
	
	if ((editListItemWillExit == TRUE && wasListItemMoved == FALSE && needDBUpdate == TRUE)
		|| repeatingTaskWasUpdated == TRUE) {
		// Do updates
		// Get rid of any carriage returns
		listItemTitle = [[listItemTitle stringByReplacingOccurrencesOfString:@"\n" withString:@" "] copy];
		refListItem.title = listItemTitle;
		refListItem.notes = listItemNotes;
		
		[refListItem updateDatabase];
		
		// Notify the delegate on whether the table view needs to be refreshed or not
        BOOL completedChanged = FALSE;
        if ([refListItem completed] != originalCompleted) {
            completedChanged = TRUE;
        }
        
		[delegate listItem:refListItem wasUpdated:YES completedStatusChanged:completedChanged];
	}
	
	// Update database
	//[refListItem updateDatabase];
	
	[super viewWillDisappear:animated];
}

- (void)keyboardDidShow:(NSNotification *)theNotification {
	//if (UIInterfaceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
	//	[myTableView setFrame:kCompressedTableViewFrame];
	//} else if (UIInterfaceOrientationIsPortrait([UIDevice currentDevice].orientation)) {
	//	[myTableView setFrame:kTableViewFrame];
	//}
}

- (void)keyboardWillHide:(NSNotification *)theNotification {
	//[myTableView setFrame:kTableViewFrame];
}

#pragma mark -
#pragma mark Public Set Methods

- (void)setPopoverController:(UIPopoverController *)thePopoverController {
	refPopoverController = thePopoverController;
}

#pragma mark -
#pragma mark Helper Methods

// Update the top right toolbar depending on the passed statuses
- (void)updateTopRightToolbarWithAlarmStatus:(BOOL)alarmStatus andRepeatStatus:(BOOL)repeatStatus {
	// Set top right bar button item to nil
	[self.navigationItem setRightBarButtonItem:nil animated:NO];
	
	// Set up toolbar buttons
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] initWithCapacity:2];
	
	if (alarmStatus == TRUE) {
		[toolbarButtons addObject:alarmActiveBarButtonItem];
	} else {
		[toolbarButtons addObject:alarmBarButtonItem];
	}
	
	if (repeatStatus == TRUE) {
		[toolbarButtons addObject:repeatActiveBarButtonItem];
	} else {
		[toolbarButtons addObject:repeatBarButtonItem];
	}
	
	[topRightToolbar setItems:toolbarButtons animated:NO];
    [toolbarButtons release];
	UIBarButtonItem *customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:topRightToolbar];
	[self.navigationItem setRightBarButtonItem:customBarButtonItem animated:NO];
	[customBarButtonItem release];
}

- (NSString *)getRepeatingHeaderTitle {
	if (recurringListItem == nil) {
		return @"";
	}
	
	if (recurringListItem.frequency == 0 && [recurringListItem.frequencyMeasurement isEqualToString:@""] && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
		// None
		return @"";
	} else if (recurringListItem.frequency == 1 && [recurringListItem.frequencyMeasurement isEqualToString:@""] && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
		// Daily
		return @"daily";
	} else if (recurringListItem.frequency == 7 && [recurringListItem.frequencyMeasurement isEqualToString:@""] && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
		// Weekly
		return @"weekly";
	} else if (recurringListItem.frequency == 14 && [recurringListItem.frequencyMeasurement isEqualToString:@""] && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
		return @"every 2 weeks";
	} else if ([recurringListItem.frequencyMeasurement isEqualToString:@""] == FALSE && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
		if (recurringListItem.frequency == 1) {
			NSString *step1 = [recurringListItem.frequencyMeasurement stringByReplacingOccurrencesOfString:@"s" withString:@""];
			NSString *step2 = [step1 lowercaseString];
			return [NSString stringWithFormat:@"every %@", step2];
		} else {
			NSString *lowerCase = [recurringListItem.frequencyMeasurement lowercaseString];
			return [NSString stringWithFormat:@"every %d %@", recurringListItem.frequency, lowerCase];
		}
	} else if ([recurringListItem.daysOfTheWeek isEqualToString:@""] == FALSE
			   && [recurringListItem.frequencyMeasurement isEqualToString:@""] == TRUE) {
		// Need to recover the days of the week
		if ([recurringListItem.daysOfTheWeek isEqualToString:@"Saturday, Sunday"]) {
			return @"every weekend";
		} else if ([recurringListItem.daysOfTheWeek isEqualToString:@"Monday, Tuesday, Wednesday, Thursday, Friday"]) {
			return @"every weekday";
		} else if ([recurringListItem.daysOfTheWeek isEqualToString:@"Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday"]) {
			return @"every day";
		} else {
			return [NSString stringWithFormat:@"every %@", recurringListItem.daysOfTheWeek];
		}
	} else if ([recurringListItem frequency] == 0 && [recurringListItem.frequencyMeasurement length] > 0
			   && [recurringListItem.daysOfTheWeek length] > 0) {
		return [NSString stringWithFormat:@"on the %@ %@ of each month",
				[recurringListItem.frequencyMeasurement lowercaseString],
				recurringListItem.daysOfTheWeek];
	}
	
	return @"";
	
}

- (CustomEdgeInsetsTextView *)getTextView {
	CustomEdgeInsetsTextView *textView = [[[CustomEdgeInsetsTextView alloc] initWithFrame:CGRectZero] autorelease];
	textView.tag = kTextViewTag;
	[textView setBackgroundColor:[UIColor clearColor]];
	
	[textView setDelegate:self];
	[textView setText:@""];
	[textView setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0]];
	[textView setTextColor:[UIColor darkTextColor]];
	[textView setDirectionalLockEnabled:YES];
	[textView setShowsVerticalScrollIndicator:NO];
	[textView setAlwaysBounceVertical:NO];
	[textView setHidden:YES];
    [textView setReturnKeyType:UIReturnKeyDone];
	
    // 1.6 Updates
    [textView setScrollEnabled:FALSE];
	[textView setContentInset:UIEdgeInsetsZero];
	
	return textView;
}

- (UILabel *)getLabel {
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	[label setBackgroundColor:[UIColor clearColor]];
	[label setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
	[label setTextColor:[UIColor darkTextColor]];
	[label setLineBreakMode:UILineBreakModeWordWrap];
	[label setTag:kLabelTag];
	
	return label;
}

/*- (UISwitch *)getSwitch {
	UISwitch *switchView = [[[UISwitch alloc] initWithFrame:CGRectZero] autorelease];
	switchView.tag = kSwitchTag;
	// We dont want user interaction, clicking on table view cell will change this value
	[switchView setFrame:CGRectMake(200, 8, 0, 0)];
	switchView.userInteractionEnabled = FALSE;
	[switchView setHidden:YES];
	return switchView;
}*/

#pragma mark -
#pragma mark TextView Delegates

- (void)textViewDidChange:(CustomEdgeInsetsTextView *)textView {
	if (textView.tag == kTextViewTag) {
		// Title text view
		listItemTitle = [textView.text copy];
		// Just set the ref list item title
		refListItem.title = textView.text;
		
	} 
    
    // Just reload the table view if this exceeds the bounds
    
    // 16 is for the padding added in by Apple
   
    CGSize theSize = [listItemTitle sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0] 
                               constrainedToSize:CGSizeMake(kTitleTextViewWidth - 16, 20000) 
                                   lineBreakMode:UILineBreakModeWordWrap];
    
    if (theSize.height == 0) {
        theSize.height = 18;
    }
    
    if (theSize.height + kTextViewHeightBuffer != textView.frame.size.height) {
        [UIView beginAnimations:@"Text Field Resize" context:nil];
        [UIView setAnimationDuration:0.5f];
        
        textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y, 
                                    textView.frame.size.width, 
                                    theSize.height + kTextViewHeightBuffer);
        
        
        [myTableView beginUpdates];
        [myTableView endUpdates];
        //[myTableView reloadData];
        
        [UIView commitAnimations];
        
        //[myTableView reloadData];
    }
    
    
    
    /*
     [cell.detailButton setImage:[UIImage imageNamed:@"NotesIcon.png"] forState:UIControlStateNormal];
     
     CGSize theSize = [listItemNotes sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0]  
     constrainedToSize:CGSizeMake(kNotesLabelWidth, kMaximumNotesHeight) lineBreakMode:UILineBreakModeWordWrap];
     NSInteger height = theSize.height;
     if (height < 20) {
     height = 20;
     }
     
     [cell.notesTextLabel setFrame:CGRectMake(cell.notesTextLabel.frame.origin.x, 
     cell.notesTextLabel.frame.origin.y, 
     cell.notesTextLabel.frame.size.width,
     height)];
     
     [cell.notesTextLabel setLineBreakMode:UILineBreakModeWordWrap];
     cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
     cell.selectionStyle = UITableViewCellSelectionStyleBlue;
     cell.notesTextLabel.text = listItemNotes;
     
     
     if ([listItemNotes length] == 0) {
     [cell.notesTextLabel setTextColor:[UIColor grayColor]];
     [cell.notesTextLabel setText:@"Notes"];
     } else if ([listItemNotes length] > 0) {
     [cell.notesTextLabel setTextColor:[UIColor darkTextColor]];
     [cell.notesTextLabel setText:listItemNotes];
     
     }
     break;
     
     */
    
    // Now need to see if we need to adjust the size of the text view
    
    
	needDBUpdate = TRUE;
}

- (void)textViewDidEndEditing:(CustomEdgeInsetsTextView *)textView {
	if ([listItemTitle length] == 0 && [textView tag] == kTextViewTag) {
		[textView setText:@"Title"];
		[textView setTextColor:[UIColor grayColor]]; 
	} //else if ([listItemNotes length] == 0 && [textView tag] == kTextViewTag + 1) {
		//[textView setText:@"Notes"];
		//[textView setTextColor:[UIColor grayColor]]; 
	//}
}

- (void)textViewDidBeginEditing:(CustomEdgeInsetsTextView *)textView {
	/*UIDeviceOrientation newOrientation = [UIDevice currentDevice].orientation;
	 if (newOrientation == UIDeviceOrientationUnknown ||
	 newOrientation == lastOrientation) {
	 return;
	 }
	 if (!UIDeviceOrientationIsPortrait(newOrientation) &&
	 !UIDeviceOrientationIsLandscape(newOrientation)) {
	 return;
	 }*/
	
	if ([textView.text isEqualToString:@"Title"] && [textView tag] == kTextViewTag &&
		[listItemTitle length] == 0) {
		// Nothing here
		[textView setText:@""];
		[textView setTextColor:[UIColor darkTextColor]];
		
	} //else if ([textView.text isEqualToString:@"Notes"] && [textView tag] == (kTextViewTag + 1) &&
		//	   [listItemNotes length] == 0) {
		//[textView setText:@""];
		//[textView setTextColor:[UIColor darkTextColor]];
	//}
}

- (BOOL)textView:(CustomEdgeInsetsTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


@end
