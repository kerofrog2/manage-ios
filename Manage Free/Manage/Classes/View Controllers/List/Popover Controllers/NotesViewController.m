//
//  NotesViewController.m
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "NotesViewController.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kTextViewFrame		CGRectMake(6, 4, 285, 288)
#define kPopoverSize		CGSizeMake(320, 318)
#define kTextViewTag		93721
#define kRowHeight			298

@implementation NotesViewController

@synthesize delegate;
@synthesize notes;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[self.notes release];
	[myTableView release];
	[super dealloc];
}


#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<NotesDelegate>)theDelegate andNotes:(NSString *)taskNotes {
	if ((self = [super init])) {
		// Set the title
		self.title = @"Edit Notes";
		
		// Set the notes
		self.notes = taskNotes;
		
		// Assign the delegate
		self.delegate = theDelegate;
		
		//[self setContentSizeForViewInPopover:kPopoverSize];
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		[self loadTableView];
	
		[self loadButtons];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	[myTableView setRowHeight:kRowHeight];
	
	[self.view addSubview:myTableView];
	
}

- (void)loadButtons {
	// Set the right bar button and the left bar button
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone 
																					   target:self 
																					   action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
																						 target:self 
																						 action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
	[cancelBarButtonItem release];
	
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	
	// Create a new list item and add it to the list item collection (task list)
	// Will need a delegate for this! bah
	//[delegate createNewListItemWithTitle:listItemTitle andNotes:listItemNotes andCompleted:boxIsTicked andDueDate:currentDueDate];
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextView *myTextView = (UITextView *)[cell.contentView viewWithTag:kTextViewTag];
	
	// Resign responder when exiting
	if ([myTextView isFirstResponder]) {
		[myTextView resignFirstResponder];
	}
	
	[delegate taskNotesUpdated:myTextView.text];
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextView *notesTextView = (UITextView *)[cell.contentView viewWithTag:kTextViewTag];
	if ([notesTextView isFirstResponder]) {
		[notesTextView resignFirstResponder];
	}
	
	[self.navigationController popViewControllerAnimated:YES];
	//[delegate dismissNewListItemPopOver];
}

#pragma mark -
#pragma mark UIViewController Delegates



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self setContentSizeForViewInPopover:kPopoverSize];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextView *notesTextView = (UITextView *)[cell.contentView viewWithTag:kTextViewTag];
	[notesTextView becomeFirstResponder];
	
	
	
	/*[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	
	[UIView commitAnimations];*/
	
	[super viewDidAppear:animated];
}

#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		
		//[cell.contentView addSubview:[self getSwitch]];
		[cell.contentView addSubview:[self getTextView]];
	}
	
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	
	return cell;
}

#pragma mark -
#pragma mark Helper Methods

- (UITextView *)getTextView {
	UITextView *textView = [[[UITextView alloc] initWithFrame:kTextViewFrame] autorelease];
	[textView setBackgroundColor:[UIColor clearColor]];
	
	[textView setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
	[textView setBackgroundColor:[UIColor clearColor]];
	[textView setTextColor:[UIColor darkTextColor]];
	[textView setShowsVerticalScrollIndicator:NO];
	[textView setAlwaysBounceVertical:NO];
	[textView setTag:kTextViewTag];
	[textView setText:self.notes];
	
	//[textView set
	
	return textView;
}


@end
