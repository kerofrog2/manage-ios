    //
//  NewListItemPopOver.m
//  Manage
//
//  Created by Cliff Viegas on 6/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Method Helper
#import "MethodHelper.h"

// Header File
#import "NewListItemViewController.h"

// Custom Table View Cells
#import "ModifyListItemTableViewCell.h"

// View Controllers
#import "ListItemTagsViewController.h"

// Data Collections
#import "ListItemTagCollection.h"
#import "TagCollection.h"

// Custom Objects
#import "CustomEdgeInsetsTextView.h"

// Data Models
#import "ListItemTag.h"

// Tags for retrieving content views
#define kTableViewFrame				CGRectMake(0, 0, 320, 318)
#define kPopoverSize				CGSizeMake(320, 318)
//#define kTableViewFrame				CGRectMake(0, 0, 320, 372)
//#define kCompressedTableViewFrame	CGRectMake(0, 0, 320, 190)
#define kSwitchTag					9868
#define kLabelTag					8334
#define kMaximumNotesHeight			300 
#define kNotesLabelWidth			240
#define kTextViewHeightBuffer       11

@interface NewListItemViewController()
// Private Methods
- (UISwitch *)getSwitch;
@end


@implementation NewListItemViewController

@synthesize delegate;
@synthesize listItemTitle;
@synthesize listItemNotes;
@synthesize currentDueDate;
@synthesize listItemTagCollection;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[self.listItemTitle release];
	[self.listItemNotes release];

	//[myTableView release];
	[self.currentDueDate release];
    [self.listItemTagCollection release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<NewListItemDelegate>)theDelegate andSize:(CGSize)theSize andTagCollection:(TagCollection *)theTagCollection {
	if ((self = [super init])) {
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Init box is ticked to false
		boxIsTicked = FALSE;
		
		// Update my popover size
		myPopoverSize = theSize;
		
		// Init the list item tag collection
		self.listItemTagCollection = [[ListItemTagCollection alloc] init];
		
		// Assign the reference to the tag collection
		refTagCollection = theTagCollection;
		
		// Set content size for view in popover
		[self setContentSizeForViewInPopover:myPopoverSize];
		
		// Set current due date
		self.currentDueDate = @"";
        
        // Init priority to normal
        listItemPriority = 0;
        
        self.listItemTitle = @"";
        self.listItemNotes = @"";
		
		// Set the title
		self.title = @"New Task";

		// Load the left and right bar buttons
		[self loadButtons];
		
		// Load the table view
		[self loadTableView];
		
		// Set that this did appear from list view
		didAppearFromListView = TRUE;
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadButtons {
	// Set the right bar button and the left bar button
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone 
																					   target:self 
																					   action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
																						 target:self 
																						 action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
	[cancelBarButtonItem release];
	
}

- (void)loadTableView {
	// Init list item title and list item notes to blank strings
	self.listItemTitle = @"";
	self.listItemNotes = @"";
	
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	//myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 400) style:UITableViewStyleGrouped];
	//[myTableView setAllowsSelection:FALSE];

	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	//[myTableView setBackgroundView:nil];
	//[myTableView setBackgroundColor:[UIColor colorWithRed:0.24313 green:0.22352 blue:0.22352 alpha:1.0]];
	
	[self.view addSubview:myTableView];
	[myTableView reloadData];
}

#pragma mark -
#pragma mark New List Item Due Date Delegate

- (void)updateDueDate:(NSString *)newDueDate {
	self.currentDueDate = newDueDate;
	
	[myTableView reloadData];
}

#pragma mark -
#pragma mark Notes Delegate

- (void)taskNotesUpdated:(NSString *)updatedNotes {
	self.listItemNotes = updatedNotes;
	[myTableView reloadData];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	// Create a new list item and add it to the list item collection (task list)
	// Get rid of any carriage returns
	self.listItemTitle = [self.listItemTitle stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
	
	// Will need a delegate for this! bah
	[delegate createNewListItemWithTitle:listItemTitle andNotes:self.listItemNotes andCompleted:boxIsTicked andDueDate:self.currentDueDate andPriority:listItemPriority andListItemTagCollection:self.listItemTagCollection];
}

- (void)cancelBarButtonItemAction {
	[delegate dismissNewListItemPopOver];
}

- (void)detailButtonAction:(id)sender {
	UIButton *detailButton = (UIButton *)sender;
	
	if (boxIsTicked == FALSE) {
		boxIsTicked = TRUE;
		[detailButton setSelected:TRUE];
	//	[detailButton setBackgroundImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlState
	} else if (boxIsTicked == TRUE) {
		boxIsTicked = FALSE;
		[detailButton setSelected:FALSE];
	}
}

#pragma mark -
#pragma mark TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
		return 1;
	}
	
	return 4;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    ModifyListItemTableViewCell *cell = (ModifyListItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ModifyListItemTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		
		//[cell.contentView addSubview:[self getSwitch]];
		[cell.contentView addSubview:[self getTextView]];
	}
	
	CustomEdgeInsetsTextView *myTextView = (CustomEdgeInsetsTextView *)[cell.contentView viewWithTag:(kTextViewTag)];
	
	// Set default cell values
	[cell clearTagsView];
	
	cell.textLabel.text = @"";
	cell.customTextLabel.text = @"";
	cell.notesTextLabel.text = @"";
	cell.imageView.image = nil;
	[cell.detailButton setImage:nil forState:UIControlStateNormal];
	[cell.detailButton setImage:nil forState:UIControlStateSelected];
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	if (indexPath.section == 0) {  
		[cell.detailButton setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
		[cell.detailButton setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
		[cell.detailButton addTarget:self action:@selector(detailButtonAction:) forControlEvents:UIControlEventTouchUpInside];
		
        
        //cell.detailButton.image = [UIImage imageNamed:@"BoxNotTicked.png"];
        
        // 16 is for the padding added in by Apple
        CGSize theSize = [self.listItemTitle sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0] 
                                   constrainedToSize:CGSizeMake(kTitleTextViewWidth - 16, 20000) 
                                       lineBreakMode:UILineBreakModeWordWrap];
        
        if (theSize.height == 0) {
            theSize.height = 18;
        }
        
        [myTextView setFrame:CGRectMake(28, 4, 268, theSize.height + kTextViewHeightBuffer)];
        
        // [myTextView setFrame:CGRectMake(28, 4, 268, 73)];
		
		if (boxIsTicked == TRUE) {
			[cell.detailButton setSelected:TRUE];
		} else {
			[cell.detailButton setSelected:FALSE];
		}
		
		if ([self.listItemTitle length] == 0) {
			[myTextView setTextColor:[UIColor grayColor]];
			[myTextView setText:@"Title"];
		} else if ([self.listItemTitle length] > 0) {
			[myTextView setTextColor:[UIColor darkTextColor]];
			[myTextView setText:self.listItemTitle];
		}
        

	} else {
		switch (indexPath.row) {
			case 0: // Tag
				[cell.detailButton setImage:[UIImage imageNamed:@"Tag.png"] forState:UIControlStateNormal];
				
				// Need to track tags now...
				if ([self.listItemTagCollection.listItemTags count] == 0) {
					cell.customTextLabel.text = @"Tags";
				} else {
					cell.customTextLabel.text = @"";
				}
				
				cell.selectionStyle = UITableViewCellSelectionStyleBlue;
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				[cell.customTextLabel setTextColor:[UIColor grayColor]];
				
				[cell loadTagsFrom:self.listItemTagCollection andTagCollection:refTagCollection];
				
				break;
			case 1: // Notes
				[cell.detailButton setImage:[UIImage imageNamed:@"NotesIcon.png"] forState:UIControlStateNormal];
				
				CGSize theSize = [self.listItemNotes sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0]  
										constrainedToSize:CGSizeMake(kNotesLabelWidth, kMaximumNotesHeight) lineBreakMode:UILineBreakModeWordWrap];
				NSInteger height = theSize.height;
				if (height < 20) {
					height = 20;
				}
				
				[cell.notesTextLabel setFrame:CGRectMake(cell.notesTextLabel.frame.origin.x, 
														 cell.notesTextLabel.frame.origin.y, 
														 cell.notesTextLabel.frame.size.width,
														 height)];
				
				[cell.notesTextLabel setLineBreakMode:UILineBreakModeWordWrap];
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				cell.selectionStyle = UITableViewCellSelectionStyleBlue;
				cell.notesTextLabel.text = self.listItemNotes;
				
				
				if ([self.listItemNotes length] == 0) {
					[cell.notesTextLabel setTextColor:[UIColor grayColor]];
					[cell.notesTextLabel setText:@"Notes"];
				} else if ([self.listItemNotes length] > 0) {
					[cell.notesTextLabel setTextColor:[UIColor darkTextColor]];
					[cell.notesTextLabel setText:self.listItemNotes];
					
				}
				break;
			case 2: // Due date
				[cell.detailButton setImage:[UIImage imageNamed:@"DateIcon.png"] forState:UIControlStateNormal];
				cell.selectionStyle = UITableViewCellSelectionStyleBlue;
				//cell.imageView.image = [UIImage imageNamed:@"DateIcon.png"];
				[myTextView setFrame:CGRectZero];
				[myTextView setHidden:YES];
				
				if ([self.currentDueDate length] > 0 && self.currentDueDate != nil) {
					NSDate *theDate = [MethodHelper dateFromString:self.currentDueDate usingFormat:K_DATEONLY_FORMAT];
					[cell.customTextLabel setTextColor:[UIColor darkTextColor]];
					cell.customTextLabel.text = [MethodHelper localizedDateFrom:theDate 
																	 usingStyle:NSDateFormatterMediumStyle 
																	   withYear:YES];
				} else {
					[cell.customTextLabel setTextColor:[UIColor grayColor]];
					cell.customTextLabel.text = @"Due Date";
				}
				
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				break;
			
            case 3:
                // Priority
                [cell.detailButton setImage:[UIImage imageNamed:@"PriorityIcon.png"] forState:UIControlStateNormal];
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                [myTextView setFrame:CGRectZero];
                [myTextView setHidden:YES];
                if (listItemPriority == 0) {
                    [cell.customTextLabel setTextColor:[UIColor darkGrayColor]];
                    [cell.customTextLabel setText:@"Normal"];
                } else if (listItemPriority == 1) {
                    [cell.customTextLabel setTextColor:[UIColor orangeColor]];
                    [cell.customTextLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
                    [cell.customTextLabel setText:@"Important"];
                } else if (listItemPriority == 2) {
                    [cell.customTextLabel setTextColor:[UIColor colorWithRed:204 / 255.0 green:0 blue:0 alpha:1.0]];
                    [cell.customTextLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
                    [cell.customTextLabel setText:@"Critical"];
                }
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
			default:
				break;
		}
	}
	
	/*UISwitch *mySwitch = (UISwitch *)[cell.contentView viewWithTag:kSwitchTag];
	 
	 NSArray *propertySection = [propertyCollection.properties objectAtIndex:indexPath.section];
	 PropertyDetail *propertyDetail = [propertySection objectAtIndex:indexPath.row];
	 cell.textLabel.text = propertyDetail.friendlyName;
	 
	 // Get the correct application setting
	 ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:[propertyDetail name]];
	   
	 cell.detailTextLabel.text = [appSetting data];
	 
	 if ([propertyDetail propertyType] == EnumPropertyTypeSettingBoolean) {
	 // Show switch
	 [mySwitch setHidden:NO];
	 [mySwitch setOn:[[appSetting data] boolValue]];
	 cell.accessoryType = UITableViewCellAccessoryNone;
	 cell.detailTextLabel.text = @"";
	 } else {
	 // Do not show switch
	 [mySwitch setHidden:YES];
	 cell.detailTextLabel.text = [appSetting data];
	 
	 if ([propertyDetail enabled]) {
	 cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	 } else {
	 cell.accessoryType = UITableViewCellAccessoryNone;
	 }
	 }
	 */
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	
	// Due date selected
	if (indexPath.section == 1 && indexPath.row == 2) {
		
		NewDueDateTableViewController *controller = [[[NewDueDateTableViewController alloc] initWithDueDate:self.currentDueDate
																								andDelegate:self] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
		
	}
	
	// Notes selected
	if (indexPath.section == 1 && indexPath.row == 1) {
		NotesViewController *controller = [[[NotesViewController alloc] initWithDelegate:self andNotes:self.listItemNotes] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	}
	
	// Tags selected
	if (indexPath.section == 1 && indexPath.row == 0) {
		ListItemTagsViewController *controller = [[[ListItemTagsViewController alloc] initNewListItemWithListItemTagCollection:self.listItemTagCollection 
																								   andTagCollection:refTagCollection 
																									  andListItemID:-1] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	}
    
    // Priority selected
    if (indexPath.section == 1 && indexPath.row == 3) {
        PriorityViewController *controller = [[[PriorityViewController alloc] initWithDelegate:self Priority:listItemPriority] autorelease];
        [self.navigationController pushViewController:controller animated:YES];
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat height = 44;
	
	/*switch (indexPath.section) {
		case 0:
			height = 100;
			break;
		case 1:
			height = 163;
			break;
		default:
			break;
	}*/
	
	if (indexPath.section == 0) {
		CGSize theSize = [self.listItemTitle sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0] 
                                   constrainedToSize:CGSizeMake(kTitleTextViewWidth - 16, 20000) 
                                       lineBreakMode:UILineBreakModeWordWrap];
        if (theSize.height == 0) {
            theSize.height = 17;
        } else if (theSize.height < 20) {
            height = theSize.height + 25;
        } else {
            height = theSize.height + 22;
        }
        
        
        //height = 80;
	} else {
		switch (indexPath.row) {
			case 0:  // Tags
				height = [self.listItemTagCollection heightOfRowForTagCollection:refTagCollection];
				break;
			case 1:  // Notes
				// Need to work out a variable/dynamic height for notes
			//	CGSize theSize = [self.listItemNotes sizeWithFont: 
				height = 44;
	
				CGSize theSize = [self.listItemNotes sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0]  
										constrainedToSize:CGSizeMake(kNotesLabelWidth, kMaximumNotesHeight) lineBreakMode:UILineBreakModeWordWrap];
				
				if (theSize.height + 20 > 44) {
					height = theSize.height + 20;
				} 
				
				break;
			case 2: // Due date
				height = 44;
				break;
			default:
				break;
		}
	}
	
	/*switch (indexPath.section) {
		case 0:
			height = 80;
			break;
		case 1:
			height = 150;
			break;
		case 2:
			height = 44;
			break;
		case 3:
			height = 44;
			break;
		default:
			break;
	}*/
	
	return height;
}

/*- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 // Get the selected cell
 UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
 
 // Update the object type in the previous view controller
 // Property detail
 NSArray *propertySection = [propertyCollection.properties objectAtIndex:indexPath.section];
 PropertyDetail *propertyDetail = [propertySection objectAtIndex:indexPath.row];
 
 // Get the correct application setting
 ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:[propertyDetail name]];
 
 // Check to make sure this option is clickable first
 if ([propertyDetail propertyType] == EnumPropertyTypeSettingString && [propertyDetail enabled]) {
 // STRING
 NSStringViewController *controller = [[[NSStringViewController alloc] initWithCurrentValue:cell.detailTextLabel.text 
 andPropertyDetail:propertyDetail 
 andApplicationSetting:appSetting] autorelease];
 [self.navigationController pushViewController:controller animated:YES];
 } else if ([propertyDetail propertyType] == EnumPropertyTypeSettingColor && [propertyDetail enabled]) {
 // COLOR
 UIColorViewController *controller = [[[UIColorViewController alloc] initWithCurrentValue:cell.detailTextLabel.text 
 andPropertyDetail:propertyDetail 
 andApplicationSetting:appSetting] autorelease];
 [self.navigationController pushViewController:controller animated:YES];
 } 
 
 
 [tableView deselectRowAtIndexPath:indexPath animated:YES];
 }*/

/*- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
 
 // Get switch object
 UISwitch *mySwitch = (UISwitch *)[cell.contentView viewWithTag:kSwitchTag];
 
 // If switch is not hidden
 if ([mySwitch isHidden] == FALSE) {
 // Property detail
 NSArray *propertySection = [propertyCollection.properties objectAtIndex:indexPath.section];
 PropertyDetail *propertyDetail = [propertySection objectAtIndex:indexPath.row];
 
 // Get the correct application setting
 ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:[propertyDetail name]];
 
 // Change switch value
 if ([mySwitch isOn]) {
 [mySwitch setOn:NO animated:YES];
 } else {
 [mySwitch setOn:YES animated:YES];
 }
 
 [appSetting updateData:[NSString stringWithFormat:@"%d", [mySwitch isOn]]];
 
 // Update setting
 //[refPage updateProperty:propertyDetail.name 
 //			   withData:[NSString stringWithFormat:@"%d", [mySwitch isOn]]];
 //[refPage updateDatabase];
 
 // Do not select row
 return nil;
 }
 
 return indexPath;
 }*/

#pragma mark -
#pragma mark Updating Height of Section Footers and Headers

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	if (section == 0) {
		return 10.0;
	}
    return 5.0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	if (section == 2) {
		return 10.0;
	}
    return 5.0;
}


- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
}

#pragma mark - PriorityDelegate Methods

- (void)priorityUpdated:(NSInteger)newPriority {
    listItemPriority = newPriority;
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
	if (didAppearFromListView == TRUE) {
		UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
		CustomEdgeInsetsTextView *titleTextView = (CustomEdgeInsetsTextView *)[cell.contentView viewWithTag:kTextViewTag];
		[titleTextView becomeFirstResponder];
		didAppearFromListView = FALSE;
	} else {
		if (popoverController) {
			[popoverController setPopoverContentSize:kPopoverSize animated:YES];
		}
	}
	
	[super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	CustomEdgeInsetsTextView *titleTextView = (CustomEdgeInsetsTextView *)[cell.contentView viewWithTag:kTextViewTag];
	[titleTextView resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated {
    // Register for keyboard notifications
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) 
	//											 name:UIKeyboardDidShowNotification object:self.view.window]; 
	//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) 
	//											 name:UIKeyboardWillHideNotification object:self.view.window]; 

	[myTableView reloadData];
	
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	// Unregister for keyboard notifications while not visible.
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil]; 
	//[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil]; 
	

	[super viewWillDisappear:animated];
}

- (void)keyboardDidShow:(NSNotification *)theNotification {
	/*if (UIInterfaceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
		[myTableView setFrame:kCompressedTableViewFrame];
	} else if (UIInterfaceOrientationIsPortrait([UIDevice currentDevice].orientation)) {
		[myTableView setFrame:kTableViewFrame];
	}*/
}

- (void)keyboardWillHide:(NSNotification *)theNotification {
	//[myTableView setFrame:kTableViewFrame];
}

#pragma mark -
#pragma mark Public Set Methods

- (void)setPopoverController:(UIPopoverController *)thePopoverController {
	popoverController = thePopoverController;
}


#pragma mark -
#pragma mark Helper Methods

- (CustomEdgeInsetsTextView *)getTextView {
	CustomEdgeInsetsTextView *textView = [[[CustomEdgeInsetsTextView alloc] initWithFrame:CGRectZero] autorelease];
	textView.tag = kTextViewTag;
	[textView setBackgroundColor:[UIColor clearColor]];
	
	[textView setDelegate:self];
	[textView setText:@""];
	[textView setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0]];
	[textView setTextColor:[UIColor darkTextColor]];
	[textView setDirectionalLockEnabled:YES];
	[textView setShowsVerticalScrollIndicator:NO];
	[textView setAlwaysBounceVertical:NO];
	
    // 1.6 Updates
    [textView setScrollEnabled:FALSE];
	[textView setContentInset:UIEdgeInsetsZero];
	
	return textView;
}

- (UISwitch *)getSwitch {
	UISwitch *switchView = [[[UISwitch alloc] initWithFrame:CGRectZero] autorelease];
	switchView.tag = kSwitchTag;
	// We dont want user interaction, clicking on table view cell will change this value
	[switchView setFrame:CGRectMake(200, 8, 0, 0)];
	switchView.userInteractionEnabled = FALSE;
	[switchView setHidden:YES];
	return switchView;
}

- (UILabel *)getLabel {
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	[label setBackgroundColor:[UIColor clearColor]];
	[label setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
	[label setTextColor:[UIColor darkTextColor]];
	[label setLineBreakMode:UILineBreakModeWordWrap];
	[label setTag:kLabelTag];
	
	return label;
}

#pragma mark -
#pragma mark TextView Delegates

- (void)textViewDidChange:(CustomEdgeInsetsTextView *)textView {
	if (textView.tag == kTextViewTag) {
		// Title text view
		self.listItemTitle = textView.text;
	} 
    CGSize theSize = [self.listItemTitle sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0] 
                               constrainedToSize:CGSizeMake(kTitleTextViewWidth - 16, 20000) 
                                   lineBreakMode:UILineBreakModeWordWrap];
    
    if (theSize.height == 0) {
        theSize.height = 18;
    }
    
    if (theSize.height + kTextViewHeightBuffer != textView.frame.size.height) {
        [UIView beginAnimations:@"Text Field Resize" context:nil];
        [UIView setAnimationDuration:0.5f];
        
        textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y, 
                                    textView.frame.size.width, 
                                    theSize.height + kTextViewHeightBuffer);
        
        
        [myTableView beginUpdates];
        [myTableView endUpdates];
        //[myTableView reloadData];
        
        [UIView commitAnimations];
        
        //[myTableView reloadData];
    }
    
}

- (void)textViewDidEndEditing:(CustomEdgeInsetsTextView *)textView {
    BOOL didChangeTextView = FALSE;
    
	if ([self.listItemTitle length] == 0 && [textView tag] == kTextViewTag) {
		[textView setText:@"Title"];
        didChangeTextView = TRUE;
		[textView setTextColor:[UIColor grayColor]]; 
	} 
    
    if ([self.listItemTitle isEqualToString:[textView text]] == FALSE && didChangeTextView == FALSE) {
        self.listItemTitle = textView.text;
    }
    
    //else if ([self.listItemNotes length] == 0 && [textView tag] == kTextViewTag + 1) {
	//	[textView setText:@"Notes"];
	//	[textView setTextColor:[UIColor grayColor]]; 
	//}
}

- (void)textViewDidBeginEditing:(CustomEdgeInsetsTextView *)textView {
	/*UIDeviceOrientation newOrientation = [UIDevice currentDevice].orientation;
	if (newOrientation == UIDeviceOrientationUnknown ||
		newOrientation == lastOrientation) {
		return;
	}
	if (!UIDeviceOrientationIsPortrait(newOrientation) &&
		!UIDeviceOrientationIsLandscape(newOrientation)) {
		return;
	}*/

	if ([textView.text isEqualToString:@"Title"] && [textView tag] == kTextViewTag &&
		[self.listItemTitle length] == 0) {
		// Nothing here
		[textView setText:@""];
		[textView setTextColor:[UIColor darkTextColor]];
		
	}// else if ([textView.text isEqualToString:@"Notes"] && [textView tag] == (kTextViewTag + 1) &&
	//		   [self.listItemNotes length] == 0) {
	//	[textView setText:@""];
	//	[textView setTextColor:[UIColor darkTextColor]];
	//}
}

- (BOOL)textView:(CustomEdgeInsetsTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end