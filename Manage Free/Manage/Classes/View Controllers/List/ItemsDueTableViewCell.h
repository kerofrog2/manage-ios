//
//  ItemsDueTableViewCell.h
//  Manage
//
//  Created by Cliff Viegas on 16/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class ListItem;

// GL Drawing
#import "GLDrawES2ViewController.h"

@interface ItemsDueTableViewCell : UITableViewCell {
    ListItem                    *refListItem;
	UILabel                     *listNameLabel;
    UILabel                     *dueDateLabel;
    UIImageView                 *scribbleImageView;
    

}

@property (nonatomic, retain)	UILabel                     *listNameLabel;
@property (nonatomic, retain)   UILabel                     *dueDateLabel;
@property (nonatomic, retain)   UIImageView                 *scribbleImageView;

- (void)loadScribbleImageForListItem:(ListItem *)listItem;

@end
