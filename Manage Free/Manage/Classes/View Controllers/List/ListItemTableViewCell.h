//
//  ToDoTableViewCell.h
//  Manage
//
//  Created by Cliff Viegas on 5/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Constants
#import "ListConstants.h"

// Fast table view cell
#import "FastTableViewCell.h"

// GLDraw
#import "GLDrawES2ViewController.h"

// Tags View and Task Date frames
#define kTagsViewFrame					CGRectMake(380, 11, 245, 20)
#define kShiftedTagsViewFrame			CGRectMake(380, 19, 245, 20)
#define kPreviewTagsViewFrame			CGRectMake(215, 11, 165, 20)
#define kPreviewShiftedTagsViewFrame	CGRectMake(215, 19, 165, 20)

#define kTaskDateFrame					CGRectMake(495, 14, 125, 20)
#define kShiftedTaskDateFrame			CGRectMake(495, 2, 125, 20)
#define kPreviewTaskDateFrame			CGRectMake(250, 14, 125, 20)
#define kPreviewShiftedTaskDateFrame	CGRectMake(250, 2, 125, 20)

// Task Item frames
#define kTaskItemFrame					CGRectMake(40, 13, 450, 21)
#define kSubTaskItemFrame				CGRectMake(63, 13, 427, 21)

#define kPreviewTaskItemFrame			CGRectMake(40, 12, 260, 21)
#define kPreviewSubTaskItemFrame		CGRectMake(55, 12, 260, 21)

// Task completed frames
#define kTaskCompletedFrame				CGRectMake(2, 2, 40, 40)
#define kPreviewTaskCompletedFrame		CGRectMake(2, 1, 40, 40)
#define kSubTaskCompletedFrame			CGRectMake(25, 2, 40, 40)
#define kPreviewSubTaskCompletedFrame	CGRectMake(17, 1, 40, 40)

// Repeating frame
#define kTaskRepeatingFrame				CGRectMake(603, 10, 22, 22)
#define kPreviewTaskRepeatingFrame		CGRectMake(358, 10, 22, 22)

// Data Collections
@class TagCollection;

// Data Models
@class ListItem;

typedef enum {
	EnumHighlightSideLeft = 0,
	EnumHighlightSideRight = 1
} EnumHighlightSide;

@protocol ListItemDelegate
@optional
- (void)taskCompleted:(BOOL)isCompleted forListItemID:(NSInteger)theListItemID;
@end

@interface ListItemTableViewCell : FastTableViewCell {
	id <ListItemDelegate>	delegate;
	
	NSString				*taskItem;
	NSString				*taskDate;
	UIButton				*taskCompleted;
	UIImageView				*scribbleImageView;
	UIImageView				*repeatingTaskImageView;
	
    // Highlighters
    UIView					*highlighterView;
	UIImageView				*rightHighlighterImageView;
	UIImageView				*leftHighlighterImageView;
	
	// Tags View
	UIView					*tagsView;
	UIFont					*tagsFont;
	
	
	
	// Updated properties
	UIImageView				*cellNotesImageView;
	
	// Frames
	CGRect					taskItemFrame;
	CGRect					taskCompletedFrame;
	CGRect					taskDateFrame;
	CGRect					leftHighlighterFrame;
	CGRect					rightHighlighterFrame;
	CGRect					repeatingTaskTagsViewFrame;
	
	// Current cell type properties
	BOOL					isPreview;
	BOOL					isSubtask;
	BOOL					isNonListSort;
	BOOL					hasTags;
	BOOL					isArchived;
    
  //  GLDrawES2ViewController *drawingLayer;
    ListItem                *refListItem;
}

@property (nonatomic, assign)	id				delegate;

@property (nonatomic, retain)	NSString		*taskItem;
@property (nonatomic, retain)	NSString		*taskDate;
@property (nonatomic, retain)	UIButton		*taskCompleted;
@property (nonatomic, retain)	UIImageView		*scribbleImageView;
@property (nonatomic, retain)	UIImageView		*repeatingTaskImageView;
@property (nonatomic, retain)	UIView			*highlighterView;
@property (nonatomic, retain)	UIImageView		*leftHighlighterImageView;
@property (nonatomic, retain)	UIImageView		*rightHighlighterImageView;

@property (nonatomic, retain)	UIView			*tagsView;
@property (nonatomic, retain)	UIFont			*tagsFont;

// Updated properties
@property (nonatomic, retain)	UIImageView		*cellNotesImageView;
@property (nonatomic, assign)	CGRect			taskItemFrame;

@property (nonatomic, assign)	BOOL			isPreview;
@property (nonatomic, assign)	BOOL			isSubtask;
@property (nonatomic, assign)	BOOL			isNonListSort;
@property (nonatomic, assign)	BOOL			hasTags;
@property (nonatomic, assign)	BOOL			isArchived;
//@property (nonatomic, retain) GLDrawES2ViewController   *drawingLayer;

- (id)initWithPreviewStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

- (void)loadDrawingLayerForListItem:(ListItem *)listItem;
- (void)loadGLDrawingLayerForListItem:(ListItem *)listItem;
- (void)showDrawingFrame;

#pragma mark Class Methods
- (void)loadTagsForListItem:(ListItem *)listItem andTagCollection:(TagCollection *)tagCollection;
- (void)setHighlightStyle:(EnumHighlighterColor)highlighter;
- (void)setIndentLevel:(NSInteger)indent;

#pragma mark Scribble Image Methods
- (void)loadScribbleImageForListItem:(ListItem *)listItem andIsPreviewList:(BOOL)isPreviewList;

#pragma mark Cell Notes Icon Methods
- (void)loadCellNotesImage;

#pragma mark Repeating Task Image Methods
- (void)loadRepeatingTaskImage;

#pragma mark Class Helper Methods
- (void)setStandardIndent;
- (void)setIndentForListItem:(ListItem *)listItem;
- (void)setIndentForPreviewListItem:(ListItem *)previewListItem;

@end
