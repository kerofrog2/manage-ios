//
//  ToDoTableViewCell.h
//  Manage
//
//  Created by Cliff Viegas on 5/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Constants
#import "ListConstants.h"

// Data Collections
@class TagCollection;

// Data Models
@class ListItem;

typedef enum {
	EnumHighlightSideLeft = 0,
	EnumHighlightSideRight = 1
} EnumHighlightSide;

@protocol BackupListItemDelegate
- (void)taskCompleted:(BOOL)isCompleted forListItemID:(NSInteger)theListItemID;
@end

@interface BackupListItemTableViewCell : UITableViewCell {
	id <BackupListItemDelegate>	delegate;
	
	UILabel					*taskItem;
	UILabel					*taskDate;
	UIButton				*taskCompleted;
	UIImageView				*scribbleImageView;
	UIView					*highlighterView;
	UIImageView				*rightHighlighterImageView;
	UIImageView				*leftHighlighterImageView;
	
	// Tags View
	UIView					*tagsView;
	UIFont					*tagsFont;
}

@property (nonatomic, assign)	id				delegate;

@property (nonatomic, retain)	UILabel			*taskItem;
@property (nonatomic, retain)	UILabel			*taskDate;
@property (nonatomic, retain)	UIButton		*taskCompleted;
@property (nonatomic, retain)	UIImageView		*scribbleImageView;
@property (nonatomic, retain)	UIView			*highlighterView;
@property (nonatomic, retain)	UIImageView		*leftHighlighterImageView;
@property (nonatomic, retain)	UIImageView		*rightHighlighterImageView;

@property (nonatomic, retain)	UIView			*tagsView;
@property (nonatomic, retain)	UIFont			*tagsFont;

- (id)initWithPreviewStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

#pragma mark Class Methods
- (void)loadTagsForListItem:(ListItem *)listItem andTagCollection:(TagCollection *)tagCollection;
- (void)setHighlightStyle:(EnumHighlighterColor)highlighter;
- (void)setIndentLevel:(NSInteger)indent;

#pragma mark Scribble Image Methods
- (void)loadScribbleImageForListItem:(ListItem *)listItem andIsPreviewList:(BOOL)isPreviewList;

#pragma mark Helper Methods
- (void)setStandardIndent;
- (void)setIndentForListItem:(ListItem *)listItem;
- (void)setIndentForPreviewListItem:(ListItem *)previewListItem;

@end
