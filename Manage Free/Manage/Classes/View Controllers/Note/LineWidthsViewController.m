//
//  LineWidthsViewController.m
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 4/10/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "LineWidthsViewController.h"


@implementation LineWidthsViewController

@synthesize delegate;

#pragma mark - Memory Management

// TODO: Not updating line width setting.

- (void)dealloc {
    [lineWidthsSegmentedControl release];
    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithMaxWidth:(CGFloat)maxWidth {
    self = [super init];
    if (self) {
        [self.view setBackgroundColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0]];
        //[self.view setBackgroundColor:[UIColor colorWithRed:0.08235294 green:0.10980392 blue:0.18431372 alpha:1.0]];
        
        // Set content size for view in popover
        [self setContentSizeForViewInPopover:CGSizeMake(220, 50)];
        
        NSInteger selectedIndex = 0;
        
        if (maxWidth == 2) {
            selectedIndex = 0;
        } else if (maxWidth == 4) {
            selectedIndex = 1;
        } else if (maxWidth == 6) {
            selectedIndex = 2;
        } else if (maxWidth == 8) {
            selectedIndex = 3;
        } else {
            selectedIndex = 0;
        }

        
        [self loadLineWidthsSegmentedControl:selectedIndex];
        
    }
    return self;
}

#pragma mark - Load Methods

- (void)loadLineWidthsSegmentedControl:(NSInteger)selectedIndex {
	lineWidthsSegmentedControl = [[UISegmentedControl alloc] init];
    
    [lineWidthsSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"LineWidthNine.png"] 
                                               atIndex:0 animated:NO];
    [lineWidthsSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"LineWidthSeven.png"] 
                                               atIndex:0 animated:NO];
    [lineWidthsSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"LineWidthFive.png"] 
                                               atIndex:0 animated:NO];
    
    [lineWidthsSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"LineWidthThree.png"] 
                                               atIndex:0 animated:NO];
    
    [lineWidthsSegmentedControl setSelectedSegmentIndex:selectedIndex];
    
    [lineWidthsSegmentedControl setFrame:CGRectMake(10, 8, 200, 37)];
    
    [lineWidthsSegmentedControl addTarget:self action:@selector(lineWidthsSegmentedControlAction) forControlEvents:UIControlEventValueChanged];
    
	[self.view addSubview:lineWidthsSegmentedControl];
    
}
#pragma mark - Button Actions

- (void)lineWidthsSegmentedControlAction {
    if ([self.delegate respondsToSelector:@selector(lineWidthUpdatedWithMinSize:andMaxSize:)]) {
        switch ([lineWidthsSegmentedControl selectedSegmentIndex]) {
            case 0:
                [self.delegate lineWidthUpdatedWithMinSize:1.f andMaxSize:2.f];
                break;
            case 1:
                [self.delegate lineWidthUpdatedWithMinSize:2.5f andMaxSize:4.f];
                break;
            case 2:
                [self.delegate lineWidthUpdatedWithMinSize:3.f andMaxSize:6.f];
                break;
            case 3:
                [self.delegate lineWidthUpdatedWithMinSize:5.f andMaxSize:8.f];
                break;
            default:
                break;
        }
    }
}

@end
