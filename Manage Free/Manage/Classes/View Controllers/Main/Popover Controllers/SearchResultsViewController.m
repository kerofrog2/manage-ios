    //
//  SearchResultsViewController.m
//  Manage
//
//  Created by Cliff Viegas on 19/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "SearchResultsViewController.h"

// Data Models
#import "TaskList.h"
#import "ListItem.h"

@implementation SearchResultsViewController

@synthesize listContent;
@synthesize listItemContent;
@synthesize filteredListContent;
@synthesize filteredListItemContent;

@synthesize savedSearchTerm;
@synthesize savedScopeButtonIndex;
@synthesize searchWasActive;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[listContent release];
	[listItemContent release];
	[self.filteredListContent release];
	[self.filteredListItemContent release];
	
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithStyle:(UITableViewStyle)style {
	if ((self = [super initWithStyle:style])) {
		
	}
	return self;
}

#pragma mark - 
#pragma mark Lifecycle Methods



- (void)viewDidLoad {
	//self.title = @"Products";
	
	// create a filtered list that will contain products for the search results table.
	self.filteredListContent = [NSMutableArray array];//]WithCapacity:[self.listContent count]];
	self.filteredListItemContent = [NSMutableArray array];//WithCapacity:[self.listItemContent count]];
	
	// restore search settings if they were saved in didReceiveMemoryWarning.
	if (self.savedSearchTerm) {
		[self.searchDisplayController setActive:self.searchWasActive];
		[self.searchDisplayController.searchBar setSelectedScopeButtonIndex:self.savedScopeButtonIndex];
		[self.searchDisplayController.searchBar setText:savedSearchTerm];
		
		self.savedSearchTerm = nil;
	}
	
	[self.tableView reloadData];
	self.tableView.scrollEnabled = YES;
}

- (void)viewDidUnload {
	self.filteredListContent = nil;
	self.filteredListItemContent = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
    // save the state of the search UI so that it can be restored if the view is re-created
    self.searchWasActive = [self.searchDisplayController isActive];
    self.savedSearchTerm = [self.searchDisplayController.searchBar text];
    self.savedScopeButtonIndex = [self.searchDisplayController.searchBar selectedScopeButtonIndex];
}



#pragma mark -
#pragma mark UITableView data source and delegate methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	/*
	 If the requesting table view is the search display controller's table view, return the count of
     the filtered list, otherwise return the count of the main list.
	 */
	NSInteger numberOfRows = 0;
	
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		if (section == 0) {
			numberOfRows = [self.filteredListContent count];
		} else if (section == 1) {
			numberOfRows = [self.filteredListItemContent count];
		}
    }
	else {
		if (section == 0) {
			numberOfRows = [self.listContent count];
		} else if (section == 1) {
			numberOfRows = [self.listItemContent count];
		}
        
    }
	
	// Should never reach this point
	return numberOfRows;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *kCellID = @"cellID";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellID] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
	
	/*
	 If the requesting table view is the search display controller's table view, configure the cell using the filtered content, otherwise use the main list.
	 */
	
	
	
/*	if (indexPath.section == 0) {
		TaskList *taskList = [self.filteredListContent objectAtIndex:indexPath.row];
		cell.textLabel.text = taskList.title;
	} else if (indexPath.section == 1) {
		ListItem *listItem = [self.filteredListItemContent objectAtIndex:indexPath.row];
		cell.textLabel.text = listItem.title;
	}*/
	
	
	
	//cell.textLabel.text = product.name;
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   /* UIViewController *detailsViewController = [[UIViewController alloc] init];
    

	 If the requesting table view is the search display controller's table view, configure the next view controller using the filtered content, otherwise use the main list.

Product *product = nil;
	if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        product = [self.filteredListContent objectAtIndex:indexPath.row];
    }
	else
	{
        product = [self.listContent objectAtIndex:indexPath.row];
    }
	detailsViewController.title = product.name;
    
    [[self navigationController] pushViewController:detailsViewController animated:YES];
    [detailsViewController release];*/
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	[self.filteredListItemContent removeAllObjects];
	
	for (TaskList *taskList in self.listContent) {
		if ([scope isEqualToString:@"All"]) {
			NSComparisonResult result = [taskList.title compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
            if (result == NSOrderedSame) {
				[self.filteredListContent addObject:taskList];
            }
		}
	}
	
	for (ListItem *listItem in self.listItemContent) {
		if ([scope isEqualToString:@"All"]) {
			NSComparisonResult result = [listItem.title compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
            if (result == NSOrderedSame) {
				[self.filteredListItemContent addObject:listItem];
            }
		}
	}
}


#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    //[self filterContentForSearchText:searchString scope:
	// [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    //[self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
	// [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}


@end
