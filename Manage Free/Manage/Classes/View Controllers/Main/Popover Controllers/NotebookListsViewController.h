//
//  NotebookListsPopoverController.h
//  Manage
//
//  Created by Cliff Viegas on 27/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TaskListCollection;

// Data Models
@class Notebook;


@interface NotebookListsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    UITableView                     *myTableView;
    Notebook                        *refNotebook;
    TaskListCollection              *refMasterTaskListCollection;
}

#pragma mark Initialisation
- (id)initWithNotebook:(Notebook *)theNotebook andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection;

@end
