//
//  NotebookListsPopoverController.m
//  Manage
//
//  Created by Cliff Viegas on 27/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "NotebookListsViewController.h"

// Method Helper
#import "MethodHelper.h"

// Data Collection
#import "TaskListCollection.h"

// Data Models
#import "Notebook.h"
#import "TaskList.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@interface NotebookListsViewController()
- (void)loadTableView;
@end

@implementation NotebookListsViewController

#pragma mark - Memory Management

- (void)dealloc {
    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithNotebook:(Notebook *)theNotebook andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection {
    self = [super init];
    if (self) {
        // Assign the passed notebook 
        refNotebook = theNotebook;
        
        // Assign the passed master task list collection
        refMasterTaskListCollection = theMasterTaskListCollection;
        
        // Load the table view
        [self loadTableView];
        
        self.title = refNotebook.title;
        
        [self setContentSizeForViewInPopover:kPopoverSize];
    }
    return self;
}

#pragma mark - Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark - TableView Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}
	// Default values
    cell.textLabel.text = @"";
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// Set up the cell...
    TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:indexPath.row];
    
    cell.textLabel.text = taskList.title;
    
    NSInteger theListID = [taskList listID];
    if ([refNotebook containsListWithID:theListID]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }

    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return [refMasterTaskListCollection.lists count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	// Set the notebook to isDirty so it requires update
    refNotebook.isDirty = TRUE;
    
    // Get the id we chose
    TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:indexPath.row];
    NSInteger listID = [taskList listID];
    
    if ([refNotebook containsListWithID:listID]) {
        // Found, get rid of it
        NSInteger notebookListIndex = [refNotebook getIndexOfListWithID:listID];
        if (notebookListIndex != -1) {
            [refNotebook.lists removeObjectAtIndex:notebookListIndex];
        } else {
            [MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Unable to find correct notebook, please report this error to manage@kerofrog.com.au" 
                                  andButtonTitle:@"Ok"];
        }
    } else {
        // Not found, add the list
        [refNotebook.lists addObject:[NSString stringWithFormat:@"%d", listID]];
    }
    
    [myTableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Lists / Notes";
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	return indexPath;
}


#pragma mark - UIViewController Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end
