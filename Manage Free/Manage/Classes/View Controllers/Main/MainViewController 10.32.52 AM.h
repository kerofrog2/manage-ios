//
//  MainViewController.h
//  Manage
//
//  Created by Cliff Viegas on 16/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// For email
#import <MessageUI/MessageUI.h>

// Globals
#import "Globals.h"

// Views
@class HitTestContainerView;
@class ListScrollView;
@class TileScrollView;

// Progress Hud
#import "MBProgressHUD.h"

// View Controllers (for delegates)
#import "EditListItemDueViewController.h"
#import "SettingsViewController.h"
#import "ArchivesViewController.h"
#import "NewDueDateTableViewController.h"
#import "AutoSyncViewController.h"
#import "ListViewController.h"
#import "NoteViewController.h"

// Hidden Toolbar
@class HiddenToolbar;

// Data Models
@class TaskList;
@class Notebook;
@class PreviewTableViewResponder;

// Data Collections
#import "TaskListCollection.h"
@class NotebookCollection;

// Responders (needed for delegate)
#import "ItemsDueTableViewResponder.h"
#import "ListPadTableViewResponder.h"
#import "DrawPadViewController.h"
#import "PngDrawPadViewController.h"

// Shop Front
#import "ShopFrontViewController.h"

// GLDraw
#import "GLDrawES2ViewController.h"

// App Delegate
@class ManageAppDelegate;

typedef enum {
    EnumCurrentSectionLists = 0,
    EnumCurrentSectionNotebooks = 1,
    EnumCurrentSectionFilters = 2,
    EnumCurrentSectionNotebookLists = 10
} EnumCurrentSection;


@interface MainViewController : UIViewController <ListViewDelegate, NoteViewDelegate, PngDrawPadDelegate, DrawPadDelegate, UIAlertViewDelegate, ShopFrontDelegate, MBProgressHUDDelegate, AutoSyncDelegate, ListPadDelegate, NewListItemDueDateDelegate, ArchivesDelegate, UIPopoverControllerDelegate, SettingsDelegate, MFMailComposeViewControllerDelegate, EditListItemDelegate, ItemsDueResponderDelegate, UIScrollViewDelegate, UIActionSheetDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource> {
	// Data Collections
	TaskListCollection		*refMasterTaskListCollection;		// The master task list collection which includes all completed items
	TaskListCollection		*taskListCollection;				// The collection used for displaying content, good for deciding what content needs to be shown
	NotebookCollection      *notebookCollection;                // The collection of notebooks
    ManageAppDelegate       *refManageAppDelegate;              // Pointer to the app delegate
    
    // List View Controller
    ListViewController      *myListViewController;
    
    // Current Drawing Layer
    GLDrawES2ViewController *refCurrentDrawingLayer;
    TaskList                *refCurrentDrawingTaskList;
    
	// Backgrounds & Images
	UIImageView				*canvassImage;
	UIImageView				*borderImage;
	
	// Misc
	UIActivityIndicatorView	*activityIndicatorView;
	UIImageView				*activityIndicatorBackgroundView;
	
	// Tile Scroll View
	TileScrollView			*tileScrollView;
	
	// List Scroll View related items
	ListScrollView			*listScrollView;
	HitTestContainerView	*leftContainerView;
	HitTestContainerView	*rightContainerView;
	NSInteger				currentListIndex;
	BOOL					isAutoScrolling;
	UIImageView				*animatedListImage;
	NSInteger			    selectedListIndex;
	NSInteger				selectedPocketRow;
	BOOL					listButtonClickDisabled;
	BOOL					newListInserted;
	
	// Buttons
	UIBarButtonItem			*newListBarButtonItem;
	UIBarButtonItem			*deleteListBarButtonItem;
	UIBarButtonItem			*searchBarButtonItem;
	UIBarButtonItem			*optionsBarButtonItem;
	UIBarButtonItem			*viewTypeBarButtonItem;
	UIBarButtonItem			*settingsBarButtonItem;
	UIBarButtonItem			*controlPanelBarButtonItem;
	HiddenToolbar			*leftToolbar;
	UIBarButtonItem			*leftNewListBarButtonItem;
	UIBarButtonItem			*leftToolbarBarButtonItem;
    UIBarButtonItem         *helpBarButtonItem;
    UIBarButtonItem         *syncBarButtonItem;
	
    // Progress HUD
    MBProgressHUD           *progressHUD;
    
	// Help menu options
	BOOL					isHelpSelected;
	 
	// Toolbar
	HiddenToolbar			*myToolbar;
	UIButton				*newListButton;
	UIButton				*deleteListButton;
	
	// Action Sheets
	UIActionSheet			*newListActionSheet;
	UIActionSheet			*deleteListActionSheet;
	UIActionSheet			*optionsListActionSheet;
    UIActionSheet           *exportNoteActionSheet;
    UIActionSheet           *newNotebookActionSheet;
    UIActionSheet           *deleteNotebookActionSheet;
    UIActionSheet           *optionsNotebookActionSheet;
	
	// Textfield & Label Items
	UITextField				*titleTextField;
	HiddenToolbar			*titleBackgroundToolbar;
	UIButton				*dateLabelButton;
	UIPopoverController		*dateLabelPopoverController;
	
	// Items due tableview
	UITableView				*itemsDueTableView;					// The items due table view
	UIImageView				*itemsDuePocket;
	ItemsDueTableViewResponder *itemsDueTableViewResponder;
	UIPopoverController		*editListItemPopoverController;
	ListPadTableViewResponder *listPadTableViewResponder;
	
	// Control Panel Objects
	UIImageView				*controlPanelImageView;				// The rounded rectangle control panel image
	UIButton				*archivesButton;					// Opens the archives view
	//UIImageView				*listPadImageView;				// The list paper background image
	UITableView				*listPadTableView;					// The list pad table view that sits on list pad image view
	UIView					*listPadHeaderLine1;				// Header line 1 for the list pad
	UIView					*listPadHeaderLine2;				// Header line 2 for the list pad
	UIView					*listPadFooterLine;					// Footer line for the list pad
	UIPopoverController		*controlPanelPopoverController;		// Popover controller for displaying the control panel popover
	UILabel					*listPadTitleLabel;
	
	//UISearchBar				*searchBar;
	//UITableView				*searchTableView;
	
	// Tag Collection
	TagCollection			*tagCollection;						// The collection of tags
	
	// View Type items
	NSInteger				currentViewType;
	UISegmentedControl		*viewTypeSegmentedControl;
    
    // Popover Controllers
    UIPopoverController		*settingsPopoverController;
    UIPopoverController     *autoSyncPopoverController;
    UIPopoverController     *notebookListsPopoverController;
    UIPopoverController     *shopFrontPopoverController;
    UIPopoverController     *drawPadPopoverController;
	
	// Settings View variables
    BOOL                    autoSyncViewControllerCanClose;
	BOOL					settingsViewControllerCanClose;
	BOOL					firstTimeRun;
	BOOL					archivesHaveBeenUpdated;
	BOOL					archivesVisited;
	BOOL					archiveListsUpdated;
    BOOL                    needToReloadPreviewTableViews;
    BOOL                    commitViewTypeHUDRunning;
    
    // Current Section variables
    NSInteger               currentSection;
    NSInteger               selectedNotebookListID;
    BOOL                    didChangeSection;                      // Just used to fix a display bug where incorrect row was being selected
    
    BOOL                    firstTimeLoad;                         // For checking if this is the first time loading.
    UIImageView             *defaultBackgroundImage;                // The default background image to use during first time loading
    PreviewTableViewResponder *responder;
}

@property   (nonatomic, retain)     UISegmentedControl *viewTypeSegmentedControl;
@property   (nonatomic, assign)     NSInteger   currentViewType;
@property   (nonatomic, assign)     NSInteger   currentSection;
@property   (nonatomic, assign)     NSInteger   selectedNotebookListID;
@property   (nonatomic, retain)     PreviewTableViewResponder * responder;

#pragma mark Initialisation
- (id)initWithMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection andManageAppDelegate:(ManageAppDelegate *)theManageAppDelegate;
- (void)runStartupCode;

#pragma mark Load Methods
- (void)showDrawingFrame;

#pragma mark Button Actions (for external use)
- (void)openListWithListID:(NSInteger)theListID usingFrame:(CGRect)touchedViewFrame;
- (void)listButtonAction:(id)sender;

#pragma mark Send Mail Methods
- (void)sendTextMailForTaskList:(TaskList *)theTaskList;
- (void)sendPDFMailForTaskList:(TaskList *)theTaskList;
- (void)sendPDFMailForNote:(TaskList *)theTaskList;

#pragma mark Notebook Methods
- (void)insertNewNotebook:(Notebook *)newNotebook;
- (void)showNotebookOptionsForNotebookAtIndex:(NSInteger)notebookIndex atRect:(CGRect)displayRect;
- (void)updateNotebookListScrollViews;
- (void)updateNotebooksUsingCurrentTaskListCollection;

@end
