//
//  SearchTableViewResponder.m
//  Manage
//
//  Created by Cliff Viegas on 19/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "SearchTableViewResponder.h"


@implementation SearchTableViewResponder

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}
	
	cell.textLabel.text = @"";
	cell.backgroundColor = [UIColor colorWithRed:0.24313 green:0.22352 blue:0.22352 alpha:1.0];
	cell.textLabel.textColor = [UIColor lightTextColor];
	
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	//[cell.selectedBackgroundView setBackgroundColor:[UIColor greenColor]];
	
	cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
	cell.textLabel.highlightedTextColor = [UIColor darkTextColor];
	
	// Set up the cell...
    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return 20;
}

@end
