//
//  MainViewController.m
//  Manage
//
//  Created by Cliff Viegas on 16/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "MainViewController.h"

// Views
#import "HitTestContainerView.h"
#import "TileScrollView.h"
#import "NotebookTileScrollView.h"

// View Controllers
#import "SearchResultsViewController.h"
#import "ListScrollView.h"
#import "ControlPanelViewController.h"
#import "NotebookListsViewController.h"

// Custom Objects
#import "HiddenToolbar.h"
#import "GLButton.h"

// Data Models
#import "TaskList.h"
#import "ListItem.h"
#import "Notebook.h"
#import "ApplicationSetting.h"

// Data Collections
#import "TaskListCollection.h"
#import "TagCollection.h"
#import "NotebookCollection.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#import "MainConstants.h"
#import "ListConstants.h"

// Responders
#import "SearchTableViewResponder.h"
#import "PreviewTableViewResponder.h"

// Flurry API
#import "FlurryAPI.h"

// App Delegate
#import "ManageAppDelegate.h"

// GL Draw
#import "EAGLView.h"

// In app purchase manager
#import "InAppPurchaseManager.h"

typedef enum {
	EnumViewTypeList = 0,
	EnumViewTypeTile = 1
} EnumViewType;

// Scroll view object constants
#define kScrollObjHeight			487
//#define kScrollObjWidth				320
#define kScrollObjWidth				414
#define kScrollObjBuffer			70

// Tags
#define kScrollViewListButtonTag	7546
#define kScrollViewTableViewTag		9863
#define kShadeViewTag				3585
#define kHelpOverlayTag				6875
#define kWelcomeAlertViewTag        90868

// Constants
const static CGRect kAdViewPortraitFrame = {0, 894, 768, 66};
const static CGRect kAdViewLandscapeFrame = {0, 638, 1024, 66};



// Private Methods
@interface MainViewController()
- (void)loadLeftNavigationBarButtons;
- (void)loadNavigationBarButtons;
- (void)loadToolbar;
- (void)loadItemsDueTableView;
- (void)loadActionSheets;
- (void)loadTileScrollView;
- (void)loadListScrollView;
- (void)updateListScrollList;
- (void)updatePreviewTableViews:(BOOL)forceUpdate;
// Load methods
- (void)addListPreviewToButton:(GLButton *)listButton forListIndex:(NSInteger)listIndex;
- (void)loadTextFields;
- (void)loadControlPanelObjects;
- (void)reloadPreviewTableViews;
// Scrollview Helper methods
- (void)insertNewListButton;
- (void)insertNewNotebookButton;
// ScrollView Methods
- (void)scrollToListAtIndex:(NSInteger)index animated:(BOOL)isAnimated; 
- (void)updateTitleAndDateForSelectedIndex;
- (void)listShaded:(BOOL)status forIndex:(NSInteger)index; 
- (GLButton *)getListItemButtonWithIndex:(NSInteger)index;
- (void)slideListToFillIndex:(NSInteger)index;
// TaskList Methods
- (void)deleteListAtCurrentIndex;
- (void)duplicateTheCurrentTaskList;
- (void)insertNewTaskList:(TaskList *)newMasterList;
- (void)deleteListAtIndex:(NSInteger)index;
// Helper Methods
- (void)updateLocalData;
- (void)commitViewType;
- (void)reloadTheme;
- (void)showActivityIndicator;
- (void)hideActivityIndicator;
// View Controller methods
- (void)checkForToodledoAutoSync;
// Delegate methods
- (void)taskCompleted:(BOOL)isCompleted forListItemID:(NSInteger)theListItemID;
@end


@implementation MainViewController

@synthesize currentSection;
@synthesize selectedNotebookListID;
@synthesize viewTypeSegmentedControl;
@synthesize currentViewType;


#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    [_adView release];
    
//    [responder release];
	// Backgrounds & Images
	[borderImage release];
	[canvassImage release];
    
	// ScrollView related
	[listScrollView release];
	[leftContainerView release];
	[rightContainerView release];
    
	// Buttons
	[newListBarButtonItem release];
	[deleteListBarButtonItem release];
	[searchBarButtonItem release];
	[optionsBarButtonItem release];
	[settingsBarButtonItem release];
	[controlPanelBarButtonItem release];
	[leftToolbar release];
	[leftNewListBarButtonItem release];
	[leftToolbarBarButtonItem release];
    [helpBarButtonItem release];
    [syncBarButtonItem release];
    
	// Misc
	[activityIndicatorBackgroundView release];
	[activityIndicatorView release];
    
	// Toolbar
	[myToolbar release];
    
	// Data Collections
	[taskListCollection release];
    
	// Action Sheets
	[newListActionSheet release];
	[deleteListActionSheet release];
	[optionsListActionSheet release];
    [exportNoteActionSheet release];
    [newNotebookActionSheet release];
    [deleteNotebookActionSheet release];
    [optionsNotebookActionSheet release];
    
	// Textfield & label items
	[titleTextField release];
	[titleBackgroundToolbar release];
	[dateLabelButton release];
	[dateLabelPopoverController release];
    
	// Items due table view
	[itemsDueTableView release];
	[itemsDuePocket release];
	[itemsDueTableViewResponder release];
    
	// Control panel objects
	[controlPanelImageView release];
	[archivesButton release];
	//[listPadImageView release];
	[listPadTableView release];
	[listPadTableViewResponder release];
	[listPadHeaderLine1 release];
	[listPadHeaderLine2 release];
	[listPadFooterLine release];
    if (controlPanelPopoverController != nil) {
        controlPanelPopoverController = nil;
        [controlPanelPopoverController release];
    }
	
	[listPadTitleLabel release];
	//[searchTableView release];
    
	// View Type objects
	[viewTypeSegmentedControl release];
	
    // Popover Controllers
    if (editListItemPopoverController != nil) {
        editListItemPopoverController = nil;
        [editListItemPopoverController release];
    }
    
    if (drawPadPopoverController != nil) {
        drawPadPopoverController = nil;
        [drawPadPopoverController release];
    }

    if (settingsPopoverController != nil) {
        settingsPopoverController = nil;
        [settingsPopoverController release];
    }
    
    // Data Collections
    [notebookCollection release];
    
	// Release tag collection
	[tagCollection release];
    
    // Remove our observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection andManageAppDelegate:(ManageAppDelegate *)theManageAppDelegate {
	if ((self = [super init])) {
        // Need to reload the preview table views on startup
        needToReloadPreviewTableViews = TRUE;
        
        // Point to manage app delegate
        refManageAppDelegate = theManageAppDelegate;
        
		// Create the list from the database
		refMasterTaskListCollection = theMasterTaskListCollection;
        
        // Set first time load to true
        firstTimeLoad = TRUE;
        
        // Set first time run to true on init
        firstTimeRun = TRUE;
        
        
        commitViewTypeHUDRunning = FALSE;
	}
	return self;
	
}

- (void)loadView {
    [super loadView];
    
    // Load default background image
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        defaultBackgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-Portrait.png"]];
        [defaultBackgroundImage setFrame:kDefaultBackgroundImagePortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        defaultBackgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-Landscape.png"]];
        [defaultBackgroundImage setFrame:kDefaultBackgroundImageLandscape];
    }
    
    [self.view addSubview:defaultBackgroundImage];
    
    //agltb3B1Yi1pbmNyDQsSBFNpdGUY-tDVFAw
    _adView = [[MPAdView alloc] initWithAdUnitId:kMoPubAdUnitID size:MOPUB_LEADERBOARD_SIZE];
    
    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchaseRemoveAdvertisingID]) {
        // We don't want advertising
        [_adView setAdUnitId:@"DUMMY ID"];
    } else {
        _adView.delegate = self;
        
        CGRect frame = _adView.frame;
        CGSize size = [_adView adContentViewSize];
        frame.origin.y = self.view.bounds.size.height - size.height;
        frame.origin.x = (self.view.bounds.size.width - size.width) / 2;	
        _adView.frame = frame;
        
        
        [self.view addSubview:_adView];
        [self.view bringSubviewToFront:_adView];
        
        [_adView loadAd];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
}

- (void)runStartupCode {
    // Reload all the lists
    [refMasterTaskListCollection reloadAllLists];
    
    // Do an archive check
    [refManageAppDelegate archiveCheck];
    
    // Create the normal task collection from the master
    taskListCollection = [[TaskListCollection alloc] init];
    [taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
    
    // Init the notebook collection
    notebookCollection = [[NotebookCollection alloc] initWithAllNotebooks];
    
    NSString *canvasImageName = [refMasterTaskListCollection getImageNameForSection:@"Canvas"];
    canvassImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:canvasImageName]];
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        //canvassImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Canvass.png" ofType:nil]]];
        //canvassImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Canvass.png" ofType:nil]]];
        [canvassImage setFrame:kCanvassImagePortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        //canvassImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Canvass.png" ofType:nil]]];
        [canvassImage setFrame:kCanvassImageLandscape];
    }
    [self.view addSubview:canvassImage];
    
    // Init the tag collection
    tagCollection = [[TagCollection alloc] initWithAllTags];
    
    // Create an observer for whenever application becomes active
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // Init the current list index to 0
    currentListIndex = 0;			  
    
    // Init selected pocket row to 0
    selectedPocketRow = 0;
    
    // Init new list inserted
    newListInserted = FALSE;
    
    // Init archives have been updated to false
    archivesHaveBeenUpdated = FALSE;
    archivesVisited = FALSE;
    archiveListsUpdated = FALSE;
    
    // Init settings and autosync view controllers can close
    settingsViewControllerCanClose = TRUE;
    autoSyncViewControllerCanClose = TRUE;
    
    // Init did change section to FALSE
    didChangeSection = FALSE;
    
    // Init current section to lists
    self.currentSection = EnumCurrentSectionLists;
    
    // Init selected notebook list index to -1
    self.selectedNotebookListID = -1;
    
    // Init the activity indicator background view
    activityIndicatorBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LoadingBackground.png"]];
    [activityIndicatorBackgroundView setFrame:kActivityViewBackgroundPortrait];
    [activityIndicatorBackgroundView setHidden:YES];
    [self.view addSubview:activityIndicatorBackgroundView];
    
    // Init the container views
    leftContainerView = [[HitTestContainerView alloc] init]; // WithFrame:CGRectMake(0, 200, 184, 480)];
    rightContainerView = [[HitTestContainerView alloc] init]; //   WithFrame:CGRectMake(584, 200, 184, 480)];
    
    // Position the container views
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        [leftContainerView setFrame:kLeftContainerViewPortrait];
        [rightContainerView setFrame:kRightContainerViewPortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        [leftContainerView setFrame:kLeftContainerViewLandscape];
        [rightContainerView setFrame:kRightContainerViewLandscape];
    }					
    
    [self.view addSubview:leftContainerView];
    [self.view addSubview:rightContainerView];							   
    
    // Load the left navigation bar buttons
    [self loadLeftNavigationBarButtons];
    
    // Load the navigation bar buttons
    [self loadNavigationBarButtons];
    
    // Load the action sheets
    [self loadActionSheets];
    
    // Init current view type to list
    self.currentViewType = EnumViewTypeList;
    
    // Load the list scroll view
    [self loadListScrollView];
    
    // Load the tile scroll view
    [self loadTileScrollView];
    
    // Commit current view type
    [listScrollView setHidden:NO];
    
    // Load the frame/border
    NSString *borderImageName = [refMasterTaskListCollection getImageNameForSection:@"Border"];
    borderImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:borderImageName]];
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        //borderImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Border.png" ofType:nil]]];
        
        [borderImage setFrame:kBorderImagePortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        //borderImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Border.png" ofType:nil]]];
        [borderImage setFrame:kBorderImageLandscape];
    }
    [self.view addSubview:borderImage];
    
    // Load the items due table view
    [self loadItemsDueTableView];
    
    // Load control panel objects
    [self loadControlPanelObjects];
    
    // Load the buttons
    [self loadToolbar];
    
    // Load the text fields
    [self loadTextFields];
    
    // Set the title
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
            if ([taskListCollection.lists count] == 0) {
                self.title = @"My Lists & Notes";
            } else {
                self.title = [NSString stringWithFormat:@"My Lists & Notes (1 of %d)", [taskListCollection.lists count]];
            }
            break;
        case EnumCurrentSectionNotebooks:
            if ([notebookCollection.notebooks count] == 0) {
                self.title = @"My Folders";
            } else {
                self.title = [NSString stringWithFormat:@"My Folders (1 of %d)", [notebookCollection.notebooks count]];
            }
            break;
        default:
            break;
    }
    
    
    
    if ([refMasterTaskListCollection.defaultView isEqualToString:@"Thumbnail"]) {
        [viewTypeSegmentedControl setSelectedSegmentIndex:1];
        
        self.currentViewType = viewTypeSegmentedControl.selectedSegmentIndex;
        
        [self commitViewType];
    }

    // Remove the default background image
    [defaultBackgroundImage removeFromSuperview];
    [defaultBackgroundImage release];
    
    [self reloadTheme];
    
    firstTimeLoad = FALSE;
    
   /* UIAlertView *welcomeAlertView = [[UIAlertView alloc] initWithTitle:@"Welcome to Manage" 
                                                               message:@"Your lists, notes and folders have now finished loading." 
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"Ok", nil];
    welcomeAlertView.tag = kWelcomeAlertViewTag;
 
    [welcomeAlertView show];
    [welcomeAlertView release];*/
}

#pragma mark -
#pragma mark Load Methods

- (void)loadLeftNavigationBarButtons {
	leftToolbar = [[HiddenToolbar alloc] initWithFrame:CGRectMake(0, 0, 110, 44)]; 
	//[toolbar setBarStyle: UIBarStyleBlack];
	[leftToolbar setTintColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.0]];
	leftToolbar.opaque = NO;
	leftToolbar.backgroundColor = [UIColor clearColor];
	leftToolbar.translucent = YES;
	
	// Array to hold the buttons in toolbar
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] init];
	
	// Create the archives button and show it
	controlPanelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Panel" style:UIBarButtonItemStyleBordered 
															target:self action:@selector(controlPanelBarButtonItemAction)];
	[toolbarButtons addObject:controlPanelBarButtonItem];
	
	// Add first spacer
	UIBarButtonItem *spacer1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace 
																			 target:nil action:nil];
	[toolbarButtons addObject:spacer1];
	[spacer1 release];
	
	// Create the left bar button item on the navigation bar
	leftNewListBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd 
																					   target:self action:@selector(newListLeftBarButtonAction:)];
	[leftNewListBarButtonItem setStyle:UIBarButtonItemStyleBordered];
	[toolbarButtons addObject:leftNewListBarButtonItem];
    
	// Place the buttons in the toolbar
	[leftToolbar setItems:toolbarButtons animated:NO];
	
	[toolbarButtons release];
	
	// Put the toolbar in the nav bar
	leftToolbarBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftToolbar];
	[self.navigationItem setLeftBarButtonItem:leftToolbarBarButtonItem animated:NO];
	[leftToolbar release];
}

// Load the navigation bar buttons
- (void)loadNavigationBarButtons {
	// Add a toolbar instead of a help button to the right bar button
	//HiddenToolbar *toolbar = [[HiddenToolbar alloc] initWithFrame:CGRectMake(10, -2, 180, 44.01)];
	HiddenToolbar *toolbar = [[HiddenToolbar alloc] initWithFrame:CGRectMake(10, 0, 180, 44)]; 
	//[toolbar setBarStyle: UIBarStyleBlack];
	[toolbar setTintColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.0]];
	toolbar.opaque = NO;
	toolbar.backgroundColor = [UIColor clearColor];
	toolbar.translucent = YES;
	
	// Array to hold the buttons in toolbar
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] init];
	
	// Add the segmented control
	UISegmentedControl * viewTypeSegmentedControlTemp = [[UISegmentedControl alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
	viewTypeSegmentedControl = viewTypeSegmentedControlTemp;
	[viewTypeSegmentedControl setSegmentedControlStyle:UISegmentedControlStyleBar];
	[viewTypeSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"TileView.png"] atIndex:0 animated:NO];
	[viewTypeSegmentedControl insertSegmentWithImage:[UIImage imageNamed:@"ListView.png"] atIndex:0 animated:NO];
	[viewTypeSegmentedControl setSelectedSegmentIndex:0];
	[viewTypeSegmentedControl setContentOffset:CGSizeMake(0, -1) forSegmentAtIndex:0];
	[viewTypeSegmentedControl setContentOffset:CGSizeMake(0, -1) forSegmentAtIndex:1];
	[viewTypeSegmentedControl setTintColor:[UIColor darkGrayColor]];
	[viewTypeSegmentedControl addTarget:self action:@selector(viewTypeSegmentedControlAction) forControlEvents:UIControlEventValueChanged];
	UIBarButtonItem *segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:viewTypeSegmentedControl];
	[toolbarButtons addObject:segmentBarItem];
	[segmentBarItem release];
    [viewTypeSegmentedControlTemp release];
	
	// Add first spacer
	UIBarButtonItem *spacer1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace 
																			 target:nil action:nil];
	[toolbarButtons addObject:spacer1];
	[spacer1 release];
	
	// Create a settings button
	settingsBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SettingsIcon.png"] 
																			  style:UIBarButtonItemStylePlain
																			 target:self 
																			 action:@selector(settingsBarButtonItemAction)];

	//[settingsBarButtonItem setStyle:UIBarButtonItemStylePlain];
	[toolbarButtons addObject:settingsBarButtonItem];
	[settingsBarButtonItem release];
	
	
	// Add second spacer
	UIBarButtonItem *spacer2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace 
																			 target:nil action:nil];
	[toolbarButtons addObject:spacer2];
	[spacer2 release];
	
	// Add a help bar button item
	helpBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"?" 
                                                         style:UIBarButtonItemStylePlain 
														target:self action:@selector(helpBarButtonItemAction)];
	//[helpBarButtonItem setStyle:UIBarButtonItemStylePlain];
	[toolbarButtons addObject:helpBarButtonItem];
	
    // Add a sync bar button item
	
	// Place the buttons in the toolbar
	[toolbar setItems:toolbarButtons animated:NO];
	
	[toolbarButtons release];
	
	// Put the toolbar in the nav bar
	UIBarButtonItem *customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:toolbar];
	[self.navigationItem setRightBarButtonItem:customBarButtonItem animated:NO];
	[toolbar release];
	[customBarButtonItem release];
	
	isHelpSelected = FALSE;
}

// Load the hidden toolbar which displays the + and delete buttons
- (void)loadToolbar {
	// Create the hidden toolbar
	myToolbar = [[HiddenToolbar alloc] init]; //WithFrame:kHiddenToolbarPortrait];
	myToolbar.opaque = NO;
	myToolbar.backgroundColor = [UIColor clearColor];
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[myToolbar setFrame:kHiddenToolbarPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[myToolbar setFrame:kHiddenToolbarLandscape];
	}					
	
	// Create an array to hold the buttons on the toolbar
	NSMutableArray *toolbarButtons = [[NSMutableArray alloc] init];
	
	// Seperator
	UIBarButtonItem *seperator0 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace 
																			   target:nil action:nil];
	seperator0.width = 5.0;
	[toolbarButtons addObject:seperator0];
	[seperator0 release];
	
	newListBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd 
																		 target:self 
																		 action:@selector(newListBarButtonItemAction:)];
	[toolbarButtons addObject:newListBarButtonItem];
	
	
	
	// Seperator
	UIBarButtonItem *seperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace 
																			   target:nil action:nil];
	seperator.width = 20.0;
	[toolbarButtons addObject:seperator];
	[seperator release];
	
	deleteListBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash 
																			target:self 
																			action:@selector(deleteListBarButtonItemAction:)];
	[toolbarButtons addObject:deleteListBarButtonItem];
	
	// Seperator
	UIBarButtonItem *seperator2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace 
																			   target:nil action:nil];
	seperator2.width = 15.0;
	[toolbarButtons addObject:seperator2];
	[seperator2 release];
	
	//emailBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose 
	//																   target:self 
	//																   action:@selector(emailBarButtonItemAction:)];
    UIImage *optionsButtonImage = [UIImage imageNamed:@"ExportListIcon.png"];
    if ([self currentSection] == EnumCurrentSectionNotebooks) {
        optionsButtonImage = [UIImage imageNamed:@"OptionsIcon.png"];
    } 
    
	optionsBarButtonItem = [[UIBarButtonItem alloc] initWithImage:optionsButtonImage
														  style:UIBarButtonItemStylePlain 
														 target:self action:@selector(optionsBarButtonItemAction:)];
	//[emailBarButtonItem set
	[toolbarButtons addObject:optionsBarButtonItem];
	
	// Add the buttons to the toolbar
	[myToolbar setItems:toolbarButtons];
	[toolbarButtons release];
	
	
	[self.view addSubview:myToolbar];
}

// Load the items due tableView
- (void)loadItemsDueTableView {
	// Load the items due tableview responder
    //nmthuong
    if (itemsDueTableViewResponder == nil) {
        itemsDueTableViewResponder = [[ItemsDueTableViewResponder alloc] initWithDelegate:self
                                                                       andParentListTitle:nil
                                                                          andParentListID:-1
                                                                    andTaskListCollection:refMasterTaskListCollection];
    }
	
	// Load the items due pocket image
	NSString *pocketImageName = [refMasterTaskListCollection getImageNameForSection:@"Pocket"];
	itemsDuePocket = [[UIImageView alloc] initWithImage:[UIImage imageNamed:pocketImageName]];
	[itemsDuePocket setFrame:CGRectMake(50, 792, 241, 149)];
						
	// Load the items due tableview
	itemsDueTableView = [[UITableView alloc] initWithFrame:CGRectMake(66, 806, 209, 121) style:UITableViewStylePlain];
	[itemsDueTableView setSeparatorColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.3]];
	[itemsDueTableView setRowHeight:40.0f];
	[itemsDueTableView setBackgroundColor:[UIColor clearColor]];
	[itemsDueTableView setDataSource:itemsDueTableViewResponder];
	[itemsDueTableView setDelegate:itemsDueTableViewResponder];
	
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[itemsDuePocket setFrame:kMainItemsDuePocketPortrait];
		[itemsDueTableView setFrame:kMainItemsDueTableViewPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[itemsDuePocket setFrame:kMainItemsDuePocketLandscape];
		[itemsDueTableView setFrame:kMainItemsDueTableViewLandscape];
	}
	
	[self.view addSubview:itemsDuePocket];
	[self.view addSubview:itemsDueTableView];
	
	// Hide pocket reminder if needed
	if (refMasterTaskListCollection.pocketReminder == FALSE) {
		[itemsDuePocket setHidden:YES];
		[itemsDueTableView setHidden:YES];
	} else if (refMasterTaskListCollection.pocketReminder == TRUE) {
		[itemsDuePocket setHidden:NO];
		[itemsDueTableView setHidden:NO];
	}
}

// Load and init the action sheets
- (void)loadActionSheets {
	newListActionSheet = [[UIActionSheet alloc] initWithTitle:nil
													 delegate:self 
											cancelButtonTitle:nil 
									   destructiveButtonTitle:nil 
											otherButtonTitles:@"New List", @"New Note", @"Duplicate List/Note", nil];
	
	deleteListActionSheet = [[UIActionSheet alloc] initWithTitle:nil
														delegate:self 
											   cancelButtonTitle:nil 
										  destructiveButtonTitle:@"Delete List/Note" 
											   otherButtonTitles:nil];
	
	optionsListActionSheet = [[UIActionSheet alloc] initWithTitle:nil 
													   delegate:self 
											  cancelButtonTitle:nil 
										 destructiveButtonTitle:nil 
											  otherButtonTitles:@"Email Text List", @"Email PDF List", nil];
    
    exportNoteActionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                         delegate:self 
                                                cancelButtonTitle:nil 
                                           destructiveButtonTitle:nil 
                                                otherButtonTitles:@"Email PDF", nil];
    
    newNotebookActionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                         delegate:self 
                                                cancelButtonTitle:nil 
                                           destructiveButtonTitle:nil 
                                                otherButtonTitles:@"New Folder", nil];
    
    deleteNotebookActionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                            delegate:self 
                                                   cancelButtonTitle:nil 
                                              destructiveButtonTitle:@"Delete Folder" 
                                                   otherButtonTitles:nil];
 
    optionsNotebookActionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                             delegate:self 
                                                    cancelButtonTitle:nil 
                                               destructiveButtonTitle:nil 
                                                    otherButtonTitles:@"Modify Lists", nil];
}

- (void)loadTileScrollView {
	// Init the tile scroll view
	tileScrollView = [[TileScrollView alloc] initWithTaskListCollection:taskListCollection 
                                                  andMainViewController:self 
                                            andMasterTaskListCollection:refMasterTaskListCollection
                                                  andNotebookCollection:notebookCollection];
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[tileScrollView setFrame:kTileScrollViewPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[tileScrollView setFrame:kTileScrollViewLandscape];
	}
	
	[tileScrollView updateScrollList];
	
	// Set tile scroll view hidden initially
	[tileScrollView setHidden:YES];
	
	[self.view addSubview:tileScrollView];
}

// Load the scroll view objects
- (void)loadListScrollView {
	// Init autoscrolling to false
	isAutoScrolling = FALSE;
	
	// Enable list button click by default (used to prevent double click when expanding)
	listButtonClickDisabled = FALSE;
	
	// Init selected list index to 0 (as a "just in case")
	selectedListIndex = 0;
	
	// Init the scroll view
	listScrollView = [[ListScrollView alloc] init];
    
	//myScrollView.indicatorStyle = UIScrollViewIndicatorStyleDefault;
	[listScrollView setShowsHorizontalScrollIndicator:NO];
	//[myScrollView setFrame:CGRectMake(0, 300, 768, 480)];
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[listScrollView setFrame:kMainScrollViewPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[listScrollView setFrame:kMainScrollViewLandscape];
	}	
	
	listScrollView.pagingEnabled = YES;
	listScrollView.clipsToBounds = NO;
	listScrollView.delaysContentTouches = YES;

	[listScrollView setScrollIndicatorInsets:UIEdgeInsetsMake(0, 35, 0, 35)];
	
	listScrollView.delegate = self;

	
	[self updateListScrollList];

    // Item count depends on the current section selected
    NSInteger itemCount = 0;
    switch (self.currentSection) {
        case EnumCurrentSectionLists:
        case EnumCurrentSectionNotebookLists:
            itemCount = [taskListCollection.lists count];
            break;
        case EnumCurrentSectionNotebooks:
            itemCount = [notebookCollection.notebooks count];
            break;
        default:
            itemCount = [taskListCollection.lists count];
            break;
    }
    
	[listScrollView setContentSize:CGSizeMake((itemCount * (kScrollObjWidth + kScrollObjBuffer)), [listScrollView bounds].size.height)];
	
	[leftContainerView setParentView:listScrollView];
	[rightContainerView setParentView:listScrollView];
	
	// Set it hidden after loading
	[listScrollView setHidden:YES];
	
	[self.view addSubview:listScrollView];
	
	[self listShaded:FALSE forIndex:0];
}

- (void)updateListScrollList {
	// Remove all subviews in contentview
	//for (UIView *aView in contentView.subviews) {
	//	[aView removeFromSuperview];
	//}
	
	// Remove all button subviews in scroll view
	for (UIView *aView in listScrollView.subviews) {
		if ([aView isKindOfClass:[GLButton class]]) {
			[aView removeFromSuperview];
		}
	}
	
    
    // Item count depends on the current section selected
    NSInteger itemCount = 0;
    switch (self.currentSection) {
        case EnumCurrentSectionLists:
        case EnumCurrentSectionNotebookLists:
            itemCount = [taskListCollection.lists count];
            break;
        case EnumCurrentSectionNotebooks:
            itemCount = [notebookCollection.notebooks count];
            break;
        default:
            itemCount = [taskListCollection.lists count];
            break;
    }
    
    UIImage *image;
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
        case EnumCurrentSectionNotebookLists:
            // Update options bar button item
            image = [UIImage imageNamed:@"PaperPreview.png"];
            [optionsBarButtonItem setImage:[UIImage imageNamed:@"ExportListIcon.png"]];
            break;
        case EnumCurrentSectionNotebooks:
            image = [UIImage imageNamed:@"NotebookOpen.png"];
            [optionsBarButtonItem setImage:[UIImage imageNamed:@"OptionsTileListButton.png"]];
            break;
        default:
            image = [UIImage imageNamed:@"PaperPreview.png"];
            break;
    }
    
    
    for (NSInteger index = 0; index < itemCount; index++) {
        GLButton *itemButton = [GLButton buttonWithType:UIButtonTypeCustom];
        
		[itemButton setBackgroundImage:image forState:UIControlStateNormal];
		[itemButton addTarget:self action:@selector(listButtonAction:) forControlEvents:UIControlEventTouchUpInside];
		
		[itemButton setOpaque:YES];
		
		[itemButton setHighlighted:NO];
		[itemButton setAdjustsImageWhenHighlighted:NO];
		
		CGRect rect = itemButton.frame;
		rect.size.height = kScrollObjHeight;
		rect.size.width = kScrollObjWidth;
		itemButton.frame = rect;
		
        if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
            TaskList *taskList = [taskListCollection.lists objectAtIndex:index];
            itemButton.tag = taskList.listID;
        } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
            Notebook *notebook = [notebookCollection.notebooks objectAtIndex:index];
            itemButton.tag = notebook.notebookID;
        }

		// Need to add subviews to list button
        if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
            [self addListPreviewToButton:itemButton forListIndex:index];
        } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
            Notebook *notebook = [notebookCollection.notebooks objectAtIndex:index];
            
            CGRect theNotebookImageFrame = CGRectZero;
            if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
                theNotebookImageFrame = kNotebookImageFramePortrait;
            } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
                theNotebookImageFrame = kNotebookImageFrameLandscape;
            }
            
            // Frame depends on portrait or landscape
            NotebookTileScrollView *notebookTileScrollView = [[NotebookTileScrollView alloc] initWithTaskListCollection:taskListCollection 
                                                                                                  andMainViewController:self 
                                                                                            andMasterTaskListCollection:refMasterTaskListCollection 
                                                                                                            andNotebook:notebook
                                                                                                  andNotebookImageFrame:theNotebookImageFrame];
            
            [itemButton addSubview:notebookTileScrollView];
            [notebookTileScrollView release];
        }
        
        // Otherwise need to add the tilenotebookscrollview to the button
		
		
		// Add shadeView
		UIView *shadeView = [[UIView alloc] init];
        if ([self currentSection] == EnumCurrentSectionLists) {
            [shadeView setFrame:kListShadeViewFrame];
        } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
            [shadeView setFrame:kNotebookShadeViewFrame];
        }                   

		[shadeView setBackgroundColor:[UIColor blackColor]];
		[shadeView setAlpha:0.3f];
		//[shadeView setHidden:YES];
		[shadeView setTag:kShadeViewTag];
		[itemButton addSubview:shadeView];
		[shadeView release];
		
		[listScrollView addSubview:itemButton];
    }
	
	UIImageView *view = nil;
	//NSArray *subviews = [contentView subviews];
	NSArray *subviews = [listScrollView subviews];
	
	// reposition all image subviews in a horizontal serial fashion
	CGFloat curXLoc = 35;
	
	NSInteger i = 0;
	for (view in subviews) {
		if ([view isKindOfClass:[GLButton class]]) {
			i++;
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			//curXLoc += (kScrollObjWidth + kScrollBuffer);
			curXLoc += kScrollObjWidth + kScrollObjBuffer;
		}
	}
    
	[listScrollView setContentSize:CGSizeMake((itemCount * (kScrollObjWidth + kScrollObjBuffer)), [listScrollView bounds].size.height)];
	
	NSInteger scrollIndex = currentListIndex;
	if (newListInserted == TRUE) {
		scrollIndex++;
		newListInserted = FALSE;
	}
	
	CGFloat newXLoc = (kScrollObjWidth + kScrollObjBuffer) * scrollIndex;
	[listScrollView setContentOffset:CGPointMake(newXLoc, 0)];
	
	// Stop animating
	if ([activityIndicatorView isAnimating]) {
		[activityIndicatorBackgroundView setHidden:YES];
		[activityIndicatorView stopAnimating];
		[activityIndicatorView removeFromSuperview];
	}
    
	
	//[self listShaded:NO forIndex:currentListIndex];
	
}

// A much more lightweight 'updateScrollList' method, as it just updates the preview tableviews
- (void)updatePreviewTableViews:(BOOL)forceUpdate {
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        for (NSInteger i = 0; i < [taskListCollection.lists count]; i++) {
            TaskList *taskList = [taskListCollection.lists objectAtIndex:i];
            
            if (taskList.mainViewRequiresUpdate == TRUE || forceUpdate == TRUE) {
                // Get the correct list button
                GLButton *listItemButton = [self getListItemButtonWithIndex:i];
                
                // Remove tableview from its subviews
                for (UIView *subview in listItemButton.subviews) {
                    if ([subview isKindOfClass:[UITableView class]]) {
                        [subview removeFromSuperview];
                    }
                }
                
                // Add a new list preview tableview
                [self addListPreviewToButton:listItemButton forListIndex:i];
                
                // Bring the shade view to the front
                UIView *shadeView = [listItemButton viewWithTag:kShadeViewTag];
                [listItemButton bringSubviewToFront:shadeView];
                
                taskList.mainViewRequiresUpdate = FALSE;
            }
        }
    }
	
}

- (void)reloadPreviewTableViews {
    
    // set data
    
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        
        for (NSInteger i = 0; i < [taskListCollection.lists count]; i++) {
            
            
            NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
            
            if (isShowCompletedTasks == 0) {
                
                TaskList *mainTaskList = [refMasterTaskListCollection.lists objectAtIndex:i];
                TaskList *editTaskList = [taskListCollection.lists objectAtIndex:i];
                
                for (ListItem *mainItem in mainTaskList.listItems) {
                    for (ListItem *editItem in editTaskList.listItems) {
                        if (mainItem.listItemID == editItem.listItemID) {
                            if (editItem.hasJustComplete == YES) {
                                mainItem.completed = editItem.hasJustComplete;
                            }
                        }
                    }
                }
            }

            // Get the correct list button
            GLButton *listItemButton = [self getListItemButtonWithIndex:i];
            
            // Remove tableview from its subviews
            for (UIView *subview in listItemButton.subviews) {
                if ([subview isKindOfClass:[UITableView class]]) {
                    UITableView *theTableView = (UITableView *)subview;
                    [theTableView reloadData];
                }
            }

        }
    }
}

// Adds the list preview tableview to the specified button
- (void)addListPreviewToButton:(GLButton *)listButton forListIndex:(NSInteger)listIndex {
	/*CGRect previewTableViewFrame = CGRectZero;
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		previewTableViewFrame = kPreviewTableViewPortrait;
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		previewTableViewFrame = kPreviewTableViewLandscape;
	}*/
    
    //TaskList *checkTaskList = [[TaskList alloc] initNewList];
    
    TaskList *checkTaskList = [refMasterTaskListCollection.lists objectAtIndex:listIndex];
    
    if (checkTaskList.isNote == TRUE) {
        // Each button needs its own permanent drawing layer... if it is a 'note button'.  This way it can be updated easily.
       /* if (listButton.drawingLayer != nil) {
            for (UIView *subview in listButton.subviews) {
                if ([subview isKindOfClass:[EAGLView class]]) {
                    [subview removeFromSuperview];
                }
            }
            [listButton.drawingLayer release];
            listButton.drawingLayer = nil;
        }*/
        


        
        // Get the image
        
        
        NSString *scribblePath;
        UIImage *myImage;
        
        BOOL foundImage = FALSE;
        
        NSRange glsRange = [checkTaskList.scribble rangeOfString:@".gls"];
        
        if (glsRange.location != NSNotFound) { 
            
            NSString *pngScribble = [checkTaskList.scribble substringToIndex:glsRange.location];
            pngScribble = [NSString stringWithFormat:@"%@.png", pngScribble];
            
            scribblePath = [NSString stringWithFormat:@"%@%@",
                            [MethodHelper getScribblePath],
                            pngScribble];
            
            //[self loadDrawingLayerForListItem:listItem];
            myImage = [UIImage imageWithContentsOfFile:scribblePath];
            
            foundImage = TRUE;
        }
        
        UIImageView *imageView;
        
        if (foundImage == TRUE) {
           imageView = [[UIImageView alloc] initWithImage:myImage];
        } else if (foundImage == FALSE) {
            imageView = [[UIImageView alloc] init];
        }
        
        [imageView setFrame:CGRectMake(16, 30, 380, 423)];
        [listButton addSubview:imageView];
        [imageView release];
        
        
        // Remove what we drew from the superview
        //[listButton.drawingLayer.view removeFromSuperview];
       // [drawingLayer.view removeFromSuperview];
        //[drawingLayer release];
        
        // Add borders
        UIView *topBorder = [[UIView alloc] initWithFrame:kDrawingLayerBorderTopFrame];
        [topBorder setBackgroundColor:[UIColor lightGrayColor]];
        [listButton addSubview:topBorder];
        [topBorder release];
        
        UIView *bottomBorder = [[UIView alloc] initWithFrame:kDrawingLayerBorderBottomFrame];
        [bottomBorder setBackgroundColor:[UIColor lightGrayColor]];
        [listButton addSubview:bottomBorder];
        [bottomBorder release];
        
        UIView *rightBorder = [[UIView alloc] initWithFrame:kDrawingLayerBorderRightFrame];
        [rightBorder setBackgroundColor:[UIColor lightGrayColor]];
        [listButton addSubview:rightBorder];
        [rightBorder release];
        
        UIView *leftBorder = [[UIView alloc] initWithFrame:kDrawingLayerBorderLeftFrame];
        [leftBorder setBackgroundColor:[UIColor lightGrayColor]];
        [listButton addSubview:leftBorder];
        [leftBorder release];
        
    } else {

        
        PreviewTableViewResponder * responder1 = [[PreviewTableViewResponder alloc] initWithTaskList:[refMasterTaskListCollection.lists objectAtIndex:listIndex]                                                                                 andTagCollection:tagCollection];
        
        UITableView *previewTableView = [[UITableView alloc] initWithFrame:kPreviewTableViewPortrait style:UITableViewStylePlain];
        
        [previewTableView setTag:kScrollViewTableViewTag];
        [previewTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [previewTableView setUserInteractionEnabled:FALSE];
        [previewTableView setBackgroundColor:[UIColor clearColor]];
        [previewTableView setDataSource:responder1];
        [previewTableView setDelegate:responder1];
        //[previewTableView setAlpha:0.8f];
        
        //[listButton insertSubview:previewTableView atIndex:0];
        [listButton addSubview:previewTableView];
        
        // Add completed image (if required)
        TaskList *taskList = [taskListCollection.lists objectAtIndex:listIndex];
        if ([taskList completed] == TRUE) {
            // Add completed image
            UIImageView *previewListCompletedImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ListPreviewStamp.png"]];
            // 414 * 551
            [previewListCompletedImage setFrame:CGRectMake(57, 149, 299, 193)];
            [previewListCompletedImage setAlpha:0.75f];
            [listButton addSubview:previewListCompletedImage];
            [previewListCompletedImage release];
        }
    }
	
	
}

- (void)showDrawingFrame {
    //[refCurrentDrawingLayer drawFrame];
    //refCurrentDrawingLayer.passThroughTouches = YES;
}

- (void)loadTextFields {
	titleBackgroundToolbar = [[HiddenToolbar alloc] init];
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[titleBackgroundToolbar setFrame:kTitleBackgroundToolbarPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[titleBackgroundToolbar setFrame:kTitleBackgroundToolbarLandscape];
	}	
	
	UIImage *backImage = [UIImage imageNamed:@"TransparentHitSpot.png"];

	UIBarButtonItem *testButton = [[UIBarButtonItem alloc] initWithImage:backImage
																					   style:UIBarButtonItemStylePlain 
																					  target:self action:@selector(titleButtonAction)];
	//UIBarButtonItem *testButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction 
	//																			target:self action:@selector(testButtonAction)];
	//testButton.width = 268;
	//[testButton setTarget:self];
	//[testButton setAction:@selector(testButtonAction)];
	titleBackgroundToolbar.opaque = NO;
	titleBackgroundToolbar.backgroundColor = [UIColor clearColor];
	[titleBackgroundToolbar setItems:[NSArray arrayWithObjects:testButton, nil]];
	[self.view addSubview:titleBackgroundToolbar];
	[testButton release];
	
	titleTextField = [[UITextField alloc] initWithFrame:CGRectZero];
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[titleTextField setFrame:kMainTitleTextFieldPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[titleTextField setFrame:kMainTitleTextFieldLandscape];
	}	
	
	[titleTextField setBorderStyle:UITextBorderStyleNone];
    
    if ([MethodHelper getIOSVersion] < 5.0) {
        [titleTextField setBackgroundColor:[UIColor clearColor]];
    }
	//[titleTextField setBackgroundColor:[UIColor clearColor]];
	[titleTextField setTextAlignment:UITextAlignmentCenter];
	[titleTextField setReturnKeyType:UIReturnKeyDone];
    
	switch ([self currentSection]) {
        case EnumCurrentSectionLists:
        case EnumCurrentSectionNotebookLists:
            if ([taskListCollection.lists count] > 0) {
                TaskList *list = [taskListCollection.lists objectAtIndex:currentListIndex];
                [titleTextField setText:list.title];
            } else {
                [titleTextField setText:@""];
            }
            break;
        case EnumCurrentSectionNotebooks:
            if ([notebookCollection.notebooks count] > 0) {
                Notebook *notebook = [notebookCollection.notebooks objectAtIndex:currentListIndex];
                [titleTextField setText:notebook.title];
            } else {
                [titleTextField setText:@""];
            }
            break;
        default:
            break;
    }
	
	[titleTextField setFont:[UIFont fontWithName:@"Helvetica" size:30.0]];
	[titleTextField setTextColor:[refMasterTaskListCollection getColourForSection:@"MainViewTitleTextField"]];
	[titleTextField setUserInteractionEnabled:FALSE];
	[titleTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
	[titleTextField setAutocapitalizationType:UITextAutocapitalizationTypeWords];
	[titleTextField setDelegate:self];
	[self.view addSubview:titleTextField];
	
	// Create the date label button
	dateLabelButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	[dateLabelButton setBackgroundColor:[UIColor clearColor]];
	[dateLabelButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	[dateLabelButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
	[dateLabelButton addTarget:self action:@selector(dateLabelButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[dateLabelButton setShowsTouchWhenHighlighted:YES];
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[dateLabelButton setFrame:kMainDateLabelPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[dateLabelButton setFrame:kMainDateLabelLandscape];
	}	
	
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
        case EnumCurrentSectionNotebookLists:
            if ([taskListCollection.lists count] > 0) {
                TaskList *list = [taskListCollection.lists objectAtIndex:currentListIndex];
                NSDate *theDate = [MethodHelper dateFromString:list.creationDate usingFormat:K_DATEONLY_FORMAT];
                [dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES]
                                 forState:UIControlStateNormal];
            } else {
                [dateLabelButton setTitle:@"" forState:UIControlStateNormal];
            }
            break;
        case EnumCurrentSectionNotebooks:
            if ([notebookCollection.notebooks count] > 0) {
                Notebook *notebook = [notebookCollection.notebooks objectAtIndex:currentListIndex];
                NSDate *theDate = [MethodHelper dateFromString:notebook.creationDate usingFormat:K_DATEONLY_FORMAT];
                [dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES]
                                 forState:UIControlStateNormal];
            } else {
                [dateLabelButton setTitle:@"" forState:UIControlStateNormal];
            }
            break;
        default:
            break;
    }
	
	
	[self.view addSubview:dateLabelButton];
}

- (void)loadControlPanelObjects {
	// Load the search table responder
	//SearchTableViewResponder *responder = [[SearchTableViewResponder alloc] init];
	
	// Load the control panel
	controlPanelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPanel.png"]];

	// Load the archives image view
	//archivesButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WallSafe.jpeg"]];
	archivesButton = [[UIButton alloc] init];
    [archivesButton setHidden:YES];
	[archivesButton setBackgroundImage:[UIImage imageNamed:@"ArchivesButton.png"] forState:UIControlStateNormal];
	[archivesButton setBackgroundImage:[UIImage imageNamed:@"ArchivesSelectedButton.png"] forState:UIControlStateSelected];
	[archivesButton addTarget:self action:@selector(archivesButtonAction) forControlEvents:UIControlEventTouchUpInside];
		
	// List paper image view
	//listPadImageView = [[UIImageView alloc] init];
	//[listPadImageView setImage:[UIImage imageNamed:@"ListPaper.png"]];
	
	// Load the list pad table view
	listPadTableViewResponder = [[ListPadTableViewResponder alloc] initWithTaskListCollection:taskListCollection notebookCollection:notebookCollection andDelegate:self];
	listPadTableView = [[UITableView alloc] initWithFrame:kListPadTableViewPortrait style:UITableViewStyleGrouped];
	//[listPadTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[listPadTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    [listPadTableView setContentInset:UIEdgeInsetsMake(0, 0, 33, 0)];
    //[listPadTableView setBackgroundColor:[UIColor colorWithRed:20 / 255.0 green:20 / 255.0 blue:20 / 255.0 alpha:1.0]];  // #272324 -- back bit is #141414
    [listPadTableView setBackgroundColor:[UIColor clearColor]];
    //[listPadTableView setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
    //[listPadTableView setBackgroundColor:[UIColor redColor]];
    [listPadTableView setShowsVerticalScrollIndicator:NO];
	[listPadTableView setDataSource:listPadTableViewResponder];
	[listPadTableView setDelegate:listPadTableViewResponder];
	
	tileScrollView.refListPadTableViewResponder = listPadTableViewResponder;
	tileScrollView.refListPadTableView = listPadTableView;
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[controlPanelImageView setFrame:kMainControlPanelImagePortrait];
		[archivesButton setFrame:kArchivesImagePortrait];
		//[listPadImageView setFrame:kListPadImagePortrait];
		[listPadTableView setFrame:kListPadTableViewPortrait];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Portrait];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Portrait];
		[listPadFooterLine setFrame:kListPadFooterLinePortrait];
		[listPadTitleLabel setFrame:kListPadTitleLabelPortrait];
		//[searchTableView setFrame:kSearchTableViewPortrait];
		//[searchBar setFrame:kSearchBarPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[controlPanelImageView setFrame:kMainControlPanelImageLandscape];
		[archivesButton setFrame:kArchivesImageLandscape];
		//[listPadImageView setFrame:kListPadImageLandscape];
		[listPadTableView setFrame:kListPadTableViewLandscape];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Landscape];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Landscape];
		[listPadFooterLine setFrame:kListPadFooterLineLandscape];
		[listPadTitleLabel setFrame:kListPadTitleLabelLandscape];
		//[searchTableView setFrame:kSearchTableViewLandscape];
		//[searchBar setFrame:kSearchBarLandscape];
	}
	
	[self.view addSubview:controlPanelImageView];
	[self.view addSubview:archivesButton];
	//[self.view addSubview:listPadImageView];
	[self.view addSubview:listPadTableView];
	[self.view addSubview:listPadHeaderLine1];
	[self.view addSubview:listPadHeaderLine2];
	[self.view addSubview:listPadFooterLine];
	[self.view addSubview:listPadTitleLabel];
    
	//[self.view addSubview:searchTableView];
	//[self.view addSubview:searchBar];
}

#pragma mark - MPAdViewDelegate Methods

- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}

- (void)adViewDidLoadAd:(MPAdView *)view {
    CGSize size = [view adContentViewSize];
    CGRect newFrame = view.frame;
    
    newFrame.size = size;
    newFrame.origin.x = (self.view.bounds.size.width - size.width) / 2;
    view.frame = newFrame;
}

#pragma mark -
#pragma mark Scroll View Method Helpers

- (void)insertNewListButton {
	// If list collection is empty, exit this method
	if ([taskListCollection.lists count] == 0) {
		return;
	}
	
	// Create a list pointer to the first list in the list collection
	TaskList *aList = [taskListCollection.lists objectAtIndex:0];
	if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
		aList = [taskListCollection.lists lastObject];
	}
	
	// Need to update the scroll list without removing all subviews first
	GLButton *listButton = [GLButton buttonWithType:UIButtonTypeCustom];
	
	UIImage *image = [UIImage imageNamed:@"PaperPreview.png"];
	[listButton setBackgroundImage:image forState:UIControlStateNormal];
	[listButton addTarget:self action:@selector(listButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	
	CGRect rect = listButton.frame;
	rect.size.height = kScrollObjHeight;
	rect.size.width = kScrollObjWidth;
	listButton.frame = rect;
	
	listButton.tag = aList.listID;	// tag our images for later use when we place them in serial fashion
	
	// Need to add subviews to list button
	if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		[self addListPreviewToButton:listButton forListIndex:0];
	} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
		[self addListPreviewToButton:listButton forListIndex:[taskListCollection.lists count] - 1];
	}
	
	// Add shadeView
	UIView *shadeView = [[UIView alloc] initWithFrame:kListShadeViewFrame];
	[shadeView setBackgroundColor:[UIColor blackColor]];
	[shadeView setAlpha:0.3f];
	//[shadeView setHidden:YES];
	[shadeView setTag:kShadeViewTag]; 
	[listButton addSubview:shadeView];
	[shadeView release];
	
	if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		[listScrollView insertSubview:listButton atIndex:0];
	} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
		[listScrollView addSubview:listButton];
	}
	
	UIImageView *view = nil;
	//NSArray *subviews = [contentView subviews];
	NSArray *subviews = [listScrollView subviews];
	
	// reposition all image subviews in a horizontal serial fashion
	CGFloat curXLoc = 35;
	
	NSInteger i = 0;
	for (view in subviews) {
		if ([view isKindOfClass:[GLButton class]]) {
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			//curXLoc += (kScrollObjWidth + kScrollBuffer);
			curXLoc += kScrollObjWidth + kScrollObjBuffer;
			
			// Update the list button tags
			//UIButton *tempListButton = (UIButton *)view;
			//tempListButton.tag
            if (i < [taskListCollection.lists count]) {
               view.tag = [[taskListCollection.lists objectAtIndex:i] listID]; 
            }
			
						
			i++;
		}
	}
	
	// set the content size so it can be scrollable
	//[myScrollView setContentSize:CGSizeMake((5 * (kScrollObjWidth + (2 * kScrollBuffer))), [myScrollView bounds].size.height)];
	
	[listScrollView setContentSize:CGSizeMake(([taskListCollection.lists count] * (kScrollObjWidth + kScrollObjBuffer)), [listScrollView bounds].size.height)];
	
	NSInteger scrollIndex = currentListIndex;
	if (newListInserted == TRUE && [refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		scrollIndex++;
		newListInserted = FALSE;
	}
	
	CGFloat newXLoc = (kScrollObjWidth + kScrollObjBuffer) * scrollIndex;
	[listScrollView setContentOffset:CGPointMake(newXLoc, 0)];
	
	// Reload the list pad table view
	[listPadTableView reloadData];
}

- (void)insertNewNotebookButton {
    // If notebook collection is empty, exit this method
	if ([notebookCollection.notebooks count] == 0) {
		return;
	}
	
	// Create a notebook pointer to the first notebook in the list collection
    Notebook *notebook = [notebookCollection.notebooks objectAtIndex:0];

	// Need to update the scroll list without removing all subviews first
	GLButton *listButton = [GLButton buttonWithType:UIButtonTypeCustom];
	
	UIImage *image = [UIImage imageNamed:@"NotebookOpen.png"];
	[listButton setBackgroundImage:image forState:UIControlStateNormal];
	[listButton addTarget:self action:@selector(listButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	
	CGRect rect = listButton.frame;
	rect.size.height = kScrollObjHeight;
	rect.size.width = kScrollObjWidth;
	listButton.frame = rect;
	
	listButton.tag = notebook.notebookID;	// Tag our images for later use when we place them in serial fashion
	
	// Add shadeView
	UIView *shadeView = [[UIView alloc] initWithFrame:kNotebookShadeViewFrame];
	[shadeView setBackgroundColor:[UIColor blackColor]];
	[shadeView setAlpha:0.3f];
	//[shadeView setHidden:YES];
	[shadeView setTag:kShadeViewTag]; 
	[listButton addSubview:shadeView];
	[shadeView release];
	
    // Add the notebook scroll view
    CGRect theNotebookImageFrame = CGRectZero;
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        theNotebookImageFrame = kNotebookImageFramePortrait;
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        theNotebookImageFrame = kNotebookImageFrameLandscape;
    }
    
    // Frame depends on portrait or landscape
    NotebookTileScrollView *notebookTileScrollView = [[NotebookTileScrollView alloc] initWithTaskListCollection:taskListCollection 
                                                                                          andMainViewController:self 
                                                                                    andMasterTaskListCollection:refMasterTaskListCollection 
                                                                                                    andNotebook:notebook
                                                                                          andNotebookImageFrame:theNotebookImageFrame];
    
    [listButton addSubview:notebookTileScrollView];
    [notebookTileScrollView release];
    
    // Add the list button to the scroll view
    [listScrollView insertSubview:listButton atIndex:0];
	
	UIImageView *view = nil;
	//NSArray *subviews = [contentView subviews];
	NSArray *subviews = [listScrollView subviews];
	
	// reposition all image subviews in a horizontal serial fashion
	CGFloat curXLoc = 35;
	
	NSInteger i = 0;
	for (view in subviews) {
		if ([view isKindOfClass:[GLButton class]]) {
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			//curXLoc += (kScrollObjWidth + kScrollBuffer);
			curXLoc += kScrollObjWidth + kScrollObjBuffer;
			
			// Update the list button tags
			//UIButton *tempListButton = (UIButton *)view;
			//tempListButton.tag
            if (i < [notebookCollection.notebooks count]) {
                view.tag = [[notebookCollection.notebooks objectAtIndex:i] notebookID]; 
            }
			
            
			i++;
		}
	}

	[listScrollView setContentSize:CGSizeMake(([notebookCollection.notebooks count] * (kScrollObjWidth + kScrollObjBuffer)), [listScrollView bounds].size.height)];
	
	NSInteger scrollIndex = currentListIndex;
	if (newListInserted == TRUE && [refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		scrollIndex++;
		newListInserted = FALSE;
	}
	
	CGFloat newXLoc = (kScrollObjWidth + kScrollObjBuffer) * scrollIndex;
	[listScrollView setContentOffset:CGPointMake(newXLoc, 0)];
	
	// Reload the list pad table view
	[listPadTableView reloadData];
}

#pragma mark -
#pragma mark UIScrollView Delegates

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	CGPoint contentOffset = [scrollView contentOffset];
	NSInteger xLoc = 0;
	
	if (contentOffset.x > 0) {
		xLoc = (contentOffset.x + 200) / (kScrollObjWidth + kScrollObjBuffer);
	}
    
	// Do nothing if xloc is greater than list or notebooks length
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
        case EnumCurrentSectionNotebookLists:
            if (xLoc >= [taskListCollection.lists count]) {
                return;
            } 
            break;
        case EnumCurrentSectionNotebooks:
            if (xLoc >= [notebookCollection.notebooks count]) {
                return;
            } 
            break;
        default:
            break;
    }
	
	int i = 0;
	if (currentListIndex != xLoc) {

		for (UIView *view in listScrollView.subviews) {
			if ([view isKindOfClass:[GLButton class]]) {
				GLButton *listButton = (GLButton *)view;
				// Do not highlight if auto scrolling
				if (isAutoScrolling == FALSE) {
					UIView *shadeView = (UIView *)[listButton viewWithTag:kShadeViewTag];
                    
					if (xLoc == i) {
						[shadeView setHidden:YES];
						//[listButton setHighlighted:NO];
					} else {
						[shadeView setHidden:NO];
						//[listButton setHighlighted:YES];
					}
				}
				i++;	// Up i here, just for each button found
			}
		}
		
		currentListIndex = xLoc;

        
		// Update the title and the date, get the current list
        switch ([self currentSection]) {
            case EnumCurrentSectionLists:
            case EnumCurrentSectionNotebookLists:
                currentListIndex = currentListIndex;  // Need this silly line for the annoying switch bug
                TaskList *list = [taskListCollection.lists objectAtIndex:currentListIndex];
                [titleTextField setText:list.title];
                NSDate *listDate = [MethodHelper dateFromString:list.creationDate usingFormat:K_DATEONLY_FORMAT];
                [dateLabelButton setTitle:[MethodHelper localizedDateFrom:listDate usingStyle:NSDateFormatterLongStyle withYear:YES]
                                 forState:UIControlStateNormal];
                break;
            case EnumCurrentSectionNotebooks:
                currentListIndex = currentListIndex;  // Need this silly line for the annoying switch bug
                Notebook *notebook = [notebookCollection.notebooks objectAtIndex:currentListIndex];
                [titleTextField setText:notebook.title];
                NSDate *notebookDate = [MethodHelper dateFromString:notebook.creationDate usingFormat:K_DATEONLY_FORMAT];
                [dateLabelButton setTitle:[MethodHelper localizedDateFrom:notebookDate usingStyle:NSDateFormatterLongStyle withYear:YES]
                                 forState:UIControlStateNormal];
                break;
            default:
                break;
        }
		
        // Set the title
        switch ([self currentSection]) {
            case EnumCurrentSectionLists:
                if ([taskListCollection.lists count] == 0) {
                    self.title = @"My Lists & Notes";
                } else {
                    self.title = [NSString stringWithFormat:@"My Lists & Notes (%d of %d)", currentListIndex + 1, [taskListCollection.lists count]];
                }
                break;
            case EnumCurrentSectionNotebooks:
                if ([notebookCollection.notebooks count] == 0) {
                    self.title = @"My Folders";
                } else {
                    self.title = [NSString stringWithFormat:@"My Folders (%d of %d)", currentListIndex + 1, [notebookCollection.notebooks count]];
                }
                break;
            case EnumCurrentSectionNotebookLists:
                if (selectedListIndex < 0 || selectedListIndex >= [notebookCollection.notebooks count]) {
                    [MethodHelper showAlertViewWithTitle:@"Error" 
                                              andMessage:@"Unable to find list for selected folder.  Please report this error to manage@kerofrog.com.au" 
                                          andButtonTitle:@"Ok"];
                    break;
                }
                Notebook *notebook = [notebookCollection.notebooks objectAtIndex:selectedListIndex];
                if ([taskListCollection.lists count] == 0) {
                    self.title = notebook.title;
                } else {
                    self.title = [NSString stringWithFormat:@"%@ (%d of %d)", notebook.title, currentListIndex + 1, [taskListCollection.lists count]];
                }
                break;
            default:
                break;
        }
        
		// Force select row in list pad
		if (isAutoScrolling == FALSE) {
			[listPadTableViewResponder tableView:listPadTableView 
                    forceSelectRowAtIndexPathRow:currentListIndex
                                      andSection:[self currentSection]];
		}
	} else if (didChangeSection == TRUE) {
        [listPadTableViewResponder tableView:listPadTableView 
                forceSelectRowAtIndexPathRow:currentListIndex
                                  andSection:[self currentSection]];
        didChangeSection = FALSE;
    }
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
	isAutoScrolling = NO;
    
	[self listShaded:NO forIndex:currentListIndex];
	
	// Update title and date
	[self updateTitleAndDateForSelectedIndex];
	
	// Force select row in list pad
	[listPadTableViewResponder tableView:listPadTableView 
            forceSelectRowAtIndexPathRow:currentListIndex
                              andSection:[self currentSection]];	
}

#pragma mark -
#pragma mark Button Actions

- (void)controlPanelBarButtonItemAction {
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
    //Tan Nguyen Fix leaks
	if (controlPanelPopoverController == nil) {
        
        ControlPanelViewController *controller = [[ControlPanelViewController alloc] initWithTaskListCollection:taskListCollection 
                                                                                             notebookCollection:notebookCollection];
        
        controller.refMainViewController = self;
        controlPanelPopoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
        [controlPanelPopoverController presentPopoverFromBarButtonItem:controlPanelBarButtonItem 
                                              permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        [controller release];
        
    }
	else if ([controlPanelPopoverController isPopoverVisible]) {
		[controlPanelPopoverController dismissPopoverAnimated:YES];
        controlPanelPopoverController = nil;
        [controlPanelPopoverController release];
		return;
	}
    else{
        [controlPanelPopoverController presentPopoverFromBarButtonItem:controlPanelBarButtonItem 
                                              permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        return;
    }
	
}

- (void)dateLabelButtonAction {
	// Popup the date controller
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        TaskList *taskList = [taskListCollection.lists objectAtIndex:currentListIndex];
        NewDueDateTableViewController *controller = [[[NewDueDateTableViewController alloc] initWithDueDate:taskList.creationDate 
                                                                                                andDelegate:self] autorelease];
        dateLabelPopoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
    } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
        Notebook *notebook = [notebookCollection.notebooks objectAtIndex:currentListIndex];
        NewDueDateTableViewController *controller = [[[NewDueDateTableViewController alloc] initWithDueDate:notebook.creationDate 
                                                                                                andDelegate:self] autorelease];
        dateLabelPopoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
    }
	
	
	// Allocate the pop over controller, that will host the nav controller and view controller
	
	// Present the popover using the calculated location to display the arrow
	[dateLabelPopoverController presentPopoverFromRect:dateLabelButton.frame inView:self.view 
							  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)archivesButtonAction {
	// Close the control panel popover if it is visible
	if ([controlPanelPopoverController isPopoverVisible]) {
		[controlPanelPopoverController dismissPopoverAnimated:YES];
        controlPanelPopoverController = nil;
        [controlPanelPopoverController release];
	}
	
	archivesVisited = TRUE;
	
	ArchivesViewController *controller = [[[ArchivesViewController alloc] initWithTagCollection:tagCollection
																		  andMasterTaskListCollection:refMasterTaskListCollection] autorelease];
	
	controller.delegate = self;
	[self.navigationController pushViewController:controller animated:NO];
}

- (void)titleButtonAction {
	//[titleTextField 
	[titleTextField setUserInteractionEnabled:TRUE];
	[titleTextField becomeFirstResponder];


}

- (void)helpBarButtonItemAction {
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
		return;
	}
	
	// Show the help overlay
	isHelpSelected = TRUE;
	
	UIButton *helpOverlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[helpOverlayButton setAlpha:0.75f];
	helpOverlayButton.tag = kHelpOverlayTag;
	[helpOverlayButton addTarget:self action:@selector(helpOverlayButtonAction:) forControlEvents:UIControlEventTouchDown];
	
	
	if ([self currentViewType] == EnumViewTypeList) {
        if ([self currentSection] == EnumCurrentSectionLists) {
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
                [helpOverlayButton setImage:[UIImage imageNamed:@"MAIN-SCREEN-SCROLLING-LIST-VIEW-PORTRAIT-OVERLAY.png"] forState:UIControlStateNormal];
                [helpOverlayButton setFrame:CGRectMake(0, 0, 768, 960)];
            } else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
                [helpOverlayButton setImage:[UIImage imageNamed:@"MAIN-SCREEN-SCROLLING-LIST-VIEW-LANDSCAPE-OVERLAY.png"] forState:UIControlStateNormal];
                [helpOverlayButton setFrame:CGRectMake(0, 0, 1024, 704)];
            }
        } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
                [helpOverlayButton setImage:[UIImage imageNamed:@"MAIN-SCREEN-SCROLLING-NOTEBOOK-VIEW-PORTRAIT-OVERLAY.png"] forState:UIControlStateNormal];
                [helpOverlayButton setFrame:CGRectMake(0, 0, 768, 960)];
            } else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
                [helpOverlayButton setImage:[UIImage imageNamed:@"MAIN-SCREEN-SCROLLING-NOTEBOOK-VIEW-LANDSCAPE-OVERLAY.png"] forState:UIControlStateNormal];
                [helpOverlayButton setFrame:CGRectMake(0, 0, 1024, 704)];
            }
        }
		
	} else if ([self currentViewType] == EnumViewTypeTile) {
		if ([self currentSection] == EnumCurrentSectionLists) {
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
                [helpOverlayButton setImage:[UIImage imageNamed:@"MAIN-SCREEN-TILE-LIST-VIEW-PORTRAIT-OVERLAY.png"] forState:UIControlStateNormal];
                [helpOverlayButton setFrame:CGRectMake(0, 0, 768, 960)];
            } else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
                [helpOverlayButton setImage:[UIImage imageNamed:@"MAIN-SCREEN-TILE-LIST-VIEW-LANDSCAPE-OVERLAY.png"] forState:UIControlStateNormal];
                [helpOverlayButton setFrame:CGRectMake(0, 0, 1024, 704)];
            }
        } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
                [helpOverlayButton setImage:[UIImage imageNamed:@"MAIN-SCREEN-TILE-NOTEBOOK-VIEW-PORTRAIT-OVERLAY.png"] forState:UIControlStateNormal];
                [helpOverlayButton setFrame:CGRectMake(0, 0, 768, 960)];
            } else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
                [helpOverlayButton setImage:[UIImage imageNamed:@"MAIN-SCREEN-TILE-NOTEBOOK-VIEW-LANDSCAPE-OVERLAY.png"] forState:UIControlStateNormal];
                [helpOverlayButton setFrame:CGRectMake(0, 0, 1024, 704)];
            }
        }
	}
	
	[self.view addSubview:helpOverlayButton];
}

- (void)helpOverlayButtonAction:(id)sender {
	isHelpSelected = FALSE;
	UIButton *helpOverlayButton = (UIButton *)sender;
	[helpOverlayButton removeFromSuperview];
}

// Search bar button item (need to show popup with tableview search)
- (void)searchBarButtonItemAction {
	// Call the popup
	//SearchResultsViewController *viewController = [[SearchResultsViewController alloc] initWithStyle:UITableViewStylePlain];
	/*SearchResultsViewController *viewController = [[SearchResultsViewController alloc] initWithNibName:@"SearchResultsView" bundle:nil];
	
	
	// Need to get all task lists (task list collection)
	viewController.listContent = listCollection.lists;
	
	// Also need to get all list items.. harder
	for (TaskList *taskList in listCollection.lists) {
		viewController.listItemContent = [viewController.listContent arrayByAddingObjectsFromArray:taskList.listItems];
	}
	
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
	
	UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:navController];
	[viewController release];
	[navController release];
	
	[popoverController presentPopoverFromBarButtonItem:searchBarButtonItem 
							  permittedArrowDirections:UIPopoverArrowDirectionUp
											  animated:YES];*/
}

// Options bar button item 
- (void)optionsBarButtonItemAction:(id)sender {
	UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
    
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
        case EnumCurrentSectionNotebookLists:
            if ([taskListCollection.lists count] == 0) {
                // do nothing
                return;
            }
            
            if (currentListIndex < 0 || currentListIndex >= [refMasterTaskListCollection.lists count]) {
                return;
            }
            TaskList *checkTaskList = [refMasterTaskListCollection.lists objectAtIndex:currentListIndex];
            if ([checkTaskList isNote] == FALSE) {
                [optionsListActionSheet showFromBarButtonItem:barButtonItem animated:NO];
            } else if ([checkTaskList isNote] == TRUE) {
                [exportNoteActionSheet showFromBarButtonItem:barButtonItem animated:NO];
            }
            
            
            break;
        case EnumCurrentSectionNotebooks:
            if ([notebookCollection.notebooks count] == 0) {
                // do nothing
                return;
            }

            if ([self currentSection] == EnumCurrentSectionNotebooks) {
                if (currentListIndex >= 0 && currentListIndex < [notebookCollection.notebooks count]) {
                    Notebook *notebook = [notebookCollection.notebooks objectAtIndex:currentListIndex];
                    
                    NotebookListsViewController *controller = [[[NotebookListsViewController alloc] initWithNotebook:notebook 
                                                                                         andMasterTaskListCollection:refMasterTaskListCollection] autorelease];
                    // Init the navigation controller that will hold the edit list item view controller
                    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
                    notebookListsPopoverController = [[UIPopoverController alloc] initWithContentViewController:navController];
                    notebookListsPopoverController.delegate = self;
                    [notebookListsPopoverController presentPopoverFromBarButtonItem:optionsBarButtonItem 
                                                           permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
                }
            }
            break;
        default:
            break;
    }

	[myToolbar setUserInteractionEnabled:FALSE];
}

// Delete list bar button item action
- (void)deleteListBarButtonItemAction:(id)sender {
	if ([taskListCollection.lists count] == 0) {
		// do nothing
		return;
	}
	
	UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
	
    // Check current section for which action sheet will display
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        [deleteListActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
        [deleteNotebookActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    }
    
	[myToolbar setUserInteractionEnabled:FALSE];
}

// New list bar button item actoin
- (void)newListBarButtonItemAction:(id)sender {
	UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;

    // Check current section for which action sheet will display
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        [newListActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
        [newNotebookActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    }
    
	[myToolbar setUserInteractionEnabled:FALSE];
}

- (void)newListLeftBarButtonAction:(id)sender {
	if ([controlPanelPopoverController isPopoverVisible]) {
		[controlPanelPopoverController dismissPopoverAnimated:YES];
        controlPanelPopoverController = nil;
        [controlPanelPopoverController release];
	}
	
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
	
	UIBarButtonItem *barButtonItem = (UIBarButtonItem *)sender;
	
    // Check current section for which action sheet will display
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        [newListActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
        [newNotebookActionSheet showFromBarButtonItem:barButtonItem animated:NO];
    }
    
	// New List pressed
	/*TaskList *newList = [[TaskList alloc] initNewList];
	[listCollection.lists addObject:newList];
	[newList release];
	
	// Refresh the scroll view
	[self updateScrollList];
	[self scrollToListAtIndex:[listCollection.lists count] - 1];
	[self listShaded:NO forIndex:[listCollection.lists count] - 1];*/
}

- (void)openListWithListID:(NSInteger)theListID usingFrame:(CGRect)touchedViewFrame {
	//viewTypeSegmentedControl.selectedSegmentIndex = EnumViewTypeList;
	//currentViewType = viewTypeSegmentedControl.selectedSegmentIndex;
    if ([self currentSection] == EnumCurrentSectionNotebooks) {
        self.selectedNotebookListID = theListID;
    }
    
    // Assign the touched view frame to our notebook collection
    notebookCollection.notebookListTileFrame = touchedViewFrame;
    
	// Exit if ref touched view is nil
	animatedListImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PaperPreview.png"]];
	[animatedListImage setFrame:touchedViewFrame];
	[self.view addSubview:animatedListImage];

	// Need to bring forward border and images on top of border
	[self.view bringSubviewToFront:borderImage];
	[self.view bringSubviewToFront:itemsDuePocket];
	[self.view bringSubviewToFront:itemsDueTableView];
	
	//if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
	[self.view bringSubviewToFront:controlPanelImageView];
	[self.view bringSubviewToFront:archivesButton];
	//[self.view bringSubviewToFront:listPadImageView];
	[self.view bringSubviewToFront:listPadTableView];
	[self.view bringSubviewToFront:listPadHeaderLine1];
	[self.view bringSubviewToFront:listPadHeaderLine2];
	[self.view bringSubviewToFront:listPadFooterLine];
	[self.view bringSubviewToFront:listPadTitleLabel];
    
	//}
	
	// Now expand the list to the correct size
	[UIView beginAnimations:@"Zoom Tile Animation" context:nil];
	[UIView setAnimationDuration:0.5f];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView setAnimationDelegate:self];
	// Set the new page size
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[animatedListImage setFrame:CGRectMake(43, 30, 684, 891)];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[animatedListImage setFrame:CGRectMake(299, 30, 684, 635)];
	}
	[UIView commitAnimations];
	
	//UIButton *selectedButton = [self getListItemButtonWithIndex:theListIndex];
	//[self listButtonAction:selectedButton];
}

- (void)listButtonAction:(id)sender {
	if (listButtonClickDisabled == TRUE) {
		// do nothing
		return;
	} else {
		listButtonClickDisabled = TRUE;
	}
	
	GLButton *selectedList = (GLButton *)sender;
	
	// Need to get correct task list with specified list id
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
            selectedListIndex = [taskListCollection getIndexOfTaskListWithID:selectedList.tag];
            break;
        case EnumCurrentSectionNotebooks:
            // Also need to change the current section
            /*self.currentSection = EnumCurrentSectionNotebookLists;
            selectedListIndex = [notebookCollection getIndexOfNotebookWithID:selectedList.tag];
            Notebook *notebook = [notebookCollection.notebooks objectAtIndex:selectedListIndex];
            
          
            [taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection 
                                                         usingList:[notebook lists]];
            notebookCollection.selectedNotebookIndex = selectedListIndex;
            for (TaskList *taskList in taskListCollection.lists) {
                NSLog(@"Found: Tasklist.title: %@", taskList.title);
            }
            selectedListIndex = 0;
            [self updateListScrollList];*/
            return;
        default:
            break;
    }
	
    // TODO: Add code to open up new task list collection based on selected notebook
    // Need to build a task list collection using a passed in list of list ID's
    
	// In case it not found, make it zero
	if (selectedListIndex == -1) {
		// Make it zero
		selectedListIndex = 0;
	}
	
	// Before pushing the new view controller, do an animation that will 'enlarge' the page so that the 
	// transition is invisible to the user
	// Generate the image and expand it
	
	animatedListImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PaperPreview.png"]];
	//UIImageView *listImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PaperPreview.png" ofType:nil]]];
	
	//(142, 180, 484, 551)];
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[animatedListImage setFrame:CGRectMake(177, 180, 414, 487)];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[animatedListImage setFrame:CGRectMake(433, 82, 414, 487)];
	}
	
	[self.view addSubview:animatedListImage];
	
	// Bring animated list image to front
	[self.view bringSubviewToFront:animatedListImage];
	
	// Need to bring forward border and images on top of border
	[self.view bringSubviewToFront:borderImage];
	[self.view bringSubviewToFront:itemsDuePocket];
	[self.view bringSubviewToFront:itemsDueTableView];

	if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[self.view bringSubviewToFront:controlPanelImageView];
		[self.view bringSubviewToFront:archivesButton];
		//[self.view bringSubviewToFront:listPadImageView];
		[self.view bringSubviewToFront:listPadTableView];
		[self.view bringSubviewToFront:listPadHeaderLine1];
		[self.view bringSubviewToFront:listPadHeaderLine2];
		[self.view bringSubviewToFront:listPadFooterLine];
		[self.view bringSubviewToFront:listPadTitleLabel];
		//[self.view bringSubviewToFront:titleTextField];
		//[self.view bringSubviewToFront:dateLabel];
		//[self.view bringSubviewToFront:myToolbar];
	}
	
	// Create an animation to grow the preview image to the correct size
	// Need to do a uiview animation
	[UIView beginAnimations:@"Zoom List Animation" context:nil];
	[UIView setAnimationDuration:0.5f];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView setAnimationDelegate:self];
	
	// Set the new page size
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[animatedListImage setFrame:CGRectMake(43, 30, 684, 829)];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[animatedListImage setFrame:CGRectMake(299, 30, 684, 573)];
	}
	
	[UIView commitAnimations];
}

- (void)settingsBarButtonItemAction {    
    
	if (settingsPopoverController != nil) {
        if ([settingsPopoverController isPopoverVisible]) {
            // Do nothing
            [settingsPopoverController dismissPopoverAnimated:YES];
            settingsPopoverController = nil;
            [settingsPopoverController release];
            return;
        }
	}

    [settingsBarButtonItem setEnabled:FALSE];
    [helpBarButtonItem setEnabled:FALSE];
    [viewTypeSegmentedControl setUserInteractionEnabled:FALSE];

    
	// Init the select view type controller
	SettingsViewController *settingsViewController = [[SettingsViewController alloc] initWithDelegate:self andTagCollection:tagCollection
																			andPropertyCollectionType:EnumPropertyCollectionApplication
																				andTaskListCollection:refMasterTaskListCollection];
	settingsViewController.refPopoverController = settingsPopoverController;
	
	// Init the navigation controller that will hold the edit list item view controller
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
	[settingsViewController release];
	
	// Allocate the pop over controller, that will host the nav controller and view controller
	settingsPopoverController = [[UIPopoverController alloc] initWithContentViewController:navController];
	[settingsPopoverController setDelegate:self];
	[navController release];
	
	// Present the popover using the calculated location to display the arrow
	[settingsPopoverController presentPopoverFromBarButtonItem:settingsBarButtonItem 
									  permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)viewTypeSegmentedControlAction {
	if ([self currentViewType] == viewTypeSegmentedControl.selectedSegmentIndex) {
		return;
	}
	
	// Close help menu if open
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
   
	self.currentViewType = viewTypeSegmentedControl.selectedSegmentIndex;
    
	[self commitViewType];
    
    // Update not having cover view
    
}

- (void)manualSetSelectedSegmentIndex:(NSInteger)newIndex {
    [viewTypeSegmentedControl setSelectedSegmentIndex:newIndex];
    if ([self currentViewType] == viewTypeSegmentedControl.selectedSegmentIndex) {
		return;
	}
	
	// Close help menu if open
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
    
	self.currentViewType = viewTypeSegmentedControl.selectedSegmentIndex;
    
	[self commitViewType];
}

#pragma mark -
#pragma mark Action Sheet Delegates

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ([actionSheet isEqual:newListActionSheet]) {
		if (buttonIndex == 0) {
			// Set new list inserted to true
			newListInserted = TRUE;
			
			// New List pressed
            //Son Nguyen fix leaks
			TaskList *newMasterList = [[TaskList alloc] initNewList];

			// Insert the new list in the database and view
			[self insertNewTaskList:newMasterList];
            [newMasterList release];
		} else if (buttonIndex == 1) {
            // Create a new note
            newListInserted = TRUE;
            //Son Nguyen fix leaks
            // New Note pressed
            TaskList *newMasterNote = [[TaskList alloc] initNewNote];
            
            // Insert the new note in the database and view
            [self insertNewTaskList:newMasterNote];
            [newMasterNote release];
        } else if (buttonIndex == 2) {
            // Duplicate the currently selected task list
            /*progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
            [self.navigationController.view addSubview:progressHUD];
            progressHUD.delegate = self;
            progressHUD.labelText = @"Duplicating task list...";
            [progressHUD showWhileExecuting:@selector(duplicateTheCurrentTaskList) onTarget:self withObject:nil animated:YES];*/
            [self duplicateTheCurrentTaskList];
		}
		
	} else if ([actionSheet isEqual:deleteListActionSheet]) {
		// Delete selected list
		if (buttonIndex == 0) {
            // Set alpha of selected index to 0
            GLButton *listButton = [self getListItemButtonWithIndex:currentListIndex];
            
            if (listButton != nil) {
                UITableView *previewTableView = (UITableView *)[listButton viewWithTag:kScrollViewTableViewTag];
                [previewTableView setAlpha:0.0f];
            }
            
            // Need to do a uiview animation
            [UIView beginAnimations:@"Delete List Animation" context:nil];
            [UIView setAnimationDuration:0.5f];
            [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
            [UIView setAnimationDelegate:self];
            
            if (listButton != nil) {
                [listButton setAlpha:0.0f];
            }
            
            [UIView commitAnimations];
		}
	} else if ([actionSheet isEqual:optionsListActionSheet]) {
		// Email list
		if (buttonIndex == 0) {
			[self sendTextMailForTaskList:[taskListCollection.lists objectAtIndex:currentListIndex]];
		} else if (buttonIndex == 1) {
			TaskList *theTaskList = [taskListCollection.lists objectAtIndex:currentListIndex];
			[self sendPDFMailForTaskList:theTaskList];
		}
	} else if ([actionSheet isEqual:exportNoteActionSheet]) {
        if (buttonIndex == 0) {
            // Email pdf version
            TaskList *theTaskList = [taskListCollection.lists objectAtIndex:currentListIndex];
			[self sendPDFMailForTaskList:theTaskList];
        }
    } else if ([actionSheet isEqual:newNotebookActionSheet]) {
        
        // Need to add new notebook
        if (buttonIndex == 0) {
            // Set new list inserted to true
			newListInserted = TRUE;
			
			// New notebook pressed
            Notebook *notebook = [[[Notebook alloc] initNewNotebook] autorelease];
            
			// Insert the new list in the database and view
			[self insertNewNotebook:notebook];
        }
        
    } else if ([actionSheet isEqual:deleteNotebookActionSheet]) {
        // Need to delete the current notebook
        if (buttonIndex == 0) {
            // Set alpha of selected index to 0
            GLButton *listButton = [self getListItemButtonWithIndex:currentListIndex];
            
            if (listButton != nil) {
                UITableView *previewTableView = (UITableView *)[listButton viewWithTag:kScrollViewTableViewTag];
                [previewTableView setAlpha:0.0f];
            }
            
            // Need to do a uiview animation
            [UIView beginAnimations:@"Delete List Animation" context:nil];
            [UIView setAnimationDuration:0.5f];
            [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
            [UIView setAnimationDelegate:self];
            
            if (listButton != nil) {
                [listButton setAlpha:0.0f];
            }
            
            [UIView commitAnimations];
        }
    } 
    
	// Set toolbar user interaction to true
	[myToolbar setUserInteractionEnabled:TRUE];
}

#pragma mark -
#pragma mark Items Due Responder Delegates

- (void)listItemSelected:(ListItem *)selectedListItem atLocation:(CGRect)popOverRect {
	// Init the edit list item view controller
	EditListItemDueViewController *editListItemDueViewController = [[EditListItemDueViewController alloc] initWithDelegate:self 
																												   andSize:kEditListItemViewControllerSize 
																											   andListItem:selectedListItem
																										  andTagCollection:tagCollection
																									   andIsFilterTagsMode:NO
																									 andTaskListCollection:refMasterTaskListCollection
                                                                                                     didAppearFromListView:FALSE];
	
    editListItemDueViewController.popoverRect = popOverRect;
    editListItemDueViewController.isDueDateViewController = YES;
    
	// Init the navigation controller that will hold the edit list item view controller
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:editListItemDueViewController];
	
    if (editListItemPopoverController != nil) {
        editListItemPopoverController = nil;
        [editListItemPopoverController release];
    }
	
	// Allocate the pop over controller, that will host the nav controller and view controller
	editListItemPopoverController = [[UIPopoverController alloc] initWithContentViewController:navController];
	
	[editListItemDueViewController setPopoverController:editListItemPopoverController];
	
	[editListItemDueViewController release];
	[navController release];
	
	// Present the popover using the calculated location to display the arrow
	[editListItemPopoverController presentPopoverFromRect:popOverRect inView:self.view 
								 permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
	
}

#pragma mark - DrawPadDelegate Methods
- (void)dismissDrawPadPopOver:(BOOL)newScribbleItem {
    [drawPadPopoverController dismissPopoverAnimated:YES];
}

- (void)drawPadUpdatedForListItem:(ListItem *)listItem {
    // Update the master task list collection
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		NSInteger filterListItemIndex = [taskList getIndexOfListItemWithID:listItem.listItemID];
		// Make sure list item is found
		if (filterListItemIndex != -1) {
			ListItem *foundListItem = [taskList.listItems objectAtIndex:filterListItemIndex];
			[foundListItem updateSelfWithListItem:listItem];
		}
	}
	
	// Update the task list collection
	for (TaskList *taskList in taskListCollection.lists) {
		NSInteger filterListItemIndex = [taskList getIndexOfListItemWithID:listItem.listItemID];
		// Make sure list item is found
		if (filterListItemIndex != -1) {
			ListItem *foundListItem = [taskList.listItems objectAtIndex:filterListItemIndex];
			[foundListItem updateSelfWithListItem:listItem];
		}
	}
    
    [self taskCompleted:listItem.completed forListItemID:listItem.listItemID];
	
	// Reload items due table
	[itemsDueTableViewResponder reloadDataWithListParentTitle:nil andTableView:itemsDueTableView
											  andParentListID:-1 andListItem:listItem];
	
	if ([self currentViewType] == EnumViewTypeList) {
		// Reload the scroll view
		[self updateListScrollList];
		
		// Remove shading on current selection
		[self listShaded:NO forIndex:currentListIndex];
	} else if ([self currentViewType] == EnumViewTypeTile) {
		[tileScrollView updateScrollList];
	}
    
    [drawPadPopoverController dismissPopoverAnimated:YES];
}

#pragma mark -
#pragma mark EditListItemDelegate Methods

- (void)editScribbleForListItem:(ListItem *)listItem usingRect:(CGRect)rect {
    drawPadPopoverRect = rect;
    
    if ([editListItemPopoverController isPopoverVisible]) {
        [editListItemPopoverController dismissPopoverAnimated:YES];
    }
    
    if (drawPadPopoverController != nil) {
        drawPadPopoverController = nil;
        [drawPadPopoverController release];
    }

    
    id drawPadViewController = [[DrawPadViewController alloc] initWithDelegate:self 
                                                                       andSize:kDrawPadViewControllerSize 
                                                                   andListItem:listItem
                                                                   andPenColor:EnumPenColorBlack
                                                            andNewScribbleItem:FALSE];
    
    UINavigationController *navController;
    
    BOOL isPNGScribble = [drawPadViewController isPNGScribble];
    if (isPNGScribble) {
        // Need to release the existing drawPadViewController and load the 'old' one
        [drawPadViewController release];
        
        drawPadViewController = [[PngDrawPadViewController alloc] initWithDelegate:self 
                                                                           andSize:kPngDrawPadViewControllerSize 
                                                                       andListItem:listItem 
                                                                       andPenColor:EnumPngPenColorBlack 
                                                                andNewScribbleItem:FALSE];
        navController = [[UINavigationController alloc] initWithRootViewController:drawPadViewController];
    }
    
    //UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:drawPadViewController];
    
    
    
    
    if (isPNGScribble == FALSE) {
        drawPadPopoverController = [[UIPopoverController alloc] initWithContentViewController:drawPadViewController];
    } else {
        drawPadPopoverController = [[UIPopoverController alloc] initWithContentViewController:navController];
        [navController release];
    }		
    
    [drawPadPopoverController setDelegate:self];
    [drawPadViewController release];
    //[navController release];
    
    UIPopoverArrowDirection direction = UIPopoverArrowDirectionUp;
    
    if (rect.origin.y > 450) {
        direction = UIPopoverArrowDirectionDown;
    }
    
    [drawPadPopoverController presentPopoverFromRect:rect inView:self.view 
                            permittedArrowDirections:direction animated:YES];
    
}

- (void)editListItemAtBaseLevel:(BOOL)isBaseLevel {
	//editListItemViewControllerAtBaseLevel = isBaseLevel;
}

- (void)dismissEditListItemPopOver {
	[editListItemPopoverController dismissPopoverAnimated:YES];
}

- (void)listItem:(ListItem *)listItem wasUpdated:(BOOL)updated completedStatusChanged:(BOOL)completeChanged {
	// Do nothing as this is not called
}

- (void)listItemDue:(ListItem *)listItem wasUpdated:(BOOL)updated completedStatusChanged:(BOOL)completeChanged {
	
	// Update the master task list collection
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		NSInteger filterListItemIndex = [taskList getIndexOfListItemWithID:listItem.listItemID];
		// Make sure list item is found
		if (filterListItemIndex != -1) {
			ListItem *foundListItem = [taskList.listItems objectAtIndex:filterListItemIndex];
			[foundListItem updateSelfWithListItem:listItem];
		}
	}
	
	// Update the task list collection
	for (TaskList *taskList in taskListCollection.lists) {
		NSInteger filterListItemIndex = [taskList getIndexOfListItemWithID:listItem.listItemID];
		// Make sure list item is found
		if (filterListItemIndex != -1) {
			ListItem *foundListItem = [taskList.listItems objectAtIndex:filterListItemIndex];
			[foundListItem updateSelfWithListItem:listItem];
		}
	}
    
    [self taskCompleted:listItem.completed forListItemID:listItem.listItemID];
	
	// Reload items due table
	[itemsDueTableViewResponder reloadDataWithListParentTitle:nil andTableView:itemsDueTableView
											  andParentListID:-1 andListItem:listItem];
	
	if ([self currentViewType] == EnumViewTypeList) {
		// Reload the scroll view
		[self updateListScrollList];
		
		// Remove shading on current selection
		[self listShaded:NO forIndex:currentListIndex];
	} else if ([self currentViewType] == EnumViewTypeTile) {
		[tileScrollView updateScrollList];
	}
}

// Used for handling repeating tasks only, run it any time a task was completed using the pocket view
- (void)taskCompleted:(BOOL)isCompleted forListItemID:(NSInteger)theListItemID {
	ListItem *listItem = nil;
	ListItem *repeatingListItem = nil;
	//BOOL didRepeatTask = FALSE;

    NSInteger taskListIndex = [taskListCollection getIndexOfTaskListWithListItemID:theListItemID];
    if (taskListIndex == -1) {
        [MethodHelper showAlertViewWithTitle:@"Error" 
                                  andMessage:[NSString stringWithFormat:@"Unable to find correct task list using task (%d). Please report this error to manage@kerofrog.com.au", theListItemID] 
                              andButtonTitle:@"Ok"];
    }
    TaskList *taskList = [taskListCollection.lists objectAtIndex:taskListIndex];
    
    taskList.mainViewRequiresUpdate = TRUE;
    NSInteger listItemIndex = [taskList getIndexOfListItemWithID:theListItemID];
    if (taskListIndex == -1) {
        [MethodHelper showAlertViewWithTitle:@"Error" 
                                  andMessage:[NSString stringWithFormat:@"Unable to find correct task (%d) in task list (%d). Please report this error to manage@kerofrog.com.au", theListItemID, taskList.listID] 
                              andButtonTitle:@"Ok"];
    }
    listItem = [taskList.listItems objectAtIndex:listItemIndex];
    if (isCompleted == TRUE) {
        repeatingListItem = [taskList allocProcessRepeatingTaskAtIndex:listItemIndex];
        
        if (repeatingListItem != nil) {
            [taskList.listItems insertObject:repeatingListItem atIndex:0];
            
            // Also insert in the master task list
            TaskList *masterTaskList = [refMasterTaskListCollection getTaskListWithID:taskList.listID];
            [masterTaskList.listItems insertObject:repeatingListItem atIndex:0];
            //didRepeatTask = TRUE;
            
            // Swap list item creation date with repeating list item
            NSString *tempListItemCreationDate = listItem.creationDateTime;
            listItem.creationDateTime = repeatingListItem.creationDateTime;
            repeatingListItem.creationDateTime = tempListItemCreationDate;
            
            // Reload items due table
            [itemsDueTableViewResponder reloadDataWithListParentTitle:taskList.title andTableView:itemsDueTableView
                                                      andParentListID:taskList.listID andListItem:repeatingListItem];
            
            [repeatingListItem updateDatabase];
            [repeatingListItem release];
        }
    }
	
	if (listItem == nil) {
		// Can not find list item, do nothing
		return;
	}
	
	listItem.completed = isCompleted;
	
	if ([listItem completed] == TRUE) {
		//NSString *completedDateTimeString = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		listItem.recurringListItemID = -1;
		listItem.completedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
        listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	} else if ([listItem completed] == FALSE) {
		listItem.completedDateTime = @"";
        listItem.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	}
    
	// Update database
	[listItem updateDatabaseWithoutModifiedLog];
}


- (void)listItemWasMoved {
	// Close edit pop over
	if ([editListItemPopoverController isPopoverVisible]) {
		[editListItemPopoverController dismissPopoverAnimated:YES];
	}
	
	// Reload lists
	if ([self currentViewType] == EnumViewTypeList) {
		// Reload the scroll view
		[self updateListScrollList];
		
		// Remove shading on current selection
		[self listShaded:NO forIndex:currentListIndex];
	} else if ([self currentViewType] == EnumViewTypeTile) {
		[tileScrollView updateScrollList];
	}
}

- (void)newHandwrittenSubtaskCreatedForParentListItem:(ListItem *)parentListItem {
    // Do nothing, just here for conformance to protocol
}

#pragma mark -
#pragma mark List Date Delegate
// Note: Technically this is truly 'NewListItemDueDateDelegate' but we are reusing it for list date

- (void)updateDueDate:(NSString *)newDueDate {
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        // Update the task list date
        TaskList *taskList = [taskListCollection.lists objectAtIndex:currentListIndex];
        taskList.creationDate = newDueDate;
        [taskList updateDatabase];
        
        // Also update the master task list collection
        NSInteger taskListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:taskList.listID];
        TaskList *masterTaskList = [refMasterTaskListCollection.lists objectAtIndex:taskListIndex];
        masterTaskList.creationDate = newDueDate;
        
        // We are going to update the list date with this date
        NSDate *theDate = [MethodHelper dateFromString:newDueDate usingFormat:K_DATEONLY_FORMAT];
        [dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES]
                         forState:UIControlStateNormal];
    } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
        // Update the notebook creation date
        Notebook *notebook = [notebookCollection.notebooks objectAtIndex:currentListIndex];
        notebook.creationDate = newDueDate;
        [notebook updateDatabase];
        
        // We are going to update the notebook date with this date
        NSDate *theDate = [MethodHelper dateFromString:newDueDate usingFormat:K_DATEONLY_FORMAT];
        [dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES]
                         forState:UIControlStateNormal];
    }
	
	// Dismiss the date label popover
	if ([dateLabelPopoverController isPopoverVisible]) {
		[dateLabelPopoverController dismissPopoverAnimated:YES];
        [dateLabelPopoverController release];
	}
	
}

#pragma mark -
#pragma mark List Pad Delegates

- (void)listSelected:(NSInteger)theSelectedRow {
    // Need some sort of delay to show... use hud
  /*  */
    
    BOOL animated = YES;
    
    // Check whether we need to update the current section
    switch ([self currentViewType]) {
        case EnumViewTypeList:
            if ([self currentSection] != EnumCurrentSectionLists) {
                self.currentSection = EnumCurrentSectionLists;
                [self updateListScrollList];
                [tileScrollView updateScrollList];
                animated = NO;
                didChangeSection = TRUE;
            }
            
            [self scrollToListAtIndex:theSelectedRow animated:animated];
            break;
        case EnumViewTypeTile:
            if ([self currentSection] != EnumCurrentSectionLists) {
                self.currentSection = EnumCurrentSectionLists;
                [tileScrollView updateScrollList];
                [self updateListScrollList];
                
            }
            
            [tileScrollView selectTileAtIndex:theSelectedRow];
            
            // Need to scroll to the right location too
            [tileScrollView scrollToSelectedTile:YES];
        default:
            break;
    }
}

- (void)loadingListsSelected {
    
    progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:progressHUD];
    progressHUD.delegate = self;
    progressHUD.labelText = @"Loading Lists...";
    [progressHUD showWhileExecuting:@selector(loadLists) onTarget:self withObject:nil animated:YES];
}

- (void)loadLists {
    [self listSelected:0];
}

- (void)notebookSelected:(NSInteger)theSelectedRow {    
    BOOL animated = YES;
    
    // Check whether we need to update the current section
    switch ([self currentViewType]) {
        case EnumViewTypeList:
            if ([self currentSection] != EnumCurrentSectionNotebooks) {
                self.currentSection = EnumCurrentSectionNotebooks;
                [self updateListScrollList];
                [tileScrollView updateScrollList];
                animated = NO;
                didChangeSection = TRUE;
            }
            
            [self scrollToListAtIndex:theSelectedRow animated:animated];
            break;
        case EnumViewTypeTile:
            if ([self currentSection] != EnumCurrentSectionNotebooks) {
                self.currentSection = EnumCurrentSectionNotebooks;
                [tileScrollView updateScrollList];
                [self updateListScrollList];
            }
            
            [tileScrollView selectTileAtIndex:theSelectedRow];
        default:
            break;
    }
}

- (void)archivesSelected {
    [self archivesButtonAction];
}

- (void)shopfrontSelectedWithFrame:(CGRect)theFrame {
    [settingsBarButtonItem setEnabled:FALSE];
    [helpBarButtonItem setEnabled:FALSE];
    [viewTypeSegmentedControl setUserInteractionEnabled:FALSE];
    
    ShopFrontViewController *controller = [[[ShopFrontViewController alloc] initWithSize:CGSizeMake(430, 545)] autorelease];
    controller.delegate = self;
    
    shopFrontPopoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
    [shopFrontPopoverController setDelegate:self];
    [shopFrontPopoverController presentPopoverFromBarButtonItem:settingsBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    /*
     ControlPanelViewController *controller = [[[ControlPanelViewController alloc] initWithTableViewResponder:listPadTableViewResponder] autorelease];
     
     controller.refMainViewController = self;
     
     controlPanelPopoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
     
     [controlPanelPopoverController presentPopoverFromBarButtonItem:controlPanelBarButtonItem 
     permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
     
     */
}

- (void)deleteRowsAtIndexPaths:(NSArray *)indexPaths {
    [listPadTableView deleteRowsAtIndexPaths:indexPaths 
       withRowAnimation:UITableViewRowAnimationTop];
}

- (void)insertRowsAtIndexPaths:(NSArray *)indexPaths {
    [listPadTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
}

- (void)reloadListPadTableView {
    [listPadTableView reloadData];
}

#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kWelcomeAlertViewTag) {
        //[self reloadPreviewTableViews];
       /* progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:progressHUD];
        progressHUD.delegate = self;
        progressHUD.labelText = @"Preparing...";
        [progressHUD showWhileExecuting:@selector(reloadPreviewTableViews) onTarget:self withObject:nil animated:YES];*/
    }
}

#pragma mark -
#pragma mark AutoSync Delegate

- (void)closeAutoSyncPopover {
    autoSyncViewControllerCanClose = YES;
	[autoSyncPopoverController dismissPopoverAnimated:YES];
    [autoSyncPopoverController release];
}

- (void)autoSyncPopoverCanClose:(BOOL)canClose {
    autoSyncViewControllerCanClose = canClose;
}

#pragma mark -
#pragma mark Settings Delegate (and AutoSync)

- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData {
	if ([settingName isEqualToString:@"ShowCompleted"]) {
		refMasterTaskListCollection.showCompletedTasks = [theData boolValue];
		taskListCollection.showCompletedTasks = [theData boolValue];
		// Update collection
		[taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
        [self updateNotebooksUsingCurrentTaskListCollection];
		if ([self currentViewType] == EnumViewTypeList) {
			[self updateListScrollList];
			[self scrollToListAtIndex:currentListIndex animated:YES];
			[self listShaded:NO forIndex:currentListIndex];
		} else if (currentViewType == EnumViewTypeTile) {
			[tileScrollView updateScrollList];
		}
	} else if ([settingName isEqualToString:@"ShowCompletedDuration"]) {
		refMasterTaskListCollection.showCompletedTasksDuration = [refMasterTaskListCollection getShowCompletedDurationEnumForData:theData];
		taskListCollection.showCompletedTasksDuration = [taskListCollection getShowCompletedDurationEnumForData:theData];
		
		// Update the other collections
		[taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
		[self updateNotebooksUsingCurrentTaskListCollection];
        
		if ([self currentViewType] == EnumViewTypeList) {
			[self updateListScrollList];
			[self scrollToListAtIndex:currentListIndex animated:YES];
			[self listShaded:NO forIndex:currentListIndex];
		} else if ([self currentViewType] == EnumViewTypeTile) {
			[tileScrollView updateScrollList];
		}
	} else if ([settingName isEqualToString:@"ArchiveItems"]) {
		refMasterTaskListCollection.archiveSetting = theData;
		taskListCollection.archiveSetting = theData;
	} else if ([settingName isEqualToString:@"PocketReminder"]) {
		refMasterTaskListCollection.pocketReminder = [theData boolValue];
		taskListCollection.pocketReminder = [theData boolValue];
		
		// Now need a pocket reminder switch
		if (refMasterTaskListCollection.pocketReminder == FALSE) {
			[itemsDuePocket setHidden:YES];
			[itemsDueTableView setHidden:YES];
		} else if (refMasterTaskListCollection.pocketReminder == TRUE) {
			[itemsDuePocket setHidden:NO];
			[itemsDueTableView setHidden:NO];
		}
	} else if ([settingName isEqualToString:@"NewListOrder"]) {
		refMasterTaskListCollection.theNewListOrder = theData;
		taskListCollection.theNewListOrder = theData;
	} else if ([settingName isEqualToString:@"DefaultSortOrder"]) {
		refMasterTaskListCollection.defaultSortOrder = theData ;
		taskListCollection.defaultSortOrder = theData;
	} else if ([settingName isEqualToString:@"NewTaskOrder"]) {
		refMasterTaskListCollection.theNewTaskOrder = theData;
		taskListCollection.theNewTaskOrder = theData;
	} else if ([settingName isEqualToString:@"SelectedTheme"]) {
        // Theme is changed
        NSString *flurryEventMessage = [NSString stringWithFormat:@"Theme Selected: %@", theData];
        [FlurryAPI logEvent:flurryEventMessage];
        
		refMasterTaskListCollection.selectedTheme = theData;
		taskListCollection.selectedTheme = theData;
		[self reloadTheme];

	} else if ([settingName isEqualToString:@"VersionNumber"]) {
        // Set show completed tasks
        // set the value
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@", theData] forKey:showCompletedTasksSetting];
        
        [self reloadPreviewTableViews];
    }
    
    // Make sure that settings button, viewtype control and help button are enabled
    [settingsBarButtonItem setEnabled:YES];
    [helpBarButtonItem setEnabled:YES];
    [viewTypeSegmentedControl setUserInteractionEnabled:YES];
}

- (void)settingsPopoverCanClose:(BOOL)canClose {
	settingsViewControllerCanClose = canClose;
}

- (void)closeSettingsPopover {
	settingsViewControllerCanClose = YES;
    if (settingsPopoverController != nil) {
        [settingsPopoverController dismissPopoverAnimated:YES];
        settingsPopoverController = nil;
        [settingsPopoverController release];
    }
    
    
    // Make sure that settings button, viewtype control and help button are enabled
    [settingsBarButtonItem setEnabled:YES];
    [helpBarButtonItem setEnabled:YES];
    [viewTypeSegmentedControl setUserInteractionEnabled:YES];
}

- (void)updatesToDataComplete {
    // Duplicate the currently selected task list
    progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:progressHUD];
    progressHUD.delegate = self;
    progressHUD.labelText = @"Updating local data...";
    [progressHUD showWhileExecuting:@selector(updateLocalData) onTarget:self withObject:nil animated:YES];
    
    //[self updateLocalData];

}

- (void)replaceToodledoDataComplete {
    [taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
    [self updateNotebooksUsingCurrentTaskListCollection];
}

- (void)shopfrontSelected {
    if (settingsPopoverController != nil) {
        if ([settingsPopoverController isPopoverVisible] == TRUE) {
            [settingsPopoverController dismissPopoverAnimated:YES];
            settingsPopoverController = nil;
            [settingsPopoverController release];
        }
    }
   
    
    [settingsBarButtonItem setEnabled:FALSE];
    [helpBarButtonItem setEnabled:FALSE];
    [viewTypeSegmentedControl setUserInteractionEnabled:FALSE];
    
    ShopFrontViewController *controller = [[[ShopFrontViewController alloc] initWithSize:CGSizeMake(430, 545)] autorelease];
    controller.delegate = self;
    
    shopFrontPopoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
    [shopFrontPopoverController setDelegate:self];
    [shopFrontPopoverController presentPopoverFromBarButtonItem:settingsBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

#pragma mark -
#pragma mark Archives Delegates

- (void)archivesUpdated {
	archivesHaveBeenUpdated = TRUE;
}

- (void)archiveListsUpdated {
	archiveListsUpdated = TRUE;
}

#pragma mark -
#pragma mark PopOver Controller Delegates

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	// Need to catch when the notebook list view controller is closed, and update if notebook is dirty
    if ([popoverController isEqual:notebookListsPopoverController]) {
        // Look for any dirty notebooks in notebook collection
        for (Notebook *notebook in notebookCollection.notebooks) {
            if ([notebook isDirty]) {
                [notebook updateDatabase];
                notebook.isDirty = FALSE;
                
                [self updateNotebookListScrollViews];
                [tileScrollView updateScrollList];
            }
        }
        
        [notebookListsPopoverController release];
    
        [myToolbar setUserInteractionEnabled:YES];
    }
    
    if ([popoverController isEqual:shopFrontPopoverController]) {
        [settingsBarButtonItem setEnabled:TRUE];
        [helpBarButtonItem setEnabled:TRUE];
        [viewTypeSegmentedControl setUserInteractionEnabled:TRUE];
        [self reloadTheme];
    }
    
    if ([popoverController isEqual:settingsPopoverController]) {
        [settingsBarButtonItem setEnabled:TRUE];
        [helpBarButtonItem setEnabled:TRUE];
        [viewTypeSegmentedControl setUserInteractionEnabled:TRUE];
    }
    
    // Make sure that settings button, viewtype control and help button are enabled
    [settingsBarButtonItem setEnabled:YES];
    [helpBarButtonItem setEnabled:YES];
    [viewTypeSegmentedControl setUserInteractionEnabled:YES];
}

/* 
 Called on the delegate when the popover controller will dismiss the popover. Return NO to prevent the dismissal of the view.
 */
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    if ([popoverController isEqual:drawPadPopoverController]) {
		return NO;
	}
    
	if ([popoverController isEqual:settingsPopoverController]) {
		if (settingsViewControllerCanClose == FALSE) {
			return NO;
		}
	} else if ([popoverController isEqual:autoSyncPopoverController]) {
        if (autoSyncViewControllerCanClose == FALSE) {
            return NO;
        }
    }
	return YES;
}

#pragma mark -
#pragma mark UIViewController Delegates


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //if (firstTimeLoad) {
    //    return UIInterfaceOrientationPortrait;
    //}
    // Overriden to allow any orientation.
    return YES;
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
	// Hide the current background view first
	if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        
		// Move toolbar
		[myToolbar setFrame:kHiddenToolbarPortrait];
		
		// Items due tableview and pocket objects
		[itemsDuePocket setFrame:kMainItemsDuePocketPortrait];
		[itemsDueTableView setFrame:kMainItemsDueTableViewPortrait];
		
		// Background elements
		[canvassImage setFrame:kCanvassImagePortrait];
		[borderImage setFrame:kBorderImagePortrait];
		
		// Update the scrollview elements
		[tileScrollView setFrame:kTileScrollViewPortrait];
		[listScrollView setFrame:kMainScrollViewPortrait];
		[leftContainerView setFrame:kLeftContainerViewPortrait];
		[rightContainerView setFrame:kRightContainerViewPortrait];

		// Title text field objects
		[titleBackgroundToolbar setFrame:kTitleBackgroundToolbarPortrait];
		[titleTextField setFrame:kMainTitleTextFieldPortrait];
		[dateLabelButton setFrame:kMainDateLabelPortrait];
		
		//*titleTextField;
		//HiddenToolbar			*labelBackgroundToolbar;
		//UILabel					*dateLabel;
		
		// Control panel elements
		[controlPanelImageView setFrame:kMainControlPanelImagePortrait];
		[archivesButton setFrame:kArchivesImagePortrait];
		//[listPadImageView setFrame:kListPadImagePortrait];
		[listPadTableView setFrame:kListPadTableViewPortrait];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Portrait];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Portrait];
		[listPadFooterLine setFrame:kListPadFooterLinePortrait];
		[listPadTitleLabel setFrame:kListPadTitleLabelPortrait];

		if ([leftToolbar.items count] < 3) {
			NSMutableArray *items = [leftToolbar.items mutableCopy];
			UIBarButtonItem *spacer1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace 
																					 target:nil action:nil];
			[items insertObject:spacer1 atIndex:0];
			[spacer1 release];
			[items insertObject:controlPanelBarButtonItem atIndex:0];
			leftToolbar.items = items;
			[items release];
		}
	
		//[searchTableView setFrame:kSearchTableViewPortrait];
		//[searchBar setFrame:kSearchBarPortrait];
        // Update notebook tile scroll view frames (need to go through every notebook in scrollview
        for (UIView *topSubview in listScrollView.subviews) {
            if ([topSubview isKindOfClass:[UIButton class]]) {
                for (UIView *subview in topSubview.subviews) {
                    if ([subview isKindOfClass:[NotebookTileScrollView class]]) {
                        // Need to update orientation
                        NotebookTileScrollView *notebookTileScrollView = (NotebookTileScrollView *)subview;
                        notebookTileScrollView.notebookImageFrame = kNotebookImageFramePortrait;
                    }
                }
            }
        }
		
	} else if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
		// Move toolbar
		[myToolbar setFrame:kHiddenToolbarLandscape];
		
		// Items due tableview and pocket objects
		[itemsDuePocket setFrame:kMainItemsDuePocketLandscape];
		[itemsDueTableView setFrame:kMainItemsDueTableViewLandscape];
		
		// Update the background elements
		[canvassImage setFrame:kCanvassImageLandscape];
		[borderImage setFrame:kBorderImageLandscape];

		// Update the scrollview elements
		[tileScrollView setFrame:kTileScrollViewLandscape];
		[listScrollView setFrame:kMainScrollViewLandscape];
		[leftContainerView setFrame:kLeftContainerViewLandscape];
		[rightContainerView setFrame:kRightContainerViewLandscape];
		
		
		// Remove another 100 y pixels
		//[myToolbar setFrame:CGRectMake(myToolbar.frame.origin.x, myToolbar.frame.origin.y - 100, 
		//							   myToolbar.frame.size.width, myToolbar.frame.size.height)];
		
		//[itemsDueTableView setFrame:CGRectMake(-500, -500, 50, 50)];
		//[itemsDuePocket setFrame:CGRectMake(-500, -500, 50, 50)];
		
		
		//[itemsDueTableView setFrame:[MethodHelper moveToLandscapeLocation:itemsDueTableView.frame]];
		//[itemsDuePocket setFrame:[MethodHelper moveToLandscapeLocation:itemsDuePocket.frame]];
		
		
		// Title text field objects
		[titleBackgroundToolbar setFrame:kTitleBackgroundToolbarLandscape];
		[titleTextField setFrame:kMainTitleTextFieldLandscape];
		[dateLabelButton setFrame:kMainDateLabelLandscape];
		
		// Control panel elements
		[controlPanelImageView setFrame:kMainControlPanelImageLandscape];
		[archivesButton setFrame:kArchivesImageLandscape];
		//[listPadImageView setFrame:kListPadImageLandscape];
		[listPadTableView setFrame:kListPadTableViewLandscape];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Landscape];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Landscape];
		[listPadFooterLine setFrame:kListPadFooterLineLandscape];
		[listPadTitleLabel setFrame:kListPadTitleLabelLandscape];
		
		
		if ([[leftToolbar.items objectAtIndex:0] isEqual:controlPanelBarButtonItem]) {
			NSMutableArray *items = [leftToolbar.items mutableCopy];
			[items removeObject:controlPanelBarButtonItem];
			[items removeObjectAtIndex:0];
			leftToolbar.items = items;
			[items release];
		}
		//[searchBar setFrame:kSearchBarLandscape];
        
        // Update notebook tile scroll view frames (need to go through every notebook in scrollview
        for (UIView *topSubview in listScrollView.subviews) {
            if ([topSubview isKindOfClass:[UIButton class]]) {
                for (UIView *subview in topSubview.subviews) {
                    if ([subview isKindOfClass:[NotebookTileScrollView class]]) {
                        // Need to update orientation
                        NotebookTileScrollView *notebookTileScrollView = (NotebookTileScrollView *)subview;
                        notebookTileScrollView.notebookImageFrame = kNotebookImageFrameLandscape;
                    }
                }
            }
        }
	}
	
	// Commit
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	// Get rid of help overlay if it is there
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
	
	
	// Get rid of control panel popover controller if it is visible
	if ([controlPanelPopoverController isPopoverVisible]) {
		[controlPanelPopoverController dismissPopoverAnimated:NO];
        controlPanelPopoverController = nil;
        [controlPanelPopoverController release];
	}
	
	[titleTextField setHidden:YES];
	[dateLabelButton setHidden:YES];
	[myToolbar setHidden:YES];
	
    // Rotate ad view
	[_adView rotateToOrientation:toInterfaceOrientation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	if ([self currentViewType] == EnumViewTypeList) {
		[titleTextField setHidden:NO];
		[dateLabelButton setHidden:NO];
		[myToolbar setHidden:NO];
	}
	
	if ([editListItemPopoverController isPopoverVisible]) {
		[editListItemPopoverController dismissPopoverAnimated:NO];
	}
	
    if ([autoSyncPopoverController isPopoverVisible]) {
        CGRect popoverRect = kAutoSyncRectLandscape;
        if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
            popoverRect = kAutoSyncRectPortrait;
		}
		
		[autoSyncPopoverController presentPopoverFromRect:popoverRect
                                                  inView:self.view 
                                permittedArrowDirections:0 
                                                animated:YES];
	}
    
    if ([drawPadPopoverController isPopoverVisible]) {
        if (UIInterfaceOrientationIsPortrait(fromInterfaceOrientation)) {
            // Now is landscape
            drawPadPopoverRect = CGRectMake(drawPadPopoverRect.origin.x + 256, 
                                            drawPadPopoverRect.origin.y - 256, 
                                            drawPadPopoverRect.size.width, 
                                            drawPadPopoverRect.size.height);
        } else if (UIInterfaceOrientationIsLandscape(fromInterfaceOrientation)) {
            // Now is portrait
            drawPadPopoverRect = CGRectMake(drawPadPopoverRect.origin.x - 256, 
                                            drawPadPopoverRect.origin.y + 256, 
                                            drawPadPopoverRect.size.width, 
                                            drawPadPopoverRect.size.height);
        }
        
        [drawPadPopoverController presentPopoverFromRect:drawPadPopoverRect inView:self.view 
                                permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    
	// Hide the current background view first
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {

	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		
	}
    
    // Obtain the new size of the ad content.
    CGSize size = [_adView adContentViewSize];
    
    // Repositioning example: keep the ad view centered horizontally.
    CGRect newFrame = _adView.frame;	
    newFrame.size = size;
    newFrame.origin.x = (self.view.bounds.size.width - size.width) / 2;	
    newFrame.origin.y = self.view.bounds.size.height - size.height;
    _adView.frame = newFrame;
}

- (void)viewDidDisappear:(BOOL)animated {
	// Re-enable list button clicking
	listButtonClickDisabled = FALSE;
	
	[super viewDidDisappear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	// Get rid of help overlay if it is there
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
	
	[super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
    if (isShowCompletedTasks == 0) {
        [self reloadPreviewTableViews];
    }
    
	[self reloadTheme];
    
	if (firstTimeRun) {
		firstTimeRun = FALSE;
		return;
	}

	// Hide pocket reminder if needed
	if (refMasterTaskListCollection.pocketReminder == FALSE) {
		[itemsDuePocket setHidden:YES];
		[itemsDueTableView setHidden:YES];
	} else if (refMasterTaskListCollection.pocketReminder == TRUE) {
		[itemsDuePocket setHidden:NO];
		[itemsDueTableView setHidden:NO];
	}
	
	// Hide the current background view first
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		// Move toolbar
		[myToolbar setFrame:kHiddenToolbarPortrait];
		
		// Items due tableview and pocket objects
		[itemsDuePocket setFrame:kMainItemsDuePocketPortrait];
		[itemsDueTableView setFrame:kMainItemsDueTableViewPortrait];
		
		// Background elements
		[canvassImage setFrame:kCanvassImagePortrait];
		[borderImage setFrame:kBorderImagePortrait];
		
		// Update the scrollview elements
		[tileScrollView setFrame:kTileScrollViewPortrait];
		[listScrollView setFrame:kMainScrollViewPortrait];
		[leftContainerView setFrame:kLeftContainerViewPortrait];
		[rightContainerView setFrame:kRightContainerViewPortrait];
		
		// Title text field objects
		[titleBackgroundToolbar setFrame:kTitleBackgroundToolbarPortrait];
		[titleTextField setFrame:kMainTitleTextFieldPortrait];
		[dateLabelButton setFrame:kMainDateLabelPortrait];
		
		//*titleTextField;
		//HiddenToolbar			*labelBackgroundToolbar;
		//UILabel					*dateLabel;
		
		// Control panel elements
		[controlPanelImageView setFrame:kMainControlPanelImagePortrait];
		[archivesButton setFrame:kArchivesImagePortrait];
		//[listPadImageView setFrame:kListPadImagePortrait];
		[listPadTableView setFrame:kListPadTableViewPortrait];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Portrait];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Portrait];
		[listPadFooterLine setFrame:kListPadFooterLinePortrait];
		[listPadTitleLabel setFrame:kListPadTitleLabelPortrait];
		//[searchTableView setFrame:kSearchTableViewPortrait];
		//[searchBar setFrame:kSearchBarPortrait];
		
        // Update notebook tile scroll view frames (need to go through every notebook in scrollview
        for (UIView *topSubview in listScrollView.subviews) {
            if ([topSubview isKindOfClass:[UIButton class]]) {
                for (UIView *subview in topSubview.subviews) {
                    if ([subview isKindOfClass:[NotebookTileScrollView class]]) {
                        // Need to update orientation
                        NotebookTileScrollView *notebookTileScrollView = (NotebookTileScrollView *)subview;
                        notebookTileScrollView.notebookImageFrame = kNotebookImageFramePortrait;
                    }
                }
            }
        }
        
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		// Move toolbar
		[myToolbar setFrame:kHiddenToolbarLandscape];
		
		// Items due tableview and pocket objects
		[itemsDuePocket setFrame:kMainItemsDuePocketLandscape];
		[itemsDueTableView setFrame:kMainItemsDueTableViewLandscape];
		
		// Update the background elements
		[canvassImage setFrame:kCanvassImageLandscape];
		[borderImage setFrame:kBorderImageLandscape];
		
		// Update the scrollview elements
		[tileScrollView setFrame:kTileScrollViewLandscape];
		[listScrollView setFrame:kMainScrollViewLandscape];
		[leftContainerView setFrame:kLeftContainerViewLandscape];
		[rightContainerView setFrame:kRightContainerViewLandscape];
		
		
		// Remove another 100 y pixels
		//[myToolbar setFrame:CGRectMake(myToolbar.frame.origin.x, myToolbar.frame.origin.y - 100, 
		//							   myToolbar.frame.size.width, myToolbar.frame.size.height)];
		
		//[itemsDueTableView setFrame:CGRectMake(-500, -500, 50, 50)];
		//[itemsDuePocket setFrame:CGRectMake(-500, -500, 50, 50)];
		
		
		//[itemsDueTableView setFrame:[MethodHelper moveToLandscapeLocation:itemsDueTableView.frame]];
		//[itemsDuePocket setFrame:[MethodHelper moveToLandscapeLocation:itemsDuePocket.frame]];
		
		
		// Title text field objects
		[titleBackgroundToolbar setFrame:kTitleBackgroundToolbarLandscape];
		[titleTextField setFrame:kMainTitleTextFieldLandscape];
		[dateLabelButton setFrame:kMainDateLabelLandscape];
		
		// Control panel elements
		[controlPanelImageView setFrame:kMainControlPanelImageLandscape];
		[archivesButton setFrame:kArchivesImageLandscape];
		//[listPadImageView setFrame:kListPadImageLandscape];
		[listPadTableView setFrame:kListPadTableViewLandscape];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Landscape];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Landscape];
		[listPadFooterLine setFrame:kListPadFooterLineLandscape];
		[listPadTitleLabel setFrame:kListPadTitleLabelLandscape];
		//[searchTableView setFrame:kSearchTableViewLandscape];
		//[searchBar setFrame:kSearchBarLandscape];
        
        // Update notebook tile scroll view frames (need to go through every notebook in scrollview
        for (UIView *topSubview in listScrollView.subviews) {
            if ([topSubview isKindOfClass:[UIButton class]]) {
                for (UIView *subview in topSubview.subviews) {
                    if ([subview isKindOfClass:[NotebookTileScrollView class]]) {
                        // Need to update orientation
                        NotebookTileScrollView *notebookTileScrollView = (NotebookTileScrollView *)subview;
                        notebookTileScrollView.notebookImageFrame = kNotebookImageFrameLandscape;
                    }
                }
            }
        }
	}
	
	
	// Only update the current list index... makes sense (no need to reload)
	// NOTE: Have to reload if the user has completed the task list and has set completed tasks visible to false
	//[taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
	
	if ([taskListCollection.lists count] > 0 && archivesHaveBeenUpdated == FALSE && archivesVisited == FALSE) {
		// Remove any completed lists that should be removed
		/*for (NSInteger i = [taskListCollection.lists count] - 1; i >= 0; i--) {
			TaskList *taskList = [taskListCollection.lists objectAtIndex:i];
			if ([taskList completed] == TRUE && refMasterTaskListCollection.showCompletedTasks == FALSE) {
				[taskListCollection.lists removeObjectAtIndex:i];
			}
		}*/
		
		// Reset current list index if it exceeds the task list collection count
        
        switch ([self currentSection]) {
            case EnumCurrentSectionLists:
            case EnumCurrentSectionNotebookLists:
                if (currentListIndex >= [taskListCollection.lists count] && [taskListCollection.lists count] > 0) {
                    currentListIndex = [taskListCollection.lists count] - 1;
                } else if ([taskListCollection.lists count] == 0) {
                    return;
                }
                break;
            case EnumCurrentSectionNotebooks:
                if (currentListIndex >= [notebookCollection.notebooks count] && [notebookCollection.notebooks count] > 0) {
                    currentListIndex = [notebookCollection.notebooks count] - 1;
                } else if ([notebookCollection.notebooks count] == 0) {
                    return;
                }
                break;
            default:
                break;
        }
		
		
		NSInteger originalListIndex = currentListIndex;
		// Might not need to call 'updatelistscrolllist'
		//[self updateListScrollList];
		
		currentListIndex = originalListIndex;
		[self scrollToListAtIndex:currentListIndex animated:YES];
		
		// Update all preview tableviews
        if ([self currentSection] == EnumCurrentSectionLists) {
            [self updatePreviewTableViews:NO];
        } else if ([self currentSection] == EnumCurrentSectionNotebooks || [self currentSection] == EnumCurrentSectionNotebookLists) {
            [self updateListScrollList];
            [tileScrollView updateScrollList];
            [self scrollToListAtIndex:currentListIndex animated:NO];
        }
		
		// Get the title for the current list (or notebook)
        if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
            if (currentListIndex >= 0 && currentListIndex < [taskListCollection.lists count]) {
                TaskList *list = [taskListCollection.lists objectAtIndex:currentListIndex];
                // Update title text field
                titleTextField.text = list.title;
            }
        } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
            if (currentListIndex >= 0 && currentListIndex < [notebookCollection.notebooks count]) {
                Notebook *notebook = [notebookCollection.notebooks objectAtIndex:currentListIndex];
                // Update title text field
                titleTextField.text = notebook.title;
            }
        }
        
		
		if ([refMasterTaskListCollection pocketReminder] == TRUE) {
			// Reload items due
			[itemsDueTableViewResponder reloadDataWithListParentTitle:nil andTableView:itemsDueTableView andParentListID:-1 andListItem:nil];
		}
		
		if ([self currentViewType] == EnumViewTypeTile) {
			
			[tileScrollView updateScrollList];
			self.title = @"My Lists & Notes";
		}
		
	} else if (archivesHaveBeenUpdated == TRUE && archiveListsUpdated == TRUE) {
		[taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
        [self updateNotebooksUsingCurrentTaskListCollection];
        
		if ([self currentViewType] == EnumViewTypeList) {
			// Reload the scroll view
			[self updateListScrollList];
			
			// Update title and date
			[self updateTitleAndDateForSelectedIndex];
			
			// Remove shading on current selection
			[self listShaded:NO forIndex:currentListIndex];
		} else if ([self currentViewType] == EnumViewTypeTile) {
			// Need to reload scroll view too, to prevent deallocated task list in preview responder
			//[self updateListScrollList];
			
			[tileScrollView updateScrollList];
		}
		archivesHaveBeenUpdated = FALSE;
		archivesHaveBeenUpdated = FALSE;
	} else if (archivesHaveBeenUpdated == TRUE && archiveListsUpdated == FALSE) {
		
		[taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
		[self updateNotebooksUsingCurrentTaskListCollection];
        
		if ([self currentViewType] == EnumViewTypeList) {
			[self updatePreviewTableViews:NO];
		} else if (currentViewType == EnumViewTypeTile) {
			[tileScrollView updateScrollList];
		}
		
		archivesHaveBeenUpdated = FALSE;
	}
	
	archivesVisited = FALSE;
	
    
    
	[super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated {
    // Check if is first load
    if (firstTimeLoad == TRUE) {
        // Deleting the currently selected task list
        progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:progressHUD];
        progressHUD.delegate = self;
        progressHUD.labelText = @"Loading...";
        [progressHUD showWhileExecuting:@selector(runStartupCode) onTarget:self withObject:nil animated:YES];
    }
    
	// Add first spacer
	if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		if ([[leftToolbar.items objectAtIndex:0] isEqual:controlPanelBarButtonItem]) {
			NSMutableArray *items = [leftToolbar.items mutableCopy];
			[items removeObject:controlPanelBarButtonItem];
			[items removeObjectAtIndex:0];
			leftToolbar.items = items;
			[items release];
		}
	} else if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		if ([leftToolbar.items count] < 3) {
			NSMutableArray *items = [leftToolbar.items mutableCopy];
			UIBarButtonItem *spacer1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace 
																					 target:nil action:nil];
			[items insertObject:spacer1 atIndex:0];
			[spacer1 release];
			[items insertObject:controlPanelBarButtonItem atIndex:0];
			leftToolbar.items = items;
			[items release];
		}
	}
	
	[listPadTableView reloadData];

    // Check for syncing
    [self checkForToodledoAutoSync];
    
	[super viewDidAppear:animated];
}

/*
 [self insertNewListButton];
 
 if (currentViewType == EnumViewTypeList) {
 [self listShaded:YES forIndex:currentListIndex];
 
 // Scroll to the new list
 [self scrollToListAtIndex:0 animated:YES];
 
 if ([taskListCollection.lists count] == 1) {
 [self updateTitleAndDateForSelectedIndex];
 }
 } else if (currentViewType == EnumViewTypeTile) {
 [self commitViewType];
 }
 
 
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
 
- (void)viewDidUnload {
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    
}


- (void)applicationDidBecomeActive {
    if ([self.navigationController.topViewController isEqual:self] == TRUE) {
        [self performSelector:@selector(checkForToodledoAutoSync) withObject:nil afterDelay:0.2];
    }
}


- (void)checkForToodledoAutoSync {
    // Run this if sync auto is set
    if (gRunToodledoAutoSync == TRUE) {
        AutoSyncViewController *controller = [[AutoSyncViewController alloc] initWithMasterTaskListCollection:refMasterTaskListCollection];
        controller.delegate = self;
        
        // Init the navigation controller that will hold the edit list item view controller
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
        [controller release];
        
        autoSyncPopoverController = [[UIPopoverController alloc] initWithContentViewController:navController];
        [autoSyncPopoverController setDelegate:self];
        [navController release];
        
        CGRect popoverRect = kAutoSyncRectLandscape;
        if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
            popoverRect = kAutoSyncRectPortrait;
        }
        
        [autoSyncPopoverController presentPopoverFromRect:popoverRect inView:self.view permittedArrowDirections:0 animated:NO];
        
        gRunToodledoAutoSync = FALSE;
    }
}

#pragma mark -
#pragma mark Text Field Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	// Disable the hidden text field toolbar
	[titleTextField setAlpha:0.0f];
	[titleTextField setBorderStyle:UITextBorderStyleRoundedRect];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1.0];
	
	[titleTextField setBorderStyle:UITextBorderStyleRoundedRect];
	[titleTextField setAlpha:1.0f];
	[titleTextField setUserInteractionEnabled:TRUE];
	[titleTextField setTextColor:[UIColor darkTextColor]];
	[titleTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
	[titleTextField setAutocapitalizationType:UITextAutocapitalizationTypeWords];
	//[titleTextField setNeedsDisplay];
	[titleBackgroundToolbar setHidden:YES];
	
	// Disable scroll view
	[listScrollView setUserInteractionEnabled:FALSE];
	[leftContainerView setUserInteractionEnabled:FALSE];
	[rightContainerView setUserInteractionEnabled:FALSE];
	
	// Update top right button with done button item
	
	
	[UIView commitAnimations];
	
	// Hide top left button
	
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	// Re-enable the hidden text field toolbar
	[titleTextField setBorderStyle:UITextBorderStyleNone];
	[titleTextField setUserInteractionEnabled:FALSE];
	[titleTextField setTextColor:[refMasterTaskListCollection getColourForSection:@"MainViewTitleTextField"]];
	[titleTextField setNeedsDisplay];
	[titleBackgroundToolbar setHidden:NO];
	
	// Enable scroll view
	[listScrollView setUserInteractionEnabled:TRUE];
	[leftContainerView setUserInteractionEnabled:TRUE];
	[rightContainerView setUserInteractionEnabled:TRUE];
	
	// Check for any changes
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        if (currentListIndex < 0 || currentListIndex >= [taskListCollection.lists count]) {
            [MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Current list index outside of bounds, please report error to manage@kerofrog.com.au" andButtonTitle:@"Ok"];
        }
        
        TaskList *list = [taskListCollection.lists objectAtIndex:currentListIndex];
        
        // Only do something if the list.title is not equal to the title field (do not allow empty title)
        if (![list.title isEqualToString:titleTextField.text] && [titleTextField.text length] > 0) {
            if ([list.title isEqualToString:@"Toodledo Tasks"] == TRUE || [titleTextField.text isEqualToString:@"Toodledo Tasks"] == TRUE) {
                // Warn user that this is reserved list name
                [MethodHelper showAlertViewWithTitle:@"Reserved Name" 
                                          andMessage:@"This list title (Toodledo Tasks) can not be changed and is reserved for Toodledo tasks with no folder" 
                                      andButtonTitle:@"Ok"];
                [titleTextField setText:list.title];
            } else {
                // Update the list title
                [list setTitle:titleTextField.text];
                
                for (ListItem *listItem in list.listItems) {
                    [listItem setParentListTitle:titleTextField.text];
                }
                
                // [list updateDatabase];
                [list updateDatabase];
                
                // Also update the master task list collection
                NSInteger listIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:list.listID];
                TaskList *masterTaskList = [refMasterTaskListCollection.lists objectAtIndex:listIndex];
                [masterTaskList setTitle:titleTextField.text];
                
                
                for (ListItem *listItem in masterTaskList.listItems) {
                    [listItem setParentListTitle:titleTextField.text];
                }
    
                
                /*
                 // Get task list in master task list and update
                 for (TaskList *taskList in refMasterTaskListCollection.lists) {
                 if (taskList.listID == currentTaskListID) {
                 taskList.title = [newListTitle copy];
                 for (ListItem *listItem in taskList.listItems) {
                 listItem.parentListTitle = [newListTitle copy];
                 }
                 self.title = newListTitle;
                 [taskList updateDatabase];
                 }
                 }
                 
                 
                 */
                
                // Also update list pad table view
                [listPadTableView reloadData];
            }
        } else {
            [titleTextField setText:list.title];
        } 
    } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
        if (currentListIndex < 0 || currentListIndex >= [notebookCollection.notebooks count]) {
            [MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Current notebook index outside of bounds, please report error to manage@kerofrog.com.au" andButtonTitle:@"Ok"];
        }
        
        Notebook *notebook = [notebookCollection.notebooks objectAtIndex:currentListIndex];
        
        // Only do something if the notebook.title is not equal to the title field (do not allow empty title)
        if (![notebook.title isEqualToString:titleTextField.text] && [titleTextField.text length] > 0) {
            // Update the list title
            [notebook setTitle:titleTextField.text];
            
            // [list updateDatabase];
            [notebook updateDatabase];
            
            // Also update list pad table view
            [listPadTableView reloadData];

        } else {
            [titleTextField setText:notebook.title];
        } 
    }
    
	
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[titleTextField resignFirstResponder];
	
	return YES;
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
	
	// Set up the cell...
	
    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return 0;
}

#pragma mark - ListViewDelegate Methods

- (void)swapWithNote:(TaskList *)noteTaskList usingFrame:(CGRect)frame {
    // Curl animation has happened, now we just need to load note and swap the views
    NSString *backButtonTitle = @"My Lists & Notes";
    if ([self currentSection] == EnumCurrentSectionNotebooks) {
        backButtonTitle = @"My Folders";
    }
    
    NoteViewController *noteViewController = [[[NoteViewController alloc] 
                                       initWithMasterTaskListCollection:refMasterTaskListCollection 
                                       andTaskList:noteTaskList 
                                       andBackButtonTitle:backButtonTitle] autorelease];
    noteViewController.tileViewFrame = frame;
    noteViewController.delegate = self;
    
    
    // Now need to swap with the top controller, which is list.
    NSArray *theViewControllers = [self.navigationController viewControllers];
    NSMutableArray *mutableViewControllers = [NSMutableArray arrayWithArray:theViewControllers];
    [mutableViewControllers removeLastObject];  // Get rid of our list view controller
    // Now need to add a new last object, which is our note view controller
    [mutableViewControllers addObject:noteViewController];
    [self.navigationController setViewControllers:mutableViewControllers animated:NO];
}

#pragma mark - NoteViewDelegate Methods

- (void)swapWithList:(TaskList *)listTaskList usingFrame:(CGRect)frame {
    // Curl animation has happened, now we just need to load note and swap the views
    NSString *backButtonTitle = @"My Lists & Notes";
    if ([self currentSection] == EnumCurrentSectionNotebooks) {
        backButtonTitle = @"My Folders";
    }
    
    myListViewController = [[ListViewController alloc] initWithTaskList:listTaskList andTagCollection:tagCollection
                                                                     andTaskListCollection:taskListCollection
                                                               andMasterTaskListCollection:refMasterTaskListCollection
                                                                        andBackButtonTitle:backButtonTitle];
    myListViewController.delegate = self;
    myListViewController.tileViewFrame = frame;
    
    
    // Now need to swap with the top controller, which is a note.
    NSArray *theViewControllers = [self.navigationController viewControllers];
    
    NSMutableArray *mutableViewControllers = [NSMutableArray arrayWithArray:theViewControllers];
    //nmthuong
//    NSMutableArray *tempMutableViewControllers = [[NSMutableArray alloc] initWithArray:theViewControllers];
//    NSMutableArray *mutableViewControllers = tempMutableViewControllers;
//    [tempMutableViewControllers release];
    [mutableViewControllers removeLastObject];  // Get rid of our list view controller
    // Now need to add a new last object, which is our note view controller
    [mutableViewControllers addObject:myListViewController];
    myListViewController = nil;
    [myListViewController release];
    [self.navigationController setViewControllers:mutableViewControllers animated:NO];
}

#pragma mark -
#pragma mark Animation Delegates

// Animation has stopped
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
	//[[self superview] removeFromSuperview];

    NSString *backButtonTitle = @"My Lists & Notes";
    if ([self currentSection] == EnumCurrentSectionNotebooks) {
        backButtonTitle = @"My Folders";
    }
    
    NSLog(@"m1");
    
	if ([animationID isEqualToString:@"Delete List Animation"]) {
		[UIView beginAnimations:@"Slide Replace Deleted List Animation" context:nil];
		[UIView setAnimationDuration:0.5f];
		[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
		[UIView setAnimationDelegate:self];
		
		// Call slide lists
		[self slideListToFillIndex:currentListIndex];
		
		[UIView commitAnimations];
		NSLog(@"m2");
	} else if ([animationID isEqualToString:@"Zoom List Animation"]) {
        // Need to get selected list index from noteboo
		TaskList *taskList = [taskListCollection.lists objectAtIndex:selectedListIndex];
		NSLog(@"m3");
		// Push the new view controller, without an animation
        if ([taskList isNote] == FALSE) {
            
            myListViewController = [[ListViewController alloc] initWithTaskList:taskList andTagCollection:tagCollection
                                                                     andTaskListCollection:taskListCollection
                                                               andMasterTaskListCollection:refMasterTaskListCollection
                                                                        andBackButtonTitle:backButtonTitle];
            myListViewController.delegate = self;
            [self.navigationController pushViewController:myListViewController animated:NO];
            myListViewController = nil;
            [myListViewController release];
            NSLog(@"m4");
        } else if ([taskList isNote] == TRUE) {
            NoteViewController *controller = [[NoteViewController alloc] 
                                               initWithMasterTaskListCollection:refMasterTaskListCollection
                                               andTaskList:taskList andBackButtonTitle:backButtonTitle];
            controller.delegate = self;
            [self.navigationController pushViewController:controller animated:NO];
            //nmthuong remove autorelease
            [controller release];
            NSLog(@"m5");
        }
		
		// Remove the animated list image
		[animatedListImage removeFromSuperview];
		[animatedListImage release];
		
		// Bring control panel views back to front
		[self.view bringSubviewToFront:controlPanelImageView];
		[self.view bringSubviewToFront:archivesButton];
		//[self.view bringSubviewToFront:listPadImageView];
		[self.view bringSubviewToFront:listPadTableView];
		[self.view bringSubviewToFront:listPadHeaderLine1];
		[self.view bringSubviewToFront:listPadHeaderLine2];
		[self.view bringSubviewToFront:listPadFooterLine];
		[self.view bringSubviewToFront:listPadTitleLabel];
		[self.view bringSubviewToFront:titleTextField];
		[self.view bringSubviewToFront:dateLabelButton];
		[self.view bringSubviewToFront:myToolbar];
		
	} else if ([animationID isEqualToString:@"Zoom Tile Animation"]) {
        NSLog(@"m6");
        
        // Need to get the notebook, then get the selected index
        TaskList *taskList = nil;
        if ([self currentSection] == EnumCurrentSectionLists) {
            taskList = [taskListCollection.lists objectAtIndex:tileScrollView.currentListIndex];
        } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
            if ([self selectedNotebookListID] != -1) {
                NSInteger listIndex = [taskListCollection getIndexOfTaskListWithID:[self selectedNotebookListID]];
                taskList = [taskListCollection.lists objectAtIndex:listIndex];
            }
        }
		
        
		if ([taskList isNote] == FALSE) {
            // Push the new view controller, without an animation
            //Tan Nguyen Fix leaks
            if (myListViewController == nil) {
                myListViewController = [[ListViewController alloc] initWithTaskList:taskList andTagCollection:tagCollection
                                                              andTaskListCollection:taskListCollection
                                                        andMasterTaskListCollection:refMasterTaskListCollection
                                                                 andBackButtonTitle:backButtonTitle];
            }
            
            myListViewController.delegate = self;
            if ([self currentViewType] == EnumViewTypeTile && [self currentSection] == EnumCurrentSectionLists) {
                myListViewController.tileViewFrame = tileScrollView.touchedViewFrame;
            } else if ([self currentViewType] == EnumViewTypeList && [self currentSection] == EnumCurrentSectionNotebooks) {
                myListViewController.tileViewFrame = notebookCollection.notebookListTileFrame;
            }
            
            [self.navigationController pushViewController:myListViewController animated:NO]; 
            myListViewController = nil;
            [myListViewController release];
        } else if ([taskList isNote]) {
            NoteViewController *controller = [[[NoteViewController alloc] 
                                               initWithMasterTaskListCollection:refMasterTaskListCollection
                                               andTaskList:taskList andBackButtonTitle:backButtonTitle] autorelease];
            controller.delegate = self;
            if ([self currentViewType] == EnumViewTypeTile && [self currentSection] == EnumCurrentSectionLists) {
                controller.tileViewFrame = tileScrollView.touchedViewFrame;
            } else if ([self currentViewType] == EnumViewTypeList && [self currentSection] == EnumCurrentSectionNotebooks) {
                controller.tileViewFrame = notebookCollection.notebookListTileFrame;
            }
            [self.navigationController pushViewController:controller animated:NO];
        }
		
		
		// Remove the animated list image
		[animatedListImage removeFromSuperview];
		[animatedListImage release];
		
		// Remove touch disabled in tile scroll view
		tileScrollView.tileButtonTouchDisabled = FALSE;
		
	} else if ([animationID isEqualToString:@"Slide Replace Deleted List Animation"]) {
        // Deleting the currently selected task list
        progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:progressHUD];
        progressHUD.delegate = self;
        progressHUD.labelText = @"Removing task list...";
        [progressHUD showWhileExecuting:@selector(deleteListAtCurrentIndex) onTarget:self withObject:nil animated:YES];
	}
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidden
    
    [progressHUD removeFromSuperview];
    [progressHUD release];
    
    
    if (needToReloadPreviewTableViews) {
        [self reloadPreviewTableViews];
        needToReloadPreviewTableViews = FALSE;
    }
    
    if (commitViewTypeHUDRunning) {
        [self listShaded:NO forIndex:currentListIndex];
        commitViewTypeHUDRunning = FALSE;
    }
    
}


#pragma mark -
#pragma mark Send Mail Methods

- (void)sendTextMailForTaskList:(TaskList *)theTaskList {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
		[mailAlert release];
		return;
	}
	
	//NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	//NSString *tempDocumentsPath = [searchPaths objectAtIndex: 0];
	//tempDocumentsPath = [NSString stringWithFormat:@"%@/DappTemp/", tempDocumentsPath];
	
	//NSString *attachmentPath = [NSString stringWithFormat:@"%@%@", tempDocumentsPath, attachment];
	
	//NSData *dataObj = [NSData dataWithContentsOfFile:attachmentPath];
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	[controller setMessageBody:[theTaskList getTextMailMessageWithTagCollection:tagCollection showSubtasks:TRUE] isHTML:NO];
	
	[controller setSubject:[NSString stringWithFormat:@"Manage Task List: %@", theTaskList.title]];
	
	//ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithDataForName:@"EmailToAddress"];
	//[controller setToRecipients:[NSArray arrayWithObject:appSetting.data]];
	//[controller setMessageBody:@"Test body" isHTML:NO];
	//[appSetting release];
	
	//[controller setMessageBody:@"" isHTML:NO];
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	//[controller addAttachmentData:dataObj 
	//					 mimeType:@"application/zip" fileName:attachment];
	
	[self presentModalViewController:controller animated:YES];
	[controller release];
}

- (void)sendPDFMailForTaskList:(TaskList *)theTaskList {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
		[mailAlert release];
		return;
	}
	
	NSString *pdfPath = @"";
    NSString *initialTitle = @"Manage Task List";
    
    if ([theTaskList isNote] == FALSE) {
        pdfPath = [theTaskList createPDFMessageWithTagCollection:tagCollection showSubtasks:YES];
    } else if ([theTaskList isNote]) {
        initialTitle = @"Manage Note";
        pdfPath = [theTaskList createPDFMessageFromNote];
    }
    
	
	NSData *dataObj = [NSData dataWithContentsOfFile:pdfPath];
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	
	[controller setSubject:[NSString stringWithFormat:@"%@: %@", initialTitle, theTaskList.title]];
	
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	[controller addAttachmentData:dataObj 
						 mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"%@.pdf", initialTitle]];
	
	[self presentModalViewController:controller animated:YES];
	[controller release];
}

- (void)sendPDFMailForNote:(TaskList *)theTaskList {
	// Make sure this device can send mail
	if ([MFMailComposeViewController canSendMail] == FALSE) {
		// Display result from mail composer
		UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Error"
															message: @"This device is not configured for sending mail"
														   delegate: nil
												  cancelButtonTitle: @"Ok"
												  otherButtonTitles: nil];
		[mailAlert show];
		[mailAlert release];
		return;
	}
	
	NSString *pdfPath = [theTaskList createPDFMessageWithTagCollection:tagCollection showSubtasks:YES];
	
	NSData *dataObj = [NSData dataWithContentsOfFile:pdfPath];
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	
	controller.mailComposeDelegate = self;
	
	[controller setSubject:[NSString stringWithFormat:@"Manage Note: %@", theTaskList.title]];
	
	
	controller.navigationBar.barStyle = UIBarStyleDefault;
	controller.wantsFullScreenLayout = YES;
	
	[controller addAttachmentData:dataObj 
						 mimeType:@"application/pdf" fileName:@"Manage Note.pdf"];
	
	[self presentModalViewController:controller animated:YES];
	[controller release];
}
 
	 
#pragma mark -
#pragma mark Mail Composer View Delegates

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	NSString *message = @"";
	
	switch (result) {
		case MFMailComposeResultCancelled:
			message = @"Email Cancelled";
			
			break;
		case MFMailComposeResultSaved:
			message = @"Email Cancelled";
			break;
		case MFMailComposeResultSent:
			message = @"Email Sent";
			break;
		case MFMailComposeResultFailed:
			message = @"Email Failed";
			break;
		default:
			break;
	}
	
	// Display result from mail composer
	UIAlertView *mailAlert = [[UIAlertView alloc] initWithTitle: @"Mail Composer"
														message: message
													   delegate: nil
											  cancelButtonTitle: @"Ok"
											  otherButtonTitles: nil];
	[mailAlert show];
	[mailAlert release];
	
	[self becomeFirstResponder];
	[self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Shop Front Delegate Methods

- (void)previewTheme:(NSString *)themeName {
    refMasterTaskListCollection.previewTheme = themeName;
    
    NSString *canvasImageName = [refMasterTaskListCollection getPreviewImageNameForSection:@"Canvas"];
    UIImage *newCanvasImage = [UIImage imageNamed:canvasImageName];
    [canvassImage setImage:newCanvasImage];
    
    NSString *borderImageName = [refMasterTaskListCollection getPreviewImageNameForSection:@"Border"];
    UIImage *newBorderImage = [UIImage imageNamed:borderImageName];
    [borderImage setImage:newBorderImage];
    
    NSString *pocketImageName = [refMasterTaskListCollection getPreviewImageNameForSection:@"Pocket"];
    UIImage *newPocketImage = [UIImage imageNamed:pocketImageName];
    [itemsDuePocket setImage:newPocketImage];
    
    // Get tile view type
    NSString *buttonType = [refMasterTaskListCollection getPreviewTileViewButtonType];
    [tileScrollView updateButtonTypes:buttonType];
    
    // Get colours
    titleTextField.textColor = [refMasterTaskListCollection getPreviewColourForSection:@"MainViewTitleTextField"];
    [dateLabelButton setTitleColor:[refMasterTaskListCollection getPreviewColourForSection:@"MainViewDateLabelButton"] forState:UIControlStateNormal];
    
    itemsDueTableViewResponder.textLabelColour = [refMasterTaskListCollection getPreviewColourForSection:@"ItemsDueTextLabel"];
    itemsDueTableViewResponder.detailTextLabelColour = [refMasterTaskListCollection getPreviewColourForSection:@"ItemsDueDetailTextLabel"];
    itemsDueTableViewResponder.listLabelColour = [refMasterTaskListCollection getPreviewColourForSection:@"ItemsDueListLabel"];
    
    [itemsDueTableViewResponder reloadTableView:itemsDueTableView];
}

- (void)purchasedTheme:(NSString *)themeName {
    refMasterTaskListCollection.selectedTheme = themeName;
    
    [ApplicationSetting updateSettingName:@"SelectedTheme" withData:themeName];
    [self reloadTheme];
}

#pragma mark - Task List Methods

- (void)deleteListAtCurrentIndex {
    // Will also need to look for instances of the list in any notebooks
    
    NSInteger index = currentListIndex;
    BOOL isLastIndex = FALSE;
    
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
            // TODO: Check if this list is contained in any notebooks
            if (index == [taskListCollection.lists count] - 1) {
                isLastIndex = TRUE;
            }
            
            TaskList *taskList = [taskListCollection.lists objectAtIndex:currentListIndex];
            
            // Also need to get rid of instances of this in notebook collection
            BOOL isDirtyStatus = FALSE;
            for (Notebook *notebook in notebookCollection.notebooks) {
                BOOL tempDirtyStatus = [notebook removeInstancesOfListID:taskList.listID];
                if (isDirtyStatus != TRUE && tempDirtyStatus == TRUE) {
                    isDirtyStatus = TRUE;
                }
            }
            if (isDirtyStatus == TRUE) {
                [self updateNotebookListScrollViews];
            }
            
            NSInteger masterListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:taskList.listID];
            
            if (masterListIndex != -1 && [refMasterTaskListCollection.lists count] > 0) {
                [refMasterTaskListCollection.lists removeObjectAtIndex:masterListIndex];
                [taskListCollection deleteListAtIndex:currentListIndex permanent:NO];
            }
            
            
            
            break;
        case EnumCurrentSectionNotebooks:
            if (index == [notebookCollection.notebooks count] - 1) {
                isLastIndex = TRUE;
            }
            
            [notebookCollection deleteNotebookAtIndex:currentListIndex];
            break;
        case EnumCurrentSectionNotebookLists:
            // TODO: The list needs to be removed from the notebook as well
            break;
        default:
            break;
    }
	
	// Update the scroll list
	
	// Hmm... just remove the list button
	// [self updateScrollList];
	
	GLButton *listItemButton = [self getListItemButtonWithIndex:index];
	[listItemButton removeFromSuperview];
	
    NSInteger contentCount = 0;
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        contentCount = [taskListCollection.lists count];
    } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
        contentCount = [notebookCollection.notebooks count];
    }
    
	if (isLastIndex == TRUE) {
		// Last element, adjust scrollview differently
		[listScrollView setContentSize:CGSizeMake((contentCount * (kScrollObjWidth + kScrollObjBuffer)), [listScrollView bounds].size.height)];
		
		for (UIView *view in listScrollView.subviews) {
			if ([view isKindOfClass:[GLButton class]]) {
				GLButton *listButton = (GLButton *)view;
				[listButton setFrame:CGRectMake(listButton.frame.origin.x - (kScrollObjWidth + kScrollObjBuffer), 
												listButton.frame.origin.y, 
												listButton.frame.size.width, listButton.frame.size.height)];
			}
		}
	} else {
		[listScrollView setContentSize:CGSizeMake((contentCount * (kScrollObjWidth + kScrollObjBuffer)), [listScrollView bounds].size.height)];
	}
	// Also need to adjust the width of the scrollview
	
	// Just need to refresh everything
	[itemsDueTableViewResponder reloadDataWithListParentTitle:nil andTableView:itemsDueTableView andParentListID:-1 andListItem:nil];
	
	// Scroll to next index
	if (contentCount == 0) {
		// Do nothing
		currentListIndex = -1;
		
		// Update the title and date
		[self updateTitleAndDateForSelectedIndex];
	} else {
		// Make sure the current list index is not larger than count
		if (currentListIndex < contentCount) {
			[self scrollToListAtIndex:currentListIndex animated:YES];
		} else {
			// Scroll to last index
			[self scrollToListAtIndex:contentCount - 1 animated:YES];
		}
		
	}
    
    // Reload list pad
    [listPadTableView reloadData];
}

- (void)duplicateTheCurrentTaskList {
    // Do nothing if there is no lists in the task list collection
    if ([taskListCollection.lists count] == 0) {
        UIAlertView *noListsAlertView = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                                   message:@"No lists present, please add a new list" 
                                                                  delegate:nil cancelButtonTitle:@"Ok" 
                                                         otherButtonTitles:nil];
        [noListsAlertView show];
        [noListsAlertView release];
        return;
    }
    
    // Duplicate list pressed
    newListInserted = TRUE;
    
    TaskList *originalTaskList = [taskListCollection.lists objectAtIndex:currentListIndex];
    if ([self currentViewType] == EnumViewTypeTile) {
        if (tileScrollView.currentListIndex >= 0) {
            originalTaskList = [taskListCollection.lists objectAtIndex:tileScrollView.currentListIndex];
        }
    }
    
    TaskList *newMasterList = [[[TaskList alloc] initWithDuplicateTaskList:originalTaskList] autorelease];
    
    // Insert the new list in the database and view
    [self insertNewTaskList:newMasterList];
}

- (void)insertNewTaskList:(TaskList *)newMasterList {
	if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		[refMasterTaskListCollection.lists insertObject:newMasterList atIndex:0];
	} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
		[refMasterTaskListCollection.lists addObject:newMasterList];
		
		// Need to update the list order (as we have added to end)
		[refMasterTaskListCollection commitListOrderChanges];
		[refMasterTaskListCollection updateListOrdersInDatabase];
	}
	
	
	TaskList *newTaskList = [[TaskList alloc] initWithTaskListCopy:newMasterList];
	if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
		[taskListCollection.lists insertObject:newTaskList atIndex:0];
	} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
		[taskListCollection.lists addObject:newTaskList];
	}
	
	//[newMasterList release];
	[newTaskList release];
	
	// Refresh the scroll view
	//[self updateScrollList];
	
    // Insert the new list button
    [self insertNewListButton];
    
    [self listShaded:YES forIndex:currentListIndex];
	
	if ([self currentViewType] == EnumViewTypeList) {
		// Scroll to the new list
		if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
			[self scrollToListAtIndex:0 animated:YES];
		} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
			NSInteger location = [taskListCollection.lists count] - 1;
			[self scrollToListAtIndex:location animated:YES];
		}
		
		if ([taskListCollection.lists count] == 1) {
			[self updateTitleAndDateForSelectedIndex];
		}
        
	} else if ([self currentViewType] == EnumViewTypeTile) {
		if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
			tileScrollView.currentListIndex = tileScrollView.currentListIndex + 1;
		} 
		[tileScrollView updateScrollList];
	}
}

- (void)deleteListAtIndex:(NSInteger)index {
	BOOL isLastIndex = FALSE;
	if (index == [taskListCollection.lists count] - 1) {
		isLastIndex = TRUE;
	}
	
	TaskList *taskList = [taskListCollection.lists objectAtIndex:currentListIndex];
	NSInteger masterListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:taskList.listID];
	
	if (masterListIndex != -1 && [refMasterTaskListCollection.lists count] > 0 && [taskList isNote]) {
        [refMasterTaskListCollection.lists removeObjectAtIndex:masterListIndex];
        [taskListCollection deleteListAtIndex:currentListIndex permanent:YES]; 
    } else if (masterListIndex != -1 && [refMasterTaskListCollection.lists count] > 0) {
        [refMasterTaskListCollection.lists removeObjectAtIndex:masterListIndex];
        [taskListCollection deleteListAtIndex:currentListIndex permanent:NO]; 
    }
	
	
	// Update the scroll list
	
	// Hmm... just remove the list button
	// [self updateScrollList];
	
	GLButton *listItemButton = [self getListItemButtonWithIndex:index];
	[listItemButton removeFromSuperview];
	
	if (isLastIndex == TRUE) {
		// Last element, adjust scrollview differently
		[listScrollView setContentSize:CGSizeMake(([taskListCollection.lists count] * (kScrollObjWidth + kScrollObjBuffer)), [listScrollView bounds].size.height)];
		
		for (UIView *view in listScrollView.subviews) {
			if ([view isKindOfClass:[GLButton class]]) {
				GLButton *listButton = (GLButton *)view;
				[listButton setFrame:CGRectMake(listButton.frame.origin.x - (kScrollObjWidth + kScrollObjBuffer), 
												listButton.frame.origin.y, 
												listButton.frame.size.width, listButton.frame.size.height)];
				
			}
		}
		
	} else {
		[listScrollView setContentSize:CGSizeMake(([taskListCollection.lists count] * (kScrollObjWidth + kScrollObjBuffer)), [listScrollView bounds].size.height)];
	}
	// Also need to adjust the width of the scrollview
	
	// Just need to refresh everything
	[itemsDueTableViewResponder reloadDataWithListParentTitle:nil andTableView:itemsDueTableView andParentListID:-1 andListItem:nil];
	
	// Scroll to next index
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
        case EnumCurrentSectionNotebookLists:
            if ([taskListCollection.lists count] == 0) {
                // Do nothing
                currentListIndex = -1;
                
                // Update the title and date
                [self updateTitleAndDateForSelectedIndex];
            } else {
                // Make sure the current list index is not larger than count
                if (currentListIndex < [taskListCollection.lists count]) {
                    [self scrollToListAtIndex:currentListIndex animated:YES];
                } else {
                    // Scroll to last index
                    [self scrollToListAtIndex:[taskListCollection.lists count] - 1 animated:YES];
                }
                
            }
            break;
        case EnumCurrentSectionNotebooks:
            if ([notebookCollection.notebooks count] == 0) {
                // Do nothing
                currentListIndex = -1;
                
                // Update the title and date
                [self updateTitleAndDateForSelectedIndex];
            } else {
                // Make sure the current list index is not larger than count
                if (currentListIndex < [notebookCollection.notebooks count]) {
                    [self scrollToListAtIndex:currentListIndex animated:YES];
                } else {
                    // Scroll to last index
                    [self scrollToListAtIndex:[notebookCollection.notebooks count] - 1 animated:YES];
                }
                
            }
            break;
        default:
            break;
    }
    
	
}

#pragma mark - Notebook Methods

- (void)insertNewNotebook:(Notebook *)newNotebook {
    [notebookCollection.notebooks insertObject:newNotebook atIndex:0];
    //[newNotebook release];

    // Insert the new notebook button
    [self insertNewNotebookButton];
    
    [self listShaded:YES forIndex:currentListIndex];
	
	if ([self currentViewType] == EnumViewTypeList) {
		// Scroll to the new list
        [self scrollToListAtIndex:0 animated:YES];

		if ([notebookCollection.notebooks count] == 1) {
			[self updateTitleAndDateForSelectedIndex];
		}
	} else if ([self currentViewType] == EnumViewTypeTile) {
        tileScrollView.currentListIndex = tileScrollView.currentListIndex + 1;

		[tileScrollView updateScrollList];
	}
}

- (void)showNotebookOptionsForNotebookAtIndex:(NSInteger)notebookIndex atRect:(CGRect)displayRect {
    Notebook *notebook = [notebookCollection.notebooks objectAtIndex:notebookIndex];
    
    NotebookListsViewController *controller = [[[NotebookListsViewController alloc] initWithNotebook:notebook 
                                                                         andMasterTaskListCollection:refMasterTaskListCollection] autorelease];
    // Init the navigation controller that will hold the edit list item view controller
    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
    notebookListsPopoverController = [[UIPopoverController alloc] initWithContentViewController:navController];
    notebookListsPopoverController.delegate = self;
    [notebookListsPopoverController presentPopoverFromRect:displayRect 
                                                    inView:tileScrollView 
                                  permittedArrowDirections:(UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp) 
                                                  animated:YES];
}

- (void)updateNotebookListScrollViews {
    NSInteger count = 0;
    // Now need to update this notebook in the scrollview
    for (UIView *buttonSubview in listScrollView.subviews) {
        if ([buttonSubview isKindOfClass:[UIButton class]]) {
            for (UIView *subview in buttonSubview.subviews) {
                if ([subview isKindOfClass:[NotebookTileScrollView class]]) {
                    count++;
                    NotebookTileScrollView *notebookTileScrollView = (NotebookTileScrollView *)subview;
                    
                    [notebookTileScrollView updateScrollList];
                }
            }
            
        }
    }
}

- (void)updateNotebooksUsingCurrentTaskListCollection {
    // Need to remove any references to lists that have been archived.. just use the task collection to filter
    BOOL dirtyStatus = FALSE;
    for (Notebook *notebook in notebookCollection.notebooks) {
        BOOL tempDirtyStatus = [notebook removeInstancesOfListIDNotFoundInArray:[taskListCollection lists]];
        if (tempDirtyStatus == TRUE) {
            dirtyStatus = TRUE;
        }
    }
    if (dirtyStatus == TRUE) {
        [self updateNotebookListScrollViews];
    }
}

#pragma mark - ScrollView Methods

- (void)scrollToListAtIndex:(NSInteger)index animated:(BOOL)isAnimated {
	CGFloat newXLoc = (kScrollObjWidth + kScrollObjBuffer) * index;
	CGRect scrollRect = CGRectMake(newXLoc, 0, kScrollObjWidth + kScrollObjBuffer, kScrollObjHeight);
	
	// Set is auto scrolling to true
	if (currentListIndex != index) {
        [self listShaded:YES forIndex:currentListIndex];
        
		isAutoScrolling = TRUE;
        switch ([self currentSection]) {
            case EnumCurrentSectionLists:
            case EnumCurrentSectionNotebookLists:
                if ([taskListCollection.lists count] == 1) {
                    [self listShaded:NO forIndex:0];
                }
                break;
            case EnumCurrentSectionNotebooks:
                if ([notebookCollection.notebooks count] == 1) {
                    [self listShaded:NO forIndex:0];
                }
                break;
            default:
                break;
        }
		
	} else {
		[self listShaded:NO forIndex:currentListIndex];
		
		// Update title and date
		[self updateTitleAndDateForSelectedIndex];
	}
	
	// Update the current index
	currentListIndex = index;
	
	[listScrollView scrollRectToVisible:scrollRect animated:isAnimated];
    
    if (isAnimated == FALSE) {
        [self listShaded:NO forIndex:currentListIndex];
        [self updateTitleAndDateForSelectedIndex];
        //[listPadTableView reloadData];
        isAutoScrolling = FALSE;
    }
}

- (void)updateTitleAndDateForSelectedIndex {
    switch ([self currentSection]) {
        case EnumCurrentSectionLists:
            if ([taskListCollection.lists count] == 0) {
                self.title = @"My Lists & Notes";
                [titleTextField setText:@""];
                [dateLabelButton setTitle:@"" forState:UIControlStateNormal];
                return;
            } else {
                self.title = [NSString stringWithFormat:@"My Lists & Notes (%d of %d)", currentListIndex + 1, [taskListCollection.lists count]];
            }
            
            TaskList *list = [taskListCollection.lists objectAtIndex:currentListIndex];
            [titleTextField setText:list.title];
            NSDate *listDate = [MethodHelper dateFromString:list.creationDate usingFormat:K_DATEONLY_FORMAT];
            [dateLabelButton setTitle:[MethodHelper localizedDateFrom:listDate usingStyle:NSDateFormatterLongStyle withYear:YES]
                             forState:UIControlStateNormal];
            break;
        case EnumCurrentSectionNotebooks:
            if ([notebookCollection.notebooks count] == 0) {
                self.title = @"My Folders";
                [titleTextField setText:@""];
                [dateLabelButton setTitle:@"" forState:UIControlStateNormal];
                return;
            } else {
                self.title = [NSString stringWithFormat:@"My Folders (%d of %d)", currentListIndex + 1, [notebookCollection.notebooks count]];
            }
            
            if (currentListIndex < 0 || currentListIndex >= [notebookCollection.notebooks count]) {
                if ([notebookCollection.notebooks count] == 0) {
                    return;
                }
                currentListIndex = [notebookCollection.notebooks count] - 1;
            }
            
            Notebook *notebook = [notebookCollection.notebooks objectAtIndex:currentListIndex];
            [titleTextField setText:notebook.title];
            NSDate *notebookDate = [MethodHelper dateFromString:notebook.creationDate usingFormat:K_DATEONLY_FORMAT];
            [dateLabelButton setTitle:[MethodHelper localizedDateFrom:notebookDate usingStyle:NSDateFormatterLongStyle withYear:YES]
                             forState:UIControlStateNormal];
            
            break;
        case EnumCurrentSectionNotebookLists:
            if (currentListIndex < 0 || currentListIndex >= [notebookCollection.notebooks count]) {
                [MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Unable to find notebook to update title.  Please report this error to manage@kerofrog.com.au" 
                                      andButtonTitle:@"Ok"];
                break;
            }
            
            //TODO: Need to get the notebook title, will need to have selected notebook index after all
            
         
            break;
        default:
            break;
    }
    
	
	
	
    
}

- (void)listShaded:(BOOL)status forIndex:(NSInteger)index {
	int i = 0;
	
	for (UIView *view in listScrollView.subviews) {
		if ([view isKindOfClass:[GLButton class]]) {
			if (i == index) {
				GLButton *listButton = (GLButton *)view;
				
				//[listButton setHighlighted:status];
				UIView *shadeView = (UIView *)[listButton viewWithTag:kShadeViewTag];
				
				if (status == TRUE) {
					[shadeView setHidden:NO];
				} else {
					[shadeView setHidden:YES];
				}
				
				return;
			}
			
			
			
			i++;
		}
	}
}

     
 
- (GLButton *)getListItemButtonWithIndex:(NSInteger)index {
	int i = 0;
	
	for (UIView *view in listScrollView.subviews) {
		if ([view isKindOfClass:[GLButton class]]) {
			if (i == index) {
				GLButton *listButton = (GLButton *)view;
				return listButton;
			}
			i++;
		}
	}
	
	return nil;
}

- (void)slideListToFillIndex:(NSInteger)index {
    
    NSInteger contentCount = 0;
    if ([self currentSection] == EnumCurrentSectionLists || [self currentSection] == EnumCurrentSectionNotebookLists) {
        contentCount = [taskListCollection.lists count];
    } else if ([self currentSection] == EnumCurrentSectionNotebooks) {
        contentCount = [notebookCollection.notebooks count];
    }
    
	if (index == contentCount - 1) {
		for (UIView *view in listScrollView.subviews) {
			if ([view isKindOfClass:[GLButton class]]) {
				GLButton *listButton = (GLButton *)view;
				[listButton setFrame:CGRectMake(listButton.frame.origin.x + (kScrollObjWidth + kScrollObjBuffer), 
												listButton.frame.origin.y, 
												listButton.frame.size.width, listButton.frame.size.height)];
			}
		}
		// Jump out of method
		return;
	}
	
	int i = 0;
	
	for (UIView *view in listScrollView.subviews) {
		if ([view isKindOfClass:[GLButton class]]) {
			if (i > index) {
				GLButton *listButton = (GLButton *)view;
				[listButton setFrame:CGRectMake(listButton.frame.origin.x - (kScrollObjWidth + kScrollObjBuffer), 
												listButton.frame.origin.y, 
												listButton.frame.size.width, listButton.frame.size.height)];
			}
			i++;
		}
	}
}


#pragma mark -
#pragma mark Helper Methods

- (void)updateLocalData {
	// Reload tag collection
	[tagCollection reloadAllTags];
	
	[refMasterTaskListCollection reloadAllLists];
	
	[taskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
	[self updateNotebooksUsingCurrentTaskListCollection];
    
	// Update the items responder
	[itemsDueTableViewResponder reloadDataWithListParentTitle:nil andTableView:itemsDueTableView andParentListID:-1 andListItem:nil];
    
	if ([self currentViewType] == EnumViewTypeList) {
		// Reload the scroll view
		[self updateListScrollList];
		
		[self updateTitleAndDateForSelectedIndex];
		
		// Remove shading on current selection
		[self listShaded:NO forIndex:currentListIndex];
	} else if ([self currentViewType] == EnumViewTypeTile) {
		[tileScrollView updateScrollList];
        
        // Reload the scroll view (need this as well)
		[self updateListScrollList];
	}
}

// Commits the changes needed to show a new view type
- (void)commitViewType {
	// Remove title text field responder
	if ([titleTextField isFirstResponder]) {
		[titleTextField resignFirstResponder];
	}
	
	if ([self currentViewType] == EnumViewTypeList) {
		[titleTextField setHidden:NO];
		[titleBackgroundToolbar setHidden:NO];
		[dateLabelButton setHidden:NO];
		[myToolbar setHidden:NO];
		[tileScrollView setHidden:YES];
		[listScrollView setHidden:NO];
		currentListIndex = tileScrollView.currentListIndex;
		
		if ([tileScrollView updated] == TRUE) {
			// Show spinner
            
            progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
            [self.navigationController.view addSubview:progressHUD];
            progressHUD.delegate = self;
            progressHUD.labelText = @"Reloading...";
            commitViewTypeHUDRunning = TRUE;
            [progressHUD showWhileExecuting:@selector(updateListScrollList) onTarget:self withObject:nil animated:YES];
        
			
			//[self performSelector:@selector(updateListScrollList) withObject:nil afterDelay:0];
			
			//[self updateListScrollList];
			
			tileScrollView.updated = FALSE;
		}
		
		
		if (currentListIndex >= [taskListCollection.lists count] && [taskListCollection.lists count] != 0) {
			currentListIndex = [taskListCollection.lists count] - 1;
		} else if ([taskListCollection.lists count] == 0) {
			return;
		}
		tileScrollView.currentListIndex = currentListIndex;
		
		[self scrollToListAtIndex:currentListIndex animated:NO];
		//[self listShaded:NO forIndex:selectedListIndex];
		//[self updateTitleAndDateForSelectedIndex];
	} else if ([self currentViewType] == EnumViewTypeTile) {
        
        switch ([self currentSection]) {
            case EnumCurrentSectionLists:
                self.title = @"My Lists & Notes";
                break;
            case EnumCurrentSectionNotebooks:
                self.title = @"My Folders";
                break;
            case EnumCurrentSectionNotebookLists:
                //TODO: Get the right notebook
                break;
            default:
                break;
        }
		
		[titleTextField setHidden:YES];
		[titleBackgroundToolbar setHidden:YES];
		[dateLabelButton setHidden:YES];
		[myToolbar setHidden:YES];
		[listScrollView setHidden:YES];
		[tileScrollView setHidden:NO];
		
        switch ([self currentSection]) {
            case EnumCurrentSectionLists:
            case EnumCurrentSectionNotebookLists:
                if (currentListIndex >= [taskListCollection.lists count] && [taskListCollection.lists count] != 0) {
                    currentListIndex = [taskListCollection.lists count] - 1;
                } else if ([taskListCollection.lists count] == 0) {
                    currentListIndex = 0;
                }
                break;
            case EnumCurrentSectionNotebooks:
                if (currentListIndex >= [notebookCollection.notebooks count] && [notebookCollection.notebooks count] != 0) {
                    currentListIndex = [notebookCollection.notebooks count] - 1;
                } else if ([notebookCollection.notebooks count] == 0) {
                    currentListIndex = 0;
                }
                break;
            default:
                break;
        }
		
		tileScrollView.currentListIndex = currentListIndex;
		
		[tileScrollView updateScrollList];
	}
}

- (void)reloadTheme {
	NSString *canvasImageName = [refMasterTaskListCollection getImageNameForSection:@"Canvas"];
	UIImage *newCanvasImage = [UIImage imageNamed:canvasImageName];
	[canvassImage setImage:newCanvasImage];
	
	NSString *borderImageName = [refMasterTaskListCollection getImageNameForSection:@"Border"];
	UIImage *newBorderImage = [UIImage imageNamed:borderImageName];
	[borderImage setImage:newBorderImage];
	
	NSString *pocketImageName = [refMasterTaskListCollection getImageNameForSection:@"Pocket"];
	UIImage *newPocketImage = [UIImage imageNamed:pocketImageName];
	[itemsDuePocket setImage:newPocketImage];
	
	// Get tile view type
	NSString *buttonType = [refMasterTaskListCollection getTileViewButtonType];
	[tileScrollView updateButtonTypes:buttonType];
	
	// Get colours
	titleTextField.textColor = [refMasterTaskListCollection getColourForSection:@"MainViewTitleTextField"];
	[dateLabelButton setTitleColor:[refMasterTaskListCollection getColourForSection:@"MainViewDateLabelButton"] forState:UIControlStateNormal];
	
	itemsDueTableViewResponder.textLabelColour = [refMasterTaskListCollection getColourForSection:@"ItemsDueTextLabel"];
	itemsDueTableViewResponder.detailTextLabelColour = [refMasterTaskListCollection getColourForSection:@"ItemsDueDetailTextLabel"];
	itemsDueTableViewResponder.listLabelColour = [refMasterTaskListCollection getColourForSection:@"ItemsDueListLabel"];
	
	[itemsDueTableViewResponder reloadTableView:itemsDueTableView];
}

- (void)showActivityIndicator {
    activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        [activityIndicatorView setFrame:kActivityIndicatorViewPortrait];
        [activityIndicatorBackgroundView setFrame:kActivityViewBackgroundPortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        [activityIndicatorView setFrame:kActivityIndicatorViewLandscape];
        [activityIndicatorBackgroundView setFrame:kActivityViewBackgroundLandscape];
    }
    
    [self.view bringSubviewToFront:activityIndicatorBackgroundView];
    [activityIndicatorBackgroundView setHidden:NO];
    
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)hideActivityIndicator {
    // Stop animating
	if ([activityIndicatorView isAnimating]) {
		[activityIndicatorBackgroundView setHidden:YES];
		[activityIndicatorView stopAnimating];
		[activityIndicatorView removeFromSuperview];
	}
}

@end
