//
//  TagsViewController.h
//  Manage
//
//  Created by Cliff Viegas on 10/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TagCollection;

@interface TagsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	UITableView			*myTableView;
	TagCollection		*refTagCollection;
}

#pragma mark Load Methods
- (void)loadTableView;

@end
