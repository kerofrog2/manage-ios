//
//  EditTagViewController.h
//  Manage
//
//  Created by Cliff Viegas on 25/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TagCollection;

// Data Models
@class Tag;

// View Controllers (import for delegates)
#import "SelectListItemTagViewController.h"
#import "ListSettingViewController.h"

@protocol EditTagDelegate
- (void)tagEdited:(Tag *)theTag;
@end


@interface EditTagViewController : UIViewController <ListSettingDelegate, SelectListItemTagDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
	id <EditTagDelegate>	delegate;
	
	UITableView				*myTableView;
	UIBarButtonItem			*doneBarButtonItem;
	
	TagCollection			*refTagCollection;
	NSInteger				selectedParentTagID;
	
	Tag						*refSelectedTag;
    
    // Colour settings
    ApplicationSetting  *tagColourSetting;
    PropertyDetail      *tagColourPropertyDetail;
}

@property (nonatomic, assign)	id	delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(id<EditTagDelegate>)theDelegate andTagCollection:(TagCollection *)theTagCollection 
		 andEditingTag:(Tag *)theEditingTag andSelectedParentTagID:(NSInteger)theSelectedParentTagID; 

@end
