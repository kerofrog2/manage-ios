    //
//  ToodledoSyncViewController.m
//  Manage
//
//  Created by Cliff Viegas on 13/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ToodledoSyncViewController.h"

// Data Models
#import "ApplicationSetting.h"
#import "ToodledoAccountInfo.h"
#import "PropertyDetail.h"

// Responders
#import "LocalResultsTableViewResponder.h"
#import "ToodledoResultsTableViewResponder.h"

// Method Helper
#import "MethodHelper.h"

// View Controllers
#import "ToodledoSyncInstructionsViewController.h"

// Constants
#define kLocalTableViewFrame                    CGRectMake(0, 0, 320, 318)
#define kToodledoTableViewFrame                 CGRectMake(280, 0, 280, 318)
#define kPopoverSize                            CGSizeMake(320, 318)
//#define kPopoverSize                          CGSizeMake(560, 560)
#define kReplaceLocalButtonFrame                CGRectMake(60, 210, 200, 40)
#define kReplaceToodledoButtonFrame             CGRectMake(60, 261, 200, 40)
#define kReplaceToodledoTasksAlertViewTag       81938
#define kReplaceLocalTasksAlertViewTag          39975


@interface ToodledoSyncViewController()
- (void)loadTableViews;
@end


@implementation ToodledoSyncViewController

@synthesize delegate;
@synthesize refPopoverController;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[localTableView release];
	[conflictPriorityAppSetting release];
	[conflictPriorityPropertyDetail release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection
		  andToodledoAccountInfo:(ToodledoAccountInfo *)theAccountInfo {
	if ((self = [super init])) {
		self.title = @"Toodledo Sync";
		
		// Load the responders
		localResultsTableViewResponder = [[LocalResultsTableViewResponder alloc] init];
		toodledoResultsTableViewResponder = [[ToodledoResultsTableViewResponder alloc] init];
		
		// Init the conflict priority objects
		conflictPriorityAppSetting = [[ApplicationSetting alloc] init];
		[conflictPriorityAppSetting loadApplicationSettingWithName:@"ToodledoServerHasConflictPriority"];
		
		conflictPriorityPropertyDetail = [[PropertyDetail alloc] initWithName:@"ToodledoServerHasConflictPriority" 
															  andFriendlyName:@"Conflict Priority" 
																   andSection:@"" 
															  andPropertyType:EnumPropertyTypeList];
		NSArray *propList = [NSArray arrayWithObjects:@"Toodledo", @"Local", nil];
		conflictPriorityPropertyDetail.description = @"Conflict priority setting only relates to lists (Toodledo refers to them as folders).  Conflict priority for tasks is handled by which task was most recently modified (local or Toodledo).";
		[conflictPriorityPropertyDetail.list addObjectsFromArray:propList];
		
		// Assign the task list collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Assign the reference to the toodledo account info
		refToodledoAccountInfo = theAccountInfo;
		
		// Load the tableview
		[self loadTableViews];
		
		// Set the background color
		self.view.backgroundColor = [UIColor colorWithRed:205 / 255.0 
													green:209 / 255.0  
													 blue:215 / 255.0 
													alpha:1.0f];
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableViews {
	myTableView = [[UITableView alloc] initWithFrame:kLocalTableViewFrame style:UITableViewStyleGrouped];
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	[self.view addSubview:myTableView];
	
	/*localTableView = [[UITableView alloc] initWithFrame:kLocalTableViewFrame style:UITableViewStyleGrouped];
	[localTableView setDelegate:localResultsTableViewResponder];
	[localTableView setDataSource:localResultsTableViewResponder];
	[self.view addSubview:localTableView];
	
	toodledoTableView = [[UITableView alloc] initWithFrame:kToodledoTableViewFrame style:UITableViewStyleGrouped];
	[toodledoTableView setDelegate:toodledoResultsTableViewResponder];
	[toodledoTableView setDataSource:toodledoResultsTableViewResponder];
	[self.view addSubview:toodledoTableView];*/
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}

	// Update the textlabel color to be dark blue
	cell.textLabel.textAlignment = UITextAlignmentCenter;
	
	cell.textLabel.text = @"";
	cell.detailTextLabel.text = @"";
	// Dark blue colour
	cell.textLabel.textColor = [UIColor colorWithRed:86 / 255.0 green:104 / 255.0 blue:148 / 255.0 alpha:1.0];
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	//[myTextField setTextColor:[UIColor colorWithRed:0.0245 green:0.0468 blue:0.398 alpha:1.0]];
	
	if (indexPath.section == 1) {
		if (indexPath.row == 0) {
			cell.textLabel.text = @"Replace Local Tasks";
		} else if (indexPath.row == 1) {
			cell.textLabel.text = @"Replace Toodledo Tasks";
		}
	} else if (indexPath.section == 0) {
			cell.textLabel.text = @"Synchronize Now";
	} else if (indexPath.section == 2) {
		if (indexPath.row == 0) {
			cell.textLabel.text = @"Instructions & Limitations";
			cell.textLabel.textAlignment = UITextAlignmentLeft;
			cell.textLabel.textColor = [UIColor darkTextColor];
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
		
	}

    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 3;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 1;
	} else if (section == 1) {
        return 2;  
    } else if (section == 2) {
		return 1;
	}
    
	return 1;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// We don't want any row to be selectable (yet)
	return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:indexPath];
	
	if ([cell.textLabel.text isEqualToString:@"Instructions & Limitations"]) {
		ToodledoSyncInstructionsViewController *controller = [[[ToodledoSyncInstructionsViewController alloc] init] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([cell.textLabel.text isEqualToString:@"Replace Local Tasks"]) {
		UIAlertView *toodledoAlertView = [[UIAlertView alloc] initWithTitle:@"Warning" 
																	message:@"This action will delete and replace all of your local tasks with your toodledo tasks. Are you sure you want to do this? (Please note that a backup of your data is also automatically created for you, should you wish to restore your tasks)" 
																   delegate:self 
														  cancelButtonTitle:@"Cancel" 
														  otherButtonTitles:@"Ok", nil];
        toodledoAlertView.tag = kReplaceLocalTasksAlertViewTag;
		[toodledoAlertView show];
		[toodledoAlertView release];
	} else if ([cell.textLabel.text isEqualToString:@"Replace Toodledo Tasks"]) {
		UIAlertView *toodledoAlertView = [[UIAlertView alloc] initWithTitle:@"Warning" 
																	message:@"This action will replace all toodledo tasks with your local tasks.  Are you sure you want to do this?" 
																   delegate:self 
														  cancelButtonTitle:@"Cancel" 
														  otherButtonTitles:@"Ok", nil];
        toodledoAlertView.tag = kReplaceToodledoTasksAlertViewTag;
		[toodledoAlertView show];
		[toodledoAlertView release];
	} else if ([cell.textLabel.text isEqualToString:@"Synchronize Now"]) {
		// We need to make sure that there has already been a sync before actioning this, call alert view if not
		if ([[ApplicationSetting getSettingDataForName:@"ToodledoLastSyncDateTime"] length] == 0) {
			[MethodHelper showAlertViewWithTitle:@"Full Sync Required" 
									  andMessage:@"As this is your first sync, please do a full sync by selecting 'Replace Local Tasks' or 'Replace Toodledo Tasks'.  Please read the instructions for more information." 
								  andButtonTitle:@"Ok"];
			[tableView deselectRowAtIndexPath:indexPath animated:YES];
			return;
		}
		
		[self.delegate settingsPopoverCanClose:NO];
		
		SyncToodledoNowViewController *controller = [[[SyncToodledoNowViewController alloc] 
													  initWithTaskListCollection:refMasterTaskListCollection 
													  andToodledoAccountInfo:refToodledoAccountInfo] autorelease];
		controller.delegate = self;
		[self.navigationController pushViewController:controller animated:NO];
	} else if (indexPath.section == 2 && indexPath.row == 0) {
		ListSettingViewController *controller = [[[ListSettingViewController alloc] initWithDelegate:self 
																					  andAppSetting:conflictPriorityAppSetting 
																				   andPropertyDetail:conflictPriorityPropertyDetail] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {	
	if (section == 5) {
		NSString *footerTitle = @"It looks like this is your first time syncing with Toodledo.  For our first sync we will need to either:\n"
		"1. Replace your Toodledo tasks with a copy of your local tasks\n"
		"2. Replace your local tasks with a copy of your Toodledo tasks";
		
		return footerTitle;
	}
	
	return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (section == 1) {
		return @"Full Sync";
	} else if (section == 0) {
		return @"Standard Sync";
	}
	
	return @"";
}

/*
#pragma mark -
#pragma mark ToodledoFolderCollection Delegates

- (void)finishedCollectingFolders {
	// Do stuff with our folder
	NSLog(@"folder finished loading");
	
	for (ToodledoFolder *folder in toodledoFolderCollection.folders) {
		NSLog(@"folder name is: %@", folder.name);
	}
	
	// Now collect the tasks
	NSString *key = [ApplicationSetting getSettingDataForName:@"ToodledoKey"];
	
	NSString *lastToodledoSync = [ApplicationSetting getSettingDataForName:@"ToodledoLastSyncDateTime"];
	
	NSString *lastSyncDateStamp = @"0";
	
	if ([lastToodledoSync length] > 0) {
		NSDate *syncDateTime = [MethodHelper dateFromString:lastToodledoSync usingFormat:K_DATETIME_FORMAT];
		lastSyncDateStamp = [NSString stringWithFormat:@"%f", [syncDateTime timeIntervalSince1970]];
		
	}
	
	toodledoTaskCollection = [[ToodledoTaskCollection alloc] initUsingKey:key andDelegate:self];
	
	NSString *fields = @"tag,folder,parent,duedate,repeat,repeatfrom,priority,note,meta";
	[toodledoTaskCollection collectTasksFromServerWithModafter:lastSyncDateStamp 
													 andFields:fields];
}

#pragma mark -
#pragma mark ToodledoTaskCollection Delegates

- (void)finishedCollectingTasks {
	NSLog(@"finished collecting tasks: %d", [toodledoTaskCollection.tasks count]);
	for (ToodledoTask *task in toodledoTaskCollection.tasks) {
		NSLog(@"task title: %@", task.title);
	}
}
*/
#pragma mark -
#pragma mark ReplaceLocalTasks and ReplaceToodledoTasks Delegates

- (void)replaceLocalTasksSyncComplete {
	[self.delegate replaceLocalTasksSyncComplete];
}

- (void)replaceToodledoDataComplete {
    [self.delegate replaceToodledoDataComplete];
}

- (void)closeSettingsPopover {
	[self.delegate settingsPopoverCanClose:YES];
	[self.delegate closeSettingsPopover];
}

#pragma mark -
#pragma mark Sync Toodledo Now Delegates

- (void)syncToodledoNowComplete {
	[self.delegate replaceLocalTasksSyncComplete];
}

#pragma mark -
#pragma mark List Setting Delegate

- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData {
	// Update the conflict priority setting
	conflictPriorityAppSetting.data = theData;
	
	// Update the database
	[conflictPriorityAppSetting updateDatabase];
	
	// Reload the table view
	[myTableView reloadData];
}


#pragma mark -
#pragma mark Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kReplaceToodledoTasksAlertViewTag) {
        if (buttonIndex == 0) {
            // Cancel
            // Do nothing
        } else if (buttonIndex == 1) {
            // Ok
            [self.delegate settingsPopoverCanClose:NO];
            ReplaceToodledoTasksViewController *controller = [[[ReplaceToodledoTasksViewController alloc]
                                                               initWithTaskListCollection:refMasterTaskListCollection] autorelease];
            controller.delegate = self;
            [self.navigationController pushViewController:controller animated:NO];
        }
    } else if (alertView.tag == kReplaceLocalTasksAlertViewTag) {
        if (buttonIndex == 0) {
            // Cancel
            // Do nothing
        } else if (buttonIndex == 1) {
            // Ok
            [self.delegate settingsPopoverCanClose:NO];
            
            ReplaceLocalTasksViewController *controller = [[[ReplaceLocalTasksViewController alloc] 
                                                            initWithTaskListCollection:refMasterTaskListCollection
                                                            andToodledoAccountInfo:refToodledoAccountInfo] autorelease];
            controller.delegate = self;
            [self.navigationController pushViewController:controller animated:NO];
        }
    }
    
	
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewWillAppear:(BOOL)animated {
	[self.delegate settingsPopoverCanClose:YES];
	
	[super viewWillAppear:animated];
}

- (void)viewDidLoad {
	NSString *lastSync = [ApplicationSetting getSettingDataForName:@"ToodledoLastSyncDateTime"];
	
	if ([lastSync length] == 0) {
		NSString *message = @"It looks like this is your first time syncing with Toodledo. For our first sync, we will "
							"need to either:\n"
							"1. replace your local tasks with a copy of your Toodledo tasks, or\n"
							"2. replace your Toodledo tasks with a copy of "
							"your local tasks.\n\nPlease read the instructions for more information.";
		[MethodHelper showAlertViewWithTitle:@"First time Sync?" 
								  andMessage:message 
							  andButtonTitle:@"Ok"];
	}
	
	// Also need to store when last sync was done... in local db
	
	[super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end
