    //
//  DataImportViewController.m
//  Manage
//
//  Created by Cliff Viegas on 26/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "DataImportViewController.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@interface DataImportViewController()
// Load Methods
- (void)loadTableView;
@end

@implementation DataImportViewController

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[myTableView release];
	[importListArray release];
	[super dealloc];
}


#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		// Set the title
		self.title = @"Data Import";
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Load the array of backup databases
		NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *defaultPath = [searchPaths objectAtIndex:0];
		
		importListArray = [[MethodHelper getSortedListOfBackupFiles:defaultPath] retain];
		
		// Load the tableview
		[self loadTableView];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}
	
	
	
	// Default values
	cell.textLabel.text = @"";
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	if (indexPath.section == 0) {
		cell.textLabel.text = @"Instructions";
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	} else {
		if ([importListArray count] == 0) {
			cell.textLabel.text = @"No import data found";
		} else {
			NSString *importData = [importListArray objectAtIndex:indexPath.row];
			
			NSString *timeStampString = [MethodHelper getTimeStampStringFromFile:importData];
			NSTimeInterval timeInterval = [timeStampString doubleValue];
			NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
			
			cell.textLabel.text = [MethodHelper localizedDateTimeFrom:date 
													   usingDateStyle:NSDateFormatterMediumStyle 
														 andTimeStyle:NSDateFormatterMediumStyle];
		}
	}

	
	

	
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 1;
	}
	
	if ([importListArray count] == 0) {
		return 1;
	}
	
	return [importListArray	count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	// Import data
	// Just use message box to confirm
	
	[myTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	if ([importListArray count] == 0 && section == 1) {
		return @"Please read the instructions on how to setup data for import";
	}
	return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (section == 1) {
		return @"Current Imports";
	}
	
	return @"";
}


#pragma mark -
#pragma mark UIViewController Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end
