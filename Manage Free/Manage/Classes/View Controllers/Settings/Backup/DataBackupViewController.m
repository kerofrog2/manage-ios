//
//  DatabaseBackupViewController.m
//  Manage
//
//  Created by Cliff Viegas on 16/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "DataBackupViewController.h"

// Method Helper
#import "MethodHelper.h"

// View Controllers
#import "DataImportViewController.h"
#import "RestoreExportDataViewController.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@interface DataBackupViewController()
// Load Methods
- (void)loadTableView;
@end

@implementation DataBackupViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[myTableView release];
	[backupListArray release];
	[importedBackupListArray release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		// Set the title
		self.title = @"Data Backup";
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Load the array of backup databases
		backupListArray = [[MethodHelper getSortedListOfBackupFiles:[MethodHelper getBackupPath]] retain];
		
		// Load the imported list of backup databases
		importedBackupListArray = [[MethodHelper getSortedListOfBackupFiles:[MethodHelper getHomePath]] retain];
		
		// Load the tableview
		[self loadTableView];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}
	
	// Default values
	cell.textLabel.text = @"";
	cell.textLabel.textAlignment = UITextAlignmentLeft;
	cell.textLabel.textColor = [UIColor darkTextColor];
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
	if (indexPath.section == 0) {
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.textLabel.textColor = [UIColor colorWithRed:86 / 255.0 green:104 / 255.0 blue:148 / 255.0 alpha:1.0];
		cell.textLabel.textAlignment = UITextAlignmentCenter;
		cell.textLabel.text = @"Create New Backup";
	} else if ([importedBackupListArray count] > 0 && indexPath.section == 1) {
		// Need list of imported backups
		NSString *backup = [importedBackupListArray objectAtIndex:indexPath.row];
		NSString *timeStampString = [MethodHelper getTimeStampStringFromFile:backup];
		NSTimeInterval timeInterval = [timeStampString doubleValue];
		NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
		cell.textLabel.text = [MethodHelper localizedDateTimeFrom:date 
												   usingDateStyle:NSDateFormatterMediumStyle 
													 andTimeStyle:NSDateFormatterMediumStyle];
	} else {
		NSString *backup = [backupListArray objectAtIndex:indexPath.row];
		
		NSString *timeStampString = [MethodHelper getTimeStampStringFromFile:backup];
		
		NSTimeInterval timeInterval = [timeStampString doubleValue];
		NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
		
		//NSString *stringDate = [MethodHelper stringFromDate:date usingFormat:K_DATETIME_FORMAT];
		cell.textLabel.text = [MethodHelper localizedDateTimeFrom:date 
												   usingDateStyle:NSDateFormatterMediumStyle 
													 andTimeStyle:NSDateFormatterMediumStyle];

	}

	
	// Selecting a database will open new screen.. maybe showing more details?... oh shit.
	// After selected, there is also the option of 'exporting' that backup via email
	//
	
	
	
	// NOTE: Also need to store log (done)
	// NOTE: Also need to store images, zip them up (done)
	
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if ([importedBackupListArray count] > 0) {
		return 3;
	} 
	return 2;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 1;
	} else if (section == 1 && [importedBackupListArray count] > 0) {
		return [importedBackupListArray count];
	}   
	
	return [backupListArray count];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	// Backup database
	if (indexPath.section == 0) {
		[MethodHelper createBackup];
		[self.delegate dataSuccessfullyBackedUp];
	} else if ([importedBackupListArray count] > 0 && indexPath.section == 1) {
		NSString *backupFile = [importedBackupListArray objectAtIndex:indexPath.row];
		NSString *backupPath = [NSString stringWithFormat:@"%@%@",
								[MethodHelper getHomePath], backupFile];
		RestoreExportDataViewController *controller = [[[RestoreExportDataViewController alloc] 
														initWithBackup:backupFile
														andBackupPath:backupPath] autorelease];
		controller.delegate = self;
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([backupListArray count] > 0) {
		NSString *backupFile = [backupListArray objectAtIndex:indexPath.row];
		NSString *backupPath = [NSString stringWithFormat:@"%@%@",
							  [MethodHelper getBackupPath], backupFile];
		
		RestoreExportDataViewController *controller = [[[RestoreExportDataViewController alloc] 
														initWithBackup:backupFile
														andBackupPath:backupPath] autorelease];
		controller.delegate = self;
		[self.navigationController pushViewController:controller animated:YES];
	}
	
	[myTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {

	return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (section == 0) {
		return @"";
	} else if (section == 1 && [importedBackupListArray count] > 0) {
		return @"Imported Backups";
	} else if ([backupListArray count] > 0) {
		return @"Current Backups";
	}
	
	return @"";
}


#pragma mark -
#pragma mark Restore Export Data Delegates

- (void)restoreDataComplete {
	[self.delegate restoreDataComplete];
}

- (void)closeSettingsPopover {
	[self.delegate closeSettingsPopover];
}

#pragma mark -
#pragma mark UIViewController Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end
