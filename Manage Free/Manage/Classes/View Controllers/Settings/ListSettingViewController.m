//
//  StringSettingViewController.m
//  Manage
//
//  Created by Cliff Viegas on 4/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ListSettingViewController.h"

// Data Models
#import "ApplicationSetting.h"
#import "PropertyDetail.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@interface ListSettingViewController()
- (void)loadTableView;
@end


@implementation ListSettingViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[myTableView release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<ListSettingDelegate>)theDelegate andAppSetting:(ApplicationSetting *)appSetting 
	 andPropertyDetail:(PropertyDetail *)thePropertyDetail {
	if ((self = [super init])) {
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign application setting
		refApplicationSetting = appSetting;
		
		// Assign the property detail
		refPropertyDetail = thePropertyDetail;
		
		// Set the title
		self.title = refPropertyDetail.friendlyName;
		
		// Load the table view
		[self loadTableView];
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}


#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
	
	// Default values
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	// Set up the cell...
	NSString *settingData = [refPropertyDetail.list objectAtIndex:indexPath.row];
	cell.textLabel.text = settingData;
	
	if ([settingData isEqualToString:refApplicationSetting.data]) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	
    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return [refPropertyDetail.list count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	NSString *newData = [refPropertyDetail.list objectAtIndex:indexPath.row];

	[delegate settingWithName:refApplicationSetting.name updatedWithData:newData];
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	return refPropertyDetail.description;
}

#pragma mark -
#pragma mark UIViewController Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





@end
