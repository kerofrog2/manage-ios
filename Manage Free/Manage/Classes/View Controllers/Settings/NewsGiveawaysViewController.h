//
//  NewsGiveawaysViewController.h
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 4/09/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NewsGiveawaysViewController : UIViewController <UIWebViewDelegate> {
    UIWebView       *myWebView;
}

@end
