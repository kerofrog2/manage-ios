//
//  ArchivesViewController.m
//  Manage
//
//  Created by Cliff Viegas on 3/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ArchivesViewController.h"

// List Constants
#import "ListConstants.h"
#import "MainConstants.h"

// Custom objects
#import "CustomSegmentedControl.h"

// Data Models
#import "ArchiveList.h"
#import "ListItem.h"
#import "TaskList.h"

// Method Helper
#import "MethodHelper.h"

// Data Collections
#import "TagCollection.h"
#import "TaskListCollection.h"

// Custom table view cell
#import "ArchivesTaskTableViewCell.h"
#import "ListItemTableViewCell.h"

// Archive Constants
#import "ArchiveConstants.h"

// Popover controller view controllers
#import "ArchivesPanelViewController.h"

@interface ArchivesViewController()
// Load methods
- (void)loadTableView;
- (void)loadButtons;
- (void)loadControlPanelObjects;
- (void)loadHeader;
// Class methods
- (void)hideArchiveCompletedItems:(BOOL)hide;
- (void)hideArchiveCompleteLists:(BOOL)hide;
- (void)updateMovedListItem;
// Helper methods
- (void)reloadTheme;
@end

@implementation ArchivesViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
#ifdef IS_FREE_BUILD
    [_adView release];
#endif
	// Popover controllers
    
	// List Objects
	[myTableView release];
	[headerLine1 release];
	[headerLine2 release];
	// Archive list
	[archiveList release];
	// Canvass and page images
	[canvassImage release];
	[borderImage release];
	[pageImage release];
	// Control panel objects
	[controlPanelImageView release];
	[listPadTableView release];
	[archivedListsTableViewResponder release];
	[listPadTitleLabel release];
	// Buttons
	[archiveCompletedListsButton release];
	[archiveCompletedItemsButton release];
	[closeArchivesButton release];
	[archiveCompletedFooterLine release];
	[panelBarButtonItem release];
	//[archiveCheckBarButtonItem release];
	// Header section (lower right)
	[titleLabel release];
	[scribblesOnOffLabel release];
	[scribblesOnOffSwitch release];
	// Alert views
	[unableToArchiveTasksAlertView release];
	[archiveListsAlertView release];
	[restoreTaskListAlertView release];
    // Remove our observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}


#pragma mark -
#pragma mark Initialisation

- (id)initWithTagCollection:(TagCollection *)theTagCollection andMasterTaskListCollection:(TaskListCollection *)theTaskListCollection {
	if ((self = [super init])) {
		// Set the title
		self.title = @"Archives";
		
		// Assign the reference to the master task collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Assign the tag collection
		refTagCollection = theTagCollection;
		
        // Create an observer for whenever application becomes active
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        
		// Init the current archive type
		currentArchiveType = EnumCurrentArchiveTypeTasks;
		
		// Init currently selected list to 0
		currentlySelectedList = 0;
		
		// Init the archive list
		archiveList = [[ArchiveList alloc] initWithArchivedListItems];
		
		// Init has updated list items to false
		hasUpdatedListItems = FALSE;
		
		// Init selected row to 0
		selectedRow = 0;
	}
	return self;
}

- (void)loadView {
#ifdef IS_FREE_BUILD
    [super loadView];
#endif
    // Load the canvass and page images
	canvassImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"RedRocketCanvas.png"]];
	pageImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PaperPreview.png"]];
	
	pageImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PaperPreview.png"]];
	// Frames are set in view will appear
	[self.view addSubview:canvassImage];
	[self.view addSubview:pageImage];
	
	// Load the table view
	[self loadTableView];
	
	// Load the header (lower right)
	[self loadHeader];
	
	// Load the frame/border
	borderImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"RedRocketBorder.png"]];
	[self.view addSubview:borderImage];
	
	// Load the control panel objects
	[self loadControlPanelObjects];
	
	// Load the buttons
	[self loadButtons];
	
	// Get rid of the left bar button item
	[self.navigationItem setHidesBackButton:YES];
    
#ifdef IS_FREE_BUILD
    //agltb3B1Yi1pbmNyDQsSBFNpdGUY-tDVFAw
    _adView = [[MPAdView alloc] initWithAdUnitId:kMoPubAdUnitID size:MOPUB_LEADERBOARD_SIZE];
    _adView.delegate = self;
    
    CGRect frame = _adView.frame;
    CGSize size = [_adView adContentViewSize];
    frame.origin.y = self.view.bounds.size.height - size.height;
    frame.origin.x = (self.view.bounds.size.width - size.width) / 2;
    _adView.frame = frame;
    
    
    [self.view addSubview:_adView];
    [self.view bringSubviewToFront:_adView];
    
    [_adView loadAd];
#endif
}

#ifdef IS_FREE_BUILD
// Better to place load calls in the view did load
- (void)viewDidLoad {
    [super viewDidLoad];
    
	
}
#endif

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	// Load/init related objects
	myTableView = [[UITableView alloc] initWithFrame:kListTableViewLandscape style:UITableViewStylePlain];
	[myTableView setBackgroundColor:[UIColor clearColor]];
	[myTableView setDataSource:self];
	[myTableView setDelegate:self];
	
	headerLine1 = [[UIView alloc] initWithFrame:kHeaderLine1Portrait];
	[headerLine1 setBackgroundColor:[UIColor lightGrayColor]];
	
	headerLine2 = [[UIView alloc] initWithFrame:kHeaderLine2Portrait];
	[headerLine2 setBackgroundColor:[UIColor lightGrayColor]];
	
	// Add to the view
	[self.view addSubview:myTableView];
	[self.view addSubview:headerLine1];
	[self.view addSubview:headerLine2];
}


- (void)loadButtons {
	// Load lists bar button item and add it if the view is portrait
	panelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Panel" style:UIBarButtonItemStyleBordered
														 target:self action:@selector(panelBarButtonItemAction)];
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[self.navigationItem setLeftBarButtonItem:panelBarButtonItem animated:NO];
	}
	
	// Init archive completed items button
	archiveCompletedItemsButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
	NSInteger numberOfCompletedTasks = [refMasterTaskListCollection getNumberOfCompletedListItems];
	NSString *buttonTitle = [NSString stringWithFormat:@"Archive %d completed tasks", numberOfCompletedTasks];
	if (numberOfCompletedTasks == 1) {
		buttonTitle = @"Archive 1 completed task";
	}
	[archiveCompletedItemsButton setTitle:buttonTitle forState:UIControlStateNormal];
	[archiveCompletedItemsButton addTarget:self action:@selector(archiveCompletedItemsButtonAction)
						  forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:archiveCompletedItemsButton];
	
	// Init archive completed lists button
	archiveCompletedListsButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
	NSInteger numberOfCompletedLists = [refMasterTaskListCollection getNumberOfCompletedLists];
	NSString *buttonTitle2= [NSString stringWithFormat:@"Archive %d completed lists", numberOfCompletedLists];
	if (numberOfCompletedLists == 1) {
		buttonTitle2 = @"Archive 1 completed list";
	}
	[archiveCompletedListsButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
	[archiveCompletedListsButton setTitle:buttonTitle2 forState:UIControlStateNormal];
	[archiveCompletedListsButton addTarget:self action:@selector(archiveCompletedListsButtonAction)
						  forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:archiveCompletedListsButton];
	
	// Init close archives button
	closeArchivesButton = [[UIButton alloc] init];
	[closeArchivesButton setBackgroundImage:[UIImage imageNamed:@"ArchivesSelectedButton.png"] forState:UIControlStateNormal];
	[closeArchivesButton addTarget:self action:@selector(closeArchivesButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:closeArchivesButton];
	
	archiveCompletedFooterLine = [[UIView alloc] initWithFrame:CGRectZero];
	[archiveCompletedFooterLine setBackgroundColor:[UIColor lightGrayColor]];
	[self.view addSubview:archiveCompletedFooterLine];
    
	if (numberOfCompletedLists == 0 || [refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
		[self hideArchiveCompleteLists:YES];
	}
	if (numberOfCompletedTasks == 0 || [refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
		[self hideArchiveCompletedItems:YES];
		//[archiveCompletedItemsButton setHidden:YES];
	}
	
	// Init and maybe show archive check bar button item
	/*archiveCheckBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Update Archives" style:UIBarButtonItemStyleBordered target:self
     action:@selector(archiveCheckBarButtonItemAction)];
     
     if ([refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
     [self.navigationItem setRightBarButtonItem:archiveCheckBarButtonItem animated:NO];
     }*/
	
	
	/*taskListSegmentedControl = [[CustomSegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Tasks", @"Lists", nil]
     offColor:[UIColor colorWithRed:0.98 green:0.98 blue:0.98 alpha:1.0]
     onColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0]];
     [taskListSegmentedControl setImage:[UIImage imageNamed:@"ListText.png"] forSegmentAtIndex:0];
     [taskListSegmentedControl setImage:[UIImage imageNamed:@"DateText.png"] forSegmentAtIndex:1];
     [taskListSegmentedControl addTarget:self action:@selector(taskListSegmentedControlAction) forControlEvents:UIControlEventValueChanged];
     [taskListSegmentedControl setSegmentedControlStyle:UISegmentedControlStyleBar];
     [taskListSegmentedControl setSelectedSegmentIndex:0];
     [self.view addSubview:taskListSegmentedControl];*/
	
	/*if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
     [archiveCompletedItemsButton setFrame:kArchiveCompletedItemsButtonPortrait];
     [closeArchivesButton setFrame:kArchivesImagePortrait];
     } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
     [archiveCompletedItemsButton setFrame:kArchiveCompletedItemsButtonLandscape];
     [closeArchivesButton setFrame:kArchivesImageLandscape];
     }*/
}

- (void)loadControlPanelObjects {
	// Load the control panel
	controlPanelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPanel.png"]];
	[self.view addSubview:controlPanelImageView];
	
	// List paper image view
	listPadImageView = [[UIImageView alloc] init];
	[listPadImageView setImage:[UIImage imageNamed:@"ListPaper.png"]];
	[self.view addSubview:listPadImageView];
	
	// Load the archived lists responder
	archivedListsTableViewResponder = [[ArchivedListsTableViewResponder alloc] initWithArchiveList:archiveList];
	// Set the delegate
	archivedListsTableViewResponder.delegate = self;
	listPadTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
	[listPadTableView setDataSource:archivedListsTableViewResponder];
	[listPadTableView setDelegate:archivedListsTableViewResponder];
	[listPadTableView setBackgroundColor:[UIColor clearColor]];
	[listPadTableView setShowsVerticalScrollIndicator:NO];
	[self.view addSubview:listPadTableView];
	
	// Load list pad header lines
	listPadHeaderLine1 = [[UIView alloc] initWithFrame:CGRectZero];
	[listPadHeaderLine1 setBackgroundColor:[UIColor lightGrayColor]];
	[listPadHeaderLine1 setAlpha:0.7f];
	[self.view addSubview:listPadHeaderLine1];
	
	listPadHeaderLine2 = [[UIView alloc] initWithFrame:CGRectZero];
	[listPadHeaderLine2 setBackgroundColor:[UIColor lightGrayColor]];
	[listPadHeaderLine2 setAlpha:0.7f];
	[self.view addSubview:listPadHeaderLine2];
	
	listPadFooterLine = [[UIView alloc] initWithFrame:CGRectZero];
	[listPadFooterLine setBackgroundColor:[UIColor lightGrayColor]];
	[listPadFooterLine setAlpha:0.7f];
	[self.view addSubview:listPadFooterLine];
	
	listPadArchivedFooterLine = [[UIView alloc] initWithFrame:CGRectZero];
	[listPadArchivedFooterLine setBackgroundColor:[UIColor lightGrayColor]];
	[listPadArchivedFooterLine setAlpha:0.7f];
	[self.view addSubview:listPadArchivedFooterLine];
	
	// Load list pad title label
	listPadTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	[listPadTitleLabel setBackgroundColor:[UIColor clearColor]];
	[listPadTitleLabel setTextColor:[UIColor lightGrayColor]];
	[listPadTitleLabel setTextAlignment:UITextAlignmentCenter];
	[listPadTitleLabel setText:@"Archived Lists"];
	[listPadTitleLabel setFont:[UIFont fontWithName:@"MarkerFelt-Thin" size:23.0]];
	[self.view addSubview:listPadTitleLabel];
}

- (void)loadHeader {
	// Load title
	titleLabel = [[UILabel alloc] init];
	[titleLabel setBackgroundColor:[UIColor clearColor]];
	[titleLabel setTextAlignment:UITextAlignmentRight];
	[titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:30.0]];
	[titleLabel setText:@"Archived Tasks"];
	[self.view addSubview:titleLabel];
	
	// Load switch
	scribblesOnOffSwitch = [[UISwitch alloc] init];
	[scribblesOnOffSwitch addTarget:self action:@selector(scribblesOnOffSwitchAction)
				   forControlEvents:UIControlEventValueChanged];
	[self.view addSubview:scribblesOnOffSwitch];
	
	// Load scribbles on off label
	scribblesOnOffLabel = [[UILabel alloc] init];
	[scribblesOnOffLabel setBackgroundColor:[UIColor clearColor]];
	[scribblesOnOffLabel setTextAlignment:UITextAlignmentRight];
	[scribblesOnOffLabel setText:@"Scribbles"];
	[self.view addSubview:scribblesOnOffLabel];
}

#pragma mark - MPAdViewDelegate Methods
#ifdef IS_FREE_BUILD
- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}

- (void)adViewDidLoadAd:(MPAdView *)view {
    CGSize size = [view adContentViewSize];
    CGRect newFrame = view.frame;
    
    newFrame.size = size;
    newFrame.origin.x = (self.view.bounds.size.width - size.width) / 2;
    view.frame = newFrame;
}
#endif

#pragma mark -
#pragma mark Button Actions

/*- (void)taskListSegmentedControlAction {
 if (taskListSegmentedControl.selectedSegmentIndex == currentArchiveType) {
 // Do nothing
 return;
 }
 
 currentArchiveType = taskListSegmentedControl.selectedSegmentIndex;
 
 if (currentArchiveType == EnumCurrentArchiveTypeTasks) {
 titleLabel.text = @"Archived Tasks";
 [scribblesOnOffSwitch setEnabled:TRUE];
 } else {
 titleLabel.text = @"Archived Lists";
 [scribblesOnOffSwitch setEnabled:FALSE];
 }
 
 // Reload the table view
 [myTableView reloadData];
 }*/


- (void)panelBarButtonItemAction {
	if ([panelPopoverController isPopoverVisible]) {
		[panelPopoverController dismissPopoverAnimated:YES];
        [panelPopoverController release];
	}
	
	ArchivesPanelViewController *controller = [[[ArchivesPanelViewController alloc] initWithArchivedListsTableViewResponder:archivedListsTableViewResponder
																								andMasterTaskListCollection:refMasterTaskListCollection] autorelease];
	
	controller.refArchivesViewController = self;
	
	panelPopoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
	
	[panelPopoverController presentPopoverFromBarButtonItem:panelBarButtonItem
								   permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


- (void)scribblesOnOffSwitchAction {
	[myTableView reloadData];
}

- (void)archiveCompletedListsButtonAction {
	// Close the panel popover if it is visible
	if ([panelPopoverController isPopoverVisible]) {
		[panelPopoverController dismissPopoverAnimated:YES];
        [panelPopoverController release];
	}
	
	// Need to give warning first, so also need to declare UIAlertViews
	archiveListsAlertView = [[UIAlertView alloc] initWithTitle:@"Note"
                                                       message:@"All tasks in these lists will also be archived"
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Ok", nil];
	
	[archiveListsAlertView show];
}

- (void)archiveCompletedItemsButtonAction {
	// Go through and archive items
	NSInteger numberOfTasksUnableToComplete = 0;
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		numberOfTasksUnableToComplete += [taskList archiveAllCompletedItemsToArchiveList:archiveList];
	}
	
	if (numberOfTasksUnableToComplete > 0) {
		NSString *alertMessage = [NSString stringWithFormat:@"%d tasks could not be archived.  This is usually due to an incomplete task or subtask.", numberOfTasksUnableToComplete];
		
		if (numberOfTasksUnableToComplete == 1) {
			alertMessage = @"1 task could not be archived.  This is usually due to an incomplete task or subtask.";
		}
		
		// No need to react to this
		unableToArchiveTasksAlertView = [[UIAlertView alloc] initWithTitle:@"Note"
																   message:alertMessage
																  delegate:nil
														 cancelButtonTitle:@"Ok"
														 otherButtonTitles:nil];
		
		[unableToArchiveTasksAlertView show];
	}
	
	// Hide the button after we are done (use animation)
	[UIView beginAnimations:@"Hide Archive Completed Button" context:nil];
	[UIView setAnimationDuration:0.5f];
	[self hideArchiveCompletedItems:YES];
	[UIView commitAnimations];
	
	
	[delegate archivesUpdated];
	
	// Reload the tableview
	[myTableView reloadData];
}

- (void)closeArchivesButtonAction {
	if ([panelPopoverController isPopoverVisible]) {
		[panelPopoverController dismissPopoverAnimated:YES];
        [panelPopoverController release];
	}
	
	if (hasUpdatedListItems) {
		[self.delegate archivesUpdated];
	}
	
	[self.navigationController popViewControllerAnimated:NO];
}

/*- (void)archiveCheckBarButtonItemAction {
 NSString *archiveSetting = refMasterTaskListCollection.archiveSetting;
 
 NSInteger archivedListCount = 0;
 NSInteger archivedListItemCount = 0;
 
 if ([archiveSetting isEqualToString:@"On Application Launch"]) {
 // Archive now, do listitems first
 for (TaskList *taskList in refMasterTaskListCollection.lists) {
 archivedListItemCount += [taskList archiveAllCompletedItems:TRUE];
 }
 
 archivedListCount = [refMasterTaskListCollection archiveAllCompletedListsToArchiveList:nil];
 } else if ([archiveSetting isEqualToString:@"Daily"]) {
 // Archive now, do listitems first
 for (TaskList *taskList in refMasterTaskListCollection.lists) {
 archivedListItemCount += [taskList archiveAllCompletedItemsUsingDuration:1];
 }
 
 archivedListCount = [refMasterTaskListCollection archiveAllCompletedListsUsingDuration:1];
 } else if ([archiveSetting isEqualToString:@"Weekly"]) {
 // Archive now, do listitems first
 for (TaskList *taskList in refMasterTaskListCollection.lists) {
 archivedListItemCount += [taskList archiveAllCompletedItemsUsingDuration:7];
 }
 
 archivedListCount = [refMasterTaskListCollection archiveAllCompletedListsUsingDuration:7];
 }
 
 // Refresh the archives
 if (archivedListCount > 0) {
 [archiveList.lists removeAllObjects];
 [archiveList loadBasicArchivedLists];
 }
 
 if (archivedListItemCount > 0) {
 [archiveList.listItems removeAllObjects];
 [archiveList loadArchivedListItems];
 }
 
 // Show an alert
 NSString *message = @"";
 if (archivedListCount == 0 && archivedListItemCount == 0) {
 if ([archiveSetting isEqualToString:@"On Application Launch"]) {
 message = @"No completed lists or valid completed tasks found to be archived";
 } else if ([archiveSetting isEqualToString:@"Daily"]) {
 message = @"No completed lists or valid completed tasks over a day old found";
 } else if ([archiveSetting isEqualToString:@"Weekly"]) {
 message = @"No completed lists or valid completed tasks over a week old found";
 }
 } else if (archivedListCount > 0 && archivedListItemCount == 0) {
 [self.delegate archiveListsUpdated];
 hasUpdatedListItems = TRUE;
 if (archivedListCount == 1) {
 message = @"Successfully archived one list";
 } else {
 message = [NSString stringWithFormat:@"Successfully archived %d lists", archivedListCount];
 }
 } else if (archivedListCount == 0 && archivedListItemCount > 0) {
 hasUpdatedListItems = TRUE;
 if (archivedListItemCount == 1) {
 message = @"Successfully archived one completed task";
 } else {
 message = [NSString stringWithFormat:@"Successfully archived %d completed tasks", archivedListItemCount];
 }
 } else {
 [self.delegate archiveListsUpdated];
 hasUpdatedListItems = TRUE;
 if (archivedListCount == 1 && archivedListItemCount == 1) {
 message = @"Successfully archived one task and one list";
 } else if (archivedListCount > 1 && archivedListItemCount == 1) {
 message = [NSString stringWithFormat:@"Successfully archived one completed task and %d lists", archivedListCount];
 } else if (archivedListCount == 1 && archivedListItemCount > 1) {
 message = [NSString stringWithFormat:@"Successfully archived one list and %d completed tasks", archivedListItemCount];
 } else {
 message = [NSString stringWithFormat:@"Successfully archived %d lists and %d completed tasks", archivedListCount, archivedListItemCount];
 }
 }
 
 if (hasUpdatedListItems == TRUE) {
 [self.delegate archivesUpdated];
 }
 
 [MethodHelper showAlertViewWithTitle:@"Archive Sync"
 andMessage:message
 andButtonTitle:@"Ok"];
 
 // Reload the tableview
 [myTableView reloadData];
 
 }*/

#pragma mark -
#pragma mark Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if ([panelPopoverController isPopoverVisible]) {
		[panelPopoverController dismissPopoverAnimated:YES];
        [panelPopoverController release];
	}
	
	
	if ([alertView isEqual:archiveListsAlertView]) {
		if (buttonIndex == 0) {
			return;	// Cancel
		} else if (buttonIndex == 1) {
			// Tell main view that archive lists have been updated
			[self.delegate archiveListsUpdated];
			
			// Set true for has updated list items
			hasUpdatedListItems = TRUE;
			
			// Archive the lists
			[refMasterTaskListCollection archiveAllCompletedListsToArchiveList:archiveList];
            
			// Need to reload the list
			[listPadTableView reloadData];
            
			// Also need to update the local tableview as listitems may also be archived
			[myTableView reloadData];
			
			// Now check whether there are any list items left to be archived before hiding list items
			NSInteger numberOfCompletedTasks = [refMasterTaskListCollection getNumberOfCompletedListItems];
			NSString *buttonTitle = [NSString stringWithFormat:@"Archive %d completed tasks", numberOfCompletedTasks];
			if (numberOfCompletedTasks == 1) {
				buttonTitle = @"Archive 1 completed task";
			}
			[archiveCompletedItemsButton setTitle:buttonTitle forState:UIControlStateNormal];
			
			[UIView beginAnimations:@"Hide Archive Completed Button" context:nil];
			[UIView setAnimationDuration:0.5f];
			// Hide archive completed listitems and lists button
			[self hideArchiveCompleteLists:YES];
			
			if (numberOfCompletedTasks == 0) {
				[self hideArchiveCompletedItems:YES];
			} else if (numberOfCompletedTasks > 0) {
				[self hideArchiveCompletedItems:NO];
			}
			
			[UIView commitAnimations];
		}
	}
	
	// Warning before restoring task lists
	if ([alertView isEqual:restoreTaskListAlertView]) {
		if (buttonIndex == 0) {
			return;	// Cancel
		} else if (buttonIndex == 1) {
			// Tell main view that archive lists have been updated
			[self.delegate archiveListsUpdated];
			
			// Set true for has updated list items
			hasUpdatedListItems = TRUE;
			
			TaskList *taskList = [archiveList.lists objectAtIndex:currentlySelectedList];
			
			// Set archived and completed to false
			taskList.archived = FALSE;
			taskList.completedDateTime = @"";
			taskList.completed = FALSE;
			taskList.archivedDateTime = @"";
			
			taskList.listOrder = [taskList getNextListOrder];
			
			// First copy and add list items
			/*for (ListItem *listItem in archiveList.listItems) {
             if ([listItem listID] == taskList.listID) {
             // Get the next list item order
             NSInteger nextListItemOrder = [taskList getNextListItemOrder];
             
             // Update the list item
             listItem.listID = [taskList listID];
             listItem.itemOrder = nextListItemOrder;
             listItem.archived = FALSE;
             listItem.archivedDateTime = @"";
             
             // Need to remove completed status when moving back, and remove completed date time
             listItem.completed = FALSE;
             listItem.completedDateTime = @"";
             
             // Update the database
             [listItem updateDatabase];
             
             [taskList.listItems addObject:listItem];
             }
             }
             
             // Go through list item backwards, removing the list items we just added
             for (int i = [archiveList.listItems count] - 1; i >= 0; i--) {
             ListItem *listItem = [archiveList.listItems objectAtIndex:i];
             
             if ([listItem listID] == taskList.listID) {
             // Remove object from archive list
             [archiveList.listItems removeObjectAtIndex:i];
             }
             }*/
			
			// Go through the list items backwards
			for (int i = [archiveList.listItems count] - 1; i >= 0; i--) {
				ListItem *listItem = [archiveList.listItems objectAtIndex:i];
				
				if ([listItem listID] == taskList.listID) {
					// Found, restore this list item
					ListItem *listItemCopy = [[ListItem alloc] initWithListItemCopy:listItem];
					
					// Remove the current list item from archives list items
					[archiveList.listItems removeObjectAtIndex:i];
					
					// Get the next list item order
					NSInteger nextListItemOrder = [taskList getNextListItemOrder];
					
					// Update the list item
					listItemCopy.listID = [taskList listID];
					listItemCopy.itemOrder = nextListItemOrder;
					listItemCopy.archived = FALSE;
					listItemCopy.archivedDateTime = @"";
					listItemCopy.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
					
					// Need to remove completed status when moving back, and remove completed date time
					listItemCopy.completed = FALSE;
					listItemCopy.completedDateTime = @"";
					
					// Update the database
					[listItemCopy updateDatabase];
					
					// Add the list item copy to the task list
					[taskList.listItems insertObject:listItemCopy atIndex:0];
					
					// Now get rid of our list item copy
					[listItemCopy release];
				}
			}
			
			// Update the database
			[taskList updateDatabase];
			
			TaskList *taskListCopy = [[TaskList alloc] initWithTaskListCopy:taskList];
			
			// Insert task list copy into master collection (change this later depending on first/last setting)
			if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
				[refMasterTaskListCollection.lists insertObject:taskListCopy atIndex:0];
			} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
				[refMasterTaskListCollection.lists addObject:taskListCopy];
			}
			
			
			// Release our copy
			[taskListCopy release];
			
			// Remove task list from archive list
			[archiveList.lists removeObjectAtIndex:currentlySelectedList];
			
			// Reload archived list table view and listitems tableview
			[listPadTableView reloadData];
			[myTableView reloadData];
		}
	}
	
}

#pragma mark -
#pragma mark Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	if (currentArchiveType == EnumCurrentArchiveTypeTasks) {
		return [archiveList.listItems count];
	} else {
		return [archiveList.lists count];
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
	// Would prefer not to, but have to use if/else, at least it is localized
	ListItemTableViewCell *cell = (ListItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[[ListItemTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
	}
	
	// Set up the cell...
	ListItem *listItem = [archiveList.listItems objectAtIndex:indexPath.row];
	
	cell.isArchived = TRUE;
	
    // Set indent if this is subtask
	[cell setIndentForListItem:listItem];
    
	// Set up tags
	[cell loadTagsForListItem:listItem andTagCollection:refTagCollection];
	
	// Load scribble image (for now), but later give option
	if ([scribblesOnOffSwitch isOn]) {
		[cell loadScribbleImageForListItem:listItem andIsPreviewList:NO];
	} else {
		cell.scribbleImageView.image = nil;
	}
    
	
	cell.tag = listItem.listItemID;
	cell.taskCompleted.tag = listItem.listItemID;
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.taskItem = [NSString stringWithFormat:@"%@", listItem.title];
	
	cell.taskDate = @"";
	
	// Display cell notes icon if notes is not empty
	if ([listItem.notes length] > 0) {
		[cell loadCellNotesImage];
	} else {
		cell.cellNotesImageView.image = nil;
	}
	
	
	
	[cell.taskCompleted setSelected:listItem.completed];
	[cell.taskCompleted setAlpha:0.3f];
	if ([listItem completed] == FALSE) {
		[cell setHighlightStyle:listItem.priority];
	} else {
		[cell setHighlightStyle:EnumHighlighterColorNone];
	}
    
	
	return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// We are only going to allow movement back to lists, no updating completed or anything.
	if (currentArchiveType == EnumCurrentArchiveTypeLists) {
		return indexPath;
	}
	
	// Update the selected row
	selectedRow = indexPath.row;
	
	ListItemTableViewCell *cell = (ListItemTableViewCell *)[myTableView cellForRowAtIndexPath:indexPath];
	
	// For correctly positioning the popover controller
	CGPoint offset = [myTableView contentOffset];
	
	// Multiply selected row by 44, get center of tableview
	CGFloat xOrigin = myTableView.frame.origin.x + (myTableView.frame.size.width - 320);
	CGFloat yOrigin = cell.frame.origin.y + 22 + myTableView.frame.origin.y - offset.y;
	CGRect rectPopover = CGRectMake(xOrigin, yOrigin, 1, 1);
	
	// Get the selected list item
	ListItem *listItem = [archiveList.listItems objectAtIndex:[archiveList getIndexOfListItemWithID:cell.tag]];
	
	// Init and show the edit list item view controller
	MoveItemsViewController *moveItemsViewController = [[MoveItemsViewController alloc]
														initWithTaskListCollection:refMasterTaskListCollection
														andListItem:listItem
														andNavigationBar:NO];
	moveItemsViewController.delegate = self;
	moveItemsViewController.refArchiveList = archiveList;
	moveItemsViewController.refTagCollection = refTagCollection;
	
	// Init the navigation controller that will hold the move item view controller
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:moveItemsViewController];
	
	// Allocate the popover controller, that will host the nav controller and view controller
	moveItemsPopoverController = [[UIPopoverController alloc] initWithContentViewController:navController];
	
	[moveItemsViewController release];
	[navController release];
	
	moveItemsPopoverController.delegate = self;
	
	[moveItemsPopoverController presentPopoverFromRect:rectPopover
												inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft
											  animated:YES];
	
	return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
}

#pragma mark -
#pragma mark Archived Lists Delegate

- (void)taskListSelected:(TaskList *)taskList atLocation:(CGRect)popoverRect withIndex:(NSInteger)theIndex {
	// Set the currently selected row
	currentlySelectedList = theIndex;
	
	NSString *message = [NSString stringWithFormat:@"'%@' will be restored, along with any of its original tasks", taskList.title];
	restoreTaskListAlertView = [[UIAlertView alloc] initWithTitle:@"Restore List?"
                                                          message:message
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Ok", nil];
	[restoreTaskListAlertView show];
}

#pragma mark -
#pragma mark Move Items Delegates

- (void)didDismissMoveItemsView {
	// Close the move items popover
	if ([moveItemsPopoverController isPopoverVisible]) {
		[moveItemsPopoverController dismissPopoverAnimated:YES];
        [moveItemsPopoverController release];
	}
}

- (void)didUpdateMoveItemsView {
	// Close the move items popover
	if ([moveItemsPopoverController isPopoverVisible]) {
		[moveItemsPopoverController dismissPopoverAnimated:YES];
        [moveItemsPopoverController release];
	}
	
	// Get a new count of tasks that are completed
	NSInteger numberOfCompletedTasks = [refMasterTaskListCollection getNumberOfCompletedListItems];
	NSString *buttonTitle = [NSString stringWithFormat:@"Archive %d completed tasks", numberOfCompletedTasks];
	
	if (numberOfCompletedTasks == 1) {
		buttonTitle = @"Archive 1 completed task";
	}
	[archiveCompletedItemsButton setTitle:buttonTitle forState:UIControlStateNormal];
	
	if ([archiveCompletedItemsButton isHidden]) {
		[UIView beginAnimations:@"Show Archive Completed Button" context:nil];
		[UIView setAnimationDuration:0.5f];
		[self hideArchiveCompletedItems:NO];
		[UIView commitAnimations];
	}
	
	// To request that the main view controller update itself
	hasUpdatedListItems = TRUE;
	
	// Reload the table view
	[myTableView reloadData];
	
	// Need to do any updates by informing the list view controller
	//[self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)updateMoveItemsSelectedRow:(NSInteger)newMoveItemsSelectedRow {
	moveItemsSelectedRow = newMoveItemsSelectedRow;
}

#pragma mark -
#pragma mark PopOver Controller Delegates


// Called on the delegate when the popover controller will dismiss the popover. Return NO to prevent the dismissal of the view.
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
	// Prevent move items popover controller from closing
	//if ([popoverController isEqual:moveItemsPopoverController]) {
	//	return NO;
	//}
    
	
	return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	if ([popoverController isEqual:moveItemsPopoverController]) {
		// Check to see if currently selected row is not -1 (means needs to move)
		if (moveItemsSelectedRow != -1) {
			[self updateMovedListItem];
		}
        
	}
}


#pragma mark -
#pragma mark Class Methods

- (void)updateMovedListItem {
	// We can get the current list item by grabbing it from selected row
	ListItemTableViewCell *cell = (ListItemTableViewCell *)[myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow
																												 inSection:0]];
	
	// Get the selected list item
	ListItem *listItem = [archiveList.listItems objectAtIndex:[archiveList getIndexOfListItemWithID:cell.tag]];
	
	// Get the currently selected parent list id
	TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:moveItemsSelectedRow];
	taskList.mainViewRequiresUpdate = TRUE;
	
	// Move item from archives to the correct list
	NSInteger listItemIndex = [archiveList getIndexOfListItemWithID:listItem.listItemID];
	
	// Get copy of list item from archives list
	ListItem *listItemCopy = [[ListItem alloc] initWithListItemCopy:[archiveList.listItems objectAtIndex:listItemIndex]];
	
	// Remove the list item from archives list
	[archiveList.listItems removeObjectAtIndex:listItemIndex];
	
	// Get the next list item order
	NSInteger nextListItemOrder = [taskList getNextListItemOrder];
	
	// Update the list item
	listItemCopy.listID = [taskList listID];
	listItemCopy.itemOrder = nextListItemOrder;
	listItemCopy.archived = FALSE;
	listItemCopy.archivedDateTime = @"";
	listItemCopy.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	
	// Need to remove completed status when moving back, and remove completed date time
	listItemCopy.completed = FALSE;
	listItemCopy.completedDateTime = @"";
	
	// Check if this is a subtask
	BOOL listItemIsSubtask = FALSE;
	if ([listItemCopy parentListItemID] != -1) {
		// Remove subtask status
		[listItemCopy setParentListItemID:-1];
		listItemIsSubtask = TRUE;
	}
	
	// Update the database
	[listItemCopy updateDatabase];
	
	// Add the list item copy to the task list
	[taskList.listItems addObject:listItemCopy];
	
	// At end of moving, look for any subtasks, and also move them
	if (listItemIsSubtask == FALSE) {
		for (int i = [archiveList.listItems count] - 1; i >= 0; i--) {
			ListItem *subListItem = [archiveList.listItems objectAtIndex:i];
            
			if ([subListItem parentListItemID] == listItemCopy.listItemID) {
				subListItem.itemOrder = listItemCopy.itemOrder;
				subListItem.listID = listItemCopy.listID;
				
				// Get copy of the sub list item
				ListItem *subListItemCopy = [[ListItem alloc] initWithListItemCopy:subListItem];
				[taskList.listItems addObject:subListItemCopy];
				
				// Remove original sub list item
				[archiveList.listItems removeObjectAtIndex:i];
				
				// Update the database
				[subListItemCopy updateDatabase];
				
				// Release our copy
				[subListItemCopy release];
			}
		}
	}
	
	// Now get rid of our list item copy
	[listItemCopy release];
	
	[self didUpdateMoveItemsView];
}

- (void)hideArchiveCompletedItems:(BOOL)hide {
	if (hide == YES) {
		[archiveCompletedFooterLine setAlpha:0.0f];
		[archiveCompletedItemsButton setAlpha:0.0f];
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
			[myTableView setFrame:kListTableViewPortrait];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
			[myTableView setFrame:kListTableViewLandscape];
		}
	} else {
		[archiveCompletedFooterLine setAlpha:1.0f];
		[archiveCompletedItemsButton setAlpha:1.0f];
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
			[myTableView setFrame:kArchiveButtonTableViewPortrait];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
			[myTableView setFrame:kArchiveButtonTableViewLandscape];
		}
	}
}

- (void)hideArchiveCompleteLists:(BOOL)hide {
	if (hide == YES) {
		[archiveCompletedListsButton setAlpha:0.0f];
		[listPadArchivedFooterLine setAlpha:0.0f];
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
			[listPadTableView setFrame:kArchiveListPadTableViewPortrait];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
			[listPadTableView setFrame:kArchiveListPadTableViewLandscape];
		}
	} else {
		[archiveCompletedListsButton setAlpha:1.0f];
		[listPadArchivedFooterLine setAlpha:0.7f];
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
			[listPadTableView setFrame:kListPadShortTableViewPortrait];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
			[listPadTableView setFrame:kListPadShortTableViewLandscape];
		}
	}
}

#pragma mark -
#pragma mark UIViewController Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}

#ifdef IS_FREE_BUILD
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	// Rotate ad view
	[_adView rotateToOrientation:toInterfaceOrientation];
}
#endif

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	if ([moveItemsPopoverController isPopoverVisible]) {
		[myTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]
						   atScrollPosition:UITableViewScrollPositionMiddle
								   animated:NO];
		
		CGPoint offset = [myTableView contentOffset];
		
		UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow
																					  inSection:0]];
		
		// Multiply selected row by 44, get center of tableview
		CGFloat xOrigin = myTableView.frame.origin.x + (myTableView.frame.size.width - 320);
		CGFloat yOrigin = cell.frame.origin.y + 22 + myTableView.frame.origin.y - offset.y;
		CGRect rectPopover = CGRectMake(xOrigin, yOrigin, 1, 1);
		
		[moveItemsPopoverController setDelegate:self];
		
		[moveItemsPopoverController presentPopoverFromRect:rectPopover
                                                    inView:self.view
                                  permittedArrowDirections:UIPopoverArrowDirectionLeft
                                                  animated:YES];
	}
#ifdef IS_FREE_BUILD
    // Obtain the new size of the ad content.
    CGSize size = [_adView adContentViewSize];
    
    // Repositioning example: keep the ad view centered horizontally.
    CGRect newFrame = _adView.frame;
    newFrame.size = size;
    newFrame.origin.x = (self.view.bounds.size.width - size.width) / 2;
    newFrame.origin.y = self.view.bounds.size.height - size.height;
    _adView.frame = newFrame;
#endif
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
	if ([panelPopoverController isPopoverVisible]) {
		[panelPopoverController dismissPopoverAnimated:NO];
        [panelPopoverController release];
	}
	
	if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
		[canvassImage setFrame:kCanvassImagePortrait];
		[pageImage setFrame:kPageImagePortrait];
		[borderImage setFrame:kBorderImagePortrait];
		// Table view objects
		if ([refMasterTaskListCollection getNumberOfCompletedListItems] == 0 ||
			[refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
			[myTableView setFrame:kListTableViewPortrait];
		} else {
			[myTableView setFrame:kArchiveButtonTableViewPortrait];
		}
		[headerLine1 setFrame:kHeaderLine1Portrait];
		[headerLine2 setFrame:kHeaderLine2Portrait];
		
		if ([refMasterTaskListCollection getNumberOfCompletedLists] == 0 ||
			[refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
			[self hideArchiveCompleteLists:YES];
			[listPadTableView setFrame:kArchiveListPadTableViewPortrait];
		} else {
			[self hideArchiveCompleteLists:NO];
			[listPadTableView setFrame:kListPadShortTableViewPortrait];
		}
		
		// Control Panel objects
		[controlPanelImageView setFrame:kMainControlPanelImagePortrait];
		[listPadImageView setFrame:kListPadImagePortrait];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Portrait];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Portrait];
		[listPadFooterLine setFrame:kListPadFooterLinePortrait];
		[listPadArchivedFooterLine setFrame:kListPadArchivedFooterLinePortrait];
		[listPadTitleLabel setFrame:kListPadTitleLabelPortrait];
		// Buttons
		[archiveCompletedItemsButton setFrame:kArchiveCompletedItemsButtonPortrait];
		[closeArchivesButton setFrame:kArchivesImagePortrait];
		[archiveCompletedFooterLine setFrame:kArchiveCompletedFooterLinePortrait];
		[archiveCompletedListsButton setFrame:kArchiveCompletedListsButtonPortrait];
		// Header objects
		[titleLabel setFrame:kTitleLabelPortrait];
		[scribblesOnOffSwitch setFrame:kScribblesOnOffSwitchPortrait];
		[scribblesOnOffLabel setFrame:kScribblesOnOffLabelPortrait];
		
		[self.navigationItem setLeftBarButtonItem:panelBarButtonItem animated:YES];
	} else if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
		
		[pageImage setFrame:kPageImageLandscape];
		[canvassImage setFrame:kCanvassImageLandscape];
		[borderImage setFrame:kBorderImageLandscape];
		// Table view objects
		if ([refMasterTaskListCollection getNumberOfCompletedListItems] == 0 ||
			[refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
			[myTableView setFrame:kListTableViewLandscape];
		} else {
			[myTableView setFrame:kArchiveButtonTableViewLandscape];
		}
		[headerLine1 setFrame:kHeaderLine1Landscape];
		[headerLine2 setFrame:kHeaderLine2Landscape];
		
		if ([refMasterTaskListCollection getNumberOfCompletedLists] == 0 ||
			[refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
			[self hideArchiveCompleteLists:YES];
			[listPadTableView setFrame:kArchiveListPadTableViewLandscape];
		} else {
			[self hideArchiveCompleteLists:NO];
			[listPadTableView setFrame:kListPadShortTableViewLandscape];
		}
		
		// Control panel objects
		[controlPanelImageView setFrame:kMainControlPanelImageLandscape];
		[listPadImageView setFrame:kListPadImageLandscape];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Landscape];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Landscape];
		[listPadFooterLine setFrame:kListPadFooterLineLandscape];
		[listPadArchivedFooterLine setFrame:kListPadArchivedFooterLineLandscape];
		[listPadTitleLabel setFrame:kListPadTitleLabelLandscape];
		// Buttons
		[archiveCompletedItemsButton setFrame:kArchiveCompletedItemsButtonLandscape];
		[closeArchivesButton setFrame:kArchivesImageLandscape];
		[archiveCompletedFooterLine setFrame:kArchiveCompletedFooterLineLandscape];
		[archiveCompletedListsButton setFrame:kArchiveCompletedListsButtonLandscape];
		// Header objects
		[titleLabel setFrame:kTitleLabelLandscape];
		[scribblesOnOffSwitch setFrame:kScribblesOnOffSwitchLandscape];
		[scribblesOnOffLabel setFrame:kScribblesOnOffLabelLandscape];
		
		[self.navigationItem setLeftBarButtonItem:nil animated:YES];
	}
	
}

- (void)applicationDidBecomeActive {
    if ([self.navigationController.topViewController isEqual:self] == TRUE) {
        if (gRunToodledoAutoSync == TRUE) {
            [MethodHelper showAlertViewWithTitle:@"Auto Sync"
                                      andMessage:@"Manage wants to sync with Toodledo.  Please return to 'My Lists' to automatically run the sync. (Note: This setting can be changed in 'Sync Settings')"
                                  andButtonTitle:@"Ok"];
        }
    }
    
    // Check if auto sync wants to run
    /* */
}

- (void)viewWillAppear:(BOOL)animated {
	[self reloadTheme];
	
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[canvassImage setFrame:kCanvassImagePortrait];
		[pageImage setFrame:kPageImagePortrait];
		[borderImage setFrame:kBorderImagePortrait];
		// Table view objects
		if ([refMasterTaskListCollection getNumberOfCompletedListItems] == 0 ||
			[refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
			[myTableView setFrame:kListTableViewPortrait];
		} else {
			[myTableView setFrame:kArchiveButtonTableViewPortrait];
		}
		[headerLine1 setFrame:kHeaderLine1Portrait];
		[headerLine2 setFrame:kHeaderLine2Portrait];
		
		if ([refMasterTaskListCollection getNumberOfCompletedLists] == 0 ||
			[refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
			[self hideArchiveCompleteLists:YES];
			[listPadTableView setFrame:kArchiveListPadTableViewPortrait];
		} else {
			[self hideArchiveCompleteLists:NO];
			[listPadTableView setFrame:kListPadShortTableViewPortrait];
		}
		
		// Control Panel objects
		[controlPanelImageView setFrame:kMainControlPanelImagePortrait];
		[listPadImageView setFrame:kListPadImagePortrait];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Portrait];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Portrait];
		[listPadFooterLine setFrame:kListPadFooterLinePortrait];
		[listPadArchivedFooterLine setFrame:kListPadArchivedFooterLinePortrait];
		[listPadTitleLabel setFrame:kListPadTitleLabelPortrait];
		// Buttons
		[archiveCompletedItemsButton setFrame:kArchiveCompletedItemsButtonPortrait];
		[closeArchivesButton setFrame:kArchivesImagePortrait];
		[archiveCompletedFooterLine setFrame:kArchiveCompletedFooterLinePortrait];
		[archiveCompletedListsButton setFrame:kArchiveCompletedListsButtonPortrait];
		// Header objects
		[titleLabel setFrame:kTitleLabelPortrait];
		[scribblesOnOffSwitch setFrame:kScribblesOnOffSwitchPortrait];
		[scribblesOnOffLabel setFrame:kScribblesOnOffLabelPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[pageImage setFrame:kPageImageLandscape];
		[canvassImage setFrame:kCanvassImageLandscape];
		[borderImage setFrame:kBorderImageLandscape];
		// Table view objects
		if ([refMasterTaskListCollection getNumberOfCompletedListItems] == 0 ||
			[refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
			[myTableView setFrame:kListTableViewLandscape];
		} else {
			[myTableView setFrame:kArchiveButtonTableViewLandscape];
		}
		[headerLine1 setFrame:kHeaderLine1Landscape];
		[headerLine2 setFrame:kHeaderLine2Landscape];
		
		if ([refMasterTaskListCollection getNumberOfCompletedLists] == 0 ||
			[refMasterTaskListCollection.archiveSetting isEqualToString:@"Manually"] == FALSE) {
			[self hideArchiveCompleteLists:YES];
			[listPadTableView setFrame:kArchiveListPadTableViewLandscape];
		} else {
			[self hideArchiveCompleteLists:NO];
			[listPadTableView setFrame:kListPadShortTableViewLandscape];
		}
		
		// Control panel objects
		[controlPanelImageView setFrame:kMainControlPanelImageLandscape];
		[listPadImageView setFrame:kListPadImageLandscape];
		[listPadHeaderLine1 setFrame:kListPadHeaderLine1Landscape];
		[listPadHeaderLine2 setFrame:kListPadHeaderLine2Landscape];
		[listPadFooterLine setFrame:kListPadFooterLineLandscape];
		[listPadArchivedFooterLine setFrame:kListPadArchivedFooterLineLandscape];
		[listPadTitleLabel setFrame:kListPadTitleLabelLandscape];
		// Buttons
		[archiveCompletedItemsButton setFrame:kArchiveCompletedItemsButtonLandscape];
		[closeArchivesButton setFrame:kArchivesImageLandscape];
		[archiveCompletedFooterLine setFrame:kArchiveCompletedFooterLineLandscape];
		[archiveCompletedListsButton setFrame:kArchiveCompletedListsButtonLandscape];
		// Header objects
		[titleLabel setFrame:kTitleLabelLandscape];
		[scribblesOnOffSwitch setFrame:kScribblesOnOffSwitchLandscape];
		[scribblesOnOffLabel setFrame:kScribblesOnOffLabelLandscape];
	}
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    // Check if auto sync wants to run
    if (gRunToodledoAutoSync == TRUE) {
        [MethodHelper showAlertViewWithTitle:@"Auto Sync"
                                  andMessage:@"Manage wants to sync with Toodledo.  Please return to 'My Lists' to automatically run the sync. (Note: This setting can be changed in 'Sync Settings')"
                              andButtonTitle:@"Ok"];
    }
    
	[super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark -
#pragma mark Helper Methods

- (void)reloadTheme {
	NSString *canvasImageName = [refMasterTaskListCollection getImageNameForSection:@"Canvas"];
	UIImage *newCanvasImage = [UIImage imageNamed:canvasImageName];
	[canvassImage setImage:newCanvasImage];
	
	NSString *borderImageName = [refMasterTaskListCollection getImageNameForSection:@"Border"];
	UIImage *newBorderImage = [UIImage imageNamed:borderImageName];
	[borderImage setImage:newBorderImage];
}

@end
