//
//  ApplicationSettingCollection.m
//  Manage
//
//  Created by Cliff Viegas on 4/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "ApplicationSettingCollection.h"

// Business Layer
#import "BLApplicationSetting.h"

// Data Models
#import "ApplicationSetting.h"

// DM Helper
#import "DMHelper.h"

@implementation ApplicationSettingCollection

@synthesize applicationSettings;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[self.applicationSettings release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		self.applicationSettings = [NSMutableArray array];
	}
	return self;
}

- (id)initWithAllUserSettings {
	if (self = [super init]) {
		self.applicationSettings = [NSMutableArray array];
		
		// Get the array of all user settings in the database
		NSArray *localArray = [BLApplicationSetting getAllApplicationSettings];
		
		// Create a new object for each setting taken from the database
		for (NSDictionary *settingDictionary in localArray) {
			NSString *theName = [DMHelper getNSStringValueForKey:@"Name" fromDictionary:settingDictionary];
			NSString *theData = [DMHelper getNSStringValueForKey:@"Data" fromDictionary:settingDictionary];
			
			ApplicationSetting *appSetting = [[[ApplicationSetting alloc] initWithName:theName 
																				andData:theData] autorelease];
			[self.applicationSettings addObject:appSetting];
		}
	}
	return self;
}

#pragma mark -
#pragma mark Get Methods

- (NSInteger)getIndexOfSettingWithID:(NSInteger)theSettingID {
	for (int i = 0; i < [self.applicationSettings count]; i++) {
		return i;
	}
	return 0;
}

- (ApplicationSetting *)getApplicationSettingWithName:(NSString *)theName {
	for (ApplicationSetting *appSetting in self.applicationSettings) {
		if ([[appSetting name] isEqualToString:theName]) {
			return appSetting;
		}
	}
	return [self.applicationSettings objectAtIndex:0];
}

@end
