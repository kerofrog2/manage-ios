//
//  DrawPointCollection.h
//  Manage
//
//  Created by Cliff Viegas on 23/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DrawPointCollection : NSObject {
	NSArray		*drawPoints;
}

@property (nonatomic, retain)	NSArray		*drawPoints;

#pragma mark Class Helper Methods
- (void)rotatePointsWithNewPoint:(CGPoint)newPoint;
- (void)addPoint:(CGPoint)newPoint;
- (BOOL)isPointsFull;
- (void)updateDrawPointAtIndex:(NSInteger)index withNewPoint:(CGPoint)newPoint;
- (void)clearDrawPoints;
- (void)resetDrawPoints;

@end
