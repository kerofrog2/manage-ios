//
//  DeletedTaskListCollection.h
//  Manage
//
//  Created by Cliff Viegas on 30/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class DeletedTaskList;

@interface DeletedTaskListCollection : NSObject {
	NSMutableArray		*deletedLists;
}

@property (nonatomic, retain)	NSMutableArray	*deletedLists;

#pragma mark Load Methods
- (void)loadWithListsDeletedAfter:(NSString *)dateTimeString checkForAlternateCopy:(BOOL)alternateCopy;

@end
