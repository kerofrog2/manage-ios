//
//  ListItemTagCollection.m
//  Manage
//
//  Created by Cliff Viegas on 10/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "ListItemTagCollection.h"

// Data Models
#import "ListItemTag.h"
#import "Tag.h"

// Business Layer
#import "BLTag.h"
#import "BLListItemTag.h"

// Dictionary Method Helper
#import "DMHelper.h"

// Data Collections
#import "TagCollection.h"

// Method Helper
#import "MethodHelper.h"

@implementation ListItemTagCollection

@synthesize listItemTags;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[self.listItemTags release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
    self = [super init];
	if (self) {
        //Tan Nguyen fix leaks
		//NSMutableArray * listItemTagsTemp = [[NSMutableArray alloc] initWithCapacity:10];
        self.listItemTags = [NSMutableArray array];// listItemTagsTemp;
        //[listItemTagsTemp release];
//        listItemTags = [NSMutableArray new];
	}
	return self;
}

- (id)initWithListItemTagsForID:(NSInteger)theListItemID {
    self = [super init];
	if (self) {
//		NSMutableArray * listItemTagsTemp = [[NSMutableArray alloc] initWithCapacity:10];
       // NSMutableArray * listItemTagsTemp = [[NSMutableArray alloc] init];
        //self.listItemTags = listItemTagsTemp;
        
        self.listItemTags = [NSMutableArray array];// listItemTagsTemp;
        
        //[listItemTagsTemp release];
		
		// Get the array of all tags in the database
		NSArray *localArray = [BLTag getListItemTagsWithListItemID:theListItemID];
		
		// Create a new object for each tag taken from the database
		for (NSDictionary *tagDictionary in localArray) {
			NSInteger theTagID = [DMHelper getNSIntegerValueForKey:@"TagID" fromDictionary:tagDictionary];
			
			ListItemTag *listItemTag = [[ListItemTag alloc] initWithListItemID:theListItemID andTagID:theTagID];
			
			[self.listItemTags addObject:listItemTag];
			[listItemTag release];
		}
	}
	return self;
}

#pragma mark -
#pragma mark Reload Methods

- (void)reloadListItemTagsForID:(NSInteger)theListItemID {
	// Remove all tags in list
	[self.listItemTags removeAllObjects];
	
	// Get the array of all tags in the database
	NSArray *localArray = [BLTag getListItemTagsWithListItemID:theListItemID];
	
	// Create a new object for each tag taken from the database
	for (NSDictionary *tagDictionary in localArray) {
		NSInteger theTagID = [DMHelper getNSIntegerValueForKey:@"TagID" fromDictionary:tagDictionary];
		
		ListItemTag *listItemTag = [[ListItemTag alloc] initWithListItemID:theListItemID andTagID:theTagID];
		
		[self.listItemTags addObject:listItemTag];
		[listItemTag release];
	}
}

#pragma mark -
#pragma mark Database Methods

- (void)insertSelfIntoDatabase {
	for (ListItemTag *listItemTag in self.listItemTags) {
		[listItemTag insertSelfIntoDatabase];
	}
}

#pragma mark -
#pragma mark Get Methods

- (NSInteger)getIndexOfTagWithID:(NSInteger)tagID {
	for (int i = 0; i < [self.listItemTags count]; i++) {
		if ([[self.listItemTags objectAtIndex:i] tagID] == tagID) {
			return i;
		}
	}
	
	return 0;
}

#pragma mark -
#pragma mark Copy Method

- (id)copyWithZone:(NSZone *)zone {
	//ListItemTagCollection *copyCollection = [[ListItemTagCollection alloc] init];
	
	ListItemTagCollection *copyCollection = [[[self class] allocWithZone:zone] init];
	
	copyCollection.listItemTags = [[self.listItemTags copyWithZone:zone] autorelease];
	
	//for (ListItemTag *listItemTag in self.listItemTags) {
	//	ListItemTag *copyTag = [[ListItemTag alloc] initWithListItemID:listItemTag.listItemID 
															//  andTagID:listItemTag.tagID];
	//	[copyCollection.listItemTags addObject:copyTag];
	//	[copyTag release];
	//}
	
	return copyCollection;
}

#pragma mark -
#pragma mark Delete Methods

- (void)deleteAllListItemTags {
	for (int i = [self.listItemTags count] - 1; i >= 0; i--) {
		ListItemTag *listItemTag = [self.listItemTags objectAtIndex:i];
		
		[BLListItemTag deleteListItemTagWithTagID:listItemTag.tagID andListItemID:listItemTag.listItemID];
		[self.listItemTags removeObjectAtIndex:i];
	}
}

- (void)removeTagWithID:(NSInteger)theTagID andIsNewListItem:(BOOL)isNewListItem {
	for (int i = 0; i < [self.listItemTags count]; i++) {
		ListItemTag *listItemTag = [self.listItemTags objectAtIndex:i];
		if (listItemTag.tagID == theTagID) {
			if (isNewListItem == FALSE) {
				[BLListItemTag deleteListItemTagWithTagID:listItemTag.tagID andListItemID:listItemTag.listItemID];
			}
			
			[self.listItemTags removeObjectAtIndex:i];
			
			break;
		}
	}
}

- (void)removeAllTags {
    for (int i = [self.listItemTags count] - 1; i >= 0; i--) {
		ListItemTag *listItemTag = [self.listItemTags objectAtIndex:i];
        [BLListItemTag deleteListItemTagWithTagID:listItemTag.tagID andListItemID:listItemTag.listItemID];
        [self.listItemTags removeObjectAtIndex:i];
	}
}

#pragma mark -
#pragma mark Toodledo Tags Compare Methods

// Takes the toodledo tags and matches up local tags to them
// RETURN: Return string is blank if no changes
- (NSString *)compareAndUpdateUsingToodledoTags:(NSString *)toodledoTags tagCollection:(TagCollection *)tagCollection listItemID:(NSInteger)listItemID {
	if ([toodledoTags length] == 0 && [self.listItemTags count] == 0) {
		// No change
		return @"";
	}
	
	NSString *returnString = @"";
	
	if ([toodledoTags length] == 0 && [self.listItemTags count] > 0) {
		// Remove all tags
		[self deleteAllListItemTags];
		returnString = [NSString stringWithFormat:@"Local: Removed all linked tags from task (%d)", listItemID];
	}
	
	NSArray *tags = [toodledoTags componentsSeparatedByString:@","];
	
	for (NSString *tagName in tags) {
		tagName = [tagName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		
		if ([tagName length] == 0) {
			continue;
		}
		
		// Look for local tags that are not in toodledo tags, delete them
		for (int i = [self.listItemTags count] - 1; i >=0; i--) {
			ListItemTag *listItemTag = [self.listItemTags objectAtIndex:i];
			NSString *localTagName = [tagCollection getNameForTagWithID:listItemTag.tagID];
			if ([MethodHelper stringObject:localTagName existsInToodledoTagArray:tags] == FALSE) {
				
				returnString = [NSString stringWithFormat:@"%@Local: Tag (%@) unlinked from task (%d)\n", returnString, localTagName, listItemID];
				[BLListItemTag deleteListItemTagWithTagID:listItemTag.tagID andListItemID:listItemTag.listItemID];
				[self.listItemTags removeObjectAtIndex:i];
			}
		}
	}
	
	
	for (NSString *tagName in tags) {
		tagName = [tagName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		
		if ([tagName length] == 0) {
			continue;
		}
		
		// Look for server tags that dont exist in local tags, add them
		NSInteger tagID = [tagCollection getTagIDOfTagWithName:tagName];
		
		if (tagID == -1) {
			// Not found... add it?
			tagID = [tagCollection getNewTagID];
			NSInteger newTagOrder = [tagCollection getNextTagOrderForParentTagID:-1];
			NSInteger tagDepth = 0;
			NSInteger parentTagID = -1;
			
			Tag *tag = [[Tag alloc] initWithTagID:tagID 
										  andName:tagName 
								   andParentTagID:parentTagID 
									  andTagOrder:newTagOrder 
									  andTagDepth:tagDepth
                                     andTagColour:@"Brown"];
			
			[tag insertSelfIntoDatabase];
			[tagCollection insertNewTag:tag];
			returnString = [NSString stringWithFormat:@"%@Local: New tag created (%@)\n", returnString, tagName];
			[tag release];
			
			// Setting new list item to false will automatically put tag in db too
			[self addTagWithID:tagID 
				 andListItemID:listItemID 
			  andIsNewListItem:FALSE];
			continue;
		} 
		
		// Look to see whether tag id exists within the listItemTag collection
		NSInteger listItemTagIndex = [self getIndexOfTagWithID:tagID];
		if (listItemTagIndex != -1) {
			// Add it to the collection as it doesn't exist
			[self addTagWithID:tagID andListItemID:listItemID andIsNewListItem:FALSE];
			returnString = [NSString stringWithFormat:@"%@Local: Tag (%@) linked to task (%d)\n", returnString, tagName, listItemID];
		}

	}
	
	
	return returnString;
}

#pragma mark -
#pragma mark Helper Methods

- (void)updateTagCollectionWithListItemID:(NSInteger)theListItemID {
	for (ListItemTag *listItemTag in self.listItemTags) {
		listItemTag.listItemID = theListItemID;
	}
}

// New list item boolean is where we don't have list item id yet (for new list item), thus cant add tag to database
- (void)addTagWithID:(NSInteger)theTagID andListItemID:(NSInteger)theListItemID andIsNewListItem:(BOOL)isNewListItem {
	ListItemTag *listItemTag = [[ListItemTag alloc] initWithListItemID:theListItemID andTagID:theTagID];
	[self.listItemTags addObject:listItemTag];
	
	// Also need to insert into database
	if (isNewListItem == FALSE) {
		[listItemTag insertSelfIntoDatabase];
	}
	
	[listItemTag release];
	
}

// For checking whether the passed tag ID exists in this list of list item tags
- (BOOL)containsTagWithID:(NSInteger)theTagID {
	for (ListItemTag *listItemTag in self.listItemTags) {
		if (listItemTag.tagID == theTagID) {
			return TRUE;
		}
	}
	return FALSE;
}

// Check if this collection contains a tag from an external collection
- (BOOL)containsTagFromListItemTags:(NSMutableArray *)theListItemTags {
	for (ListItemTag *listItemTag in theListItemTags) {
		if ([self containsTagWithID:listItemTag.tagID]) {
			// Return true
			return TRUE;
		}
	}
	return FALSE;
}

// Careful about changing values here as they are also present in ModifyListItemTableViewcell
- (CGFloat)heightOfRowForTagCollection:(TagCollection *)tagCollection {
	CGFloat rowHeight = 44.0;
	
	CGFloat totalBuffer = 0.0;
	CGFloat tagBuffer = 10.0;
	CGFloat tagBorder = 8.0;
	
	//CGFloat yBuffer = 0.0;
	//CGFloat standardRowHeight = 44.0;
	CGFloat tagsViewHeight = 20.0;
	CGFloat tagsViewWidth = 230.0;
	
	
	for (ListItemTag *listItemTag in self.listItemTags) {
		NSString *tagName = [tagCollection getNameForTagWithID:listItemTag.tagID];
		CGSize tagSize = [tagName sizeWithFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
		
		if (totalBuffer + (tagSize.width + tagBorder) > tagsViewWidth) {
			totalBuffer = 0.0;
			rowHeight += (tagsViewHeight + tagBuffer);
		}
		
		totalBuffer = totalBuffer + (tagSize.width + tagBorder) + tagBuffer;
	}
	
	return rowHeight;
}

@end
