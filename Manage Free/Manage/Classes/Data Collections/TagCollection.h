//
//  TagCollection.h
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class Tag;

@interface TagCollection : NSObject {
	NSMutableArray		*tags;
}

@property (nonatomic, retain) NSMutableArray	*tags;

#pragma mark Initialisation
- (id)initWithAllTags;
- (id)initWithAllTagsForParentTagID:(NSInteger)parentTagID;
- (id)initWithAllTagsIgnoringTagFamily:(NSMutableArray *)tagFamily;

#pragma mark Reload Tags Methods
- (void)reloadAllTags;
- (void)reloadAllTagsForParentTagID:(NSInteger)parentTagID;

#pragma mark Sort Methods
- (void)sortTags;

#pragma mark Get Methods
- (NSString *)getNameForTagWithID:(NSInteger)theTagID; 
- (NSInteger)getIndexOfTagWithID:(NSInteger)theTagID;
- (Tag *)getTagWithID:(NSInteger)theTagID;
- (NSInteger)getIndentLevelOfTagWithID:(NSInteger)theTagID;
- (NSInteger)getNewTagID;
- (NSInteger)getNextTagOrderForParentTagID:(NSInteger)parentTagID;
- (NSInteger)getNumberOfChildTagsForParentTagID:(NSInteger)parentTagID;
- (NSMutableArray *)getArrayOfTagsToDeleteForParentTag:(Tag *)parentTag;
- (NSMutableArray *)getFamilyOfTagsCopyForParentTag:(Tag *)parentTag;
- (NSInteger)getTagIDOfTagWithName:(NSString *)tagName;

#pragma mark Delete Methods
- (void)deleteTagAtIndex:(NSInteger)index;

#pragma mark Update Methods
- (void)updateChildrenTagDepthForParentTag:(Tag *)parentTag; 

#pragma mark Insert Methods
- (void)insertNewTag:(Tag *)tag;

#pragma mark Check Methods
- (BOOL)checkForAnyTagWithParentTagID:(NSInteger)theParentTagID;

#pragma mark Tag Order Methods
- (void)updateTagOrderFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;

@end
