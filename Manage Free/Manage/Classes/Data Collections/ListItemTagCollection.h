//
//  ListItemTagCollection.h
//  Manage
//
//  Created by Cliff Viegas on 10/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Collections
@class TagCollection;

@interface ListItemTagCollection : NSObject <NSCopying> {
	NSMutableArray		*listItemTags;
}

@property (nonatomic, retain) NSMutableArray	*listItemTags;

#pragma mark Initialisation
- (id)initWithListItemTagsForID:(NSInteger)theListItemID; 

#pragma mark Reload Methods
- (void)reloadListItemTagsForID:(NSInteger)theListItemID;

#pragma mark Database Methods
- (void)insertSelfIntoDatabase;

#pragma mark Get Methods
- (NSInteger)getIndexOfTagWithID:(NSInteger)tagID;

#pragma mark Copy Method

#pragma mark Delete Methods
- (void)deleteAllListItemTags;
- (void)removeTagWithID:(NSInteger)theTagID andIsNewListItem:(BOOL)isNewListItem;
- (void)removeAllTags;

#pragma mark Toodledo Tags Compare Methods
- (NSString *)compareAndUpdateUsingToodledoTags:(NSString *)toodledoTags tagCollection:(TagCollection *)tagCollection listItemID:(NSInteger)listItemID;

#pragma mark Helper Methods
- (void)updateTagCollectionWithListItemID:(NSInteger)theListItemID;
- (void)addTagWithID:(NSInteger)theTagID andListItemID:(NSInteger)theListItemID andIsNewListItem:(BOOL)isNewListItem;
- (BOOL)containsTagWithID:(NSInteger)theTagID;
- (BOOL)containsTagFromListItemTags:(NSMutableArray *)theListItemTags;
- (CGFloat)heightOfRowForTagCollection:(TagCollection *)tagCollection;

@end
