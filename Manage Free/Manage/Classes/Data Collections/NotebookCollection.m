//
//  NotebookCollection.m
//  Manage
//
//  Created by Cliff Viegas on 17/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "NotebookCollection.h"

// Business Layer
#import "BLNotebook.h"

// Data Models
#import "Notebook.h"

// Helper Classes
#import "DMHelper.h"

@implementation NotebookCollection

@synthesize notebooks;
@synthesize selectedNotebookIndex;
@synthesize notebookListTileFrame;

#pragma mark - Memory Management

- (void)dealloc {
    [self.notebooks release];
    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithAllNotebooks {
    self = [super init];
    if (self) {
        // Init the notebooks array
		self.notebooks = [NSMutableArray array];
        
        // Assign the selected notebook index
        self.selectedNotebookIndex = 0;
        
        // Init tile frame to cgrect zero
        self.notebookListTileFrame = CGRectZero;
        
        // Get all notebooks, call DB
        NSArray *localArray = [BLNotebook getAllNotebooks];
        
        // Create a new object for each notebook taken from the database and add it to our list of notebooks
		for (NSDictionary *notebookDictionary in localArray) {
            Notebook *notebook = [[[Notebook alloc] init] autorelease];
            
            notebook.notebookID = [DMHelper getIntegerValueForKey:@"NotebookID" fromDictionary:notebookDictionary];
            notebook.title = [DMHelper getNSStringValueForKey:@"Title" fromDictionary:notebookDictionary];
            notebook.itemOrder = [DMHelper getIntegerValueForKey:@"ItemOrder" fromDictionary:notebookDictionary];
            notebook.creationDate = [DMHelper getNSStringValueForKey:@"CreationDate" fromDictionary:notebookDictionary];
            NSString *theLists = [DMHelper getNSStringValueForKey:@"Lists" fromDictionary:notebookDictionary];
            [notebook loadLists:theLists];
            
			[self.notebooks addObject:notebook];
		}
    }
    return self;
}

#pragma mark - Get Methods

- (NSInteger)getIndexOfNotebookWithID:(NSInteger)theNotebookID {
	int i = 0;
	for (Notebook *notebook in self.notebooks) {
		if (notebook.notebookID == theNotebookID) {
			// Return this task list location
			return i;
		}
		i++;
	}
	
	// Not found, return -1
	return -1;
}

#pragma mark - Delete Methods

- (void)deleteNotebookAtIndex:(NSInteger)index {
    // Get the notebook id
    Notebook *notebook = [self.notebooks objectAtIndex:index];
    
    [BLNotebook deleteNotebookWithID:notebook.notebookID];
    
    [self.notebooks removeObjectAtIndex:index];
}


@end
