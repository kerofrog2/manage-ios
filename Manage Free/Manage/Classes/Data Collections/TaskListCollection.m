//
//  ListCollection.m
//  Manage
//
//  Created by Cliff Viegas on 17/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "TaskListCollection.h"

// Quartzcore
#import <QuartzCore/QuartzCore.h>

// Business Layer
#import "BLList.h"
#import "BLApplicationSetting.h"

// Data Models
#import "TaskList.h"
#import "ListItemTag.h"
#import "ListItem.h"
#import "ArchiveList.h"
#import "Tag.h"
#import "DeletedTaskList.h"
#import "ToodledoFolder.h"
#import "MainConstants.h"

// Method Helper
#import "MethodHelper.h"

// Data Collections
#import "ListItemTagCollection.h"
#import "TagCollection.h"
#import "MainConstants.h"

// Dictionary Method Helper
#import "DMHelper.h"

// In app purchase manager
#import "InAppPurchaseManager.h"

@implementation TaskListCollection

@synthesize lists;
@synthesize showCompletedTasks;
@synthesize showCompletedTasksDuration;
@synthesize currentTaskListID;
@synthesize sortOrderUpdated;
@synthesize archiveSetting;
@synthesize pocketReminder;
@synthesize defaultView;
@synthesize theNewListOrder;
@synthesize theNewTaskOrder;
@synthesize badgeNotification;
@synthesize defaultSortOrder;
@synthesize selectedTheme;
@synthesize previewTheme;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    //Tan Nguyen
    [self.lists release];
	[archiveSetting release];
	[defaultView release];
	[theNewListOrder release];
	[theNewTaskOrder release];
	[badgeNotification release];
	[defaultSortOrder release];
	[selectedTheme release];
    [previewTheme release];

    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if ((self = [super init])) {
		// Init the lists array
        // Tan Nguyen - should remove it but now it's used for temp data
		self.lists = [NSMutableArray array];
		self.sortOrderUpdated = FALSE;
	}
	return self;
}

- (id)initWithAllLists {
	if ((self = [super init])) {
		// Init the lists array
        //Tan Nguyen
		self.lists = [NSMutableArray array];
		
		// Get the completed tasks settings
		NSString *completedTasksDuration = [BLApplicationSetting getSettingData:@"ShowCompletedDuration"];
		self.showCompletedTasksDuration = [self getShowCompletedDurationEnumForData:completedTasksDuration];
		self.showCompletedTasks = TRUE; // Always true now - //[[BLApplicationSetting getSettingData:@"ShowCompleted"] boolValue];
		self.currentTaskListID = -1;
		self.sortOrderUpdated = FALSE;
		self.archiveSetting = [BLApplicationSetting getSettingData:@"ArchiveItems"];
		self.pocketReminder = [[BLApplicationSetting getSettingData:@"PocketReminder"] boolValue];
		self.defaultView = [BLApplicationSetting getSettingData:@"DefaultView"];
		self.theNewListOrder = [BLApplicationSetting getSettingData:@"NewListOrder"];
		self.theNewTaskOrder = [BLApplicationSetting getSettingData:@"NewTaskOrder"];
		self.badgeNotification = [BLApplicationSetting getSettingData:@"BadgeNotification"];
		self.defaultSortOrder = [BLApplicationSetting getSettingData:@"DefaultSortOrder"];
		self.selectedTheme = [BLApplicationSetting getSettingData:@"SelectedTheme"];
		self.previewTheme = @"";
        
		// Get the array of all wireframes in the database
		NSArray *localArray = [BLList getAllLists];
		
		// Create a new object for each list taken from the database and add it to our list of lists
		for (NSDictionary *listDictionary in localArray) {
			NSInteger theListID = [DMHelper getNSIntegerValueForKey:@"ListID" fromDictionary:listDictionary];
			
			NSString *theTitle = [DMHelper getNSStringValueForKey:@"Title" fromDictionary:listDictionary];
			
			TaskList *list = [[TaskList alloc] initExistingWithListID:theListID andTitle:theTitle];
			
			list.listID = theListID;
			list.title = theTitle;
			list.creationDate = [DMHelper getNSStringValueForKey:@"CreationDate" fromDictionary:listDictionary];
			list.screenshot = [DMHelper getNSStringValueForKey:@"Screenshot" fromDictionary:listDictionary];
			list.listOrder = [DMHelper getNSIntegerValueForKey:@"ListOrder" fromDictionary:listDictionary];
			list.completedDateTime = [DMHelper getNSStringValueForKey:@"CompletedDateTime" fromDictionary:listDictionary];
			list.completed = [DMHelper getBoolValueForKey:@"Completed" fromDictionary:listDictionary];
			list.archived = [DMHelper getBoolValueForKey:@"Archived" fromDictionary:listDictionary];
			list.archivedDateTime = [DMHelper getNSStringValueForKey:@"ArchivedDateTime" fromDictionary:listDictionary];
			list.toodledoFolderID = [DMHelper getNSIntegerValueForKey:@"ToodledoFolderID" fromDictionary:listDictionary];
			list.creationDateTime = [DMHelper getNSStringValueForKey:@"CreationDateTime" fromDictionary:listDictionary];
			list.isNote = [DMHelper getBoolValueForKey:@"IsNote" fromDictionary:listDictionary];
            list.scribble = [DMHelper getNSStringValueForKey:@"Scribble" fromDictionary:listDictionary];
            

            
			[self.lists addObject:list];
            //Tan Nguyen
            [list release];
            
		}
	}
	return self;
}

#pragma mark -
#pragma mark Load/Reload Methods

// Deprecated (still here for testing, and for initial load)
- (void)reloadAllLists {
	// Init the lists array
	[self.lists removeAllObjects];
	
    // Get the completed tasks settings
    NSString *completedTasksDuration = [BLApplicationSetting getSettingData:@"ShowCompletedDuration"];
    self.showCompletedTasksDuration = [self getShowCompletedDurationEnumForData:completedTasksDuration];
    self.showCompletedTasks = TRUE; // Always true now - //[[BLApplicationSetting getSettingData:@"ShowCompleted"] boolValue];
    self.currentTaskListID = -1;
    self.sortOrderUpdated = FALSE;
    self.archiveSetting = [BLApplicationSetting getSettingData:@"ArchiveItems"];
    self.pocketReminder = [[BLApplicationSetting getSettingData:@"PocketReminder"] boolValue];
    self.defaultView = [BLApplicationSetting getSettingData:@"DefaultView"];
    self.theNewListOrder = [BLApplicationSetting getSettingData:@"NewListOrder"];
    self.theNewTaskOrder = [BLApplicationSetting getSettingData:@"NewTaskOrder"];
    self.badgeNotification = [BLApplicationSetting getSettingData:@"BadgeNotification"];
    self.defaultSortOrder = [BLApplicationSetting getSettingData:@"DefaultSortOrder"];
    self.selectedTheme = [BLApplicationSetting getSettingData:@"SelectedTheme"];
    self.previewTheme = @"";
    
	// Get the array of all wireframes in the database
	NSArray *localArray = [BLList getAllLists];
	
	// Create a new object for each list taken from the database and add it to our list of lists
	for (NSDictionary *listDictionary in localArray) {
		NSInteger theListID = [DMHelper getNSIntegerValueForKey:@"ListID" fromDictionary:listDictionary];
		NSString *theTitle = [DMHelper getNSStringValueForKey:@"Title" fromDictionary:listDictionary];
		
		TaskList *list = [[TaskList alloc] initExistingWithListID:theListID andTitle:theTitle];
        
        list.listID = theListID;
        list.title = theTitle;
        list.creationDate = [DMHelper getNSStringValueForKey:@"CreationDate" fromDictionary:listDictionary];
        list.screenshot = [DMHelper getNSStringValueForKey:@"Screenshot" fromDictionary:listDictionary];
        list.listOrder = [DMHelper getNSIntegerValueForKey:@"ListOrder" fromDictionary:listDictionary];
        list.completedDateTime = [DMHelper getNSStringValueForKey:@"CompletedDateTime" fromDictionary:listDictionary];
        list.completed = [DMHelper getBoolValueForKey:@"Completed" fromDictionary:listDictionary];
        list.archived = [DMHelper getBoolValueForKey:@"Archived" fromDictionary:listDictionary];
        list.archivedDateTime = [DMHelper getNSStringValueForKey:@"ArchivedDateTime" fromDictionary:listDictionary];
        list.toodledoFolderID = [DMHelper getNSIntegerValueForKey:@"ToodledoFolderID" fromDictionary:listDictionary];
        list.creationDateTime = [DMHelper getNSStringValueForKey:@"CreationDateTime" fromDictionary:listDictionary];
        list.isNote = [DMHelper getBoolValueForKey:@"IsNote" fromDictionary:listDictionary];
        list.scribble = [DMHelper getNSStringValueForKey:@"Scribble" fromDictionary:listDictionary];
        
        
        [self.lists addObject:list];
        //Tan Nguyen
        [list release];
	}
}

- (void)reloadWithMasterTaskListCollection:(TaskListCollection *)masterTaskListCollection {
	// Remove all lists from the array
	[self.lists removeAllObjects];
	
	self.showCompletedTasks = masterTaskListCollection.showCompletedTasks;
	self.showCompletedTasksDuration = masterTaskListCollection.showCompletedTasksDuration;
	self.currentTaskListID = masterTaskListCollection.currentTaskListID;
	//self.sortOrderUpdated = masterTaskListCollection.sortOrderUpdated;
	self.archiveSetting = masterTaskListCollection.archiveSetting;
	self.pocketReminder = masterTaskListCollection.pocketReminder;
	self.defaultView = masterTaskListCollection.defaultView;
	self.theNewListOrder = masterTaskListCollection.theNewListOrder;
	self.theNewTaskOrder = masterTaskListCollection.theNewTaskOrder;
	self.badgeNotification = masterTaskListCollection.badgeNotification;
	self.defaultSortOrder = masterTaskListCollection.defaultSortOrder;
	self.selectedTheme = masterTaskListCollection.selectedTheme;
	
	for (TaskList *masterTaskList in masterTaskListCollection.lists) {
		// Make sure the list is not complete and is not the selected list
		//if ([masterTaskList completed] == FALSE || [self showCompletedTasks] == TRUE 
		//	|| masterTaskList.listID == self.currentTaskListID) {
			
			TaskList *newTaskList = [[TaskList alloc] initWithBasicTaskListCopy:masterTaskList];

			for (ListItem *listItem in masterTaskList.listItems) {
				
				ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
				// Get index of the current list item
				//NSInteger listItemIndex = [masterTaskList indexOfListItemWithID:newListItem.listItemID];
				
				
				// Now that we dont use show completed duration here, always load them
				[newTaskList.listItems addObject:newListItem];
				
				/*if ([newListItem completed] == FALSE) {
					[newTaskList.listItems addObject:newListItem];
				} else if ([newListItem completed] == TRUE && 
						   [masterTaskList hasIncompleteSubtasksForListItemAtIndex:listItemIndex forCompletedDuration:[self showCompletedTasksDuration]
															andShowCompletedStatus:[self showCompletedTasks]]
						   && [masterTaskList currentSortType] == EnumSortByList) {
					// Look for any subtasks
					[newTaskList.listItems addObject:newListItem];
				} else if ([newListItem completed] == TRUE && [self showCompletedTasks] == TRUE) {
					// Need to check the duration
					NSInteger dayDifference = [newListItem getCompletedDateDifferenceFromToday];
					// Convert to positive
					dayDifference = dayDifference * -1;
					
					// 0 means all tasks regardless of duration
					if (dayDifference <= [self showCompletedTasksDuration] || [self showCompletedTasksDuration] == 0) {
						// Show (add to list)
						[newTaskList.listItems addObject:newListItem];
					}
				}*/
				
				[newListItem release];
			}
			[self.lists addObject:newTaskList];
			[newTaskList release];
		//}
	}
}

- (void)reloadWithMasterTaskListCollection:(TaskListCollection *)masterTaskListCollection usingList:(NSArray *)listArray {
	// Remove all lists from the array
	[self.lists removeAllObjects];
	
	self.showCompletedTasks = masterTaskListCollection.showCompletedTasks;
	self.showCompletedTasksDuration = masterTaskListCollection.showCompletedTasksDuration;
	self.currentTaskListID = masterTaskListCollection.currentTaskListID;
	//self.sortOrderUpdated = masterTaskListCollection.sortOrderUpdated;
	self.archiveSetting = masterTaskListCollection.archiveSetting;
	self.pocketReminder = masterTaskListCollection.pocketReminder;
	self.defaultView = masterTaskListCollection.defaultView;
	self.theNewListOrder = masterTaskListCollection.theNewListOrder;
	self.theNewTaskOrder = masterTaskListCollection.theNewTaskOrder;
	self.badgeNotification = masterTaskListCollection.badgeNotification;
	self.defaultSortOrder = masterTaskListCollection.defaultSortOrder;
	self.selectedTheme = masterTaskListCollection.selectedTheme;

	for (TaskList *masterTaskList in masterTaskListCollection.lists) {
        
        if ([MethodHelper stringObject:[NSString stringWithFormat:@"%d", masterTaskList.listID] existsInArray:listArray]) {
            TaskList *newTaskList = [[TaskList alloc] initWithBasicTaskListCopy:masterTaskList];
            
            for (ListItem *listItem in masterTaskList.listItems) {
                
                ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
                // Get index of the current list item
                //NSInteger listItemIndex = [masterTaskList indexOfListItemWithID:newListItem.listItemID];
                
                
                // Now that we dont use show completed duration here, always load them
                [newTaskList.listItems addObject:newListItem];
                
                /*if ([newListItem completed] == FALSE) {
                 [newTaskList.listItems addObject:newListItem];
                 } else if ([newListItem completed] == TRUE && 
                 [masterTaskList hasIncompleteSubtasksForListItemAtIndex:listItemIndex forCompletedDuration:[self showCompletedTasksDuration]
                 andShowCompletedStatus:[self showCompletedTasks]]
                 && [masterTaskList currentSortType] == EnumSortByList) {
                 // Look for any subtasks
                 [newTaskList.listItems addObject:newListItem];
                 } else if ([newListItem completed] == TRUE && [self showCompletedTasks] == TRUE) {
                 // Need to check the duration
                 NSInteger dayDifference = [newListItem getCompletedDateDifferenceFromToday];
                 // Convert to positive
                 dayDifference = dayDifference * -1;
                 
                 // 0 means all tasks regardless of duration
                 if (dayDifference <= [self showCompletedTasksDuration] || [self showCompletedTasksDuration] == 0) {
                 // Show (add to list)
                 [newTaskList.listItems addObject:newListItem];
                 }
                 }*/
                
                [newListItem release];
            }
            [self.lists addObject:newTaskList];
            [newTaskList release];
        }

	}
}


// Load with tasks that have been created after last sync date
// getNonSynced: Boolean for checking whether we also get any tasks that haven't been synced with toodledo (toodledoTaskID = -1)
- (void)loadWithTasksCreatedAfter:(NSString *)dateTimeString usingTaskCollection:(TaskListCollection *)masterTaskListCollection getNonSynced:(BOOL)getNonSynced {
	// Remove all lists from the array (although shouldn't be any)
	[self.lists removeAllObjects];
	
	for (TaskList *masterTaskList in masterTaskListCollection.lists) {
        if ([masterTaskList isNote]) {
            continue;
        }
        
		TaskList *newTaskList = [[TaskList alloc] initWithBasicTaskListCopy:masterTaskList];
		
		for (ListItem *listItem in masterTaskList.listItems) {
            // Need to skip list items that have toodledo task id
            if ([listItem toodledoTaskID] != -1) {
                continue;
            }
            
			// Need to compare dates
			NSComparisonResult comparisonResult = [listItem.creationDateTime compare:dateTimeString];
			
			// Means our dateTimeString (last sync datetime) is earlier than the creation date, so get it, (also get it if nonsynced is true and toodledotaskid is -1)
			if (comparisonResult == NSOrderedDescending || (getNonSynced == TRUE && [listItem toodledoTaskID] == -1)) {
                ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
				[newTaskList.listItems addObject:newListItem];
				[newListItem release];
			} 
			
		}
		
		[self.lists addObject:newTaskList];
		[newTaskList release];
	}
	
}

- (void)loadWithTasksModifiedAfter:(NSString *)dateTimeString usingTaskCollection:(TaskListCollection *)masterTaskListCollection {
	// Remove all lists from the array (although shouldn't be any)
	[self.lists removeAllObjects];
	
	for (TaskList *masterTaskList in masterTaskListCollection.lists) {
        if ([masterTaskList isNote]) {
            continue;
        }
        
		TaskList *newTaskList = [[TaskList alloc] initWithBasicTaskListCopy:masterTaskList];
		
		for (ListItem *listItem in masterTaskList.listItems) {
            // Ignore modified tasks that dont have toodledo task id
            if ([listItem toodledoTaskID] == -1) {
                continue;
            }
            
			// Need to compare dates
			NSComparisonResult comparisonResult = [listItem.modifiedDateTime compare:dateTimeString];
			
			// Means our dateTimeString (last sync datetime) is earlier than the creation date, so get it
			if (comparisonResult == NSOrderedDescending) {
				ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
				[newTaskList.listItems addObject:newListItem];
				[newListItem release];
			} 
			
		}
		
		[self.lists addObject:newTaskList];
		[newTaskList release];
	}
}

// Load with task lists created after last sync date
- (void)loadWithTaskListsCreatedAfter:(NSString *)dateTimeString usingTaskCollection:(TaskListCollection *)masterTaskListCollection {
	// Remove all lists from the array (in case there are any)
	[self.lists removeAllObjects];
	
	for (TaskList *masterTaskList in masterTaskListCollection.lists) {
        if ([masterTaskList isNote]) {
            continue;
        }
        
        // Need to ignore those with toodledo folder id
        if ([masterTaskList toodledoFolderID] != -1) {
            continue;
        }
        
		TaskList *newTaskList = [[[TaskList alloc] initWithBasicTaskListCopy:masterTaskList] autorelease];
		
		// Need to compare dates
		NSComparisonResult comparisonResult = [newTaskList.creationDateTime compare:dateTimeString];
		
		// Means our dateTimeString (last sync datetime) is earlier than creation date, so get it
		if (comparisonResult == NSOrderedDescending) {
			[self.lists addObject:newTaskList];
		}
	}
}

#pragma mark -
#pragma mark Filter Methods

- (void)filterListsUsingListItemTagCollection:(ListItemTagCollection *)listItemTagCollection
				andOriginalTaskListCollection:(TaskListCollection *)originalTaskListCollection {
	
	// Set completed tasks settings
	self.showCompletedTasks = [originalTaskListCollection showCompletedTasks];
	self.showCompletedTasksDuration = [originalTaskListCollection showCompletedTasksDuration];
	//self.sortOrderUpdated = [originalTaskListCollection sortOrderUpdated];
	
	// Clean out all lists in this class
	[self.lists removeAllObjects];
	
	for (TaskList *taskList in originalTaskListCollection.lists) {
		TaskList *newTaskList = [[TaskList alloc] initWithBasicTaskListCopy:taskList];
		
		//newTaskList.title = [taskList.title copy];
		//newTaskList.listID = taskList.listID;
		
		for (ListItem *listItem in taskList.listItems) {
			if ([listItem.listItemTagCollection containsTagFromListItemTags:listItemTagCollection.listItemTags]) {
                
                NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
               
                if (isShowCompletedTasks == 1) {
                    // Add this object
                    ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
                    
                    [newTaskList.listItems addObject:newListItem];
                    [newListItem release];
                }else{
                    
                    if (listItem.hasJustComplete != YES && listItem.completed != YES) {
                        // Add this object
                        ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
                        
                        [newTaskList.listItems addObject:newListItem];
                        [newListItem release];
                    }
                }
			}
		}
		
		if ([newTaskList.listItems count] > 0) {
			[self.lists addObject:newTaskList];
		}
		[newTaskList release];
	}
}

#pragma mark -
#pragma mark Archive Methods

// Archiving will either occur on application launch, manual or after one day
- (NSInteger)archiveAllCompletedListsToArchiveList:(ArchiveList *)archiveList {
	NSInteger numberOfListsArchived = 0;
	
	// Have to do the update backwards in the array, so we can remove lists
	for (int i = [self.lists count] - 1; i >= 0; i--) {
		TaskList *taskList = [self.lists objectAtIndex:i];
		if ([taskList completed] == TRUE && [taskList archived] == FALSE) {			
			taskList.archived = TRUE;
			taskList.archivedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
			
			// Update & archive all items in the task list
			if (archiveList == nil) {
				[taskList archiveAllItems];
			} else if (archiveList != nil) {
				[taskList archiveAllItemsToArchiveList:archiveList];
			}
			
			// Update database
			[taskList updateDatabase];
			
			// Get a copy of the task list before we remove it
			TaskList *taskListCopy = [[TaskList alloc] initWithBasicTaskListCopy:taskList];
			
			// Now remove this list from the collection
			[self.lists removeObjectAtIndex:i];
			
			// Add the task list copy to the archive list
			if (archiveList != nil) {
				[archiveList.lists insertObject:taskListCopy atIndex:0];
			}
			
			// Release our copy of the tasklist
			[taskListCopy release];
			
			// Update the number of lists that have been archived
			numberOfListsArchived++;
		}
	}
	
	return numberOfListsArchived;
}

// Duration is used to check whether completed lists should be archived (if completed date is far enough back)
- (NSInteger)archiveAllCompletedListsUsingDuration:(NSInteger)duration {
	NSInteger numberOfListsArchived = 0;
	
	// Have to do the update backwards in the array, so we can remove lists
	for (int i = [self.lists count] - 1; i >= 0; i--) {
		TaskList *taskList = [self.lists objectAtIndex:i];
		
		// Negative number for date in past, multiply by -1 to make it positive
		NSInteger dateDifference = [taskList getCompletedDateDifferenceFromToday] * -1;
		
		if ([taskList completed] == TRUE && [taskList archived] == FALSE
			&& dateDifference >= duration) {			
			taskList.archived = TRUE;
			taskList.archivedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
			
			// Update & archive all items in the task list
			[taskList archiveAllItems];
			
			// Update database
			[taskList updateDatabase];
			
			// Get a copy of the task list before we remove it
			TaskList *taskListCopy = [[TaskList alloc] initWithBasicTaskListCopy:taskList];
			
			// Now remove this list from the collection
			[self.lists removeObjectAtIndex:i];
			
			// Release our copy of the tasklist
			[taskListCopy release];
			
			// Update the number of lists that have been archived
			numberOfListsArchived++;
		}
	}
	
	return numberOfListsArchived;
}

#pragma mark -
#pragma mark Move Methods

- (void)moveListItemWithID:(NSInteger)theListItemID fromTaskListWithID:(NSInteger)fromTaskListID toTaskListWithID:(NSInteger)toTaskListID {
	NSInteger fromListIndex = [self getIndexOfTaskListWithID:fromTaskListID];
	if (fromListIndex == -1) {
        [MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Unable to find source list for move.  Please report this error to manage@kerofrog.com.au" andButtonTitle:@"Ok"];
        return;
    }
    
    NSInteger toListIndex = [self getIndexOfTaskListWithID:toTaskListID];
	if (toListIndex == -1) {
        [MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Unable to find destination list for move.  Please report this error to manage@kerofrog.com.au" andButtonTitle:@"Ok"];
        return;
    }
    
	TaskList *toTaskList = [self.lists objectAtIndex:toListIndex];
	toTaskList.mainViewRequiresUpdate = TRUE;
	
	TaskList *fromTaskList = [self.lists objectAtIndex:fromListIndex];
	fromTaskList.mainViewRequiresUpdate = TRUE;
	NSInteger listItemIndex = [fromTaskList getIndexOfListItemWithID:theListItemID];
	if (listItemIndex == -1) {
        [MethodHelper showAlertViewWithTitle:@"Error" andMessage:@"Unable to find source task for move.  Please report this error to manage@kerofrog.com.au" andButtonTitle:@"Ok"];
        return;
    }
    
	// Get copy of the list item at from list item index
	ListItem *fromListItemCopy = [[ListItem alloc] initWithListItemCopy:[fromTaskList.listItems objectAtIndex:listItemIndex]];
	
	// Remove the from list item
	[fromTaskList.listItems	removeObjectAtIndex:listItemIndex];
	
	// Update the from list item copy with correct parent list id and item order
	fromListItemCopy.listID = [toTaskList listID];
	fromListItemCopy.parentListTitle = [toTaskList title];
	NSInteger newItemOrder = [toTaskList getNextListItemOrder];
	fromListItemCopy.itemOrder = newItemOrder;
	
	// Make sure to remove parent list item id, we don't want no sub list items staying as subtask
	fromListItemCopy.parentListItemID = -1;
	
	// Update the database
	[fromListItemCopy updateDatabase];
	
	// Add the list item copy to the toTaskList
	[toTaskList.listItems addObject:fromListItemCopy];
	
	// At end of moving, look for any subtasks, and also move them
	for (int i = [fromTaskList.listItems count] - 1; i >= 0; i--) {
		ListItem *subListItem = [fromTaskList.listItems objectAtIndex:i];
		
		if (subListItem.parentListItemID == fromListItemCopy.listItemID) {
			subListItem.itemOrder = fromListItemCopy.itemOrder;
			subListItem.listID = fromListItemCopy.listID;
			subListItem.parentListTitle = fromListItemCopy.parentListTitle;
			
			// Get copy of the sub list item
			ListItem *subListItemCopy = [[ListItem alloc] initWithListItemCopy:subListItem];
			[toTaskList.listItems addObject:subListItemCopy];
			
			// Remove original sub list item
			[fromTaskList.listItems removeObjectAtIndex:i];
			
			// Update the database
			[subListItemCopy updateDatabase];
			
			// Release our copy
			[subListItemCopy release];
		}
	}
	
	// Release our list item copy
	[fromListItemCopy release];
}

#pragma mark -
#pragma mark Select/Get Methods

- (NSInteger)getIndexOfTaskListWithTitle:(NSString *)theTitle {
	NSInteger foundIndex = -1;
	NSInteger i = 0;
	for (TaskList *taskList in self.lists) {
        if ([taskList isNote]) {
            i++;
            continue;
        }
        
		if ([taskList.title isEqualToString:theTitle]) {
			foundIndex = i;
			break;
		}
		i++;
	}
	
	return foundIndex;
}

- (NSString *)getListTitleForListWithID:(NSInteger)theListID {
	for (TaskList *taskList in self.lists) {
		if (taskList.listID == theListID) {
			return taskList.title;
		}
	}
	
	return @"";
}

// Find the task list index that has the task we want
- (NSInteger)getIndexOfTaskListWithListItemID:(NSInteger)theListItemID {
    int i = 0;
	for (TaskList *taskList in self.lists) {
        NSInteger listItemIndex = [taskList getIndexOfListItemWithID:theListItemID];
        
		if (listItemIndex >= 0) {
			// Return this task list location
			return i;
		}
		i++;
	}
	
	// Not found, return -1
	return -1;
}

- (NSInteger)getIndexOfTaskListWithID:(NSInteger)theListID {
	int i = 0;
	for (TaskList *taskList in self.lists) {
		if (taskList.listID == theListID) {
			// Return this task list location
			return i;
		}
		i++;
	}
	
	// Not found, return -1
	return -1;
}

- (NSInteger)getIndexOfTaskListWithToodledoFolderID:(NSInteger)theToodledoFolderID {
	int i = 0;
	
	// We need to return 'Toodledo Tasks' list if folder id is 0
	if (theToodledoFolderID == 0) {
		NSInteger toodledoTasksIndex = [self getIndexOfTaskListWithTitle:@"Toodledo Tasks"];
		return toodledoTasksIndex;
	}
	
	for (TaskList *taskList in self.lists) {
		if (taskList.toodledoFolderID == theToodledoFolderID) {
			// Return this task location
			return i;
		}
		i++;
	}
	
	// Not found, return -1
	return -1;
}

- (TaskList *)getTaskListWithID:(NSInteger)theListID {
	TaskList *refTaskList = nil;
	for (TaskList *taskList in self.lists) {
		if (taskList.listID == theListID) {
			refTaskList = taskList;
		}
	}
	
	return refTaskList;
}

- (NSInteger)getShowCompletedDurationEnumForData:(NSString *)theData {
	// Go through the options and return 
	if ([theData isEqualToString:@"Always"]) {
		return EnumCompletedDurationAlways;
	} else if ([theData isEqualToString:@"One Day"]) {
		return EnumCompletedDurationOneDay;
	} else if ([theData isEqualToString:@"One Week"]) {
		return EnumCompletedDurationOneWeek;
	} else if ([theData isEqualToString:@"Four Weeks"]) {
		return EnumCompletedDurationFourWeeks;
	}
	
	return EnumCompletedDurationAlways;
}

- (NSInteger)getNumberOfCompletedListItems {
	NSInteger completedListItemCount = 0;
	
	for (TaskList *taskList in self.lists) {
		for (ListItem *listItem in taskList.listItems) {
			if ([listItem completed] == TRUE) {
				completedListItemCount++;
			}
		}
	}
	return completedListItemCount;
}

- (NSInteger)getNumberOfCompletedLists {
	NSInteger completedListItemCount = 0;
	
	for (TaskList *taskList in self.lists) {
		if ([taskList completed] == TRUE) {
			completedListItemCount++;
		}
	}
	
	return completedListItemCount;
}

- (ListItem *)getListItemReferenceFromListItemWithToodledoTaskID:(SInt64)theTaskID {
	ListItem *theListItem = nil;
	
	for (TaskList *taskList in self.lists) {
		theListItem = [taskList getListItemReferenceFromListItemWithToodledoTaskID:theTaskID];
		if (theListItem != nil) {
			return theListItem;
		}
	}
	
	return theListItem;
}

- (NSInteger)getCountOfTaskListWithTitle:(NSString *)theTitle {
	NSInteger titleCount = 0;
	
	for (TaskList *taskList in self.lists) {
		if ([taskList.title isEqualToString:theTitle]) {
			titleCount++;
		}
	}
	
	return titleCount;
}

- (NSString *)getImageNameForSection:(NSString *)section {
	NSString *imageName = @"Pocket.png";
	
	if ([self.selectedTheme isEqualToString:@"Classic"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"Border.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"Canvass.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"Pocket.png";
		}
	} else if ([self.selectedTheme isEqualToString:@"Autumn Dreams"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"AutumnDreamsBorder.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"AutumnDreamsCanvas.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"AutumnDreamsPocket.png";
		}
	} else if ([self.selectedTheme isEqualToString:@"Hippy Wonderland"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"HippyWonderlandBorder.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"HippyWonderlandCanvas.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"HippyWonderlandPocket.png";
		}
	} else if ([self.selectedTheme isEqualToString:@"Red Rocket"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"RedRocketBorder.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"RedRocketCanvas.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"RedRocketPocket.png";
		}
	} else if ([self.selectedTheme isEqualToString:@"Gold Rush"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"GoldRushBorder.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"GoldRushCanvas.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"GoldRushPocket.png";
		}
	}   
    // Grab premium themes, but only if they are purchased, if not, revert to classic
	else if ([self.selectedTheme isEqualToString:@"Purple Haze"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemePurpleHazeID]) {
        if ([section isEqualToString:@"Border"]) {
            imageName = @"PurpleHazeBorder.png";
        } else if ([section isEqualToString:@"Canvas"]) {
            imageName = @"PurpleHazeCanvas.png";
        } else if ([section isEqualToString:@"Pocket"]) {
            imageName = @"PurpleHazePocket.png";
        }
    } else if ([self.selectedTheme isEqualToString:@"Old Timer"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeOldTimerID]) {
        if ([section isEqualToString:@"Border"]) {
            imageName = @"OldTimerBorder.png";
        } else if ([section isEqualToString:@"Canvas"]) {
            imageName = @"OldTimerCanvas.png";
        } else if ([section isEqualToString:@"Pocket"]) {
            imageName = @"OldTimerPocket.png";
        }
    } else if ([self.selectedTheme isEqualToString:@"Ninja Bunnies"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeNinjaBunniesID]) {
        if ([section isEqualToString:@"Border"]) {
            imageName = @"NinjaBunniesBorder.png";
        } else if ([section isEqualToString:@"Canvas"]) {
            imageName = @"NinjaBunniesCanvas.png";
        } else if ([section isEqualToString:@"Pocket"]) {
            imageName = @"NinjaBunniesPocket.png";
        }
    } else if ([self.selectedTheme isEqualToString:@"Sweet Cravings"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeSweetCravingsID]) {
        if ([section isEqualToString:@"Border"]) {
            imageName = @"SweetCravingsBorder.png";
        } else if ([section isEqualToString:@"Canvas"]) {
            imageName = @"SweetCravingsCanvas.png";
        } else if ([section isEqualToString:@"Pocket"]) {
            imageName = @"SweetCravingsPocket.png";
        }
    } else if ([self.selectedTheme isEqualToString:@"Nitro Burn"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeNitroBurnID]) {
        if ([section isEqualToString:@"Border"]) {
            imageName = @"NitroBurnBorder.png";
        } else if ([section isEqualToString:@"Canvas"]) {
            imageName = @"NitroBurnCanvas.png";
        } else if ([section isEqualToString:@"Pocket"]) {
            imageName = @"NitroBurnPocket.png";
        }
    } else {
        if ([section isEqualToString:@"Border"]) {
			imageName = @"Border.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"Canvass.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"Pocket.png";
		}
    }
    
	return imageName;
}

- (NSString *)getPreviewImageNameForSection:(NSString *)section {
	NSString *imageName = @"Pocket.png";
	
	if ([self.previewTheme isEqualToString:@"Old Timer"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"OldTimerBorder.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"OldTimerCanvas.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"OldTimerPocket.png";
		}
	} else if ([self.previewTheme isEqualToString:@"Ninja Bunnies"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"NinjaBunniesBorder.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"NinjaBunniesCanvas.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"NinjaBunniesPocket.png";
		}
	} else if ([self.previewTheme isEqualToString:@"Purple Haze"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"PurpleHazeBorder.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"PurpleHazeCanvas.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"PurpleHazePocket.png";
		}
	} else if ([self.previewTheme isEqualToString:@"Nitro Burn"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"NitroBurnBorder.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"NitroBurnCanvas.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"NitroBurnPocket.png";
		}
	} else if ([self.previewTheme isEqualToString:@"Sweet Cravings"]) {
		if ([section isEqualToString:@"Border"]) {
			imageName = @"SweetCravingsBorder.png";
		} else if ([section isEqualToString:@"Canvas"]) {
			imageName = @"SweetCravingsCanvas.png";
		} else if ([section isEqualToString:@"Pocket"]) {
			imageName = @"SweetCravingsPocket.png";
		}
	}
	
	return imageName;
}

- (UIColor *)getColourForSection:(NSString *)section {
	// Colour sections
	UIColor *colour = [UIColor blueColor];
	
	if ([self.selectedTheme isEqualToString:@"Classic"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor lightTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor lightGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor lightTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor lightGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor lightGrayColor];
		}
	} else if ([self.selectedTheme isEqualToString:@"Autumn Dreams"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
	} else if ([self.selectedTheme isEqualToString:@"Hippy Wonderland"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
	} else if ([self.selectedTheme isEqualToString:@"Red Rocket"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
	} else if ([self.selectedTheme isEqualToString:@"Gold Rush"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
	}
	
    // Grab premium themes, but only if they are purchased, if not, revert to classic
	else if ([self.selectedTheme isEqualToString:@"Purple Haze"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemePurpleHazeID]) {
        if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
    } else if ([self.selectedTheme isEqualToString:@"Old Timer"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeOldTimerID]) {
        if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
    } else if ([self.selectedTheme isEqualToString:@"Ninja Bunnies"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeNinjaBunniesID]) {
        if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
    } else if ([self.selectedTheme isEqualToString:@"Sweet Cravings"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeSweetCravingsID]) {
        if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
    } else if ([self.selectedTheme isEqualToString:@"Nitro Burn"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeNitroBurnID]) {
        if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor lightTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor lightGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
    } else {
        if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor lightTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor lightGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor lightTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor lightGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor lightGrayColor];
		}
    }
    
	return colour;
}

- (UIColor *)getPreviewColourForSection:(NSString *)section {
	// Colour sections
	UIColor *colour = [UIColor blueColor];
	
	if ([self.previewTheme isEqualToString:@"Nitro Burn"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor lightTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor lightGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
        

	} else if ([self.previewTheme isEqualToString:@"Old Timer"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
	} else if ([self.previewTheme isEqualToString:@"Purple Haze"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
	} else if ([self.previewTheme isEqualToString:@"Sweet Cravings"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
	} else if ([self.previewTheme isEqualToString:@"Ninja Bunnies"]) {
		if ([section isEqualToString:@"MainViewTitleTextField"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"MainViewDateLabelButton"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueTextLabel"]) {
			colour = [UIColor darkTextColor];
		} else if ([section isEqualToString:@"ItemsDueDetailTextLabel"]) {
			colour = [UIColor darkGrayColor];
		} else if ([section isEqualToString:@"ItemsDueListLabel"]) {
			colour = [UIColor darkGrayColor];
		}
	}
	
	return colour;
}

- (NSString *)getTileViewButtonType {
	// Colour sections
	NSString *buttonType = @"Light";
	
	if ([self.selectedTheme isEqualToString:@"Classic"]) {
		buttonType = @"Light";
	} else if ([self.selectedTheme isEqualToString:@"Autumn Dreams"]) {
		buttonType = @"Dark";
	} else if ([self.selectedTheme isEqualToString:@"Hippy Wonderland"]) {
		buttonType = @"Dark";
	} else if ([self.selectedTheme isEqualToString:@"Red Rocket"]) {
		buttonType = @"Dark";
	} else if ([self.selectedTheme isEqualToString:@"Gold Rush"]) {
		buttonType = @"Dark";
	}// Grab premium themes, but only if they are purchased, if not, revert to classic
	else if ([self.selectedTheme isEqualToString:@"Purple Haze"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemePurpleHazeID]) {
        buttonType = @"Light";
    } else if ([self.selectedTheme isEqualToString:@"Old Timer"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeOldTimerID]) {
        buttonType = @"Light";
    } else if ([self.selectedTheme isEqualToString:@"Ninja Bunnies"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeNinjaBunniesID]) {
        buttonType = @"Dark";
    } else if ([self.selectedTheme isEqualToString:@"Sweet Cravings"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeSweetCravingsID]) {
        buttonType = @"Dark";
    } else if ([self.selectedTheme isEqualToString:@"Nitro Burn"] && [[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeNitroBurnID]) {
        buttonType = @"Light";
    } else {
        buttonType = @"Light";
    }
	
	return buttonType;
}

- (NSString *)getPreviewTileViewButtonType {
	// Colour sections
	NSString *buttonType = @"Light";
	
	if ([self.previewTheme isEqualToString:@"Nitro Burn"]) {
		buttonType = @"Light";
	} else if ([self.previewTheme isEqualToString:@"Sweet Cravings"]) {
		buttonType = @"Dark";
	} else if ([self.previewTheme isEqualToString:@"Ninja Bunnies"]) {
		buttonType = @"Dark";
	} else if ([self.previewTheme isEqualToString:@"Purple Haze"]) {
		buttonType = @"Light";
	} else if ([self.previewTheme isEqualToString:@"Old Timer"]) {
		buttonType = @"Dark";
	}
	
	return buttonType;
}

#pragma mark -
#pragma mark Query Methods

- (BOOL)doesListExistWithTitle:(NSString *)theTitle orFolderID:(NSInteger)folderID {
	for (TaskList *taskList in self.lists) {
        if ([taskList isNote]) {
            continue;
        }
        
		if (taskList.toodledoFolderID == folderID || [taskList.title isEqualToString:theTitle]) {
			// This is the matching folder, return YES
			return YES;
		}
	}
	return NO;
}

- (BOOL)doesListItemExistWithToodledoServerID:(SInt64)theToodledoServerID {
	for (TaskList *taskList in self.lists) {
        if ([taskList isNote]) {
            continue;
        }
        
		for (ListItem *listItem in taskList.listItems) {
            
			if ([listItem toodledoTaskID] == theToodledoServerID) {
				return YES;
			}
		}
	}
	return FALSE;
}

#pragma mark -
#pragma mark Update Methods

// Used when updating fiterTags and searchMode lists when using pocket reminder to update
- (void)findAndUpdateListItem:(ListItem *)listItemUpdate {
	// First try to find the list item in this task list collection
	NSInteger listIndex = [self getIndexOfTaskListWithID:listItemUpdate.listID];
	
	// Return if list index is not found
	if (listIndex == -1) {
		return;
	} 
		
	TaskList *taskList = [self.lists objectAtIndex:listIndex];
	
	// Now check if the listitem update is in this task list
	NSInteger listItemIndex = [taskList getIndexOfListItemWithID:listItemUpdate.listItemID];
	
	// Return if list item is not found
	if (listItemIndex == -1) {
		return;
	}
	
	ListItem *foundListItem = [taskList.listItems objectAtIndex:listItemIndex];
	[foundListItem updateSelfWithListItem:listItemUpdate];
    
}

- (BOOL)insertListItem:(ListItem *)listItem atCorrectLocationUsingArchiveList:(ArchiveList *)archiveList {
	NSInteger listIndex = [self getIndexOfTaskListWithID:listItem.listID];
	NSInteger archiveListIndex = [archiveList getIndexOfTaskListWithID:listItem.listID];
	if (listIndex == -1 && archiveListIndex == -1) {
		return NO;
	}
	
	TaskList *taskList = nil;
	if (listIndex != -1) {
		taskList = [self.lists objectAtIndex:listIndex];
	} else if (archiveListIndex != -1) {
		TaskList *taskListCopy = [archiveList.lists objectAtIndex:archiveListIndex];
		taskList = [[[TaskList alloc] initWithBasicTaskListCopy:taskListCopy] autorelease];
		[self.lists addObject:taskList];
	}
	
	if (taskList != nil) {
		[taskList.listItems addObject:listItem];
	} else {
		return NO;
	}

	return YES;
}

- (void)updateWithToodledoFolder:(ToodledoFolder *)folder {
	if (folder.folderID == 0 || folder.folderID == -1) {
		return;
	}
	
	NSInteger toodledoFolderListIndex = [self getIndexOfTaskListWithToodledoFolderID:folder.folderID];
	
	if (toodledoFolderListIndex != -1) {
		// All good, it exists
		return;
	}
	
	NSInteger listIndex = [self getIndexOfTaskListWithTitle:folder.name];
	
	if (listIndex == -1) {
		// Unable to find this title/name
		return;
	}
		 
	TaskList *taskList = [self.lists objectAtIndex:listIndex];
	taskList.toodledoFolderID = folder.folderID;
	
	// Update database
	[taskList updateDatabaseWithoutEditLog];
}

#pragma mark -
#pragma mark Sort Methods

/*- (void)moveFromListWithID:(NSInteger)fromID toListWithID:(NSInteger)toID {
	 ... Do whatever you need to do ... 
	//NSInteger fromIndex = [self getIndexOfTaskListWithID:fromID];
	//NSInteger toIndex = [self getIndexOfTaskListWithID:toID];
	
	// Exit method if either not found
	if (fromIndex == -1 || toIndex == -1) {
		return;
	}
	
	TaskList *listCopy = [[TaskList alloc] initWithTaskListCopy:[self.lists objectAtIndex:fromIndex]];
	
	[self.lists removeObjectAtIndex:fromIndex];
	
	// Insert the copy of the list in the correct location
	[self.lists insertObject:listCopy atIndex:toIndex];
	[listCopy release];*/
	
	//[self.lists exchangeObjectAtIndex:fromIndex withObjectAtIndex:toIndex];
	
	// Create a list item copy
	/*ListItem *listItemCopy = [[ListItem alloc] initWithListItemCopy:[refTaskList.listItems objectAtIndex:fromIndexPath.row]];
	
	// Delete the old list item
	[refTaskList.listItems removeObjectAtIndex:fromIndexPath.row];
	
	// Insert the new copy of the list item in the correct location
	[refTaskList.listItems insertObject:listItemCopy atIndex:toIndexPath.row];
	[listItemCopy release];
	
	// Update the item order for the affected list items
	ListItem *listItem = [refTaskList.listItems objectAtIndex:toIndexPath.row];
}*/

- (void)moveObjectWithListID:(NSUInteger)fromID toObjectWithListID:(NSUInteger)toID {
	NSInteger fromIndex = [self getIndexOfTaskListWithID:fromID];
	NSInteger toIndex = [self getIndexOfTaskListWithID:toID];
	
    if (toIndex != fromIndex) {
        id obj = [self.lists objectAtIndex:fromIndex];
        [obj retain];
        [self.lists removeObjectAtIndex:fromIndex];
        if (toIndex >= [self.lists count]) {
            [self.lists addObject:obj];
        } else {
            [self.lists insertObject:obj atIndex:toIndex];
        }
        [obj release];
    }
}

#pragma mark -
#pragma mark Database Methods

// Commit list order changes to the collection
- (void)commitListOrderChanges {
	NSInteger i = [self.lists count] - 1;
	for (TaskList *list in self.lists) {
		list.listOrder = i;
		//[list updateListOrder];
		i--;
	}
}

// Update database with list order changes
- (void)updateListOrdersInDatabase {
	for (TaskList *taskList in self.lists) {
		[taskList updateListOrder];
	}
}

#pragma mark -
#pragma mark Delete Methods

// Delete all lists and tasks (used for full sync)
- (void)deleteAllLists:(BOOL)isPermanent {
	// Need to set count initially and go backwards to accurately clean lists
	int count = [self.lists count];
	
	for (int i = count - 1; i >= 0; i--) {
        TaskList *taskList = [lists objectAtIndex:i];
        if ([taskList isNote] == FALSE) {
            [self deleteListAtIndex:i permanent:isPermanent];
        } 
	}
    
}


// Deletes the list at the specified index
- (void)deleteListAtIndex:(NSInteger)index permanent:(BOOL)isPermanent {
    if (index == -1 || index >= [lists count]) {
        return;
    }
    
	// Get the list id
	TaskList *list = [lists objectAtIndex:index];
	
	// Deleting list items must go in reverse.. ack
	
	// Need to set count initially and go backwards to accurately clean list items
	int count = [list.listItems count];
	for (int i = count - 1; i >= 0; i--) {
		// Need to make sure we do not delete archived list items
		// Note: There shouldn't be any archived list items in this list
		[list deleteListItemAtIndex:i permanent:isPermanent];
		//ListItem *listItem = [list.listItems objectAtIndex:i];
		//if ([listItem archived] == FALSE) {
		//}
	}
	
	if (isPermanent == FALSE) {
		NSString *lastDeleteDateString = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		[ApplicationSetting updateSettingName:@"LastDeleteListDateTime" withData:lastDeleteDateString];
		DeletedTaskList *deletedList = [[DeletedTaskList alloc] init];
		[deletedList softDeleteList:list];
		[deletedList release];
	}
	
    
    // Look if image exists to delete the gls and png versions
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSString *scribbleName = [list scribble];
	
	if ([scribbleName length] == 0) {
		scribbleName = @"GarbageValue";
	}

	NSString *imagePath = [NSString stringWithFormat:@"%@%@",
						   [MethodHelper getScribblePath], scribbleName];
	
	if ([fileManager fileExistsAtPath:imagePath]) {
		NSError *deleteFileError = nil;
		// Remove the file
		
		[fileManager removeItemAtPath:imagePath error:&deleteFileError];
		if (deleteFileError) {
			NSLog(@"%@", [deleteFileError localizedDescription]);
		}
	}
    
    // Delete png version if it exists
    NSRange glsRange = [scribbleName rangeOfString:@".gls"];
    NSString *pngScribbleName = @"GarbageValue";
    if (glsRange.length > 0) {
        pngScribbleName = [scribbleName substringToIndex:glsRange.location];
        pngScribbleName = [NSString stringWithFormat:@"%@.png", pngScribbleName];
    }
    NSString *pngImagePath = [NSString stringWithFormat:@"%@%@",
						   [MethodHelper getScribblePath], pngScribbleName];
    
    if ([fileManager fileExistsAtPath:pngImagePath]) {
		NSError *deleteFileError = nil;
		// Remove the file
		
		[fileManager removeItemAtPath:pngImagePath error:&deleteFileError];
		if (deleteFileError) {
			NSLog(@"%@", [deleteFileError localizedDescription]);
		}
	}
    
    
	// Delete the list from the database
	[BLList deleteListWithListID:list.listID];
	
	// Remove the list from the collection
	[self.lists removeObjectAtIndex:index];
}

#pragma mark -
#pragma mark Mail Methods

- (NSString *)createPDFMessageWithTagCollection:(TagCollection *)tagCollection showSubtasks:(BOOL)showSubtasks {
	NSString *saveDirectory = [MethodHelper getScribblePath];
	
	NSString *pdfFileName = [saveDirectory stringByAppendingPathComponent:@"Manage Task Lists.pdf"];	
	
	// Create the PDF context using the default page size of 612 x 792.
	UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
	
	// Mark the beginning of a new page.
	UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
	
	// Set up completed and not completed images
	UIImage *completedImage = [UIImage imageNamed:@"BoxTicked.png"];
	UIImage *notCompletedImage = [UIImage imageNamed:@"BoxNotTicked.png"];
	
	UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 553, 1)];
	[lineView setBackgroundColor:[UIColor lightGrayColor]];
	[lineView setAlpha:0.3f];
	UIGraphicsBeginImageContext(lineView.bounds.size);
	[lineView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *lineViewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	[lineView release];
	
	NSInteger currentPage = 0;
	
	int i = 0;
	
	NSInteger totalHeight = 0;
	
	for (TaskList *taskList in self.lists) {
		BOOL firstListItem = TRUE;
		
		taskList.currentRowHeight = 44;
		
		for (ListItem *listItem in taskList.listItems) {
			if (firstListItem == TRUE && i >= 13) {
				// Mark the beginning of a new page.
				UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
				i = 0;
			}
			
			if (i == 0) {
				// Draw title
				NSString *titleString = @"Manage Task Lists";
				[titleString drawInRect:CGRectMake(100, 35, 412, 40) 
							   withFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0] 
						  lineBreakMode:UILineBreakModeTailTruncation 
							  alignment:UITextAlignmentCenter];
				
				[lineViewImage drawInRect:CGRectMake(25, 84, 553, 1)];
				
				// Draw a page number at the bottom of each page
				currentPage++;
				[taskList drawPageNumber:currentPage];
			}
			
			if (firstListItem == TRUE) {
				// Draw task list title
				NSString *taskListTitle = taskList.title;
				[taskListTitle drawInRect:CGRectMake(38, totalHeight + 86 + 12, 412, 21) 
								 withFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0] 
							lineBreakMode:UILineBreakModeTailTruncation 
								alignment:UITextAlignmentLeft];
				
				// Draw line
				[lineViewImage drawInRect:CGRectMake(25, totalHeight + 44 + 84, 553, 1)];
				
				// Increment total height
				totalHeight = taskList.currentRowHeight + totalHeight;
				taskList.m_totalHeight = totalHeight;
				
				i++;
				firstListItem = FALSE;
			}
			
			// Item Title
			CGRect textRect = CGRectZero;
			taskList.taskItemFrame = CGRectZero;
			
			// Setup tags, and return text rect width
			
			// Task
			if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
				textRect = CGRectMake(55, totalHeight + 86 + 12, 450, 21);
			} else {
				// Subtask
				textRect = CGRectMake(78, totalHeight + 86 + 12, 427, 21);
			}
			
			BOOL hasDueDate = FALSE;
			
			if ([listItem.dueDate length] > 0 && listItem.dueDate != nil) {
				hasDueDate = TRUE;
			}
			
			UIView *tagsView = [taskList allocTagsForListItem:listItem 
										 andTagCollection:tagCollection 
												   forRow:i 
										 andTaskItemFrame:textRect
											   andHasDate:hasDueDate];
			
			if (tagsView != nil) {
				UIGraphicsBeginImageContext(tagsView.bounds.size);
				[tagsView.layer renderInContext:UIGraphicsGetCurrentContext()];
				UIImage *tagsViewImage = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();
				[tagsViewImage drawInRect:tagsView.frame];
			}
			
			NSString *listItemString = listItem.title;
			
			if (listItem.completed) {
				[[UIColor grayColor] setFill];
			} else {
				[[UIColor darkTextColor] setFill];
			}
			
			if (taskList.taskItemFrame.size.width > 0) {
				textRect = taskList.taskItemFrame;
			} else {
				taskList.taskItemFrame = textRect;
			}

			
			[listItemString drawInRect:textRect 
							  withFont:[UIFont fontWithName:@"Helvetica" size:14.0]
						 lineBreakMode:UILineBreakModeTailTruncation
							 alignment:UITextAlignmentLeft];
			
			// Scribbles
			if ([listItem.scribble length] > 0 && listItem.scribble != nil) {
				NSString *scribblePath = [NSString stringWithFormat:@"%@%@",
										  [MethodHelper getScribblePath],
										  listItem.scribble];
				UIImage *scribbleImage = [UIImage imageWithContentsOfFile:scribblePath];
				scribbleImage = [MethodHelper scaleImage:scribbleImage 
												maxWidth:500 maxHeight:44];
				
				CGRect drawingRect = CGRectZero;
				
				// Task
				if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
					drawingRect = CGRectMake(50, totalHeight + 86, 500, 44);
				} else {
					// Subtask
					drawingRect = CGRectMake(73, totalHeight + 86, 500, 44);
				}
				
				[scribbleImage drawInRect:drawingRect];
			}
			
			// Completed icon
			CGRect completedRect = CGRectZero;
			
			// Task
			if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
				completedRect = CGRectMake(18, totalHeight + 86, 40, 40);
			} else {
				// Subtask
				completedRect = CGRectMake(41, totalHeight + 86, 40, 40);
			}
			
			if (listItem.completed) {
				[completedImage drawInRect:completedRect];
			} else {
				[notCompletedImage drawInRect:completedRect];
			}
			
			// Highlighters
			if (listItem.priority != 0 && listItem.completed == NO) {
				[taskList drawHighlighterAtIndex:i forListItem:listItem showSubtasks:showSubtasks];
			}
			
			// Draw the due date
			[taskList drawDueDateAtIndex:i forListItem:listItem andTagsView:tagsView];
			
			// Tags view no longer needed
			[tagsView release];
			
			if (listItem.completed) {
				[[UIColor grayColor] setFill];
			} else {
				[[UIColor darkTextColor] setFill];
			}
			
			// Draw any notes
			if ([listItem.notes length] > 0) {
				CGRect notesRect = CGRectZero;
				
				NSString *notesString = listItem.notes;
				UIFont *notesFont = [UIFont fontWithName:@"Helvetica" size:10.0];
				
				NSInteger thisMaxRowHeight = kTotalTaskSectionHeight - taskList.currentRowHeight;
				
				// Need to get the height of it
				CGSize notesSize = [notesString sizeWithFont:notesFont
										   constrainedToSize:CGSizeMake(taskList.taskItemFrame.size.width, thisMaxRowHeight) lineBreakMode:UILineBreakModeWordWrap];
				
                // Make sure the notes don't go past the end of the page, if they do, then we need to split them up
                NSLog(@"notesize.height: %f + taskListhHeight: %d - totalTaskSectionHeight: %d",
                      notesSize.height, taskList.m_totalHeight, kTotalTaskSectionHeight);
                if (notesSize.height + taskList.m_totalHeight >= kTotalTaskSectionHeight) {
                    notesSize.height = kTotalTaskSectionHeight - taskList.m_totalHeight;
                }
                NSLog(@"2 notesize.height: %f + taskListhHeight: %d - totalTaskSectionHeight: %d",
                      notesSize.height, taskList.m_totalHeight, kTotalTaskSectionHeight);
				// Add to total row height (-10 so difference isnt so pronounced.
				if (notesSize.height > 8) {
					taskList.currentRowHeight = (notesSize.height - 8) + (taskList.currentRowHeight + 2);
				}
				
				if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
					// Task
					notesRect = CGRectMake((55 + 8), totalHeight + 86 + 12 + (taskList.taskItemFrame.size.height - 4), (taskList.taskItemFrame.size.width - 7), notesSize.height);
				} else {
					// Subtask
					notesRect = CGRectMake((78 + 8), totalHeight + 86 + 12 + (taskList.taskItemFrame.size.height - 4), (taskList.taskItemFrame.size.width - 7), notesSize.height);
				}
				
				[notesString drawInRect:notesRect withFont:notesFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
			}
			
			// Increment total height
			totalHeight = taskList.currentRowHeight + totalHeight;
			taskList.m_totalHeight = totalHeight;
			
			// Reset current row height
			taskList.currentRowHeight = 44;
			
			// Draw line
			[lineViewImage drawInRect:CGRectMake(25, totalHeight + 84, 553, 1)];

			// End of adding elements
			i++;
			
			if (i >= 14 || totalHeight >= kTotalTaskSectionHeight) {
				// Mark the beginning of a new page.
				UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
				i = 0;
				totalHeight = 0;
				taskList.m_totalHeight = totalHeight;
			}
		}
	}
	
	// Close the PDF context and write the contents out.
	UIGraphicsEndPDFContext();
	
	return pdfFileName;
}

- (NSString *)getTextMailMessageWithTagCollection:(TagCollection *)tagCollection showSubtasks:(BOOL)showSubtasks {
	NSMutableString *mailMessage = [NSMutableString stringWithString:@""];
	
	
	BOOL firstTaskList = TRUE;
	for (TaskList *taskList in self.lists) {
		// Add header in message
		if (firstTaskList == TRUE) {
			firstTaskList = FALSE;
		} else {
			[mailMessage appendString:@"\n\n"];
		}

		[mailMessage appendString:taskList.title];
		[mailMessage appendString:@"\n\n"];
		
		for (ListItem *listItem in taskList.listItems) {
			
			// Check if listitem is task or sub task
			if (listItem.parentListItemID == -1 || showSubtasks == FALSE) {
				[mailMessage appendString:@" - "];
			} else {
				[mailMessage appendString:@"   -- "];
			}
			
			if (listItem.priority == 1) {
				[mailMessage appendString:@"(Important) "];
			} else if (listItem.priority == 2) {
				[mailMessage appendString:@"(Urgent) "];
			}
			
			[mailMessage appendString:listItem.title];
			
			if ([listItem.dueDate length] > 0 && listItem.dueDate != nil) {
				NSDate *theDate = [MethodHelper dateFromString:listItem.dueDate usingFormat:K_DATEONLY_FORMAT];
				[mailMessage appendFormat:@" (%@)", 
				 [MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterFullStyle withYear:YES]];
			}
			
			
			NSMutableString *tagsString = [NSMutableString stringWithString:@""];
			BOOL firstRun = TRUE;
			for (Tag *tag in tagCollection.tags) {
				if ([listItem.listItemTagCollection containsTagWithID:tag.tagID] == FALSE) {
					continue;
				}
				
				if (firstRun == TRUE) {
					[tagsString appendFormat:@" {%@", tag.name];
					firstRun = FALSE;
				} else {
					[tagsString appendFormat:@", %@", tag.name];
				}
			}
			
			if ([tagsString length] > 0) {
				[tagsString appendString:@"}"];
				[mailMessage appendString:tagsString];
			}
			
			[mailMessage appendString:@"\n"];
			
			// Check for any notes
			if ([listItem.notes length] > 0) {
				if (listItem.parentListItemID == -1) {
					[mailMessage appendString:@"  [Notes: "];
				} else {
					[mailMessage appendString:@"      [Notes: "];
				}
				
				[mailMessage appendFormat:@"%@]\n", listItem.notes];
			}
		}
	}
	
	
	
	return mailMessage;
	
}

@end
