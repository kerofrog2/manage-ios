//
//  ItemsDueCollection.h
//  Manage
//
//  Created by Cliff Viegas on 15/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data collections
@class TaskListCollection;

// Data Models
@class ListItem;

@interface ItemsDueCollection : NSObject {
	NSMutableArray			*listItems;
	NSString				*currentParentListTitle;
	NSInteger				currentParentListID;
//	TaskListCollection		*taskListCollection;
	TaskListCollection		*refTaskListCollection;
}

@property	(nonatomic, retain)		NSMutableArray		*listItems;
//@property	(nonatomic, retain)		TaskListCollection	*taskListCollection;

#pragma mark Initialisation
- (id)initWithListParentTitle:(NSString *)theTitle andParentListID:(NSInteger)theParentListID 
		andTaskListCollection:(TaskListCollection *)theTaskListCollection;

#pragma mark Update Methods
- (void)sortListItems;
- (void)updateListItemsWithListItem:(ListItem *)listItem;
- (void)updateListItemsWithListParentTitle:(NSString *)theTitle andParentListID:(NSInteger)theParentListID;

#pragma mark Get Methods
- (NSInteger)getIndexOfListItemWithID:(NSInteger)theListItemID;

@end
