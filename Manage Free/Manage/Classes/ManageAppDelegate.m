//
//  ManageAppDelegate.m
//  Manage
//
//  Created by Cliff Viegas on 16/06/10.
//  Copyright kerofrog 2010. All rights reserved.
//

#import "ManageAppDelegate.h"

// View Controllers
#import "MainViewController.h"

// Business Layer
#import "BLApplicationSetting.h"
#import "BLConfig.h"
#import "BLTag.h"

// Data Models
#import "Tag.h"
#import "TaskList.h"
#import "ApplicationSetting.h"
#import "ListItem.h"
#import "Alarm.h"

// Data Collections
#import "TaskListCollection.h"
#import "AlarmCollection.h"

// Method Helper
#import "MethodHelper.h"

// Flurry
#import "FlurryAPI.h"

// GTExceptional
#import "GTExceptionalAPI.h"

// Appirater
#import "Appirater.h"

// In app purchase manager
#import "InAppPurchaseManager.h"

// Adjust constants for advertising
#import "MainConstants.h"
#import "ListConstants.h"

@implementation ManageAppDelegate

// Global access

@synthesize window;
@synthesize navigationController;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[masterTaskListCollection release];
	[navigationController release];
    [window release];
    [super dealloc];
}

#pragma mark -
#pragma mark Application Lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions { 
    // Set advertising buffer
    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchaseRemoveAdvertisingID] == FALSE) {
        [[NSUserDefaults standardUserDefaults] setInteger:66 forKey:kAdvertisingBuffer];
    } else {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:kAdvertisingBuffer];
    }
    
    // Start the flurry session
    [FlurryAPI startSession:@"N7WMP4QZCS6K1U6S3BY9"];
    
    // Start GTExceptional
    [[GTExceptionalAPI sharedAPI] setupExceptionHandling];
    
	// Required before running anything
	[self databaseChecks];
	
	// Create the window object
	UIWindow *localWindow = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	
	// Assign the localWindow to the AppDelegate window, then release the local window
	self.window = localWindow;
	[localWindow release];
	
	// Create any required resource folders
	[self createResourceFolders];
	
	// Set application will terminate delegate to self
	[[UIApplication sharedApplication] setDelegate:self];
	
	// Load the master task list collection
	masterTaskListCollection = [[TaskListCollection alloc] init];
	
	// Do the archive check to archive any completed items that meet requirements - do this in main view controller
	//[self archiveCheck];
	
	// NOTE: Important to do archive check before passing collection to main view controller
	
    // Override point for customization after application launch
	MainViewController *mainViewController = [[MainViewController alloc] initWithMasterTaskListCollection:masterTaskListCollection andManageAppDelegate:self];
	
	navigationController = [[UINavigationController alloc] initWithRootViewController:mainViewController];
	
	//navigationController.navigationBar.barStyle = UIBarStyleBlack;
	navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.14 green:0.14 blue:0.14 alpha:1.0];
	
	//navigationController.navigationBar.tintColor = [UIColor blackColor];
	//navigationController.navigationBar.translucent = YES;
	
	// Navigation controller has copy of projectsViewController, so release our copy
	[mainViewController release];
	
    // Override point for customization after app launch    
	[window addSubview:[navigationController view]];
	
    [window makeKeyAndVisible];

    [Appirater appLaunched:YES];
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Get number of list items with due date
	gNumberOfListItemsWithDueDate = [MethodHelper numberOfListItemsForBadgeNotification];
	
	// Update database if needed
	if ([masterTaskListCollection sortOrderUpdated] == TRUE) {
		[masterTaskListCollection updateListOrdersInDatabase];
	}

	// Add badge number to icon
	[[UIApplication sharedApplication] setApplicationIconBadgeNumber:gNumberOfListItemsWithDueDate];
}

// Handling a local notification when an application is already running
/*- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)localNotification {
	if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
		// The app was already running
		
		// Get the correct alarm (no need, just need to remove from DB and)
		// local notification queue (which is already done).
		
		
		// Create alarm collection and count how many alarms for this listitemID
		
		// Update 'hasAlarms' setting if there are no more alarms left for list item
		// Can't update this setting, just remove the alarm

		// Just move the alarm, can only update 'hasAlarms' when opening the item
		// Just delete the alarm from the db
	
		// Oops, also need to check any currently open collections to update listitem
		// Solution is only update it on startup... this can be handled when opened.
		
		//AlarmCollection *alarmCollection = [[AlarmCollection alloc] 
		
		// Need to create an alarm collection using the listitemID from this alarm
		
		// Need to call some business layer functions.  Can't be helped.
		// First get the alarm
		
		UIAlertView *alarmAlert = [[UIAlertView alloc] initWithTitle:@"Alarm" message:localNotification.alertBody delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alarmAlert show];
		[alarmAlert release];
		
	} else {
		// The user tapped the 'action' button while the app was not running
		// Note: Look at not having an 'action' button and just use an ok button
		// Do nothing as alarm was already shown.  Cleanup can be done elsewhere... ack.  Just view it.
	}
	
	// Remove the alarm from the DB, this 'should' be ok, but best to do testing anyways
	NSString *alarmGUID = [localNotification.userInfo objectForKey:@"AlarmGUID"];
	
	Alarm *alarm = [[Alarm alloc] initWithAlarmGUID:alarmGUID];
	
	// Delete the alarm from database
	[alarm deleteSelfFromDatabase];
	[alarm release];
	
	
   // [viewController displayItem:itemName];  // custom method
   // application.applicationIconBadgeNumber = notification.applicationIconBadgeNumber-1;
}*/

#pragma mark -
#pragma mark Multitasking Delegates

// Application resigning active state, pause any needed tasks
- (void)applicationWillEnterForeground:(UIApplication *)application {
    [Appirater appEnteredForeground:YES];
}

// Application is now in the background
- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Get number of list items with due date
	gNumberOfListItemsWithDueDate = [MethodHelper numberOfListItemsForBadgeNotification];
	
	// Update database if needed
	if ([masterTaskListCollection sortOrderUpdated] == TRUE) {
		[masterTaskListCollection updateListOrdersInDatabase];
	}
	
	// Add badge number to icon
	[[UIApplication sharedApplication] setApplicationIconBadgeNumber:gNumberOfListItemsWithDueDate];
}

- (void)applicationWillResignActive:(UIApplication *)application {
   // [self archiveCheck];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self archiveCheck];
    
    // 5007f29760f82600ea0000e4
    
    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchaseRemoveAdvertisingID] == FALSE) {
        [RevMobAds showFullscreenAdWithAppID:@"5007f29760f82600ea0000e4"];
    } 
    
    // Check for auto sync
    gRunToodledoAutoSync = FALSE;
    BOOL currentAutoSyncSetting = [[ApplicationSetting getSettingDataForName:@"ToodledoAutoSync"] boolValue];

    if (currentAutoSyncSetting == TRUE) {
        gRunToodledoAutoSync = TRUE;
    }
}

#pragma mark -
#pragma mark Class Methods

// Create scribble and other folders at run-time
- (void)createResourceFolders {
	NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *documentsPath;
	
	// Add scribble folder to documents path
	documentsPath = [searchPaths objectAtIndex: 0];
	documentsPath = [NSString stringWithFormat:@"%@/Scribble/", documentsPath];
	[fileManager createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:NULL];
	
	// Add backup folder to documents path
	documentsPath = [searchPaths objectAtIndex: 0];
	documentsPath = [NSString stringWithFormat:@"%@/Backup/", documentsPath];
	[fileManager createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:NULL];

	// Add log folder to documents path
	documentsPath = [searchPaths objectAtIndex: 0];
	documentsPath = [NSString stringWithFormat:@"%@/Log/", documentsPath];
	[fileManager createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:NULL];	
}

#pragma mark -
#pragma mark Custom Methods

- (void)archiveCheck {
	NSString *archiveItems = [ApplicationSetting getSettingDataForName:@"ArchiveItems"];

	if ([archiveItems isEqualToString:@"Manually"]) {
		// Do nothing
		return;
	}
	
	if ([archiveItems isEqualToString:@"On Application Launch"]) {
		// Archive now, do listitems first
		for (TaskList *taskList in masterTaskListCollection.lists) {
			[taskList archiveAllCompletedItems:FALSE];
		}
		
		[masterTaskListCollection archiveAllCompletedListsToArchiveList:nil];
	} else if ([archiveItems isEqualToString:@"Daily"]) {
		// Archive now, do listitems first
		for (TaskList *taskList in masterTaskListCollection.lists) {
			[taskList archiveAllCompletedItemsUsingDuration:1];
		}
		
		[masterTaskListCollection archiveAllCompletedListsUsingDuration:1];
	} else if ([archiveItems isEqualToString:@"Weekly"]) {
		// Archive now, do listitems first
		for (TaskList *taskList in masterTaskListCollection.lists) {
			[taskList archiveAllCompletedItemsUsingDuration:7];
		}
		
		[masterTaskListCollection archiveAllCompletedListsUsingDuration:7];
	}
}

// Just used for updating of database schema if required
- (void)databaseChecks {
	// First get version
	NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
	
	if ([[BLApplicationSetting getVersionNumber] isEqualToString:version]) {
		return;
	}
	
	// Look for application setting table, add if it does not exist
	if ([BLConfig checkIfTableExists:@"ApplicationSetting"] == FALSE) {
		//[BLConfig createTable:@"ApplicationSetting" withPrimaryKey:@"SettingID" ofType:@"INTEGER"];
		//[BLConfig addField:@"Name" toTable:@"ApplicationSetting" withType:@"VARCHAR"];
		//[BLConfig addField:@"Data" toTable:@"ApplicationSetting" withType:@"VARCHAR"];
		[BLConfig createApplicationSettingTable];
	}
	
	// Check if settings need to be updated
	[self updateSettings:version];
	
	// Look for the ListItemTag table
	if ([BLConfig checkIfTableExists:@"ListItemTag"] == FALSE) {
		// Create it as it doesnt exist
		[BLConfig createTable:@"ListItemTag" withPrimaryKey:@"ListItemID" andSecondaryKey:@"TagID" ofType:@"INTEGER"];
	}
	
	// Look for the alarm table for version 1.5, create it if it does not exist
	//if ([BLConfig checkIfTableExists:@"Alarm"] == FALSE) {
	//	[BLConfig createAlarmTable];
	//}
	
	// Look for the Tag table, add if it does not exist
	if ([BLConfig checkIfTableExists:@"Tag"] == FALSE) {
		[BLConfig createTable:@"Tag" withPrimaryKey:@"TagID" ofType:@"INTEGER"];
		[BLConfig addField:@"Name" toTable:@"Tag" withType:@"VARCHAR"];
		[BLConfig addField:@"TagOrder" toTable:@"Tag" withType:@"INTEGER"];
		[BLConfig addField:@"ParentTagID" toTable:@"Tag" withType:@"INTEGER"];
		[BLConfig addField:@"TagDepth" toTable:@"Tag" withType:@"INTEGER"];
        [BLConfig addField:@"Colour" toTable:@"Tag" withType:@"VARCHAR"];
		
		// Create records
		Tag *homeTag = [[Tag alloc] initWithTagID:1 andName:@"Home" andParentTagID:-1 andTagOrder:0 andTagDepth:0 andTagColour:@"Brown"];
		[homeTag insertSelfIntoDatabase];
		[homeTag release];
		
		Tag *workTag = [[Tag alloc] initWithTagID:2 andName:@"Work" andParentTagID:-1 andTagOrder:1 andTagDepth:0 andTagColour:@"Brown"];
		[workTag insertSelfIntoDatabase];
		[workTag release];
	}
	
    // For 1.6, look for the notebook table
    if ([BLConfig checkIfTableExists:@"Notebook"] == FALSE) {
        [BLConfig createTable:@"Notebook" withPrimaryKey:@"NotebookID" ofType:@"INTEGER"];
        [BLConfig addField:@"Title" toTable:@"Notebook" withType:@"VARCHAR"];
        [BLConfig addField:@"CreationDate" toTable:@"Notebook" withType:@"VARCHAR"];
        [BLConfig addField:@"ItemOrder" toTable:@"Notebook" withType:@"INTEGER"];
        [BLConfig addField:@"Lists" toTable:@"Notebook" withType:@"VARCHAR"];
    }
    
    // For 1.5.2, look for tag colour integer in Tag table
    if ([BLConfig checkIfColumn:@"Colour" existsInTable:@"Tag"] == FALSE) {
        // Add the tag colour field
        [BLConfig addField:@"Colour" toTable:@"Tag" withType:@"VARCHAR"];
    }
    
	// Look for the completed date time in the List table
	if ([BLConfig checkIfColumn:@"CompletedDateTime" existsInTable:@"List"] == FALSE) {
		// Add the completed date time field
		[BLConfig addField:@"CompletedDateTime" toTable:@"List" withType:@"VARCHAR"];
	}
	
	// Add a completed field with a default of 0
	if ([BLConfig checkIfColumn:@"Completed" existsInTable:@"List"] == FALSE) {
		// Add the completed field
		[BLConfig addField:@"Completed" toTable:@"List" withType:@"INTEGER" 
			   andNullable:NO andDefault:@"0"];
	}
	
	// Look to see if ModifiedDateTime field exists, if not, add it to ListItem table
	if ([BLConfig checkIfColumn:@"ModifiedDateTime" existsInTable:@"ListItem"] == FALSE) {
		// Add 'ModifiedDateTime' field
		[BLConfig addField:@"ModifiedDateTime" toTable:@"ListItem" withType:@"VARCHAR"];
	}
	
	// Add an archived field with a default of 0 to ListItem table
	if ([BLConfig checkIfColumn:@"Archived" existsInTable:@"ListItem"] == FALSE) {
		// Add 'Archived' field
		[BLConfig addField:@"Archived" toTable:@"ListItem" withType:@"INTEGER" andNullable:NO andDefault:@"0"];
	}
	
	// Add an archived date time field to ListItem table
	if ([BLConfig checkIfColumn:@"ArchivedDateTime" existsInTable:@"ListItem"] == FALSE) {
		// Add 'ArchivedDateTime' field
		[BLConfig addField:@"ArchivedDateTime" toTable:@"ListItem" withType:@"VARCHAR"];
	}
	
	// Added in 1.4.1, check if there is a 'HasAlarms' field in listitem table
	if ([BLConfig checkIfColumn:@"HasAlarms" existsInTable:@"ListItem"] == FALSE) {
		// Add 'HasAlarms' field
		[BLConfig addField:@"HasAlarms" toTable:@"ListItem" withType:@"INTEGER"];
	}
    
	// Add an archived field with a default of 0 to List table
	if ([BLConfig checkIfColumn:@"Archived" existsInTable:@"List"] == FALSE) {
		// Add 'Archived' field
		[BLConfig addField:@"Archived" toTable:@"List" withType:@"INTEGER" andNullable:NO andDefault:@"0"];
	}
	
	// Add an archived date time field to List table
	if ([BLConfig checkIfColumn:@"ArchivedDateTime" existsInTable:@"List"] == FALSE) {
		// Add 'ArchivedDateTime' field
		[BLConfig addField:@"ArchivedDateTime" toTable:@"List" withType:@"VARCHAR"];
	}
	
	// Added in 1.5, Look for the ToodledoFolderID field in the list table
	if ([BLConfig checkIfColumn:@"ToodledoFolderID" existsInTable:@"List"] == FALSE) {
		// Add the 'ToodledoFolderID' field
		[BLConfig addField:@"ToodledoFolderID" toTable:@"List" withType:@"VARCHAR"];
	}
	
	if ([BLConfig checkIfColumn:@"CreationDateTime" existsInTable:@"List"] == FALSE) {
		// Create the creationDateTime field
		[BLConfig addField:@"CreationDateTime" toTable:@"List" withType:@"VARCHAR"];
	}
	
	// Look for the ListOrder field in the List table
	if ([BLConfig checkIfColumn:@"ListOrder" existsInTable:@"List"] == FALSE) {
		// Create the field
		[BLConfig addField:@"ListOrder" toTable:@"List" withType:@"INTEGER"];
		
		// Need to add list order values to the List table
		TaskListCollection *taskListCollection = [[TaskListCollection alloc] initWithAllLists];
		NSInteger i = [taskListCollection.lists	count] - 1;
		for (TaskList *list in taskListCollection.lists) {
			list.listOrder = i;
			[list updateDatabase];
			i--;
		}
		
		[taskListCollection release];
	}
    
    // Added in 2.0, field for the is a note bool
    if ([BLConfig checkIfColumn:@"IsNote" existsInTable:@"List"] == FALSE) {
        // Add 'IsNote' field
        [BLConfig addField:@"IsNote" toTable:@"List" withType:@"INTEGER"];
    }
    
    // Added in 2.0, field for the location of note dictionary/drawing data (Scribble)
    if ([BLConfig checkIfColumn:@"Scribble" existsInTable:@"List"] == FALSE) {
        // Add 'Scribble' field
        [BLConfig addField:@"Scribble" toTable:@"List" withType:@"VARCHAR"];
    }
	
	// Added in 1.5, check if there is a 'ToodledoTaskID' field in listitem table
	if ([BLConfig checkIfColumn:@"ToodledoTaskID" existsInTable:@"ListItem"] == FALSE) {
		// Add 'toodledoTaskID' field
		[BLConfig addField:@"ToodledoTaskID" toTable:@"ListItem" withType:@"VARCHAR"];
	}
		 
	// Look for the completedDateTime field in the list item table
	if ([BLConfig checkIfColumn:@"CompletedDateTime" existsInTable:@"ListItem"] == FALSE) {
		// Create completed date time field if not there
		[BLConfig addField:@"CompletedDateTime" toTable:@"ListItem" withType:@"VARCHAR"];
		
		// Need to go through any completed items and set the current time
		TaskListCollection *taskListCollection = [[TaskListCollection alloc] initWithAllLists];
		
		for (TaskList *list in taskListCollection.lists) {
			for (ListItem *listItem in list.listItems) {
				if ([listItem completed] == TRUE) {
					NSString *theCompletedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
					listItem.completedDateTime = theCompletedDateTime;
					[listItem updateDatabase];
				}
			}
		}
		
		[taskListCollection release];
	}
	
	// Create recurring list item fields and table
	if ([BLConfig checkIfTableExists:@"RecurringListItem"] == FALSE) {
		[BLConfig createTable:@"RecurringListItem" withPrimaryKey:@"RecurringListItemID" ofType:@"INTEGER"];
		[BLConfig addField:@"Frequency" toTable:@"RecurringListItem" withType:@"INTEGER"];
		[BLConfig addField:@"FrequencyMeasurement" toTable:@"RecurringListItem" withType:@"VARCHAR"];
		[BLConfig addField:@"DaysOfTheWeek" toTable:@"RecurringListItem" withType:@"VARCHAR"];
		[BLConfig addField:@"UseCompletedDate" toTable:@"RecurringListItem" withType:@"INTEGER"];
	}
	
	// Also need to create a recurring list item ID field in 'ListItem' table
	if ([BLConfig checkIfColumn:@"RecurringListItemID" existsInTable:@"ListItem"] == FALSE) {
		 [BLConfig addField:@"RecurringListItemID" toTable:@"ListItem" withType:@"INTEGER" 
				andNullable:YES andDefault:@"-1"];
	}
	
	// Create DeletedListItem table
	if ([BLConfig checkIfTableExists:@"DeletedListItem"] == FALSE) {
		[BLConfig createDeletedListItemTable];
	}
	
	// Create DeletedList table
	if ([BLConfig checkIfTableExists:@"DeletedList"] == FALSE) {
		[BLConfig createDeletedListTable];
	}

	if ([BLConfig checkIfColumn:@"IsNote" existsInTable:@"DeletedList"] == FALSE) {
        [BLConfig addField:@"IsNote" toTable:@"DeletedList" withType:@"INTEGER"];
    }
    
	// Update the version number
	[BLApplicationSetting updateSetting:@"VersionNumber" withData:version];
	
}

// For checking what settings need to be updated
- (void)updateSettings:(NSString *)version {
	// 100 - 199 will be reserved for system settings
	// 200 - 299 will be for user settings
	if ([BLApplicationSetting checkIfSettingExists:@"VersionNumber"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"VersionNumber" andData:version];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"BadgeNotification"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"BadgeNotification" andData:@"Due and Overdue Tasks"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ShowCompleted"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ShowCompleted" andData:@"1"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"SelectedTheme"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"SelectedTheme" andData:@"Classic"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ShowCompletedDuration"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ShowCompletedDuration" andData:@"One Day"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ArchiveItems"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ArchiveItems" andData:@"Daily"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"PocketReminder"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"PocketReminder" andData:@"1"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"DefaultView"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"DefaultView" andData:@"List"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"NewListOrder"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"NewListOrder" andData:@"Beginning"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"NewTaskOrder"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"NewTaskOrder" andData:@"Beginning"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"DefaultSortOrder"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"DefaultSortOrder" andData:@"List"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	// DATE & TIME FOR LAST EDIT, ADD AND DELETE TASK
	if ([BLApplicationSetting checkIfSettingExists:@"LastEditTaskDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastEditTaskDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"LastDeleteTaskDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastDeleteTaskDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"LastAddTaskDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastAddTaskDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	// DATE & TIME FOR LAST EDIT, ADD AND DELETE LIST
	if ([BLApplicationSetting checkIfSettingExists:@"LastEditListDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastEditListDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
		
	if ([BLApplicationSetting checkIfSettingExists:@"LastDeleteListDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastDeleteListDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
		
	if ([BLApplicationSetting checkIfSettingExists:@"LastAddListDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"LastAddListDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	// TOODLEDO settings
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoUserName"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoUserName" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoUserID"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoUserID" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoSessionToken"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoSessionToken" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoKey"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoKey" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoServiceStatus"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoServiceStatus" andData:@"Not Connected"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoLastSyncDateTime"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoLastSyncDateTime" andData:@""];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
	
	if ([BLApplicationSetting checkIfSettingExists:@"ToodledoServerHasConflictPriority"] == FALSE) {
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoServerHasConflictPriority" andData:@"Local"];
		[BLApplicationSetting insertSetting:appSetting];
		[appSetting release];
	}
    
    if ([BLApplicationSetting checkIfSettingExists:@"ToodledoAutoSync"] == FALSE) {
        // 0 should be false
        ApplicationSetting *appSetting = [[ApplicationSetting alloc] initWithName:@"ToodledoAutoSync" andData:@"0"];
        [BLApplicationSetting insertSetting:appSetting];
        [appSetting release];
    }
}

@end
