//
//  ApplicationSetting.m
//  Manage
//
//  Created by Cliff Viegas on 4/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ApplicationSetting.h"

// Business Layer
#import "BLApplicationSetting.h"

@implementation ApplicationSetting

@synthesize name;
@synthesize data;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[self.name release];
	//[self.data release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		self.name = @"";
		self.data = @"";
	}
	return self;
}

- (id)initWithName:(NSString *)theName andData:(NSString *)theData {
	if (self = [super init]) {
		self.name = theName;
		self.data = theData;
	}
	return self;
}

#pragma mark -
#pragma mark Select Methods

- (void)loadApplicationSettingWithName:(NSString *)theName {
	NSString *settingData = [BLApplicationSetting getSettingData:theName];
	
	if ([settingData isEqual:[NSNull null]]) {
		settingData = @"";
	}

	self.name = theName;
	self.data = settingData;
}

#pragma mark -
#pragma mark Static Get and Set Methods

+ (NSString *)getSettingDataForName:(NSString *)theName {
	NSString *settingData = [BLApplicationSetting getSettingData:theName];
	
	if ([settingData isEqual:[NSNull null]]) {
		settingData = @"";
	}
	
	return settingData;
}

+ (void)updateSettingName:(NSString *)theName withData:(NSString *)theData {
	[BLApplicationSetting updateSetting:theName withData:theData];
}

#pragma mark -
#pragma mark Update Methods

- (void)updateDatabase {
	[BLApplicationSetting updateSetting:self.name withData:self.data];
}

@end
