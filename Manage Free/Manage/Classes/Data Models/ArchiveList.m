//
//  ArchiveList.m
//  Manage
//
//  Created by Cliff Viegas on 3/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ArchiveList.h"

// Business Layer
#import "BLListItem.h"
#import "BLList.h"

// Data Models
#import	"ListItem.h"
#import "TaskList.h"
#import "ApplicationSetting.h"
#import "DeletedTaskList.h"
#import "DeletedListItem.h"

// Data Collections
#import "TaskListCollection.h"

// Helper Class
#import "DMHelper.h"

// Method Helper
#import "MethodHelper.h"

// Private methods
@interface ArchiveList()

@end


@implementation ArchiveList

@synthesize listItems;
@synthesize lists;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[self.listItems release];
	//[self.lists release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithArchivedListItems {
	if ((self = [super init])) {
		self.listItems = [[[NSMutableArray alloc] init] autorelease];
		self.lists = [[[NSMutableArray alloc] init] autorelease];
		
		// Load archived list items
		[self loadArchivedListItems];
		
		// Load a basic version of archived lists
		[self loadBasicArchivedLists];
	}
	
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadArchivedListItems {
	// Get all archived list items
	NSArray *localArray = [BLListItem getAllArchivedListItems];
	
	// Create a new object for each listitem taken from the database and add it to our list
	for (NSDictionary *listItemDictionary in localArray) {
		NSInteger theListID = [DMHelper getNSIntegerValueForKey:@"ListID" fromDictionary:listItemDictionary];
		NSInteger thePriority = [DMHelper getNSIntegerValueForKey:@"Priority" fromDictionary:listItemDictionary];
		NSInteger theListItemID = [DMHelper getNSIntegerValueForKey:@"ListItemID" fromDictionary:listItemDictionary];

        
		ListItem *listItem = [[[ListItem alloc] initExistingListItemWithListItemID:theListItemID
																		 andListID:theListID
																	   andPriority:thePriority
																	andParentTitle:@""] autorelease];
		
		listItem.title = [DMHelper getNSStringValueForKey:@"Title" fromDictionary:listItemDictionary];
		listItem.creationDateTime = [DMHelper getNSStringValueForKey:@"CreationDateTime" fromDictionary:listItemDictionary];
		listItem.dueDate = [DMHelper getNSStringValueForKey:@"DueDate" fromDictionary:listItemDictionary];
		listItem.notes = [DMHelper getNSStringValueForKey:@"Notes" fromDictionary:listItemDictionary];
		listItem.scribble = [DMHelper getNSStringValueForKey:@"Scribble" fromDictionary:listItemDictionary];
		listItem.completed = [DMHelper getBoolValueForKey:@"Completed" fromDictionary:listItemDictionary];
		listItem.itemOrder = [DMHelper getIntegerValueForKey:@"ItemOrder" fromDictionary:listItemDictionary];
		listItem.parentListItemID = [DMHelper getIntegerValueForKey:@"ParentListItemID" fromDictionary:listItemDictionary];
		listItem.subItemOrder = [DMHelper getIntegerValueForKey:@"SubItemOrder" fromDictionary:listItemDictionary];
		listItem.completedDateTime = [DMHelper getNSStringValueForKey:@"CompletedDateTime" fromDictionary:listItemDictionary];
		listItem.archived = [DMHelper getBoolValueForKey:@"Archived" fromDictionary:listItemDictionary];
		listItem.archivedDateTime = [DMHelper getNSStringValueForKey:@"ArchivedDateTime" fromDictionary:listItemDictionary];
		listItem.recurringListItemID = [DMHelper getNSIntegerValueForKey:@"RecurringListItemID" fromDictionary:listItemDictionary];
		listItem.modifiedDateTime = [DMHelper getNSStringValueForKey:@"ModifiedDateTime" fromDictionary:listItemDictionary];
		listItem.toodledoTaskID = [DMHelper getSInt64ValueForKey:@"ToodledoTaskID" fromDictionary:listItemDictionary];
		

        
		[self.listItems addObject:listItem];
	}
}

- (void)loadBasicArchivedLists {
	// Get all archived lists
	NSArray *localArray = [BLList getAllArchivedLists];
	
	// Create a new object for each list taken from the database and add it to our lists
	for (NSDictionary *listDictionary in localArray) {
		// Basic init as we dont want listitems being added
		TaskList *taskList = [[[TaskList alloc] init] autorelease];
		
		taskList.listID = [DMHelper getNSIntegerValueForKey:@"ListID" fromDictionary:listDictionary];
		taskList.title = [DMHelper getNSStringValueForKey:@"Title" fromDictionary:listDictionary];
		taskList.creationDate = [DMHelper getNSStringValueForKey:@"CreationDate" fromDictionary:listDictionary];
		taskList.screenshot = [DMHelper getNSStringValueForKey:@"Screenshot" fromDictionary:listDictionary];
		taskList.listOrder = [DMHelper getNSIntegerValueForKey:@"ListOrder" fromDictionary:listDictionary];
		taskList.completedDateTime = [DMHelper getNSStringValueForKey:@"CompletedDateTime" fromDictionary:listDictionary];
		taskList.completed = [DMHelper getBoolValueForKey:@"Completed" fromDictionary:listDictionary];
		taskList.archived = [DMHelper getBoolValueForKey:@"Archived" fromDictionary:listDictionary];
		taskList.archivedDateTime = [DMHelper getNSStringValueForKey:@"ArchivedDateTime" fromDictionary:listDictionary];
		taskList.toodledoFolderID = [DMHelper getNSIntegerValueForKey:@"ToodledoFolderID" fromDictionary:listDictionary];
		taskList.creationDateTime = [DMHelper getNSStringValueForKey:@"CreationDateTime" fromDictionary:listDictionary];
		
        taskList.isNote = [DMHelper getBoolValueForKey:@"IsNote" fromDictionary:listDictionary];
        taskList.scribble = [DMHelper getNSStringValueForKey:@"Scribble" fromDictionary:listDictionary];

        
		[self.lists addObject:taskList];
	}
}

#pragma mark -
#pragma mark Delete Methods

// Only used for when doing a local replace from toodledo
- (void)deleteAllArchives {
	[BLList deleteAllArchivedLists];
	[BLListItem deleteAllArchivedListItems];
}

// Deletes the list at the specified index
- (void)deleteListAtIndex:(NSInteger)index permanent:(BOOL)isPermanent {
	// Get the list id
	TaskList *list = [lists objectAtIndex:index];
	
	if (isPermanent == FALSE) {
		NSString *lastDeleteDateString = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		[ApplicationSetting updateSettingName:@"LastDeleteListDateTime" withData:lastDeleteDateString];
		DeletedTaskList *deletedList = [[DeletedTaskList alloc] init];
		[deletedList softDeleteList:list];
		[deletedList release];
	}
	
	// Delete the list from the database
	[BLList deleteListWithListID:list.listID];
	
	// Remove the list from the collection
	[self.lists removeObjectAtIndex:index];
}

// Deletes the list item at the specified index
- (void)deleteListItemAtIndex:(NSInteger)index permanent:(BOOL)isPermanent {
	// Get the list item id
	ListItem *listItem = [self.listItems objectAtIndex:index];
	
	if (isPermanent == FALSE) {
		NSString *lastDeleteDateString = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		[ApplicationSetting updateSettingName:@"LastDeleteTaskDateTime" withData:lastDeleteDateString];
		DeletedListItem *deletedListItem = [[DeletedListItem alloc] init];
		[deletedListItem softDeleteListItem:listItem];
		[deletedListItem release];
	}
	
	// Delete the listitem from the database
	[BLListItem deleteListItemWithListItemID:listItem.listItemID];
	
	// Remove the list item from the collection
	[self.listItems removeObjectAtIndex:index];
}

#pragma mark -
#pragma mark Get Methods

- (NSInteger)getIndexOfListItemWithID:(NSInteger)listItemID {
	int i = 0;
	for (ListItem *listItem in self.listItems) {
		if (listItem.listItemID == listItemID) {
			return i;
		}
		i++;
	}
	
	// Not found
	return -1;
}

- (NSInteger)getIndexOfListItemWithToodledoServerID:(SInt64)theToodledoServerID {
	int i = 0;
	for (ListItem *listItem in self.listItems) {
		if (listItem.toodledoTaskID == theToodledoServerID) {
			return i;
		}
		i++;
	}
	
	// Not found
	return -1;
}

#pragma mark -
#pragma mark Query Methods

- (BOOL)doesListItemExistWithToodledoServerID:(SInt64)theToodledoServerID {
	for (ListItem *listItem in self.listItems) {
		if ([listItem toodledoTaskID] == theToodledoServerID) {
			return YES;
		}
	}
	return NO;
}

#pragma mark -
#pragma mark Helper Methods

- (void)updateTaskListCollection:(TaskListCollection *)taskListCollection withListsCreatedAfter:(NSString *)dateTimeString {
	for (TaskList *archiveTaskList in self.lists) {
		TaskList *newTaskList = [[[TaskList alloc] initWithBasicTaskListCopy:archiveTaskList] autorelease];
		
        if ([newTaskList isNote]) {
            continue;
        }
        
		// Need to compare dates
		NSComparisonResult comparisonResult = [newTaskList.creationDateTime compare:dateTimeString];
		
		// Means our dateTimeString (last sync datetime) is earlier than creation date, so get it
		if (comparisonResult == NSOrderedDescending) {
			[taskListCollection.lists addObject:newTaskList];
		}
	}
}

// GetnonSynced bool is there to grab tasks that were missed in a collection due to datetime differences
- (void)updateTaskListCollection:(TaskListCollection *)taskListCollection withListItemsCreatedAfter:(NSString *)dateTimeString getNonSynced:(BOOL)getNonSynced {
	// Perhaps no need to grab these as no need to add to toodledo?  Nope, we want them
	for (ListItem *archiveListItem in self.listItems) {
		ListItem *newListItem = [[[ListItem alloc] initWithListItemCopy:archiveListItem] autorelease];
		
		// Compare dates
		NSComparisonResult comparisonResult = [newListItem.creationDateTime compare:dateTimeString];
		
		// Means our dateTimeString (last sync datetime) is earlier than creation date, so get it
		if (comparisonResult == NSOrderedDescending || (getNonSynced == TRUE && [newListItem toodledoTaskID] == -1)) {
			[taskListCollection insertListItem:newListItem atCorrectLocationUsingArchiveList:self];
		}
	}
}

- (void)updateTaskListCollection:(TaskListCollection *)taskListCollection withListItemsModifiedAfter:(NSString *)dateTimeString {
	// Perhaps no need to grab these as no need to add to toodledo?  Nope, we want them
	for (ListItem *archiveListItem in self.listItems) {
		ListItem *newListItem = [[[ListItem alloc] initWithListItemCopy:archiveListItem] autorelease];
		
		// Compare dates
		NSComparisonResult comparisonResult = [newListItem.modifiedDateTime compare:dateTimeString];
		
		// Means our dateTimeString (last sync datetime) is earlier than creation date, so get it
		if (comparisonResult == NSOrderedDescending) {
			[taskListCollection insertListItem:newListItem atCorrectLocationUsingArchiveList:self];
		}
	}
}

- (NSInteger)getIndexOfTaskListWithToodledoFolderID:(NSInteger)theToodledoFolderID {
	int i = 0;
	for (TaskList *taskList in self.lists) {
		if (taskList.toodledoFolderID == theToodledoFolderID) {
			// Return this task location
			return i;
		}
		i++;
	}
	
	// Not found, return -1
	return -1;
}

- (NSInteger)getIndexOfTaskListWithID:(NSInteger)theListID {
	int i = 0;
	for (TaskList *taskList in self.lists) {
		if (taskList.listID == theListID) {
			// Return this task list location
			return i;
		}
		i++;
	}
	
	// Not found, return -1
	return -1;
}

// Need to find and return the parent list title, it has to exist somewhere... unless the list has been deleted?  In which case return "Toodledo Tasks"
- (NSString *)getParentListTitleOfListItemWithParentListID:(NSInteger)parentListID usingMasterTaskListCollection:(TaskListCollection *)masterTaskListCollection {
    NSInteger parentListIndex = [masterTaskListCollection getIndexOfTaskListWithID:parentListID];
    NSInteger archiveParentListIndex = [self getIndexOfTaskListWithID:parentListID];
    
    NSString *parentListTitle = @"";
    
    // Defaul title to return if not found
    if (parentListIndex != -1) {
        TaskList *taskList = [masterTaskListCollection.lists objectAtIndex:parentListIndex];
        parentListTitle = taskList.title;
    } else if (archiveParentListIndex != -1) {
        TaskList *taskList = [self.lists objectAtIndex:archiveParentListIndex];
        parentListTitle = taskList.title;
    } else if (parentListIndex == -1 && archiveParentListIndex == -1) {
        parentListTitle = @"Toodledo Tasks";
    } 
    
    return parentListTitle;
}

@end
