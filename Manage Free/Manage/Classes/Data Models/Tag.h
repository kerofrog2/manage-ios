//
//  Tag.h
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	EnumTagColourBrown = 0,
	EnumTagColourBlue = 1,
    EnumTagColourRed = 2
} EnumTagColour;

@interface Tag : NSObject <NSCopying> {
	NSInteger	tagID;
	NSString	*name;
	NSInteger	tagOrder;
	NSInteger	parentTagID;
	NSInteger	tagDepth;
    NSString    *colour;
}

@property (nonatomic, assign)		NSInteger		tagID;
@property (nonatomic, copy)         NSString		*name;
@property (nonatomic, assign)		NSInteger		tagOrder;
@property (nonatomic, assign)		NSInteger		parentTagID;
@property (nonatomic, assign)		NSInteger		tagDepth;
@property (nonatomic, copy)         NSString        *colour;

#pragma mark Initialisation
- (id)initWithTagID:(NSInteger)theTagID andName:(NSString *)theName 
	 andParentTagID:(NSInteger)theParentTagID andTagOrder:(NSInteger)theTagOrder
		andTagDepth:(NSInteger)theTagDepth andTagColour:(NSString *)theTagColour;

#pragma mark Database Methods
- (NSInteger)insertSelfIntoDatabase;

#pragma mark Class Methods
- (NSInteger)getNewTagOrder;

#pragma mark Update Methods
- (void)updateSelfInDatabase;

@end
