//
//  ToodledoTask.h
//  Manage
//
//  Created by Cliff Viegas on 14/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ToodledoTask : NSObject {
	SInt64				serverID;		// (float)Server id number for this task
	NSString			*title;			// Title of task (up to 255 characters)
	NSString			*tags;			// Comma seperated string listing tags of task
	SInt64				folderID;		// The id number of the folder
	NSInteger			contextID;		// The id number of the context
	NSInteger			goalID;			// The id number of the goal
	NSInteger			locationID;		// The id number of the location
	SInt64				parentID;		// The id number of the parent task
	NSInteger			children;		// The number of children for this task
	NSInteger			order;			// The order of subtasks
	SInt64				dueDate;		// (float) Timestamp for when task is due
	NSInteger			dueDateMod;		// integer representing due date modifier 
										// (0 = due by, 1 = dueOn, 2 = dueAfter, 3 = optionally
	SInt64				startDate;		// (float) Unix timestamp for when task starts (time component always noon)
	SInt64				dueTime;		// (float) Unix timestamp for time when task is due
	SInt64				startTime;		// (float) Unix timestamp for time when task starts
	NSInteger			remind;			// Inter that represents number of minutes prior to duedate/time
	NSString			*repeat;			// String indicating how task repeats
	NSInteger			repeatFrom;		// How task repeats, 0 from due-date or 1 from completion date
	NSInteger			status;			// Integer that represents status of task
	NSInteger			length;			// Integer representing number of minutes task will take to complete
	NSInteger			priority;		// Integer that represents priority, 0=low, 1=medium,
										// 2=high, 3=top and -1=negative
	BOOL				star;			// Boolean that indicates if task has star or not
	SInt64				modified;		// (float) Unix timestamp for when task last modified
	SInt64				completed;		// (float) Unix timestamp for when task was completed (0 if not completed)
	NSString			*added;			// (float) Unix timestamp for when task was added (time not recorded, always noon)
	NSString			*timer;			// (float) Number of seconds that have elapsed for the timer
	NSString			*timerOn;		// (float) If timer is currently on, then will have timestamp
	NSString			*note;			// Text string up to 32k bytes long
	NSString			*meta;			// For storing Manage metadata about the task

	// Helper properties
	BOOL				needsUpdate;
}

@property	(nonatomic, assign)		SInt64			serverID;
@property	(nonatomic, copy)		NSString		*title;
@property	(nonatomic, copy)		NSString		*tags;
@property	(nonatomic, assign)		SInt64			folderID;
@property	(nonatomic, assign)		NSInteger		contextID;
@property	(nonatomic, assign)		NSInteger		goalID;
@property	(nonatomic, assign)		NSInteger		locationID;
@property	(nonatomic, assign)		SInt64			parentID;
@property	(nonatomic, assign)		NSInteger		children;
@property	(nonatomic, assign)		NSInteger		order;
@property	(nonatomic, assign)		SInt64			dueDate;
@property	(nonatomic, assign)		NSInteger		dueDateMod;
@property	(nonatomic,	assign)		SInt64			startDate;
@property	(nonatomic, assign)		SInt64			dueTime;
@property	(nonatomic, assign)		SInt64			startTime;
@property	(nonatomic, assign)		NSInteger		remind;
@property	(nonatomic, copy)		NSString		*repeat;
@property	(nonatomic, assign)		NSInteger		repeatFrom;
@property	(nonatomic, assign)		NSInteger		status;
@property	(nonatomic, assign)		NSInteger		length;
@property	(nonatomic, assign)		NSInteger		priority;
@property	(nonatomic, assign)		BOOL			star;
@property	(nonatomic, assign)		SInt64			modified;
@property	(nonatomic, assign)		SInt64			completed;
@property	(nonatomic, copy)		NSString		*added;
@property	(nonatomic, copy)		NSString		*timer;
@property	(nonatomic, copy)		NSString		*timerOn;
@property	(nonatomic, copy)		NSString		*note;
@property	(nonatomic, copy)		NSString		*meta;

// Helper properties
@property	(nonatomic, assign)		BOOL			needsUpdate;

@end
