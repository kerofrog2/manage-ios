//
//  DMHelper.m
//  iDesign
//
//  Created by Cliff Viegas on 25/02/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// DMHelper stands for Dictionary Method Helper
// It is used to assist in extracting non-string values from a dictionary object.  It
// is also used to cater for cases where the value in the dictionary object is null.

// Header File
#import "DMHelper.h"

@implementation DMHelper

// Returns string value for the passed key
+ (NSString *)getNSStringValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary {
	NSString *stringReturn;
	
	if ([[aDictionary valueForKey:key] isEqual:[NSNull null]]) {
		stringReturn = @"";
	} else {
		stringReturn = [aDictionary valueForKey:key];
	}
	
	if ([stringReturn isEqualToString:@"(null)"]) {
			stringReturn = @"";
	}

	
	
	return stringReturn;
}


// Returns NSInteger value for the passed key
+ (NSInteger)getNSIntegerValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary {
	NSInteger integerReturn;
	
	if ([[aDictionary valueForKey:key] isEqual:[NSNull null]]) {
		integerReturn = 0;
	} else {
		integerReturn = [[aDictionary valueForKey:key] integerValue];
	}
	
	return integerReturn;
}

// Returns int value for the passed key
+ (int)getIntegerValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary {
	int integerReturn;
	
	if ([[aDictionary valueForKey:key] isEqual:[NSNull null]]) {
		integerReturn = 0;
	} else {
		integerReturn = [[aDictionary valueForKey:key] intValue];
	}
	
	return integerReturn;
}

// Returns double value for the passed key
+ (double)getDoubleValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary {
	double doubleReturn;
	
	if ([[aDictionary valueForKey:key] isEqual:[NSNull null]]) {
		doubleReturn = 0;
	} else {
		doubleReturn = [[aDictionary valueForKey:key] doubleValue];
	}
	
	return doubleReturn;
}

// Returns float value for the passed key
+ (float)getFloatValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary {
	float floatReturn;
	
	if ([[aDictionary valueForKey:key] isEqual:[NSNull null]]) {
		floatReturn = 0;
	} else {
		floatReturn = [[aDictionary valueForKey:key] floatValue];
	}
	
	return floatReturn;
}

// Returns BOOL value for the passed key
+ (BOOL)getBoolValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary {
	BOOL boolReturn;
	
	if ([[aDictionary valueForKey:key] isEqual:[NSNull null]]) {
		boolReturn = FALSE;
	} else {
		boolReturn = [[aDictionary valueForKey:key] boolValue];
	}
	
	return boolReturn;
}

// Returns long long value for the passed key
+ (SInt64)getSInt64ValueForKey:(NSString *)key fromDictionary:(NSDictionary *)aDictionary {
	SInt64 longlongValue;
	
	if ([[aDictionary valueForKey:key] isEqual:[NSNull null]]) {
		longlongValue = 0;
	} else {
		longlongValue = [[aDictionary valueForKey:key] longLongValue];
	}
	
	return longlongValue;
}

@end
