//
//  BLList.h
//  Manage
//
//  Created by Cliff Viegas on 17/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class TaskList;

// CREATE TABLE "List" ("ListID" INTEGER PRIMARY KEY  NOT NULL ,"Title" VARCHAR NOT NULL  DEFAULT 'List' ,"CreationDate" VARCHAR,"Screenshot" VARCHAR)

@interface BLList : NSObject {

}

#pragma mark SELECT Methods
+ (NSArray *)getAllLists;
+ (NSArray *)getAllArchivedLists;
+ (NSInteger)getNextListOrder;
+ (NSInteger)getNumberOfListsWithTitle:(NSString *)theTitle;

#pragma mark INSERT Methods
+ (NSInteger)insertNewList:(TaskList *)list;

#pragma mark UPDATE Methods
+ (void)updateDatabaseWithList:(TaskList *)list;
+ (void)updateListOrder:(NSInteger)newOrder forListID:(NSInteger)listID;

#pragma mark DELETE Methods
+ (void)deleteListWithListID:(NSInteger)listID;
+ (void)deleteAllArchivedLists;

@end
