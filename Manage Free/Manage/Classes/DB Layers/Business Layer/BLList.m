//
//  BLList.m
//  Manage
//
//  Created by Cliff Viegas on 17/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "BLList.h"

// Data Layer
#import "SQLiteAccess.h"

// Data Models
#import "TaskList.h"

// Method Helper
#import "MethodHelper.h"

@implementation BLList

#pragma mark -
#pragma mark SELECT Methods

+ (NSArray *)getAllLists {
	NSString *sql = @"SELECT * FROM List WHERE Archived = 0 ORDER BY ListOrder DESC, ListID DESC";
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

+ (NSArray *)getAllArchivedLists {
	NSString *sql = @"SELECT * FROM List WHERE Archived = 1 ORDER BY ArchivedDateTime DESC";
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

+ (NSInteger)getNextListOrder {
	NSString *sql = @"SELECT ListOrder FROM List WHERE Archived = 0 ORDER BY ListOrder DESC LIMIT 1";
	NSString *largestListOrder = [SQLiteAccess selectOneValueSQL:sql];
	
	NSInteger nextListOrder = 0;
	if (largestListOrder != nil && [largestListOrder isEqual:[NSNull null]] == FALSE) {
		nextListOrder = [largestListOrder integerValue] + 1;
	}
	
	//NSLog(@"%@", sql);
	return nextListOrder;
}


+ (NSInteger)getNumberOfListsWithTitle:(NSString *)theTitle {
	NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM List WHERE Title = %@", 
					 [MethodHelper escapeStringsForSQL:theTitle]];
	//NSLog(@"%@", sql);
	return [[SQLiteAccess selectOneValueSQL:sql] integerValue];
}

#pragma mark -
#pragma mark INSERT Methods

+ (NSInteger)insertNewList:(TaskList *)list {
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO List (Title, CreationDate, Screenshot, ListOrder, Archived, ArchivedDateTime, ToodledoFolderID, CreationDateTime, IsNote, Scribble) " 
												"VALUES (%@, %@, '', %d, %d, %@, %d, %@, %d, %@)",
					 [MethodHelper escapeStringsForSQL:list.title], [MethodHelper escapeStringsForSQL:list.creationDate], list.listOrder, list.archived,
				[MethodHelper escapeStringsForSQL:list.archivedDateTime], list.toodledoFolderID, [MethodHelper escapeStringsForSQL:list.creationDateTime],
                     list.isNote, [MethodHelper escapeStringsForSQL:list.scribble]];
	
	//NSLog(@"%@", sql);
	return [[SQLiteAccess insertWithSQL:sql] integerValue];
}

#pragma mark -
#pragma mark UPDATE Methods

// Updates the specified list in the database
+ (void)updateDatabaseWithList:(TaskList *)list {
	NSString *sql = [NSString stringWithFormat:@"UPDATE List SET Title = %@, CreationDate = %@, Screenshot = %@, ListOrder = %d, "
					 "CompletedDateTime = %@, Completed = %d, Archived = %d, ArchivedDateTime = %@, ToodledoFolderID = %d, "
                     "CreationDateTime = %@, IsNote = %d, Scribble = %@ WHERE ListID = %d", 
					 [MethodHelper escapeStringsForSQL:list.title],
					 [MethodHelper escapeStringsForSQL:list.creationDate],
					 [MethodHelper escapeStringsForSQL:list.screenshot], list.listOrder,
					 [MethodHelper escapeStringsForSQL:list.completedDateTime], list.completed, list.archived,
					 [MethodHelper escapeStringsForSQL:list.archivedDateTime], list.toodledoFolderID,
					 [MethodHelper escapeStringsForSQL:list.creationDateTime], list.isNote,
                     [MethodHelper escapeStringsForSQL:list.scribble],
					 list.listID];
	//NSLog(@"%@", sql);
	[SQLiteAccess updateWithSQL:sql];
}

// Updates the list order for the specified list
+ (void)updateListOrder:(NSInteger)newOrder forListID:(NSInteger)listID {
	NSString *sql = [NSString stringWithFormat:@"UPDATE List SET ListOrder = %d WHERE ListID = %d", newOrder, listID];
	//NSLog(@"%@", sql);
	[SQLiteAccess updateWithSQL:sql];
}

#pragma mark -
#pragma mark DELETE Methods

+ (void)deleteListWithListID:(NSInteger)listID {
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM List WHERE ListID = %d", listID];
	
	//NSLog(@"%@", sql);
	[SQLiteAccess deleteWithSQL:sql];
	// Also need to delete list items
}

+ (void)deleteAllArchivedLists {
	NSString *sql = @"DELETE FROM List WHERE Archived = 1";
	
	//NSLog(@"%@", sql);
	[SQLiteAccess deleteWithSQL:sql];
}

@end
