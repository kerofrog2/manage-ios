//
//  ItemsDueTableViewDelegate.m
//  Manage
//
//  Created by Cliff Viegas on 4/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "ItemsDueTableViewResponder.h"

// Data Models
#import "ListItem.h"

// Data Collections
#import "ItemsDueCollection.h"
#import "TaskListCollection.h"

// Custom Table View Cells
#import "ItemsDueTableViewCell.h"

// Method Helper
#import "MethodHelper.h"

@implementation ItemsDueTableViewResponder

@synthesize delegate;
@synthesize textLabelColour;
@synthesize detailTextLabelColour;
@synthesize listLabelColour;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[itemsDueCollection release];
	//[currentParentListTitle release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<ItemsDueResponderDelegate>)theDelegate andParentListTitle:(NSString *)theTitle 
	   andParentListID:(NSInteger)theParentListID andTaskListCollection:(TaskListCollection *)theTaskListCollection {
	if ((self = [super init])) {
		self.delegate = theDelegate;
		
		self.textLabelColour = [UIColor lightTextColor];
		self.detailTextLabelColour = [UIColor lightGrayColor];
		self.listLabelColour = [UIColor lightGrayColor];
		
		// Assign the passed task list collection
		refTaskListCollection = theTaskListCollection;
		
		currentParentListID = theParentListID;
		
		// Title may be nil, handle it
		if (theTitle == nil) {
			currentParentListTitle = @"";
		} else {
			currentParentListTitle = [theTitle copy];
		}

		itemsDueCollection = [[ItemsDueCollection alloc] initWithListParentTitle:currentParentListTitle
																 andParentListID:currentParentListID
														   andTaskListCollection:refTaskListCollection];
	}
	return self;
}

#pragma mark -
#pragma mark Class Methods

- (void)reloadDataWithListParentTitle:(NSString *)theTitle andTableView:(UITableView *)tableView andParentListID:(NSInteger)theParentListID 
						  andListItem:(ListItem *)listItem {
	currentParentListID = theParentListID;
	
	// Title may be nil, handle it
	if (theTitle == nil) {
		currentParentListTitle = @"";
	} else {
		currentParentListTitle = [theTitle copy];
	}
	
	// Refresh the itemsdueCollection AND the tableview (only do full update if is nil
	if (listItem == nil) {
		[itemsDueCollection updateListItemsWithListParentTitle:currentParentListTitle
											   andParentListID:currentParentListID];
	} else {
		[itemsDueCollection updateListItemsWithListItem:listItem];
	}
	
	
	[tableView reloadData];
}

- (void)reloadTableView:(UITableView *)tableView {
	[tableView reloadData];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    ItemsDueTableViewCell *cell = (ItemsDueTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ItemsDueTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
	
	// Set up the cell...
	ListItem *listItem = [itemsDueCollection.listItems objectAtIndex:indexPath.row];
	
	cell.listNameLabel.textColor = self.listLabelColour;
	
	if (listItem.listID != currentParentListID) {
		// Title stuff
		cell.listNameLabel.text = listItem.parentListTitle;
	} else {
		cell.listNameLabel.text = @"";
	}
	
	cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
	
	cell.textLabel.textColor = self.textLabelColour;
	
	cell.dueDateLabel.textColor = self.detailTextLabelColour;
	//cell.detailTextLabel.textColor = [UIColor redColor];
	
	//cell.dueDateLabel.font = [UIFont fontWithName:@"Helvetica" size:11.0];

	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;

	cell.textLabel.highlightedTextColor = [UIColor darkTextColor];
	cell.dueDateLabel.highlightedTextColor = [UIColor darkTextColor];
	

	
	cell.textLabel.text = [listItem title];
	
	if ([listItem.dueDate length] > 0) {
		NSDate *theDate = [MethodHelper dateFromString:listItem.dueDate usingFormat:K_DATEONLY_FORMAT];
		NSDate *today = [NSDate date];
		
		NSTimeInterval difference = [theDate timeIntervalSinceDate:today];
		NSString *dateToDisplay = @"";
		
		// 86400 seconds in one day
		if (difference < -86400 && [listItem completed] == FALSE) {
			dateToDisplay = @"overdue";
		} else {

			dateToDisplay = [MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterMediumStyle withYear:YES];
		}
		
		cell.dueDateLabel.text = dateToDisplay;
	}
	
    [cell loadScribbleImageForListItem:listItem];
    
	//cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ [%@]", cell.detailTextLabel.text, @"Rosherl's List"];
	
	//cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", cell.detailTextLabel.text, @"Overdue"];
	
    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return [itemsDueCollection.listItems count];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	CGPoint offset = [tableView contentOffset];
	
	// Multiply selected row by 40, get center of tableview
	CGFloat xOrigin = tableView.frame.origin.x + (tableView.frame.size.width - 8);
	CGFloat yOrigin = cell.frame.origin.y + 20 + tableView.frame.origin.y - offset.y;
	CGRect rectPopover = CGRectMake(xOrigin, yOrigin, 1, 1);
	
	// Get the selected list item
	ListItem *listItem = [itemsDueCollection.listItems objectAtIndex:indexPath.row];
	
	// Call the delegate method
	[delegate listItemSelected:listItem atLocation:rectPopover];
	
	// Return nil to select nothing
	return nil;
}


@end
