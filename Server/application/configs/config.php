<?php

/* * ********* Email text when the server send mail successfully ********** */
return array(
    // email server , this email use for register,resetPassword,activation email ws 
    "email" => "managecontact2013@gmail.com",
    "password" => "smarterapp",
    "loginFail" => "Incorrect username or password. Please try again!",
    "checkemail" => "<h4>Please check your email to get instruction.</h4>",
    "invalidCaptcha" => "<strong>Error!</strong> Invalid captcha!",
    "invalidFileType" => "Invalid file type",
    "unsuccessfulUpload" => "Error! Unsuccessful upload.",
    "subjectPasswordRecovery" => "Manage password recovery assistance",
    "subjectNewPassword" => "Manage new password",
    "oneTimeUseUrl" => "<h4>Sorry the link is out of date</h4>",
    "insertsuccessfully" => "Insert successfully",
    "alertResetSuccessfully" => "<div align='left'><h4>Your password has been reset successfully!
        Your new password has been sent to your primary email address.
        Click <a href='http://" . $_SERVER['HTTP_HOST'] . "/manage/admin/index/');'>here</a> to return to the login page</h4>",
    "contentPasswordRecovery" =>
    "
            <h4>Dear [emailUser]</h4>
            
            <p>
                To initiate the password reset process for your account, click the link below:<br/>
                <a href='[activeLink]'>[activeLink]</a>
            </p>
            
            <p>
                If clicking the link above doesn't work, please copy and paste the URL in a
                new browser window instead.
            </p>
    ",
    "contentNewPassword" =>
    "             
            <p>
            Your Email is: [email]<br/>
            Your Password is: [password]<br/>
            </p>
    "
);
/* * ********* End email text ********** */
?>
