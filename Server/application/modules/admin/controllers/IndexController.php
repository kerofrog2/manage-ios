<?php

class IndexController extends Zend_Controller_Action {

    public function init() {

        // The path of the layout folder
        $layoutPath = APPLICATION_PATH . '/layouts/admin/login/';
        $option = array('layout' => 'layoutlogin', 'layoutPath' => $layoutPath);
        // Configure Zend_Layout
        Zend_Layout::startMvc($option);
    }

    //The action run first 
    public function indexAction() {
        //redirect to the login page
        $this->_redirect("admin/index/login");
    }

    /*
     * Logout and redirect to the login page
     */

    public function logoutAction() {

        //Call Zend_Auth to get the email
        $auth = Zend_Auth::getInstance();
        $info = $auth->getIdentity();
        $email = $info->email;

        //write log record to log table
        $this->writeToLog($email, $email . ' have successfully logged out');

        //clear identity and destroy sesion        
        $auth->clearIdentity();
        //redirect to the login page
        $this->_redirect('admin/index/login');
    }

    /*
     * The getEmailInfo function config Zend_Translate and response acontent for the client
     */

    private function getEmailInfo($str) {
        //config Zend_Translate
        $get_msg = new Zend_Translate('array', APPLICATION_PATH . '/configs/config.php', 'en');
        $get_msg->setLocale('en');
        return $get_msg->translate($str);
    }

    public function loginAction() {

        //Zend_Auth checks the login session
        $auth = Zend_Auth::getInstance();
        //if user logged in already, redirect to the food page
        if ($auth->hasIdentity()) {
            $this->_redirect("admin/food");
        }
        //if user is not log in, redirect to the login page
        else {
            //get value from cookie
            $cookieUsername = $this->_request->getCookie('username');
            $cookiePassword = $this->_request->getCookie('password');
            $cookieCheckRemember = $this->_request->getCookie('remember');

            //when the page is processing
            if ($this->_request->isPost()) {

                //get value in textbox
                $u = $this->_request->getPost("username");
                $p = $this->_request->getPost("password");
                $remember = $this->_request->getPost("remember");

                //if the checkbox is checked
                if ($remember == 1) {
                    //cookie expire time is 24 hours
                    setcookie("username", $u, time() + 3600 * 24);
                    setcookie("password", $p, time() + 3600 * 24);
                    setcookie("remember", $remember, time() + 3600 * 24);
                } else {
                    setcookie("username", $u, time() - 3600 * 24);
                    setcookie("password", $p, time() - 3600 * 24);
                    setcookie("remember", $remember, time() - 3600 * 24);
                }
                //zend auth check logged in
                $authAdapter = new Zend_Auth_Adapter_DbTable();
                $authAdapter->setTableName("manage_users")
                        ->setIdentityColumn("username")
                        ->setCredentialcolumn("password");

                //get value in textbox
                $fname = $this->_request->getParam("username");
                $fpass = $this->_request->getParam("password");

                if ($fname && $fpass) {
                    // Set the input credential values
                    $authAdapter->setIdentity($fname)
                            ->setCredential(md5($fpass));
                    // Perform the authentication query, saving the result
                    $result = $auth->authenticate($authAdapter);

                    if ($result->isValid()) {
                        $user = $authAdapter->getResultRowObject(null, array("password"));
                        $auth->getStorage()->write($user);
                        $login_date = date("Y-m-d H:i:s");
                        $info = $auth->getIdentity();
                        $userId = $info->id;

                        // Get IP address
                        $ipaddress = $_SERVER["REMOTE_ADDR"];
                        // Converting the value into binary
                        $bin = decbin(ord($ipaddress));



                        // Updating the manage_users table
                        $mduser = new Admin_Model_Adminuser();
                        $data = array(
                            "lastlogin_date" => $login_date,
                            "lastlogin_ip" => $bin
                        );

                        // Check if update successful mysql
                        if ($mduser->updateUser($userId, $data)) {
                            // Write log record to log table
                            $this->writeToLog($fname, $fname . ' have successfully logged in');
                        }
                        // Back to the index page
                        $this->_redirect("admin/apns");
                    }
                    else
                        $this->view->loginfail = $this->getEmailInfo("loginFail");
                }
                else
                    $this->_redirect("admin/index/login");
            }

            //send value to the view file
            $this->view->coouser = $cookieUsername;
            $this->view->coopass = $cookiePassword;
            $this->view->coocheck = $cookieCheckRemember;
        }
    }

    // Process writing log
    private function writeToLog($email, $message) {
        // Updating manage_log table
        $columnMapping = array(
            'priority' => 'priority',
            'message' => 'message',
            'timestamp' => 'timestamp',
            'email' => 'email'
        );
        $db = Zend_Registry::get('connectDb');
        //define logfile table
        $writer = new Zend_Log_Writer_Db($db, 'manage_log', $columnMapping);
        $logger = new Zend_Log($writer);
        $logger->setEventItem('email', $email);
        //implement to write log 
        $logger->info($message);
    }

    /*
     * Function randoms a string
     */

    private function randomString($length) {
        $str = "";
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str;
    }

    /*
     * The function creates captcha string
     */

    public function captchaAction() {
        // Initiate Zend_Captcha_Image();
        $captcha = new Zend_Captcha_Image();

        //config captcha
        $captcha->setWordLen('6') //set length of string display in image
                ->setTimeout('300') //time destroy session of the captcha
                ->setHeight('80')//height
                ->setWidth('233')//width
                ->setImgDir(APPLICATION_PATH . '/../public/captcha/img')//set path to images directory will be created
                ->setImgUrl(URL_ROOT . 'public/captcha/img')//set URL path to images directory
                ->setFont('public/captcha/fonts/AGENTORANGE.TTF'); //set path to font display in captcha
        //run create
        $captcha->generate();
        $this->view->captcha = $captcha->render($this->view);
        $this->view->captchaID = $captcha->getId();
        $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . $captcha->getId());
        $captchaSession->word = $captcha->getWord();
    }

    /*
     * the action to create new image captcha
     */

    public function refreshAction() {

        $captcha = AuthController::genCaptcha($this);
        $data = array();
        $data["id"] = $captcha->getId();
        $data["url"] = $captcha->url;
        $this->_helper->json($data);
    }

    /*
     * The action helps get the password
     */

    public function forgotAction() {
        // Call the action to create captcha
        $this->captchaAction();
        if ($this->_request->isPost()) {

            //Get an email when users submit the form
            $email = $this->_request->getPost("email");
            $captchaID = $this->_request->captcha_id;
            $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . $captchaID);
            $captchaIterator = $captchaSession->getIterator();
            $captchaWord = $captchaIterator['word'];
            $mdadmin = new Admin_Model_Adminuser();



            //check email in Admin Users table.
            if ($mdadmin->checksEmail($email) == false) {
                $this->view->eml = $email;
                $error = "<strong>Error!</strong>Email not exist!";
                $this->view->error = $error;
            } else {
                //validate captcha
                if ($this->_request->captcha != $captchaWord) {
                    $this->view->eml = $email;
                    $error = $this->getEmailInfo("invalidCaptcha");
                    $this->view->error = $error;
                } else {
                    $getUser = $mdadmin->getUserEmail($email);
                    $id = $getUser["id"];
                    $date = date('Y-m-d H:i:s', time());



                    $activeLink = "http://" . $_SERVER['HTTP_HOST'] . "/manage/admin/index/sendpass/key/" . base64_encode($id . "/" . $date);

                    $email_contents = $this->getEmailInfo("contentPasswordRecovery");
                    $email_contents = str_replace("[emailUser]", $email, $email_contents);
                    $email_contents = str_replace("[activeLink]", $activeLink, $email_contents);

                    //Create SMTP connection Object
                    $configInfo = array('auth' => 'login',
                        'username' => $this->getEmailInfo("email"),
                        'password' => $this->getEmailInfo("password"),
                        'ssl' => 'ssl',
                        'port' => 465
                    );

                    // Config mail server info.
                    $smtpHost = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $configInfo);
                    Zend_Mail::setDefaultTransport($smtpHost);
                    $MailObj = new Zend_Mail('UTF-8');
                    $emailMessage = $email_contents;
                    $fromEmail = $this->getEmailInfo("email");
                    $toEmail = $email;
                    $subject = $this->getEmailInfo("subjectPasswordRecovery");
                    $MailObj->setBodyHtml($emailMessage);
                    $MailObj->setSubject($subject);
                    $MailObj->setFrom($fromEmail);
                    $MailObj->addTo($toEmail);


                    try {
                        // Sending email
                        $MailObj->send($smtpHost);


                        // Updating the modified_date field for using one time use url
                        $data = array(
                            "modified_date" => $date
                        );
                        $mdlUser = new Admin_Model_Adminuser();
                        $mdlUser->updateUser($id, $data);

                        echo $this->getEmailInfo("checkemail");
                        exit;
                    } catch (Zend_Mail_Exception $e) {
                        $e->getMessage();
                    }
                }
            }
        }
    }

    /*
     * The action sends new password
     */

    public function sendpassAction() {


        //Get param key from the url for getting id
        $key = base64_decode($this->_request->getParam('key'));


        $str_key = explode("/", $key);
        $id = $str_key[0];
        $dateInParam = $str_key[1];


        $mdlUser = new Admin_Model_Adminuser();
        $getUser = $mdlUser->getUserID($id);
        $email = $getUser["email"];
        $getDate = $getUser["modified_date"];


        if ($dateInParam == $getDate) {

            // Random a string using new password
            $password_reset = strtolower($this->randomString(6));
            $currentTime = date('Y-m-d H:i:s', time());


            //Get new password and current date.
            $data = array(
                "password" => md5($password_reset),
                "modified_date" => $currentTime
            );


            $email_contents = $this->getEmailInfo("contentNewPassword");
            $email_contents = str_replace("[email]", $email, $email_contents);
            $email_contents = str_replace("[password]", $password_reset, $email_contents);

            //Create SMTP connection Object
            $configInfo = array('auth' => 'login',
                'username' => $this->getEmailInfo("email"),
                'password' => $this->getEmailInfo("password"),
                'ssl' => 'ssl',
                'port' => 465
            );

            //Configure Zend_Mail using SMTP
            $smtpHost = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $configInfo);
            Zend_Mail::setDefaultTransport($smtpHost);
            // Format type is utf-8
            $MailObj = new Zend_Mail('UTF-8');
            // Email body content
            $emailMessage = $email_contents;
            // Sender
            $fromEmail = $this->getEmailInfo("email");
            // Recipient
            $toEmail = $email;
            //Subject
            $subject = $this->getEmailInfo("subjectNewPassword");
            //Formating type
            $MailObj->setBodyHtml($emailMessage);
            $MailObj->setSubject($subject);
            $MailObj->setFrom($fromEmail);
            $MailObj->addTo($toEmail);


            //Implementing send an mail
            try {
                $MailObj->send($smtpHost);

                if ($mdlUser->updateUser($id, $data)) {

                    // Write log record to log table
                    $this->writeToLog($email, $email . ' s password has been changed ');
                    echo $this->getEmailInfo("alertResetSuccessfully");
                }
            } catch (Zend_Mail_Exception $e) {
                $e->getMessage();
            }
        } else {
            echo $this->getEmailInfo("oneTimeUseUrl");
        }
    }
    public function errorAction()
    {
        echo 'Your do not have permission.';
    }

}