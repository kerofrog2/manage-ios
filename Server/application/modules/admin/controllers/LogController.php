<?php

class LogController extends Zend_Controller_Action {

    protected $_arrParam;
    protected $_paginator = array(
        'itemCountPerPage' => 4,
        'pageRange' => 10,
    );
    public function init() {
        $this->view->headTitle('Log Page');
         // Init pagination param
        $this->_arrParam = $this->_request->getParams();
        $this->_paginator['currentPage'] = $this->_request->getParam('page', 1);
        $this->_arrParam['paginator'] = $this->_paginator;
        $this->view->arrParam = $this->_arrParam;

        // The path of the layout folder
        $layoutPath = APPLICATION_PATH . '/layouts/admin/index/';
        $option = array('layout' => 'layout', 'layoutPath' => $layoutPath);
        Zend_Layout::startMvc($option);
        $params = $this->_getAllParams();
        $this->view->param = $params;
    }

    public function indexAction() {
        
        // Handle getting devices
       $tbl = new Admin_Model_Log();
        $this->view->listItem = $tbl->listItem($this->_arrParam, array('task' => 'list'));
        $this->view->numCount = $totalItem = $tbl->listItem($this->_arrParam, array('task' => 'countList'));
        $data = $tbl->getAllLog();
        // Place data into Zend_Paginator
        $adapter = new Zend_Paginator_Adapter_DbSelect($data);
        $paginator = new Zend_Paginator($adapter);
        // Items per page
        $paginator->setItemCountPerPage(4);
        // Page number be displayed
        $paginator->setPageRange(10);
        $currentPage = $this->_request->getParam('page', 1);
        $paginator->setCurrentPageNumber($currentPage);
        // Show to the view
        $this->view->data = $paginator;
        
    }
    public function loadAction()
    {
       $this->_helper->layout->disableLayout();
        // Handle getting devices
       $tbl = new Admin_Model_Log();
        $this->view->listItem = $tbl->listItem($this->_arrParam, array('task' => 'list'));
        $this->view->numCount = $totalItem = $tbl->listItem($this->_arrParam, array('task' => 'countList'));
        $data = $tbl->getAllLog();
        // Place data into Zend_Paginator
        $adapter = new Zend_Paginator_Adapter_DbSelect($data);
        $paginator = new Zend_Paginator($adapter);
        // Items per page
        $paginator->setItemCountPerPage(4);
        // Page number be displayed
        $paginator->setPageRange(10);
        $currentPage = $this->_request->getParam('page', 1);
        $paginator->setCurrentPageNumber($currentPage);
        // Show to the view
        $this->view->data = $paginator;
    }
}
