<?php

class PushController extends Zend_Controller_Action {

    /**
     * Absolute path to your Production Certificate
     *
     * @var string
     * @access private
     */
    private $certificate = '/../public/local/apns/apns-dev-com.kerofrog.dapp-12345678@X.pem';

    /**
     * Apples Production APNS Gateway
     *
     * @var string
     * @access private
     */
    public function init() {
        
    }

    public function indexAction() {
        
    }

    public function addmessageAction() {
         
        $message = array();
        $message['aps'] = array();
        $message['aps']['badge'] = $this->_request->getPost("badge");
        $message['aps']['alert'] = $this->_request->getPost("editor");//'Son wants to play poker';
        $message['aps']['sound'] = $this->_request->getPost("sound");//'default';
        $delivery = $this->_request->getPost("delivery");
        $tmp = date("d/m/Y H:i:s", strtotime($delivery));
        $delivered = date("Y-m-d H:i:s", strtotime($tmp));
        $this->AddMessage(NULL,$delivered,$message);
        $this->_redirect("admin/apns");
    }
    
    public function pushnotificationAction() {
        $mdmessage = new Admin_Model_Messages();
        $result = $mdmessage->fetchMessages();
        foreach ($result AS $value) {
            $pid = $value['pid'];
            $message = $value['message'];
            $token = $value['devicetoken'];
            $certificate = APPLICATION_PATH  . $this->certificate;
            $this->PushMessage($pid, $message, $token, $certificate);
        }
    }
    
    
    /**
     * Start a New Message
     *
     *
     * @param mixed $fk_device Foreign Key, or Array of Foreign Keys to the device you want to send a message to.
     * @param string $delivery Possible future date to send the message.
     * @access public
     */
    private function AddMessage($fk_device = NULL, $delivery = NULL, $message) {
        $listdevices = (is_array($fk_device)) ? $fk_device : array($fk_device);
        $mddevices = new Admin_Model_Devices();
        //Get devices
        $listdevices = $mddevices->GetDevices($listdevices);
        foreach ($listdevices AS $row) {
            $deliver = true;
            // Device id.
            $deviceid = $row['pid'];
            // Get the push settings.
            $pushbadge = $row['pushbadge'];
            $pushalert = $row['pushalert'];
            $pushsound = $row['pushsound'];
            // has user disabled messages?
            if($pushbadge=='disabled' && $pushalert=='disabled' && $pushsound=='disabled')
                    $deliver = false;
            if($deliver===false && count($listdevices) > 0) {
                    $this->_triggerError('This user has disabled all push notifications. Message will not be delivered.');
            }
            else if($deliver===true) {
                    // make temp copy of message so we can cut out stuff this user may not get
                    $usermessage = $message;

                    // only send badge if user will get it
                    if($pushbadge=='disabled'){
                            $this->_triggerError('This user has disabled Push Badge Notifications, Badge will not be delivered.');
                            unset($usermessage['aps']['badge']);
                    }

                    // only send alert if user will get it
                    if($pushalert=='disabled'){
                            $this->_triggerError('This user has disabled Push Alert Notifications, Alert will not be delivered.');
                            unset($usermessage['aps']['alert']);
                    }

                    // only send sound if user will get it
                    if($pushsound=='disabled'){
                            $this->_triggerError('This user has disabled Push Sound Notifications, Sound will not be delivered.');
                            unset($usermessage['aps']['sound']);
                    }

                    if(empty($usermessage['aps'])) {
                            unset($usermessage['aps']);
                    }

                    $fk_device = $deviceid;
                    $mess = $this->_jsonEncode($usermessage);
                    $delivered = (!empty($delivery)) ? "{$delivery}": new Zend_Db_Expr('NOW()');
                    $mdMessage = new Admin_Model_Messages();
                    $mdMessage->queueMessage($fk_device, $mess, $delivered);
                    unset($usermessage);
            }
        }
    }

    

    private function PushMessage($pid, $message, $token, $certificate) {
        $message = json_decode($message,TRUE);
        $alert = $message['aps']['alert'];
        $badge = $message['aps']['badge'];
        $sound = $message['aps']['sound'];
        unset($message);
        $message = new Zend_Mobile_Push_Message_Apns();
        $message->setAlert($alert);
        $message->setBadge($badge);
        $message->setSound($sound);
        $message->setId(time());
        $message->setToken($token);

        $apns = new Zend_Mobile_Push_Apns();
        $apns->setCertificate($certificate);
        // if you have a passphrase on your certificate:
        $apns->setCertificatePassphrase('12345678@X');
        $mdmessage = new Admin_Model_Messages();
        try {
            $apns->connect(Zend_Mobile_Push_Apns::SERVER_SANDBOX_URI);
        } catch (Zend_Mobile_Push_Exception_ServerUnavailable $e) {
            // you can either attempt to reconnect here or try again later
            $mdmessage->pushFailed($pid);
            exit(1);
        } catch (Zend_Mobile_Push_Exception $e) {
            $mdmessage->pushFailed($pid);
            echo 'APNS Connection Error:' . $e->getMessage();
            exit(1);
        }

        try {
            $apns->send($message);
            $mdmessage->pushSuccess($pid);
        } catch (Zend_Mobile_Push_Exception_InvalidToken $e) {
            // you would likely want to remove the token from being sent to again
            $mdmessage->pushFailed($pid);
            echo $e->getMessage();
        } catch (Zend_Mobile_Push_Exception $e) {
            // all other exceptions only require action to be sent
            $mdmessage->pushFailed($pid);
            echo $e->getMessage();
        }
        $apns->close();
    }

    /**
     * Check Setup
     *
     * Check to make sure that the certificates are available and also provide a notice if they are not as secure as they could be.
     *
     * @access private
     */
    private function checkSetup() {
        if (!file_exists($this->certificate))
            $this->_triggerError('Missing Production Certificate.', E_USER_ERROR);
        if (!file_exists($this->sandboxCertificate))
            $this->_triggerError('Missing Sandbox Certificate.', E_USER_ERROR);

        clearstatcache();
        $certificateMod = substr(sprintf('%o', fileperms($this->certificate)), -3);
        $sandboxCertificateMod = substr(sprintf('%o', fileperms($this->sandboxCertificate)), -3);

        if ($certificateMod > 644)
            $this->_triggerError('Production Certificate is insecure! Suggest chmod 644.');
        if ($sandboxCertificateMod > 644)
            $this->_triggerError('Sandbox Certificate is insecure! Suggest chmod 644.');
    }

    /**
     * Register Apple device
     *
     * Using your Delegate file to auto register the device on application launch.  This will happen automatically from the Delegate.m file in your iPhone Application using our code.
     *
     * @param string $appname Application Name
     * @param string $appversion Application Version
     * @param string $devicetoken 64 character unique device token tied to device id
     * @param string $devicename User selected device name
     * @param string $devicemodel Modle of device 'iPhone' or 'iPod'
     * @param string $deviceversion Current version of device
     * @param string $pushbadge Whether Badge Pushing is Enabled or Disabled
     * @param string $pushalert Whether Alert Pushing is Enabled or Disabled
     * @param string $pushsound Whether Sound Pushing is Enabled or Disabled
     * @param string $clientid The clientid of the app for message grouping
     * @access private
     */
    public function registerdeviceAction() {
        if ($this->_request->isGet()) {
            $arrParam = $this->_request->getParams();
            if (strlen($arrParam['appname']) == 0)
                $this->_triggerError('Application Name must not be blank.', E_USER_ERROR);
            else if (strlen($arrParam['appversion']) == 0)
                $this->_triggerError('Application Version must not be blank.', E_USER_ERROR);
            else if (strlen($arrParam['devicetoken']) != 64)
                $this->_triggerError('Device Token must be 64 characters in length.', E_USER_ERROR);
            else if (strlen($arrParam['devicename']) == 0)
                $this->_triggerError('Device Name must not be blank.', E_USER_ERROR);
            else if (strlen($arrParam['devicemodel']) == 0)
                $this->_triggerError('Device Model must not be blank.', E_USER_ERROR);
            else if (strlen($arrParam['deviceversion']) == 0)
                $this->_triggerError('Device Version must not be blank.', E_USER_ERROR);
            else if ($arrParam['pushbadge'] != 'disabled' && $arrParam['pushbadge'] != 'enabled')
                $this->_triggerError('Push Badge must be either Enabled or Disabled.', E_USER_ERROR);
            else if ($arrParam['pushalert'] != 'disabled' && $arrParam['pushalert'] != 'enabled')
                $this->_triggerError('Push Alert must be either Enabled or Disabled.', E_USER_ERROR);
            else if ($arrParam['pushsound'] != 'disabled' && $arrParam['pushsound'] != 'enabled')
                $this->_triggerError('Push Sount must be either Enabled or Disabled.', E_USER_ERROR);
            $arrParam = array_diff($arrParam, array($arrParam['action'], $arrParam['controller'], $arrParam['module']));
            $insertData = $arrParam;
            $date = new Zend_Db_Expr('NOW()');
            
            $updateData = array_diff($arrParam, array($arrParam['appname'], $arrParam['devicetoken']));
            $insertData = array_merge($insertData, array('development' => 'production', 'status' => 'active', 'created' => $date, 'modified' => $date));
            $updateData = array_merge($updateData, array('status' => 'active', 'modified' => $date));
            $mddevice = new Admin_Model_Devices();
            $result = $mddevice->registerDevice($insertData, $updateData);
            echo 1;
            return;
        }
        else {
            $arrParam = $this->_request->getParams();
            $arrParam = array_diff($arrParam, array($arrParam['action'], $arrParam['controller'], $arrParam['module']));
            var_dump($arrParam);
            echo 'no param';
        }
    }

    /**
     * Trigger Error
     *
     * Use PHP error handling to trigger User Errors or Notices.  If logging is enabled, errors will be written to the log as well.
     * Disable on screen errors by setting showErrors to false;
     * case E_USER_WARNING:
     * case E_USER_NOTICE:
     * case E_USER_ERROR:
     * @param string $error Error String
     * @param int $type Type of Error to Trigger
     * @access private
     */
    private function _triggerError($error, $type = E_USER_NOTICE) {
        $columnMapping = array(
            'priority' => 'priority',
            'message' => 'message',
            'timestamp' => 'timestamp'
        );
        $backtrace = debug_backtrace();
        $backtrace = array_reverse($backtrace);
        $error .= "\n";
        $i = 1;

        foreach ($backtrace as $errorcode) {
            $file = ($errorcode['file'] != '') ? "-> File: " . basename($errorcode['file']) . " (line " . $errorcode['line'] . ")" : "";
            $error .= "\n\t" . $i . ") " . $errorcode['class'] . "::" . $errorcode['function'] . " {$file}";
            $i++;
        }
        $error .= "\n\n";
//        $db = Zend_Registry::get('connectDb');
//        //define logfile table
//        $writer = new Zend_Log_Writer_Db($db, 'manage_log', $columnMapping);
//        $logger = new Zend_Log($writer);

        // Use this in your model, view and controller files
        if ($type == E_USER_NOTICE)
            $logger->log($error, Zend_Log::NOTICE);
        else if ($type == E_USER_WARNING)
            $logger->log($error, Zend_Log::WARN);
        else
            $logger->log($error, Zend_Log::ERR);
        trigger_error($error, $type);
    }

    /**
     * JSON Encode
     *
     * Some servers do not have json_encode, so use this instead.
     *
     * @param array $array Data to convert to JSON string.
     * @access private
     * @return string
     */
    private function _jsonEncode($array = false) {
        //Using json_encode if exists
        if (function_exists('json_encode')) {
            return json_encode($array);
        }
        if (is_null($array))
            return 'null';
        if ($array === false)
            return 'false';
        if ($array === true)
            return 'true';
        if (is_scalar($array)) {
            if (is_float($array)) {
                return floatval(str_replace(",", ".", strval($array)));
            }
            if (is_string($array)) {
                static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
                return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $array) . '"';
            }
            else
                return $array;
        }
        $isList = true;
        for ($i = 0, reset($array); $i < count($array); $i++, next($array)) {
            if (key($array) !== $i) {
                $isList = false;
                break;
            }
        }
        $result = array();
        if ($isList) {
            foreach ($array as $v)
                $result[] = $this->_jsonEncode($v);
            return '[' . join(',', $result) . ']';
        } else {
            foreach ($array as $k => $v)
                $result[] = $this->_jsonEncode($k) . ':' . $this->_jsonEncode($v);
            return '{' . join(',', $result) . '}';
        }
    }

}
