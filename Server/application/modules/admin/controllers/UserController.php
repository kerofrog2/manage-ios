<?php

class UserController extends Zend_Controller_Action {

    protected $_arrParam;
    protected $_paginator = array(
        'itemCountPerPage' => 5,
        'pageRange' => 10,
    );

    public function init() {
        

        // The path of the layout folder
        $layoutPath = APPLICATION_PATH . '/layouts/admin/index/';
        $option = array('layout' => 'layout', 'layoutPath' => $layoutPath);
        // Configure Zend_Layout
        Zend_Layout::startMvc($option);
        $params = $this->_getAllParams();
        $this->view->param = $params;
    }

    /*
     * The action show data of Food table
     */

    public function indexAction() {
        // Check if a users has already logged in
        if (!$this->checkLogin()) {

            // Init pagination param
            $this->_arrParam = $this->_request->getParams();
            $this->_paginator['currentPage'] = $this->_request->getParam('page', 1);
            $this->_arrParam['paginator'] = $this->_paginator;
            $this->view->arrParam = $this->_arrParam;

            $tbl = new Admin_Model_Adminuser;
            $this->view->listItem = $tbl->listItem($this->_arrParam, array('task' => 'list'));
            $this->view->numCount = $totalItem = $tbl->listItem($this->_arrParam, array('task' => 'countList'));
            // Get all record in Food table
            $data = $tbl->getAllUsers();
            // Place data into Zend_Paginator
            $adapter = new Zend_Paginator_Adapter_DbSelect($data);
            $paginator = new Zend_Paginator($adapter);
            // Items per page
            $paginator->setItemCountPerPage(5);
            // Page number be displayed
            $paginator->setPageRange(10);
            $currentPage = $this->_request->getParam('page', 1);
            $paginator->setCurrentPageNumber($currentPage);
            // Show to the view
            $this->view->data = $paginator;
        }
        else
            $this->_redirect("admin/index");
    }

    /*
     * The action fills data into Food table
     */

    public function loadAction() {

        // Init pagination param
        $this->_arrParam = $this->_request->getParams();
        $this->_paginator['currentPage'] = $this->_request->getParam('page', 1);
        $this->_arrParam['paginator'] = $this->_paginator;
        $this->view->arrParam = $this->_arrParam;

        // Disable because layout not needed
        $this->_helper->layout->disableLayout();
        // The function get pagination data
        $tbl = new Admin_Model_Adminuser;
        $this->view->listItem = $tbl->listItem($this->_arrParam, array('task' => 'list'));
        $this->view->numCount = $totalItem = $tbl->listItem($this->_arrParam, array('task' => 'countList'));
        // Get all record in Food table
        $data = $tbl->getAllUsers();
        // Place data into Zend_Paginator
        $adapter = new Zend_Paginator_Adapter_DbSelect($data);
        $paginator = new Zend_Paginator($adapter);
        // Items per page
        $paginator->setItemCountPerPage(5);
        // Page number be displayed
        $paginator->setPageRange(10);
        $currentPage = $this->_request->getParam('page', 1);
        $paginator->setCurrentPageNumber($currentPage);
        // Show to the view
        $this->view->data = $paginator;
    }

    private function checkLogin() {
        // Check users login before view data
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            if ($this->_request->getActionName() != 'login') {
                $this->_redirect('/admin/index');
            }
        }
    }

    public function deleteAction() {

        // Check user login or not
        if (!$this->checkLogin()) {

            // Call Zend_Auth to get the email
            $auth = Zend_Auth::getInstance();
            $info = $auth->getIdentity();
            $email = $info->email;
            $id = $this->_request->getParam("id");

            $mdluser = new Admin_Model_Adminuser();
            $getName = $mdluser->getUserID($id);
            
            if ($mdluser->deleteUser($id)) {
                
                // Write log record to log table
                $this->writeToLog($email, $getName['email'] . ' has already been deleted');

                // Get dir to unlink
                $getUser = $mdluser->getUserID($id);
                $dir = $getUser["photo_id"];
                // Ensure dir path ends with a slash
                $dirPath = "public/files/imageusers/" . $dir . "/";
                // Delete directory
                system("rm -rf ".escapeshellarg($dirPath));
            }
        }
        else
            $this->_redirect("admin/index");
    }

    /*
     * The function create captcha
     */

    public function captchaAction() {
        // Initiate Zend_Captcha_Image();
        $captcha = new Zend_Captcha_Image();

        //config captcha
        $captcha->setWordLen('6') //set length of string display in image
                ->setTimeout('300') //time destroy session of the captcha
                ->setHeight('80')//height
                ->setWidth('233')//width
                ->setImgDir(APPLICATION_PATH . '/../public/captcha/img')//set path to images directory will be created
                ->setImgUrl(URL_ROOT . 'public/captcha/img')//set URL path to images directory
                ->setFont('public/captcha/fonts/AGENTORANGE.TTF'); //set path to font display in captcha
        //run create
        $captcha->generate();
        $this->view->captcha = $captcha->render($this->view);
        $this->view->captchaID = $captcha->getId();
        $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . $captcha->getId());
        $captchaSession->word = $captcha->getWord();
    }

    /*
     * The function checks email format
     */

    private function checkValidEmail($email) {
        if (strlen($email) == 0)
            return FALSE;
        if (preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email))
            return TRUE;
        return FALSE;
    }

    /*
     * the action to create new image captcha
     */

    public function refreshAction() {

        $captcha = AuthController::genCaptcha($this);
        $data = array();
        $data["id"] = $captcha->getId();
        $data["url"] = $captcha->url;
        $this->_helper->json($data);
    }

    /*
     * The getEmailInfo function config Zend_Translate and response acontent for the client
     */

    private function getEmailInfo($str) {
        //config Zend_Translate
        $get_msg = new Zend_Translate('array', APPLICATION_PATH . '/configs/config.php', 'en');
        $get_msg->setLocale('en');
        return $get_msg->translate($str);
    }

    /*
     * The action helps get the password
     */

    public function insertAction() {


        // Check if a users has already logged in
        if (!$this->checkLogin()) {

            // Call the action to create captcha
            $this->captchaAction();

            // when page is processed
            if ($this->_request->isPost()) {

                //Get an email captcha when page is processed
                $captchaID = $this->_request->captcha_id;
                $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . $captchaID);
                $captchaIterator = $captchaSession->getIterator();
                $captchaWord = $captchaIterator['word'];

                //validate captcha
                if ($this->_request->captcha != $captchaWord) {
                    $this->view->eml = $this->_request->getPost("email");
                    $this->view->displayname = $this->_request->getPost("displayname");
                    $this->view->username = $this->_request->getPost("username");
                    $this->view->password = $this->_request->getPost("password");
                    $error = $this->getEmailInfo("invalidCaptcha");
                    $this->view->error = $error;
                } else {

                    // Name for image
                    $dir = date("dmYhis");

                    //$imgDir = (int)$dir;
                    $imgDir = floatval($dir);


                    // Store image in folder
                    mkdir("./public/files/imageusers/$dir", 0755);

                    //Config using Zend_File_Transfer
                    $file = new Zend_File_Transfer_Adapter_Http;


                    // Imagepath in database 
                    $path = "./public/files/imageusers/" . $dir;
                    $file->setDestination($path);
                    $file->setValidators(array("size" => array("min" => "20", "max" => "600000")))
                            ->addValidator('Extension', false, array("jpg", "jpeg", "png", "gif"));


                    // Getting information of the picture
                    $info = $file->getFileInfo();
                    $picture = $info['picture']['name'];


                    $getValueEmail = $this->_request->getPost("email");
                    // get IP address
                    $ipaddress = $_SERVER["REMOTE_ADDR"];
                    $bin = decbin(ord($ipaddress));
                    // To update fields in creation_date
                    $today = date("Y-m-d H:i:s");



                    // Getting field values
                    $data = array("email" => $getValueEmail,
                        "displayname" => $this->_request->getPost("displayname"),
                        "photo_id" => $dir,
                        "username" => $this->_request->getPost("username"),
                        "password" => md5($this->_request->getPost("password")),
                        "creation_date" => $today,
                        "creation_ip" => $bin,
                        "modified_date" => $today,
                        "lastlogin_date" => $today,
                        "lastlogin_ip" => $bin,
                        "level" => $this->_request->getPost("level"),
                        "status" => $this->_request->getPost("status")
                    );


                    // Call Adminuser Model updates data
                    $mdlAdminuser = new Admin_Model_Adminuser;

                    if ($file->receive($picture)) {
                        $mdlAdminuser->insertUser($data);
                        $this->RenamePicture($dir, $path, $picture);
                        $this->_redirect("admin/user");
                    }

                    if ($picture == "") {
                        $mdlAdminuser->insertUser($data);
                        $this->_redirect("admin/user");
                    }
                }
            }
        }
        else
            $this->_redirect("admin/index");
    }

    /*
     * function rename the picture before saving
     */

    public function RenamePicture($today, $path, $picture) {
        $time1 = $today . "." . jpg;
        $old = $path . "/" . $picture;
        $new = $path . "/" . $time1;
        rename($old, $new);
        unlink($old);
    }

    /*
     * The function will update the user's information
     */

    public function updateAction() {

        // Check if a users has already logged in
        if (!$this->checkLogin()) {

            // Call the action to create captcha
            $this->captchaAction();


            $getId = $this->_request->getParam("id");
            $mdlUser = new Admin_Model_Adminuser();
            $getUser = $mdlUser->getUserID($getId);
            $photo_id = $getUser["photo_id"];

            if ($this->_request->isPost()) {

                //Get an email captcha when page is processed
                $captchaID = $this->_request->captcha_id;
                $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . $captchaID);
                $captchaIterator = $captchaSession->getIterator();
                $captchaWord = $captchaIterator['word'];

                //validate captcha
                if ($this->_request->captcha != $captchaWord) {
                    $this->view->eml = $this->_request->getPost("email");
                    $this->view->displayname = $this->_request->getPost("displayname");
                    $this->view->username = $this->_request->getPost("username");
                    $this->view->photo = $photo_id;
                    $this->view->password = $this->_request->getPost("password");
                    $this->view->level = $this->_request->getPost("level");
                    $this->view->status = $this->_request->getPost("status");
                    $error = $this->getEmailInfo("invalidCaptcha");
                    $this->view->error = $error;
                } else {

                    $email = $this->_request->getPost("email");
                    $displayname = $this->_request->getPost("displayname");
                    $username = $this->_request->getPost("username");
                    $password = $getUser["password"];
                    $getPassword = $this->_request->getPost("password");
                    if ($password == $getPassword) {
                        $usePassword = $password;
                    }
                    else
                        $usePassword = md5($getPassword);
                    $level = $this->_request->getPost("level");
                    $status = $this->_request->getPost("status");
                    $getTime = date("Y-m-d H:i:s");


                    $data = array(
                        "email" => $email,
                        "displayname" => $displayname,
                        "username" => $username,
                        "password" => $usePassword,
                        "modified_date" => $getTime,
                        "level" => $level,
                        "status" => $status,
                    );

                    // Configure Zend_File_Transfer
                    $file = new Zend_File_Transfer_Adapter_Http;
                    $dir = $getUser["photo_id"];
                    $file->setDestination("./public/files/imageusers/" . $dir);
                    $file->setValidators(array(
                                "size" => array(
                                    "min" => "20",
                                    "max" => "500000")))
                            ->addValidator('Extension', false, array("jpg", "jpeg", "png", "gif"));

                    // Get file info of picture and icon
                    $info = $file->getFileInfo();
                    $picture = $info['picture']['name'];

                    if ($picture != "") {
                        if ($file->receive($picture)) {

                            $path = "./public/files/imageusers/" . $dir;
                            // Exclude Hidden Files from Scandir PHP
                            $scan = preg_grep('/^([^.])/', scandir($path));
                            //print_r($scan[3]);exit;

                            if ($scan[3]) {
                                // Define the path to the old image
                                $previous = "public/files/imageusers/" . $dir . "/" . $scan[2];
                                // Delete it
                                unlink($previous);
                                // Rename new image
                                $curr = "./public/files/imageusers/" . $dir . "/" . $scan[3];
                                $rmk = "./public/files/imageusers/" . $dir . "/$dir.jpg";
                                rename($curr, $rmk);
                            } else {
                                $curr = "./public/files/imageusers/" . $dir . "/" . $scan[2];
                                $rmk = "./public/files/imageusers/" . $dir . "/$dir.jpg";
                                rename($curr, $rmk);
                            }

                            $mdlUser->updateUser($getId, $data);
                            $this->_redirect("/admin/user/");
                        }
                    }
                    if ($picture == "") {
                        $mdlUser->updateUser($getId, $data);
                        $this->_redirect("/admin/user/");
                    }
                }
            } else {
                $this->view->getUser = $getUser;
            }
        }
        else
            $this->_redirect("admin/index");
    }

    // Process writing log
    private function writeToLog($email, $message) {
        // Update data
        $columnMapping = array(
            'priority' => 'priority',
            'message' => 'message',
            'timestamp' => 'timestamp',
            'email' => 'email'
        );
        $db = Zend_Registry::get('connectDb');
        // Define logfile table
        $writer = new Zend_Log_Writer_Db($db, 'log', $columnMapping);
        $logger = new Zend_Log($writer);
        $logger->setEventItem('email', $email);
        // Implement to write log 
        $logger->info($message);
    }

}
