<?php

class DevicesController extends Zend_Controller_Action {

    protected $_arrParam;
    protected $_paginator = array(
        'itemCountPerPage' => 4,
        'pageRange' => 10,
    );
    public function init() {
        $this->view->headTitle('Devices Page');
        // Init pagination param
        $this->_arrParam = $this->_request->getParams();
        $this->_paginator['currentPage'] = $this->_request->getParam('page', 1);
        $this->_arrParam['paginator'] = $this->_paginator;
        $this->view->arrParam = $this->_arrParam;

        // The path of the layout folder
        $layoutPath = APPLICATION_PATH . '/layouts/admin/index/';
        $option = array('layout' => 'layout', 'layoutPath' => $layoutPath);
        Zend_Layout::startMvc($option);
        $params = $this->_getAllParams();
        $this->view->param = $params;
    }

    public function indexAction() {
        // Handle getting devices
       $tbl = new Admin_Model_Devices();
        $this->view->listItem = $tbl->listItem($this->_arrParam, array('task' => 'list'));
        $this->view->numCount = $totalItem = $tbl->listItem($this->_arrParam, array('task' => 'countList'));
        $data = $tbl->getAllDevices();
        // Place data into Zend_Paginator
        $adapter = new Zend_Paginator_Adapter_DbSelect($data);
        $paginator = new Zend_Paginator($adapter);
        // Items per page
        $paginator->setItemCountPerPage(4);
        // Page number be displayed
        $paginator->setPageRange(10);
        $currentPage = $this->_request->getParam('page', 1);
        $paginator->setCurrentPageNumber($currentPage);
        // Show to the view
        $this->view->data = $paginator;
        
    // Handle getting devices history
       $tbl = new Admin_Model_DevicesHistory();
        $this->view->listItemHistory = $tbl->listItemHistory($this->_arrParam, array('task' => 'list'));
        $this->view->numCountHistory = $totalItem = $tbl->listItemHistory($this->_arrParam, array('task' => 'countList'));
        $dataHistory = $tbl->getAllDevicesHistory();
        // Place data into Zend_Paginator
        $adapterHistory = new Zend_Paginator_Adapter_DbSelect($dataHistory);
        $paginatorHistory = new Zend_Paginator($adapterHistory);
        // Items per page
        $paginatorHistory->setItemCountPerPage(4);
        // Page number be displayed
        $paginatorHistory->setPageRange(10);
        $currentPage = $this->_request->getParam('page', 1);
        $paginatorHistory->setCurrentPageNumber($currentPage);
        // Show to the view
        $this->view->data = $paginatorHistory;
        
        // Handle getting all messages
        $tbl = new Admin_Model_Messages();
        $this->view->listItemMessages = $tbl->listItemMessages($this->_arrParam, array('task' => 'list'));
        $this->view->numCountMessages = $totalItem = $tbl->listItemMessages($this->_arrParam, array('task' => 'countList'));
        $dataMessage = $tbl->getAllMessages();
        // Place data into Zend_Paginator
        $adapterMessages = new Zend_Paginator_Adapter_DbSelect($dataMessage);
        $paginatorMessages = new Zend_Paginator($adapterMessages);
        // Items per page
        $paginatorMessages->setItemCountPerPage(4);
        // Page number be displayed
        $paginatorMessages->setPageRange(10);
        $currentPage = $this->_request->getParam('page', 1);
        $paginatorMessages->setCurrentPageNumber($currentPage);
        // Show to the view
        $this->view->data = $paginatorMessages;
        
        
    }
    public function loadAction()
    {
        $this->_helper->layout->disableLayout();
        $tbl = new Admin_Model_Devices();
        
        $this->view->listItem = $tbl->listItem($this->_arrParam, array('task' => 'list'));
        $this->view->numCount = $totalItem = $tbl->listItem($this->_arrParam, array('task' => 'countList'));
        $data = $tbl->getAllDevices();
        // Place data into Zend_Paginator
        $adapter = new Zend_Paginator_Adapter_DbSelect($data);
        $paginator = new Zend_Paginator($adapter);
        // Items per page
        $paginator->setItemCountPerPage(4);
        // Page number be displayed
        $paginator->setPageRange(10);
        $currentPage = $this->_request->getParam('page', 1);
        $paginator->setCurrentPageNumber($currentPage);
        // Show to the view
        $this->view->data = $paginator;
        
    }
    public function loadhistoryAction()
    {
        $this->_helper->layout->disableLayout();
        $tbl = new Admin_Model_DevicesHistory();
        $this->view->listItemHistory = $tbl->listItemHistory($this->_arrParam, array('task' => 'list'));
        $this->view->numCountHistory = $totalItem = $tbl->listItemHistory($this->_arrParam, array('task' => 'countList'));
        $dataHistory = $tbl->getAllDevicesHistory();
        // Place data into Zend_Paginator
        $adapterHistory = new Zend_Paginator_Adapter_DbSelect($dataHistory);
        $paginatorHistory = new Zend_Paginator($adapterHistory);
        // Items per page
        $paginatorHistory->setItemCountPerPage(4);
        // Page number be displayed
        $paginatorHistory->setPageRange(10);
        $currentPage = $this->_request->getParam('page', 1);
        $paginatorHistory->setCurrentPageNumber($currentPage);
        // Show to the view
        $this->view->data = $paginatorHistory;
        
    }
    public function loadmessagesAction()
    {
        $this->_helper->layout->disableLayout();
        // Handle getting all messages
        $tbl = new Admin_Model_Messages();
        $this->view->listItemMessages = $tbl->listItemMessages($this->_arrParam, array('task' => 'list'));
        $this->view->numCountMessages = $totalItem = $tbl->listItemMessages($this->_arrParam, array('task' => 'countList'));
        $dataMessage = $tbl->getAllMessages();
        // Place data into Zend_Paginator
        $adapterMessages = new Zend_Paginator_Adapter_DbSelect($dataMessage);
        $paginatorMessages = new Zend_Paginator($adapterMessages);
        // Items per page
        $paginatorMessages->setItemCountPerPage(4);
        // Page number be displayed
        $paginatorMessages->setPageRange(10);
        $currentPage = $this->_request->getParam('page', 1);
        $paginatorMessages->setCurrentPageNumber($currentPage);
        // Show to the view
        $this->view->data = $paginatorMessages;
    }
    public function activeAction()
    {
        // Get id of devices history
	$id = $this->_request->getParam("id");
        $data_active = array(
		"status"=>"active"
		);
        $data_uninstalled = array(
		"status"=>"uninstalled"
		);
         
        $mdDevicesHistory = new Admin_Model_DevicesHistory();
        $result = $mdDevicesHistory->getDevicesHistoryById($id);
        $status = $result['status'];
        
	$msg_active = '';
        if($status == "active")
        {
            $mdDevicesHistory->updateDevicesHistory($data_uninstalled, $id);
            $msg_active = '<span>uninstalled</span>';
        }
        else
        {
            
            $mdDevicesHistory->updateDevicesHistory($data_active, $id);
            $msg_active = '<span>active</span>';
        }
        
        $arr_json = array(
			'e' => 0,
			'data' => $msg_active
		);
	echo json_encode($arr_json);
	exit;
    }



}
