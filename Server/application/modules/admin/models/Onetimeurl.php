<?php

/**
 * Project
 *
 * @category   Model
 * @package    ADMIN
 * @copyright  Copyright 2012-2013 Cliff Viegas Developments
 * @license    V1
 * @version    Onetimeurl.php 2013-01-29 11:30:12
 * @author     Cliff Viegas
 */
class Admin_Model_Onetimeurl extends Zend_Db_Table_Abstract {

    protected $_name = "onetimeurl";
    protected $_primary = "id";


    public function insertString($data) {
        $kq = $this->insert($data);
        if ($kq)
            return true;
        else
            return false;
    }

    
   
    public function checksString($string) {
        $se = $this->select();
        $se->where("string = ?", $string);
        $kq = $this->fetchRow($se);
        if ($kq)
            return true;
        else
            return false;
    }    
}

?>
