<?php

/**
 * Project
 *
 * @category   Model
 * @package    ADMIN
 * @copyright  Copyright 2012-2013 Cliff Viegas Developments
 * @license    V1
 * @version    Devices.php 2013-01-29 11:30:12
 * @author     Cliff Viegas
 */
class Admin_Model_Log extends Zend_Db_Table_Abstract {

    protected $_name = "manage_log";
    protected $_primary = "id";

    public function getAllLog() {
        $query = $this->select();
        $query->from($this->_name, array('id'));
        $query->order("id DESC ");
        return $query;
    }
    public function listItem($arrParam = null, $options = null) {
        $db = $this->getAdapter();

        if ($options['task'] == 'list') {
            $paginator = $arrParam['paginator'];

            $select = $db->select()
                    ->from('manage_log', array('id', 'email', 'priority', 'message', 'timestamp'))
                    ->order("id ASC ");

            if ($paginator['itemCountPerPage'] > 0) {
                $page = $paginator['currentPage'];
                $rowCount = $paginator['itemCountPerPage'];
                $select->limitPage($page, $rowCount);
            }
            $result = $db->fetchAll($select);
        }
        if ($options['task'] == 'countList') {
            $select = $db->select()
                    ->from('manage_log', array('COUNT(id) AS totalItem'));
            $result = count($db->fetchOne($select));
        }
        return $result;
    }
}

?>
