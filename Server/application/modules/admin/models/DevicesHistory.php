<?php

/**
 * Project
 *
 * @category   Model
 * @package    ADMIN
 * @copyright  Copyright 2012-2013 Cliff Viegas Developments
 * @license    V1
 * @version    Devices.php 2013-01-29 11:30:12
 * @author     Cliff Viegas
 */
class Admin_Model_DevicesHistory extends Zend_Db_Table_Abstract {

    protected $_name = "manage_apns_device_history";
    protected $_primary = "pid";

    public function getAllDevicesHistory() {
        $query = $this->select();
        $query->from($this->_name, array('pid'));
        $query->order("appname DESC ");
        return $query;
    }
    public function listItemHistory($arrParam = null, $options = null) {
        $db = $this->getAdapter();

        if ($options['task'] == 'list') {
            $paginator = $arrParam['paginator'];

            $select = $db->select()
                    ->from('manage_apns_device_history', array('pid', 'appname', 'appversion', 'deviceuid', 'devicetoken', 'devicename','status'))
                    ->order("appname ASC ");

            if ($paginator['itemCountPerPage'] > 0) {
                $page = $paginator['currentPage'];
                $rowCount = $paginator['itemCountPerPage'];
                $select->limitPage($page, $rowCount);
            }
            $result = $db->fetchAll($select);
        }
        if ($options['task'] == 'countList') {
            $select = $db->select()
                    ->from('manage_apns_device_history', array('COUNT(pid) AS totalItem'));
            $result = count($db->fetchOne($select));
        }
        return $result;
    }
    public function getDevicesHistoryById($id){
		$query=$this->select();
                $query->where("pid = ?",$id);
		return $this->fetchRow($query);
	}
        
   public function updateDevicesHistory($data, $id) {
        if ($this->update($data, "pid = $id"))
            return true;
        else
            return false;
    }
}

?>
