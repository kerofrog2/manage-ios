<?php

/**
 * Project
 *
 * @category   Model
 * @package    ADMIN
 * @copyright  Copyright 2012-2013 Cliff Viegas Developments
 * @license    V1
 * @version    Foods.php 2013-01-29 11:30:12
 * @author     Cliff Viegas
 */
class Admin_Model_Devices extends Zend_Db_Table_Abstract {

    protected $_name = "manage_apns_devices";
    protected $_primary = "pid";
    /**
	 * Execute an INSERT ON DUPLICATE KEY statement
	 * @param array $insertData Data to use for INSERT portion
	 * @param array $updateData Data to use for UPDATE portion
	 * @return Zend_Db_Statement
	 */
	public function registerDevice($insertData, $updateData) {
		// generate INSERT part statement
		$insertCols = '';
		$insertVals = '';
		foreach($insertData as $column => $value) {
			$insertCols .= $this->getAdapter()->quoteIdentifier($column) . ',';
			$insertVals .=	(get_class($value) == 'Zend_Db_Expr')
						? $value->__toString()
						: $this->getAdapter()->quoteInto('?', $value);
			$insertVals .= ',';
		}
		$insertCols = rtrim($insertCols, ',');
		$insertVals = rtrim($insertVals, ',');
		//echo $insertCols;
		//echo $insertVals;
                //exit;
		// generate UPDATE part of statement
		$updateExpr = '';
		foreach($updateData as $column => $value) {
			$updateExpr .=	$this->getAdapter()->quoteIdentifier($column) . ' = ';
			$updateExpr .=	(get_class($value) == 'Zend_Db_Expr')
						? $value->__toString()
						: $this->getAdapter()->quoteInto('?', $value);
			$updateExpr .= ',';
		}
		$updateExpr = rtrim($updateExpr, ',');
		
		// generate statement
		$sql = 'INSERT INTO ' . $this->_name . ' (' . $insertCols . ') VALUES (' . $insertVals . ') ';
		$sql .= 'ON DUPLICATE KEY UPDATE ' . $updateExpr . ';';
		$stmt = $this->_db->query($sql);
//                echo $sql;
//                exit(1);
                return $stmt->execute();
	}
        /**
	 * Unregister Apple device
	 *
	 * This gets called automatically when Apple's Feedback Service responds with an invalid token.
	 *
	 * @param string $token 64 character unique device token tied to device id
	 * @access private
	 */
	public function unregisterDevice($token){
            $data = array(
                'status'      => 'uninstalled'
            );
            $where = $this->getAdapter()->quoteInto('devicetoken = ?', $token);
            return $this->update($data, $where)->limit(1, 0);
        }
        /**
	 * Start get Devices
	 *
	 * @param mixed $fk_device Foreign Key, or Array of Foreign Keys to the device you want to send a message to.
	 * @access public
	 */
	public function GetDevices($fk_device=NULL){
		// If no device is specified then that means we sending a message to all.
                $select = $this->select();
                $select->from($this->_name, array('pid','pushbadge','pushalert','pushsound'));
                if (is_null($fk_device))
                    $select->where("status IN (". implode(', ', $fk_device) .")");
                $select->where("status = ?", 'active');
                $fk_device = $this->fetchAll($select)->toArray();
                return $fk_device;
                
  	}
       
}

?>
