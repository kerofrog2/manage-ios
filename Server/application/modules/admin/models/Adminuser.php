<?php

/**
 * Project
 *
 * @category   Model
 * @package    ADMIN
 * @copyright  Copyright 2012-2013 Cliff Viegas Developments
 * @license    V1
 * @version    Adminuser.php 2013-01-29 11:30:12
 * @author     Cliff Viegas
 */
class Admin_Model_Adminuser extends Zend_Db_Table_Abstract {

    protected $_name = "manage_users";
    protected $_primary = "id";

    /*
     * The function update Adminuser table
     */

    public function updateUser($id, $data) {
        if ($this->update($data, "id = $id"))
            return true;
        else
            return false;
    }

    /*
     * The function get pagination data
     */

    public function listItem($arrParam = null, $options = null) {
        $db = $this->getAdapter();

        if ($options['task'] == 'list') {
            $paginator = $arrParam['paginator'];

            $select = $db->select()
                    ->from('manage_users', array('id', 'email', 'displayname', 'photo_id', 'username',
                                                'creation_date','creation_ip','modified_date','lastlogin_date',
                                                'lastlogin_ip','level','status'
                                        ))
                    ->order("username DESC ");

            if ($paginator['itemCountPerPage'] > 0) {
                $page = $paginator['currentPage'];
                $rowCount = $paginator['itemCountPerPage'];
                $select->limitPage($page, $rowCount);
            }
            $result = $db->fetchAll($select);
        }
        if ($options['task'] == 'countList') {
            $select = $db->select()
                    ->from('manage_users', array('COUNT(id) AS totalItem'));
            $result = count($db->fetchOne($select));
        }
        return $result;
    }
    /*
     * The function gets all record
     */

    public function getAllUsers() {
        $query = $this->select();
        $query->from($this->_name, array('id'));
        $query->order("username DESC ");
        return $query;
    }    
    public function getAllUser() {
        $query = $this->select();        
        return $this->fetchAll($query)->toArray();        
    }    
    
    public function deleteUser($id) {
        if ($this->delete("id =$id"))
            return true;
        else
            return false;
    }
    public function insertUser($data) {
        $kq = $this->insert($data);
        if ($kq)
            return true;
        else
            return false;
    }
    /*
     * The function gets name for deleteAction at FoodController
     */

    public function getUserID($id) {
        $se = $this->select();
        $se->where("id = ?", $id);
        return $this->fetchRow($se);
    }    
    
/*
 * 
 */
    public function getUserEmail($email) {
        $se = $this->select();
        $se->where("email = ?", $email);
        return $this->fetchRow($se);
    }    
    /*
     * The function checks email
     */    
    public function checksEmail($email) {
        $se = $this->select();
        $se->where("email = ?", $email);
        $kq = $this->fetchRow($se);
        if ($kq)
            return $kq;
        else
            return false;
    }    
}

?>
