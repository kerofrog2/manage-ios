<?php

/**
 * Project
 *
 * @category   Model
 * @package    ADMIN
 * @copyright  Copyright 2012-2013 Cliff Viegas Developments
 * @license    V1
 * @version    Foods.php 2013-01-29 11:30:12
 * @author     Cliff Viegas
 */
class Admin_Model_Messages extends Zend_Db_Table_Abstract {

    protected $_name = "manage_apns_messages";
    protected $_primary = "pid";
    
        /**
	 * Fetch Messages
	 *
	 * This gets called by a cron job that runs as often as you want.  You might want to set it for every minute.
	 *
	 * @access public
	 */
	public function fetchMessages(){
		// only send one message per user... oldest message first
            $date = new Zend_Db_Expr('NOW()');
            $select = $this->select();
            $select->setIntegrityCheck(false);
            $select->from(array('messages' => 'manage_apns_messages'), array('messages.pid', 'messages.message'))
                    ->joinLeft(array('devices' => 'manage_apns_devices'),
                                      'devices.pid = messages.fk_device',
                                        array('devices.devicetoken', 'devices.development'));            
            $select->where("messages.status = ?", 'queued');
            $select->where("messages.delivery <= ?", $date);
            $select->where("devices.status = ?", 'active');
            $select->group('messages.fk_device');
            $select->order('messages.created ASC');
            $select->limit(100, 0);
            $result = $this->fetchAll($select);
            return $result;
	}

	/**
	 * Flush Messages
	 *
	 * This gets called by a cron job that runs as often as you want.  You might want to set it for every minute.
	 * Like _fetchMessages, but sends all the messages for each device (_fetchMessage sends only the first message for device)
	 *
	 * @access public
	 */
	public function flushMessages(){
		// only send one message per user... oldest message first
		
            $date = new Zend_Db_Expr('NOW()');
            $select = $this->select();
            $select->setIntegrityCheck(false);
            $select->from(array('messages' => 'manage_apns_messages'), array('messages.pid', 'messages.message'))
                    ->joinLeft(array('devices' => 'manage_apns_devices'),
                                'devices.pid = messages.fk_device',
                                array('devices.devicetoken', 'devices.development'));
            $select->where("messages.status = ?", 'queued');
            $select->where("messages.delivery <= ?", $date);
            $select->where("devices.status = ?", 'active');
            $select->order('messages.created ASC');
            $select->limit(100, 0);
            $result = $this->fetchAll($select);
            return $result;
            
            
	}
       /**
	 * Queue Message for Delivery
	 *
	 * <code>
	 * <?php
	 * $db = new DbConnect('localhost','dbuser','dbpass','dbname');
	 * $db->show_errors();
	 * $apns = new APNS($db);
	 * $apns->newMessage(1, '2010-01-01 00:00:00');
	 * $apns->addMessageAlert('You got your emails.');
	 * $apns->addMessageBadge(9);
	 * $apns->addMessageSound('bingbong.aiff');
	 * $apns->queueMessage(); // ADD THE MESSAGE TO QUEUE
	 * ?>
 	 * </code>
	 *
	 * @access public
	 */
    public function queueMessage($fk_device,$message,$delivery){
            $date = new Zend_Db_Expr('NOW()');   
            $data = array(
                'fk_device'=>$fk_device,
                'message'=>$message,
                'delivery'=>$delivery,
                'status'=>'queued',
                'created'=>$date,
                'modified'=>$date
            );
            //print_r($data);exit;
            $this->insert($data);
    }
        /**
	 * APNS Push Success
	 *
	 * This gets called automatically by _pushMessage.  When no errors are present, then the message was delivered.
	 *
	 * @param int $pid Primary ID of message that was delivered
	 * @access public
	 */
	public function pushSuccess($pid){
            $data = array(
                'status'      => 'delivered'
            );
            $where = $this->getAdapter()->quoteInto('pid = ?', $pid);
            return $this->update($data, $where);
	}

	/**
	 * APNS Push Failed
	 *
	 * This gets called automatically by _pushMessage.  If an error is present, then the message was NOT delivered.
	 *
	 * @param int $pid Primary ID of message that was delivered
	 * @access public
	 */
	public function pushFailed($pid){
            $data = array(
                'status'      => 'failed'
            );
            $where = $this->getAdapter()->quoteInto('pid = ?', $pid);
            return $this->update($data, $where);
	}
        
        public function listItemMessages($arrParam = null, $options = null) {
        $db = $this->getAdapter();

        if ($options['task'] == 'list') {
            $paginator = $arrParam['paginator'];

             $select = $this->select();
            $select->setIntegrityCheck(false);
            $select->from(array('messages' => 'manage_apns_messages'), array('messages.pid', 'messages.message','messages.delivery','messages.status'))
                    ->joinLeft(array('devices' => 'manage_apns_devices'),
                                'devices.pid = messages.fk_device',
                                array('devices.devicetoken', 'devices.development','devices.appname'));

            if ($paginator['itemCountPerPage'] > 0) {
                $page = $paginator['currentPage'];
                $rowCount = $paginator['itemCountPerPage'];
                $select->limitPage($page, $rowCount);
            }
            $result = $db->fetchAll($select);
        }
        if ($options['task'] == 'countList') {
            $select = $db->select()
                    ->from('manage_apns_messages', array('COUNT(pid) AS totalItem'));
            $result = count($db->fetchOne($select));
        }
        return $result;
    }
    public function getAllMessage() {
        $select = $this->select();
        $select->from($this->_name, array('pid'));
        return $select;
    }
    public function countQueued(){
		
		$select = $this->select();
                $select->from($this->_name, array('COUNT(status) as total'));
                $select->where('status = "queued"');
                return $this->fetchAll($select)->toArray();
	}
   public function countDelivered(){
		
		$select = $this->select();
                $select->from($this->_name, array('COUNT(status) as delivered'));
                $select->where('status = "delivered"');
                return $this->fetchAll($select)->toArray();
	}
   public function countFailed(){
		
		$select = $this->select();
                $select->from($this->_name, array('COUNT(status) as failed'));
                $select->where('status = "failed"');
                return $this->fetchAll($select)->toArray();
	}

}

?>
