<?php

/* * ********* Email text when the server send mail successfully ********** */
return array(
    // email server , this email use for register,resetPassword,activation email ws 
    "email" => "tailieucuongnd2010@gmail.com",
    "password" => "smarterapp",
    
    "successfully" => "<h4>Please check your email inbox for your instructions.",
    "subjectAdminLostpassword" => "Password Assistance",
    "subjectAdminSendpass" => "New Password",
    "resetSuccessfully" => "<h3>Your password has been reset successfully!</h3>",
    "contentAdminLostpassword" =>
    "
            <h4>Dear [emailUser]</h4>
            <p>
            To initiate the password reset process for your caregivers account, click the link below:
            <a href='[activeLink]'>[activeLink]</a>
            </p>
            <p>
            If clicking the link above doesn't work, please copy and paste the URL in a
            new browser window instead.
            </p>
    ",
    "contentAdminSendpass" =>
    "             
            <p>
            Your Email is: [email]<br/>
            Your Password is: [password]<br/>
            </p>
    "


);
/* * ********* End email text ********** */
?>
