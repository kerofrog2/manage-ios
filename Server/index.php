<?php

date_default_timezone_set('Asia/Ho_Chi_Minh');
define('URL_ROOT', '/Manage/Server/');

define('PUBLIC_URL', URL_ROOT . 'public');
define('SCRIPTS_URL', URL_ROOT . 'public/scripts');

define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/public'));
define('FILES_PATH', PUBLIC_PATH . '/files');

//Define the path to the application
defined('APPLICATION_PATH')
	|| define('APPLICATION_PATH', 
			  realpath(dirname(__FILE__) . '/application'));

//Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV',
              (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
                                         : 'development'));
//Define library
set_include_path(implode(PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH . '/../library'),
            get_include_path(),
        )));


//Call Zend_Application class
require_once 'Zend/Application.php';
$environment = APPLICATION_ENV;
$options = APPLICATION_PATH . '/configs/application.ini';
$application = new Zend_Application($environment, $options);
$application->bootstrap()->run();
