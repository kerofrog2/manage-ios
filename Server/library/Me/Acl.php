<?php
class Me_Acl extends Zend_Acl{
    public function __construct() {
        $this->addRole(new Zend_Acl_Role("user"));
        $this->addRole(new Zend_Acl_Role("moderator"));
        $this->addRole(new Zend_Acl_Role("admin"));
        
        $this->addResource(new Zend_Acl_Resource("admin:index"));
        $this->addResource(new Zend_Acl_Resource("admin:food"));
        $this->addResource(new Zend_Acl_Resource("default:index"));
        
        $this->allow("user","admin:index",array('index','login','logout'));
        $this->allow("moderator","admin:index",null);
        $this->allow("moderator","admin:food",array('index','load'));
        $this->allow("admin",null,null);
    }
}

?>