<?php
class Me_AclPlugin extends Zend_Controller_Plugin_Abstract{
    
    protected $_acl;
    public function __construct($acl) {
        $this->_acl = $acl;
    }
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $info = $auth->getIdentity();
            $level = $info->level;
            $role = "";
            switch($level){
				case 1: $role = "user"; break;
				case 2: $role = "moderator"; break;
				case 3: $role = "admin"; break; 
			}
             $module = $request->getModuleName();
             $controller = $request->getControllerName();
             $action = $request->getActionName();
             if(!$this->_acl->isAllowed($role, $module.':'.$controller,$action))
             {
                 $request->setModuleName("admin")
                         ->setControllerName("index")
                         ->setActionName("error");
             }
        }
    }
}

?>