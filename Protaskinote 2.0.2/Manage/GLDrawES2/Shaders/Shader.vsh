//
//  Shader.vsh
//  GLDrawES2
//
//  Created by Adam Lockhart on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//



attribute vec4 position;
attribute vec4 color;

varying vec4 colorVarying;

uniform mat4 u_mvpMatrix;
uniform mat4 tsMatrix;


void main() {
    gl_Position = position;
    
    gl_Position *= tsMatrix;
    gl_Position *= u_mvpMatrix;
    
    colorVarying = color;
}
