//
//  Knot.h
//  GLDrawES2
//
//  Created by Adam Lockhart on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Structures.h"

@interface Knot : NSObject {
    float distance, t, distanceToHere, linearDistance;
    BOOL remove;
    vertex v, closeLinear;
}

@property (assign) float distance;
@property (assign) float linearDistance;
@property (assign) float distanceToHere;
@property (assign) float t;
@property (assign) vertex v;
@property (assign) vertex closeLinear;
@property (assign) BOOL remove;

+(Knot*) knot;

-(void) reset;

@end
