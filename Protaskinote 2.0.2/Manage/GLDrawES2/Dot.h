//
//  Dot.h
//  GLDrawES2
//
//  Created by Adam Lockhart on 5/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Structures.h"

@interface Dot : NSObject <NSCopying, NSCoding> {
    //vertex v;
    //GLfloat radius;
}

+(Dot*) dotWithVertex: (vertex) newVertex andColor: (rgba) newColor andRadius: (GLfloat) newRadius;


//-(void) setVertex: (vertex) newVertex;
//-(void) setRadius: (GLfloat) newRadius;

//-(vertex*) dotVertex;
//-(GLfloat*) radius;

@property (nonatomic, assign) vertex v;
@property GLfloat radius;
@property (nonatomic, assign) rgba color;

@end
