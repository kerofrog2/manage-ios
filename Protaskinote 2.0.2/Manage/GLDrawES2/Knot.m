//
//  Knot.m
//  GLDrawES2
//
//  Created by Adam Lockhart on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Knot.h"

int removeCount = 0;

@implementation Knot

@synthesize distance, v, t, distanceToHere, linearDistance, closeLinear;

- (id)init
{
    self = [super init];
    if (self) {
        distance = 100000000.f;
        linearDistance = 100000000.f;
        remove = NO;
        t = -1.f;
    }
    
    return self;
}

-(void) dealloc {
    
    [super dealloc];
}

+(Knot*) knot {
    Knot *newKnot = [[Knot alloc] init];
    return [newKnot autorelease];
}

-(void) reset {
    if (remove) {
        remove = NO;
        removeCount--;
    }
    distance = 100000000.f;
    linearDistance = 100000000.f;
    t = -1.f;
}

-(void)setRemove:(BOOL)rmv {
    remove = rmv;
    if (remove) {
        removeCount++;
    }
}
-(BOOL)remove{return remove;}

@end
