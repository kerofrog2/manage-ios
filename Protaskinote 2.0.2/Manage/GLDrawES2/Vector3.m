//
//  Vector3.m
//  GLDrawES2
//
//  Created by Adam Lockhart on 7/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Vector3.h"
#import <math.h>

@implementation Vector3

@synthesize x,y,z,magnitude;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+(Vector3*) vector {
    return [[[Vector3 alloc] init] autorelease];
}

-(Vector3*) minus: (Vector3*) operand {
    Vector3* v = [Vector3 vector];
    v.x = x-operand.x;
    v.y = y-operand.y;
    v.z = z-operand.z;
    v.magnitude = sqrtf(SQUARED(v.x)+SQUARED(v.y)+SQUARED(v.z));
    return v;
}

-(Vector3*) minusf:(float)operand {
    Vector3* v = [Vector3 vector];
    v.x = x-operand;
    v.y = y-operand;
    v.z = z-operand;
    v.magnitude = sqrtf(SQUARED(v.x)+SQUARED(v.y)+SQUARED(v.z));
    return v;
}

-(Vector3*) plus: (Vector3*) operand {
    Vector3* v = [Vector3 vector];
    v.x = x + operand.x;
    v.y = y + operand.y;
    v.z = z + operand.z;
    v.magnitude = sqrtf(SQUARED(v.x)+SQUARED(v.y)+SQUARED(v.z));
    return v;
}

-(Vector3*) scaled: (float) operand {
    Vector3* v = [Vector3 vector];
    v.x = x* operand;
    v.y = y* operand;
    v.z = z* operand;
    v.magnitude = sqrtf(SQUARED(v.x)+SQUARED(v.y)+SQUARED(v.z));
    return v;
}

-(Vector3*) normalized {
    Vector3* v = [Vector3 vector];
    v.x = x/magnitude;
    v.y = y/magnitude;
    v.z = z/magnitude;
    return v;
}

-(void) setxyz: (float)xx:(float)yy:(float)zz {
    x = xx;
    y= yy;
    z= zz;
    magnitude = sqrtf(SQUARED(x)+SQUARED(y)+SQUARED(z));
}

-(float) dot: (Vector3*) v {
    return v.x*x+v.y*y+v.z*z;    
}


-(float) sqrMagnitude {
    return magnitude*magnitude;
}

@end
