//
//  BezierPath.h
//  
//
//  Created by Adam Lockhart on 7/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Structures.h"
#import "Vector3.h"

#define DIVISION_THRESHOLD -0.99f
#define SEGMENTS_PER_CURVE 10
#define MINIMUM_SQR_DISTANCE 0.01f



@interface BezierPath : NSObject {
    NSMutableArray *controlPoints;
    NSUInteger curveCount;
}

@property (retain) NSMutableArray *controlPoints;

-(void) interpolate: (NSMutableArray*) segmentPoints withScale: (NSInteger) scale;
-(void) samplePoints: (NSMutableArray*) sourcePoints : (float) minSqrDistance : (float) maxSqrDistance: (float) scale;
-(Vector3*) calculateBezierPoint: (int) curveIndex : (float) t;
-(NSMutableArray*) getDrawingPoints0;
-(NSMutableArray*) getDrawingPoints1;
-(NSMutableArray*) getDrawingPoints2;
-(NSMutableArray*) findDrawingPoints: (int) curveIndex;
-(int) findDrawingPoints2: (int) curveIndex : (float) t0 : (float) t1: (NSMutableArray*) pointList : (int) insertionIndex;
-(Vector3*) calculateBezierPoint2: (float) t:(Vector3*) p0:(Vector3*) p1:(Vector3*) p2:(Vector3*) p3;

@end
