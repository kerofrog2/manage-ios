//
//  Structures.h
//  GLDraw
//
//  Created by Adam Lockhart on 4/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#include <OpenGLES/ES1/gl.h>
#include <QuartzCore/QuartzCore.h>


typedef struct vertex {
    GLfloat x,y;
} vertex;

typedef struct vector { 
    vertex v1, v2, dir, norm;
    GLfloat endWidth, startWidth, len;
    GLboolean vectorIsSet;
} vector;

typedef struct tangent {
    vertex origin, direction;
} tangent;

typedef struct glWinCoord {
    GLint x,y;
}glWinCoord;

typedef struct rgba {
    float r,g,b,a;
} rgba;

rgba rgbaMake(float r,float g,float b,float a);

rgba rgbMake(float r,float g,float b);

void setUpAngle();

GLboolean vertexIsEqual(vertex a, vertex b);

GLfloat angleSin(int i);

GLfloat angleCos(int i);

void rotateAbyB(vertex *a, vertex b);

void translateAbyB(vertex *a, vertex b);

vertex vertexMake (GLfloat x, GLfloat y);

vertex vectorDirection (vertex origin, vertex end);

void vectorMagnitude (vector *v);

void vectorNormalize (vector *v);

vector vectorMake (vertex origin, vertex end);

vertex negativeVertex(vertex v);

vertex addVertex (vertex a, vertex b);

vertex subtractVertex (vertex a, vertex b);

vertex scaled(vertex v, GLfloat s);

void vectorSet (vector *v, vertex origin, vertex end);

GLfloat distanceBetweenPoints(vertex a, vertex b);

//GLfloat crossProduct(vector a, vector b);

GLfloat dotProduct(vertex a, vertex b);

GLfloat angleInRadians(vector a, vector b);

vertex weightedCurve(GLfloat t, GLfloat hj, GLfloat hj1);
void setWeightedCurveConstants(tangent v1, tangent v2, GLfloat alphaj, GLfloat betaj1);
vertex cp1();
vertex cp2();

void setCurveConstants(tangent v1, tangent v2);
void setCubicCurveConstants(tangent t1, tangent t2);
void setCatmullConstants(vertex v1,vertex v2,vertex v3,vertex v4);
tangent tangentForPoints(vertex v1, vertex v2, vertex v3, vertex v4, vertex v5);
tangent tangentj(vertex v1, vertex v2, vertex v3, vertex v4, vertex v5);
vertex catmullCurve(GLfloat t);
vertex curve(GLfloat t);
vertex cubicCurve(GLfloat t);
vertex controlPoint (vertex p1, vertex p2, vertex p3);

vertex normalOfVectors(vector a, vector b);

vertex controlPointB1(vector a, vector b);

vertex controlPointB2 (vector a, vector b);

float percentDiff(float a, float b);

GLfloat bezier(GLfloat t, GLfloat p1, GLfloat p2, GLfloat p3, GLfloat p4);

GLfloat linearInterp(GLfloat t, GLfloat start, GLfloat end);

void setColor(GLubyte *color, int *colorindex);

void setColour(GLubyte *color, int *colorindex);

void setDotColor(GLubyte *dotColor, rgba aColor);

GLboolean colorIsEqual (rgba color1, rgba color2);

