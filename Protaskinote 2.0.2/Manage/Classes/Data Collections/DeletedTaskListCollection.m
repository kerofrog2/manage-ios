//
//  DeletedTaskListCollection.m
//  Manage
//
//  Created by Cliff Viegas on 30/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "DeletedTaskListCollection.h"

// Business Layer
#import "BLDeletedList.h"

// Data Models
#import "DeletedTaskList.h"
#import "TaskList.h"

// DM Helper
#import "DMHelper.h"

@implementation DeletedTaskListCollection

@synthesize deletedLists;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[self.deletedLists release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if ((self = [super init])) {
		// Init the deleted lists array
		self.deletedLists = [NSMutableArray array];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

// Alternate copy is where we want to make sure there is no other list with this name active in DB
- (void)loadWithListsDeletedAfter:(NSString *)dateTimeString checkForAlternateCopy:(BOOL)alternateCopy {
	// Remove all lists from array (although shouldn't be any)
	[self.deletedLists removeAllObjects];
	
	// Get the array of deleted lists
	NSArray *localArray = [BLDeletedList getAllDeletedLists];
	
	// Create a new object for each deleted list taken from database
	for (NSDictionary *listDictionary in localArray) {
		DeletedTaskList *deletedTaskList = [[[DeletedTaskList alloc] init] autorelease];
		
		deletedTaskList.listID = [DMHelper getNSIntegerValueForKey:@"ListID" fromDictionary:listDictionary];
		deletedTaskList.title = [DMHelper getNSStringValueForKey:@"Title" fromDictionary:listDictionary];
		deletedTaskList.toodledoFolderID = [DMHelper getSInt64ValueForKey:@"ToodledoFolderID" fromDictionary:listDictionary];
		deletedTaskList.deletedDateTime = [DMHelper getNSStringValueForKey:@"DeletedDateTime" fromDictionary:listDictionary];
        deletedTaskList.isNote = [DMHelper getBoolValueForKey:@"IsNote" fromDictionary:listDictionary];
        
        if ([deletedTaskList isNote]) {
            continue;
        }
        
		// Need to compare dates
		NSComparisonResult comparisonResult = [deletedTaskList.deletedDateTime compare:dateTimeString];
		
		// Means our dateTimeString (last sync datetime) is earlier than the deletion date, so get it
		if (comparisonResult == NSOrderedDescending) {
			if (alternateCopy == NO) {
				[self.deletedLists addObject:deletedTaskList];
			} else {
				// Check if there are any 'alternate copies' of this deleted list before getting it
				if ([TaskList getCountOfListsWithTitle:deletedTaskList.title] == 0) {
					[self.deletedLists addObject:deletedTaskList];
				}
			}

			
		} 
		
	}
}


@end
