//
//  ApplicationSettingCollection.h
//  Manage
//
//  Created by Cliff Viegas on 4/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class ApplicationSetting;

@interface ApplicationSettingCollection : NSObject {
	NSMutableArray		*applicationSettings;
}

@property (nonatomic, retain)	NSMutableArray	*applicationSettings;

#pragma mark Initialisation
- (id)initWithAllUserSettings;

#pragma mark Get Methods
- (NSInteger)getIndexOfSettingWithID:(NSInteger)theSettingID;
- (ApplicationSetting *)getApplicationSettingWithName:(NSString *)theName;

@end
