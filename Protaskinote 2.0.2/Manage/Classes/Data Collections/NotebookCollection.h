//
//  NotebookCollection.h
//  Manage
//
//  Created by Cliff Viegas on 17/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotebookCollection : NSObject {
    NSMutableArray      *notebooks;             // Array of notebooks
    NSInteger           selectedNotebookIndex;  // The selected notebook index
    
    
    CGRect              notebookListTileFrame;  // Purely a helper method for positioning when opening up list tile in notebook
}

@property   (nonatomic, retain) NSMutableArray  *notebooks;
@property   (nonatomic, assign) NSInteger       selectedNotebookIndex;
@property   (nonatomic, assign) CGRect          notebookListTileFrame;

#pragma mark Initialisation
- (id)initWithAllNotebooks;

#pragma mark Get Methods
- (NSInteger)getIndexOfNotebookWithID:(NSInteger)theNotebookID;

#pragma mark Delete Methods
- (void)deleteNotebookAtIndex:(NSInteger)index;

@end
