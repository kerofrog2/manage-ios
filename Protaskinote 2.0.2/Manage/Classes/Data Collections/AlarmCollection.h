//
//  AlarmCollection.h
//  Manage
//
//  Created by Cliff Viegas on 15/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlarmCollection : NSObject {
	NSMutableArray		*alarms;
}

@property (nonatomic, retain)	NSMutableArray	*alarms;

#pragma mark Load Methods
- (void)loadAlarmsWithListItemID:(NSInteger)listItemID;

#pragma mark Get Methods
- (NSInteger)getIndexOfAlarmWithAlarmGUID:(NSString *)theAlarmGUID;
- (NSInteger)getNumberOfAlarmsWithListItemID:(NSInteger)listItemID;

#pragma mark Delete Methods
- (void)deleteAlarmAtIndex:(NSInteger)alarmIndex;

@end
