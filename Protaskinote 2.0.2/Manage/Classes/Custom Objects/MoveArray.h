//
//  MoveArray.h
//  Manage
//
//  Created by Cliff Viegas on 14/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

@interface NSMutableArray (MoveArray)

- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to;

@end
