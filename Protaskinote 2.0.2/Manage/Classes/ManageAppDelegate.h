//
//  ManageAppDelegate.h
//  Manage
//
//  Created by Cliff Viegas on 16/06/10.
//  Copyright kerofrog 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

// Globals
#import "Globals.h"

#import <AudioToolbox/AudioToolbox.h>

// Data Collections
@class TaskListCollection;

@interface ManageAppDelegate : NSObject <UIApplicationDelegate> {
	TaskListCollection			*masterTaskListCollection;
    UIWindow					*window;
	UINavigationController		*navigationController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) UINavigationController *navigationController;

#pragma mark Class Methods
- (void)createResourceFolders;

#pragma mark Custom Methods
- (void)archiveCheck;
- (void)databaseChecks;
- (void)updateSettings:(NSString *)version;

@end

