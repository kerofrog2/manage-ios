//
//  HitTestContainerView.h
//  Manage
//
//  Created by Cliff Viegas on 16/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HitTestContainerView : UIView {
	UIView *refParentView;
}

- (void)setParentView:(UIView *)parentView;


@end
