//
//  ToodledoFolders.h
//  Manage
//
//  Created by Cliff Viegas on 13/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ToodledoFolderCollection : NSObject {
	NSURLConnection							*toodledoConnection;
	NSMutableArray							*folders;
	NSString								*toodledoKey;
	NSMutableData							*responseData;
}

@property	(nonatomic, retain)		NSMutableArray		*folders;
@property	(nonatomic, copy)		NSString			*toodledoKey;

#pragma mark Initialisation
- (id)initUsingKey:(NSString *)theToodledoKey;

#pragma mark Public Methods
- (NSString *)collectFoldersFromServer;
- (void)cancelConnection;

#pragma mark Get Methods
- (NSInteger)getIDOfFolderWithName:(NSString *)folderName;
- (NSString *)getNameOfFolderWithID:(NSInteger)theFolderID;
- (BOOL)doesFolderExistWithName:(NSString *)folderName;
- (BOOL)doesFolderExistWithID:(NSInteger)theFolderID;

#pragma mark Delete Methods
- (void)deleteAllFoldersUsingResponseString:(NSMutableString **)mutableResponseString;

#pragma mark Test Methods
- (void)testDisplayAllFolders;

@end
 
