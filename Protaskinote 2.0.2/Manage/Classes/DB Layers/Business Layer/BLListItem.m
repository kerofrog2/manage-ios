//
//  BLListItem.m
//  Manage
//
//  Created by Cliff Viegas on 17/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "BLListItem.h"

// Data Layer
#import "SQLiteAccess.h"

// Method Helper
#import	"MethodHelper.h"

// Data Models
#import "ListItem.h"

// JSON Support
#import "JSON.h"

// Dot Object
#import "Dot.h"

@implementation BLListItem

#pragma mark -
#pragma mark SELECT Methods

+ (NSArray *)getAllListItemsWithListID:(NSInteger)listID {
	NSString *sql =[NSString stringWithFormat:@"SELECT * FROM ListItem WHERE ListID = %d AND Archived = 0 ORDER BY ItemOrder DESC, "
					"ParentListItemID ASC, SubItemOrder DESC, CreationDateTime DESC",
					listID];
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

+ (NSArray *)getAllArchivedListItems {
	NSString *sql = @"SELECT * FROM ListItem WHERE Archived = 1 ORDER BY ArchivedDateTime DESC";
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

// Completed
+ (NSArray *)getAllListItemsWithListID:(NSInteger)listID sortedBy:(NSInteger)sorted {
	NSString *sql = [NSString stringWithFormat:@"SELECT * FROM ListItem WHERE ListID = %d AND Archived = 0", listID];
	
	switch (sorted) {
		case EnumSortByList:
			sql = [NSString stringWithFormat:@"%@ ORDER BY ItemOrder DESC, ParentListItemID ASC, SubItemOrder DESC, "
				   "CreationDateTime DESC", sql];
			break;
		case EnumSortByPriority:
			sql = [NSString stringWithFormat:@"%@ ORDER BY Priority DESC, ItemOrder DESC, ParentListItemID ASC, "
				   "SubItemOrder DESC, CreationDateTime DESC", sql];
			break;
		case EnumSortByDueDate:
			sql = [NSString stringWithFormat:@"SELECT * FROM ListItem WHERE ListID = %d AND DueDate NOT NULL " 
				   "ORDER BY DueDate ASC, ItemOrder DESC, ParentListItemID ASC, "
				   "SubItemOrder DESC, CreationDateTime DESC", listID];
			NSMutableArray *firstArray = [NSMutableArray arrayWithArray:[SQLiteAccess selectManyRowsWithSQL:sql]];
			
			sql = [NSString stringWithFormat:@"SELECT * FROM ListItem WHERE ListID = %d AND DueDate IS NULL " 
				   "ORDER BY ItemOrder DESC, ParentListItemID ASC, "
				   "SubItemOrder DESC, CreationDateTime DESC", listID];
			NSMutableArray *secondArray = [NSMutableArray arrayWithArray:[SQLiteAccess selectManyRowsWithSQL:sql]];
			
			[firstArray addObjectsFromArray:secondArray];
			
			return firstArray;
			
			break;
		case EnumSortByCompleted:
			sql = [NSString stringWithFormat:@"%@ ORDER BY Completed DESC, ItemOrder DESC, ParentListItemID ASC, "
				   "SubItemOrder DESC, CreationDateTime DESC", sql];
			
			break;
		default:
			break;
	}
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
} 

+ (NSArray *)getAllListItemsWithDueDate {
	NSString *sql = [NSString stringWithFormat:@"SELECT * FROM ListItem WHERE DueDate NOT NULL "
					 "AND Completed = 0 AND Archived = 0 ORDER BY DueDate ASC"];
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

+ (NSArray *)getDateDifferenceForAllListItemsWithDueDate {
	NSString *sql = @" SELECT (julianday(Date('now')) - julianday(DueDate)) AS DateDifference FROM ListItem WHERE DueDate NOT NULL AND Completed = 0 AND Archived = 0";
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

// Returns the largest item order + 1
+ (NSInteger)getNextItemOrderForListID:(NSInteger)listID {
	NSString *sql = [NSString stringWithFormat:@"SELECT ItemOrder FROM ListItem WHERE ListID = %d AND Archived = 0 ORDER BY ItemOrder DESC LIMIT 1", listID];
	NSString *largestItemOrder = [SQLiteAccess selectOneValueSQL:sql];

	NSInteger nextItemOrder = 0;
	if (largestItemOrder != nil) {
		nextItemOrder = [largestItemOrder integerValue] + 1;
	}
	//NSLog(@"%@", sql);
	return nextItemOrder;
}

+ (NSInteger)getFirstItemOrderForListID:(NSInteger)listID {
	NSString *sql = [NSString stringWithFormat:@"SELECT ItemOrder FROM ListItem WHERE ListID = %d AND Archived = 0 ORDER BY ItemOrder ASC LIMIT 1", listID];
	NSString *smallestItemOrder = [SQLiteAccess selectOneValueSQL:sql];
    
	NSInteger firstItemOrder = 0;
	if (smallestItemOrder != nil) {
		firstItemOrder = [smallestItemOrder integerValue] - 1;
	}
	//NSLog(@"%@", sql);
	return firstItemOrder;
}

+ (NSInteger)getNextSubItemOrderForListItemID:(NSInteger)listItemID {
	NSString *sql = [NSString stringWithFormat:@"SELECT SubItemOrder FROM ListItem WHERE ParentListItemID = %d AND Archived = 0 " 
					 "ORDER BY SubItemOrder DESC LIMIT 1", listItemID];
	NSString *largestSubItemOrder = [SQLiteAccess selectOneValueSQL:sql];
	NSInteger nextSubItemOrder = 0;
	if (largestSubItemOrder != nil) {
		nextSubItemOrder = [largestSubItemOrder integerValue] + 1;
	}
	//NSLog(@"%@", sql);
	return nextSubItemOrder;
}

+ (NSInteger)getNumberOfListItemsWithDueDate {
	NSString *sql = @"SELECT COUNT(*) FROM ListItem WHERE DueDate NOT NULL AND Completed = 0 AND Archived = 0";
	//NSLog(@"%@", sql);
	return [[SQLiteAccess selectOneValueSQL:sql] integerValue];
}

#pragma mark -
#pragma mark INSERT Methods

// Insert the passed page into the page table
+ (NSInteger)insertNewListItem:(ListItem *)listItem {
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO ListItem (ListID, CategoryID, Priority, "
					 "Title, CreationDateTime, DueDate, Notes, Scribble, Completed, ItemOrder, ParentListItemID, "
					 "SubItemOrder, CompletedDateTime, Archived, ArchivedDateTime, RecurringListItemID, HasAlarms, "
					 "ModifiedDateTime, ToodledoTaskID) "
					 "Values (%d, %d, %d, %@, %@, %@, %@, %@, %d, %d, %d, %d, %@, %d, %@, %d, %d, %@, %qi)",
					 listItem.listID, listItem.categoryID, listItem.priority,
					 [MethodHelper escapeStringsForSQL:listItem.title],
					 [MethodHelper escapeStringsForSQL:listItem.creationDateTime],
					 [MethodHelper escapeStringsForSQL:listItem.dueDate],
					 [MethodHelper escapeStringsForSQL:listItem.notes],
					 [MethodHelper escapeStringsForSQL:listItem.scribble], listItem.completed,
					 listItem.itemOrder, listItem.parentListItemID, listItem.subItemOrder,
					 [MethodHelper escapeStringsForSQL:listItem.completedDateTime], listItem.archived,
					 [MethodHelper escapeStringsForSQL:listItem.archivedDateTime],
					 listItem.recurringListItemID, listItem.hasAlarms,
					 [MethodHelper escapeStringsForSQL:listItem.modifiedDateTime],
					 listItem.toodledoTaskID];
	//NSLog(@"%@", sql);
	return [[SQLiteAccess insertWithSQL:sql] integerValue];
	
}

#pragma mark -
#pragma mark UPDATE Methods

// CREATE TABLE "ListItem" ("ListItemID" INTEGER PRIMARY KEY  NOT NULL , "ListID" INTEGER NOT NULL , 
// "CategoryID" INTEGER, "PriorityID" INTEGER, "Title" VARCHAR, "CreationDateTime" VARCHAR NOT NULL , 
// "DueDate" VARCHAR, "Notes" TEXT, "Scribble" VARCHAR, "Completed" INTEGER)

// Updates the wireframe in the database for the specified wireframe guid
+ (void)updateDatabaseWithListItem:(ListItem *)listItem {
	NSString *sql = [NSString stringWithFormat:@"UPDATE ListItem SET ListID = %d, CategoryID = %d, Priority = %d, "
					 "Title = %@, CreationDateTime = %@, DueDate = %@, Notes = %@, Scribble = %@, Completed = %d, "
					 "ItemOrder = %d, ParentListItemID = %d, SubItemOrder = %d, CompletedDateTime = %@, Archived = %d, "
					 "ArchivedDateTime = %@, RecurringListItemID = %d, HasAlarms = %d, ModifiedDateTime = %@, "
					 "ToodledoTaskID = '%qi' WHERE ListItemID = %d",
					 listItem.listID, listItem.categoryID, listItem.priority, 
					 [MethodHelper escapeStringsForSQL:listItem.title],
					 [MethodHelper escapeStringsForSQL:listItem.creationDateTime],
					 [MethodHelper escapeStringsForSQL:listItem.dueDate],
					 [MethodHelper escapeStringsForSQL:listItem.notes],
					 [MethodHelper escapeStringsForSQL:listItem.scribble],
					 listItem.completed, listItem.itemOrder, listItem.parentListItemID,
					 listItem.subItemOrder,
					 [MethodHelper escapeStringsForSQL:listItem.completedDateTime], listItem.archived,
					 [MethodHelper escapeStringsForSQL:listItem.archivedDateTime],
					 listItem.recurringListItemID, listItem.hasAlarms,
					 [MethodHelper escapeStringsForSQL:listItem.modifiedDateTime], listItem.toodledoTaskID,
					 listItem.listItemID];		// This LINE is needed to be last, as it is the list item ID filter
	//  NSLog(@"%@", sql);
	[SQLiteAccess updateWithSQL:sql];
}

// Updates the item order for the specified list item
+ (void)updateItemOrder:(NSInteger)newOrder forListItemID:(NSInteger)listItemID {
	NSString *sql = [NSString stringWithFormat:@"UPDATE ListItem SET ItemOrder = %d WHERE ListItemID = %d", newOrder, listItemID];
	//NSLog(@"%@", sql);
	[SQLiteAccess updateWithSQL:sql];
}

// Updates the sub item order for the specified list item
+ (void)updateSubItemOrder:(NSInteger)newOrder forListItemID:(NSInteger)listItemID {
	NSString *sql = [NSString stringWithFormat:@"UPDATE ListItem SET SubItemOrder = %d WHERE ListItemID = %d", newOrder, listItemID];
	//NSLog(@"%@", sql);
	[SQLiteAccess updateWithSQL:sql];
}

#pragma mark -
#pragma mark DELETE Methods

+ (void)deleteListItemWithListItemID:(NSInteger)listItemID {
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM ListItem WHERE ListItemID = %d", listItemID];
	//NSLog(@"%@", sql);
	[SQLiteAccess deleteWithSQL:sql];
}

+ (void)deleteAllArchivedListItems {
	NSString *sql = @"DELETE FROM ListItem WHERE Archived = 1";
	
	[SQLiteAccess deleteWithSQL:sql];
}

@end
