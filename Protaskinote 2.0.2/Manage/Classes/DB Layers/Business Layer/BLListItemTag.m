//
//  BLListItemTag.m
//  Manage
//
//  Created by Cliff Viegas on 10/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "BLListItemTag.h"

// Data Layer
#import "SQLiteAccess.h"

// Data Models
#import "ListItemTag.h"

@implementation BLListItemTag

#pragma mark -
#pragma mark DELETE Methods

+ (void)deleteListItemTagWithTagID:(NSInteger)tagID andListItemID:(NSInteger)listItemID {
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM ListItemTag WHERE ListItemID = %d AND TagID = %d", listItemID, tagID];
	[SQLiteAccess deleteWithSQL:sql];
}

+ (void)deleteAllListItemTagsWithListItemID:(NSInteger)listItemID {
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM ListItemTag WHERE ListItemID = %d", listItemID];
	[SQLiteAccess deleteWithSQL:sql];
}

+ (void)deleteAllListItemTagsWithTagID:(NSInteger)tagID {
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM ListItemTag WHERE TagID = %d", tagID];
	[SQLiteAccess deleteWithSQL:sql];
}

+ (void)deleteAllListItemTags {
	NSString *sql = @"DELETE FROM ListItemTag";
	[SQLiteAccess deleteWithSQL:sql];
}

#pragma mark -
#pragma mark INSERT Methods

// Insert the new list item tag into database
+ (NSInteger)insertNewListItemTag:(ListItemTag *)listItemTag {
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO ListItemTag (ListItemID, TagID) VALUES (%d, %d)",
					 listItemTag.listItemID, listItemTag.tagID];
	return [[SQLiteAccess insertWithSQL:sql] integerValue];
	
}

@end
