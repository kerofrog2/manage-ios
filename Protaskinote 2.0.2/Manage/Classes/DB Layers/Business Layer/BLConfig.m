//
//  BLConfig.m
//  Manage
//
//  Created by Cliff Viegas on 5/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "BLConfig.h"

// Data Layer
#import "SQLiteAccess.h"

// DMHelper
#import "DMHelper.h"

// Method Helper
#import "MethodHelper.h"

@implementation BLConfig

#pragma mark -
#pragma mark Check Methods

+ (BOOL)checkIfColumn:(NSString *)column existsInTable:(NSString *)tableName {
	NSString *sql = [NSString stringWithFormat:@"PRAGMA table_info(%@)", tableName];
	NSArray *tableSchema = [SQLiteAccess selectManyRowsWithSQL:sql];
	
	for (NSDictionary *schemaDictionary in tableSchema) {
		NSString *schemaColumn = [DMHelper getNSStringValueForKey:@"name" fromDictionary:schemaDictionary];
		
		if ([schemaColumn isEqualToString:column]) {
			return TRUE;
		}
	}
	return FALSE;
}

+ (BOOL)checkIfTableExists:(NSString *)tableName {
	NSString *sql = [NSString stringWithFormat:@"SELECT * FROM sqlite_master WHERE name = '%@'", tableName];
	NSArray *tableSchema = [SQLiteAccess selectManyRowsWithSQL:sql];
	
	if ([tableSchema count] > 0) {
		return TRUE;
	}
	
	return FALSE;
}

#pragma mark -
#pragma mark Alter Methods

+ (void)addField:(NSString *)column toTable:(NSString *)tableName withType:(NSString *)type {
	NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ %@", tableName, column, type];
	[SQLiteAccess updateWithSQL:sql];
}

+ (void)addField:(NSString *)column toTable:(NSString *)tableName withType:(NSString *)type andNullable:(BOOL)isNullable andDefault:(NSString *)defaultValue {
	NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ %@", tableName, column, type];
	
	// Check if is nullable
	if (isNullable == FALSE) {
		sql = [NSString stringWithFormat:@"%@ NOT NULL", sql];
	}
	
	// Set the default value
	if ([type isEqualToString:@"INTEGER"]) {
		NSInteger integerDefault = [defaultValue integerValue];
		sql = [NSString stringWithFormat:@"%@ DEFAULT %d", sql, integerDefault];
	} else {
		sql = [NSString stringWithFormat:@"%@ DEFAULT %@", sql, defaultValue];
	}
	
	//NSLog(@"%@", sql);
	
	[SQLiteAccess updateWithSQL:sql];
}


+ (void)createTable:(NSString *)tableName withPrimaryKey:(NSString *)primaryField ofType:(NSString *)type {
	NSString *sql = [NSString stringWithFormat:@"CREATE TABLE \"%@\" (\"%@\" %@ PRIMARY KEY NOT NULL)",
					 tableName, primaryField, type];
	[SQLiteAccess updateWithSQL:sql];
}

+ (void)createTable:(NSString *)tableName withPrimaryKey:(NSString *)primaryField 
		andSecondaryKey:(NSString *)secondaryField ofType:(NSString *)type {
	
	NSMutableString *sql = [[[NSMutableString alloc] initWithString:@"CREATE TABLE"] autorelease];
	
	[sql appendFormat:@" %@ ", [MethodHelper escapeStringsForSQL:tableName]];
	[sql appendFormat:@" (%@ %@ NOT NULL,", [MethodHelper escapeStringsForSQL:primaryField], type];
	[sql appendFormat:@" %@ %@ NOT NULL,", [MethodHelper escapeStringsForSQL:secondaryField], type];
	[sql appendFormat:@" PRIMARY KEY (%@, %@))", [MethodHelper escapeStringsForSQL:primaryField],
	 [MethodHelper escapeStringsForSQL:secondaryField]];
	
	[SQLiteAccess updateWithSQL:sql];
}

#pragma mark -
#pragma mark Helper Methods

// Create the application setting table
+ (void)createApplicationSettingTable {
	NSString *sql = @"CREATE TABLE \"ApplicationSetting\" (\"Name\" VARCHAR NOT NULL DEFAULT 'Unnamed', \"Data\" VARCHAR)";
	[SQLiteAccess updateWithSQL:sql];
}

// Create the alarm table
+ (void)createAlarmTable {
	NSString *sql = @"CREATE TABLE \"Alarm\" (\"AlarmGUID\" VARCHAR PRIMARY KEY NOT NULL UNIQUE, \"ListItemID\" INTEGER NOT NULL, "
	"\"FireDateTime\" VARCHAR, \"Delay\" INTEGER, \"Measurement\" VARCHAR)";
	[SQLiteAccess updateWithSQL:sql];
}

+ (void)createDeletedListTable {
	NSString *sql = @"CREATE TABLE \"DeletedList\" (\"ListID\" INTEGER NOT NULL , \"Title\" VARCHAR, \"ToodledoFolderID\" VARCHAR, \"DeletedDateTime\" VARCHAR NOT NULL)";
	[SQLiteAccess updateWithSQL:sql];
}

+ (void)createDeletedListItemTable {
	NSString *sql = @"CREATE TABLE \"DeletedListItem\" (\"ListItemID\" INTEGER NOT NULL , \"Title\" VARCHAR, \"ListID\" INTEGER NOT NULL , "
	"\"ParentListItemID\" INTEGER NOT NULL , \"ToodledoTaskID\" VARCHAR, \"DeletedDateTime\" VARCHAR NOT NULL)";
	[SQLiteAccess updateWithSQL:sql];
}

@end
