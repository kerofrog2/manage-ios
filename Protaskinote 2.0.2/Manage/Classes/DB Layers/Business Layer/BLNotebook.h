//
//  BLNotebook.h
//  Manage
//
//  Created by Cliff Viegas on 17/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class Notebook;

@interface BLNotebook : NSObject {
    
}

#pragma mark SELECT Methods
+ (NSArray *)getAllNotebooks;
+ (NSInteger)getNextNotebookOrder;

#pragma mark DELETE Methods
+ (void)deleteNotebookWithID:(NSInteger)notebookID;

#pragma mark INSERT Methods
+ (NSInteger)insertNewNotebook:(Notebook *)notebook;

#pragma mark UPDATE Methods
+ (void)updateDatabaseWithNotebook:(Notebook *)notebook;

@end
