//
//  NoteViewController.m
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 13/09/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "NoteViewController.h"

// Data Models
#import "TaskListCollection.h"
#import "TaskList.h"

// View Controllers
#import "NewDueDateTableViewController.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#import "ListConstants.h"
#import "NoteConstants.h"

#define kHelpOverlayTag             7638

#define kWristPadViewPortrait       CGRectMake(68, 790, 634, 0)
#define kWristPadViewLandscape      CGRectMake(324, 530, 634, 0)
#define kWristPadDragViewPortrait   CGRectMake(335, 790, 100, 30)
#define kWristPadDragViewLandscape  CGRectMake(591, 530, 100, 30)


// Divide by 5 from 405
CGRect _NoteScrollControlFrame = {965, 238, 35, 81};

@interface NoteViewController()
- (void)loadBackground;
- (void)loadNavigationBarButtons;
- (void)loadTextFields;
- (void)loadButtons;
- (void)loadScrollView;
- (void)loadScrollControlWindow;
- (void)loadDrawingLayer;
- (void)loadControlPanel;
- (void)loadWristPad;
- (void)showDrawingFrame;
- (void)deselectAll;
- (void)blackPenButtonAction;
// Update Collections
- (void)reloadCollections;
@end

@implementation NoteViewController

@synthesize backButtonTitle;
@synthesize drawingLayer;
@synthesize dataStorage = _dataStorage;
@synthesize tileViewFrame;
@synthesize delegate;
@synthesize glScribbleDictionary;

#pragma mark - Memory Management

- (void)dealloc {
    
    // Footer and header lines
    [headerLine1 release];
    [footerLine1 release];
    [leftBorderLine release];
    [rightBorderLine release];
    
    // Control Panel
    [miniListPadTableView release];
    [miniListPadTableViewResponder release];
    [controlPanelImage release];
    [toolsContainerView release];
    [dayCalendarDateLabel release];
    [dayCalendarImageView release];
    
    // Buttons
    [greenPenButton release];
    [redPenButton release];
    [bluePenButton release];
    [blackPenButton release];
    [undoButton release];
    [redoButton release];
    [eraserButton release];
    [clearButton release];
    [lineWidthsButton release];
    
    // Drawing layer scroll view
    [drawingLayerScrollView release];
    
    // Drawing Layer
    [drawingLayer release];
    [_dataStorage release];
    [self.glScribbleDictionary release];
    
    // Wrist pad
    [wristPadView release];
    [wristPadDragView release];
    
    // Popover Controllers
    if (lineWidthsPopoverController != nil) {
        lineWidthsPopoverController = nil;
        [lineWidthsPopoverController release];
    }
    
    // Remove our observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection andTaskList:(TaskList *)theTaskList andBackButtonTitle:(NSString *)theBackButtonTitle {
    self = [super init];
    if (self) {
        refMasterTaskListCollection = theMasterTaskListCollection;
        refTaskList = theTaskList;
        
        
        self.glScribbleDictionary = [NSDictionary dictionary];
        
        self.backButtonTitle = theBackButtonTitle;
        
        // Init tile view frame to nil
		self.tileViewFrame = CGRectZero;
        
        // Create an observer for whenever application becomes active
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        self.title = refTaskList.title;
        
        [self loadBackground];
        
        [self loadNavigationBarButtons];
        
       
        
        [self loadButtons];
        
        [self loadScrollView];
        
        // Init the list options action sheet
        clearActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" 
                                         destructiveButtonTitle:@"Clear Drawing" 
                                              otherButtonTitles:nil];
        
        [self loadDrawingLayer];
    
        [self loadTextFields];
        
        [self loadScrollControlWindow];
        
        [self blackPenButtonAction];
        
        [self loadControlPanel];
        
        [self loadWristPad];
    }
    return self;
}

#pragma mark - Load Methods

- (void)loadWristPad {
    //wristPadView = [[UIView alloc] initWithFrame:CGRectMake(<#CGFloat x#>, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    // #define kListTableViewPortrait				CGRectMake(68, 85, 634, 705)
    // #define kListTableViewLandscape				CGRectMake(324, 85, 634, 445)
    wristPadView = [[UIView alloc] init];
    wristPadDragView = [[UIView alloc] init];
    
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        [wristPadView setFrame:kWristPadViewPortrait];
        [wristPadDragView setFrame:kWristPadDragViewPortrait]; // 385
        wristPadStartY= 790.0;
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        [wristPadView setFrame:kWristPadViewLandscape];
        [wristPadDragView setFrame:kWristPadDragViewLandscape]; // 641
        wristPadStartY = 530.0;
    }
    
    [wristPadView setBackgroundColor:[UIColor blackColor]];
    [wristPadDragView setBackgroundColor:[UIColor clearColor]];
    
    [wristPadView setAlpha:0.3f];
    
    UILabel *wristPadLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [wristPadLabel setTextAlignment:UITextAlignmentCenter];
    [wristPadLabel setTextColor:[UIColor whiteColor]];
    [wristPadLabel setBackgroundColor:[UIColor blackColor]];
    [wristPadLabel setText:@"Wrist Pad"];
    [wristPadLabel setAlpha:0.3f];
    [wristPadDragView addSubview:wristPadLabel];
    [wristPadLabel release];
    
    [self.view addSubview:wristPadView];
    [self.view addSubview:wristPadDragView];
    
    wristPadDragging = FALSE;
    wristPadOffset = 0.0;
    
}

- (void)loadBackground {
    // Load the canvass and page images
    NSString *canvasImageName = [refMasterTaskListCollection getImageNameForSection:@"Canvas"];
    canvassImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:canvasImageName]];
    pageImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PaperPreview.png"]];
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        [canvassImage setFrame:kCanvassImagePortrait];
        [pageImage setFrame:kPageImagePortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        [pageImage setFrame:kPageImageLandscape];
        [canvassImage setFrame:kCanvassImageLandscape];
    }
    [self.view addSubview:canvassImage];
    [self.view addSubview:pageImage];
    
    // Load the frame/border
    NSString *borderImageName = [refMasterTaskListCollection getImageNameForSection:@"Border"];
    borderImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:borderImageName]];
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        [borderImage setFrame:kBorderImagePortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        [borderImage setFrame:kBorderImageLandscape];
    }
    [self.view addSubview:borderImage];
    
    // Load/init related objects
	headerLine1 = [[UIView alloc] initWithFrame:kHeaderLine1Portrait];
	[headerLine1 setBackgroundColor:[UIColor lightGrayColor]];
	
    
    leftBorderLine = [[UIView alloc] initWithFrame:kNoteLeftBorderLinePortrait];
    [leftBorderLine setBackgroundColor:[UIColor lightGrayColor]];
    
    rightBorderLine = [[UIView alloc] initWithFrame:kNoteRightBorderLinePortrait];
    [rightBorderLine setBackgroundColor:[UIColor lightGrayColor]];
    
    footerLine1 = [[UIView alloc] initWithFrame:kFooterLine1Portrait];
	[footerLine1 setBackgroundColor:[UIColor lightGrayColor]];
	
	// Orientation calls
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[headerLine1 setFrame:kHeaderLine1Portrait];
        [footerLine1 setFrame:kFooterLine1Portrait];
        [rightBorderLine setFrame:kNoteRightBorderLinePortrait];
        [leftBorderLine setFrame:kNoteLeftBorderLinePortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[headerLine1 setFrame:kHeaderLine1Landscape];
        [footerLine1 setFrame:kFooterLine1Landscape];
        [rightBorderLine setFrame:kNoteRightBorderLineLandscape];
        [leftBorderLine setFrame:kNoteLeftBorderLineLandscape];
	}
    
	// Add to the view
	[self.view addSubview:headerLine1];
    [self.view addSubview:footerLine1];
    [self.view addSubview:leftBorderLine];
    [self.view addSubview:rightBorderLine];
}

- (void)loadControlPanel {
    // Create the tools container view
    toolsContainerView = [[UIView alloc] init];
    [toolsContainerView setClipsToBounds:YES];
    
    // Day calendar view
    dayCalendarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DayCalendar.png"]];
    [dayCalendarImageView setFrame:kNoteDayCalendarImageViewFrame];
    
    // Day calendar label
    dayCalendarDateLabel = [[UILabel alloc] initWithFrame:kNoteDayCalendarLabelFrame];
    [dayCalendarDateLabel setText:[MethodHelper localizedDateFrom:[NSDate date] usingStyle:NSDateFormatterLongStyle withYear:YES]];
    [dayCalendarDateLabel setTextColor:[UIColor lightTextColor]];
    [dayCalendarDateLabel setTextAlignment:UITextAlignmentCenter];
    [dayCalendarDateLabel setBackgroundColor:[UIColor clearColor]];
    
    // Load the control panel image
	controlPanelImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPanel.png"]];
    
    // Mini list pad tableview
    miniListPadTableViewResponder = [[MiniListPadTableViewResponder alloc] initWithTaskListCollection:refMasterTaskListCollection andDelegate:self];
	miniListPadTableView = [[UITableView alloc] initWithFrame:kNoteMiniListPadTableViewFrame style:UITableViewStyleGrouped];
	//[listPadTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[miniListPadTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    [miniListPadTableView setContentInset:UIEdgeInsetsMake(0, 0, 33, 0)];
    //[miniListPadTableView setBackgroundColor:[UIColor colorWithRed:20 / 255.0 green:20 / 255.0 blue:20 / 255.0 alpha:1.0]];  // #272324 -- back bit is #141414
    //[listPadTableView setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
    [miniListPadTableView setBackgroundColor:[UIColor clearColor]];
    [miniListPadTableView setShowsVerticalScrollIndicator:NO];
	[miniListPadTableView setDataSource:miniListPadTableViewResponder];
	[miniListPadTableView setDelegate:miniListPadTableViewResponder];
    

    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        [controlPanelImage setFrame:kControlPanelImagePortrait];
        [toolsContainerView setFrame:kNoteToolsContainerViewPortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        [controlPanelImage setFrame:kControlPanelImageLandscape];
        [toolsContainerView setFrame:kNoteToolsContainerViewLandscape];
    }
    
    // Add to subview
	[self.view addSubview:controlPanelImage];
    [self.view addSubview:toolsContainerView];
    [toolsContainerView addSubview:miniListPadTableView];
    [toolsContainerView addSubview:dayCalendarImageView];
    [toolsContainerView addSubview:dayCalendarDateLabel];
    
    // Force select the correct row
    NSInteger theSelectedRow = [refMasterTaskListCollection getIndexOfTaskListWithID:refTaskList.listID];
    [miniListPadTableViewResponder tableView:miniListPadTableView forceSelectRowAtIndexPathRow:theSelectedRow andSection:0];
}

- (void)loadNavigationBarButtons {
	// Replace the left bar button item
	UIBarButtonItem *listsButtonItem = [[UIBarButtonItem alloc] initWithTitle:self.backButtonTitle
																		style:UIBarButtonItemStyleBordered 
																	   target:self action:@selector(listButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:listsButtonItem animated:NO];
	[listsButtonItem release];
    
    // Add a help bar button item
	UIBarButtonItem *helpBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"?" 
																		  style:UIBarButtonItemStyleBordered 
																		 target:self action:@selector(helpBarButtonItemAction)];
	[helpBarButtonItem setStyle:UIBarButtonItemStylePlain];
    [self.navigationItem setRightBarButtonItem:helpBarButtonItem animated:NO];
	[helpBarButtonItem release];
}

- (void)loadTextFields {
	// Load/init related objects
	titleTextField = [[UITextField alloc] initWithFrame:kTitleTextFieldPortrait];
    
    if ([MethodHelper getIOSVersion] < 5.0) {
        [titleTextField setBackgroundColor:[UIColor clearColor]];
    }
	
	[titleTextField setTextAlignment:UITextAlignmentRight];
	[titleTextField setFont:[UIFont fontWithName:@"Helvetica" size:30.0]];
	[titleTextField setTextColor:[UIColor darkTextColor]];
	[titleTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
	[titleTextField setAutocapitalizationType:UITextAutocapitalizationTypeWords];
	[titleTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
	[titleTextField setReturnKeyType:UIReturnKeyDone];
	[titleTextField setDelegate:self];
	[titleTextField setText:refTaskList.title];
	
	// Create the date label
	dateLabelButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	[dateLabelButton setBackgroundColor:[UIColor clearColor]];
	[dateLabelButton setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
	[dateLabelButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
	[dateLabelButton addTarget:self action:@selector(dateLabelButtonAction) forControlEvents:UIControlEventTouchUpInside];
	NSDate *theDate = [MethodHelper dateFromString:refTaskList.creationDate usingFormat:K_DATEONLY_FORMAT];
	[dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES]
					 forState:UIControlStateNormal];
    
	// Orientation related updates
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
		[titleTextField setFrame:kTitleTextFieldPortrait];
		[dateLabelButton setFrame:kDateLabelPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
		[titleTextField setFrame:kTitleTextFieldLandscape];
		[dateLabelButton setFrame:kDateLabelLandscape];
	}
	
	// Add objects to the view
	[self.view addSubview:titleTextField];
	[self.view addSubview:dateLabelButton];
}

- (void)loadButtons {
    // Green Pen button
    greenPenButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [greenPenButton addTarget:self action:@selector(greenPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[greenPenButton setImage:[UIImage imageNamed:@"Green.png"] forState:UIControlStateNormal];
	[greenPenButton setImage:[UIImage imageNamed:@"GreenSelected.png"] forState:UIControlStateSelected];
    
    // Red Pen button
    redPenButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [redPenButton addTarget:self action:@selector(redPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [redPenButton setImage:[UIImage imageNamed:@"Red.png"] forState:UIControlStateNormal];
	[redPenButton setImage:[UIImage imageNamed:@"RedSelected.png"] forState:UIControlStateSelected];
    
    // Blue Pen button
    bluePenButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [bluePenButton addTarget:self action:@selector(bluePenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[bluePenButton setImage:[UIImage imageNamed:@"Blue.png"] forState:UIControlStateNormal];
	[bluePenButton setImage:[UIImage imageNamed:@"BlueSelected.png"] forState:UIControlStateSelected];
    
    // Black Pen button
    blackPenButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [blackPenButton addTarget:self action:@selector(blackPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [blackPenButton setImage:[UIImage imageNamed:@"Black.png"] forState:UIControlStateNormal];
	[blackPenButton setImage:[UIImage imageNamed:@"BlackSelected.png"] forState:UIControlStateSelected];
    
    // Eraser button
    eraserButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [eraserButton addTarget:self action:@selector(eraserButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[eraserButton setImage:[UIImage imageNamed:@"Eraser.png"] forState:UIControlStateNormal];
	[eraserButton setImage:[UIImage imageNamed:@"EraserSelected.png"] forState:UIControlStateSelected];
    
    
    // Clear button
    clearButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [clearButton addTarget:self action:@selector(clearButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[clearButton setShowsTouchWhenHighlighted:YES];
	[clearButton setBackgroundImage:[UIImage imageNamed:@"ClearDrawPadButton.png"] forState:UIControlStateNormal];
    
    // Undo button
    undoButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [undoButton addTarget:self action:@selector(undoButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[undoButton setShowsTouchWhenHighlighted:YES];
	[undoButton setBackgroundImage:[UIImage imageNamed:@"UndoButton.png"] forState:UIControlStateNormal];
    
    // Redo button
    redoButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [redoButton addTarget:self action:@selector(redoButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[redoButton setShowsTouchWhenHighlighted:YES];
	[redoButton setBackgroundImage:[UIImage imageNamed:@"RedoButton.png"] forState:UIControlStateNormal];
    
    // Line widths button
    lineWidthsButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [lineWidthsButton addTarget:self action:@selector(lineWidthsButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[lineWidthsButton setShowsTouchWhenHighlighted:YES];
	[lineWidthsButton setBackgroundImage:[UIImage imageNamed:@"LineWidthThreeButton.png"] forState:UIControlStateNormal];
    
    // Orientation settings (useless here as this is called in init method)
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        // Pens
		[greenPenButton setFrame:kNoteGreenPenButtonPortrait];
        [redPenButton setFrame:kNoteRedPenButtonPortrait];
        [bluePenButton setFrame:kNoteBluePenButtonPortrait];
        [blackPenButton setFrame:kNoteBlackPenButtonPortrait];
        
         // Eraser and Clear buttons
		[eraserButton setFrame:kNoteEraserButtonPortrait];
		[clearButton setFrame:kNoteClearButtonPortrait];
        
        // Undo/redo buttons
		[undoButton setFrame:kNoteUndoButtonPortrait];
		[redoButton setFrame:kNoteRedoButtonPortrait];
        
        [lineWidthsButton setFrame:kLineWidthsButtonPortrait];
	} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        // Pens
		[greenPenButton setFrame:kNoteGreenPenButtonLandscape];
        [redPenButton setFrame:kNoteRedPenButtonLandscape];
        [bluePenButton setFrame:kNoteBluePenButtonLandscape];
        [blackPenButton setFrame:kNoteBlackPenButtonLandscape];
        
        // Eraser and Clear buttons
		[eraserButton setFrame:kNoteEraserButtonLandscape];
		[clearButton setFrame:kNoteClearButtonLandscape];
        
        // Undo/redo buttons
		[undoButton setFrame:kNoteUndoButtonLandscape];
		[redoButton setFrame:kNoteRedoButtonLandscape];
        [lineWidthsButton setFrame:kLineWidthsButtonLandscape];
	}
	
	// Add to subview
	[self.view addSubview:greenPenButton];
    [self.view addSubview:redPenButton];
    [self.view addSubview:bluePenButton];
    [self.view addSubview:blackPenButton];
    
	[self.view addSubview:eraserButton];
	[self.view addSubview:clearButton];
	[self.view addSubview:undoButton];
	[self.view addSubview:redoButton];
    [self.view addSubview:lineWidthsButton];
}

- (void)loadScrollView {
    drawingLayerScrollView = [[UIScrollView alloc] init];
    [drawingLayerScrollView setContentSize:CGSizeMake(634, 705)];
    [drawingLayerScrollView setBackgroundColor:[UIColor clearColor]];
    
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        [drawingLayerScrollView setFrame:kListTableViewPortrait];
    } else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        [drawingLayerScrollView setFrame:kListTableViewLandscape];
    }
    
    [drawingLayerScrollView setScrollEnabled:FALSE];
    
               
    [self.view addSubview:drawingLayerScrollView];
}

- (void)loadDrawingLayer { 
    if (drawingLayer == nil) {
        drawingLayer = [GLDrawES2ViewController alloc];
        [drawingLayer initialFrame:CGRectMake(0, 0, 634, 705)];
        //[drawingLayer setDelegate:self];
        [drawingLayerScrollView addSubview:drawingLayer.view];
        
        [drawingLayer setScale:1.0f];
        //[drawingLayer drawFrame];
        
        // Load the gl scribble dictionary
        NSString *fileSource = [[MethodHelper getScribblePath] stringByAppendingPathComponent:refTaskList.scribble];
        NSData *glScribbleData = [[NSData alloc] initWithContentsOfFile:fileSource];
        
        self.glScribbleDictionary = [MethodHelper decodeObject:glScribbleData andKey:@"GLScribbleData"];
        [glScribbleData release];
        
        [self performSelector:@selector(showDrawingFrame) withObject:nil afterDelay:0.0000001];
    }
}

- (void)showDrawingFrame {
    [drawingLayer setDrawingData:self.glScribbleDictionary];
    [drawingLayer drawFrame];
}

- (void)loadScrollControlWindow {
    
    scrollControlWindow = [[UIImageView alloc] initWithFrame:kNoteScrollControllWindowFrame];
    [scrollControlWindow setImage:[UIImage imageNamed:@"VerticalScrollControlWindow.png"]];
    [self.view addSubview:scrollControlWindow];
    
    
    scrollControl = [[UIImageView alloc] initWithFrame:_NoteScrollControlFrame];
    [scrollControl setImage:[UIImage imageNamed:@"VerticalScrollControl.png"]];
    
    
    
    //scrollControl = [[UIView alloc] initWithFrame:_ScrollControlFrame];
    //[scrollControl setBackgroundColor:[UIColor whiteColor]];
    //scrollControl.layer.borderColor = [UIColor grayColor].CGColor;
    //scrollControl.layer.borderWidth = 3.0f;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"VerticalMoveLines.png"]];
    [imageView setFrame:CGRectMake(6, 28, 23, 23)];
    [scrollControl addSubview:imageView];
    [imageView release];
    [self.view addSubview:scrollControl];
    
    
   /* scrollControl = [[UIView alloc] initWithFrame:_NoteScrollControlFrame];
    [scrollControl setBackgroundColor:[UIColor whiteColor]];
    scrollControl.layer.borderColor = [UIColor grayColor].CGColor;
    scrollControl.layer.borderWidth = 3.0f;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"HorizontalMoveLines.png"]];
    [imageView setFrame:CGRectMake(6, 28, 23, 23)];
    [scrollControl addSubview:imageView];
    [imageView release];
    [self.view addSubview:scrollControl];*/
    
    scrollControlDragging = FALSE;
}

#pragma mark - Button Actions

- (void)helpBarButtonItemAction {
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
		return;
	}
	
	// Show the help overlay
	isHelpSelected = TRUE;
	
	UIButton *helpOverlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[helpOverlayButton setAlpha:0.75f];
	helpOverlayButton.tag = kHelpOverlayTag;
	[helpOverlayButton addTarget:self action:@selector(helpOverlayButtonAction:) forControlEvents:UIControlEventTouchDown];
	
	
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        [helpOverlayButton setImage:[UIImage imageNamed:@"NOTE-SCREEN-PORTRAIT-OVERLAY.png"] forState:UIControlStateNormal];
        [helpOverlayButton setFrame:CGRectMake(0, 0, 768, 960)];
		
	} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        [helpOverlayButton setImage:[UIImage imageNamed:@"NOTE-SCREEN-LANDSCAPE-OVERLAY.png"] forState:UIControlStateNormal];
        [helpOverlayButton setFrame:CGRectMake(0, 0, 1024, 704)];
		
	}
	
	[self.view addSubview:helpOverlayButton];
}

- (void)helpOverlayButtonAction:(id)sender {
	isHelpSelected = FALSE;
	UIButton *helpOverlayButton = (UIButton *)sender;
	[helpOverlayButton removeFromSuperview];
}

- (void)greenPenButtonAction {
	[drawingLayer setColour:[UIColor colorWithRed:0.0 green:0.411764 blue:0.21568627 alpha:1.0]];
	
    // Deselect all
	[self deselectAll];
    
	// Select this pen
	[greenPenButton setSelected:YES];
}

- (void)redPenButtonAction {
    [drawingLayer setColour:[UIColor colorWithRed:0.8274509 green:0.0235294 blue:0.0 alpha:1.0]];
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[redPenButton setSelected:YES];
}

- (void)bluePenButtonAction {
    [drawingLayer setColour:[UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:1.0]];
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[bluePenButton setSelected:YES];
}

- (void)blackPenButtonAction {
    [drawingLayer setColour:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]];
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[blackPenButton setSelected:YES];
}

- (void)listButtonItemAction {
    
    // Goes back to the main screen
    // Need to save the drawing
    if ([titleTextField isFirstResponder]) {
		[titleTextField resignFirstResponder];
	}
    
    // Hide any objects that need to be hidden
	[titleTextField setHidden:YES];
	[dateLabelButton setHidden:YES];
	[headerLine1 setHidden:YES];
    [rightBorderLine setHidden:YES];
    [leftBorderLine setHidden:YES];
	[footerLine1 setHidden:YES];
    
    [wristPadView setHidden:YES];
    [wristPadDragView setHidden:YES];
    
    [scrollControl setHidden:YES];
    [scrollControlWindow setHidden:YES];
    
    [greenPenButton setHidden:YES];
    [bluePenButton setHidden:YES];
    [blackPenButton setHidden:YES];
    [redPenButton setHidden:YES];
    [eraserButton setHidden:YES];
    [clearButton setHidden:YES];
    [undoButton setHidden:YES];
    [redoButton setHidden:YES];
    [lineWidthsButton setHidden:YES];
    
    [drawingLayer.view setHidden:YES];
    
    // Saving the drawing data
    //[refTaskList.glScribbleDictionary release];
    //if (refTaskList.glScribbleDictionary != nil) {
    //    [refTaskList.glScribbleDictionary release];
    //}
    
    [self updateDrawingData];
    
    // Shrink page animation
    [UIView beginAnimations:@"Shrink Page Animation" context:nil];
	[UIView setAnimationDuration:0.5f];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView setAnimationDelegate:self];
	
	if (self.tileViewFrame.size.width > 0) {
		[pageImage setFrame:self.tileViewFrame];
	} else {
		if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
			[pageImage setFrame:CGRectMake(177, 180, 414, 551)];
		} else if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
			[pageImage setFrame:CGRectMake(433, 82, 414, 551)];
		}
	}
	
	[UIView commitAnimations];
    
    // Set requires update
    refTaskList.mainViewRequiresUpdate = TRUE;
}

/*
 - (void)doneButtonAction {
 // Need to save the drawing data
 //[refListItem.glScribbleDictionary release];
 //NSDictionary *newDictionary = [[NSDictionary alloc] initWithDictionary:[drawingLayer drawingData]];
 
 refListItem.glScribbleDictionary = [NSDictionary dictionaryWithDictionary:[drawingLayer drawingData]];
 
 NSString *newScribble;
 
 if ([refListItem.scribble length] > 0) {
 newScribble = refListItem.scribble;
 } else {
 newScribble = [MethodHelper getUniqueFileName];
 newScribble = [newScribble stringByAppendingString:@".gls"];
 refListItem.scribble = newScribble;
 }
 
 NSString *fileDestination = [[MethodHelper getScribblePath] stringByAppendingPathComponent:newScribble];
 //  NSLog(@"%@", fileDestination);
 //  NSLog(@"%@", refListItem.glScribbleDictionary);
 NSData *glScribbleData = [MethodHelper encodeObject:refListItem.glScribbleDictionary withKey:@"GLScribbleData"];
 
 
 if ([glScribbleData writeToFile:fileDestination atomically:YES] == NO) {
 NSLog(@"NSData write failed");
 }
 
 // Update the database
 [refListItem updateDatabase];
 
 
 NSRange glsRange = [newScribble rangeOfString:@".gls"];
 if (glsRange.length == 0) {
 [delegate drawPadUpdatedForListItem:refListItem];
 
 }
 
 NSString *pngScribble = [newScribble substringToIndex:glsRange.location];
 pngScribble = [NSString stringWithFormat:@"%@.png", pngScribble];
 
 // Get the file destination
 NSString *pngFileDestination = [[MethodHelper getScribblePath] stringByAppendingPathComponent:pngScribble];
 
 // Write the image to the database
 UIImage *myImage = [self.drawingLayer getUIImage];
 myImage = [MethodHelper scaleImage:myImage maxWidth:500 maxHeight:44];
 
 [UIImagePNGRepresentation(myImage) writeToFile:pngFileDestination atomically:YES];
 
 [delegate drawPadUpdatedForListItem:refListItem];
 
 }
 */

- (void)dateLabelButtonAction {
    // Popup the date controller
	NewDueDateTableViewController *controller = [[[NewDueDateTableViewController alloc] initWithDueDate:refTaskList.creationDate 
                                                                                            andDelegate:self] autorelease];
	
	// Allocate the pop over controller, that will host the nav controller and view controller
	
	dateLabelPopoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
    
	// Present the popover using the calculated location to display the arrow
	[dateLabelPopoverController presentPopoverFromRect:dateLabelButton.frame inView:self.view 
                              permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
	
}

- (void)eraserButtonAction {
	[drawingLayerScrollView setScrollEnabled:NO];
	
    // Deselect all
	[self deselectAll];
    
    if ([drawingLayer eraserSelected] == FALSE) {
        [drawingLayer eraseToggle];
    }
    
	// Select this pen
	[eraserButton setSelected:YES];    
}

- (void)deselectAll {
	[greenPenButton setSelected:NO];
	[redPenButton setSelected:NO];
	[bluePenButton setSelected:NO];
	[blackPenButton setSelected:NO];
	[eraserButton setSelected:NO];
    
    if ([drawingLayer eraserSelected] == TRUE) {
        [drawingLayer eraseToggle];
    }
}

- (void)undoButtonAction {
    [drawingLayer undo];
}

- (void)redoButtonAction {
    [drawingLayer redo];
}

- (void)lineWidthsButtonAction {
    // Need to show a popover
    // Popup the date controller
	LineWidthsViewController *controller = [[[LineWidthsViewController alloc] initWithMaxWidth:drawingLayer.maxLineWidth] autorelease];
	
	// Allocate the pop over controller, that will host the nav controller and view controller
	[controller setDelegate:self];
    
    if (lineWidthsPopoverController != nil) {
        lineWidthsPopoverController = nil;
        [lineWidthsPopoverController release];
    }
    
	lineWidthsPopoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
    
	// Present the popover using the calculated location to display the arrow
	[lineWidthsPopoverController presentPopoverFromRect:lineWidthsButton.frame inView:self.view 
                              permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

- (void)clearButtonAction {
    [clearActionSheet showFromRect:[clearButton frame] 
                            inView:self.view animated:NO];
}

#pragma mark -
#pragma mark UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	[textField setBorderStyle:UITextBorderStyleRoundedRect];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
		[textField setFrame:kTitleTextFieldEditingPortrait];
	} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
		[textField setFrame:kTitleTextFieldEditingLandscape];
	}
	[UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	[textField setBorderStyle:UITextBorderStyleNone];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
		[textField setFrame:kTitleTextFieldPortrait];
	} else if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
		[textField setFrame:kTitleTextFieldLandscape];
	}
	
	// Update the title
	if ([textField.text length] > 0) {
		NSString *newTitle = [textField text];
		
		if ([newTitle isEqualToString:refTaskList.title] == FALSE) {
			if ([newTitle isEqualToString:@"Toodledo Tasks"] == TRUE || [textField.text isEqualToString:@"Toodledo Tasks"] == TRUE) {
				// Warn user that this is reserved list name
				[MethodHelper showAlertViewWithTitle:@"Reserved Name" 
										  andMessage:@"This note title can not be changed and is reserved for Toodledo tasks with no folder" 
									  andButtonTitle:@"Ok"];
				[textField setText:refTaskList.title];
			} else {
				// Update title
				refTaskList.title = newTitle;
                self.title = newTitle;
			}
			
            
		}
        
        
	} else {
		[textField setText:refTaskList.title];
	}
	
	// Update list completed button
	[UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	// Update list completed button
	//[self updateListCompletedFrameForOrientation:[self interfaceOrientation]];
	
	// This will call text field did end editing
	[textField resignFirstResponder];
	return YES;
}

#pragma mark - UIActionSheetDelegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ([actionSheet isEqual:clearActionSheet]) {
		
		if (buttonIndex == 0) {
			// Clear the page
            [drawingLayer clearView];
		}
	}
}

#pragma mark -
#pragma mark Touches Handling


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	// Enumerate through all the touch objects.
    CGPoint touchPosition = CGPointMake(0, 0);
	for (UITouch *touch in touches) {
        touchPosition = [touch locationInView:self.view];
        
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		//[self dispatchFirstTouchAtPoint:[touch locationInView:self] forEvent:nil];
        
	}	
    
    if (CGRectContainsPoint([scrollControl frame], touchPosition)) {
        // Within bounds
        scrollControlDragging = TRUE;
        
        scrollOffset = touchPosition.y - scrollControl.frame.origin.y;
    } else {
        scrollControlDragging = FALSE;
    }
    
    if (CGRectContainsPoint([wristPadDragView frame], touchPosition)) {
        wristPadDragging = TRUE;
        wristPadOffset = touchPosition.y - wristPadDragView.frame.origin.y;
    } else {
        wristPadDragging = FALSE;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	// If ref touched view not nil, shrink
	for (UITouch *touch in touches) {
        
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		//[self dispatchTouchAtPoint:[touch locationInView:self] forEvent:nil];
	}	
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	[self touchesEnded:touches withEvent:event];
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (scrollControlDragging) {
        // Move it along it's y axis
        CGPoint touchPosition = CGPointZero;
        for (UITouch *touch in touches) {
            touchPosition = [touch locationInView:self.view];
        }	
        
        
        // Move the scroll control up to a maximum, minumum.
        if (touchPosition.y != 0) {
            CGFloat newPosition = touchPosition.y - scrollOffset;
            
            if (newPosition >= (scrollControlWindow.frame.origin.y) && 
                (newPosition + scrollControl.frame.size.height) <= (scrollControlWindow.frame.origin.y + scrollControlWindow.frame.size.height)) {
                [scrollControl setFrame:CGRectMake(scrollControl.frame.origin.x, 
                                                   newPosition, 
                                                   scrollControl.frame.size.width, 
                                                   scrollControl.frame.size.height)];
            } else if (newPosition < scrollControlWindow.frame.origin.y) {
                newPosition = scrollControlWindow.frame.origin.y;
                [scrollControl setFrame:CGRectMake(scrollControl.frame.origin.x, 
                                                   newPosition, 
                                                   scrollControl.frame.size.width, 
                                                   scrollControl.frame.size.height)];
            } else if ((newPosition + scrollControl.frame.size.height) > (scrollControlWindow.frame.origin.y + scrollControlWindow.frame.size.height)) {
                newPosition = (scrollControlWindow.frame.origin.y + scrollControlWindow.frame.size.height) - scrollControl.frame.size.height;
                [scrollControl setFrame:CGRectMake(scrollControl.frame.origin.x, 
                                                   newPosition, 
                                                   scrollControl.frame.size.width, 
                                                   scrollControl.frame.size.height)];
            }
        }
        
        // Now need to adjust the scrollview, the multiple is 5
        CGFloat newScrollViewOffset = (scrollControl.frame.origin.y - (scrollControlWindow.frame.origin.y)) * 5.0f;
        
        [drawingLayerScrollView scrollRectToVisible:CGRectMake(0, newScrollViewOffset, 634, 445) animated:NO];
    }
    
    if (wristPadDragging) {
        // Move it along it's y axis
        CGPoint touchPosition = CGPointZero;
        for (UITouch *touch in touches) {
            touchPosition = [touch locationInView:self.view];
        }	
        
        
        // Move the scroll control up to a maximum, minumum.
        if (touchPosition.y != 0) {
            CGFloat newPosition = touchPosition.y - wristPadOffset;
            
            if (newPosition >= wristPadDragView.frame.origin.y && newPosition <= wristPadStartY) {
                [wristPadDragView setFrame:CGRectMake(wristPadDragView.frame.origin.x, 
                                                      newPosition, 
                                                      wristPadDragView.frame.size.width, 
                                                      wristPadDragView.frame.size.height)];
                [wristPadView setFrame:CGRectMake(wristPadView.frame.origin.x, 
                                                  newPosition, 
                                                  wristPadView.frame.size.width, 
                                                  wristPadStartY - newPosition)];
            } else if (newPosition < wristPadDragView.frame.origin.y && newPosition > 200) {
                [wristPadDragView setFrame:CGRectMake(wristPadDragView.frame.origin.x, 
                                                      newPosition, 
                                                      wristPadDragView.frame.size.width, 
                                                      wristPadDragView.frame.size.height)];
                [wristPadView setFrame:CGRectMake(wristPadView.frame.origin.x, 
                                                  newPosition, 
                                                  wristPadView.frame.size.width, 
                                                  wristPadStartY - newPosition)];
            } else if (newPosition >= wristPadStartY) {
                [wristPadDragView setFrame:CGRectMake(wristPadDragView.frame.origin.x, 
                                                      wristPadStartY, 
                                                      wristPadDragView.frame.size.width, 
                                                      wristPadDragView.frame.size.height)];
                [wristPadView setFrame:CGRectMake(wristPadView.frame.origin.x, 
                                                  wristPadStartY, 
                                                  wristPadView.frame.size.width, 
                                                  0)];
            }
            
            
           /* if (newPosition >= (scrollControlWindow.frame.origin.y) && 
                (newPosition + scrollControl.frame.size.height) <= (scrollControlWindow.frame.origin.y + scrollControlWindow.frame.size.height)) {
                [scrollControl setFrame:CGRectMake(scrollControl.frame.origin.x, 
                                                   newPosition, 
                                                   scrollControl.frame.size.width, 
                                                   scrollControl.frame.size.height)];
            } else if (newPosition < scrollControlWindow.frame.origin.y) {
                newPosition = scrollControlWindow.frame.origin.y;
                [scrollControl setFrame:CGRectMake(scrollControl.frame.origin.x, 
                                                   newPosition, 
                                                   scrollControl.frame.size.width, 
                                                   scrollControl.frame.size.height)];
            } else if ((newPosition + scrollControl.frame.size.height) > (scrollControlWindow.frame.origin.y + scrollControlWindow.frame.size.height)) {
                newPosition = (scrollControlWindow.frame.origin.y + scrollControlWindow.frame.size.height) - scrollControl.frame.size.height;
                [scrollControl setFrame:CGRectMake(scrollControl.frame.origin.x, 
                                                   newPosition, 
                                                   scrollControl.frame.size.width, 
                                                   scrollControl.frame.size.height)];
            }*/
        }
        
  
    }
}

#pragma mark -
#pragma mark Update Collections


- (void)reloadCollections {
	// Collections:		refTaskListCollection
	//					refTaskList
	//					filterTagsTaskListCollection
	//					itemsDueCollection
	
	// RefTaskList should be updated automatically when refTaskListCollection changes (and vice-versa)
    
	// Keep the last sort type
	//NSInteger lastSortType = refTaskList.lastSortType;
    
   // [refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
    
	//refTaskList = [refTaskListCollection getTaskListWithID:currentTaskListID];
	//refTaskList.lastSortType = lastSortType;
    
	// Reload items due table
	//[itemsDueTableViewResponder reloadDataWithListParentTitle:refTaskList.title andTableView:itemsDueTableView
	//										  andParentListID:refTaskList.listID andListItem:nil];
    
	//[filterTagsTaskListCollection filterListsUsingListItemTagCollection:filterListItemTagCollection 
	//									  andOriginalTaskListCollection:refTaskListCollection];
    
    // Reload the calendar view
    //[miniCalendarView reloadData];
    
    // Reload the list pad
    //[miniListPadTableView reloadData];
}

#pragma mark - UIViewControllerDelegate Methods

- (void)applicationDidBecomeActive {
    if ([self.navigationController.topViewController isEqual:self] == TRUE) {
        if (gRunToodledoAutoSync == TRUE) {
            [MethodHelper showAlertViewWithTitle:@"Auto Sync" 
                                      andMessage:@"Manage wants to sync with Toodledo.  Please return to 'My Lists' to automatically run the sync. (Note: This setting can be changed in 'Sync Settings')" 
                                  andButtonTitle:@"Ok"];
        }
    }
}

#pragma mark -
#pragma mark UIAnimation Delegate Methods

// Animation has stopped
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
	if ([animationID isEqualToString:@"Shrink Page Animation"]) {
		[self.navigationController popViewControllerAnimated:NO];
	} 
	

	
    if ([animationID isEqualToString:@"Page Curl Animation"]) {
        // Send page image to back
        [self.view sendSubviewToBack:pageImage];
        [self.view sendSubviewToBack:canvassImage];
        [miniListPadTableView setUserInteractionEnabled:TRUE];
    }
    
    if ([animationID isEqualToString:@"Page to List Curl Animation"]) {
        // Swapping with note
        [self.delegate swapWithList:refTaskList usingFrame:self.tileViewFrame];
        
        [miniListPadTableView setUserInteractionEnabled:TRUE];
    }
    
}

#pragma mark -
#pragma mark NewListItemDueDateDelegate
// Note: Technically this is truly 'NewListItemDueDateDelegate' but we are reusing it for list date

- (void)updateDueDate:(NSString *)newDueDate {
	// Update the task list date
	refTaskList.creationDate = newDueDate;
	[refTaskList updateDatabase];
	
	// We are going to update the list date with this date
	NSDate *theDate = [MethodHelper dateFromString:newDueDate usingFormat:K_DATEONLY_FORMAT];
	[dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES]
					 forState:UIControlStateNormal];
    
	// Dismiss the date label popover
	if ([dateLabelPopoverController isPopoverVisible]) {
		[dateLabelPopoverController dismissPopoverAnimated:YES];
        [dateLabelPopoverController release];
	}
	
    // Reload the calendar view
    //[miniCalendarView reloadData];
    
    // Reload the list pad
    //[miniListPadTableView reloadData];
}

#pragma mark - LineWidthsDelegate

- (void)lineWidthUpdatedWithMinSize:(CGFloat)minSize andMaxSize:(CGFloat)maxSize {
    [drawingLayer setMinLineWidth:minSize];
    [drawingLayer setMaxLineWidth:maxSize];
    
    switch ((int)drawingLayer.minLineWidth) {
        case 1:
            drawingLayer.strokeStep = 0.65f;
            [lineWidthsButton setImage:[UIImage imageNamed:@"LineWidthThreeButton.png"] forState:UIControlStateNormal];
            break;
        case 3:
            drawingLayer.strokeStep = 0.55f;
            [lineWidthsButton setImage:[UIImage imageNamed:@"LineWidthFiveButton.png"] forState:UIControlStateNormal];
            break;
        case 5:
            drawingLayer.strokeStep = 0.45f;
            [lineWidthsButton setImage:[UIImage imageNamed:@"LineWidthSevenButton.png"] forState:UIControlStateNormal];
            break;
        case 7:
            drawingLayer.strokeStep = 0.35f;
            [lineWidthsButton setImage:[UIImage imageNamed:@"LineWidthNineButton.png"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    
    [drawingLayer changeWidth];
    
    if ([lineWidthsPopoverController isPopoverVisible]) {
        [lineWidthsPopoverController dismissPopoverAnimated:YES];
    }
}

#pragma mark - ListPadDelegate

- (void)listSelected:(NSInteger)theSelectedRow {
    if (theSelectedRow < 0 || theSelectedRow >= [refMasterTaskListCollection.lists count]) {
        return;
    }
    [miniListPadTableView setUserInteractionEnabled:FALSE];
    
    [self updateDrawingData];
    
    // Bring the page image to front
    [self.view bringSubviewToFront:pageImage];
    [self.view bringSubviewToFront:borderImage];
    [self.view bringSubviewToFront:controlPanelImage];
    [self.view bringSubviewToFront:toolsContainerView];

    
    // Assign the task list to local reference object
    refTaskList = [refMasterTaskListCollection.lists objectAtIndex:theSelectedRow];
    //currentTaskListID = refTaskList.listID;
    
    // Need to jump out here if this is not a note
    if ([refTaskList isNote] == FALSE) {
        [scrollControlWindow setHidden:YES];
        [scrollControl setHidden:YES];
        
        [UIView beginAnimations:@"Page to List Curl Animation" context:nil];
        [UIView setAnimationDuration:1.0f];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:pageImage cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
        [UIView commitAnimations];
        return;
    }
    
    // Assign the current task list id to the master task list collection
    refMasterTaskListCollection.currentTaskListID = refTaskList.listID;
    
    // Set title
    self.title = refTaskList.title;
    
    // Update title text field, date and list completed
    [titleTextField setText:refTaskList.title];
    NSDate *theDate = [MethodHelper dateFromString:refTaskList.creationDate usingFormat:K_DATEONLY_FORMAT];
    [dateLabelButton setTitle:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES] forState:UIControlStateNormal];

    
    [UIView beginAnimations:@"Page Curl Animation" context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:pageImage cache:YES];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView commitAnimations];
    

}

- (void)notebookSelected:(NSInteger)theSelectedRow {
    NSLog(@"Notebook selected: %d", theSelectedRow);
}

- (void)loadingListsSelected {

}

- (void)archivesSelected {

}

- (void)shopfrontSelectedWithFrame:(CGRect)theFrame {

}

- (void)deleteRowsAtIndexPaths:(NSArray *)indexPaths {

}

- (void)insertRowsAtIndexPaths:(NSArray *)indexPaths {
    NSLog(@"Insert rows at index path");
}

- (void)reloadListPadTableView {
    NSLog(@"Reload list pad table view");
}



#pragma mark -
#pragma mark UIView Controller Delegates

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
	if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        // Update our tileviewframe
        CGRect frame = self.tileViewFrame;
        NSString *barButtonItemTitle = self.navigationItem.leftBarButtonItem.title;
        if ([barButtonItemTitle isEqualToString:@"My Folders"] == FALSE) {
            self.tileViewFrame = CGRectMake(frame.origin.x - 256, frame.origin.y, frame.size.width, frame.size.height);
        } else if ([barButtonItemTitle isEqualToString:@"My Folders"]) {
            self.tileViewFrame = CGRectMake(frame.origin.x - 256, frame.origin.y + 98, frame.size.width, frame.size.height);
        }
        
        // Update the control panel
        [controlPanelImage setFrame:kControlPanelImagePortrait];
        [toolsContainerView setFrame:kNoteToolsContainerViewPortrait];
        
		// Update the background
		[canvassImage setFrame:kCanvassImagePortrait];
		[borderImage setFrame:kBorderImagePortrait];
		[pageImage setFrame:kPageImagePortrait];

		// Update the header and footer positions
		[headerLine1 setFrame:kHeaderLine1Portrait];
		[footerLine1 setFrame:kFooterLine1Portrait];
        [leftBorderLine setFrame:kNoteLeftBorderLinePortrait];
        [rightBorderLine setFrame:kNoteRightBorderLinePortrait];
        
		// Update the button positions
		// Pens
		[greenPenButton setFrame:kNoteGreenPenButtonPortrait];
        [redPenButton setFrame:kNoteRedPenButtonPortrait];
        [bluePenButton setFrame:kNoteBluePenButtonPortrait];
        [blackPenButton setFrame:kNoteBlackPenButtonPortrait];
        
        [eraserButton setFrame:kNoteEraserButtonPortrait];
        [clearButton setFrame:kNoteClearButtonPortrait];
        [undoButton setFrame:kNoteUndoButtonPortrait];
        [redoButton setFrame:kNoteRedoButtonPortrait];
        [lineWidthsButton setFrame:kLineWidthsButtonPortrait];
        
        // Scroll view
        [drawingLayerScrollView setFrame:kListTableViewPortrait];
        
		// Update title and date elements
		if ([titleTextField isEditing]) {
			[titleTextField setFrame:kTitleTextFieldEditingPortrait];
		} else {
			[titleTextField setFrame:kTitleTextFieldPortrait];
		}
		[dateLabelButton setFrame:kDateLabelPortrait];
        
        wristPadStartY = 790;
        
        
        if (wristPadView.frame.size.height == 0) {
            [wristPadDragView setFrame:kWristPadDragViewPortrait];
            [wristPadView setFrame:kWristPadViewPortrait];
        } else {
            [wristPadDragView setFrame:CGRectMake(wristPadDragView.frame.origin.x - 256, 
                                                  wristPadDragView.frame.origin.y + 260, 
                                                  wristPadDragView.frame.size.width, 
                                                  wristPadDragView.frame.size.height)];
            [wristPadView setFrame:CGRectMake(wristPadView.frame.origin.x - 256, 
                                              wristPadView.frame.origin.y + 260, 
                                              wristPadView.frame.size.width, 
                                              wristPadView.frame.size.height)];
        }
        
            
        
	} else if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
        // Update our tileviewframe
        CGRect frame = self.tileViewFrame;
        NSString *barButtonItemTitle = self.navigationItem.leftBarButtonItem.title;
        if ([barButtonItemTitle isEqualToString:@"My Folders"] == FALSE) {
            self.tileViewFrame = CGRectMake(frame.origin.x + 256, frame.origin.y, frame.size.width, frame.size.height);
        } else if ([barButtonItemTitle isEqualToString:@"My Folders"]) {
            self.tileViewFrame = CGRectMake(frame.origin.x + 256, frame.origin.y - 98, frame.size.width, frame.size.height);
        }
        // Update the control panel
        [controlPanelImage setFrame:kControlPanelImageLandscape];
        [toolsContainerView setFrame:kNoteToolsContainerViewLandscape];
        
		// Update the background
		[canvassImage setFrame:kCanvassImageLandscape];
		[borderImage setFrame:kBorderImageLandscape];
		[pageImage setFrame:kPageImageLandscape];
		
        // Update the button positions
        // Pens
		[greenPenButton setFrame:kNoteGreenPenButtonLandscape];
        [redPenButton setFrame:kNoteRedPenButtonLandscape];
        [bluePenButton setFrame:kNoteBluePenButtonLandscape];
        [blackPenButton setFrame:kNoteBlackPenButtonLandscape];
        
        [eraserButton setFrame:kNoteEraserButtonLandscape];
        [clearButton setFrame:kNoteClearButtonLandscape];
        [undoButton setFrame:kNoteUndoButtonLandscape];
        [redoButton setFrame:kNoteRedoButtonLandscape];
        [lineWidthsButton setFrame:kLineWidthsButtonLandscape];
        
		// Update the header and footers position
		[headerLine1 setFrame:kHeaderLine1Landscape];
        [footerLine1 setFrame:kFooterLine1Landscape];
        [leftBorderLine setFrame:kNoteLeftBorderLineLandscape];
        [rightBorderLine setFrame:kNoteRightBorderLineLandscape];
		
        // Scroll view
        [drawingLayerScrollView setFrame:kListTableViewLandscape];
        
		// Update title and date elements
		if ([titleTextField isEditing]) {
			[titleTextField setFrame:kTitleTextFieldEditingLandscape];
		} else {
			[titleTextField setFrame:kTitleTextFieldLandscape];
		}
		[dateLabelButton setFrame:kDateLabelLandscape];
        
        // Now need to adjust the scrollview, the multiple is 5
        CGFloat newScrollViewOffset = (scrollControl.frame.origin.y - (scrollControlWindow.frame.origin.y)) * 5.0f;
        
        [drawingLayerScrollView scrollRectToVisible:CGRectMake(0, newScrollViewOffset, 634, 445) animated:NO];
        
        
        wristPadStartY = 530;
        if (wristPadView.frame.size.height == 0) {
            [wristPadDragView setFrame:kWristPadDragViewLandscape];
            [wristPadView setFrame:kWristPadViewLandscape];
        } else {
            CGFloat modifiedYPosition = wristPadView.frame.origin.y;
            CGFloat heightModification = 0;
            
            if (modifiedYPosition < wristPadStartY) {
                heightModification = wristPadStartY - modifiedYPosition;
                modifiedYPosition = wristPadStartY;
            }
            
            
            [wristPadDragView setFrame:CGRectMake(wristPadDragView.frame.origin.x + 256, 
                                                  modifiedYPosition - 260, 
                                                  wristPadDragView.frame.size.width, 
                                                  wristPadDragView.frame.size.height)];
            [wristPadView setFrame:CGRectMake(wristPadView.frame.origin.x + 256, 
                                                  modifiedYPosition - 260, 
                                                  wristPadView.frame.size.width, 
                                                  wristPadView.frame.size.height - heightModification)];
        }
            
	}
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	// Get rid of help overlay if it is there
	if (isHelpSelected) {
		UIButton *helpButton = (UIButton *)[self.view viewWithTag:kHelpOverlayTag];
		if (helpButton != nil) {
			[helpButton removeFromSuperview];
		}
		isHelpSelected = FALSE;
	}
	
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {

    if ([lineWidthsPopoverController isPopoverVisible]) {
        // Present the popover using the calculated location to display the arrow
        [lineWidthsPopoverController presentPopoverFromRect:lineWidthsButton.frame inView:self.view 
                                   permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
    
    if ([clearActionSheet isVisible]) {
        [clearActionSheet dismissWithClickedButtonIndex:-1 animated:YES];
    }
    

}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark - Class Methods

- (void)updateDrawingData {
    self.glScribbleDictionary = [NSDictionary dictionaryWithDictionary:[drawingLayer drawingData]];
    
    NSString *newScribble;
    
    if ([refTaskList.scribble length] > 0) {
        newScribble = refTaskList.scribble;
    } else {
        newScribble = [MethodHelper getUniqueFileName];
        newScribble = [newScribble stringByAppendingString:@".gls"];
        refTaskList.scribble = newScribble;
    }
    
	NSString *fileDestination = [[MethodHelper getScribblePath] stringByAppendingPathComponent:newScribble];
    NSData *glScribbleData = [MethodHelper encodeObject:self.glScribbleDictionary withKey:@"GLScribbleData"];
    if ([glScribbleData writeToFile:fileDestination atomically:YES] == NO) {
        NSLog(@"NSData write failed");
    }
    [refTaskList updateDatabase];
    
    // Save the scribble at the size we want to file destination
    NSRange glsRange = [newScribble rangeOfString:@".gls"];
    if (glsRange.length > 0) {
        NSString *pngScribble = [newScribble substringToIndex:glsRange.location];
        pngScribble = [NSString stringWithFormat:@"%@.png", pngScribble];
        
        // Get the file destination
        NSString *pngFileDestination = [[MethodHelper getScribblePath] stringByAppendingPathComponent:pngScribble];
        
        // Write the image to the database
        UIImage *myImage = [self.drawingLayer getUIImage];
        myImage = [MethodHelper scaleImage:myImage maxWidth:380 maxHeight:423];
        
        [UIImagePNGRepresentation(myImage) writeToFile:pngFileDestination atomically:YES];
        
    }
    
    for (TaskList *masterTaskList in refMasterTaskListCollection.lists) {
        if (masterTaskList.listID == refTaskList.listID) {
            //if (masterTaskList.glScribbleDictionary != nil) {
            //  [masterTaskList.glScribbleDictionary release];
            //}
            
            masterTaskList.scribble = newScribble;
            break;
        }
    }
}


@end
