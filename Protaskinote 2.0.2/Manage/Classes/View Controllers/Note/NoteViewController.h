//
//  NoteViewController.h
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 13/09/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Globals
#import "Globals.h"

// GLDraw ES2 View Controller
#import "GLDrawES2ViewController.h"

// Pop Over Controllers (with required delegate link)
#import "NewListItemViewController.h"
#import "LineWidthsViewController.h"

// Responders (with delegate)
#import "MiniListPadTableViewResponder.h"

@class TaskListCollection;
@class TaskList;

@protocol NoteViewDelegate
- (void)swapWithList:(TaskList *)listTaskList usingFrame:(CGRect)frame;
@end

@interface NoteViewController : UIViewController < ListPadDelegate, LineWidthsDelegate, NewListItemDueDateDelegate, UIPopoverControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate> {
    id <NoteViewDelegate>           delegate;
    
    // Backgrounds & Images
	UIImageView						*canvassImage;
	UIImageView						*borderImage;
	UIImageView						*pageImage;
    UIView							*headerLine1;
    UIView                          *rightBorderLine;
    UIView                          *leftBorderLine;
    UIView                          *footerLine1;
    
    // Control panel objects
    MiniListPadTableViewResponder   *miniListPadTableViewResponder;         // The responder for the list pad table view
	UITableView                     *miniListPadTableView;					// The list pad table view that sits on list pad image view
    UIImageView						*controlPanelImage;
    UIView                          *toolsContainerView;
    UIImageView                     *dayCalendarImageView;
    UILabel                         *dayCalendarDateLabel;
    
    // Scroll View
    UIScrollView                    *drawingLayerScrollView;
    
    // Title, completed and date field
	UITextField						*titleTextField;
	UIButton						*dateLabelButton;
    
    // Pen Buttons
	UIButton						*greenPenButton;
    UIButton                        *redPenButton;
    UIButton                        *bluePenButton;
    UIButton                        *blackPenButton;
    UIButton                        *eraserButton;
    UIButton                        *clearButton;
    UIButton                        *undoButton;
    UIButton                        *redoButton;
    UIActionSheet                   *clearActionSheet;
    UIButton                        *lineWidthsButton;
    
    // Popover Controllers
    UIPopoverController				*dateLabelPopoverController;
    UIPopoverController             *lineWidthsPopoverController;
    
    // Ref Objects
    TaskListCollection              *refMasterTaskListCollection;
    TaskList                        *refTaskList;
    
    // Note view controller object properties
    NSString                        *backButtonTitle;
    
    // Drawing Layer
    GLDrawES2ViewController         *drawingLayer;
    NSDictionary                    *dataStorage;
    NSDictionary                    *glScribbleDictionary;
    
    // Need a tile view frame for shrinking (if needed), normal shrink if this is cgrectzero
	CGRect							tileViewFrame;
    
    // Sub scrolling control view
    UIImageView                  *scrollControlWindow;
    UIImageView                  *scrollControl;
    BOOL                    scrollControlDragging;
    CGFloat                 scrollOffset;
    
    BOOL							isHelpSelected;		// For covering when help overlay is on or off
    
    // Wrist pad
    UIView                      *wristPadView;
    UIView                      *wristPadDragView;
    BOOL                        wristPadDragging;
    CGFloat                     wristPadOffset;
    CGFloat                     wristPadStartY;
}

@property (nonatomic, assign)   id                          delegate;
@property (nonatomic, assign)	CGRect                      tileViewFrame;
@property (nonatomic, copy)     NSString                    *backButtonTitle;
@property (nonatomic, retain)   GLDrawES2ViewController     *drawingLayer;
@property (retain)              NSDictionary                *dataStorage;
@property (nonatomic, retain)   NSDictionary                *glScribbleDictionary;

#pragma mark Initialisation
- (id)initWithMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection andTaskList:(TaskList *)theTaskList andBackButtonTitle:(NSString *)theBackButtonTitle;
- (void)updateDrawingData;

@end
