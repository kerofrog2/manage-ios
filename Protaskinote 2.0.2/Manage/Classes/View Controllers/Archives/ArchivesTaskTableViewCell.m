//
//  ArchivesTaskTableViewCell.m
//  Manage
//
//  Created by Cliff Viegas on 3/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ArchivesTaskTableViewCell.h"

@implementation ArchivesTaskTableViewCell

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
		self.tagsView = [[[UIView alloc] initWithFrame:kTagsViewFrame] autorelease];
		[self.tagsView setBackgroundColor:[UIColor colorWithRed:242.0 / 255.0 
														  green:242.0 / 255.0 
														   blue:242.0 / 255.0 
														  alpha:1.0]];
		
		self.taskCompleted = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
		[self.taskCompleted setFrame:kTaskCompletedFrame];
		//[self.taskCompleted addTarget:self action:@selector(taskCompletedAction) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:self.taskCompleted];
		//[self.taskCompleted release];
		
		// Load the notes image
		self.cellNotesImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)] autorelease];
		self.cellNotesImageView.image = nil;
		
		// Init the highlighter views
		self.highlighterView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
		self.leftHighlighterImageView = [[[UIImageView alloc] initWithFrame:CGRectZero] autorelease];
		self.leftHighlighterImageView.image = nil;
		self.rightHighlighterImageView = [[[UIImageView alloc] initWithFrame:CGRectZero] autorelease];
		self.rightHighlighterImageView.image = nil;
		
		// Init the scribble image view
		self.scribbleImageView = [[[UIImageView alloc] initWithFrame:kListItemScribbleRect] autorelease];
		[self.scribbleImageView setContentMode:UIViewContentModeLeft];
		self.scribbleImageView.image = nil;
		
		// Init the tags font
		self.tagsFont = [UIFont fontWithName:@"Helvetica" size:11.0];
		
		// Set task item and date frame
		taskItemFrame = kTaskItemFrame;
		taskDateFrame = kTaskDateFrame;
		
		// Set is preview
		self.isPreview = FALSE;
		self.isNonListSort = FALSE;
		self.hasTags = FALSE;
    }
    return self;
}


@end
