//
//  DateRangeViewController.m
//  Manage
//
//  Created by Cliff Viegas on 8/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "DateRangeViewController.h"

// Method Helper
#import "MethodHelper.h"

// Constants
const CGRect kTableViewFrame = {0, 0, 320, 318};
const CGSize kPopoverSize = {320, 318};

@implementation DateRangeViewController

@synthesize startDate;
@synthesize endDate;
@synthesize delegate;

#pragma mark - Memory Management

- (void)dealloc {
    //[self.startDate release];
    //[self.endDate release];
    [super dealloc];
}

#pragma mark - Initialisation

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (id)initWithStartDate:(NSString *)theStartDate andEndDate:(NSString *)theEndDate {
    self = [super init];
    if (self) {
        self.startDate = theStartDate;
        self.endDate = theEndDate;
        
        // Set content size for view in popover
		[self setContentSizeForViewInPopover:kPopoverSize];
        
        self.title = @"Date Range";
        
        // Create a clear bar button item
		UIBarButtonItem *clearBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered 
																			  target:self action:@selector(clearBarButtonItemAction)];
		[self.navigationItem setRightBarButtonItem:clearBarButtonItem animated:NO];
		[clearBarButtonItem release];
        
        [self loadTableView];
    }
    return self;
}

#pragma mark - Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark - Action Handlers

- (void)clearBarButtonItemAction {
    self.startDate = @"";
    self.endDate = @"";
    [self.delegate dateRangeSelectedWithStartDate:self.startDate endDate:self.endDate];
    [myTableView reloadData];
}

#pragma mark - Date Range Delegates

- (void)updateStartDate:(NSString *)newStartDate {
    self.startDate = newStartDate;
    [myTableView reloadData];
    [self.delegate dateRangeSelectedWithStartDate:self.startDate endDate:self.endDate];
}


- (void)updateEndDate:(NSString *)newEndDate {
    self.endDate = newEndDate;
    [myTableView reloadData];
    [self.delegate dateRangeSelectedWithStartDate:self.startDate endDate:self.endDate];
}

#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
	}	
    
	// Set default values
    cell.textLabel.text = @"";
    cell.detailTextLabel.text = @"";
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Start Date";
            NSDate *theStartDate = [MethodHelper dateFromString:self.startDate usingFormat:K_DATEONLY_FORMAT];
            cell.detailTextLabel.text = [MethodHelper localizedDateFrom:theStartDate usingStyle:NSDateFormatterMediumStyle withYear:YES];
            break;
        case 1:
            cell.textLabel.text = @"End Date";
            NSDate *theEndDate = [MethodHelper dateFromString:self.endDate usingFormat:K_DATEONLY_FORMAT];
            cell.detailTextLabel.text = [MethodHelper localizedDateFrom:theEndDate usingStyle:NSDateFormatterMediumStyle withYear:YES];
            break;
        default:
            break;
    }
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Load the correct date
    if (indexPath.row == 0) {
        DateRangeTableViewController *controller = [[[DateRangeTableViewController alloc] initWithStartDate:self.startDate andDelegate:self] autorelease];
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.row == 1) {
        DateRangeTableViewController *controller = [[[DateRangeTableViewController alloc] initWithEndDate:self.endDate andDelegate:self] autorelease];
        [self.navigationController pushViewController:controller animated:YES];
    }
}


@end
