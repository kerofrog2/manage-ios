//
//  DateRangeViewController.h
//  Manage
//
//  Created by Cliff Viegas on 8/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// View Controllers
#import "DateRangeTableViewController.h"

@protocol DateRangeDelegate
- (void)dateRangeSelectedWithStartDate:(NSString *)theStartDate endDate:(NSString *)theEndDate;
@end

@interface DateRangeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SelectDateRangeDelegate> {
    id <DateRangeDelegate>      delegate;
    UITableView                 *myTableView;
    NSString                    *startDate;
    NSString                    *endDate;
}

@property   (nonatomic, assign) id          delegate;
@property   (nonatomic, copy)   NSString    *startDate;
@property   (nonatomic, copy)   NSString    *endDate;

#pragma mark Initialisation
- (id)initWithStartDate:(NSString *)theStartDate andEndDate:(NSString *)theEndDate;

#pragma mark Load Methods
- (void)loadTableView;

@end
