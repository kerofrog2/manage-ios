//
//  DateRangeTableViewController.m
//  Manage
//
//  Created by Cliff Viegas on 8/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "DateRangeTableViewController.h"

// Method Helper
#import "MethodHelper.h"

typedef enum EnumDateType {
    EnumDateTypeStart = 0,
    EnumDateTypeEnd = 1
} EnumDateType;

@implementation DateRangeTableViewController

@synthesize delegate;
@synthesize selectedDate;
@synthesize selectedDateType;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[selectedDate release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (id)initWithStartDate:(NSString *)theStartDate andDelegate:(id<SelectDateRangeDelegate>)theDelegate {
    self = [super init];
    if (self) {
        self.selectedDateType = EnumDateTypeStart;
        
        // Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the date to due date
		self.selectedDate = theStartDate;
		
		// Set title
		self.title = @"Start Date";
		
		// Create clear bar button item
		UIBarButtonItem *clearBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear" 
																			   style:UIBarButtonItemStyleBordered 
																			  target:self 
																			  action:@selector(clearBarButtonItemAction)];
		[self.navigationItem setRightBarButtonItem:clearBarButtonItem animated:NO];
		[clearBarButtonItem release];
    }
    return self;
}

- (id)initWithEndDate:(NSString *)theEndDate andDelegate:(id<SelectDateRangeDelegate>)theDelegate {
    self = [super init];
    if (self) {
        self.selectedDateType = EnumDateTypeEnd;
        
        // Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the date to due date
		self.selectedDate = theEndDate;
		
		// Set title
		self.title = @"End Date";
		
		// Create clear bar button item
		UIBarButtonItem *clearBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear" 
																			   style:UIBarButtonItemStyleBordered 
																			  target:self 
																			  action:@selector(clearBarButtonItemAction)];
		[self.navigationItem setRightBarButtonItem:clearBarButtonItem animated:NO];
		[clearBarButtonItem release];
    }
    return self;
}


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	//	[self.monthView reload];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	[self setContentSizeForViewInPopover:CGSizeMake(self.monthView.frame.size.width, 
													self.monthView.frame.size.height)];
	[UIView commitAnimations];
	
	[self.monthView reload];
	
	if (self.selectedDate != nil && [self.selectedDate length] > 0) {
        NSDate *theDate = [MethodHelper dateGMTFromString:self.selectedDate usingFormat:K_DATEONLY_FORMAT];
		[self.monthView selectDate:theDate];
	}
	
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	if (self.selectedDate != nil && [self.selectedDate length] > 0) {
        NSDate *theDate = [MethodHelper dateGMTFromString:self.selectedDate usingFormat:K_DATEONLY_FORMAT];
		[self.monthView selectDate:theDate];
	}	
}

#pragma mark -
#pragma mark Tapku Calendar Delegates

- (void)calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)d {
	// Update the date
	self.selectedDate = [MethodHelper stringGMTFromDate:d usingFormat:K_DATEONLY_FORMAT];
    
    if ([self selectedDateType] == EnumDateTypeStart) {
        [self.delegate updateStartDate:[self selectedDate]];
    } else if ([self selectedDateType] == EnumDateTypeEnd) {
        [self.delegate updateEndDate:[self selectedDate]];
    }
    
	[self performSelector:@selector(popViewDelay) withObject:nil afterDelay:0.2f];
}

#pragma mark -
#pragma mark Button Events

- (void)clearBarButtonItemAction {
	// Clear date field
	self.selectedDate = @"";
    
    if ([self selectedDateType] == EnumDateTypeStart) {
        [self.delegate updateStartDate:@""];
    } else if ([self selectedDateType] == EnumDateTypeEnd) {
        [self.delegate updateEndDate:@""]; 
    }
	
	[self.navigationController popViewControllerAnimated:YES];
}


@end
