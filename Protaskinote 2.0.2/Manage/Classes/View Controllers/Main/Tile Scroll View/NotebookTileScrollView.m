//
//  NotebookTileScrollView.m
//  Manage
//
//  Created by Cliff Viegas on 28/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "NotebookTileScrollView.h"

// Method Helper
#import "MethodHelper.h"

// Data Collections
#import "TaskListCollection.h"

// Data Models
#import "Notebook.h"
#import "TaskList.h"
#import "ListItem.h"

// View Controllers
#import "MainViewController.h"

// Constants
#define kTileSize                       CGSizeMake(120, 145)
#define kTagBuffer                      2500
#define kOriginalListTitleFrame         CGRectMake(5, 115, 110, 30)
#define kTileIconTag                    5356
#define kTileIconLabelTag               5358
#define kTileButtonTag                  5361
#define kTilesPerRow                    2
#define kTileStartXBuffer               53
#define kTileStartYBuffer               13
#define kTileXBuffer                    30.0
#define kTileYBuffer                    18.0
#define kScrollViewFrame                CGRectMake(50, 25, 350, 496)

#define kScribbleImageViewTag           318654

// Paper Preview is 414 * 551

// Tile Stamp Constants
#define kTileStampTag			5635
#define kTileStampFrame			CGRectMake(12, 43, 95, 61)

#define kTileScrollViewFooter           5

@implementation NotebookTileScrollView

@synthesize refNotebook;
@synthesize notebookImageFrame;

#pragma mark - Memory Management

- (void)dealloc {
    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection andMainViewController:(MainViewController *)theController
     andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection andNotebook:(Notebook *)theNotebook 
           andNotebookImageFrame:(CGRect)theNotebookImageFrame {
    self = [super init];
    if (self) {
        self.frame = kScrollViewFrame;
        
        // Assign reference to the passed notebook
        self.refNotebook = theNotebook;
        
        // Assign the value for the passed notebook image frame
        notebookImageFrame = theNotebookImageFrame;
        
        // Assign the passed task list collection
        refTaskListCollection = theTaskListCollection;
        
        // Assign the passed master task list collection
        refMasterTaskListCollection = theMasterTaskListCollection;
        
        // Assign the main view controller pointer/reference
        refMainViewController = theController;
        
        // Do not show vertical scroll indicator
		self.showsVerticalScrollIndicator = FALSE;
        
        [self updateScrollList];
    }
    return self;
}

#pragma mark - Update Methods

- (void)updateScrollList {
    // Remove all button subviews in scroll view
	for (UIView *aView in self.subviews) {
		if ([aView isKindOfClass:[UIImageView class]]) {
			[aView removeFromSuperview];
		}
	}
    
    // Used for running cleanup on the notebook (in case we find invalid list reference)
    BOOL foundInvalidReference = FALSE;
    
    for (NSInteger index = 0; index < [refNotebook.lists count]; index++) {
        // Create the image/button
		UIImageView *tileImageView = [[UIImageView alloc] init];
        
        [tileImageView setImage:[UIImage imageNamed:@"PaperPreview.png"]];
        
        // Set the frame of the button
		CGSize tileImageSize = kTileSize;
		[tileImageView setFrame:CGRectMake(0, 0, tileImageSize.width, tileImageSize.height)];
		[tileImageView setUserInteractionEnabled:YES];
        
        NSInteger listID = [[refNotebook.lists objectAtIndex:index] integerValue];
        NSInteger listIndex = [refTaskListCollection getIndexOfTaskListWithID:listID];
        
        if (listIndex < 0 || listIndex >= [refTaskListCollection.lists count]) {
            //[MethodHelper showAlertViewWithTitle:@"Error" 
              //                        andMessage:[NSString stringWithFormat:@"Unable to find reference (%d) to list (%d) within notebook.  Please report this error to manage@kerofrog.com.au",
                //                                  listIndex, listID]
                  //                andButtonTitle:@"Ok"];
            
            foundInvalidReference = TRUE;
            [tileImageView release];
            // Need to remove this from the lists... hmm..  Just a 'cleanup' routine to run after, put it in notebook
            continue;
        }
        
        TaskList *taskList = [refTaskListCollection.lists objectAtIndex:listIndex];
        
        tileImageView.tag = taskList.listID + kTagBuffer;
        
        UILabel *listTitleLabel = [[self getListTitleLabelCopy] retain];
        
        // Resize the label as needed
        [self resizeLabelFrame:listTitleLabel forTitle:taskList.title];
        
        // Add icons to the tile image View
        [self loadIconsInTile:tileImageView forList:taskList];
        
        // Load completed stamp (if needed)
        if ([taskList completed] == TRUE) {
            [self loadCompletedStampForTile:tileImageView];
        }
        
        [listTitleLabel setText:taskList.title];
        
        [tileImageView addSubview:listTitleLabel];
        [listTitleLabel release];
        
        // Add the button
        [self addSubview:tileImageView];
        [tileImageView release];
    }
    
    // Cleaning up notebook (in case we found invalid list reference)
    if (foundInvalidReference == TRUE) {
        [refNotebook removeInstancesOfListIDNotFoundInArray:[refTaskListCollection lists]]; 
         
         // - (BOOL)removeinstancesOfListIDNotFoundInArray:(NSArray *)taskListsArray
    }
    
    // Now to readjust tiles
    // Current x and y location (start with a buffer)
	CGFloat curXLoc = kTileStartXBuffer;
	CGFloat curYLoc = kTileStartYBuffer;
	
    BOOL lastItemBuffered = FALSE;      // Just for positioning when the number of lists in notebook is odd
    
	// Loop through the views (uibuttons) and reposition them
	NSInteger i = 1;
	for (UIView *view in self.subviews) {
		if ([view isKindOfClass:[UIImageView class]]) {
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, curYLoc);
			view.frame = frame;
			
			curXLoc += (view.frame.size.width + kTileXBuffer);
			
			if (i % kTilesPerRow == 0) {
				// Next Row
				curYLoc += (view.frame.size.height + kTileYBuffer);
				curXLoc = kTileStartXBuffer;
                lastItemBuffered = TRUE;
			} else {
                lastItemBuffered = FALSE;
            }
			
			i++;
		}
	}
    
    if (lastItemBuffered == FALSE) {
        curYLoc = curYLoc + 145 + kTileYBuffer;   // 145 is the height of the image views
    }
    
    // Set the content size so that it is scrollable
	// curYLoc is the top tip of the last list button
	[self setContentSize:CGSizeMake([self bounds].size.width, curYLoc + kTileScrollViewFooter)];
    
}

- (void)loadIconsInTile:(UIImageView *)tileImageView forList:(TaskList *)taskList {
	// Need to get count for everything
	NSInteger dueCount = 0;
	NSInteger overdueCount = 0;
	NSInteger incompleteCount = 0;
	
    if ([taskList isNote]) {
        NSString *scribblePath = [MethodHelper getPngScribblePathFromGLSScribble:taskList.scribble];
        
        if ([scribblePath length] > 0) {
            UIImage *scribbleImage = [UIImage imageWithContentsOfFile:scribblePath];
            
            scribbleImage = [MethodHelper scaleImage:scribbleImage maxWidth:120 maxHeight:145];
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:scribbleImage];
            [imageView setFrame:CGRectMake(7, 5, 105.5, 117.5)];
            [imageView setTag:kScribbleImageViewTag];
            [tileImageView addSubview:imageView];
            
            [imageView release];
        }
        
        
        // 115, 128
        
        
        
        
        
    }
    
	// Get counts
	for (ListItem *listItem in taskList.listItems) {
		if ([listItem completed] == FALSE) {
			incompleteCount++;
		}
		
		if (listItem.dueDate != nil && [listItem.dueDate length] > 0 && [listItem completed] == FALSE) {
			NSInteger dueDateDifference = [listItem getDueDateDifferenceFromToday];
			if (dueDateDifference >= 0) {
				dueCount++;
			} else if (dueDateDifference < 0) {
				overdueCount++;
			}
		}
	}
	
	CGFloat yPosition = 5;
	
	if (incompleteCount > 0) {
		// Create icon
		UIImageView *tileIncompleteIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TileIncompleteIcon.png"]];
		[tileIncompleteIcon setFrame:CGRectMake(5, yPosition, 21, 21)];
		[tileIncompleteIcon setTag:kTileIconTag];
		yPosition = yPosition + tileIncompleteIcon.frame.size.height + 3;
		[tileImageView addSubview:tileIncompleteIcon];
		
		// Create label
		UILabel *incompleteLabel = [[UILabel alloc] initWithFrame:CGRectMake(tileIncompleteIcon.frame.origin.x + 25, 
																			 tileIncompleteIcon.frame.origin.y, 
																			 50, 21)];
		[incompleteLabel setBackgroundColor:[UIColor clearColor]];
		[incompleteLabel setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[incompleteLabel setText:[NSString stringWithFormat:@"%d", incompleteCount]];
		[tileImageView addSubview:incompleteLabel];
		
		// Release
		[tileIncompleteIcon release];
		[incompleteLabel release];
	}
	
	if (dueCount > 0) {
		// Create icon
		UIImageView *tileDueIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TileDueIcon.png"]];
		[tileDueIcon setFrame:CGRectMake(5, yPosition, 21, 21)];
		[tileDueIcon setTag:kTileIconTag];
		yPosition = yPosition + tileDueIcon.frame.size.height + 3;
		[tileImageView addSubview:tileDueIcon];
		
		// Create label
		UILabel *dueLabel = [[UILabel alloc] initWithFrame:CGRectMake(tileDueIcon.frame.origin.x + 25, 
                                                                      tileDueIcon.frame.origin.y, 
                                                                      50, 21)];
		[dueLabel setBackgroundColor:[UIColor clearColor]];
		[dueLabel setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[dueLabel setText:[NSString stringWithFormat:@"%d", dueCount]];
		[tileImageView addSubview:dueLabel];
		
		// Release
		[tileDueIcon release];
		[dueLabel release];
	}
	
	if (overdueCount > 0) {
		// Create icon
		UIImageView *tileOverdueIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TileOverdueIcon.png"]];
		[tileOverdueIcon setFrame:CGRectMake(5, yPosition, 21, 21)];
		[tileOverdueIcon setTag:kTileIconTag];
		//yPosition = yPosition + tileOverdueIcon.frame.size.height + 3;
		[tileImageView addSubview:tileOverdueIcon];
		
		// Create label
		UILabel *overdueLabel = [[UILabel alloc] initWithFrame:CGRectMake(tileOverdueIcon.frame.origin.x + 25, 
                                                                          tileOverdueIcon.frame.origin.y, 
                                                                          50, 21)];
		[overdueLabel setBackgroundColor:[UIColor clearColor]];
		[overdueLabel setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[overdueLabel setText:[NSString stringWithFormat:@"%d", overdueCount]];
		[tileImageView addSubview:overdueLabel];
		
		// Release
		[tileOverdueIcon release];
		[overdueLabel release];
	}
}

- (void)loadCompletedStampForTile:(UIImageView *)tile {
	// Add completed image
	UIImageView *tileCompletedImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TileViewStamp.png"]];
	[tileCompletedImage setTag:kTileStampTag];
	[tileCompletedImage setFrame:kTileStampFrame];
	[tile addSubview:tileCompletedImage];
	[tileCompletedImage release];
}

#pragma mark -
#pragma mark Touches Handling

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	[self touchesEnded:touches withEvent:event];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	// Enumerate through all the touch objects.
	for (UITouch *touch in touches) {
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		//[self dispatchFirstTouchAtPoint:[touch locationInView:self] forEvent:nil];
	}	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	// If ref touched view not nil, shrink
	for (UITouch *touch in touches) {
        
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		[self dispatchTouchAtPoint:[touch locationInView:self] forEvent:nil];
	}	
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {

}

#pragma mark - Dispatch Touch Methods

- (void)dispatchTouchAtPoint:(CGPoint)touchPoint forEvent:(UIEvent *)event {
	for (UIView *view in [self subviews]) {
		if (CGRectContainsPoint([view frame], touchPoint) && [view isKindOfClass:[UIImageView class]]) {
			// Handle by getting the tag from our touched view
           // NSInteger listIndex = [refNotebook getIndexOfTaskListWithID:(view.tag - kTagBuffer)];
            
            // Now need to open this.. call main view controller
            
            UIImageView *imageView = (UIImageView *)view;
            
            CGRect touchedViewFrame = CGRectMake(self.frame.origin.x + imageView.frame.origin.x + notebookImageFrame.origin.x, 
                                                 self.frame.origin.y + imageView.frame.origin.y + notebookImageFrame.origin.y - self.contentOffset.y, 
                                                 imageView.frame.size.width, imageView.frame.size.height);
            
            // how to get the reference..
            // Let's try to get the position of this first.
            NSInteger theListID = imageView.tag - kTagBuffer;
            
            [refMainViewController openListWithListID:theListID usingFrame:touchedViewFrame];
		}
	}
}




#pragma mark - Helper Methods

- (void)resizeLabelFrame:(UILabel *)titleLabel forTitle:(NSString *)titleString {
    CGRect titleLabelFrame = CGRectZero;
    NSInteger widthAdjustment = 0;
    
    titleLabelFrame = kOriginalListTitleFrame;
    widthAdjustment = 10;
    
	// Resize the label as needed
	CGSize tileSize = kTileSize;
	CGSize maximumSize = CGSizeMake(tileSize.width - widthAdjustment, 40);
	CGSize titleStringSize = [titleString sizeWithFont:[titleLabel font]
									 constrainedToSize:maximumSize
										 lineBreakMode:[titleLabel lineBreakMode]];
    
	CGRect listTitleFrame = CGRectMake(titleLabelFrame.origin.x, 
									   titleLabelFrame.origin.y - (titleStringSize.height - 27), 
									   titleLabelFrame.size.width, 
									   titleStringSize.height);
	[titleLabel setFrame:listTitleFrame];
}

- (UILabel *)getListTitleLabelCopy {
	UILabel *listTitleLabel = [[[UILabel alloc] init] autorelease];
	
	[listTitleLabel setBackgroundColor:[UIColor clearColor]];
	[listTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
    

    [listTitleLabel setTextColor:[UIColor darkTextColor]];
    [listTitleLabel setFrame:kOriginalListTitleFrame];
    
	[listTitleLabel setTextAlignment:UITextAlignmentRight];
	[listTitleLabel setLineBreakMode:UILineBreakModeWordWrap];
	[listTitleLabel setNumberOfLines:0];
	return listTitleLabel;
}

@end
