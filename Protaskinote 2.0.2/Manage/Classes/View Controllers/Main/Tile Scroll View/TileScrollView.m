//
//  TileScrollView.m
//  Manage
//
//  Created by Cliff Viegas on 2/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "TileScrollView.h"

// Data Collections
#import "TaskListCollection.h"
#import "NotebookCollection.h"

// Data Models
#import "TaskList.h"
#import "ListItem.h"
#import "Notebook.h"

// Subclassed UIObjects
#import "TileImageView.h"

// View Controllers
#import "MainViewController.h"

// Responders
#import "ListPadTableViewResponder.h"

// Method Helper
#import "MethodHelper.h"

// GL Draw
#import "GLDrawES2ViewController.h"

// Constants
#define kTileSize                       CGSizeMake(120, 145)
#define kTileWidth                      120
#define kTileExpandedSize               CGSizeMake(132, 160)
#define kOriginalListTitleFrame         CGRectMake(5, 115, 110, 30)
#define kOriginalNotebookTitleFrame     CGRectMake(15, 115, 97, 30)
#define kTileExpandedWidth              132
#define kTileXBuffer                    60.0
#define kTileYBuffer                    50.0
#define kTilesPerRow                    4
#define kTileStartXBuffer               35
#define kIgnoreTouchViewTag             90674

#define kTagBuffer                      1500
#define kTileIconTag                    8356
#define kTileIconLabelTag               8358
#define kTileButtonTag                  8361

#define kTileScrollViewFooter           340
#define kScribbleImageViewTag           26468

// Tile Stamp Constants
#define kTileStampTag			8635
#define kTileStampFrame			CGRectMake(12, 43, 95, 61)
#define kTileStampExpandedFrame	CGRectMake(18, 50, 95, 61)

@interface TileScrollView()
// Touch Handling
- (void)dispatchFirstTouchAtPoint:(CGPoint)touchPoint forEvent:(UIEvent *)event;
- (void)dispatchTouchAtPoint:(CGPoint)touchPoint forEvent:(UIEvent *)event;
// Helper Methods
- (void)resizeLabelFrame:(UILabel *)titleLabel forTitle:(NSString *)titleString;
- (void)hideTileButtons;
- (UIImageView *)getImageViewListWithListID:(NSInteger)listID;
- (UILabel *)getListTitleLabelCopy;
- (void)shrink:(UIView *)view;
- (void)expand:(UIView *)view;
- (void)deleteCurrentlySelectedTileList;
- (void)deleteCurrentlySelectedNotebook;
@end


@implementation TileScrollView

@synthesize currentListIndex;
@synthesize updated;
@synthesize touchedViewFrame;
@synthesize tileButtonTouchDisabled;
@synthesize refListPadTableViewResponder;
@synthesize refListPadTableView;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	// Action sheets
	[deleteTileListActionSheet release];
	[optionsTileListActionSheet release];
    [deleteTileNotebookActionSheet release];
    [optionsTileNoteActionSheet release];
	[animatedListImage release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection 
		   andMainViewController:(MainViewController *)theController 
	 andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection
           andNotebookCollection:(NotebookCollection *)theNotebookCollection {
	if ((self = [super init])) {
		// Assigned the passed master task list collection
		refMasterTaskListCollection = theMasterTaskListCollection;
		
		// Assign the passed task list collection
		refTaskListCollection = theTaskListCollection;
		
        // Assign the passed notebook collection
        refNotebookCollection = theNotebookCollection;
        
		// Assign the main view controller pointer/reference
		refMainViewController = theController;
		
		// Do not show vertical scroll indicator
		self.showsVerticalScrollIndicator = FALSE;
	
		// Init ref touched view to nil
		refTouchedView = nil;
		
		// Init updated to false
		self.updated = FALSE;
		
		// Init touch timer to nil
		touchTimer = nil;
		
		// Init starting frame to zero
		startingFrame = CGRectZero;
		
		// Init touch point offset to zero
		touchPointOffset = CGPointZero;
		
		// Init touched view start index to 0
		touchedViewStartIndex = 0;
		
		// Init is rearranging list to false
		isRearrangingList = FALSE;
		
		// Init is dragging list to false
		isDraggingList = FALSE;
		
		// Init the current list index to -1
		self.currentListIndex = -1;
		
		// Init the buttons
		deleteTileListButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[deleteTileListButton setImage:[UIImage imageNamed:@"DeleteTileListButton.png"] forState:UIControlStateNormal];
		[deleteTileListButton setHidden:YES];
		[deleteTileListButton setShowsTouchWhenHighlighted:YES];
		[deleteTileListButton addTarget:self action:@selector(deleteTileListButtonAction) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:deleteTileListButton];
		
		optionsTileListButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[optionsTileListButton setImage:[UIImage imageNamed:@"OptionsTileListButton.png"] forState:UIControlStateNormal];
		[optionsTileListButton setHidden:YES];
		[optionsTileListButton setShowsTouchWhenHighlighted:YES];
		[optionsTileListButton addTarget:self action:@selector(optionsTileListButtonAction) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:optionsTileListButton];
		
        
		openTileListButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[openTileListButton setImage:[UIImage imageNamed:@"OpenTileListButton.png"] forState:UIControlStateNormal];
		[openTileListButton setHidden:YES];
		[openTileListButton setShowsTouchWhenHighlighted:YES];
		[openTileListButton addTarget:self action:@selector(openTileListButtonAction) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:openTileListButton];
		
		// Init the action sheets
		deleteTileListActionSheet = [[UIActionSheet alloc] initWithTitle:nil
															delegate:self 
												   cancelButtonTitle:nil 
											  destructiveButtonTitle:@"Delete List" 
												   otherButtonTitles:nil];
		
		optionsTileListActionSheet = [[UIActionSheet alloc] initWithTitle:nil 
														   delegate:self 
												  cancelButtonTitle:nil 
											 destructiveButtonTitle:nil 
												  otherButtonTitles:@"Email Text List", @"Email PDF List", nil];
        
        optionsTileNoteActionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                                 delegate:self 
                                                        cancelButtonTitle:nil 
                                                   destructiveButtonTitle:nil 
                                                        otherButtonTitles:@"Email PDF", nil];
        
        
        deleteTileNotebookActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                     delegate:self 
                                                            cancelButtonTitle:nil 
                                                       destructiveButtonTitle:@"Delete Folder" 
                                                            otherButtonTitles:nil];
		
	}
	return self;
}

#pragma mark -
#pragma mark Update Methods

- (void)updateScrollList {
	// Remove all button subviews in scroll view
	for (UIView *aView in self.subviews) {
		if ([aView isKindOfClass:[UIImageView class]]) {
			[aView removeFromSuperview];
		}
	}
	
	// Hide buttons
	[self hideTileButtons];
	
    NSInteger itemCount = 0;
    
    NSString *buttonType = [refMasterTaskListCollection getTileViewButtonType];
    [self updateButtonTypes:buttonType];
    
    if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
        refMainViewController.title = @"My Lists & Notes";
        itemCount = [refMasterTaskListCollection.lists count];
    } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
        refMainViewController.title = @"My Folders";
        itemCount = [refNotebookCollection.notebooks count];
    }

    for (NSInteger index = 0; index < itemCount; index++) {
        // Create the image/button
		UIImageView *tileImageView = [[UIImageView alloc] init];
		
        
        if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
            [tileImageView setImage:[UIImage imageNamed:@"PaperPreview.png"]];
        } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
            [tileImageView setImage:[UIImage imageNamed:@"NotebookSmall.png"]];
        }
		
		
		// Set the frame of the button
		CGSize tileImageSize = kTileSize;
		[tileImageView setFrame:CGRectMake(0, 0, tileImageSize.width, tileImageSize.height)];
		[tileImageView setUserInteractionEnabled:YES];
		
        
        if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
            TaskList *taskList = [refTaskListCollection.lists objectAtIndex:index];

            tileImageView.tag = taskList.listID + kTagBuffer;
            
            // Need to add a label with the title
            UILabel *listTitleLabel = [[self getListTitleLabelCopy] retain];
            
            // Resize the label as needed
            [self resizeLabelFrame:listTitleLabel forTitle:taskList.title];
            
            // Add icons to the tile image View
            [self loadIconsInTile:tileImageView forList:taskList];
            
            // Load completed stamp (if needed)
            if ([taskList completed] == TRUE) {
                [self loadCompletedStampForTile:tileImageView];
            }
            
            [listTitleLabel setText:taskList.title];
            
            [tileImageView addSubview:listTitleLabel];
            [listTitleLabel release];
            
        } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
            Notebook *notebook = [refNotebookCollection.notebooks objectAtIndex:index];
            
            tileImageView.tag = notebook.notebookID + kTagBuffer;
            
            // Need to add a label with the title
            UILabel *listTitleLabel = [[self getListTitleLabelCopy] retain];
            
            // Resize the label as needed
            [self resizeLabelFrame:listTitleLabel forTitle:notebook.title];
            
            // Add icons to the tile image view
            [self loadIconsInTile:tileImageView forNotebook:notebook];
            
            [listTitleLabel setText:notebook.title];
            
            [tileImageView addSubview:listTitleLabel];
            [listTitleLabel release];
        }
        
		
		
		// Add the button
		[self addSubview:tileImageView];
        
        [tileImageView release];
    }
    
	
	// Current x and y location (start with a buffer)
	CGFloat curXLoc = kTileStartXBuffer;
	CGFloat curYLoc = 35;
	
	// Loop through the views (uibuttons) and reposition them
	NSInteger i = 1;
	for (UIView *view in self.subviews) {
		if ([view isKindOfClass:[UIImageView class]]) {
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, curYLoc);
			view.frame = frame;
			
			curXLoc += (view.frame.size.width + kTileXBuffer);
			
			if (i % kTilesPerRow == 0) {
				// Next Row
				curYLoc += (view.frame.size.height + kTileYBuffer);
				curXLoc = kTileStartXBuffer;
			}
			
			i++;
		}
	}
	
	// Set the content size so that it is scrollable
	// curYLoc is the top tip of the last list button
	[self setContentSize:CGSizeMake([self bounds].size.width, curYLoc + kTileScrollViewFooter)];
	
	// Now select the correct thumbnail
    switch ([refMainViewController currentSection]) {
        case EnumCurrentSectionLists:
            if (currentListIndex >= 0 && [refTaskListCollection.lists count] > 0 && currentListIndex < [refTaskListCollection.lists count]) {
                if (currentListIndex >= [refTaskListCollection.lists count]) {
                    currentListIndex = [refTaskListCollection.lists count] - 1;
                }
                NSInteger currentListID = [[refTaskListCollection.lists objectAtIndex:currentListIndex] listID];
                refTouchedView = [self getImageViewListWithListID:currentListID];
                
                [self showTileButtons];
            }
            break;
        case EnumCurrentSectionNotebooks:
            if (currentListIndex >= 0 && [refNotebookCollection.notebooks count] > 0) {
                if (currentListIndex >= [refNotebookCollection.notebooks count]) {
                    currentListIndex = [refNotebookCollection.notebooks count] - 1;
                }
                NSInteger currentNotebookID = [[refNotebookCollection.notebooks objectAtIndex:currentListIndex] notebookID];
                refTouchedView = [self getImageViewListWithListID:currentNotebookID];
                
                [self showTileButtons];
            }
        default:
            break;
    }
	
	
}

- (void)loadCompletedStampForTile:(UIImageView *)tile {
	// Add completed image
	UIImageView *tileCompletedImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TileViewStamp.png"]];
	[tileCompletedImage setTag:kTileStampTag];
	[tileCompletedImage setFrame:kTileStampFrame];
	[tile addSubview:tileCompletedImage];
	[tileCompletedImage release];
}


- (void)loadIconsInTile:(UIImageView *)tileImageView forList:(TaskList *)taskList {
	// Need to get count for everything
	NSInteger dueCount = 0;
	NSInteger overdueCount = 0;
	NSInteger incompleteCount = 0;
	
    // 16, 60, 380, 423
    
    //(0, 0, 634, 705)
    if ([taskList isNote]) {
        // Get the image
        UIImage *myImage;
        
        //  CGSizeMake(120, 145) / 6.  105.5, 117.5
        
        NSString *pngImagePath = [MethodHelper getPngScribblePathFromGLSScribble:taskList.scribble];
        
        if ([pngImagePath length] > 0) {
            myImage = [UIImage imageWithContentsOfFile:pngImagePath];
            myImage = [MethodHelper scaleImage:myImage maxWidth:120 maxHeight:145];
         
            UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
            
            [imageView setFrame:CGRectMake(7, 5, 105.5, 117.5)];
            [imageView setTag:kScribbleImageViewTag];
            [tileImageView addSubview:imageView];
            
            [imageView release];
            
        }
        
        // 115, 128
        
        
        
        
        //[drawingLayer.view removeFromSuperview];
        //drawingLayer = nil;
        
        
    }
    
    
    
	// Get counts
	for (ListItem *listItem in taskList.listItems) {
        
        
		if ([listItem completed] == FALSE && [listItem.dueDate length] == 0) {
			incompleteCount++;
		}
		
		if (listItem.dueDate != nil && [listItem.dueDate length] > 0 && [listItem completed] == FALSE) {
            NSInteger dueDateDifference = [listItem getDueDateDifferenceFromToday];
			if (dueDateDifference >= 0) {
				dueCount++;
			} else if (dueDateDifference < 0) {
				overdueCount++;
			}
		}
	}
	
	CGFloat yPosition = 5;
	
	if (incompleteCount > 0 && [taskList isNote] == FALSE) {
		// Create icon
		UIImageView *tileIncompleteIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TileIncompleteIcon.png"]];
		[tileIncompleteIcon setFrame:CGRectMake(5, yPosition, 21, 21)];
		[tileIncompleteIcon setTag:kTileIconTag];
		yPosition = yPosition + tileIncompleteIcon.frame.size.height + 3;
		[tileImageView addSubview:tileIncompleteIcon];
		
		// Create label
		UILabel *incompleteLabel = [[UILabel alloc] initWithFrame:CGRectMake(tileIncompleteIcon.frame.origin.x + 25, 
																			 tileIncompleteIcon.frame.origin.y, 
																			 50, 21)];
		[incompleteLabel setBackgroundColor:[UIColor clearColor]];
		[incompleteLabel setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[incompleteLabel setText:[NSString stringWithFormat:@"%d", incompleteCount]];
		[tileImageView addSubview:incompleteLabel];
		
		// Release
		[tileIncompleteIcon release];
		[incompleteLabel release];
	}
	
	if (dueCount > 0 && [taskList isNote] == FALSE) {
		// Create icon
		UIImageView *tileDueIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TileDueIcon.png"]];
		[tileDueIcon setFrame:CGRectMake(5, yPosition, 21, 21)];
		[tileDueIcon setTag:kTileIconTag];
		yPosition = yPosition + tileDueIcon.frame.size.height + 3;
		[tileImageView addSubview:tileDueIcon];
		
		// Create label
		UILabel *dueLabel = [[UILabel alloc] initWithFrame:CGRectMake(tileDueIcon.frame.origin.x + 25, 
																			 tileDueIcon.frame.origin.y, 
																			 50, 21)];
		[dueLabel setBackgroundColor:[UIColor clearColor]];
		[dueLabel setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[dueLabel setText:[NSString stringWithFormat:@"%d", dueCount]];
		[tileImageView addSubview:dueLabel];
		
		// Release
		[tileDueIcon release];
		[dueLabel release];
	}
	
	if (overdueCount > 0 && [taskList isNote] == FALSE) {
		// Create icon
		UIImageView *tileOverdueIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TileOverdueIcon.png"]];
		[tileOverdueIcon setFrame:CGRectMake(5, yPosition, 21, 21)];
		[tileOverdueIcon setTag:kTileIconTag];
		//yPosition = yPosition + tileOverdueIcon.frame.size.height + 3;
		[tileImageView addSubview:tileOverdueIcon];
		
		// Create label
		UILabel *overdueLabel = [[UILabel alloc] initWithFrame:CGRectMake(tileOverdueIcon.frame.origin.x + 25, 
																			 tileOverdueIcon.frame.origin.y, 
																			 50, 21)];
		[overdueLabel setBackgroundColor:[UIColor clearColor]];
		[overdueLabel setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[overdueLabel setText:[NSString stringWithFormat:@"%d", overdueCount]];
		[tileImageView addSubview:overdueLabel];
		
		// Release
		[tileOverdueIcon release];
		[overdueLabel release];
	}
    
}

- (void)loadIconsInTile:(UIImageView *)tileImageView forNotebook:(Notebook *)noteBook {	
    CGFloat yPosition = 5;
    
    if ([noteBook.lists count] > 0) {
        // Create icon
		UIImageView *tileIncompleteIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TileNotebookListsIcon.png"]];
		[tileIncompleteIcon setFrame:CGRectMake(23, yPosition, 21, 21)];
		[tileIncompleteIcon setTag:kTileIconTag];
		//yPosition = yPosition + tileIncompleteIcon.frame.size.height + 3;
		[tileImageView addSubview:tileIncompleteIcon];
		
		// Create label
		UILabel *incompleteLabel = [[UILabel alloc] initWithFrame:CGRectMake(tileIncompleteIcon.frame.origin.x + 25, 
																			 tileIncompleteIcon.frame.origin.y, 
																			 50, 21)];
		[incompleteLabel setBackgroundColor:[UIColor clearColor]];
		[incompleteLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0]];
        [incompleteLabel setTextColor:[UIColor lightTextColor]];
		[incompleteLabel setText:[NSString stringWithFormat:@"%d", [noteBook.lists count]]];
		[tileImageView addSubview:incompleteLabel];
		
		// Release
		[tileIncompleteIcon release];
		[incompleteLabel release];
    }
}

- (void)rearrangeScrollList {
	// Set is rearranging list to TRUE
	isRearrangingList = TRUE;
	
	NSInteger refTouchedViewTag = -1;
	
	// Ignore ref touched view if it is not nil
	if (refTouchedView != nil) {
		refTouchedViewTag = refTouchedView.tag;
	}
	
	[UIView beginAnimations:@"Rearrange Animation" context:nil];
	[UIView setAnimationDuration:0.2f];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView setAnimationDelegate:self];
	
	// Current x and y location (start with a buffer)
	CGFloat curXLoc = kTileStartXBuffer;
	CGFloat curYLoc = 35;
	CGSize imageViewSize = kTileSize;
	
	NSInteger i = 1;
    
    if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
        for (TaskList *taskList in refTaskListCollection.lists) {
            if (refTouchedViewTag != taskList.listID + kTagBuffer) {
                UIImageView *listImageView = [self getImageViewListWithListID:taskList.listID];
                
                CGRect frame = listImageView.frame;
                frame.origin = CGPointMake(curXLoc, curYLoc);
                listImageView.frame = frame;
            } 
            
            curXLoc += (imageViewSize.width + kTileXBuffer);
            
            if (i % kTilesPerRow == 0) {
                // Next Row
                curYLoc += (imageViewSize.height + kTileYBuffer);
                curXLoc = kTileStartXBuffer;
            }
            
            i++;
        }
    } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
        for (Notebook *notebook in refNotebookCollection.notebooks) {
            if (refTouchedViewTag != notebook.notebookID + kTagBuffer) {
                UIImageView *listImageView = [self getImageViewListWithListID:notebook.notebookID];
                
                CGRect frame = listImageView.frame;
                frame.origin = CGPointMake(curXLoc, curYLoc);
                listImageView.frame = frame;
            } 
            
            curXLoc += (imageViewSize.width + kTileXBuffer);
            
            if (i % kTilesPerRow == 0) {
                // Next Row
                curYLoc += (imageViewSize.height + kTileYBuffer);
                curXLoc = kTileStartXBuffer;
            }
            
            i++;
        }
    }
    
	
	
	[self setContentSize:CGSizeMake([self bounds].size.width, curYLoc + kTileScrollViewFooter)];
	
	[UIView commitAnimations];
}

- (void)updateButtonTypes:(NSString *)newButtonType {
    /*
     UIImage *optionsButtonImage = [UIImage imageNamed:@"ExportListIcon.png"];
     if ([self currentSection] == EnumCurrentSectionNotebooks) {
     optionsButtonImage = [UIImage imageNamed:@"OptionsIcon.png"];
     } 
     
     optionsBarButtonItem = [[UIBarButtonItem alloc] initWithImage:optionsButtonImage
     style:UIBarButtonItemStylePlain 
     target:self action:@selector(optionsBarButtonItemAction:)];
     
     */
	if ([newButtonType isEqualToString:@"Dark"]) {
		[deleteTileListButton setImage:[UIImage imageNamed:@"DeleteTileListButtonDark.png"] forState:UIControlStateNormal];
        if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
            [optionsTileListButton setImage:[UIImage imageNamed:@"ExportTileListButtonDark.png"] forState:UIControlStateNormal];
        } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
            [optionsTileListButton setImage:[UIImage imageNamed:@"OptionsTileListButtonDark.png"] forState:UIControlStateNormal];
        }

		[openTileListButton setImage:[UIImage imageNamed:@"OpenTileListButtonDark.png"] forState:UIControlStateNormal];
	} else if ([newButtonType isEqualToString:@"Light"]) {
		[deleteTileListButton setImage:[UIImage imageNamed:@"DeleteTileListButton.png"] forState:UIControlStateNormal];
        if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
            [optionsTileListButton setImage:[UIImage imageNamed:@"ExportTileListButton.png"] forState:UIControlStateNormal];
        } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
            [optionsTileListButton setImage:[UIImage imageNamed:@"OptionsTileListButton.png"] forState:UIControlStateNormal];
        }
		
		[openTileListButton setImage:[UIImage imageNamed:@"OpenTileListButton.png"] forState:UIControlStateNormal];
	}
}

#pragma mark -
#pragma mark Action Sheet Delegates

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ([actionSheet isEqual:deleteTileListActionSheet] && buttonIndex == 0) {
		// Duplicate the currently selected task list
        progressHUD = [[MBProgressHUD alloc] initWithView:refMainViewController.navigationController.view];
        [refMainViewController.navigationController.view addSubview:progressHUD];
        progressHUD.delegate = self;
        progressHUD.labelText = @"Deleting task list...";
        [progressHUD showWhileExecuting:@selector(deleteCurrentlySelectedTileList) onTarget:self withObject:nil animated:YES];
	} else if ([actionSheet isEqual:optionsTileListActionSheet]) {
		if (buttonIndex == 0) {
			[refMainViewController sendTextMailForTaskList:[refTaskListCollection.lists objectAtIndex:currentListIndex]];
		} else if (buttonIndex == 1) {
			TaskList *theTaskList = [refTaskListCollection.lists objectAtIndex:currentListIndex];
			[refMainViewController sendPDFMailForTaskList:theTaskList];
		}
	} else if ([actionSheet isEqual:deleteTileNotebookActionSheet]) {
        if (buttonIndex == 0) {
          [self deleteCurrentlySelectedNotebook];  
        }
    } else if ([actionSheet isEqual:optionsTileNoteActionSheet]) {
        if (buttonIndex == 0) {
            TaskList *theTaskList = [refTaskListCollection.lists objectAtIndex:currentListIndex];
            [refMainViewController sendPDFMailForTaskList:theTaskList];
        }
    }
}

#pragma mark -
#pragma mark Button Actions

- (void)deleteTileListButtonAction {
    if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
        [deleteTileListActionSheet showFromRect:[deleteTileListButton frame] inView:self 
                                       animated:NO];
    } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
        [deleteTileNotebookActionSheet showFromRect:[deleteTileListButton frame] inView:self 
                                       animated:NO];
    }
}

- (void)optionsTileListButtonAction {
    if ([refMainViewController currentSection] == EnumCurrentSectionLists || [refMainViewController currentSection] == EnumCurrentSectionNotebookLists) {
        // Need to work out if we are looking at a note or something else
        TaskList *taskList = [refTaskListCollection.lists objectAtIndex:currentListIndex];
        
        if ([taskList isNote] == FALSE) {
            [optionsTileListActionSheet showFromRect:[optionsTileListButton frame] inView:self 
                                            animated:NO];
        } else if ([taskList isNote] == TRUE) {
            [optionsTileNoteActionSheet showFromRect:[optionsTileListButton frame] inView:self 
                                            animated:NO];
        }
        
        
    } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
        [refMainViewController showNotebookOptionsForNotebookAtIndex:currentListIndex atRect:[optionsTileListButton frame]];
    }
	
}

- (void)openTileListButtonAction {
	if (tileButtonTouchDisabled == TRUE) {
		// Do nothing
		return;
	} else {
		tileButtonTouchDisabled = TRUE;
	}
	switch ([refMainViewController currentSection]) {
        case EnumCurrentSectionLists:
            if (currentListIndex != -1) {
                NSInteger currentListID = [[refTaskListCollection.lists objectAtIndex:currentListIndex] listID];
                UIImageView *tempImageView = [self getImageViewListWithListID:currentListID];
                
                self.touchedViewFrame = CGRectMake(self.frame.origin.x + tempImageView.frame.origin.x, 
                                                   self.frame.origin.y + tempImageView.frame.origin.y - self.contentOffset.y, 
                                                   tempImageView.frame.size.width, tempImageView.frame.size.height);
                [refMainViewController openListWithListID:currentListID usingFrame:self.touchedViewFrame];
            }
            break;
        case EnumCurrentSectionNotebooks:
            if (currentListIndex != -1) {
                tileButtonTouchDisabled = FALSE;
                [refMainViewController manualSetSelectedSegmentIndex:0];  // 0 means set it to be 'list view'
            }
            
            break;
        default:
            break;
    }
	
	
}


#pragma mark -
#pragma mark Touches Handling

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	[self touchesEnded:touches withEvent:event];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	// Do nothing if touch is disabled
	if (tileButtonTouchDisabled == TRUE) {
		// Do nothing
		return;
	}
	
	// Set ref touched view to nil
	refTouchedView = nil;
	
	// Enumerate through all the touch objects.
	for (UITouch *touch in touches) {
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		[self dispatchFirstTouchAtPoint:[touch locationInView:self] forEvent:nil];
	}	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	// If ref touched view not nil, shrink
	if (refTouchedView != nil) {
		[self shrink:refTouchedView];
		
		// Commit changes (if changed) - using random negative number to prevent 'garbage' value analyze issue
		NSInteger endIndex = -465; 
        
        
        if ([refMainViewController currentSection] == EnumCurrentSectionLists || [refMainViewController currentSection] == EnumCurrentSectionNotebookLists) {
            endIndex = [refTaskListCollection getIndexOfTaskListWithID:refTouchedView.tag - kTagBuffer];
        } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
            endIndex = [refNotebookCollection getIndexOfNotebookWithID:refTouchedView.tag - kTagBuffer];
        }

		if (endIndex != touchedViewStartIndex && [refMainViewController currentSection] != EnumCurrentSectionNotebooks) {
			refMasterTaskListCollection.sortOrderUpdated = TRUE;
			[refMasterTaskListCollection commitListOrderChanges];
			
			//[refMasterTaskListCollection updateListOrdersInDatabase];
			self.currentListIndex = endIndex;
			self.updated = TRUE;
			
			// Update list pad
			[refListPadTableViewResponder tableView:refListPadTableView 
                       forceSelectRowAtIndexPathRow:endIndex
                                         andSection:[refMainViewController currentSection]];
		}
		
		if (isDraggingList == FALSE) {
			// Show buttons on tile
			[self showTileButtons];
		}
		
		// Always set is dragging list to false after touch ended
		isDraggingList = FALSE;
		
		// Set touched view to nil and enable scrolling
		refTouchedView = nil;
		[self setScrollEnabled:TRUE];
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	// Enumerate through all the touch objects.
	for (UITouch *touch in touches) {
		// Send to the dispatch method, which will make sure the appropriate subview is acted upon
		[self dispatchTouchAtPoint:[touch locationInView:self] forEvent:nil];
	}	
}

#pragma mark -
#pragma mark Dispatch Touch Methods

- (void)dispatchFirstTouchAtPoint:(CGPoint)touchPoint forEvent:(UIEvent *)event {
	// Cycle through all objects in the collection, checking whether this can 'grab' any of them
	// Cycle through all the lists, checking whether the user has 'grabbed' a list
	// Do nothing if is rearranging list
	if (isRearrangingList) {
		return;
	}
	
	for (UIView *view in [self subviews]) {

		if (CGRectContainsPoint([view frame], touchPoint) && [view isKindOfClass:[UIImageView class]] && 
					[view tag] != kIgnoreTouchViewTag) {
			refTouchedView = view;
			startingFrame = [refTouchedView frame];
			
			// Get the starting list order
            if ([refMainViewController currentSection] == EnumCurrentSectionLists || [refMainViewController currentSection] == EnumCurrentSectionNotebookLists) {
                touchedViewStartIndex = [refTaskListCollection getIndexOfTaskListWithID:refTouchedView.tag - kTagBuffer];
            } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
                touchedViewStartIndex = [refNotebookCollection getIndexOfNotebookWithID:refTouchedView.tag - kTagBuffer];
            }
			
			
			self.currentListIndex = touchedViewStartIndex;
			
			touchTimer = [NSTimer scheduledTimerWithTimeInterval:0.35f target:self selector:@selector(touchTimerTicked:) 
														userInfo:nil repeats:NO];
			
			touchPointOffset.x = touchPoint.x - refTouchedView.frame.origin.x;
			touchPointOffset.y = touchPoint.y - refTouchedView.frame.origin.y;
			
			// Bring ref touched view to front
			[self bringSubviewToFront:refTouchedView];
		}
	}
}

- (void)dispatchTouchAtPoint:(CGPoint)touchPoint forEvent:(UIEvent *)event {
	if (refTouchedView != nil && [refMainViewController currentSection] != EnumCurrentSectionNotebooks) {
		// Move the touched view to the specified point
		refTouchedView.frame = CGRectMake(touchPoint.x - touchPointOffset.x,
										  touchPoint.y - touchPointOffset.y,
										  refTouchedView.frame.size.width,
										  refTouchedView.frame.size.height);
	
		// Exit if is rearranging list
		if (isRearrangingList) {
			return;
		}
		
		// Need to check and see if the touched view displaces any other views
		
		// 1. (DONE) Create a list order field in the Task List table
		
		// 1. Create mini version of frame and see if this overlaps any image views in the
		// scroll view. (half size) - or use a sub view frame of imageview
		CGRect miniFrame = CGRectMake(refTouchedView.frame.origin.x + (refTouchedView.frame.size.width / 4.0), 
									  refTouchedView.frame.origin.y + (refTouchedView.frame.size.height / 4.0), 
									  refTouchedView.frame.size.width / 2.0, 
									  refTouchedView.frame.size.height / 2.0);
		for (UIView *view in [self subviews]) {
			if ([view isKindOfClass:[UIImageView class]] && CGRectIntersectsRect([view frame], miniFrame)
				&& [view isEqual:refTouchedView] == FALSE && isRearrangingList == FALSE) {
				// Found to overlap
				// Swap lists in array (swap is wrong, it is insert)
				//[refMasterTaskListCollection moveFromListWithID:refTouchedView.tag - kTagBuffer toListWithID:view.tag - kTagBuffer];
				
				//[refTaskListCollection moveFromListWithID:refTouchedView.tag - kTagBuffer toListWithID:view.tag - kTagBuffer];
				
				[refMasterTaskListCollection moveObjectWithListID:refTouchedView.tag - kTagBuffer toObjectWithListID:view.tag - kTagBuffer];
				
				[refTaskListCollection moveObjectWithListID:refTouchedView.tag - kTagBuffer toObjectWithListID:view.tag - kTagBuffer];
				
				// Commit changes to list order and update database (too slow to use here)
				/*[refMasterTaskListCollection commitListOrderChanges];
				
				// Reload the task list collection
				[refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];*/
				
				// Change start frame to new frame
				startingFrame = [view frame];
				
				// Reload tile scroll view
				[self rearrangeScrollList];
				
				// Also need to update the master
				
				
				// Need to prevent this rearrange occuring more than once
				
				// Need to update all the tags list ID's (No, not needed)
				
				
				// Commit to DB
			} 
		}
		
		// 2. If it does overlap an imageview, then get the selected imageview and the overlapped
		// imageview and swap them in the array
		// 3. Reload the tileScrollView array, using Animations and reposition all the imageViews
		
		// 1. Commit: Once the user has let go, check to see if the selected imageview has changed its
		// position.  If it has, then do a database update and update the list order.
	}
}



#pragma mark -
#pragma mark Touch Timer Actions

- (void)touchTimerTicked:(id)sender {
	if (refTouchedView != nil && [refMainViewController currentSection] != EnumCurrentSectionNotebooks) {
		// Only clear tile buttons if they are in this view
		for (UIView *view in refTouchedView.subviews) {
			if ([view tag] == kTileButtonTag) {
				[view removeFromSuperview];
			}
		}
		
		[self hideTileButtons];
		
		[self expand:refTouchedView];
		isDraggingList = TRUE;
		[self setScrollEnabled:FALSE];
	}
}

#pragma mark -
#pragma mark Animation Delegates

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
	if ([animationID isEqualToString:@"Rearrange Animation"]) {
		// Finished animation
		isRearrangingList = FALSE;
	}
}

#pragma mark -
#pragma mark Helper Methods

- (void)scrollToSelectedTile:(BOOL)animated {
    if (refTouchedView != nil) {
        CGFloat yOrigin = refTouchedView.frame.origin.y + 200;
        if (self.contentOffset.y > yOrigin - 300) {
            yOrigin = yOrigin - 300;
        }
        
        if (yOrigin < 0) {
            yOrigin = 0;
        }
        

        CGRect scrollRect = CGRectMake(refTouchedView.frame.origin.x, 
                                       yOrigin, 
                                       refTouchedView.frame.size.width, 
                                       refTouchedView.frame.size.height);
        [self scrollRectToVisible:scrollRect animated:animated];
    }
}

- (void)resizeLabelFrame:(UILabel *)titleLabel forTitle:(NSString *)titleString {
	
    CGRect titleLabelFrame = CGRectZero;
    NSInteger widthAdjustment = 0;
    if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
        titleLabelFrame = kOriginalListTitleFrame;
        widthAdjustment = 10;
    } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
        titleLabelFrame = kOriginalNotebookTitleFrame;
        widthAdjustment = 23;
    }

	
	// Resize the label as needed
	CGSize tileSize = kTileSize;
	CGSize maximumSize = CGSizeMake(tileSize.width - widthAdjustment, 40);
	CGSize titleStringSize = [titleString sizeWithFont:[titleLabel font]
									 constrainedToSize:maximumSize
										 lineBreakMode:[titleLabel lineBreakMode]];

	CGRect listTitleFrame = CGRectMake(titleLabelFrame.origin.x, 
									   titleLabelFrame.origin.y - (titleStringSize.height - 27), 
									   titleLabelFrame.size.width, 
									   titleStringSize.height);
	[titleLabel setFrame:listTitleFrame];
}

- (void)selectTileAtIndex:(NSInteger)index {
    switch ([refMainViewController currentSection]) {
        case EnumCurrentSectionLists:
            if ([refTaskListCollection.lists count] > 0) {
                currentListIndex = index;
                NSInteger currentListID = [[refTaskListCollection.lists objectAtIndex:index] listID];
                refTouchedView = [self getImageViewListWithListID:currentListID];
            }
            
            break;
        case EnumCurrentSectionNotebooks:
            if ([refNotebookCollection.notebooks count] > 0) {
                currentListIndex = index;
                NSInteger currentNotebookID = [[refNotebookCollection.notebooks objectAtIndex:index] notebookID];
                refTouchedView = [self getImageViewListWithListID:currentNotebookID];
            }
            
            break;
        default:
            break;
    }
    
	
	
	[self showTileButtons];
}

- (void)showTileButtons {
	if (refTouchedView != nil) {
		//[self hideTileButtons];
		
		// Add tile buttons
		/*UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[deleteButton setTag:kTileButtonTag];
		[deleteButton setImage:[UIImage imageNamed:@"AddButton.png"] forState:UIControlStateNormal];
		[deleteButton addTarget:self action:@selector(deleteButtonAction) forControlEvents:UIControlEventTouchUpInside];
		[deleteButton setFrame:CGRectMake(20, 140, 50, 44)];
		[deleteButton setUserInteractionEnabled:YES];
		[refTouchedView addSubview:deleteButton];*/
		
		//#define kTileSize				CGSizeMake(120, 145)
		
		//UIImageView *currentTileButton
		[deleteTileListButton setFrame:CGRectMake(refTouchedView.frame.origin.x, 
												  refTouchedView.frame.origin.y + 140, 
												  40, 40)];
		[optionsTileListButton setFrame:CGRectMake(refTouchedView.frame.origin.x + 40, 
												  refTouchedView.frame.origin.y + 140, 
												  40, 40)];
		[openTileListButton setFrame:CGRectMake(refTouchedView.frame.origin.x + 80, 
												refTouchedView.frame.origin.y + 140, 
												40, 40)];
		
		//[exportTileListButton setBackgroundColor:[UIColor yellowColor]];
		//[deleteTileListButton setBackgroundColor:[UIColor greenColor]];
		
		[deleteTileListButton setHidden:NO];
		[optionsTileListButton setHidden:NO];
		[openTileListButton setHidden:NO];
		
		// Scroll and select the selected thumbnail
		
		NSInteger selectedIndex = 0;
        
        if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
            selectedIndex = [refTaskListCollection getIndexOfTaskListWithID:refTouchedView.tag - kTagBuffer];
        } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
            selectedIndex = [refNotebookCollection getIndexOfNotebookWithID:refTouchedView.tag - kTagBuffer];
        }
        
        if (selectedIndex == -1) {
            [self hideTileButtons];
            //[MethodHelper showAlertViewWithTitle:@"Error" 
              //                        andMessage:[NSString stringWithFormat:@"Unable to find selected index (%d) for section (%d). Please report this error to manage@kerofrog.com.au.", selectedIndex, [refMainViewController currentSection]]
                //                  andButtonTitle:@"Ok"];
            return;
        }
        
		[refListPadTableViewResponder tableView:refListPadTableView 
                   forceSelectRowAtIndexPathRow:selectedIndex
                                     andSection:[refMainViewController currentSection]];
		
	}
}

- (void)hideTileButtons {
	[deleteTileListButton setHidden:YES];
	[optionsTileListButton setHidden:YES];
	[openTileListButton setHidden:YES];
	//self.currentListIndex = -1;
	
	// Clear any other tile buttons
	/*for (UIView *view in self.subviews) {
		if ([view isKindOfClass:[UIImageView class]]) {
			// Found a view, look for any buttons and remove
			for (UIView *subview in view.subviews) {
				if ([subview tag] == kTileButtonTag) {
					[subview removeFromSuperview];
				}
			}
		}
	}*/
}


- (UILabel *)getListTitleLabelCopy {
	//#define kTileSize				CGSizeMake(120, 145)
	
	//UILabel *listTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 20, 
	//																	tileSize.width - 10, 
	//																	tileSize.height - 40)];
	UILabel *listTitleLabel = [[[UILabel alloc] init] autorelease]; //WithFrame:kOriginalListTitleFrame];
	
	[listTitleLabel setBackgroundColor:[UIColor clearColor]];
	[listTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
    
    if ([refMainViewController currentSection] == EnumCurrentSectionLists) {
        [listTitleLabel setTextColor:[UIColor darkTextColor]];
        [listTitleLabel setFrame:kOriginalListTitleFrame];
    } else if ([refMainViewController currentSection] == EnumCurrentSectionNotebooks) {
        [listTitleLabel setTextColor:[UIColor lightTextColor]];
        [listTitleLabel setFrame:kOriginalNotebookTitleFrame];
    }
	
	[listTitleLabel setTextAlignment:UITextAlignmentRight];
	[listTitleLabel setLineBreakMode:UILineBreakModeWordWrap];
	[listTitleLabel setNumberOfLines:0];
	return listTitleLabel;
	
	/*
	 #define kTileSize				CGSizeMake(120, 145)
	 #define kTileWidth				120
	 #define kTileExpandedSize		CGSizeMake(132, 160)
	 
	 */
}

- (UIImageView *)getImageViewListWithListID:(NSInteger)listID {
	for (UIView *view in [self subviews]) {
		if (view.tag == listID + kTagBuffer && [view isKindOfClass:[UIImageView class]]) {
			// Found
			UIImageView *imageView = (UIImageView *)view;
			return imageView;
		}
	}
	
	return nil;
}

- (void)shrink:(UIView *)view {
	// Get label in tile
	UILabel *tileLabel = nil;
	
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.2f];
	//[view setFrame:CGRectMake(view.frame.origin.x + ((view.frame.size.width - newSize.width) / 2), 
	//						  view.frame.origin.y + ((view.frame.size.height - newSize.height) / 2), 
	//						  newSize.width, newSize.height)];
	[view setFrame:startingFrame];
    
    UIImageView *tileStamp = nil;
	for (UIView *subview in view.subviews) {
		if ([subview isKindOfClass:[UILabel class]] && subview.tag != kTileIconLabelTag) {
			tileLabel = (UILabel *)subview;
		} else if ([subview isKindOfClass:[UIImageView class]] && subview.tag == kTileStampTag) {
			tileStamp = (UIImageView *)subview;
		} else if ([subview isKindOfClass:[UIImageView class]] && subview.tag == kScribbleImageViewTag) {
			[subview setFrame:CGRectMake(7, 5, 105.5, 117.5)];
		}
        
	}

	if (tileStamp != nil) {
		[tileStamp setFrame:kTileStampFrame];
	}
	
	//CGSize tileSize = kTileSize;
	
	//UILabel *listTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 20, 
	//																	tileSize.width - 10, 
	//																	tileSize.height - 40)];
//	CGRect originalFrame = kOriginalListTitleFrame;
	
	[self resizeLabelFrame:tileLabel forTitle:tileLabel.text];
	
	//#define kTileExpandedSize		CGSizeMake(132, 160)
	
	// Resize the label as needed
	/*CGSize tileSize = kTileSize;
	CGSize maximumSize = CGSizeMake(tileSize.width - 10, 60);
	NSString *titleString = tileLabel.text;
	CGSize titleStringSize = [titleString sizeWithFont:[tileLabel font]
									 constrainedToSize:maximumSize
										 lineBreakMode:[tileLabel lineBreakMode]];
	CGRect listTitleFrame = CGRectMake(tileLabel.frame.origin.x, 
									   tileLabel.frame.origin.y - (titleStringSize.height - 27), 
									   tileLabel.frame.size.width, 
									   titleStringSize.height);
	[tileLabel setFrame:listTitleFrame];*/
	
	/*CGSize tileSize = kTileSize;
	[tileLabel setFrame:CGRectMake(5, 20, tileSize.width - 10, tileSize.height - 40)];*/
	[UIView commitAnimations];
}

/*
 
 #define kTileSize				CGSizeMake(120, 145)
 #define kTileWidth				120
 #define kTileExpandedSize		CGSizeMake(132, 160)
 
 */

- (void)expand:(UIView *)view {
	// Get label in tile
	UILabel *tileLabel = [[[UILabel alloc] init] autorelease];
	UIImageView *tileStamp = [[[UIImageView alloc] init] autorelease];
	
	
    // GRectMake(7, 5, 105.5, 117.5)]
    
	CGSize newSize = kTileExpandedSize;
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.2f];
    
    for (UIView *subview in view.subviews) {
		if ([subview isKindOfClass:[UILabel class]] && subview.tag != kTileIconLabelTag) {
			tileLabel = (UILabel *)subview;
		} else if ([subview isKindOfClass:[UIImageView class]] && subview.tag == kTileStampTag) {
			tileStamp = (UIImageView *)subview;
		} else if ([subview isKindOfClass:[UIImageView class]] && subview.tag == kScribbleImageViewTag) {
			[subview setFrame:CGRectMake(7, 5, 117, 130)]; //1.01
		}
	}
    
	//[tileLabel setFrame:CGRectMake(tileLabel.frame.origin.x + ((newSize.width - view.frame.size.width) / 2), 
	//							   tileLabel.frame.origin.y + ((newSize.height - view.frame.size.height) / 2), 
	//							   tileLabel.frame.size.width, tileLabel.frame.size.height)];
	if (tileStamp != nil) {
		[tileStamp setFrame:kTileStampExpandedFrame];
	}
	[tileLabel setFrame:CGRectMake(tileLabel.frame.origin.x + 12, tileLabel.frame.origin.y + 15, 
								   tileLabel.frame.size.width, tileLabel.frame.size.height)];
	[view setFrame:CGRectMake(view.frame.origin.x - ((newSize.width - view.frame.size.width) / 2), 
							  view.frame.origin.y - ((newSize.height - view.frame.size.height) / 2), 
							  newSize.width, newSize.height)];
	
	
	[UIView commitAnimations];
}

- (void)deleteCurrentlySelectedTileList {
    // Remove the touched view from the subviews
    // Get reftouched view for current list index
    NSInteger currentListID = [[refTaskListCollection.lists objectAtIndex:currentListIndex] listID];
    refTouchedView = [self getImageViewListWithListID:currentListID];
    
    if (refTouchedView != nil) {
        [refTouchedView removeFromSuperview];
        
        NSInteger masterListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:currentListID];
        [refMasterTaskListCollection deleteListAtIndex:masterListIndex permanent:NO];
        
        // Need to remove from collection too
        [refTaskListCollection reloadWithMasterTaskListCollection:refMasterTaskListCollection];
        
        // Hide buttons
        [self hideTileButtons];
        
        [self rearrangeScrollList];
        
        refTouchedView = nil;
        
        // Tile scroll view has been updated
        self.updated = TRUE;
        
        // Update list pad
        [refListPadTableView reloadData];
    }
}

- (void)deleteCurrentlySelectedNotebook {
    // Remove the touched view from the subviews
    // Get reftouched view for current list index
    NSInteger currentNotebookID = [[refNotebookCollection.notebooks objectAtIndex:currentListIndex] notebookID];
    refTouchedView = [self getImageViewListWithListID:currentNotebookID];
    
    if (refTouchedView != nil) {
        [refTouchedView removeFromSuperview];
        
        NSInteger notebookListIndex = [refNotebookCollection getIndexOfNotebookWithID:currentNotebookID];
        [refNotebookCollection deleteNotebookAtIndex:notebookListIndex];
        
        // Hide buttons
        [self hideTileButtons];
        
        [self rearrangeScrollList];
        
        refTouchedView = nil;
        
        // Tile scroll view has been updated
        self.updated = TRUE;
        
        // Update list pad
        [refListPadTableView reloadData];
    }
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
    [progressHUD removeFromSuperview];
    [progressHUD release];
}

@end
