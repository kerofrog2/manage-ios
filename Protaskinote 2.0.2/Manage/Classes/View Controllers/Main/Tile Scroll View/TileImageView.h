//
//  TileImageView.h
//  Manage
//
//  Created by Cliff Viegas on 2/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TileImageView : UIImageView {
	BOOL	expanded;
}

@property (nonatomic, assign)	BOOL	expanded;

@end
