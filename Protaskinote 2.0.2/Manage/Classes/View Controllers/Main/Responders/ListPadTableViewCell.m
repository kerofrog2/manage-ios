//
//  ListPadTableViewCell.m
//  Manage
//
//  Created by Cliff Viegas on 19/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ListPadTableViewCell.h"

@implementation ListPadTableViewCell

@synthesize listTitle;
@synthesize rightBorder;
@synthesize leftBorder;
@synthesize separatorView;
@synthesize taskCountImageView;

@synthesize taskCountLabel;
@synthesize overdueCountLabel;

// Constants
#define kTaskCountLabelFrame        CGRectMake(0, 0, 21, 21)
#define kOverdueCountLabelFrame     CGRectMake(0, 0, 21, 21)

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    //Tan Nguyen leaks here
	[self.listTitle release];
    [self.rightBorder release];
    [self.leftBorder release];
    [self.separatorView release];
    [self.taskCountImageView release];
    
    [self.taskCountLabel release];
    [self.overdueCountLabel release];
//	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
		[self setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
		//[self setBackgroundColor:[UIColor clearColor]];
        
        // Init the separator view
        //Tan Nguyen
        UIView * separatorViewTemp = [[UIView alloc] initWithFrame:kSeparatorViewFrame];
        self.separatorView = separatorViewTemp;
        //CBCCCD, 203, 204, 205
        //[self.separatorView setBackgroundColor:[UIColor lightGrayColor]];
        [separatorView setBackgroundColor:[UIColor colorWithRed:100 / 255.0 green:100 / 255.0 blue:100 / 255.0 alpha:1.0]];
        [self addSubview:separatorView];
        [separatorViewTemp release];
       // 272324
        
        /*
         // Also need to set the UILabel
         
         */
        
		//[self addSubview:self.listCompleted];
		//[self.listCompleted release];
        //Tan Nguyen
        UIImageView * taskCountImageViewTemp = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.taskCountImageView = taskCountImageViewTemp;
        
        //Tan Nguyen
        UILabel *  taskCountLabelTemp = [[UILabel alloc] init];
        taskCountLabel = taskCountLabelTemp;
        [taskCountLabel setBackgroundColor:[UIColor clearColor]];
        [taskCountLabel setFont:[UIFont fontWithName:@"Helvetica" size:10.0]];
        [taskCountLabel setFrame:kTaskCountLabelFrame];
        [taskCountLabel setTextAlignment:UITextAlignmentCenter];
        [taskCountLabel setTextColor:[UIColor darkTextColor]];
        [taskCountImageView addSubview:taskCountLabel];
        
        //Tan Nguyen

        UILabel * overdueCountLabelTemp = [[UILabel alloc] init];
        overdueCountLabel = overdueCountLabelTemp;
        [overdueCountLabel setBackgroundColor:[UIColor clearColor]];
        [overdueCountLabel setFont:[UIFont fontWithName:@"Helvetica" size:10.0]];
        [overdueCountLabel setFrame:kOverdueCountLabelFrame];
        [overdueCountLabel setTextAlignment:UITextAlignmentCenter];
        [overdueCountLabel setTextColor:[UIColor darkTextColor]];
        [taskCountImageView addSubview:overdueCountLabel];
        
        [self addSubview:taskCountImageView];
        
        [taskCountImageViewTemp release];
        [taskCountLabelTemp release];
        [overdueCountLabelTemp release];
		
        // Title for the table view cell frame
        //Tan Nguyen
		UILabel * listTitleTemp = [[UILabel alloc] initWithFrame:kListTitleFrame] ;
        listTitle = listTitleTemp;
		[self.listTitle setBackgroundColor:[UIColor clearColor]];
		[self.listTitle setTextColor:[UIColor whiteColor]];
		[self.listTitle setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
		[self addSubview:self.listTitle];
        
        // Right border of tableview
        //Tan Nguyen
		UIImageView * rightBorderTemp = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPadRight.png"]];
        rightBorder = rightBorderTemp;
        [self.rightBorder setFrame:kRightBorderFrame];
        [self addSubview:rightBorder];
        
        // Left border of tableview
        //Tan Nguyen
        UIImageView * leftBorderTemp = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPadLeft.png"]];
        leftBorder = leftBorderTemp;
        [self.leftBorder setFrame:kLeftBorderFrame];
        [self addSubview:leftBorder];
        [rightBorderTemp release];
        [leftBorderTemp release];
        UIView * bgView = [UIView new];
        [self setBackgroundView:bgView];
        [bgView release];
    }
    return self;
}


#pragma mark - Public Methods

- (void)updateTaskCount:(NSInteger)taskCount andOverdueCount:(NSInteger)overdueCount note:(BOOL)isNote {
    if (taskCount == 0 && overdueCount == 0 && isNote == FALSE) {
        [self.taskCountImageView setHidden:YES];
        return;
    }
    [self.taskCountImageView setHidden:NO];
    
    if (isNote) {
        [self.taskCountImageView setFrame:CGRectMake(152, 6, 23, 23)];
        [self.taskCountImageView setImage:[UIImage imageNamed:@"SilverPenIcon.png"]];
        [self.overdueCountLabel setHidden:YES];
        //[self.taskCountLabel setText:@"N"];
        [self.taskCountLabel setHidden:YES];
        [self.taskCountLabel setFrame:kTaskCountLabelFrame];
    } else if (taskCount > 0 && overdueCount == 0) {
        [self.taskCountImageView setFrame:CGRectMake(155, 7, 21, 21)];
        [self.taskCountImageView setImage:[UIImage imageNamed:@"TaskCount.png"]];
        [self.overdueCountLabel setHidden:YES];
        [self.taskCountLabel setText:[NSString stringWithFormat:@"%d", taskCount]];
        [self.taskCountLabel setHidden:NO];
        [self.taskCountLabel setFrame:kTaskCountLabelFrame];
    } else if (taskCount > 0 && overdueCount > 0) {
        [self.taskCountImageView setFrame:CGRectMake(140, 7, 36, 21)];
        [self.taskCountImageView setImage:[UIImage imageNamed:@"TaskAndOverdueCount.png"]];
        [self.overdueCountLabel setHidden:NO];
        [self.taskCountLabel setHidden:NO];
        [self.overdueCountLabel setFrame:CGRectMake(0, 0, 21, 21)];
        [self.taskCountLabel setFrame:CGRectMake(16, 0, 20, 21)];
        [self.taskCountLabel setText:[NSString stringWithFormat:@"%d", taskCount]];
        [self.overdueCountLabel setText:[NSString stringWithFormat:@"%d", overdueCount]];
    } else if (taskCount == 0 && overdueCount > 0) {
        [self.taskCountImageView setFrame:CGRectMake(155, 7, 21, 21)];
        [self.taskCountImageView setImage:[UIImage imageNamed:@"OverdueCount.png"]];
        [self.overdueCountLabel setHidden:NO];
        [self.taskCountLabel setHidden:YES];
        [self.overdueCountLabel setFrame:kOverdueCountLabelFrame];
        [self.overdueCountLabel setText:[NSString stringWithFormat:@"%d", overdueCount]];
    }
}



@end
