//
//  ListPadTableViewResponder.m
//  Manage
//
//  Created by Cliff Viegas on 19/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ListPadTableViewResponder.h"

// Data Models
#import "TaskList.h"
#import "Notebook.h"

// Data Collections
#import "TaskListCollection.h"
#import "NotebookCollection.h"

// Custom Table View Cells
#import "ListPadTableViewCell.h"

// Constants
const CGRect kNumberCaseFrame = { 154.f, 34.f, 20.f, 20.f };

@implementation ListPadTableViewResponder

@synthesize delegate;
@synthesize listsCollapsed;
@synthesize notebooksCollapsed;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[super dealloc];
}


#pragma mark -
#pragma mark Initialisation

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection andDelegate:(id<ListPadDelegate>)theDelegate {
	if ((self = [super init])) {
		// Assign reference to passed task list collection
		refTaskListCollection = theTaskListCollection;
		
        // Two more lists are 'Filters' and 'Notebooks'.  
        // Get the note book collection
        refNotebookCollection = theNotebookCollection;
        
        // Init collapsed bools
        self.listsCollapsed = FALSE;
        self.notebooksCollapsed = FALSE;
        
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Init selected row and section to 0
		selectedRow = 0;
        selectedSection = 0;
	}
	return self;
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    ListPadTableViewCell *cell = (ListPadTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ListPadTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        //[cell.contentView addSubview:[self getViewForTaskCount:taskCount overdueCount:overdueCount]];
		//[cell.contentView addSubview:[self getTextFieldForIndexPath:indexPath]];
	}	
    
    // Get the counts
    NSInteger taskCount = 0;
    NSInteger overdueCount = 0;
    BOOL isNote = FALSE;
    if (indexPath.section == 0 && [refTaskListCollection.lists count] > 0) {
        TaskList *taskList = [refTaskListCollection.lists objectAtIndex:indexPath.row];
        isNote = [taskList isNote];
        taskCount = [taskList getCountOfNonOverdueTasks];
        overdueCount = [taskList getCountOfOverdueTasks];
    } else if (indexPath.section == 1 && [refNotebookCollection.notebooks count] > 0) {
        Notebook *notebook = [refNotebookCollection.notebooks objectAtIndex:indexPath.row];
        taskCount = [notebook.lists count];
    }
    
    [cell updateTaskCount:taskCount andOverdueCount:overdueCount note:isNote];
    
    
	// Default values
	cell.accessoryType = UITableViewCellAccessoryNone;
	[cell.listTitle setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
    [cell.listTitle setHighlightedTextColor:[UIColor darkTextColor]];
    [cell.listTitle setTextColor:[UIColor whiteColor]];
    //[cell.accessoryView setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
    
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.backgroundView = [[[UIView alloc] init] autorelease];
    //cell.backgroundView.backgroundColor = [UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0];
    
    
    cell.backgroundView.backgroundColor = [UIColor colorWithRed:69 / 255.0 green:65 / 255.0 blue:66 / 255.0 alpha:1.0];
    
    
    
    //[cell.contentView setBackgroundColor:[UIColor greenColor]];
    //[cell setBackgroundColor:[UIColor clearColor]];
    //[cell setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
    //[cell.contentView setBackgroundColor:[UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0]];
    //cell.listTitle.text = @"";

    if ([indexPath section] == 0) {
        // Setup the cell
        TaskList *taskList = [refTaskListCollection.lists objectAtIndex:indexPath.row];
        
        //cell.imageView.image = [UIImage imageNamed:@"BoxTicked.png"];
        if (indexPath.row == selectedRow && indexPath.section == selectedSection) {
            cell.backgroundView.backgroundColor = [UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0];
           // [cell.listTitle setTextColor:[UIColor lightGrayColor]];
            //cell.backgroundView.backgroundColor = [UIColor colorWithRed:69 / 255.0 green:65 / 255.0 blue:66 / 255.0 alpha:1.0];
            //[cell.listTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
        }
        
        cell.listTitle.text = taskList.title;
    } else if ([indexPath section] == 1) {
        Notebook *notebook = [refNotebookCollection.notebooks objectAtIndex:indexPath.row];
        
        if (indexPath.row == selectedRow && indexPath.section == selectedSection) {
            cell.backgroundView.backgroundColor = [UIColor colorWithRed:39 / 255.0 green:35 / 255.0 blue:36 / 255.0 alpha:1.0];
            //cell.backgroundView.backgroundColor = [UIColor colorWithRed:69 / 255.0 green:65 / 255.0 blue:66 / 255.0 alpha:1.0];
            //[cell.listTitle setTextColor:[UIColor lightGrayColor]];
            //[cell.listTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
        }
        
        cell.listTitle.text = notebook.title;
    }
	
	
    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    NSInteger rowCount = 0;
    if (section == 0) {
        if ([self listsCollapsed] == TRUE) {
            rowCount = 0;
        } else if ([self listsCollapsed] == FALSE) {
            rowCount = [refTaskListCollection.lists count];
        }
    } else if (section == 1) {
        if ([self notebooksCollapsed] == TRUE) {
            rowCount = 0;
        } else if ([self notebooksCollapsed] == FALSE) {
            rowCount = [refNotebookCollection.notebooks count];
        }
    } else if (section == 2) {
        rowCount = 0;
    } else if (section == 3) {
        rowCount = 0;
    }
    
	return rowCount;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 0) {
        selectedRow = indexPath.row;
        selectedSection = indexPath.section;
        [tableView reloadData];
        [self.delegate listSelected:indexPath.row];
    } else if ([indexPath section] == 1) {
        selectedRow = indexPath.row;
        selectedSection = indexPath.section;
        [tableView reloadData];
        [self.delegate notebookSelected:indexPath.row];
    }
	
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    // Init the header and footer views
    //Tan Nguyen fix leaks
    static NSString *HeaderIdentifier = @"Header";
    
    UIView *headerView = (UIView* )[tableView dequeueReusableCellWithIdentifier:HeaderIdentifier];
    if (headerView == nil) {
        headerView = [[[UIView alloc] initWithFrame:kHeaderViewFrame] autorelease];
    }
//    UIView *headerView = [[[UIView alloc] initWithFrame:kHeaderViewFrame] autorelease];
    [headerView setBackgroundColor:[UIColor colorWithRed:25 / 255.0 green:25 / 255.0 blue:25 / 255.0 alpha:1.0]];
    //[headerView setBackgroundColor:[UIColor greenColor]];
    // Add a subview imageView
    UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:kHeaderImageViewFrame];
    [headerImageView setImage:[UIImage imageNamed:@"ControlPadHeader.png"]];
    
    if (selectedSection == section) {
        UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow.png"]];
        [arrowImageView setFrame:CGRectMake(18, 20, 9, 17)];
        [headerImageView addSubview:arrowImageView];
        [arrowImageView release];
    }
    
    [headerImageView setOpaque:YES];
    [headerView addSubview:headerImageView];
    [headerImageView release];
    
    // Add a label
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:kHeaderLabelFrame];
    headerLabel.text = @"Lists / Notes";
    if (section == 1) {
        headerLabel.text = @"Folders";
    } else if (section == 2) {
        headerLabel.text = @"Archives";
    } else if (section == 3) {
        headerLabel.text = @"In-App Purchases";
    }
    [headerLabel setBackgroundColor:[UIColor clearColor]];
    [headerLabel setTextAlignment:UITextAlignmentCenter];
    [headerLabel setTextColor:[UIColor darkTextColor]];
    [headerLabel setFont:[UIFont fontWithName:@"Helvetica" size:16.0]];
    [headerView addSubview:headerLabel];
    [headerLabel release];
    
    // Add the button
    UIButton *headerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [headerButton setFrame:kHeaderViewFrame];
    [headerButton setBackgroundColor:[UIColor clearColor]];
    [headerButton addTarget:self action:@selector(headerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    headerButton.tag = section;
    [headerView addSubview:headerButton];
    
    // Add the number frame
    
    // Number case image view
    if (section == 0 || section == 1) {
        UIImageView *numberCaseImageView = [[UIImageView alloc] initWithFrame:kNumberCaseFrame];
        [numberCaseImageView setImage:[UIImage imageNamed:@"NumberCase.png"]];
        [numberCaseImageView setBackgroundColor:[UIColor clearColor]];
        [headerView addSubview:numberCaseImageView];
        
        
        UILabel *numberCaseLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.5f, 2, 21, 15)];
        
        // 19.f, 34.f, 20.f, 20.f
        [numberCaseLabel setTextColor:[UIColor darkTextColor]];
        [numberCaseLabel setBackgroundColor:[UIColor clearColor]];
        [numberCaseLabel setTextAlignment:UITextAlignmentCenter];
        [numberCaseLabel setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
        
        NSString *numberOfItems = 0;
        switch (section) {
            case 0:
                numberOfItems = [NSString stringWithFormat:@"%d", [refTaskListCollection.lists count]];
                break;
            case 1:
                numberOfItems = [NSString stringWithFormat:@"%d", [refNotebookCollection.notebooks count]];
                break;
            default:
                break;
        }
        [numberCaseLabel setText:numberOfItems];
        [numberCaseImageView addSubview:numberCaseLabel];
        [numberCaseLabel release];
        
        [numberCaseImageView release]; 
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    //Tan Nguyen fix leaks
    static NSString *FooterIdentifier = @"Footer";
    
    UIView *footerView = (UIView* )[tableView dequeueReusableCellWithIdentifier:FooterIdentifier];
    if (footerView == nil) {
        footerView = [[[UIView alloc] initWithFrame:kFooterViewFrame] autorelease];
    }
    // Add a subview imageView
    UIImageView *footerImageView = [[UIImageView alloc] initWithFrame:kFooterViewFrame];
    [footerImageView setImage:[UIImage imageNamed:@"ControlPadFooter.png"]];
    [footerImageView setBackgroundColor:[UIColor colorWithRed:20 / 255.0 green:20 / 255.0 blue:20 / 255.0 alpha:1.0]];
    //[footerImageView setOpaque:YES];
    [footerView addSubview:footerImageView];
    [footerView setOpaque:YES];
    [footerImageView release];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 11.0;
}

#pragma mark - Button Actions

- (void)headerButtonAction:(id)sender {
    UIButton *headerButton = (UIButton *)sender;
    NSInteger section = headerButton.tag;
    
    if (section == 0) {
        // Lists
        if ([self listsCollapsed] == TRUE && selectedSection == section) {
            self.listsCollapsed = FALSE;
            [self updateSection:section forNumberOfRows:[refTaskListCollection.lists count]];
        } else if ([self listsCollapsed] == FALSE && selectedSection == section) {
            self.listsCollapsed = TRUE;
            [self updateSection:section forNumberOfRows:[refTaskListCollection.lists count]];
        } else {
            selectedSection = section;
            // Need to somehow update the tableview, create a delegate for this
            [self.delegate reloadListPadTableView];
            [self.delegate loadingListsSelected];
        }
    } else if (section == 1) {
        // Notebooks
        if ([self notebooksCollapsed] == TRUE && selectedSection == section) {
            self.notebooksCollapsed = FALSE;
            [self updateSection:section forNumberOfRows:[refNotebookCollection.notebooks count]];
        } else if ([self notebooksCollapsed] == FALSE && selectedSection == section) {
            self.notebooksCollapsed = TRUE;
            [self updateSection:section forNumberOfRows:[refNotebookCollection.notebooks count]];
        } else {
            selectedSection = section;
            
            // Need to update the tableview
            [self.delegate reloadListPadTableView];
            [self.delegate notebookSelected:0];
        }
        
    } else if (section == 2) {
        // Open archives
        selectedSection = section;
        [self.delegate reloadListPadTableView];
        [self.delegate archivesSelected];
    } else if (section == 3) {
        // Open up the shop
        selectedSection = section;
        [self.delegate reloadListPadTableView];
        [self.delegate shopfrontSelectedWithFrame:CGRectMake(236, 350, 1, 1)];
    }
}

- (void)updateSection:(NSInteger)section forNumberOfRows:(NSInteger)rows {
    NSMutableArray *indexPaths = [[[NSMutableArray alloc] init] autorelease];
    for (NSInteger i = 0; i < rows; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    
    switch (section) {
        case 0:
            if ([self listsCollapsed] == TRUE) {
                [self.delegate deleteRowsAtIndexPaths:indexPaths];
            } else {
                [self.delegate insertRowsAtIndexPaths:indexPaths];
            }
            
            break;

        case 1:
            if ([self notebooksCollapsed] == TRUE) {
                [self.delegate deleteRowsAtIndexPaths:indexPaths];
            } else {
                [self.delegate insertRowsAtIndexPaths:indexPaths];
            }
            
            break;
        default:
            break;
    }
    
}

#pragma mark -
#pragma mark Class Methods

- (void)tableView:(UITableView *)tableView forceSelectRowAtIndexPathRow:(NSInteger)theSelectedRow andSection:(NSInteger)theSelectedSection {
	selectedRow = theSelectedRow;
    selectedSection = theSelectedSection;
    if (theSelectedSection == 0 && self.listsCollapsed == TRUE) {
        return;
    } else if (theSelectedSection == 1 && self.notebooksCollapsed == TRUE) {
        return;
    }
    
	[tableView reloadData];
	[tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:theSelectedRow inSection:theSelectedSection] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}



@end
