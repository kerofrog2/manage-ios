//
//  SearchResultsViewController.h
//  Manage
//
//  Created by Cliff Viegas on 19/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SearchResultsViewController : UITableViewController <UISearchDisplayDelegate, UISearchBarDelegate> {
	NSArray			*listContent;				// The master content.
	NSArray			*listItemContent;
	NSMutableArray	*filteredListContent;		// The content filtered as a result of a search.
	NSMutableArray	*filteredListItemContent;
	
	// The saved state of the search UI if a memory warning removed the view.
    NSString		*savedSearchTerm;
    NSInteger		savedScopeButtonIndex;
    BOOL			searchWasActive;	
}

@property (nonatomic, retain) NSArray *listContent;
@property (nonatomic, retain) NSArray *listItemContent;
@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, retain) NSMutableArray *filteredListItemContent;

@property (nonatomic, copy) NSString *savedSearchTerm;
@property (nonatomic) NSInteger savedScopeButtonIndex;
@property (nonatomic) BOOL searchWasActive;

@end
