//
//  SearchDisplayTableViewController.h
//  Manage
//
//  Created by Cliff Viegas on 20/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SearchDisplayTableViewController : UITableViewController <UISearchDisplayDelegate, UISearchBarDelegate> {
	UISearchBar					*searchBar;
	UISearchDisplayController	*searchController;
}

@end
