//
//  ShopFrontTableViewCell.h
//  Manage
//
//  Created by Cliff Viegas on 20/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShopFrontTableViewCellDelegate
- (void)purchaseButtonPressed:(NSString *)productID;
- (void)previewButtonPressed:(NSString *)productID;
@end

@interface ShopFrontTableViewCell : UITableViewCell {
    UIImageView         *itemScreenshot;
    UILabel             *itemTypeLabel;
    UILabel             *itemTitleLabel;
    UILabel             *itemPriceLabel;
    UILabel             *itemDescriptionLabel;
    UIButton            *itemPreviewButton;
    UIButton            *itemPurchaseButton;
    NSString            *itemProductID;
    
    // Delegate
    id <ShopFrontTableViewCellDelegate> delegate;
}

@property (nonatomic, retain)   UIImageView     *itemScreenshot;
@property (nonatomic, retain)   UILabel         *itemTypeLabel;
@property (nonatomic, retain)   UILabel         *itemTitleLabel;
@property (nonatomic, retain)   UILabel         *itemPriceLabel;
@property (nonatomic, retain)   UILabel         *itemDescriptionLabel;
@property (nonatomic, retain)   UIButton        *itemPreviewButton;
@property (nonatomic, retain)   UIButton        *itemPurchaseButton;
@property (nonatomic, retain)   NSString        *itemProductID;
@property (nonatomic, assign)   id <ShopFrontTableViewCellDelegate> delegate;

@end
