//
//  InAppPurchaseManager.h
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 21/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Store Kit
#import <StoreKit/StoreKit.h>



// Constants
#define kInAppPurchaseManagerTransactionFailedNotification @"kInAppPurchaseManagerTransactionFailedNotification"
#define kInAppPurchaseManagerTransactionSucceededNotification @"kInAppPurchaseManagerTransactionSucceededNotification"
#define kInAppPurchaseManagerProductsFetchedNotification @"kInAppPurchaseManagerProductsFetchedNotification"

// Product id Constants
#define kInAppPurchasePremiumThemeNinjaBunniesID @"PTNINJABUNNIES01"
#define kInAppPurchasePremiumThemePurpleHazeID @"PTPURPLEHAZE01"
#define kInAppPurchasePremiumThemeOldTimerID @"PTOLDTIMER01"
#define kInAppPurchasePremiumThemeSweetCravingsID @"PTSWEETCRAVINGS01"
#define kInAppPurchasePremiumThemeNitroBurnID @"PTNITROBURN01"

// Receipt Constants
#define kInAppPurchasePremiumThemeNinjaBunniesReceipt @"premiumThemeNinjaBunniesReceipt"
#define kInAppPurchasePremiumThemePurpleHazeReceipt @"premiumThemePurpleHazeReceipt"
#define kInAppPurchasePremiumThemeOldTimerReceipt @"premiumThemeOldTimerReceipt"
#define kInAppPurchasePremiumThemeSweetCravingsReceipt @"premiumThemeSweetCravingsReceipt"
#define kInAppPurchasePremiumThemeNitroBurnReceipt @"premiumThemeNitroBurnReceipt"

// Provide Content Constants
#define kInAppPurchasePremiumThemeNinjaBunniesProvideContent @"premiumThemeNinjaBunniesProvideContent"
#define kInAppPurchasePremiumThemePurpleHazeProvideContent @"premiumThemePurpleHazeProvideContent"
#define kInAppPurchasePremiumThemeOldTimerProvideContent @"premiumThemeOldTimerProvideContent"
#define kInAppPurchasePremiumThemeSweetCravingsProvideContent @"premiumThemeSweetCravingsProvideContent"
#define kInAppPurchasePremiumThemeNitroBurnProvideContent @"premiumThemeNitroBurnProvideContent"

@interface InAppPurchaseManager : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    SKProduct               *proUpgradeProduct;
    SKProductsRequest       *productsRequest;
    NSMutableArray          *products;
}

@property   (nonatomic, retain)        NSMutableArray   *products;

+ (id)sharedPurchaseManager;

//+ (InAppPurchaseManager *)sharedPurchaseManager;

// Public methods
- (void)requestProUpgradeProductData;
- (void)loadStore;
- (BOOL)canMakePurchases;
- (void)purchaseUpgrade:(NSString *)productID;
- (NSString *)getPremiumThemeName:(NSString *)productID;
- (BOOL)contentPurchased:(NSString *)productID;

@end
