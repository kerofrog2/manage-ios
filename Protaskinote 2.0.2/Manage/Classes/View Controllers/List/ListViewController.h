//
//  ListViewController.h
//  Manage
//
//  Created by Cliff Viegas on 2/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Responders (import needed for delegate)
#import "ItemsDueTableViewResponder.h"

// For email
#import <MessageUI/MessageUI.h>

// Globals
#import "Globals.h"

// Progress Hud
#import "MBProgressHUD.h"

// Audio Toolbox
#import <AudioToolbox/AudioToolbox.h>

// Custom objects
@class CustomSegmentedControl;
#import "KFSegmentedControl.h"

// Data Models
@class TaskList;

// Globals
#import "Globals.h"

// Constants
#import "ListConstants.h"

// Data Collections
@class TagCollection;
@class ListItemTagCollection;
@class TaskListCollection;

// Pop Over Controllers (with required delegate link)
#import "NewListItemViewController.h"
#import "EditListItemViewController.h"
#import "HighlightersViewController.h"
#import "SettingsViewController.h"
#import "NewDueDateTableViewController.h"
#import "DateRangeViewController.h"
#import "DrawPadViewController.h"
#import "PngDrawPadViewController.h"

// Responders (with delegate)
#import "MiniListPadTableViewResponder.h"

// Calendar (for delegate)
#import "MiniCalendarView.h"

// Custom Objects
#import "ListItemTableViewCell.h"
@class CustomSegmentedControl;

// Enum
typedef enum EnumControlPanelType {
    EnumControlPanelTypeCalendarList = 0,
    EnumControlPanelTypeDrawingTools = 1
} EnumControlPanelType;

typedef enum EnumSearchRangeType {
    EnumSearchRangeTypeAllLists = 0,
    EnumSearchRangeTypeThisList = 1
} EnumSearchRangeType;

@protocol ListViewDelegate
- (void)swapWithNote:(TaskList *)noteTaskList usingFrame:(CGRect)frame;
@end

@interface ListViewController : UIViewController <PngDrawPadDelegate, ListPadDelegate, MiniCalendarDelegate, DateRangeDelegate, MBProgressHUDDelegate, KFSegmentedControlDelegate, UISearchBarDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, NewListItemDueDateDelegate, SettingsDelegate, UIAlertViewDelegate, UITextFieldDelegate, ItemsDueResponderDelegate, UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, NewListItemDelegate, ListItemDelegate, EditListItemDelegate, DrawPadDelegate, HighlightersPopoverDelegate> {
	id <ListViewDelegate>           delegate;
    
    // Data Models/Collections
	TaskList						*refTaskList;
	TaskListCollection				*refTaskListCollection;
	TaskListCollection				*refMasterTaskListCollection;
	NSInteger						currentTaskListID;
	
	// Search Objects
	TaskListCollection				*filteredSearchTaskListCollection;
	UISearchBar						*mySearchBar;
	UISegmentedControl				*searchScopeSegmentedControl;
	UIButton						*searchButton;
	UIButton						*clearSearchObjectsButton;
	BOOL							isSearchMode;
	UILabel							*searchTitleLabel;
    UILabel                         *searchDateRangeLabel;
    UIButton                        *searchDateRangeButton;
    UIButton                        *showAllTasksButton;
    UIButton                        *searchListRangeButton;
    NSInteger                       searchRangeType;
	
    // Progress HUD
    MBProgressHUD                   *progressHUD;
    
	// Popover Controllers
	UIPopoverController				*newListItemPopOverController;
	UIPopoverController				*editListItemPopOverController;
	BOOL							editListItemViewControllerAtBaseLevel;
	UIPopoverController				*drawPadPopOverController;
	UIPopoverController				*highlightersPopoverController;
	UIPopoverController				*filterTagsPopoverController;
	UIPopoverController				*settingsPopoverController;
	UIPopoverController				*dateLabelPopoverController;
	UIPopoverController             *dateRangePopoverController;
    
	// Backgrounds & Images
	UIImageView						*canvassImage;
	UIImageView						*borderImage;
	UIImageView						*pageImage;
	UIImageView						*listCompletedStampImage;
	
	// List objects
	UITableView						*myTableView;
	UIView							*headerLine1;
	UIView							*headerLine2;
	NSInteger						selectedRow;
	BOOL							itemsDueSelected;
	UIButton						*listOptionsButton;
	UIActionSheet					*listOptionsActionSheet;
	
	// Title, completed and date field
	UITextField						*titleTextField;
	UIButton						*dateLabelButton;
	UIButton						*listCompletedButton;
	
	// Items due tableview
	UITableView						*itemsDueTableView;
	UIImageView						*itemsDuePocket;
	ItemsDueTableViewResponder		*itemsDueTableViewResponder;
	
	// Top buttons
	UIButton						*editListButton;
	UIButton						*newListItemButton;
	UIButton						*doneEditingButton;
	UIButton						*newScribbleListItemButton;
	KFSegmentedControl				*sortItemsSegmentedControl;
	
	// Control Panel Objects
	UIImageView						*controlPanelImage;
	UIImageView						*highlightersContainerImageView;
	UIImageView						*pensContainerImageView;	
    UIImageView                     *calendarListsDividerImageView;
    UISegmentedControl              *controlPanelTypeSegmentedControl;
    UIView                          *drawingToolsContainerView;
    UIView                          *calendarListToolsContainerView;
    UIView                          *toolsContainerView;
	MiniListPadTableViewResponder   *miniListPadTableViewResponder;         // The responder for the list pad table view
	UITableView                     *miniListPadTableView;					// The list pad table view that sits on list pad image view
    UIImageView                     *dayCalendarImageView;
    UILabel                         *dayCalendarDateLabel;
    
	// Actual highlighter and pen buttons
	UIButton						*greenPenButton;
	UIButton						*redPenButton;
	UIButton						*bluePenButton;
	UIButton						*blackPenButton;
	
	UIButton						*yellowHighlighterButton;
	UIButton						*redHighlighterButton;
	UIButton						*whiteHighlighterButton;
	
	// Pen and Highlighter Icon Buttons
	UIButton						*penButton;
	UIButton						*highlighterButton;
	UIImageView						*selectTaskImageView;
	BOOL							selectRowPrompt;
	EnumHighlighterColor			highlighterSelected;
	EnumPenColor					penSelected;
	
	// Cover View
	UIImageView						*highlighterCoverView;
	BOOL							isHelpSelected;		// For covering when help overlay is on or off
	
	// Tag related properties/Objects
	TagCollection					*refTagCollection;
	UIButton						*filterTagsButton;
	UIButton						*filterTagsClearButton;
	ListItemTagCollection			*filterListItemTagCollection;
	TaskListCollection				*filterTagsTaskListCollection;
	UIView							*filterTagsFooterLine;
	UILabel							*filterTagsLabel;
	UIImageView						*filterTagsImageView;
	
	// Settings View Objects
	BOOL							settingsViewControllerCanClose;
	UIBarButtonItem					*settingsBarButtonItem;
	
	// Need a tile view frame for shrinking (if needed), normal shrink if this is cgrectzero
	CGRect							tileViewFrame;
    
    // Calendar
    MiniCalendarView                *miniCalendarView;
    NSString                        *searchStartDateRange;
    NSString                        *searchEndDateRange;
    
    // List View Controller Object Properties
    NSString                        *backButtonTitle;
}

@property (nonatomic, assign)   id                          delegate;
@property (nonatomic, assign)	CGRect                      tileViewFrame;
@property (nonatomic, assign)	BOOL                        isSearchMode;
@property (nonatomic, retain)   MiniCalendarView            *miniCalendarView;
@property (nonatomic, copy)     NSString                    *backButtonTitle;
@property (nonatomic, assign)   NSInteger                   searchRangeType;

#pragma mark Initialisation
- (id)initWithTaskList:(TaskList *)theList andTagCollection:(TagCollection *)theTagCollection 
 andTaskListCollection:(TaskListCollection *)theTaskListCollection 
andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection 
    andBackButtonTitle:(NSString *)theBackButtonTitle;


#pragma mark Send Mail Methods
- (void)sendTextMailForTaskList:(TaskList *)theTaskList;
- (void)sendTextMailForTaskListCollection:(TaskListCollection *)theTaskListCollection;
- (void)sendPDFMailForTaskList:(TaskList *)theTaskList;
- (void)sendPDFMailForTaskListCollection:(TaskListCollection *)theTaskListCollection;

@end
