    //
//  DaysOfTheWeekViewController.m
//  Manage
//
//  Created by Cliff Viegas on 10/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "DaysOfTheWeekViewController.h"

// Data Models
#import "RecurringListItem.h"
#import "DayOfTheWeek.h"

// Constants
#define kTableViewFrame				CGRectMake(0, 0, 320, 318)
#define kViewControllerSize			CGSizeMake(320, 318)

@interface DaysOfTheWeekViewController()
- (void)loadDaysOfTheWeekArray;
- (void)loadBarButtonItems;
- (void)loadTableView;
// Class Methods
- (void)updateRecurringListItemDays;
- (void)updateSelectedDays;
- (NSInteger)getArrayIndexOfDay:(NSString *)day;
@end


@implementation DaysOfTheWeekViewController

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[initialDaysOfTheWeek release];
	[weekDaysArray release];
	[myTableView release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithRecurringListItem:(RecurringListItem *)theRecurringListItem {
	if ((self = [super init])) {
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Assign the passed list item
		refRecurringListItem = theRecurringListItem;
		
		// Set the inital days of the week
		initialDaysOfTheWeek = [refRecurringListItem.daysOfTheWeek copy];
		
		// Get rid of frequency and frequency measurement
		refRecurringListItem.frequency = 0;
		refRecurringListItem.frequencyMeasurement = @"";
		
		// Init the week days array
		[self loadDaysOfTheWeekArray];
		
		// Update the selected days in days of week array
		[self updateSelectedDays];
		
		// Set the title
		self.title = @"Select Days";
		
		// Load the bar button items
		[self loadBarButtonItems];
		
		// Load the table view
		[self loadTableView];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadDaysOfTheWeekArray {
	weekDaysArray = [[NSMutableArray alloc] init];
	
	DayOfTheWeek *monday = [[[DayOfTheWeek alloc] initWithTitle:@"Monday" andSelected:FALSE andCompValue:2] autorelease];
	DayOfTheWeek *tuesday = [[[DayOfTheWeek alloc] initWithTitle:@"Tuesday" andSelected:FALSE andCompValue:3] autorelease];
	DayOfTheWeek *wednesday = [[[DayOfTheWeek alloc] initWithTitle:@"Wednesday" andSelected:FALSE andCompValue:4] autorelease];
	DayOfTheWeek *thursday = [[[DayOfTheWeek alloc] initWithTitle:@"Thursday" andSelected:FALSE andCompValue:5] autorelease];
	DayOfTheWeek *friday = [[[DayOfTheWeek alloc] initWithTitle:@"Friday" andSelected:FALSE andCompValue:6] autorelease];
	DayOfTheWeek *saturday = [[[DayOfTheWeek alloc] initWithTitle:@"Saturday" andSelected:FALSE andCompValue:7] autorelease];
	DayOfTheWeek *sunday = [[[DayOfTheWeek alloc] initWithTitle:@"Sunday" andSelected:FALSE andCompValue:1] autorelease];
	
	NSArray *localArray = [[NSArray alloc] initWithObjects:monday, tuesday, wednesday, thursday,
						   friday, saturday, sunday, nil];
	[weekDaysArray addObjectsFromArray:localArray];
	[localArray release];
	
}

- (void)loadBarButtonItems {
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
										  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
										  target:self
										  action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]
											initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
											target:self 
											action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
	[cancelBarButtonItem release];
}

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [weekDaysArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
	}

	// Default settings
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	DayOfTheWeek *day = [weekDaysArray objectAtIndex:indexPath.row];
	
	cell.textLabel.text = day.title;
	if ([day selected] == TRUE) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	DayOfTheWeek *day = [weekDaysArray objectAtIndex:indexPath.row];
	
	if ([day selected] == TRUE) {
		day.selected = FALSE;
	} else if ([day selected] == FALSE) {
		day.selected = TRUE;
	}
	
	// Need to update the recurring list item days
	[self updateRecurringListItemDays];
	
	[myTableView reloadData];
}

#pragma mark -
#pragma mark Button Actions

- (void)cancelBarButtonItemAction {
	refRecurringListItem.daysOfTheWeek = initialDaysOfTheWeek;
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)doneBarButtonItemAction {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Class Methods

- (void)updateRecurringListItemDays {
	refRecurringListItem.daysOfTheWeek = @"";
	
	BOOL firstRun = TRUE;
    
    // If weekdays array is empty
    if ([weekDaysArray count] == 0) {
        [weekDaysArray release];
        [self loadDaysOfTheWeekArray];
    }
    
	for (DayOfTheWeek *day in weekDaysArray) {
		if ([day selected] == TRUE) {
			if (firstRun == TRUE) {
				refRecurringListItem.daysOfTheWeek = day.title;
				firstRun = FALSE;
			} else {
				refRecurringListItem.daysOfTheWeek = [NSString stringWithFormat:@"%@, %@", refRecurringListItem.daysOfTheWeek, day.title];
			}
		}
	}
}

- (void)updateSelectedDays {
	NSArray *days = [refRecurringListItem.daysOfTheWeek componentsSeparatedByString:@","];
	
	// Reset all days to not selected
	for (DayOfTheWeek *dayOfTheWeek in weekDaysArray) {
		dayOfTheWeek.selected = FALSE;
	}
	
	for (NSString *day in days) {
		day = [day stringByReplacingOccurrencesOfString:@" " withString:@""];
		NSInteger index = [self getArrayIndexOfDay:day];
		
		if (index != -1) {
			DayOfTheWeek *dayOfTheWeek = [weekDaysArray objectAtIndex:index];
			dayOfTheWeek.selected = TRUE;
		}
	}
}

- (NSInteger)getArrayIndexOfDay:(NSString *)day {
	NSInteger i = 0;
	for (DayOfTheWeek *dayOfTheWeek in weekDaysArray) {
		if ([dayOfTheWeek.title isEqualToString:day]) {
			return i;
		}
		i++;
	}
	return -1;
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end
