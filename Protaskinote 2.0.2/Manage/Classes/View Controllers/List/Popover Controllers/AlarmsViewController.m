    //
//  AlarmsViewController.m
//  Manage
//
//  Created by Cliff Viegas on 16/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "AlarmsViewController.h"

// Data Collections
#import "AlarmCollection.h"

// Data Models
#import "Alarm.h"
#import "ListItem.h"

// Method Helper
#import "MethodHelper.h"

// View Controllers
#import "NewAlarmViewController.h"

// Constants
#define kViewControllerSize					CGSizeMake(320, 318)
#define kTableViewFrame						CGRectMake(0, 0, 320, 318)

@interface AlarmsViewController()
// Private methods
- (void)loadBarButtonItems;
- (void)loadTableView;
@end


@implementation AlarmsViewController

@synthesize	refPopoverController;
@synthesize currentListItemID;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[myTableView release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithAlarmCollection:(AlarmCollection *)theAlarmCollection andCurrentListItem:(ListItem *)theCurrentListItem {
	if (self = [super init]) {
		// Set the title
		self.title = @"Alarms";
		
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Assign the passed alarm collection
		refAlarmCollection = theAlarmCollection;
		
		// Assign the current list item reference
		refCurrentListItem = theCurrentListItem;
		
		// Load the bar button items
		[self loadBarButtonItems];
		
		// Load the tableview
		[self loadTableView];
	}
	
	return self;
}


#pragma mark -
#pragma mark Load Methods

- (void)loadBarButtonItems {
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
										  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
										  target:self
										  action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	// Hide the back bar button item
	self.navigationItem.hidesBackButton = YES;
	
	/*UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]
											initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
											target:self 
											action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
	[cancelBarButtonItem release];*/
}

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDataSource:self];
	[myTableView setDelegate:self];
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return [refAlarmCollection.alarms count];
	}
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}	
	
	// Default options
	cell.imageView.image = nil;
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	// Case options
	if (indexPath.section == 1) {
		cell.textLabel.text = @"Add new alarm";
		cell.imageView.image = [UIImage imageNamed:@"AlarmActiveIcon.png"];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	} else if (indexPath.section == 0) {
		Alarm *alarm = [refAlarmCollection.alarms objectAtIndex:indexPath.row];
		NSDate *theDate = [MethodHelper dateFromString:alarm.fireDateTime usingFormat:K_DATETIME_FORMAT];
		cell.textLabel.text = [MethodHelper localizedDateTimeFrom:theDate usingDateStyle:NSDateFormatterMediumStyle andTimeStyle:NSDateFormatterMediumStyle];
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
		// An existing alarm has been selected
		Alarm *alarm = [refAlarmCollection.alarms objectAtIndex:indexPath.row];
		NewAlarmViewController *controller = [[[NewAlarmViewController alloc] initWithExistingAlarmGUID:alarm.alarmGUID andCurrentListItem:refCurrentListItem 
																					 andAlarmCollection:refAlarmCollection] autorelease];
		controller.refPopoverController = refPopoverController;
		[self.navigationController pushViewController:controller animated:YES];	
	} else {
		NewAlarmViewController *controller = [[[NewAlarmViewController alloc] initWithCurrentListItem:refCurrentListItem
																				   andAlarmCollection:refAlarmCollection] autorelease];
		controller.refPopoverController = refPopoverController;
		[self.navigationController pushViewController:controller animated:YES];
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[tableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return @"";
}


//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {

  //  if (indexPath.row == 0 && indexPath.section == 0) {
//		return UITableViewCellEditingStyleInsert;
//	}
	
    return UITableViewCellEditingStyleNone;
}


#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidAppear:(BOOL)animated {
	if (refPopoverController) {
		[refPopoverController setPopoverContentSize:kViewControllerSize animated:YES];
	}
}

- (void)viewWillAppear:(BOOL)animated {
	[myTableView reloadData];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





@end
