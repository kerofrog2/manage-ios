//
//  NewListItemPopOver.h
//  Manage
//
//  Created by Cliff Viegas on 6/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class ListItem;

// Data Collections
@class ListItemTagCollection;
@class TagCollection;

// Custom Objects
@class CustomEdgeInsetsTextView;

// View Controllers (for delegate)
#import "NewDueDateTableViewController.h"
#import "NotesViewController.h"
#import "PriorityViewController.h"


// Tags for retrieving content views
#define	kTextViewTag				5384

// Constants
#define kTitleTextViewWidth         268

@protocol NewListItemDelegate
//- (void)createNewListItemWithTitle:(NSString *)theTitle andNotes:(NSString *)theNotes andCompleted:(BOOL)completed;
- (void)createNewListItemWithTitle:(NSString *)theTitle andNotes:(NSString *)theNotes 
					  andCompleted:(BOOL)completed andDueDate:(NSString *)theDate andPriority:(NSInteger)thePriority
		  andListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection;
- (NSInteger)createNewSubListItemWithTitle:(NSString *)theTitle andNotes:(NSString *)theNotes andCompleted:(BOOL)completed
						   andDueDate:(NSString *)theDate andParentListItem:(ListItem *)parentListItem
			 andListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection; //listItemTagCollection:(ListItemTagCollection *)listItemTagCollection;
- (void)dismissNewListItemPopOver;
@end

@interface NewListItemViewController : UIViewController <PriorityDelegate, NewListItemDueDateDelegate, UITableViewDelegate, UITableViewDataSource, NotesDelegate, UITextViewDelegate> {
	id <NewListItemDelegate> delegate;
	
	UITableView		*myTableView;
	CGSize			myPopoverSize;
	
	NSString		*listItemTitle;
	NSString		*listItemNotes;
    NSInteger       listItemPriority;
	BOOL			boxIsTicked;
	BOOL			didAppearFromListView;
	
	NSString		*currentDueDate;
    
	
	// Need a list items tag collection
	ListItemTagCollection	*listItemTagCollection;
	TagCollection			*refTagCollection;
	
	// Reference to the pop over controller
	UIPopoverController		*popoverController;
}

@property (nonatomic, copy)     NSString    *listItemTitle;
@property (nonatomic, copy)     NSString    *listItemNotes;
@property (nonatomic, copy)     NSString    *currentDueDate;
@property (nonatomic, assign)		id		delegate;
@property (nonatomic, retain)   ListItemTagCollection *listItemTagCollection;

#pragma mark Initialisation
- (id)initWithDelegate:(id<NewListItemDelegate>)theDelegate andSize:(CGSize)theSize andTagCollection:(TagCollection *)theTagCollection;

#pragma mark Ex Private Methods (Public for subclassing)
- (void)loadButtons;
- (void)loadTableView;
- (CustomEdgeInsetsTextView *)getTextView;
- (UILabel *)getLabel;

#pragma mark Public Set Methods
- (void)setPopoverController:(UIPopoverController *)thePopoverController;


@end