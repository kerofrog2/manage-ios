//
//  RecurringDetailsViewController.h
//  Manage
//
//  Created by Cliff Viegas on 9/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class ListItem;
@class RecurringListItem;

// View Controllers (with delegate)
#import "RepeatStartDateTableViewController.h"

@protocol RecurringDetailsDelegate
- (void)didUpdateWithRecurringListItemID:(NSInteger)recurringListItemID;
@end


@interface RecurringDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	id <RecurringDetailsDelegate>	delegate;
	RecurringListItem				*recurringListItem;		// Recurring list item, this will either be loaded or created
	NSMutableArray					*repeatOptionsArray;	// The options available for repeating
	ListItem						*refListItem;			// The passed list item to use
	UITableView						*myTableView;			// The table view for displaying the options available
	BOOL							isFromCompletedDate;	// Check for whether completed date is selected
	
	
	// Reference to the popover controller
	UIPopoverController				*refPopoverController;
}

@property (nonatomic, assign) id					delegate;
@property (nonatomic, assign) UIPopoverController	*refPopoverController;

#pragma mark Initialisation
- (id)initWithListItem:(ListItem *)theListItem;

@end
