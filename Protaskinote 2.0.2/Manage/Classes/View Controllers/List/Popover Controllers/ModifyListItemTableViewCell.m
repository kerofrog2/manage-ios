//
//  PopOverTableViewCell.m
//  Manage
//
//  Created by Cliff Viegas on 6/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "ModifyListItemTableViewCell.h"

// Views
#import "QuartzTagView.h"

// Data Collections
#import "TagCollection.h"
#import "ListItemTagCollection.h"

// Data Models
#import "Tag.h"
#import "ListItemTag.h"
#import "ListItem.h"

// Method Helper
#import "MethodHelper.h"

CGRect modifyScribbleRect = {36, 0, 230, 40};

@implementation ModifyListItemTableViewCell

@synthesize detailButton;
@synthesize customTextLabel;
@synthesize notesTextLabel;

@synthesize tagsView;
@synthesize tagsFont;
//@synthesize tagsRowHeight;

//@synthesize drawingLayer;
@synthesize scribbleImageView;

#pragma mark -
#pragma mark Memory Maangement

- (void)dealloc {
	//[self.detailButton release];
	//[self.customTextLabel release];
	//[self.notesTextLabel release];
	//[self.tagsView release];
	//[self.tagsFont release];
    //[self.drawingLayer release];
    //[self.scribbleImageView release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
        
        // Initialization code
		//detailButton = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 20, 20)];
		//detailButton = [[UIButton alloc] initWithFrame:CGRectMake(8, 8, 20, 20)];
		detailButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 3, 36, 36)];
		[detailButton setContentMode:UIViewContentModeCenter];
		
		[self.contentView addSubview:self.detailButton];
		//[self.detailButton release];
		
		customTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 10, 200, 24)];
		[customTextLabel setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[customTextLabel setTextColor:[UIColor darkTextColor]];
		[customTextLabel setHighlightedTextColor:[UIColor whiteColor]];
		[customTextLabel setBackgroundColor:[UIColor clearColor]];
		[self.contentView addSubview:self.customTextLabel];
		
		// Load the tags view
		self.tagsView = [[[UIView alloc] initWithFrame:CGRectMake(36, 12, 230, 20)] autorelease];
		[self.tagsView setBackgroundColor:[UIColor clearColor]];
		[self.contentView addSubview:self.tagsView];
		
		// Init the tags row height
		//self.tagsRowHeight = 44.0;
		
		// Init the tags font
		self.tagsFont = [UIFont fontWithName:@"Helvetica" size:11.0];
	
		self.notesTextLabel = [[[UILabel alloc] initWithFrame:CGRectMake(36, 12, 240, 20)] autorelease];
		[notesTextLabel setBackgroundColor:[UIColor clearColor]];
		[notesTextLabel setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[notesTextLabel setTextColor:[UIColor darkTextColor]];
		[notesTextLabel setLineBreakMode:UILineBreakModeWordWrap];
		[notesTextLabel setHighlightedTextColor:[UIColor whiteColor]];
		[notesTextLabel setNumberOfLines:0];
		[self.contentView addSubview:self.notesTextLabel];
        
        self.scribbleImageView = [[[UIImageView alloc] initWithFrame:modifyScribbleRect] autorelease];
        self.scribbleImageView.image = nil;
        [self.contentView addSubview:self.scribbleImageView];
    }
    return self;
}



#pragma mark -
#pragma mark Class Methods

- (void)loadScribbleImageForListItem:(ListItem *)listItem {
	// Display image if needed
	if ([[listItem scribble] length] > 0 && [listItem scribble] != nil) {
		self.scribbleImageView.image = nil;
        
       // if (self.drawingLayer != nil) {
        //    [self.drawingLayer.view removeFromSuperview];
        //    self.drawingLayer = nil;
        //}
		
        NSString *scribblePath;
        
        NSRange glsRange = [listItem.scribble rangeOfString:@".gls"];
        
        if (glsRange.location != NSNotFound) { 
            
            NSString *pngScribble = [listItem.scribble substringToIndex:glsRange.location];
            pngScribble = [NSString stringWithFormat:@"%@.png", pngScribble];
            
            scribblePath = [NSString stringWithFormat:@"%@%@",
                            [MethodHelper getScribblePath],
                            pngScribble];
            
            //[self loadDrawingLayerForListItem:listItem];
        } else {
            scribblePath = [NSString stringWithFormat:@"%@%@",
                            [MethodHelper getScribblePath],
                            listItem.scribble];
        }
		
		if ([listItem completed] == TRUE || [listItem archived] == TRUE) {
			[self.scribbleImageView setAlpha:0.2f];
		} else {
			[self.scribbleImageView setAlpha:1.0f];
		}
        
        UIImage *scribbleImage = [UIImage imageWithContentsOfFile:scribblePath];
        
		if (glsRange.location == NSNotFound) {
            scribbleImage = [MethodHelper scaleImage:scribbleImage 
                                            maxWidth:500 maxHeight:44];
        }
        
        scribbleImage = [MethodHelper cropImage:scribbleImage usingCropRect:CGRectMake(0, 0, 230, 44)];
        
        [self.scribbleImageView setFrame:modifyScribbleRect];
		
		[self.scribbleImageView setImage:scribbleImage];
	} else {
		self.scribbleImageView.image = nil;
	}
    

}

- (void)loadDrawingLayerForListItem:(ListItem *)listItem {
    
   /* drawingLayer = [GLDrawES2ViewController alloc];
    [drawingLayer initialFrame:CGRectMake(1500, 0, 1500, 132)];
    
    [drawingLayer.view setUserInteractionEnabled:FALSE];
    
    [drawingLayer drawFrame];
    drawingLayer.passThroughTouches = YES;
    //[self addSubview:drawingLayer.view];
    
    UIImage *myImage = [[[drawingLayer getUIImage] retain] autorelease];
    
    // need to squeeze into 209 by 40
    //[MethodHelper cropImage:<#(UIImage *)#> usingCropRect:<#(CGRect)#>
    
    myImage = [MethodHelper scaleImage:myImage maxWidth:500 maxHeight:44];
    myImage = [MethodHelper cropImage:myImage usingCropRect:CGRectMake(0, 0, 230, 44)];
    
    [self.scribbleImageView setImage:myImage];
    [self.scribbleImageView setFrame:modifyScribbleRect];
    */
    
    // if (isPreview) {
    //    [imageView setFrame:kListItemPreviewScribbleRect];
    //} else {
    //    [imageView setFrame:kListItemScribbleRect];
    //}
    
    //[drawingLayer.view removeFromSuperview];
    //drawingLayer = nil;
    //[self addSubview:drawingLayer.view];
    
    
    
    //[drawingLayer drawFrame];
    //[self performSelector:@selector(showDrawingFrame) withObject:nil afterDelay:0.0000001];
    // }
}

- (void)clearTagsView {
	// Remove all subviews first
	for (int i = [[self.tagsView subviews] count] - 1; i >= 0; i--) {
		[[[self.tagsView subviews] objectAtIndex:i] removeFromSuperview];
	}
}

// Careful about changing values here as they are also present in ListItemTagCollection
- (void)loadTagsFrom:(ListItemTagCollection *)listItemTagCollection andTagCollection:(TagCollection *)tagCollection {
	// Exit if there are no tags
	if ([listItemTagCollection.listItemTags count] == 0) {
		return;
	}
	
	CGFloat totalBuffer = 0.0;
	CGFloat tagBuffer = 10.0;
	CGFloat tagBorder = 8.0;
	
	CGFloat yBuffer = 0.0;
	
	//for (ListItemTag *listItemTag in listItemTagCollection.listItemTags) {
	for (Tag *theTag in tagCollection.tags) {
			
		if ([listItemTagCollection containsTagWithID:theTag.tagID] == FALSE) {
			continue;
		}
		
		NSString *tagName = theTag.name;//[tagCollection getNameForTagWithID:listItemTag.tagID];
		
		CGSize tagSize = [tagName sizeWithFont:self.tagsFont];
		
		if (totalBuffer + (tagSize.width + tagBorder) > self.tagsView.frame.size.width) {
			totalBuffer = 0.0;
			yBuffer += (self.tagsView.frame.size.height + tagBuffer);
			//self.tagsRowHeight += yBuffer;
		}
		
		// Add the label to the tag view
		UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(totalBuffer + (tagBorder / 2), 
																	  yBuffer, 
																	  tagSize.width + tagBorder, self.tagsView.frame.size.height)];
		
		totalBuffer = totalBuffer + tagLabel.frame.size.width + tagBuffer;
		
		
		
		[tagLabel setBackgroundColor:[UIColor clearColor]];
		[tagLabel setTextAlignment:UITextAlignmentCenter];
		[tagLabel setTextColor:[UIColor darkTextColor]];
		[tagLabel setText:tagName];
		[tagLabel setFont:self.tagsFont];
		
		// Create the quartz tag view
		QuartzTagView *quartzTagView = [[QuartzTagView alloc] initWithFrame:CGRectMake(tagLabel.frame.origin.x - (tagBorder / 2), 
																					   yBuffer, tagLabel.frame.size.width + tagBorder, 
																					   self.tagsView.frame.size.height)
                                        andTagColour:theTag.colour];
		// If this goes over task item then make it transparent
		[quartzTagView setAlpha:0.9f];
		[tagLabel setAlpha:0.9f];
		[self.tagsView addSubview:quartzTagView];
		[quartzTagView release];
		
		[self.tagsView addSubview:tagLabel];
		[tagLabel release];
	}
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
	//[self.customTextLabel setTextColor:[UIColor lightTextColor]];

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
	//[self.customTextLabel setTextColor:[UIColor lightTextColor]];
}





@end
