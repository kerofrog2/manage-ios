//
//  PriorityViewController.m
//  Manage - To Do Lists
//
//  Created by Cliffard Viegas on 16/10/11.
//  Copyright (c) 2011 kerofrog. All rights reserved.
//

#import "PriorityViewController.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@implementation PriorityViewController

@synthesize delegate;
@synthesize priority;

#pragma mark - Memory Management

- (void)dealloc {
    [myTableView release];
    [super dealloc];
}

#pragma mark - Initialisation

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (id)initWithDelegate:(id<PriorityDelegate>)theDelegate Priority:(NSInteger)thePriority {
    if (self = [super init]) {
        // Set the title
		self.title = @"Task Priority";
        
        self.delegate = theDelegate;
        self.priority = thePriority;
        
        [self setContentSizeForViewInPopover:kPopoverSize];
        
        
        [self loadTableView];
        
        
    }
    return self;
}


#pragma mark - Load Methods

- (void)loadTableView {
    myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}


#pragma mark - UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
	
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    cell.imageView.image = [UIImage imageNamed:@"PriorityIcon.png"];
    
    if ([self priority] == indexPath.row) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    
    switch (indexPath.row) {
        case 0:
            [cell.textLabel setText:@"Normal"];
            [cell.textLabel setTextColor:[UIColor darkTextColor]];
            break;
        case 1:
            [cell.textLabel setText:@"Important"];
            [cell.textLabel setTextColor:[UIColor orangeColor]];
            break;
        case 2:
            [cell.textLabel setText:@"Critical"];
            [cell.textLabel setTextColor:[UIColor redColor]];
            break;
        default:
            break;
    }

    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.priority = indexPath.row;
    [myTableView reloadData];
	
    [self.delegate priorityUpdated:self.priority];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
