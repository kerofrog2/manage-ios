//
//  PriorityViewController.h
//  Manage - To Do Lists
//
//  Created by Cliffard Viegas on 16/10/11.
//  Copyright (c) 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PriorityDelegate
- (void)priorityUpdated:(NSInteger)newPriority;
@end

@interface PriorityViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    id <PriorityDelegate>       delegate;
    NSInteger                   priority;
    
    UITableView                 *myTableView;
}

@property (nonatomic, assign)		id			delegate;
@property (assign)                  NSInteger   priority;

#pragma mark Initialisation
- (id)initWithDelegate:(id<PriorityDelegate>)theDelegate Priority:(NSInteger)thePriority;

#pragma mark Load Methods
- (void)loadTableView;

@end
