//
//  FastTableViewCell.h
//  Manage
//
//  Created by Cliff Viegas on 17/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>


// to use: subclass FastTableViewCell and implement -drawContentView:

@interface FastTableViewCell : UITableViewCell
{
	UIView *contentView;
}

- (void)drawContentView:(CGRect)r; // subclasses should implement

@end
