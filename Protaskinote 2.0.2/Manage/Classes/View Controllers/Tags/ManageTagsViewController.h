//
//  ManageTagsViewController.h
//  Manage
//
//  Created by Cliff Viegas on 16/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TagCollection;

// New Tag Delegate
#import "NewTagDelegate.h"

// View Controllers (for delegates)
#import "EditTagViewController.h"

@interface ManageTagsViewController : UIViewController <EditTagDelegate, UIAlertViewDelegate, NewTagDelegate, UITableViewDelegate, UITableViewDataSource> {
	TagCollection		*refTagCollection;
	TagCollection		*tagCollection;
	UITableView			*myTableView;
	UIToolbar			*myToolbar;
	
	NSInteger			selectedTagID;
	NSInteger			manageTagsDepth;
	NSInteger			manageParentTagID;
	
	// Edit bar button item
	UIBarButtonItem		*editBarButtonItem;
	UIBarButtonItem		*newTagBarButtonItem;
	UIBarButtonItem		*doneBarButtonItem;
}

#pragma mark Initialisation
- (id)initWithTagCollection:(TagCollection *)theTagCollection;
- (id)initWithTagCollection:(TagCollection *)theTagCollection forParentTagID:(NSInteger)theParentTagID
		   andParentTagName:(NSString *)theParentTagName andSelectedTagID:(NSInteger)theSelectedTagID andManageTagsDepth:(NSInteger)theManageTagsDepth;

@end
