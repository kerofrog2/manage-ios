//
//  FilterListItemTagViewController.h
//  Manage
//
//  Created by Cliff Viegas on 26/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TagCollection;
@class ListItemTagCollection;

@interface FilterListItemTagViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	UITableView				*myTableView;
	TagCollection			*refTagCollection;
	ListItemTagCollection	*refFilterListItemTagCollection;
}

#pragma mark Initialisation
- (id)initWithFilterListItemTagCollection:(ListItemTagCollection *)theFilterListItemTagCollection 
						 andTagCollection:(TagCollection *)theTagCollection;
@end
