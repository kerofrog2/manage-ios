    //
//  EditTagViewController.m
//  Manage
//
//  Created by Cliff Viegas on 25/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "EditTagViewController.h"

// Data Models
#import "ApplicationSetting.h"
#import "Tag.h"
#import "PropertyDetail.h"

// Data Collections
#import "TagCollection.h"

// Custom View Objects
#import "QuartzTagView.h"

// Quartz
#import <QuartzCore/QuartzCore.h>

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)
#define kPopoverFrame		CGRectMake(0, 0, 320, 318)
#define kTextFieldTag		86551
#define kTextFieldFrame		CGRectMake(43, 12, 247, 23)

@interface EditTagViewController()
- (void)loadButtons;
- (void)loadTableView;
// Helper Methods
- (UITextField *)getTextFieldForIndexPath:(NSIndexPath *)indexPath;
@end


@implementation EditTagViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    [tagColourSetting release];
    [tagColourPropertyDetail release];
	[myTableView release];
	[doneBarButtonItem release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<EditTagDelegate>)theDelegate andTagCollection:(TagCollection *)theTagCollection 
		 andEditingTag:(Tag *)theEditingTag andSelectedParentTagID:(NSInteger)theSelectedParentTagID {
	if ((self = [super init])) {
		// Assign the tag collection
		refTagCollection = theTagCollection;
		
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the editing tag
		refSelectedTag = theEditingTag;
		
        // Setup tag colour setting
        tagColourSetting = [[ApplicationSetting alloc] init];
        tagColourSetting.name = @"TagColour";               // This doesn't matter
        tagColourSetting.data = refSelectedTag.colour;         // Default tag colour
        
        // Setup the property detail
        tagColourPropertyDetail = [[PropertyDetail alloc] initWithName:@"TagColour" 
                                                       andFriendlyName:@"Tag Colour" 
                                                            andSection:@"" 
                                                       andPropertyType:EnumPropertyTypeList];
        NSArray *tagColourList = [NSArray arrayWithObjects:@"Brown", @"Blue", @"Red", @"Green", @"Yellow", @"Orange", @"Purple", @"Pink",  nil];
        tagColourPropertyDetail.description = @"The colour to use for this tag";
        [tagColourPropertyDetail.list addObjectsFromArray:tagColourList];
        
		// Assign the selected tag ID
		selectedParentTagID = theSelectedParentTagID;
		
		// Set the title
		self.title = @"Edit Tag";
		
		// Load the table view
		[self loadTableView];
		
		// Load the buttons
		[self loadButtons];
		
		// Set popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadButtons {
	doneBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" 
														 style:UIBarButtonItemStyleDone 
														target:self 
														action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:YES];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" 
																			style:UIBarButtonItemStyleBordered 
																		   target:self 
																		   action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:YES];
	[cancelBarButtonItem release];
	
}

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	[myTableView setScrollEnabled:NO];
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	
	// Don't make any changes if it is the same
	if (refSelectedTag.parentTagID == selectedParentTagID &&
		[[myTextField text] isEqualToString:refSelectedTag.name]
        && [refSelectedTag.colour isEqualToString:tagColourSetting.data]) {
		[self.navigationController popViewControllerAnimated:YES];
		return;
	}
	
	// Need to update the tag depth
	if (selectedParentTagID == -1) {
		refSelectedTag.tagDepth = 0;
	} else {
		Tag *parentTag = [refTagCollection getTagWithID:selectedParentTagID];
		refSelectedTag.tagDepth = parentTag.tagDepth + 1;
	}
	
	// Also need to update childrens depth
	[refTagCollection updateChildrenTagDepthForParentTag:refSelectedTag];
	
	// Also need to get the correct tag order for the new depth
	refSelectedTag.tagOrder = [refTagCollection getNextTagOrderForParentTagID:selectedParentTagID];
	
	// Update the tag
	refSelectedTag.parentTagID = selectedParentTagID;
	refSelectedTag.name = [myTextField text];
	refSelectedTag.colour = tagColourSetting.data;
    
	[delegate tagEdited:refSelectedTag];
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }
    
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		
		[cell.contentView addSubview:[self getTextFieldForIndexPath:indexPath]];
	}	
	
	// Get content view objects
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	
	// Default values
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	[cell setAccessoryType:UITableViewCellAccessoryNone];
	[cell.textLabel setText:@""];
	[cell.textLabel setTextColor:[UIColor darkTextColor]];
	cell.imageView.image = nil;
	
	if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [myTextField setHidden:NO];
            [myTextField setText:refSelectedTag.name];
            cell.imageView.image = [UIImage imageNamed:@"TagNoRightBuffer.png"];  
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"Colour";
            [myTextField setHidden:YES];
            cell.detailTextLabel.text = tagColourSetting.data;
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            // Set the image view
            QuartzTagView *tagView = [[[QuartzTagView alloc] initWithFrame:CGRectMake(0, 0, 20, 20) 
                                                              andTagColour:tagColourSetting.data] autorelease];
            UIGraphicsBeginImageContext(tagView.bounds.size);
            [tagView.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            cell.imageView.image = viewImage;
        }
	} else {
		[myTextField setHidden:YES];
		[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		[cell.textLabel setTextColor:[UIColor colorWithRed:0.243 green:0.306 blue:0.435 alpha:1.0]];
		
		if (selectedParentTagID == -1) {
			[cell.textLabel setText:@"Tags"];
		} else {
			NSInteger tagIndex = [refTagCollection getIndexOfTagWithID:selectedParentTagID];
			cell.textLabel.text = [[refTagCollection.tags objectAtIndex:tagIndex] name];
		}
	}
	
	// Set cell properties
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 1) {
		// Need to have a new tag collection without children tags
		NSMutableArray *tagFamilyToIgnore = [[refTagCollection getFamilyOfTagsCopyForParentTag:refSelectedTag] retain];
		TagCollection *tagCollection = [[[TagCollection alloc] initWithAllTagsIgnoringTagFamily:tagFamilyToIgnore] autorelease];
		[tagFamilyToIgnore release];
		
		SelectListItemTagViewController *controller = [[[SelectListItemTagViewController alloc] initWithDelegate:self 
																								andTagCollection:tagCollection 
																								andSelectedTagID:selectedParentTagID] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if (indexPath.section == 0) {
        if (indexPath.row == 1) {
            ListSettingViewController *controller = [[[ListSettingViewController alloc] initWithDelegate:self 
                                                                                           andAppSetting:tagColourSetting 
                                                                                       andPropertyDetail:tagColourPropertyDetail] autorelease];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	if (section == 0) {
		return 75.0;
	}
	return 0.0;
}

- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
}


#pragma mark -
#pragma mark Select List Item Tag Delegates

- (void)tagIDSelected:(NSInteger)theSelectedTagID {
	selectedParentTagID = theSelectedTagID;
	[myTableView reloadData];
}

#pragma mark -
#pragma mark UITextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	
	// To handle text field should return
	// Will pop view controller and store value
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	
	if ([myTextField isFirstResponder]) {
		[myTextField resignFirstResponder];
	}
	
	return YES;
}

//- (void)textFieldDidChange:(NSNotification *)theNotification {
/*UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
 UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
 
 
 if ([myTextField.text length] == 0) {
 [doneBarButtonItem setEnabled:FALSE];
 }
 
 [doneBarButtonItem setEnabled:TRUE];*/
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	if (range.location == 0 && range.length == 0) {
		[doneBarButtonItem setEnabled:TRUE];
		return YES;
	} else if (range.location == 0 && range.length == [textField.text length]) {
		[doneBarButtonItem setEnabled:FALSE];
		return YES;
	}
	
	[doneBarButtonItem setEnabled:TRUE];
	
	return YES;
}

#pragma mark -
#pragma mark Helper Methods

- (UITextField *)getTextFieldForIndexPath:(NSIndexPath *)indexPath {
	UITextField *textField = [[[UITextField alloc] initWithFrame:kTextFieldFrame] autorelease];
	
	if (indexPath.section == 0) {
		[textField becomeFirstResponder];
	}
	
	[textField setBackgroundColor:[UIColor clearColor]];
	[textField setDelegate:self];
	[textField setText:@""];
	[textField setAutocorrectionType:UITextAutocorrectionTypeNo];
	[textField setTag:kTextFieldTag];
	[textField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19.0]];
	//[textField setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
	[textField setTextColor:[UIColor darkTextColor]];
	[textField setReturnKeyType:UIReturnKeyDone];
	//[textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
	
	return textField;
}

#pragma mark -
#pragma mark List Setting Delegates

- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData {
    tagColourSetting.data = theData;        // Copy property, auto-copy
    [myTableView reloadData];
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewDidAppear:(BOOL)animated {
	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextField *myTextField = (UITextField *)[cell.contentView viewWithTag:kTextFieldTag];
	if ([myTextField isFirstResponder] == FALSE) {
		[myTextField becomeFirstResponder];
	}
	
	[super viewDidAppear:animated];
}


-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
