//
//  DataImportViewController.h
//  Manage
//
//  Created by Cliff Viegas on 26/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DataImportViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	UITableView					*myTableView;
	NSArray						*importListArray;
}

@end
