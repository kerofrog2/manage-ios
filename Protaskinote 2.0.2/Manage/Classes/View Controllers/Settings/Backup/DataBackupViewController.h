//
//  DatabaseBackupViewController.h
//  Manage
//
//  Created by Cliff Viegas on 16/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// View Controllers
#import "RestoreExportDataViewController.h"

@protocol DataBackupDelegate
- (void)dataSuccessfullyBackedUp;
- (void)restoreDataComplete;
@end


@interface DataBackupViewController : UIViewController <RestoreExportDataDelegate, UITableViewDelegate, UITableViewDataSource> {
	id <DataBackupDelegate>	delegate;
	
	UITableView					*myTableView;
	
	NSArray						*backupListArray;
	NSArray						*importedBackupListArray;
}

@property (nonatomic, assign)	id		delegate;

@end
