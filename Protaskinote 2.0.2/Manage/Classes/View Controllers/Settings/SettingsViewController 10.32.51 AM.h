//
//  SettingsViewController.h
//  Manage
//
//  Created by Cliff Viegas on 6/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class ApplicationSettingCollection;
@class TagCollection;
@class TaskListCollection;

// For enums
#import "PropertyCollection.h"

// View Controllers (for delegates)
#import "SynchronizationViewController.h"
#import "ListSettingViewController.h"
#import "DataBackupViewController.h"

// Delegate
@protocol SettingsDelegate
- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData;
- (void)settingsPopoverCanClose:(BOOL)canClose;
- (void)closeSettingsPopover;
- (void)updatesToDataComplete;
- (void)replaceToodledoDataComplete;
- (void)shopfrontSelected;
@end

@interface SettingsViewController : UIViewController <DataBackupDelegate, SynchronizationDelegate, ListSettingDelegate, UITableViewDelegate, UITableViewDataSource> {
	id <SettingsDelegate>			delegate;
	
	ApplicationSettingCollection	*applicationSettingCollection;
	PropertyCollection				*propertyCollection;
	
	UITableView						*myTableView;
	TagCollection					*refTagCollection;
	
	TaskListCollection				*refMasterTaskListCollection;
	
	UIPopoverController				*refPopoverController;
}

@property (nonatomic, assign)	id						delegate;
@property (nonatomic, assign)	UIPopoverController		*refPopoverController;

#pragma mark Initialisation
- (id)initWithDelegate:(id<SettingsDelegate>)theDelegate andTagCollection:(TagCollection *)theTagCollection 
			andPropertyCollectionType:(NSInteger)propertyCollectionType 
				andTaskListCollection:(TaskListCollection *)theTaskListCollection;
@end
