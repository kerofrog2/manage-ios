//
//  SyncToodledoViewController.h
//  Manage
//
//  Created by Cliff Viegas on 9/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Import toodledo account info (also for delegate)
#import "ToodledoAccountInfo.h"

// Import toodledo authentication (for delegate) and when we may need to reconnect
#import "ToodledoAuthentication.h"

// View Controllers
#import "ToodledoSyncViewController.h"

// Data Collections
@class TaskListCollection;

// Constants
#define kPopoverSize		CGSizeMake(320, 318)

// Delegate
@protocol SynchronizationDelegate
- (void)settingsPopoverCanClose:(BOOL)canClose;
- (void)closeSettingsPopover;
- (void)replaceLocalTasksSyncComplete;
- (void)replaceToodledoDataComplete;
@end


@interface SynchronizationViewController : UIViewController <UIAlertViewDelegate, ToodledoSyncDelegate, ToodledoAuthenticationDelegate, ToodledoAccountInfoDelegate, UITableViewDelegate, UITableViewDataSource> {
	id <SynchronizationDelegate>	delegate;
	
	ToodledoAccountInfo				*toodledoAccountInfo;
	ToodledoAuthentication			*toodledoAuthentication;
	UITableView						*myTableView;
	NSString						*toodledoServiceStatus;
	NSString						*toodledoErrorMessage;
	
	TaskListCollection				*refMasterTaskListCollection;
	
	BOOL							getAccountInfoOnceFlag;
	
	UIPopoverController				*refPopoverController;
}

@property (nonatomic, assign)	id						delegate;
@property (nonatomic, assign)	UIPopoverController		*refPopoverController;

@property (nonatomic, copy)		NSString		*toodledoServiceStatus;
@property (nonatomic, copy)		NSString		*toodledoErrorMessage;
@property (nonatomic, assign)	BOOL			getAccountInfoOnceFlag;

#pragma mark Initialisation
- (id)initWithMasterTaskListCollection:(TaskListCollection *)theTaskListCollection;

#pragma mark Helper Methods
- (void)loadTableView;
- (UITextField *)getTextFieldForIndexPath:(NSIndexPath *)indexPath;

@end
