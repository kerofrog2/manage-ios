//
//  SyncToodledoNowViewController.h
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Toodledo Collections
@class ToodledoTaskCollection;
@class ToodledoFolderCollection;

// Data Collections
@class TaskListCollection;
@class TagCollection;
@class ToodledoAccountInfo;
@class ArchiveList;

// Data Models
#import "ToodledoFolder.h"

@protocol SyncToodledoNowDelegate
- (void)syncToodledoNowComplete;
- (void)closeSettingsPopover;
@end


@interface SyncToodledoNowViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	id <SyncToodledoNowDelegate>		delegate;
	
	// Data Collections
	TaskListCollection					*refMasterTaskListCollection;
	ToodledoTaskCollection				*toodledoTaskCollection;
	ToodledoFolderCollection			*toodledoFolderCollection;
	TagCollection						*tagCollection;
	ArchiveList							*archiveList;
	NSMutableArray						*alreadyUpdatedLocalTasks;
	ToodledoTaskCollection				*toodledoSubtaskCollection;
	
	// Used for handling when toodledo changes folders on tasks and subtasks
	NSMutableArray						*moveItemsLocalTasksArray;
	NSMutableArray						*moveSubItemsLocalTasksArray;
	
	
	
	// Data Models
	ToodledoAccountInfo					*refToodledoAccountInfo;
	
	// Helper variables
	NSString							*toodledoKey;
	NSString							*lastSyncDateTimeString;
	BOOL								toodledoServerHasConflictPriority;	// For seeing who has priority in event of conflicts
	BOOL								toodledoFoldersHaveBeenCollected;
	BOOL								localChangesHaveBeenMade;
	
	// UIKit Objects
	UIBarButtonItem						*cancelBarButtonItem;
	UIBarButtonItem						*doneBarButtonItem;
	UITextView							*logTextView;
	UILabel								*syncLabel;
	UIActivityIndicatorView				*syncActivityIndicatorView;
	UITableView							*myTableView;
	
	// Keep track of Backup path
	NSString							*backupPath;
	NSMutableString						*logContent;
	BOOL								dataHasChanged;	// Need this to see whether we need to restore on a 'cancel'
	
}

@property (nonatomic, assign)	id			delegate;
@property (nonatomic, copy)		NSString	*toodledoKey;
@property (nonatomic, copy)		NSString	*lastSyncDateTimeString;
@property (nonatomic, assign)	BOOL		toodledoServerHasConflictPriority;
@property (nonatomic, assign)	BOOL		toodledoFoldersHaveBeenCollected;
@property (nonatomic, assign)	BOOL		localChangesHaveBeenMade;

#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection 
		  andToodledoAccountInfo:(ToodledoAccountInfo *)theAccountInfo;

#pragma mark Subtask Methods
- (void)actionLocalSubtasksFromCollection:(TaskListCollection *)taskListCollection;
- (void)updateListItemsThatNeedToBeSubtasks;

#pragma mark Helper Methods
- (void)setSyncNowTitle:(NSString *)newTitle;

@end
