//
//  ToodledoSyncViewController.h
//  Manage
//
//  Created by Cliff Viegas on 13/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections (also for delegate)
//#import "ToodledoFolderCollection.h"
//#import "ToodledoTaskCollection.h"
@class TaskListCollection;
@class ToodledoAccountInfo;

// Responders
@class LocalResultsTableViewResponder;
@class ToodledoResultsTableViewResponder;

// Data Models
@class PropertyDetail;
@class ApplicationSetting;

// View Controllers
#import "ReplaceLocalTasksViewController.h"
#import "ReplaceToodledoTasksViewController.h"
#import "SyncToodledoNowViewController.h"
#import "ListSettingViewController.h"

// Delegate
@protocol ToodledoSyncDelegate
- (void)settingsPopoverCanClose:(BOOL)canClose;
- (void)closeSettingsPopover;
- (void)replaceLocalTasksSyncComplete;
- (void)replaceToodledoDataComplete;
@end


@interface ToodledoSyncViewController : UIViewController <ListSettingDelegate, SyncToodledoNowDelegate, UIAlertViewDelegate, ReplaceToodledoTasksDelegate, ReplaceLocalTasksDelegate, UITableViewDelegate, UITableViewDataSource> {
	id <ToodledoSyncDelegate>			delegate;
	
	UITableView							*localTableView;
	UITableView							*toodledoTableView;
	UITableView							*myTableView;
	TaskListCollection					*refMasterTaskListCollection;
	ToodledoAccountInfo					*refToodledoAccountInfo;
	
	// Conflict Priority settings
	ApplicationSetting					*conflictPriorityAppSetting;
	PropertyDetail						*conflictPriorityPropertyDetail;
	
	// Responders
	LocalResultsTableViewResponder		*localResultsTableViewResponder;
	ToodledoResultsTableViewResponder	*toodledoResultsTableViewResponder;
	
	UIPopoverController					*refPopoverController;
}

@property (nonatomic, assign)	id						delegate;
@property (nonatomic, assign)	UIPopoverController		*refPopoverController;

#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection
		  andToodledoAccountInfo:(ToodledoAccountInfo *)theAccountInfo;

@end
