    //
//  ToodledoSyncInstructionsViewController.m
//  Manage
//
//  Created by Cliff Viegas on 16/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ToodledoSyncInstructionsViewController.h"

// Constants
#define kPopoverSize				CGSizeMake(320, 318)
#define kMyTextViewFrame			CGRectMake(0, 0, 320, 318)

@interface ToodledoSyncInstructionsViewController()
- (void)loadTextView;
@end


@implementation ToodledoSyncInstructionsViewController

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[myTextView release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if ((self = [super init])) {
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Set the title
		self.title = @"Instructions";
		
		// We just gonna have a text view...
		[self loadTextView];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTextView {
	NSString *textViewText = @"\nFIRST TOODLEDO SYNC\n"
							"The first sync requires us to do an initial full sync which involves either:\n\n"
							"1. Replacing your Toodledo tasks with a copy of your local tasks, or\n"
							"2. Replacing your local tasks with a copy of your Toodledo tasks.\n\n"
							"This enables us to "
							"create a 'link' between the tasks which will then allow accurate syncing.\n\n"
							
							"You may then select the 'Synchronize Now' button at any time to sync your tasks.\n\n"
	
							"RECOMMENDATIONS\n"
							"It is recommended that you do not have lists with duplicate titles when syncing, as Toodledo does "
							"not support duplicate folder names.  Backing up your data before large syncs is also recommended.\n\n"
	
							"ISSUES WITH SYNC?\n"
							"If for whatever reason a normal sync has any errors, then you can use a full sync to "
							"correctly sync your data.  It is recommended that you perform a backup before any large scale sync.\n\n"
							"Note: A backup is automatically performed for you on a 'Replace Local Tasks' full sync.\n\n"
	
							"LIMITATIONS\n"
							"Currently the only limitation is when changing the folder of a subtask on Toodledo. Manage will automatically "
							"reset the subtask to the original Toodledo folder.\n\n"
							"If you wish to move "
							"a Toodledo task to a new folder then please only change the parent task folder.  Manage will automatically "
							"move the parent task and any subtasks to the new folder (list) when syncing.\n\n";
	
	myTextView = [[UITextView alloc] initWithFrame:kMyTextViewFrame];
	[myTextView setEditable:FALSE];
	[myTextView setText:textViewText];
	[myTextView setTextColor:[UIColor darkTextColor]];
	[self.view addSubview:myTextView];
	
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





@end
