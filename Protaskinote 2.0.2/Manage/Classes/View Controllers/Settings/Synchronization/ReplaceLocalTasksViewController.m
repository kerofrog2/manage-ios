    //
//  ReplaceLocalTasksViewController.m
//  Manage
//
//  Created by Cliff Viegas on 17/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ReplaceLocalTasksViewController.h"


// Data Models
#import "ApplicationSetting.h"
#import "ToodledoTask.h"
#import "TaskList.h"
#import "ListItem.h"
#import "Tag.h"
#import "ListItemTag.h"
#import "RecurringListItem.h"
#import "ArchiveList.h"
#import "ToodledoAccountInfo.h"
#import "DeletedTaskList.h"
#import "DeletedListItem.h"

// Data Collections
#import "TaskListCollection.h"
#import "TagCollection.h"
#import "ListItemTagCollection.h"

// Toodledo Collections
#import "ToodledoTaskCollection.h"
#import "ToodledoFolderCollection.h"

// Method Helper
#import "MethodHelper.h"

// Business Layer
#import "BLTag.h"
#import "BLListItemTag.h"
#import "BLRecurringListItem.h"

// JSON
#import "JSON.h"

// Zip Archive
#import "ZipArchive.h"

// Constants
#import "ListConstants.h"
#define kPopoverSize				CGSizeMake(320, 318)
#define kLogTextViewFrame			CGRectMake(10, 0, 280, 250)
#define kActivityIndicatorFrame		CGRectMake(65, 278, 20, 21)
#define kSyncLabelFrame				CGRectMake(0, 258, 320, 60)
#define kTableViewFrame				CGRectMake(0, 0, 320, 318)
#define kTextViewTag				9228

@interface ReplaceLocalTasksViewController()
- (void)synchronize;
- (void)createToodledoTasksFolderIfRequired;
- (void)updateToodledoTasksWithNoFolderUsingFolderID:(SInt64)folderID;
- (void)createNewListsFromToodledoFolders;
// Create local tasks
- (void)createLocalTagsFromToodledoTasks;
- (void)createLocalTasksFromToodledoTasks;
- (NSInteger)createRecurringListItemFromRepeatString:(NSString *)repeatString andRepeatFrom:(NSInteger)repeatFrom;
- (void)createListItemTagsFromToodledoTaskTags;
- (void)updateListItemsThatNeedToBeSubtasks;
- (void)addLinkingDataToToodledoTasks;
// Load methods
- (void)loadTableView;
- (void)loadBarButtonItems;
- (void)loadTextView;
- (UITextView *)getTextView; 
- (void)loadLabel;
- (void)loadActivityView;
// Class Method Helpers
- (void)updateLogTextView:(NSString *)newText;
- (void)sameThreadTextViewUpdate:(NSString *)newText;
@end




@implementation ReplaceLocalTasksViewController

@synthesize delegate;
@synthesize toodledoKey;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[toodledoTaskCollection release];
	[toodledoFolderCollection release];
	[tagCollection release];
	
	// UIKit objects
	[logTextView release];
	[syncLabel release];
	[syncActivityIndicatorView release];
	[myTableView release];
	
	[logContent release];
	
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection 
		  andToodledoAccountInfo:(ToodledoAccountInfo *)theAccountInfo {
	if ((self = [super init])) {
		// Assign the passed master task list collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Assign the passed toodledo account info
		refToodledoAccountInfo = theAccountInfo;
		
		// Set background colour
		self.view.backgroundColor = [UIColor lightTextColor];
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Set the title
		self.title = @"Local Replace";
		
		// Init data has changed to false
		dataHasChanged = FALSE;
		
		// Init log content
		logContent = [[NSMutableString alloc] initWithString:@""];
		
		// Load tableview
		[self loadTableView];
		
		// Load the bar button items
		[self loadBarButtonItems];
		
		// Load the logging text view
		[self loadTextView];
		
		// Load the label
		[self loadLabel];
		
		// Load the activity indicator
		[self loadActivityView];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setScrollEnabled:FALSE];
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

- (void)loadBarButtonItems {
	doneBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" 
														 style:UIBarButtonItemStyleDone 
														target:self 
														action:@selector(doneBarButtonItemAction)];
	[doneBarButtonItem setEnabled:FALSE];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:YES];
	
	cancelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" 
																			style:UIBarButtonItemStyleBordered 
																		   target:self 
																		   action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:YES];
	[cancelBarButtonItem release];
}


- (void)loadTextView {
	logTextView = [[UITextView alloc] initWithFrame:kLogTextViewFrame];
	[logTextView setEditable:FALSE];
	[logTextView setBackgroundColor:[UIColor clearColor]];
	[logTextView setTextColor:[UIColor darkTextColor]];
	//[self.view addSubview:logTextView];
}

- (UITextView *)getTextView {
	UITextView *textView = [[[UITextView alloc] initWithFrame:kLogTextViewFrame] autorelease];
	textView.tag = kTextViewTag;
	[textView setEditable:FALSE];
	[textView setBackgroundColor:[UIColor lightTextColor]];
	[textView setTextColor:[UIColor darkTextColor]];
	
	//[textView set
	
	return textView;
}

- (void)loadLabel {
	syncLabel = [[UILabel alloc] initWithFrame:kSyncLabelFrame];
	syncLabel.text = @"Synchronizing...";
	syncLabel.textAlignment = UITextAlignmentCenter;
	syncLabel.backgroundColor = [UIColor clearColor];
	
	[self.view addSubview:syncLabel];
}

- (void)loadActivityView {
	syncActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[syncActivityIndicatorView setFrame:kActivityIndicatorFrame];
	[syncActivityIndicatorView setHidesWhenStopped:YES];
	[self.view addSubview:syncActivityIndicatorView];
	
	[syncActivityIndicatorView startAnimating];
}


#pragma mark -
#pragma mark ToodledoFolder Delegates

- (void)folderActionSuccessful:(id)sender {
	// Get the folder
	ToodledoFolder *folder = (ToodledoFolder *)sender;
	
	if ([folder actionType] == EnumToodledoFolderActionAdd) {
		if ([folder.name isEqualToString:@"Toodledo Tasks"]) {
			// Go to next step
			[self updateToodledoTasksWithNoFolderUsingFolderID:folder.folderID];
		}
	} else if ([folder actionType] == EnumToodledoFolderActionUpdate) {
		
	} else if ([folder actionType] == EnumToodledoFolderActionDelete) {
		
	}
	
	// Release the folder
	[folder release];
}

- (void)folderActionConnectionFailedWithError:(NSError *)error {
	
}

- (void)folderActionFailedWithErrorCode:(NSInteger)errorCode andErrorDescription:(NSString *)errorDesc {
	
}

#pragma mark -
#pragma mark Replace Local Tasks Sync Methods

// We are returning the backup path
- (void)synchronize {
	// Try doing this in seperate thread
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	NSString *key = [ApplicationSetting getSettingDataForName:@"ToodledoKey"];
	self.toodledoKey = key;
	
	[self updateLogTextView:@"GETTING COPY OF TOODLEDO FOLDERS"];
	toodledoFolderCollection = [[ToodledoFolderCollection alloc] initUsingKey:self.toodledoKey];
	NSString *collectFoldersResponse = [toodledoFolderCollection collectFoldersFromServer];
	if ([collectFoldersResponse length] > 0) {
		[self updateLogTextView:collectFoldersResponse];
	}
	
	[self updateLogTextView:@"\nGETING COPY OF TOODLEDO TASKS"];
	NSString *lastSyncDateStamp = @"0"; // We always want all tasks
	toodledoTaskCollection = [[ToodledoTaskCollection alloc] initUsingKey:self.toodledoKey];
	// The extra fields to return
	NSString *fields = @"tag,folder,parent,duedate,repeat,repeatfrom,priority,note,meta";
    
    
    /*
     while ([self loadTasksUsingArray:responseArray] == FALSE) {
     start += 1000;
     responseString = [self getTasksFromServerWithModafter:modAfter andFields:fields andStart:start];
     }
     */
    
    NSInteger start = 0;
	NSString *collectTasksResponse = [toodledoTaskCollection getTasksFromServerWithModafter:lastSyncDateStamp 
																				  andFields:fields
                                                                                   andStart:start];
    
    while ([collectTasksResponse isEqualToString:@"Toodledo Server: Collecting tasks..."]) {
        [self updateLogTextView:collectTasksResponse];
        start += 1000;
        collectTasksResponse = [toodledoTaskCollection getTasksFromServerWithModafter:lastSyncDateStamp 
                                                                            andFields:fields
                                                                             andStart:start];
    }

	if ([collectTasksResponse length] > 0) {
		[self updateLogTextView:collectTasksResponse];
	}
	
	// Do a backup first
	[self updateLogTextView:@"\nBACKING UP LOCAL DATA"];
	
	// Returns the backup path
	backupPath = [MethodHelper createBackup];
	
	// Set data has changed to true
	dataHasChanged = TRUE;
	
	//[MethodHelper createDatabaseBackup];
	
	[self updateLogTextView:@"Local: Database backup successful"];
	
	
	// Delete all our lists (also need to delete all archives, or maybe not)
	[self updateLogTextView:@"\nREMOVING LOCAL DATA"];
	[refMasterTaskListCollection deleteAllLists:YES];
	// TODO: Also need to delete DeletedListItem and DeletedList entries
	
	[self updateLogTextView:@"Local: All lists and tasks removed"];
	
	// Remove all archives
	ArchiveList *archiveList = [[ArchiveList alloc] init];
	[archiveList deleteAllArchives];
	[self updateLogTextView:@"Local: All archived lists and tasks removed"];
	[archiveList release];
	
	// Remove all deleted lists and deleted items
	[DeletedTaskList emptyDeletedListDatabase];
	[DeletedListItem emptyDeletedListItemDatabase];
	
	// But do need to delete all repeating tasks and all tags and listitem tags
	[BLTag deleteAllTags];
	[self updateLogTextView:@"Local: All tags removed"];
	[BLListItemTag deleteAllListItemTags];
	[BLRecurringListItem deleteAllRecurringListItems];
	[self updateLogTextView:@"Local: All repeating tasks removed"];
	
	// Update toodledo tasks with no folder
	//[self updateLogTextView:@"\nUPDATING TOODLEDO TASKS WITH NO FOLDER"];
	//[self createToodledoTasksFolderIfRequired];
	
	// Also need to be passing in reference to task list collection
	// Create new lists using toodledo folders we grabbed
	[self updateLogTextView:@"\nCREATING NEW LOCAL LISTS"];
	[self createNewListsFromToodledoFolders];
	
	[self updateLogTextView:@"\nCREATING NEW LOCAL TAGS"];
	[self createLocalTagsFromToodledoTasks];
	
	// Next create new tasks
	[self updateLogTextView:@"\nCREATING NEW LOCAL TASKS"];
	[self createLocalTasksFromToodledoTasks];
	
	// Next create list item tags
	[self updateLogTextView:@"\nASSIGNING TAGS TO NEW LOCAL TASKS"];
	[self createListItemTagsFromToodledoTaskTags];
	
	// Find and update tasks that need to be converted to subtasks
	[self updateLogTextView:@"\nCONVERTING TASKS TO SUBTASKS"];
	[self updateListItemsThatNeedToBeSubtasks];
	
	// Update toodledo tasks with 'meta' from our local tasks, listItemID, listID and scribble name
	//[self updateLogTextView:@"\nLINKING TOODLEDO TASKS"];
	//[self addLinkingDataToToodledoTasks];
	
	// Also need to save text log file, can use MethodHelper for this,
	// Make sure to get timestamp from database backup, so it can be reused in log file name
	// Also now need to update existing database with last sync dateTime
	// Now need to reload everything in main view, use delegate
	[self updateLogTextView:@"\nREFRESHING LOCAL TASKS AND LISTS"];
	[self.delegate replaceLocalTasksSyncComplete];
	[self updateLogTextView:@"Local: Refresh completed"];
	
	[self updateLogTextView:@"\nSYNCHRONIZATION COMPLETE"];
	
	// Update local database last sync time
	NSString *dateTimeString = [MethodHelper stringFromDate:[NSDate date] 
												usingFormat:K_DATETIME_FORMAT];
	[ApplicationSetting updateSettingName:@"ToodledoLastSyncDateTime" 
								 withData:dateTimeString];
	
	[syncActivityIndicatorView stopAnimating];
	
	[syncLabel setText:@"Synchronization Complete"];
	[cancelBarButtonItem setEnabled:FALSE];
	[doneBarButtonItem setEnabled:TRUE];
	
	[MethodHelper createLogFile:logContent fromBackupPath:backupPath];
	
	// Release the pool
	[pool release];
}

// Creates a Toodledo Tasks folder on toodledo server if required
- (void)createToodledoTasksFolderIfRequired {
	// Check if the folder exists
	BOOL found = FALSE;
	NSInteger folderID = 0;
	for (ToodledoFolder *folder in toodledoFolderCollection.folders) {
		if ([folder.name isEqualToString:@"Toodledo Tasks"]) {
			found = TRUE;
			folderID = folder.folderID;
		}
	}  
	
	// If folder wasn't found then create it
	if (found == FALSE) {
		ToodledoFolder *newFolder = [[ToodledoFolder alloc] init];
		newFolder.delegate = self;
		
		NSError *error = nil;
		NSString *responseString = @"";
		if ([newFolder addFolderWithName:@"Toodledo Tasks" usingKey:self.toodledoKey responseString:&responseString error:&error] == TRUE) {
			
			if ([responseString length] > 0) {
				[self updateLogTextView:responseString];
			}
			
			// Add the folder to toodledofolderCollection
			[toodledoFolderCollection.folders addObject:newFolder];
			
			// Update the task list collection with toodledo folder id (if required)
			[refMasterTaskListCollection updateWithToodledoFolder:newFolder];
			
			[newFolder release];
			
			// Go through and update toodledo tasks that have no folder
			[self updateToodledoTasksWithNoFolderUsingFolderID:newFolder.folderID];
		} else {
			[newFolder release];
			if (error) {
				[self updateLogTextView:[NSString stringWithFormat:@"Connection Error: %@",
										responseString]];
			} else {
				[self updateLogTextView:[NSString stringWithFormat:@"Toodledo Error: %@",
										 responseString]];
			}
		}
	} else {
		[self updateToodledoTasksWithNoFolderUsingFolderID:folderID];
	}
}

// Updates toodledo tasks on the server that don't have a folder
// Called from create toodledo tasks if reuired
- (void)updateToodledoTasksWithNoFolderUsingFolderID:(SInt64)folderID {
	BOOL updateRequired = FALSE;
	
	for (ToodledoTask *task in toodledoTaskCollection.tasks) {
		if ([task folderID] == 0) {
			// Has no folder point it to toodledo folder
			task.folderID = folderID;
			task.needsUpdate = TRUE;
			updateRequired = TRUE;
		}
	}
	
	// Call update on tasks now (if required)
	if (updateRequired == TRUE) {
		// Post updates to folder names for the tasks
		NSError *error = nil;
		NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
		[mutableResponseString setString:@""];
		
        NSInteger section = 0;
		BOOL postUpdatesComplete = FALSE;
		while (postUpdatesComplete == FALSE) {
			postUpdatesComplete = [toodledoTaskCollection postUpdatesToTaskFolderNamesWithResponseString:&mutableResponseString section:section error:&error];
			
			// Output the response
			if ([mutableResponseString length] > 0) {
				[self updateLogTextView:mutableResponseString];
			}
            section++;
		}
	}
	
}

// Create new lists from toodledo folders
- (void)createNewListsFromToodledoFolders {
	// Create new lists based on folders..
	for (ToodledoFolder *folder in toodledoFolderCollection.folders) {
		TaskList *newTaskList = [[TaskList alloc] initNewList];
		newTaskList.toodledoFolderID = folder.folderID;
		newTaskList.title = folder.name;
		
		// Update the database
		[newTaskList updateDatabase];
		
		// Need to check where we add the new list
		if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
			[refMasterTaskListCollection.lists insertObject:newTaskList atIndex:0];
		} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
			[refMasterTaskListCollection.lists addObject:newTaskList];
			
			// Need to update the list order (as we have added to end)
			[refMasterTaskListCollection commitListOrderChanges];
			[refMasterTaskListCollection updateListOrdersInDatabase];
		}
		[self updateLogTextView:[NSString stringWithFormat:@"Local: New task list created (%@)", 
								 newTaskList.title]];
		
		[newTaskList release];
	}
	
	// Also create toodledo tasks list if it doesnt exist
	NSInteger toodledoTasksListIndex = [refMasterTaskListCollection getIndexOfTaskListWithTitle:@"Toodledo Tasks"];
	if (toodledoTasksListIndex == -1) {
		// Create the list
		TaskList *newTaskList = [[TaskList alloc] initNewList];
		// -1 means there is no folder id connection, 0 means its just task with no folder
		newTaskList.toodledoFolderID = 0;
		newTaskList.title = @"Toodledo Tasks";
		
		// Update the database
		[newTaskList updateDatabase];
		
		// Need to check where we add the new list
		if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
			[refMasterTaskListCollection.lists insertObject:newTaskList atIndex:0];
		} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
			[refMasterTaskListCollection.lists addObject:newTaskList];
			
			// Need to update the list order (as we have added to end)
			[refMasterTaskListCollection commitListOrderChanges];
			[refMasterTaskListCollection updateListOrdersInDatabase];
		}
		[self updateLogTextView:[NSString stringWithFormat:@"Local: New task list created (%@)", 
								 newTaskList.title]];
		
		[newTaskList release];
		
	}
}


// Create local tags and list item tags from toodledo tasks
- (void)createLocalTagsFromToodledoTasks {
	// Create new tag collection
	tagCollection = [[TagCollection alloc] initWithAllTags];
	
	// Delete all the tags first (have to go backwards)
	for (int i = [tagCollection.tags count] - 1; i >= 0; i--) {
		[tagCollection deleteTagAtIndex:i];
	}
	
	// Go through all the toodledo tasks fetching all tags
	NSArray *tagsList = [[toodledoTaskCollection getListOfTagsCopy] retain];
	
	// Create all the tags
	// For each tag, link them up and add to list item tags
	// Add any tags that need to be added to tag collection
	// Add these tags to our tag collection
	for (NSString *tagName in tagsList) {
		NSInteger newTagID = [tagCollection getNewTagID];
		NSInteger newTagOrder = [tagCollection getNextTagOrderForParentTagID:-1];
		NSInteger tagDepth = 0;
		NSInteger parentTagID = -1;
		
		Tag *tag = [[Tag alloc] initWithTagID:newTagID 
									  andName:tagName 
							   andParentTagID:parentTagID 
								  andTagOrder:newTagOrder 
								  andTagDepth:tagDepth
                                 andTagColour:@"Brown"];
		
		[tag insertSelfIntoDatabase];
		[tagCollection insertNewTag:tag];
		[self updateLogTextView:[NSString stringWithFormat:@"Local: New tag created (%d)", 
								 tag.tagID]];
		[tag release];
	}
	
	[tagsList release];
	
}

- (void)createLocalTasksFromToodledoTasks {
	
	// Need to get the title (folder) for the task
	for (ToodledoTask *task in toodledoTaskCollection.tasks) {
		// Get the parent title, set it to 'toodledo tasks list' if it is empty
	//	NSString *parentTitle = [toodledoFolderCollection getNameOfFolderWithID:task.folderID];
	//	if ([parentTitle length] == 0) {
	//		parentTitle = @"Toodledo Tasks";
	//	}
		
		// Need to get the task list that matches up to this toodledo task
		NSInteger listIndex = [refMasterTaskListCollection getIndexOfTaskListWithToodledoFolderID:task.folderID];
		
		if (listIndex == -1) {
			[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to find task list with toodledo folder (%qi)", 
									 task.folderID]];
			continue;
		}
		
		// Get the tasklist
		TaskList *refTaskList = [refMasterTaskListCollection.lists objectAtIndex:listIndex];
		
		// Start adding tasks... hmm... empty tag list prevents a db call
		ListItem *newListItem = [[ListItem alloc] initEmptyListItemWithListID:refTaskList.listID 
															   andParentTitle:refTaskList.title
																	 DBInsert:NO
																 emptyTagList:YES];
		
		newListItem.toodledoTaskID = task.serverID;
		newListItem.title = task.title;

		if ([task completed] != 0) {
			newListItem.completed = YES;
            
            // Need to get the completed date
            NSDate *localCompletedDate = [NSDate dateWithTimeIntervalSince1970:[task completed]];
            NSDate *destinationDate = [MethodHelper convertDateToUTCTimezone:localCompletedDate forDestinationZone:@"Local"];
            newListItem.completedDateTime = [MethodHelper stringFromDate:destinationDate usingFormat:K_DATETIME_FORMAT];
        }
		
        
        
		// Set up due date, convert using method helper
		if ([task dueDate] != 0) {
			NSDate *localDueDate = [NSDate dateWithTimeIntervalSince1970:[task dueDate]];
			NSDate *destinationDate = [MethodHelper convertDateToUTCTimezone:localDueDate forDestinationZone:@"Local"];
			newListItem.dueDate = [MethodHelper stringFromDate:destinationDate usingFormat:K_DATEONLY_FORMAT];
		}
		
		// Check whether it repeats, setup as needed
		if ([[task repeat] length] > 0 && [[task repeat] isEqualToString:@"With Parent"] == FALSE) {
			RecurringListItem *recurringListItem = [[RecurringListItem alloc] init];
			[recurringListItem loadUsingToodledoRepeatString:[task repeat] repeatFrom:[task repeatFrom]];
			NSInteger recurringListItemID = [recurringListItem insertSelfIntoDatabase];
			newListItem.recurringListItemID = recurringListItemID;
			[recurringListItem release];
			[self updateLogTextView:[NSString stringWithFormat:@"Local: New repeating action assigned (%d)", 
									 newListItem.recurringListItemID]];
		}
		
		// Setup when to repeat from... it will either be from due date or after completed
		// Again, do after
		
		// Priority
		if ([task priority] == -1 || [task priority] == 0) {
			newListItem.priority = EnumHighlighterColorNone;
		} else if ([task priority] == 1 || [task priority] == 2) {
			newListItem.priority = EnumHighlighterColorYellow;
		} else {
			newListItem.priority = EnumHighlighterColorRed;
		}
		
		// Update notes
		newListItem.notes = task.note;
		
		
		// Add the list item to the task list
		[refTaskList.listItems addObject:newListItem];
		[newListItem insertSelfIntoDatabase];
		[self updateLogTextView:[NSString stringWithFormat:@"Local: New task created (%d)", 
								 newListItem.listItemID]];
		
		// Add recurring list items if needed
		
		
		[newListItem release];
	}
}

// DEPRECATED
// RETURN: Recurring list item ID
- (NSInteger)createRecurringListItemFromRepeatString:(NSString *)repeatString andRepeatFrom:(NSInteger)repeatFrom {
	// Create recurring list item
	RecurringListItem *recurringListItem = [[RecurringListItem alloc] init];
	
	// Need to decipher the string, some possibles are
	// "the last tue of each month", "the 2nd mon of each month"
	// "every sat, sun", "every mon"
	// "every 1 week", "every 1 year"
	NSArray *words = [repeatString componentsSeparatedByString:@" "];
	
	if ([[words objectAtIndex:0] isEqualToString:@"the"]) {
		NSString *ordinal = [words objectAtIndex:1];
		ordinal = [recurringListItem getWeekOrdinalStringFromToodledoString:ordinal];
		recurringListItem.frequencyMeasurement = ordinal;
		
		NSString *weekday = [words objectAtIndex:2];
		weekday = [recurringListItem replaceShortenedToodledoWeekday:weekday];
		recurringListItem.daysOfTheWeek = weekday;
		
	} else if ([[words objectAtIndex:0] isEqualToString:@"Every"]) {
		NSString *word = [words objectAtIndex:1];
		word = [word stringByReplacingOccurrencesOfString:@"," withString:@""];
		if ([word isEqualToString:@"Mon"] || [word isEqualToString:@"Tue"] || [word isEqualToString:@"Wed"]
			|| [word isEqualToString:@"Thu"] || [word isEqualToString:@"Fri"]
			|| [word isEqualToString:@"Sat"] || [word isEqualToString:@"Sun"]) {
			
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Every " withString:@""];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Mon" withString:@"Monday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Tue" withString:@"Tuesday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Wed" withString:@"Wednesday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Thu" withString:@"Thursday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Fri" withString:@"Friday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Sat" withString:@"Saturday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Sun" withString:@"Sunday"];
			
			[recurringListItem setDaysOfTheWeek:repeatString];
			
		} else {
			[recurringListItem setFrequency:[[words objectAtIndex:1] integerValue]];
			NSString *measurement = [words objectAtIndex:2];
			
			measurement = [measurement capitalizedString];
			
			if ([measurement isEqualToString:@"Day"]) {
				measurement = @"Days";
			} else if ([measurement isEqualToString:@"Week"]) {
				measurement = @"Weeks";
			} else if ([measurement isEqualToString:@"Month"]) {
				measurement = @"Months";
			} else if ([measurement isEqualToString:@"Year"]) {
				measurement = @"Years";
			}
			[recurringListItem setFrequencyMeasurement:measurement];
		}
	}
	
	if (repeatFrom == 0) {
		// From due date
		[recurringListItem setUseCompletedDate:FALSE];
	} else {
		// From completed date
		[recurringListItem setUseCompletedDate:TRUE];
	}

	NSInteger recurringListItemID = [recurringListItem insertSelfIntoDatabase];
	[recurringListItem release];
	return recurringListItemID;
}


- (void)createListItemTagsFromToodledoTaskTags {
	// Tag collection already exist, with all tags

	for (ToodledoTask *task in toodledoTaskCollection.tasks) {
		// Need to check that there are tags
		if ([[task tags] length] > 0) {
			// Need to get correct listItemID
			ListItem *listItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:[task serverID]];
			
			if (listItem == nil) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to locate local task with toodledo task ID (%qi)", 
										 [task serverID]]];
				continue;
			}
			
		//	ListItem *listItem = [refMasterTaskListCollection
			
			NSArray *localArray = [task.tags componentsSeparatedByString:@","];
			for (NSString *localTag in localArray) {
				// Get rid of white spaces in local tag
				localTag = [localTag stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
				
				if ([localTag length] == 0) {
					continue;
				}
				
				// Now need to get the tag id
				NSInteger tagID = [tagCollection getTagIDOfTagWithName:localTag];
				
				if (tagID == -1) {
					[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to find tag ID for tag (%@)", 
											 localTag]];
					continue;
				}
				
				// Setting new list item to false will automatically put tag in db too
				[listItem.listItemTagCollection addTagWithID:tagID 
											   andListItemID:listItem.listItemID 
											andIsNewListItem:FALSE];
				
				[self updateLogTextView:[NSString stringWithFormat:@"Local: Tag (%d) assigned to task (%d)", 
										 tagID, listItem.listItemID]];
			}
		}
		
		
	}
	
}


- (void)updateListItemsThatNeedToBeSubtasks {
	SInt64 topLevelParentID = 0;
	for (ToodledoTask *task in toodledoTaskCollection.tasks) {
		if ([task parentID] != 0) {
			// Means it is a subtask
			// Get the top level parent ID, as we do not do subtasks of subtasks
			topLevelParentID = [toodledoTaskCollection getTopLevelLocalTaskIDFromToodledoParentTaskID:[task parentID]];
		
			if (topLevelParentID == 0) {
				[self updateLogTextView:@"Local Error: Top level toodledo task parent ID not found"];
				continue;
			}
			
			// Now point to the correct listItem
			ListItem *listItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:[task serverID]];
			
			if (listItem == nil) {
				[self updateLogTextView:@"Local Error: Unable to find correct local task"];
				continue;
			}
			
			ListItem *parentListItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:topLevelParentID];
			
			if (parentListItem == nil) {
				[self updateLogTextView:@"Local Error: Unable to find correct parent local task"];
				continue;
			}
			
			// Sub task item order has to match parent item order
			listItem.itemOrder = parentListItem.itemOrder;
			
			NSInteger subItemOrder = [parentListItem getNewSubItemOrder];
			
			listItem.parentListItemID = parentListItem.listItemID;
			listItem.subItemOrder = subItemOrder;
			
			// Make sure that our list item has same parent list
			if ([listItem listID] != [parentListItem listID]) {
				// Need to update toodledo tasks too
				
				NSInteger parentIndex = [toodledoTaskCollection getIndexOfTaskWithTaskID:topLevelParentID]; 
				if (parentIndex == -1) {
					[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to find index of local Toodledo parent task (%qi)", 
											 topLevelParentID]];
					continue;
				}
				ToodledoTask *parentTask = [toodledoTaskCollection.tasks objectAtIndex:parentIndex];
				
				// Update folder ID of the task
				task.folderID = parentTask.folderID;
				task.needsUpdate = TRUE; 
				
				// TODO: Need to update the folder of the 'task' not the parent task, but get both, task and parent task
				listItem.listID = parentListItem.listID;
				
			}
			
			[listItem updateDatabase];
			
			[self updateLogTextView:[NSString stringWithFormat:@"Local: Task (%d) converted into subtask", 
									 listItem.listItemID]];
		}
		
	}
	
	// Update toodledo task folders online
	NSError *error = nil;
	NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
	[mutableResponseString setString:@""];
	// Now need to update tasks that need updates
    NSInteger section = 0;
	BOOL postUpdatesComplete = FALSE;
	while (postUpdatesComplete == FALSE) {
		postUpdatesComplete = [toodledoTaskCollection postUpdatesToTaskFolderNamesWithResponseString:&mutableResponseString section:section error:&error];
		
		// Output the response
		if ([mutableResponseString length] > 0) {
			[self updateLogTextView:[NSString stringWithFormat:@"%@", 
									 mutableResponseString]];
		}
		section++;
	}
	
}

// Does a new update where 'meta' is added to all toodledo tasks
- (void)addLinkingDataToToodledoTasks {
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		for (ListItem *listItem in taskList.listItems) {
			// Get correct toodledo task
			
			NSInteger taskIndex = [toodledoTaskCollection getIndexOfTaskWithTaskID:listItem.toodledoTaskID];
			
			ToodledoTask *task = [toodledoTaskCollection.tasks objectAtIndex:taskIndex];
			
			
			// Meta - store scribble, list id and listitem id
			NSString *meta = [NSString stringWithFormat:@"{\"listitemid\":\"%d\",\"listid\":\"%d\"", 
							  listItem.listItemID, listItem.listID];
			if ([listItem.scribble length] > 0) {
				meta = [NSString stringWithFormat:@"%@,\"scribble\":\"%@\"", meta, [listItem scribble]];
			}
			meta = [NSString stringWithFormat:@"%@}", meta];
			task.meta = meta;
			
			// Set needs update to true
			task.needsUpdate = TRUE;
		}
	}
	
	NSError *error = nil;
	NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
	[mutableResponseString setString:@""];
	
    NSInteger section = 0;
	BOOL postUpdatesComplete = FALSE;
	while (postUpdatesComplete == FALSE) {
		postUpdatesComplete = [toodledoTaskCollection postUpdatesToodledoMetaWithResponseString:&mutableResponseString section:section error:&error];
		
		// Output the response
		if ([mutableResponseString length] > 0) {
			[self updateLogTextView:mutableResponseString];
		}
        section++;
	}
}

#pragma mark -
#pragma mark Button Action Methods

- (void)doneBarButtonItemAction {
	// This should really close everything..
	[self.delegate closeSettingsPopover];
	//[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
	// Need to restore saved backup db
	[toodledoTaskCollection cancelConnection];
	[toodledoFolderCollection cancelConnection];
	
	if (dataHasChanged == TRUE) {
		NSString *backupFile = [MethodHelper getBackupFileFromBackupPath:backupPath];
		[MethodHelper restoreBackupData:backupFile backupPath:backupPath];
		
		[MethodHelper showAlertViewWithTitle:@"Sync Cancelled"
								  andMessage:@"Please be patient as data is restored" 
							  andButtonTitle:@"Ok"];
	}
	
	
	
	[self.navigationController popViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark Class Method Helpers

- (void)updateLogTextView:(NSString *)newText {
//[self performSelectorOnMainThread:@selector(sameThreadTextViewUpdate:) withObject:newText waitUntilDone:NO];
	//newText = [NSString stringWithFormat:@"%@%@\n", [logTextView text], newText];
	//[logTextView performSelectorOnMainThread:@selector(setText:) withObject:newText waitUntilDone:YES];
	//[logTextView performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:YES];
	//NSLog(@"%@", newText);
	[logContent appendFormat:@"%@\n", newText];
	
	//[logTextView setNeedsDisplay];
	[self performSelectorOnMainThread:@selector(sameThreadTextViewUpdate:) withObject:newText waitUntilDone:YES];
}

- (void)sameThreadTextViewUpdate:(NSString *)newText {
	//newText = [NSString stringWithFormat:@"%@%@\n", [logTextView text], newText];
	//[logTextView performSelectorOnMainThread:@selector(setText:) withObject:newText waitUntilDone:YES];
	[logTextView setText:[NSString stringWithFormat:@"%@%@\n",
						  [logTextView text],
						  newText]];

	[logTextView scrollRangeToVisible:NSMakeRange([logTextView.text length] - 7, 7)];
	
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewDidAppear:(BOOL)animated {
	[NSThread detachNewThreadSelector:@selector(synchronize) toTarget:self withObject:nil];

	[super viewDidAppear:animated];
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark UITableView Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	
		[cell.contentView addSubview:logTextView];
	}

	// Load tableview cell
	return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 250;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return 1;
}



@end
