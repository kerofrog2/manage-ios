//
//  KeychainWrapper.h
//  Manage
//
//  Created by Cliff Viegas on 9/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface KeychainWrapper : NSObject {

}

- (NSData *)searchKeychainCopyMatching:(NSString *)identifier;
- (BOOL)createKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier;
- (BOOL)updateKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier;
- (void)deleteKeychainValue:(NSString *)identifier;

@end
