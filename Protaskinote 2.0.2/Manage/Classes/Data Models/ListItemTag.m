//
//  ListItemTag.m
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ListItemTag.h"

// Business Layer
#import "BLListItemTag.h"

@implementation ListItemTag

@synthesize listItemID;
@synthesize tagID;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithListItemID:(NSInteger)theListItemID andTagID:(NSInteger)theTagID {
	if (self = [super init]) {
		self.listItemID = theListItemID;
		self.tagID = theTagID;
	}
	return self;
}

#pragma mark -
#pragma mark Database Methods

- (void)insertSelfIntoDatabase {
	[BLListItemTag insertNewListItemTag:self];
}

@end
