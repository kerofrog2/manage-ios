//
//  ListItem.h
//  Manage
//
//  Created by Cliff Viegas on 5/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// CREATE TABLE "ListItem" ("ListItemID" INTEGER PRIMARY KEY  NOT NULL , "ListID" INTEGER NOT NULL , 
// "CategoryID" INTEGER, "PriorityID" INTEGER, "Title" VARCHAR, 
// "CreationDateTime" VARCHAR NOT NULL , "DueDate" VARCHAR, "Notes" TEXT, "Scribble" VARCHAR)

// CREATE TABLE "ListItem" ("ListItemID" INTEGER PRIMARY KEY  NOT NULL ,"ListID" INTEGER NOT NULL ,"CategoryID" INTEGER,
// "Priority" INTEGER,"Title" VARCHAR,"CreationDateTime" VARCHAR NOT NULL ,"DueDate" VARCHAR,"Notes" TEXT,
// "Scribble" VARCHAR,"Completed" INTEGER,"ItemOrder" INTEGER NOT NULL  DEFAULT 0 ,"ParentListItemID" INTEGER,"SubItemOrder" INTEGER NOT NULL  DEFAULT 0 )

// Data Collections
@class ListItemTagCollection;
@class TagCollection;

@interface ListItem : NSObject {
	NSInteger					listItemID;				// The list item id
	NSInteger					listID;
	NSInteger					categoryID;
	NSInteger					priority;
	NSString					*title;
	NSString					*creationDateTime;
	NSString					*dueDate;
	NSString					*notes;
	NSString					*scribble;
	BOOL						completed;
	NSInteger					itemOrder;
	NSInteger					parentListItemID;
	NSInteger					subItemOrder;
	NSString					*parentListTitle;
	
	// Added fields
	NSString					*completedDateTime;
	BOOL						archived;
	NSString					*archivedDateTime;
	NSInteger					recurringListItemID;
	BOOL						hasAlarms;
	NSString					*modifiedDateTime;
	SInt64						toodledoTaskID;
	
	// List item tag collection
	ListItemTagCollection		*listItemTagCollection;
}

@property (nonatomic, readonly)		NSInteger				listItemID;
@property (nonatomic, assign)		NSInteger				listID;
@property (nonatomic, readonly)		NSInteger				categoryID;
@property (nonatomic, assign)		NSInteger				priority;
@property (nonatomic, copy)			NSString				*title;
@property (nonatomic, copy)			NSString				*creationDateTime;
@property (nonatomic, copy)			NSString				*dueDate;
@property (nonatomic, copy)			NSString				*notes;
@property (nonatomic, copy)			NSString				*scribble;
@property (nonatomic, assign)		BOOL					completed;
@property (nonatomic, assign)		NSInteger				itemOrder;
@property (nonatomic, assign)		NSInteger				parentListItemID;
@property (nonatomic, assign)		NSInteger				subItemOrder;
@property (nonatomic, copy)			NSString				*parentListTitle;
@property (nonatomic, copy)			NSString				*completedDateTime;
@property (nonatomic, assign)		BOOL					archived;
@property (nonatomic, copy)			NSString				*archivedDateTime;
@property (nonatomic, assign)		NSInteger				recurringListItemID;
@property (nonatomic, assign)		BOOL					hasAlarms;
@property (nonatomic, copy)			NSString				*modifiedDateTime;
@property (nonatomic, assign)		SInt64					toodledoTaskID;


// Extra properties
@property (nonatomic, retain)		ListItemTagCollection	*listItemTagCollection;

#pragma mark Initialisation
- (id)initNewListItemWithID:(NSInteger)theListID andTitle:(NSString *)theTitle andNotes:(NSString *)theNotes 
			   andCompleted:(BOOL)isCompleted andParentTitle:(NSString *)theParentTitle 
                 andDueDate:(NSString *)theDueDate 
   andListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection 
	   andCompletedDateTime:(NSString *)theCompletedDateTime andPriority:(NSInteger)thePriority
                   addToEnd:(BOOL)addToEnd;
- (id)initEmptyListItemWithListID:(NSInteger)theListID andParentTitle:(NSString *)theParentTitle DBInsert:(BOOL)dbInsert emptyTagList:(BOOL)isEmptyTagList;
- (id)initNewSubListItemWithID:(NSInteger)theListID andTitle:(NSString *)theTitle andNotes:(NSString *)theNotes 
				  andCompleted:(BOOL)isCompleted andParentTitle:(NSString *)theParentTitle 
					andDueDate:(NSString *)theDueDate 
		   andParentListItemID:(NSInteger)theParentListItemID andItemOrder:(NSInteger)theItemOrder 
			   andSubItemOrder:(NSInteger)theSubItemOrder
	  andListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection
		  andCompletedDateTime:(NSString *)theCompletedDateTime;
- (id)initExistingListItemWithListItemID:(NSInteger)theListItemID andListID:(NSInteger)theListID 
							 andPriority:(NSInteger)thePriority andParentTitle:(NSString *)theParentTitle;
- (id)initWithListItemCopy:(ListItem *)listItemToCopy;

#pragma mark Update Methods
- (void)updateSelfWithListItem:(ListItem *)refListItem;

#pragma mark Database Methods
- (NSInteger)insertSelfIntoDatabase;
- (void)updateDatabase;
- (void)updateDatabaseWithoutLastEditTaskDateTime;
- (void)updateDatabaseWithoutModifiedLog;

#pragma mark Sort Methods
- (NSComparisonResult)localizedCompare:(NSString *)otherDueDate;

#pragma mark Class Methods
- (BOOL)isLinkedToRecurringListItem;
- (NSInteger)getDueDateDifferenceFromToday;
- (NSInteger)getCompletedDateDifferenceFromToday;
- (NSInteger)getNewSubItemOrder;
- (void)deleteRepeatingTaskDetails;
- (NSString *)getToodledoStringOfTagsUsingTagCollection:(TagCollection *)tagCollection;

@end
