//
//  Notebook.h
//  Manage
//
//  Created by Cliff Viegas on 17/06/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Model
@class TaskList;

@interface Notebook : NSObject {
    NSInteger           notebookID;         // The notebook ID
    NSString            *title;             // The notebook title
    NSString            *creationDate;      // The creation date of the notebook
    NSInteger           itemOrder;          // The order for this notebook
    NSMutableArray      *lists;             // The array of List ID's for this notebook
    BOOL                isDirty;            // Requires update if true
    NSInteger           selectedListIndex;  // The selected list index
}

@property   (nonatomic, assign)     NSInteger       notebookID;
@property   (nonatomic, copy)       NSString        *title;
@property   (nonatomic, copy)       NSString        *creationDate;
@property   (nonatomic, assign)     NSInteger       itemOrder;
@property   (nonatomic, retain)     NSMutableArray  *lists;
@property   (nonatomic, assign)     BOOL            isDirty;
@property   (nonatomic, assign)     NSInteger       selectedListIndex;

#pragma mark Initialisation
- (id)initWithNotebookID:(NSInteger)theNotebookID title:(NSString *)theTitle itemOrder:(NSInteger)theItemOrder lists:(NSString *)theLists;
- (id)initNewNotebook;

#pragma mark Load Methods
- (void)loadLists:(NSString *)theLists;
 
#pragma mark Get Methods
- (NSInteger)getIndexOfListWithID:(NSInteger)theListID;

#pragma mark Delete Methods
- (BOOL)removeInstancesOfListID:(NSInteger)theListID;
- (BOOL)removeInstancesOfListIDNotFoundInArray:(NSArray *)taskListsArray;

#pragma mark Check Methods
- (BOOL)containsListWithID:(NSInteger)theListID;

#pragma mark Database Methods
- (void)updateDatabase;

@end
