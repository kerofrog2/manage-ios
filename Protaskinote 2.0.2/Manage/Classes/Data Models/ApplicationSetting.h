//
//  ApplicationSetting.h
//  Manage
//
//  Created by Cliff Viegas on 4/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// App Setting Enums
typedef enum {
	EnumSettingVersionNumber = 100,
	EnumSettingBadgeNotification = 200
} EnumSetting;

@interface ApplicationSetting : NSObject {
	NSString		*name;
	NSString		*data;
}

@property (nonatomic, copy)		NSString	*name;
@property (nonatomic, copy)		NSString	*data;

#pragma mark Initialisation
- (id)initWithName:(NSString *)theName andData:(NSString *)theData;

#pragma mark Static Get and Set Methods
+ (NSString *)getSettingDataForName:(NSString *)theName;
+ (void)updateSettingName:(NSString *)theName withData:(NSString *)theData;

#pragma mark Select Methods
- (void)loadApplicationSettingWithName:(NSString *)theName;

#pragma mark Update Methods
- (void)updateDatabase;

@end
