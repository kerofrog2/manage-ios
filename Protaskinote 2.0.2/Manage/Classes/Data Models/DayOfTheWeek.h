//
//  DayOfTheWeek.h
//  Manage
//
//  Created by Cliff Viegas on 10/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//
//  Quick data model for selecting days of the week

#import <Foundation/Foundation.h>


@interface DayOfTheWeek : NSObject {
	NSString		*title;
	BOOL			selected;
	NSInteger		compValue;
}

@property (nonatomic, copy)		NSString	*title;
@property (nonatomic, assign)	BOOL		selected;
@property (nonatomic, assign)	NSInteger	compValue;

#pragma mark Initialisation
- (id)initWithTitle:(NSString *)theTitle andSelected:(BOOL)isSelected andCompValue:(NSInteger)theCompValue;

@end
