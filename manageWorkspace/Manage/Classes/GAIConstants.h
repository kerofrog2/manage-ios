//
//  GAIConstants.h
//  Manage
//
//  Created by Cliff Viegas on 13/06/13.
//  Copyright 2010 kerofrog. All rights reserved.
//

/******** Define screen name **********/

#define kScreen_DateRangeTableViewController                @"Date Range table Screen"
#define kScreen_DateRangeViewController                     @"Date Range Screen"
#define kScreen_MiniCalendarViewController                  @"Mini Calendar Screen"
#define kScreen_LineWidthsViewController                    @"Line Widths Screen"
#define kScreen_NoteViewController                          @"Note Screen"
#define kScreen_EditTagViewController                       @"Edit Tag Screen"
#define kScreen_FilterListItemTagViewController             @"Filter List Item Tag Screen"
#define kScreen_ListItemTagsViewController                  @"List Item Tags Screen"
#define kScreen_ManageTagsViewController                    @"Manage Tags Screen"
#define kScreen_NewTagViewController                        @"New Tag Screen"
#define kScreen_SelectListItemTagViewController             @"Select List Item Tag Screen"
#define kScreen_TagsViewController                          @"Tags Screen"
#define kScreen_PreviewListItemViewController               @"Preview List Item Screen"
#define kScreen_AlertTimeViewController                     @"Alert Time Screen"
#define kScreen_ChooseTimeViewController                    @"Choose Time Screen"
#define kScreen_AlarmsViewController                        @"Alarms Screen"
#define kScreen_DrawPadViewController                       @"Draw Pad Screen"
#define kScreen_EditListItemViewController                  @"Edit List Item Screen"
#define kScreen_HighlightersViewController                  @"High lighters Screen"
#define kScreen_MoveItemsViewController                     @"Move Items Screen"
#define kScreen_NewAlarmViewController                      @"New Alarm Screen"
#define kScreen_NewListItemViewController                   @"New List Item Screen"
#define kScreen_NotesViewController                         @"Notes Screen"
#define kScreen_PngDrawPadViewController                    @"Png Draw Pad Screen"
#define kScreen_PriorityViewController                      @"Priority Screen"
#define kScreen_DaysOfTheWeekViewController                 @"Days Of The Week Screen"
#define kScreen_EveryXTRepeatScheduleViewController         @"Every XT Repeat Schedule Screen"
#define kScreen_XDOfEachMonthRepeatScheduleViewController   @"XD Of Each Month Repeat Schedule Screen"
#define kScreen_RecurringDetailsViewController              @"Recurring Details Screen"
#define kScreen_ListViewController                          @"List Screen"
#define kScreen_MainViewController                          @"Main Screen"
#define kScreen_ControlPanelViewController                  @"Control Panel Screen"
#define kScreen_NotebookListsViewController                 @"Notebook Lists Screen"
#define kScreen_SelectViewTypeViewController                @"Select View Type Screen"
#define kScreen_ArchivesViewController                      @"Archives Screen"
#define kScreen_ArchivesPanelViewController                 @"Archives Panel Screen"
#define kScreen_NameAndPasswordViewController               @"Name And Password Screen"
#define kScreen_ReplaceLocalTasksViewController             @"Replace Local Tasks Screen"
#define kScreen_ReplaceToodledoTasksViewController          @"Replace Toodledo Tasks Screen"
#define kScreen_SynchronizationViewController               @"Synchronization Screen"
#define kScreen_SyncToodledoNowViewController               @"Sync Toodledo Now Screen"
#define kScreen_ToodledoSyncInstructionsViewController      @"Toodledo Sync Instructions Screen"
#define kScreen_ToodledoSyncViewController                  @"Toodledo Sync Screen"
#define kScreen_DataBackupViewController                    @"Data Backup Screen"
#define kScreen_DataImportViewController                    @"Data Import Screen"
#define kScreen_RestoreExportDataViewController             @"Restore Export Data Screen"
#define kScreen_ListSettingViewController                   @"List Setting Screen"
#define kScreen_NewsGiveawaysViewController                 @"News Give aways Screen"
#define kScreen_SettingsViewController                      @"Settings Screen"
#define kScreen_ShopFrontViewController                     @"Shop Front Screen"
#define kScreen_SearchDisplayTableViewController            @"Search Display Table Screen"
#define kScreen_SearchResultsViewController                 @"Search Results Screen"

/******** Define for tracking event **********/

// event category
#define kCategory_event                                     @"event category"

// 1. action handwritting
#define kAction_handwritting                                @"handwritting"

// 2. action about synce file (Toodledo)
#define kAction_sync_file                                   @"sync file"
#define kLabel_login_toodledo                               @"signin Toodledo account"
#define kLabel_sync_create_toodledo                         @"create Toodledo account"
#define kLabel_sync_toodledo                                @"synce Toodledo"

// 3. action about subtask
#define kAction_subtask                                     @"subtask"
#define kLabel_subtask_text                                 @"create subtask by inputting text"
#define kLabel_subtask_handwritting                         @"create subtask by handwritting"
#define kLabel_subtask_change_to_subtask                    @"change this task to subtask"

// 4. action support
#define kAction_support                                     @"support"

// 5. action sharing
#define kAction_sharing                                     @"share notes"
#define kLabel_sharing_text                                 @"sharing text"
#define kLabel_sharing_pdf                                  @"sharing pdf"

// 6. action about editing
#define kAction_edit                                        @"edit task name"
#define kLabel_edit_by_pen                                  @"edit by drawing"
#define kLabel_edit_highlighter                             @"edit highlighter"

// 7. action duplicate list/note
#define kAction_duplicate                                   @"duplicate"

// 8. action about tag
#define kAction_tag                                         @"tag"
#define kLabel_tag_create                                   @"create tag:"
#define kLabel_tag_set                                      @"set tag"

// 9. action about searching
#define kAction_search                                      @"search"
#define kLabel_search_format                                @"search with key: %@, scope: %@, type: %@, dateRange: %@"

// 10. action about backup
#define kAction_backup                                      @"backup"
#define kLabel_create_backup                                @"create backup"
#define kLabel_restore_backup                               @"restore backup"
#define kLabel_export_backup                                @"export backup"
#define kLabel_link_dropbox                                 @"link with Dropbox"
#define kLabel_unlink_dropbox                               @"unlink with Dropbox"

// 11. action about creating new task by input text
#define kAction_new_task                                    @"new task"
#define kLabel_new_task_with_name                           @"new task:"
#define kLabel_new_sub_task_with_name                       @"new subtask:"


// 12. action about theme
#define kAction_theme                                       @"theme"
#define kLabel_selected_theme                               @"Theme Selected:"
#define kLabel_purchased_theme                              @"Theme Purchased:"

// 14. action about adwords
#define kAction_adwords                                     @"Google AdWords"
#define kLabel_more_button_pressed                          @"More button pressed"
#define kLabel_setting_button_pressed                       @"Setting button pressed"


