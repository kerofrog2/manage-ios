//
//  MainConstants.h
//  Manage
//
//  Created by Cliff Viegas on 19/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#define showCompletedTasksSetting               @"ShowCompletedTasksSetting"

#pragma mark -
#pragma mark Landscape & Portrait View Positions
/***********************************************/

//#define kActivityIndicatorViewPortrait			CGRectMake(280, 448, 20, 20)
#define kActivityIndicatorViewPortrait			CGRectMake(364, 448, 20, 20)
//#define kActivityIndicatorViewLandscape			CGRectMake(526, 350, 20, 20)
#define kActivityIndicatorViewLandscape			CGRectMake(630, 350, 20, 20)

//#define kActivityViewBackgroundPortrait			CGRectMake(233, 423, 302, 65)
#define kActivityViewBackgroundPortrait			CGRectMake(317, 423, 302, 65)
//#define kActivityViewBackgroundLandscape		CGRectMake(479, 326, 302, 65)
#define kActivityViewBackgroundLandscape		CGRectMake(583, 326, 302, 65)

/****
Toolbar Objects
****/

#define	kHiddenToolbarPortrait					CGRectMake(305, 845, 158, 44)
#define kHiddenToolbarLandscape					CGRectMake(561, 627, 158, 44)
//#define kHiddenToolbarLandscape					CGRectMake(49, 619, 158, 44)

/****
Items Due TableView Objects
****/
#define kMainItemsDueTableViewPortrait			CGRectMake(66, 806, 209, 121)
#define kMainItemsDueTableViewLandscape			CGRectMake(322, 550, 209, 121)

#define kMainItemsDuePocketPortrait				CGRectMake(50, 792, 241, 149)
#define kMainItemsDuePocketLandscape			CGRectMake(306, 536, 241, 149)

/****
Scroll View Objects
****/

#define kMainScrollViewPortrait					CGRectMake(142, 180, 484, 551)
//#define kMainScrollViewLandscape				CGRectMake(398, 52, 484, 551)
#define kMainScrollViewLandscape				CGRectMake(398, 82, 484, 551)

#define kTileScrollViewPortrait					CGRectMake(17, 11, 734, 921)
#define kTileScrollViewLandscape				CGRectMake(273, 8, 734, 676)

#define kLeftContainerViewPortrait				CGRectMake(0, 200, 184, 480)
#define kLeftContainerViewLandscape				CGRectMake(256, 72, 184, 480)

#define kRightContainerViewPortrait				CGRectMake(584, 200, 184, 480)
#define kRightContainerViewLandscape			CGRectMake(840, 72, 184, 480)

#define kPreviewTableViewPortrait				CGRectMake(15, 10, 380, 530)

#define kNotePreviewHeaderLine2                 CGRectMake(16, 59, 380, 1)
#define kNotePreviewHeaderLine1                 CGRectMake(16, 56, 380, 1)
#define kNotePreviewFooterLine1                 CGRectMake(16, 397, 380, 1)
#define kDrawingLayerPreviewFrame               CGRectMake(16, 60, 380, 423)

#define kDrawingLayerBorderTopFrame             CGRectMake(16, 60, 380, 1)
#define kDrawingLayerBorderLeftFrame            CGRectMake(16, 60, 1, 423)
#define kDrawingLayerBorderBottomFrame          CGRectMake(16, 483, 380, 1)
#define kDrawingLayerBorderRightFrame           CGRectMake(396, 60, 1, 423)

// Notebook image frames
#define kNotebookImageFramePortrait             CGRectMake(142 + 35, 180, 414, 551)
#define kNotebookImageFrameLandscape            CGRectMake(398 + 35, 82, 414, 551)

//#define kPreviewTableViewLandscape				CGRectMake(10, 10, 400, 400)

#define kListShadeViewFrame                     CGRectMake(5, 6, 402, 537)
#define kNotebookShadeViewFrame                 CGRectMake(70, 26, 338, 500)

/****
Text Field Objects
****/
#define kTitleBackgroundToolbarPortrait			CGRectMake(240, 81, 268, 50)
//#define kTitleBackgroundToolbarLandscape		CGRectMake(-15, 41, 212, 50)
#define kTitleBackgroundToolbarLandscape		CGRectMake(496, 16, 268, 50)

#define kMainTitleTextFieldPortrait				CGRectMake(150, 85, 468, 43)
//#define kMainTitleTextFieldLandscape			CGRectMake(20, 45, 212, 43)
#define kMainTitleTextFieldLandscape			CGRectMake(406, 20, 468, 43)

#define kMainDateLabelPortrait					CGRectMake(250, 115, 268, 43)
//#define kMainDateLabelLandscape				CGRectMake(20, 75, 212, 43)
#define kMainDateLabelLandscape					CGRectMake(506, 46, 268, 43)

/****
 Control Panel Objects
 ****/
// 215 * 636
//#define kControlPanelImagePortrait			CGRectMake(-236, 34, 215, 636)
//#define kControlPanelImageLandscape			CGRectMake(20, 34, 215, 636)

#define kMainControlPanelImagePortrait			CGRectMake(-245, 24, 224, 656)
#define kMainControlPanelImageLandscape			CGRectMake(14, 24, 224, 656)

#define kArchivesImagePortrait					CGRectMake(-232, 521, 198, 145)
#define kArchivesImageLandscape					CGRectMake(27, 521, 198, 145)

#define kListPadImagePortrait					CGRectMake(-234, 38, 202, 462)
#define kListPadImageLandscape					CGRectMake(25, 38, 202, 462)

#define kListPadTableViewPortrait				CGRectMake(-229, 25, 192, 655)
#define kListPadTableViewLandscape				CGRectMake(30, 25, 192, 655)

#define kArchiveListPadTableViewPortrait		CGRectMake(-229, 98, 192, 390)
#define kArchiveListPadTableViewLandscape		CGRectMake(30, 98, 192, 390)

#define kListPadHeaderLine1Portrait				CGRectMake(-229, 97, 192, 1)
#define kListPadHeaderLine1Landscape			CGRectMake(30, 97, 192, 1)

#define kListPadHeaderLine2Portrait				CGRectMake(-229, 95, 192, 1)
#define kListPadHeaderLine2Landscape			CGRectMake(30, 95, 192, 1)

#define kListPadTitleLabelPortrait				CGRectMake(-229, 67, 192, 25)
#define kListPadTitleLabelLandscape				CGRectMake(30, 67, 192, 25)

#define kListPadFooterLinePortrait				CGRectMake(-229, 488, 192, 1)
#define kListPadFooterLineLandscape				CGRectMake(30, 488, 192, 1)

//#define kMainControlPanelImagePortrait			CGRectMake(-245, 124, 224, 506)
//#define kMainControlPanelImageLandscape			CGRectMake(14, 124, 224, 506)



/****
 Custom Floating Rect Object
 ****/
#define kAutoSyncRectPortrait                   CGRectMake(384, 450, 1, 1)
#define kAutoSyncRectLandscape                  CGRectMake(512, 320, 1, 1)
