//
//  ArchiveConstants.h
//  Manage
//
//  Created by Cliff Viegas on 10/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Local Object Frame Constants
#define kArchiveCompletedItemsButtonPortrait			CGRectMake(259, 91, 250, 31)
#define kArchiveCompletedItemsButtonLandscape			CGRectMake(515, 91, 250, 31)

#define kTitleLabelPortrait								CGRectMake(383, 801, 300, 43)
#define kTitleLabelLandscape							CGRectMake(639, 545, 300, 43)

#define kScribblesOnOffSwitchPortrait					CGRectMake(586, 851, 97, 27)
#define kScribblesOnOffSwitchLandscape					CGRectMake(842, 595, 97, 27)

#define kScribblesOnOffLabelPortrait					CGRectMake(491, 856, 80, 27)
#define kScribblesOnOffLabelLandscape					CGRectMake(747, 600, 80, 27)

#define kArchiveButtonTableViewPortrait					CGRectMake(68, 129, 634, 661)
#define kArchiveButtonTableViewLandscape				CGRectMake(324, 129, 634, 401)

#define kArchiveCompletedFooterLinePortrait				CGRectMake(68, 128, 634, 1)
#define kArchiveCompletedFooterLineLandscape			CGRectMake(324, 128, 634, 1)

#define kTaskListSegmentedControlPortrait				CGRectMake(300, 47, 168, 29)
#define kTaskListSegmentedControlLandscape				CGRectMake(556, 47, 168, 29)

#define kArchivedListsTableViewPortrait					CGRectMake(-232, 50, 204, 320)
#define kArchivedListsTableViewLandscape				CGRectMake(24, 50, 204, 320)

#define kArchiveCompletedListsButtonPortrait			CGRectMake(-220, 105, 180, 30)
#define kArchiveCompletedListsButtonLandscape			CGRectMake(36, 105, 180, 30)

#define kListPadArchivedFooterLinePortrait				CGRectMake(-229, 141, 192, 1)
#define kListPadArchivedFooterLineLandscape				CGRectMake(30, 141, 192, 1)

#define kListPadShortTableViewPortrait					CGRectMake(-229, 142, 192, 346)
#define kListPadShortTableViewLandscape					CGRectMake(30, 142, 192, 346)