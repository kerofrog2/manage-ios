    //
//  SettingsViewController.m
//  Manage
//
//  Created by Cliff Viegas on 6/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "SettingsViewController.h"

// Data Collections
#import "ApplicationSettingCollection.h"
#import "TagCollection.h"
#import "TaskListCollection.h"

// Method Helper
#import "MethodHelper.h"
#import "Reachability.h"

// Data Models
#import "ApplicationSetting.h"
#import "PropertyDetail.h"
#import "GADBannerView.h"
#import "GADRequest.h"

// View Controllers
#import "ManageTagsViewController.h"
#import "NewsGiveawaysViewController.h"
#import "Appirater.h"

// Test view controller
#import "ReplaceLocalTasksViewController.h"

// Zip File
#import "ZipArchive.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)
#define kSwitchTag			98673

@interface SettingsViewController()
- (void)loadTableView;
- (UISwitch *)getSwitch;
@end


@implementation SettingsViewController {
    int _propertyCollectionType;
}

@synthesize delegate;
@synthesize refPopoverController;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[applicationSettingCollection release];
	[propertyCollection release];
    self.adBanner.delegate = nil;
    [self.adBanner release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<SettingsDelegate>)theDelegate andTagCollection:(TagCollection *)theTagCollection 
andPropertyCollectionType:(NSInteger)propertyCollectionType 
 andTaskListCollection:(TaskListCollection *)theTaskListCollection {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_SettingsViewController;
#endif
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the passed tag collection
		refTagCollection = theTagCollection;
		
		// Assign the passed task list collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Load the app setting collection
		applicationSettingCollection = [[ApplicationSettingCollection alloc] initWithAllUserSettings];
		
		// Load the property collection
		propertyCollection = [[PropertyCollection alloc] initWithCollectionType:propertyCollectionType];
        _propertyCollectionType = propertyCollectionType;
		
		// Load the table view
		[self loadTableView];
		
		// Set the title
		self.title = propertyCollection.title;
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
        
        // Connection work, App show ad
        if ([MethodHelper connectedToInternet]) {
            [self loadAdmob];
        }
        
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
    
    //[myTableView setContentSize:CGSizeMake(320, 400)];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		
		[cell.contentView addSubview:[self getSwitch]];
	}
	
	// Get relevant data
	UISwitch *mySwitch = (UISwitch *)[cell.contentView viewWithTag:kSwitchTag];
    [mySwitch setHidden:YES];
    
	NSArray *propertySection = [propertyCollection.properties objectAtIndex:indexPath.section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:indexPath.row];

	ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:propertyDetail.name];
    // Default values
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    cell.detailTextLabel.text = @"";
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    // Set up the cell...
    cell.textLabel.text = propertyDetail.friendlyName;
    
    if (propertyDetail.propertyType == EnumPropertyTypeList) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.text = appSetting.data;
    } else if (propertyDetail.propertyType == EnumPropertyTypeBoolean) {
        [mySwitch setHidden:NO];
        [mySwitch setOn:[[appSetting data] boolValue]];
    } else if (propertyDetail.propertyType == EnumPropertyTypeCustom) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        cell.detailTextLabel.text = [appSetting data];
    }
    
    // Set status of show completed task
    if ([propertyDetail.friendlyName isEqualToString:NSLocalizedString(@"HIDE_COMPLETED_TASKS", nil)]) {
        
        // Get status show completed tasks in setting
        NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
        
        [mySwitch setHidden:NO];
        if (isShowCompletedTasks == 0) {
            [mySwitch setOn:YES]; //isShowCompletedTasks = 0 ===> Hok show ---> hide task is YES
        }else{
            [mySwitch setOn:NO]; //isShowCompletedTasks = 1 ===> Show ---> hide task is NO
        }
        
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSArray *propertySection = [propertyCollection.properties objectAtIndex:section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:0];
    if (section == [propertyCollection.properties count]-1 && [propertyDetail.name isEqualToString:@"ForAds"]) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0 ,0 , 320, 50)];
        [headerView addSubview:self.adBanner];

        return [headerView autorelease];
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    NSArray *propertySection = [propertyCollection.properties objectAtIndex:section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:0];
    if (section == [propertyCollection.properties count]-1 && [propertyDetail.name isEqualToString:@"ForAds"]) {
        return 19;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    NSArray *propertySection = [propertyCollection.properties objectAtIndex:section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:0];
    if (section == [propertyCollection.properties count]-1 && [propertyDetail.name isEqualToString:@"ForAds"]) {
        return 0;
        
    }
    return [[propertyCollection.properties objectAtIndex:section] count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [propertyCollection.properties count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSArray *propertySection = [propertyCollection.properties objectAtIndex:section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:0];
	return propertyDetail.section;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	NSArray *propertySection = [propertyCollection.properties objectAtIndex:indexPath.section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:indexPath.row];
	ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:propertyDetail.name];
	
	if ([propertyDetail propertyType] == EnumPropertyTypeList) {
		ListSettingViewController *controller = [[[ListSettingViewController alloc] initWithDelegate:self 
																					   andAppSetting:appSetting
																				   andPropertyDetail:propertyDetail] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"ManageTags"]) {
		// MANAGE TAGS
		[delegate settingsPopoverCanClose:NO];
		ManageTagsViewController *controller = [[[ManageTagsViewController alloc]
												 initWithTagCollection:refTagCollection] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"GeneralSettings"]) {
		// GENERAL SETTINGS
		SettingsViewController *controller = [[[SettingsViewController alloc] initWithDelegate:self.delegate 
																			  andTagCollection:refTagCollection 
																	 andPropertyCollectionType:EnumPropertyCollectionGeneral
																		 andTaskListCollection:refMasterTaskListCollection] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"Synchronize"]) {
		// SYNC WITH TOODLEDO
		SynchronizationViewController *controller = [[[SynchronizationViewController alloc] initWithMasterTaskListCollection:refMasterTaskListCollection] autorelease];
		controller.delegate = self;
		controller.refPopoverController = refPopoverController;
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"BackupDB"]) {
		// BACKUP DATABASE
		DataBackupViewController *controller = [[[DataBackupViewController alloc] init] autorelease];
		controller.delegate = self;
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"SyncSettings"]) {
        SettingsViewController *controller = [[[SettingsViewController alloc] initWithDelegate:self.delegate 
                                                                              andTagCollection:refTagCollection 
                                                                     andPropertyCollectionType:EnumPropertyCollectionSync 
                                                                         andTaskListCollection:refMasterTaskListCollection] autorelease];
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[propertyDetail name] isEqualToString:@"PremiumThemes"]) {
        [self.delegate shopfrontSelected];
    } else if ([[propertyDetail name] isEqualToString:@"NewsGiveaways"]) {
        NewsGiveawaysViewController *controller = [[[NewsGiveawaysViewController alloc] init] autorelease];
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[propertyDetail name] isEqualToString:@"RatingReview"]) {
        [Appirater rateApp];
    } else if ([[propertyDetail name] isEqualToString:@"SupportPage"]) {
#if ENABLE_GOOGLE_ANALYTICS
        // track this event to Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker sendEventWithCategory:kCategory_event
                            withAction:kAction_support
                             withLabel:@""
                             withValue:0];
#endif
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://manage.kerofrog.com.au/support/"]];
    } else if ([[propertyDetail name] isEqualToString:@"SendReport"]){
        
        // Send bug report
        NSArray *recipientArray = [[NSArray alloc] initWithObjects:kInfoDefaultEmail, nil];
        
        // Show the composer
        MFMailComposeViewController *controller = [[[MFMailComposeViewController alloc] init] autorelease];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Send bug report"];
        [controller setToRecipients:recipientArray];
        [recipientArray release];
        
        //Get the path to the documents directory and append the databaseName
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *dbPath = [NSString stringWithFormat:@"%@/", documentsDirectory];
        
        // Create our time stamp string
        NSTimeInterval currentTimeStamp = [[NSDate date] timeIntervalSince1970];
        NSString *currentTimeStampString = [NSString stringWithFormat:@"%f", currentTimeStamp];
        NSRange decimalRange = [currentTimeStampString rangeOfString:@"."];
        currentTimeStampString = [currentTimeStampString substringToIndex:decimalRange.location];
        
        // Init the file manager
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Zip file
        // Get the zip file path
        NSString *zipFilePath = [NSString stringWithFormat:@"%@Manage/ManageDB_%@.zip",
                                 dbPath, currentTimeStampString];
        
        // Init the zip archive
        ZipArchive *zipArchive = [[ZipArchive alloc] init];
        [zipArchive CreateZipFile2:zipFilePath Password:@"0ECCB59F-FE90-4B40-B068-24F857BB475B"];
        
        // Get the list of files (scribbles)
        NSArray *scribbles = [fileManager contentsOfDirectoryAtPath:[MethodHelper getScribblePath]
                                                              error:nil];
        
        // First lets get scribbles (and log file)
        for (NSString *scribble in scribbles) {
            // Ignore .pdf file
            NSRange pdfRange = [scribble rangeOfString:@"pdf"];
            NSRange txtRange = [scribble rangeOfString:@"txt"];
            if (pdfRange.length == 0 && txtRange.length == 0) {
                NSString *longPath = [NSString stringWithFormat:@"%@%@",
                                      [MethodHelper getScribblePath], scribble];
                [zipArchive addFileToZip:longPath newname:scribble];
            }
        }

        
        // Add the database
        NSString *dbLongPath = [NSString stringWithFormat:@"%@%@",
                                dbPath, @"Manage/ManageDB.db"];
        
        NSString *newDBName = [NSString stringWithFormat:@"ManageDB_%@.db",
                               currentTimeStampString];
        
        // Attach the databse
        [zipArchive addFileToZip:dbLongPath newname:newDBName];
        
        // Compress
        BOOL successCompressing = [zipArchive CloseZipFile2];
        
        if (successCompressing) {
            
            [controller addAttachmentData:[NSData dataWithContentsOfFile:zipFilePath]  mimeType:@"application/zip" fileName:[NSString stringWithFormat:@"ManageDB_%@.zip",currentTimeStampString]];
            
            // Delete file
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            NSString *backupLogPath = [NSString stringWithFormat:@"%@Manage/ManageDB_%@.zip", dbPath, currentTimeStampString];
            
            if ([fileManager fileExistsAtPath:backupLogPath]) {
                NSError *deleteFileError = nil;
                [fileManager removeItemAtPath:backupLogPath error:&deleteFileError];
                
                if (deleteFileError) {
                    NSLog(@"%@", [deleteFileError localizedDescription]);
                }
            }
            
            if (controller) {
                [self presentModalViewController:controller animated:YES];
            }
            
        } else {
            NSLog(@"Compression failed");
        }
    }
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	// Get switch object
	UISwitch *mySwitch = (UISwitch *)[cell.contentView viewWithTag:kSwitchTag];

	NSArray *propertySection = [propertyCollection.properties objectAtIndex:indexPath.section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:indexPath.row];
	ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:propertyDetail.name];
	
	if ([propertyDetail propertyType] == EnumPropertyTypeBoolean) {
		
		BOOL switchStatus;
		// Change switch value
		if ([mySwitch isOn]) {
			[mySwitch setOn:NO animated:YES];
			switchStatus = NO;
		} else {
			[mySwitch setOn:YES animated:YES];
			switchStatus = YES;
		}
		
		// Update data
		appSetting.data = [NSString stringWithFormat:@"%d", switchStatus];
		[appSetting updateDatabase];
		
		[delegate settingWithName:appSetting.name updatedWithData:appSetting.data];
		
		return nil;
	}
	
	return indexPath;
}


#pragma mark -
#pragma mark String Settings Delegate

- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData {
	ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:settingName];
	
	appSetting.data = theData;
	
	[appSetting updateDatabase];
	
	[delegate settingWithName:settingName updatedWithData:theData];
}

#pragma mark -
#pragma mark Database Backup Delegates

- (void)dataSuccessfullyBackedUp {
	[MethodHelper showAlertViewWithTitle:@"Success" 
							  andMessage:@"Data successfully backed up" 
						  andButtonTitle:@"Ok"];
	[self.delegate closeSettingsPopover];
    // Create file to push to dropbox
}

- (void)restoreDataComplete {
	[self.delegate updatesToDataComplete];
}

#pragma mark -
#pragma mark SynchronizationViewController Delegates

// Chain of delegates running back to main view, used to present a modal view...
- (void)settingsPopoverCanClose:(BOOL)canClose {
	[self.delegate settingsPopoverCanClose:canClose];
}

// Chain of delegates running back to main view, used to refresh everything
- (void)replaceLocalTasksSyncComplete {
	[self.delegate updatesToDataComplete];
}

- (void)replaceToodledoDataComplete {
    [self.delegate replaceToodledoDataComplete];
}

- (void)closeSettingsPopover {
	[self.delegate closeSettingsPopover];
}

#pragma mark -
#pragma mark UIViewController Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void) viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
}
- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewWillAppear:(BOOL)animated {
	[myTableView reloadData];
	
	[delegate settingsPopoverCanClose:YES];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
        // Asking Point tag
        [APManager requestCommandsWithTag:kAskingPointTagSetting];
    }
	
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
//        refPopoverController.popoverContentSize = kTableViewFrame.size;
        self.contentSizeForViewInPopover = kTableViewFrame.size;
        self.preferredContentSize = self.contentSizeForViewInPopover;
    }
    
	[super viewWillAppear:animated];
    
    
}

#pragma mark -
#pragma mark Helper Methods

- (UISwitch *)getSwitch {
	UISwitch *switchView = [[[UISwitch alloc] initWithFrame:CGRectZero] autorelease];
	switchView.tag = kSwitchTag;
	// We dont want user interaction, clicking on table view cell will change this value
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [switchView setFrame:CGRectMake(240, 8, 0, 0)];
    } else {
        [switchView setFrame:CGRectMake(200, 8, 0, 0)];
    }
	switchView.userInteractionEnabled = FALSE;
	[switchView setHidden:YES];
	return switchView;
}


#pragma mark - Adsmod

- (void)loadAdmob{
    
    CGPoint origin = CGPointMake(0.0, 3.0);
    
    // Use predefined GADAdSize constants to define the GADBannerView.
    self.adBanner = [[[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner
                                                    origin:origin]
                     autorelease];
    
    // Note: Edit SampleConstants.h to provide a definition for kSampleAdUnitID
    // before compiling.
    self.adBanner.adUnitID = kManageAdmob2;
    self.adBanner.delegate = self;
    [self.adBanner setRootViewController:self];
    
    [self.adBanner loadRequest:[self createRequest]];
}

#pragma mark GADRequest generation

// Here we're creating a simple GADRequest and whitelisting the application
// for test ads. You should request test ads during development to avoid
// generating invalid impressions and clicks.
- (GADRequest *)createRequest {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as
    // well as any devices you want to receive test ads.
//    request.testDevices =
//    [NSArray arrayWithObjects:
//     // TODO: Add your device/simulator test identifiers here. They are
//     // printed to the console when the app is launched.
//     nil];
    return request;
}


#pragma mark GADBannerViewDelegate impl

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"Received ad successfully");
    
    propertyCollection = [[PropertyCollection alloc] initWithCollectionType:_propertyCollectionType];
    [myTableView reloadData];

}

- (void)adView:(GADBannerView *)view
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
    propertyCollection = [[PropertyCollection alloc] initWithCollectionType:_propertyCollectionType];
    [myTableView reloadData];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    // Notifies users about errors associated with the interface
    switch (result) {
        case MFMailComposeResultCancelled:
            break;
            
        case MFMailComposeResultSaved:
            break;
            
        case MFMailComposeResultSent:
            break;
            
        case MFMailComposeResultFailed:
            break;
            
        default: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-(" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}


@end
