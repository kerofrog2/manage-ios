//
//  Structures.c
//  GLDraw
//
//  Created by Adam Lockhart on 4/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "Structures.h"
#include "ConstantsAndMacros.h"
#include <math.h>



static const GLfloat angle = 0.31415927410125732421875f;//1.0f/20.0f*6.28318531f;

static const GLfloat dotAngle = 1.0f/12.0f*6.28318531f;

static GLfloat sinAngle[20], cosAngle[20];

// curve coefficients
static GLfloat p0,p1,p2,p3,q0,q1,q2,q3,p0c,p1c,p2c,p3c,q0c,q1c,q2c,q3c;

void setUpAngle() {
    for (int i = 0; i < 20; i++) {
        sinAngle[i] = sinf(angle*i);
        cosAngle[i] = cosf(angle*i);
    }
}


rgba rgbaMake(float r,float g,float b,float a) {
    rgba c;
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = a;
    return c;
}

rgba rgbMake(float r,float g,float b) {
    rgba c;
    c.r = r;
    c.g = g;
    c.b = b;
    c.a = 1.f;
    return c;
}

GLfloat angleSin(int i) {
    return sinAngle[i];
}

GLfloat angleCos(int i) {
    return cosAngle[i];
}

vertex vertexMake (GLfloat x, GLfloat y) {
    vertex v;
    v.x = x;
    v.y = y;
    return v;
}

vertex scaled(vertex v, GLfloat s) {
    vertex new;
    new.x = v.x*s;
    new.y = v.y*s;
    return new;
}


GLboolean vertexIsEqual(vertex a, vertex b) {
    return ((a.x == b.x) && (a.y == b.y));
}

vertex vectorDirection (vertex origin, vertex end) {
    vertex diff;
    diff.x = end.x - origin.x;
    diff.y = end.y - origin.y;
    return diff;
}

void vectorMagnitude (vector *v) {
    v->len = sqrtf(v->dir.x * v->dir.x + v->dir.y * v->dir.y);
}

void vectorNormalize (vector *v) {
    v->norm.x = v->dir.x / v->len;
    v->norm.y = v->dir.y / v->len;
}

vertex vectorDirNormalize(vertex v) {
    if (v.x == 0 && v.y == 0) return v;
    float len = sqrtf(v.x*v.x+v.y*v.y);
    return vertexMake(v.x/len, v.y/len);
}

vector vectorMake (vertex origin, vertex end) {
    vector v;
    v.v1 = origin;
    v.v2 = end;
    v.dir = vectorDirection(origin, end);
    vectorMagnitude(&v);
    vectorNormalize(&v);
    v.vectorIsSet = TRUE;
    return v;
}

void vectorSet (vector *v, vertex origin, vertex end) {
    v->v1 = origin;
    v->v2 = end;
    v->dir = vectorDirection(origin, end);
    vectorMagnitude(v);
    vectorNormalize(v);
}
/*
GLfloat crossProduct(vector a, vector b) {
    // this is like cross product 3D
    // but it only returns the z value
    // because the 2D input z values are zero
    // the output x and y values would be zero
    return a.dir.x * b.dir.y - a.dir.y * b.dir.x;
}*/

GLfloat crossProduct(vertex a, vertex b) {
    // this is like cross product 3D
    // but it only returns the z value
    // because the 2D input z values are zero
    // the output x and y values would be zero
    return a.x * b.y - a.y * b.x;
}

vertex tangentProduct(GLfloat z, vertex v) {
    // condensed version of another 3D cross product operation
    return  vertexMake(-(v.y * z), (z * v.x));
}

vertex tangentProductB(GLfloat z, vertex v) {
    // condensed version of another 3D cross product operation
    return  vertexMake((v.y * z), -(z * v.x));
}

GLfloat bernstein(GLint n, GLint j, GLfloat t) {
    assert(n>=0&&j>=0);
    GLfloat f, tInv, expnt;
    f = powf(t, (float)j);
    tInv = 1-t;
    expnt = (float)(n-j);
    f *= powf(tInv, expnt);
    return f;
}

GLfloat rationalBasisFunction(GLint n, GLint j, GLfloat t, GLfloat *h) {
    assert(n>=0&&j>=0);
    GLfloat numer, denom = 0;
    numer = h[j] * bernstein(n, j, t);
    for (int k = 0; k<=n; k++) {
        denom += h[k]*bernstein(n, k, t);
    }
    return (numer/denom);
}

vertex Rj,Tj,Rj1,Tj1;

void setWeightedCurveConstants(tangent v1, tangent v2, GLfloat alphaj, GLfloat betaj1) {
    // Tj = Rj + alphaj*tj;
    // where
    Rj = v1.origin;
    // and
    // tj = v1.direction;
    Tj.x = Rj.x +alphaj*v1.direction.x;
    Tj.y = Rj.y +alphaj*v1.direction.y;
    
    // Tj1 = Rj1 - betaj1*tj1;
    // where
    Rj1 = v2.origin;
    // and
    // tj1 = v2.direction;
    Tj1.x = Rj1.x - betaj1*v2.direction.x;
    Tj1.y = Rj1.y - betaj1*v2.direction.y;
}

void setWeights() {
    
}

vertex weightedCurve(GLfloat t, GLfloat hj, GLfloat hj1) {
    vertex v;
    GLfloat tInv = 1-t;
    GLfloat t2 = SQUARED(t);
    GLfloat t3 = CUBED(t);
    GLfloat tInv2 = SQUARED(tInv);
    GLfloat tInv3 = CUBED(tInv);
    GLfloat numer = tInv3*Rj.x + 3*hj*t*tInv2*Tj.x + 3*hj1*t2*tInv*Tj1.x + t3*Rj1.x;
    GLfloat denom = tInv3 + 3*hj*t*tInv2 + 3*hj1*t2*tInv + t3;
    v.x = numer/denom;
    numer = tInv3*Rj.y + 3*hj*t*tInv2*Tj.y + 3*hj1*t2*tInv*Tj1.y + t3*Rj1.y;
    v.y = numer/denom;
    return v;
}

vertex cp1() {
    return Tj;
}
vertex cp2() {
    return Tj1;
}

void setCurveConstants(tangent v1, tangent v2) {
    GLfloat r;
    r = distanceBetweenPoints(v1.origin, v2.origin);
    
    p0 = v1.origin.x;
    p1 = r * v1.direction.x;
    p2 = 3 * (v2.origin.x - v1.origin.x) - r * (v2.direction.x + 2 * v1.direction.x);
    p3 = -2 * (v2.origin.x - v1.origin.x) + r * (v2.direction.x + v1.direction.x);
    
    q0 = v1.origin.y;
    q1 = r * v1.direction.y;
    q2 = 3 * (v2.origin.y - v1.origin.y) - r * (v2.direction.y + 2 * v1.direction.y);
    q3 = -2 * (v2.origin.y - v1.origin.y) + r * (v2.direction.y + v1.direction.y);
    
}

void setCatmullConstants(vertex v1,vertex v2,vertex v3,vertex v4) {
    GLfloat d = distanceBetweenPoints(v2, v3);
    GLfloat t = d<4?0.1f:((d<15)?0.3f:0.5f);
    
    p0c = v2.x;
    p1c = t*(v3.x - v1.x);
    p2c = 3*(v3.x - v2.x) - t*(v4.x - v2.x) - 2*t*(v3.x - v1.x);
    p3c = -2*(v3.x - v2.x) + t*(v4.x - v2.x) + t*(v3.x - v1.x);
    
    q0c = v2.y;
    q1c = t*(v3.y - v1.y);
    q2c = 3*(v3.y - v2.y) - t*(v4.y - v2.y) - 2*t*(v3.y - v1.y);
    q3c = -2*(v3.y - v2.y) + t*(v4.y - v2.y) + t*(v3.y - v1.y);
}

vertex catmullCurve(GLfloat t) {
    vertex v;
    v.x = p0c + p1c*t + p2c*SQUARED(t) + p3c*CUBED(t);
    v.y = q0c + q1c*t + q2c*SQUARED(t) + q3c*CUBED(t);
    return v;
}

vertex curve(GLfloat t) {
    vertex v;
    v.x = p0 + p1*t + p2*SQUARED(t) + p3*CUBED(t);
    v.y = q0 + q1*t + q2*SQUARED(t) + q3*CUBED(t);
    return v;
}

vertex intersection(tangent t1, tangent t2) {
	GLfloat ratio,dx,dy;

    dx = t2.origin.x - t1.origin.x;
    dy = t2.origin.y - t1.origin.y;
    
	ratio = (dx * t2.direction.y - dy * t2.direction.x) / (t1.direction.x * t2.direction.y - t1.direction.y * t2.direction.x);
	return vertexMake(t1.origin.x + t1.direction.x * ratio, t1.origin.y + t1.direction.y * ratio);
}

vertex rj, tj, tjPlus1, rjPlus1;
GLfloat hj = 1.f, hjPlus1 = 1.f, aj;

void setCubicCurveConstants(tangent t1, tangent t2) {
    rj = t1.origin;
    rjPlus1 = t2.origin;
    vertex intx = intersection(t1, t2);
    GLfloat distance1 = distanceBetweenPoints(t1.origin, intx);
    GLfloat distance2 = distanceBetweenPoints(t1.origin, addVertex(t1.origin, t1.direction));
    GLfloat distance3 = distanceBetweenPoints(t2.origin, intx);
    GLfloat distance4 = distanceBetweenPoints(t2.origin, subtractVertex(t2.origin, t2.direction));
    
    GLfloat ratio1 = distance1/distance2;
    GLfloat ratio2 = distance3/distance4;
    GLfloat m = 1.f;//0.666f
    
    aj = (ratio1<1&&ratio2<1? (ratio1<ratio2?ratio1*m:ratio2*m) :0.5f);
    
    tj = addVertex(t1.origin, scaled(t1.direction, aj));
    tjPlus1 = subtractVertex(t2.origin, scaled(t2.direction, aj));
}


vertex cubicCurve(GLfloat t) {
    vertex v;
    GLfloat tminus = 1-t;
    
    
    v.x = CUBED(tminus)*rj.x + 3.f*hj*t*SQUARED(tminus)*tj.x + 3*hjPlus1*SQUARED(t)*tminus*tjPlus1.x + CUBED(t)*rjPlus1.x;
    v.x /= (CUBED(tminus) + 3*hj*t*SQUARED(tminus) + 3*hjPlus1*SQUARED(t)*tminus + CUBED(t));
    v.y = CUBED(tminus)*rj.y + 3.f*hj*t*SQUARED(tminus)*tj.y + 3*hjPlus1*SQUARED(t)*tminus*tjPlus1.y + CUBED(t)*rjPlus1.y;
    v.y /= (CUBED(tminus) + 3*hj*t*SQUARED(tminus) + 3*hjPlus1*SQUARED(t)*tminus + CUBED(t));
    return v;
}

GLfloat invSqrt (GLfloat x){
    GLfloat xhalf = 0.5f*x;
    int i = *(int*)&x;
    i = 0x5f3759df - (i>>1);
    x = *(GLfloat*)&i;
    x = x*(1.5f - xhalf*x*x);
    return x;
}


tangent tangentj(vertex v1, vertex v2, vertex v3, vertex v4, vertex v5) {
    // if this were normalized it would equal the other tangent function
    tangent t;
    GLfloat w2,w3,mu;
    vertex d1,d2,d3,d4;
    
    // difference vectors
    d1 = subtractVertex(v2, v1);    // Sj-1
    d2 = subtractVertex(v3, v2);    // Sj
    d3 = subtractVertex(v4, v3);    // Sj+1
    d4 = subtractVertex(v5, v4);    // Sj+2
    w2 = _ABS(crossProduct(d3, d4));
    w3 = _ABS(crossProduct(d1, d2));
    mu = w3/(w3+w2);
    // tj = (1-mu)Sj + mu*Sj+1;
    t.direction = addVertex(scaled(d2, (1-mu)), scaled(d3, mu));
    t.origin.x = v3.x;
    t.origin.y = v3.y;
    return t;
}

tangent tangentForPoints(vertex v1, vertex v2, vertex v3, vertex v4, vertex v5) {
    tangent t;
    GLfloat a0,b0;
    GLfloat w2,w3;
    vertex d1,d2,d3,d4;
    
    // difference vectors
    d1 = subtractVertex(v2, v1);
    d2 = subtractVertex(v3, v2);
    d3 = subtractVertex(v4, v3);
    d4 = subtractVertex(v5, v4);
    w2 = _ABS(crossProduct(d3, d4));
    w3 = _ABS(crossProduct(d1, d2));
    
    a0 = w2*d2.x+w3*d3.x;
    b0 = w2*d2.y+w3*d3.y;
    
    t.origin = v3;
    
    GLfloat multiplier = invSqrt(SQUARED(a0)+SQUARED(b0));
    t.direction.x = a0 * multiplier;
    t.direction.y = b0 * multiplier;
    return t;
}


void rotateAbyB(vertex *a, vertex b) {
    vertex old = *a;
    // rotate a by b
    // a.x = x*cos(theta) - y*sin(theta)
    // a.y = x*sin(theta) + y*cos(theta)
    // b.x is sin(theta) and b.y is cos(theta)
    a->x = old.x * b.y - old.y * b.x;
    a->y = old.x * b.x + old.y * b.y;
}

void translateAbyB(vertex *a, vertex b) {
    a->x += b.x;
    a->y += b.y;
}

GLfloat dotProduct(vertex a, vertex b) {
    return a.x * b.x + a.y * b.y;
}

GLfloat angleInRadians(vector a, vector b) {
    return acosf(dotProduct(a.norm, b.norm));
}

vertex negativeVertex(vertex v) {
    vertex new;
    new.x = -v.x;
    new.y = -v.y;
    return new;
}

vertex subtractVertex (vertex a, vertex b) {
    vertex new;
    new.x = a.x-b.x;
    new.y = a.y-b.y;
    return new;
}

GLfloat distanceBetweenPoints(vertex a, vertex b) {
    vertex dir = subtractVertex(b, a);
    return sqrtf(dir.x * dir.x + dir.y * dir.y);
}

vertex addVertex (vertex a, vertex b) {
    vertex new;
    new.x = a.x+b.x;
    new.y = a.y+b.y;
    return new;
}

vertex rotate90CCW(vertex v) {
    vertex new;
    
    // simplified rotation matrix from
    // new.x = x*0 - y*1
    // new.y = x*1 + y*0
    
    new.x = -v.y;
    new.y = v.x;
    
    return new;
}

vertex rotate90CW(vertex v) {
    vertex new;
    
    // simplified rotation matrix from
    // a.x = x*0 + y*1
    // a.y = -x*1 + y*0
    
    new.x = v.y;
    new.y = -v.x;
    
    return new;
}

vertex normalOfVectors(vector a, vector b) {
    //return addVertex(addVertex(a.norm, negativeVertex(b.norm)), a.v2);
    return vectorDirNormalize(addVertex(a.norm, negativeVertex(b.norm)));
}
/*
GLfloat f1 (GLfloat t) { 
    return -4*t*t*t + 8*t*t - 5*t + 1;
}
GLfloat f2 (GLfloat t) { 
    return -4*t*t + 4*t;
}
GLfloat f3 (GLfloat t) { 
    return -4*t*t*t + 6*t*t - 2*t;
}
GLfloat f4 (GLfloat t) { 
    return 4*t*t*t - 4*t*t + 1;
}

vertex controlPointB1(vector a, vector b) {
    vertex tan = vectorDirNormalize(tangentProduct( crossProduct(a, b), normalOfVectors(a, b)));
    return addVertex(((a.len < b.len) ? scaled(tan, a.len/2.f) : scaled(tan, b.len/2.f)), b.v1);
    *//*if (a.len < b.len) {
     tan = scaled(tan, a.len/2.f);
     } else tan = scaled(tan, b.len/2.f);
     return addVertex(tan, a.v2);*//*
}

vertex controlPointB2(vector a, vector b) {
    vertex tan = vectorDirNormalize(tangentProductB( crossProduct(a, b), normalOfVectors(a, b)));
    *//*
     if (a.len < b.len) {
     tan = scaled(tan, a.len/2.f);
     } else tan = scaled(tan, b.len/2.f);
     return addVertex(tan, a.v2);*//*
    return addVertex(((a.len < b.len) ? scaled(tan, a.len/2.f) : scaled(tan, b.len/2.f)), a.v2);
}
*/
float percentDiff(float a, float b) {
    float absoluteDifference = (a-b);
    if (absoluteDifference<0) absoluteDifference = -absoluteDifference;
    absoluteDifference /= AVG(a, b);
    return ((absoluteDifference<0)?(-absoluteDifference*100):(absoluteDifference*100));
}


GLfloat bezier(GLfloat t, GLfloat p1, GLfloat p2, GLfloat p3, GLfloat p4) {
    // interpolate cubic bezier curve by t
    GLfloat tminus = (1-t);// storing (1-t) reduces computations
    return p4*t*t*t + p3*3.f*t*t*tminus + p2*3.f*t*tminus*tminus + p1*tminus*tminus*tminus;
}

GLfloat linearInterp(GLfloat t, GLfloat start, GLfloat end) {
    return start + (end-start)*t; //t * end + (1.0f - t) * start;
}

void setColour(GLubyte *color, int *colorindex) {
    
}

void setColor(GLubyte *color, int *colorindex) {
    if (*colorindex == BLACK) {
        color[0] = 0;
        color[1] = 0;
        color[2] = 0;
    } else if (*colorindex == BLUE) {
        color[0] = 0;
        color[1] = 0;
        color[2] = 255;
    } else if (*colorindex==RED) {
        color[0] = 255;
        color[1] = 0;
        color[2] = 0;
    } else if (*colorindex==GREEN) {
        color[0] = 0;
        color[1] = 255;
        color[2] = 0;
    }
}

void setDotColor(GLubyte *dotColor, rgba aColor) {
    
    GLubyte color[3];
    
    color[0] = (GLubyte) (aColor.r * 255.f);
    color[1] = (GLubyte) (aColor.g * 255.f);
    color[2] = (GLubyte) (aColor.b * 255.f);
    
    dotColor[0] = color[0];
    dotColor[1] = color[1];
    dotColor[2] = color[2];
    
    dotColor[4] = color[0];
    dotColor[5] = color[1];
    dotColor[6] = color[2];
    
    dotColor[8] = color[0];
    dotColor[9] = color[1];
    dotColor[10] = color[2];
    
    dotColor[12] = color[0];
    dotColor[13] = color[1];
    dotColor[14] = color[2];
    
    dotColor[16] = color[0];
    dotColor[17] = color[1];
    dotColor[18] = color[2];
    
    dotColor[20] = color[0];
    dotColor[21] = color[1];
    dotColor[22] = color[2];
    
    dotColor[24] = color[0];
    dotColor[25] = color[1];
    dotColor[26] = color[2];
    
    dotColor[28] = color[0];
    dotColor[29] = color[1];
    dotColor[30] = color[2];
    
    dotColor[32] = color[0];
    dotColor[33] = color[1];
    dotColor[34] = color[2];
    
    dotColor[36] = color[0];
    dotColor[37] = color[1];
    dotColor[38] = color[2];
    
    dotColor[40] = color[0];
    dotColor[41] = color[1];
    dotColor[42] = color[2];
    
    dotColor[44] = color[0];
    dotColor[45] = color[1];
    dotColor[46] = color[2];
    
    dotColor[48] = color[0];
    dotColor[49] = color[1];
    dotColor[50] = color[2];
    
    dotColor[52] = color[0];
    dotColor[53] = color[1];
    dotColor[54] = color[2];
    
    dotColor[56] = color[0];
    dotColor[57] = color[1];
    dotColor[58] = color[2];
    
    dotColor[60] = color[0];
    dotColor[61] = color[1];
    dotColor[62] = color[2];
    
    dotColor[64] = color[0];
    dotColor[65] = color[1];
    dotColor[66] = color[2];
    
    dotColor[68] = color[0];
    dotColor[69] = color[1];
    dotColor[70] = color[2];
    
    dotColor[72] = color[0];
    dotColor[73] = color[1];
    dotColor[74] = color[2];
    
    dotColor[76] = color[0];
    dotColor[77] = color[1];
    dotColor[78] = color[2];
    
    dotColor[80] = color[0];
    dotColor[81] = color[1];
    dotColor[82] = color[2];
    
    dotColor[84] = color[0];
    dotColor[85] = color[1];
    dotColor[86] = color[2];
    
}

GLboolean colorIsEqual (rgba color1, rgba color2) {
    return color1.r == color2.r && color1.g == color2.g && color1.b == color2.b &&color1.a == color2.a;
}

