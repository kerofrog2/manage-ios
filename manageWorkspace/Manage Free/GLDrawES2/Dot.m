//
//  Dot.m
//  GLDrawES2
//
//  Created by Adam Lockhart on 5/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Dot.h"


@implementation Dot
@synthesize v,radius,color;

+(Dot*) dotWithVertex: (vertex) newVertex andColor: (rgba) newColor andRadius: (GLfloat) newRadius {
//+(Dot*) dotWithVertex: (vertex) newVertex andColor: (int) newColor andRadius: (GLfloat) newRadius {
    Dot *newDot = [Dot alloc];
    //[newDot setColor:newColor];
    //[newDot setVertex:newVertex];
    //[newDot setRadius:newRadius];
    newDot.v = newVertex;
    newDot.radius = newRadius;
    newDot.color = newColor;
    return newDot;
}

- (id)copyWithZone:(NSZone *)zone {
    Dot *newCopy = [[[self class] allocWithZone: zone] init];
    newCopy.color = self.color;
    newCopy.v = self.v;
    newCopy.radius = self.radius;
    return newCopy;
}
/*
-(void)setColor: (int) c {
    color = c;
}
-(void) setVertex: (vertex) newVertex {
    v = newVertex;
}

-(void) setRadius: (GLfloat) newRadius {
    radius = newRadius;
}

-(vertex*) dotVertex {
    return &v;
}
-(int*) color {
    return &color;
}

-(GLfloat*) radius {
    return &radius;
}
*/

-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        vertex new;
        new.x = [aDecoder decodeFloatForKey:@"v.x"];
        new.y = [aDecoder decodeFloatForKey:@"v.y"];
        self.v = new;
        self.radius = [aDecoder decodeFloatForKey:@"radius"];
        color.r = [aDecoder decodeFloatForKey: @"r"];
        color.g = [aDecoder decodeFloatForKey:@"g"];
        color.b = [aDecoder decodeFloatForKey:@"b"];
        color.a = [aDecoder decodeFloatForKey:@"a"];
    }
    return self;
}

-(void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeFloat:self.v.x forKey:@"v.x"];
    [aCoder encodeFloat:self.v.y forKey:@"v.y"];
    [aCoder encodeFloat:self.radius forKey:@"radius"];
    [aCoder encodeFloat:self.color.r forKey:@"r"];
    [aCoder encodeFloat:self.color.g forKey:@"g"];
    [aCoder encodeFloat:self.color.b forKey:@"b"];
    [aCoder encodeFloat:self.color.a forKey:@"a"];
}

- (BOOL) isEqual:(id)object{
    if (object == self) return YES;
    if (!object || ![object isKindOfClass:[self class]]) return NO;
    Dot *dotFoo = object;
    return (vertexIsEqual(dotFoo.v, self.v) && colorIsEqual(dotFoo.color, self.color) && dotFoo. radius == self.radius);
}

- (NSUInteger) hash {
    NSUInteger hash = [[NSNumber numberWithFloat:self.v.x] hash];
    hash ^= [[NSNumber numberWithFloat:self.v.y] hash];
    hash ^= [[NSNumber numberWithFloat:self.radius] hash];
    hash ^= [[NSNumber numberWithFloat:self.color.r] hash];
    hash ^= [[NSNumber numberWithFloat:self.color.g] hash];
    hash ^= [[NSNumber numberWithFloat:self.color.b] hash];
    hash ^= [[NSNumber numberWithFloat:self.color.a] hash];
    return hash;
}

@end
