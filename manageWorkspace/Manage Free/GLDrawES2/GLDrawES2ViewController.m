//
//  GLDrawES2ViewController.m
//  GLDrawES2
//
//  Created by Adam Lockhart on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "GLDrawES2ViewController.h"
#import "EAGLView.h"
#import "project.c"
#import "Dot.h"
#import "Knot.h"

#define DOTS 0
//#define DOTS 1

#define VERTS 0
//#define VERTS 1

#define POLY 0
//#define POLY 1

#define NEW_METHOD 0

// Uniform index.
enum {
    UNIFORM_TRANSLATE_AND_SCALE,
    UNIFORM_MVP,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

// Attribute index.
enum {
    ATTRIB_VERTEX,
    ATTRIB_COLOR,
    NUM_ATTRIBUTES
};

enum {
    ERASE_FOUR_REGIONS,
    ERASE_TWO_REGIONS_VERTICLE,
    ERASE_TWO_REGIONS_HORIZONTAL,
    ERASE_ONE_REGION
};

enum {
    ABOVE,
    BELOW,
    LEFT,
    RIGHT
};

// CONSTANTS for - (void)renderVector;
//
// dot overlap
const GLfloat kStrokeStep = 0.65f;//1.95f; //0.65f; //0.45f;  
// steps through t to find next suitable t value.
const GLfloat kIncrement = 0.001f;  

// dotVertices: constant for every point drawn:
// 20 sided polygon draws a dot using these vertices
// with radius of 1 and center of {0,0}
static const GLfloat dotVertices[] = {
    0.f, 0.f,
    0.f, 1.f,
    0.309017002689348f, 0.951056513593641f,
    0.587785257520039f, 0.809016990576898f,
    0.809017009790694f, 0.587785231074518f,
    0.951056518070045f, 0.309016988912394f,
    0.999999999999999f, -0.00000004371139f,
    0.951056518070045f, -0.309016988912394f,
    0.809017009790694f, -0.587785231074518f,
    0.587785257520039f, -0.809016990576898f,
    0.309017002689348f, -0.951056513593641f,
    0.f, -1.f,
    -0.309017002689348f, -0.951056513593641f,
    -0.587785257520039f, -0.809016990576898f,
    -0.809017009790694f, -0.587785231074518f,
    -0.951056518070045f, -0.309016988912394f,
    -0.999999999999999f, 0.00000004371139f,
    -0.951056518070045f, 0.309016988912394f,
    -0.809017009790694f, 0.587785231074518f,
    -0.587785257520039f, 0.809016990576898f,
    -0.309017002689348f, 0.951056513593641f,
    0.f, 1.f,
};

// translateAndScale transformation matrix
// 0,5,10 are x,y,z scalars
// 3,7,11 are x,y,z translate coords
GLfloat translateAndScale[] = {
    1.f, 0.f, 0.f, 0.f,
    0.f, 1.f, 0.f, 0.f,
    0.f, 0.f, 1.f, 0.f,
    0.f, 0.f, 0.f, 1.f,
};

// dotColors[] gets manipulated to change colors
// but the alpha value remain the same.
GLubyte dotColors[] = { 
    0, 0, 0, 255,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
};

@interface GLDrawES2ViewController ()
- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;
- (void)reshape;
- (void)drawFrame;
- (void)renderVector;
- (void)mergeDictionary:(NSMutableDictionary*) newDictionary;
- (void)storeNewDot: (Dot*) newDot;
@end

@implementation GLDrawES2ViewController

@synthesize undoManager, delegate, eraserSelected, selectedColour, passThroughTouches, removal, maxLineWidth, minLineWidth, strokeStep;

- (void)dealloc {
    if (program) {
        glDeleteProgram(program);
        program = 0;
    }
    dispatch_release(eraseQueue);
    [undoManager release];
    [regionMap release];
    [tempDic release];
    [removals release];
    [knotRemovals release];
    [neighbors release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload {
	[super viewDidUnload];
    glDisableVertexAttribArray(ATTRIB_VERTEX);
    glDisableVertexAttribArray(ATTRIB_COLOR);
    glDisable(GL_ALPHA_TEST);
    glDisable (GL_BLEND);
    if (program) {
        glDeleteProgram(program);
        program = 0;
    }
}

-(void) initialFrame: (CGRect) frame {
    rect = frame;
}

-(void) loadView {
    if (rect.size.width==0&&rect.size.height==0) rect = [UIScreen mainScreen].bounds;
    self.view = [[(EAGLView*)[EAGLView alloc] initWithFrame:rect] autorelease];
    self.view.userInteractionEnabled=YES;
    self.view.backgroundColor = [UIColor clearColor];
    
    if ([(EAGLView *)self.view api] == kEAGLRenderingAPIOpenGLES2)
        [self loadShaders];

    // Use shader program.
    glUseProgram(program);
    
    // set up viewport:
    xc = 0;
    yc = 0;
    scale = 1.f;
    [self reshape];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    // Set line widths
    self.maxLineWidth = MAX_LINE_WIDTH;
    self.minLineWidth = MIN_LINE_WIDTH;
    self.strokeStep = kStrokeStep;
    
    passThroughTouches = FALSE;
    self.removal = 10.f;
    glEnable(GL_ALPHA_TEST);
    glEnable (GL_BLEND); 
    // alpha is premultiplied in fragment shader
    // so we use this blending funtion:
    glBlendFunc (GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    
    glClear(GL_COLOR_BUFFER_BIT);
    glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, 0, 0, dotVertices);
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glEnableVertexAttribArray(ATTRIB_COLOR);
    
    strokeCount = 0;
    eraseRadius = 25;
    regionSize = 52;
    
    strokeWidth = self.maxLineWidth;
    // widthFactor: increase this number to make line width more unifrom 
    // by clipping against max width
    widthFactor =  13*strokeWidth;
    
    eraseQueue =  dispatch_queue_create("eraseQueue", NULL);   
    undoManager = [[NSUndoManager alloc] init]; 
    regionMap = [[NSMutableDictionary alloc] init];
    tempDic = [[NSMutableDictionary alloc] init];
    removals = [[NSMutableIndexSet alloc] init];
    neighbors = [[NSMutableArray alloc] init];
    knotRemovals = [[NSMutableArray alloc] init];
    selectedColour = [UIColor blackColor];    
    [self drawFrame];
}

#pragma mark OpenGL

-(void) reshape {
    left = xc-self.view.frame.size.width/2*scale;
    right = xc+self.view.frame.size.width/2*scale;
    bottom = yc-self.view.frame.size.height/2*scale; 
    top = yc+self.view.frame.size.height/2*scale;
    
    mvp[0]  =  2.0/(right-left);
    mvp[1]  =  0.0;
    mvp[2]  =  0.0;
    mvp[3]  = -((right + left)/(right - left));
    mvp[4]  =  0.0;
    mvp[5]  =  2/(top - bottom);
    mvp[6]  =  0.0;
    mvp[7]  = -((top + bottom)/(top - bottom));
    mvp[8]  =  0.0;
    mvp[9]  =  0.0;
    mvp[10] = -1.0;
    mvp[11] =  0.0;
    mvp[12] =  0.0;
    mvp[13] =  0.0;
    mvp[14] =  0.0;
    mvp[15] =  1.0;
    
    // update uinform value
    glUniformMatrix4fv(uniforms[UNIFORM_MVP], 1, GL_FALSE, (GLfloat*)&mvp[0]);
}

-(vertex) getOGLPos:(CGPoint)winPos {
    // Returns the OpenGL model position relative to a window position
    //
    GLint viewport[4];
    
    viewport[0] = 0;
    viewport[1] = 0;
    viewport[2] = self.view.frame.size.width;
    viewport[3] = self.view.frame.size.height;
    
    //opengl 0,0 is at the bottom not at the top
    winPos.y = (float)viewport[3] - winPos.y;
    
    float cX, cY, cZ;
    // NOTE: unProject has been modified to work for this 2D ortho ES 2.x program
    gluUnProject( winPos.x, winPos.y, 0, mvp, viewport, &cX, &cY, &cZ);
    return vertexMake(cX, cY);
}

-(vertex) getWindowPos:(vertex) glPos {
    // Returns the OpenGL window position relative to an OpenGL model position
    //
    GLint viewport[4];
    viewport[0] = 0;
    viewport[1] = 0;
    viewport[2] = self.view.frame.size.width;
    viewport[3] = self.view.frame.size.height;
    float cX, cY, cZ;
    // NOTE: project has been modified to work for this 2D ortho ES 2.x program
    gluProject( glPos.x, glPos.y, 0, mvp, viewport, &cX, &cY, &cZ);
    return vertexMake(cX, cY);
}

-(void) scissorRegion: (NSString*) regionString {
    NSArray *foo = [regionString componentsSeparatedByString:@"_"];
    vertex vert = [self getWindowPos: vertexMake([[foo objectAtIndex:1] floatValue]*regionSize, 
                                                 [[foo objectAtIndex:2] floatValue]*regionSize)];
    glScissor((GLint) roundf(vert.x), (GLint) roundf(vert.y), (GLint) regionSize, (GLint) regionSize);
}

-(void) quadScissorRegion: (NSString*) regionString {
    // given the bottom left region, set up glScissor appropriately.
    NSArray *foo = [regionString componentsSeparatedByString:@"_"];
    vertex vert = [self getWindowPos: vertexMake([[foo objectAtIndex:1] floatValue]*regionSize, 
                                                 [[foo objectAtIndex:2] floatValue]*regionSize)];
    glScissor((GLint) roundf(vert.x), (GLint) roundf(vert.y), (GLint) (regionSize*2), (GLint) (regionSize*2));
}

-(void) dualHorizontalScissorRegion: (NSString*) regionString {
    // given the leftmost region, set up glScissor appropriately.
    NSArray *foo = [regionString componentsSeparatedByString:@"_"];
    vertex vert = [self getWindowPos: vertexMake([[foo objectAtIndex:1] floatValue]*regionSize, 
                                                 [[foo objectAtIndex:2] floatValue]*regionSize)];
    glScissor((GLint) roundf(vert.x), (GLint) roundf(vert.y), (GLint) (regionSize*2), (GLint) regionSize);
}

-(void) dualVerticalScissorRegion: (NSString*) regionString {
    // given the lower region, set up glScissor appropriately.
    NSArray *foo = [regionString componentsSeparatedByString:@"_"];
    vertex vert = [self getWindowPos: vertexMake([[foo objectAtIndex:1] floatValue]*regionSize, 
                                                 [[foo objectAtIndex:2] floatValue]*regionSize)];
    glScissor((GLint) roundf(vert.x), (GLint) roundf(vert.y), (GLint) regionSize, (GLint) (regionSize*2));
}

-(NSString*) regionString: (vertex) v {
    return [NSString stringWithFormat:@"region_%i_%i",(int)floorf(v.x/regionSize),(int)floorf(v.y/regionSize)];
}

-(NSString*) neighborOfRegion: (NSString*) region inDirection: (int) direction {
    int v, h;
    NSArray *foo = [region componentsSeparatedByString:@"_"];
    switch (direction) {
        case ABOVE:
            h = [[foo objectAtIndex:1] intValue];
            v = [[foo objectAtIndex:2] intValue] + 1;
            break;
        case BELOW:
            h = [[foo objectAtIndex:1] intValue];
            v = [[foo objectAtIndex:2] intValue] - 1;
            break;
        case LEFT:
            h = [[foo objectAtIndex:1] intValue] - 1;
            v = [[foo objectAtIndex:2] intValue];
            break;
        case RIGHT:
            h = [[foo objectAtIndex:1] intValue] + 1;
            v = [[foo objectAtIndex:2] intValue];
            break;
            
        default:
            @throw [NSException exceptionWithName:@"neighborOfRegion" reason:@"input out of bounds" userInfo:nil];
            return nil;
            break;
    }
    return [NSString stringWithFormat:@"region_%i_%i",h,v];
}

- (void) drawAndErase: (vertex) eraserXY {
    /*******************************
    // int *thisColor;                  Pointer to stored color used for each dot redrawn.
    // vertex *vert;                    Pointer to cartesian coordinates for each dot redrawn.
    // GLfloat *width;                  Pointer to radius for each dot redrawn.
    // NSArray *keyArray;               Keys for regions being erased.
    // NSString *regionString;          Pointer to keys in keyArray
    // NSMutableArray *currentArray;    Pointer to arrays in regionMap for keys in keyArray.
    *******************************/
    
    dispatch_async(eraseQueue, ^{
        
        __block rgba thisColor;
        __block vertex vert;
        __block GLfloat width;
        NSMutableArray *keyArray;
        __block NSString *regionString;
        __block NSMutableArray *currentArray;
        
        //int rSize = (int)regionSize - MAX_LINE_WIDTH;
        
        keyArray = [[NSMutableArray alloc] init];
        
        dispatch_sync(dispatch_get_main_queue(), ^{ [(EAGLView *)self.view setFramebuffer];});
        
        // test for regions that are within eraser bounds and return them in an array
        eraseRegionMessage = ERASE_FOUR_REGIONS;
        
        // top right corner
        vertex corner = addVertex(eraserXY, vertexMake(eraseRadius, eraseRadius));
        regionString = [NSString stringWithFormat:@"region_%i_%i",(int)floorf(corner.x/regionSize),(int)floorf(corner.y/regionSize)];
        [keyArray addObject:regionString];
        // top left corner
        corner = addVertex(eraserXY, vertexMake(-eraseRadius, eraseRadius));
        regionString = [NSString stringWithFormat:@"region_%i_%i",(int)floorf(corner.x/regionSize),(int)floorf(corner.y/regionSize)];
        // test if it matches previous region
        if ([keyArray indexOfObjectPassingTest:
             ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                 return [obj isEqualToString:regionString]; }] == NSNotFound) 
        {
            [keyArray addObject:regionString];
        } else {
            eraseRegionMessage = ERASE_TWO_REGIONS_VERTICLE;
            // bottom right corner
            corner = addVertex(eraserXY, vertexMake(eraseRadius, -eraseRadius));
            regionString = [NSString stringWithFormat:@"region_%i_%i",(int)floorf(corner.x/regionSize),(int)floorf(corner.y/regionSize)];
            if ([keyArray indexOfObjectPassingTest:
                 ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                     return [obj isEqualToString:regionString]; }] == NSNotFound) 
            {
                [keyArray addObject:regionString];
            } else {
                eraseRegionMessage =  ERASE_ONE_REGION;
            }
        }
        
        if (eraseRegionMessage == ERASE_FOUR_REGIONS) {
            // bottom left corner
            corner = addVertex(eraserXY, vertexMake(-eraseRadius, -eraseRadius));
            regionString = [NSString stringWithFormat:@"region_%i_%i",(int)floorf(corner.x/regionSize),(int)floorf(corner.y/regionSize)];
            if ([keyArray indexOfObjectPassingTest:
                 ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                     return [obj isEqualToString:regionString]; }] == NSNotFound) 
            {
                [keyArray addObject:regionString];
                // if the third test returns a new string, there will be four regions
                // process the last region:
                // bottom right corner
                corner = addVertex(eraserXY, vertexMake(eraseRadius, -eraseRadius));
                regionString = [NSString stringWithFormat:@"region_%i_%i",(int)floorf(corner.x/regionSize),(int)floorf(corner.y/regionSize)];
                [keyArray addObject:regionString];
            } else { 
                eraseRegionMessage = (eraseRegionMessage==ERASE_FOUR_REGIONS ? ERASE_TWO_REGIONS_HORIZONTAL : ERASE_ONE_REGION);
            }
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (eraseRegionMessage == ERASE_FOUR_REGIONS) {
                [self quadScissorRegion:[keyArray objectAtIndex:2]];
            }
            else if (eraseRegionMessage == ERASE_TWO_REGIONS_HORIZONTAL) {
                [self dualHorizontalScissorRegion:[keyArray objectAtIndex:1]];
            }
            else if (eraseRegionMessage == ERASE_TWO_REGIONS_VERTICLE) {
                [self dualVerticalScissorRegion:[keyArray objectAtIndex:1]];
            } else {
                [self scissorRegion:[keyArray objectAtIndex:0]];
            }
            glClear(GL_COLOR_BUFFER_BIT);
            
            for (NSString* regionString in keyArray) {
                currentArray = [regionMap objectForKey: regionString];
                
                for (NSUInteger i = 0; i<[currentArray count]; i++) {
                    thisColor = ((Dot*)[currentArray objectAtIndex:i]).color;
                    vert = ((Dot*)[currentArray objectAtIndex:i]).v;
                    
                    if (distanceBetweenPoints(vert, eraserXY)<eraseRadius) {
                        [removals addIndex:i];
                    }
                    else {
                        width = ((Dot*)[currentArray objectAtIndex:i]).radius;
                        
                        setDotColor(dotColors, thisColor);
                        
                        // update matrix for translate
                        translateAndScale[3] = vert.x;
                        translateAndScale[7] = vert.y;
                        // update martix for scale
                        translateAndScale[0] = width;
                        translateAndScale[5] = width;
                        translateAndScale[10] = width;
                        
                        // Update uniform value
                        glUniformMatrix4fv(uniforms[UNIFORM_TRANSLATE_AND_SCALE], 1, GL_FALSE, (GLfloat*)&translateAndScale[0]);
                        
                        // update attribute value
                        glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, 1, 0, dotColors);
                        
                        glDrawArrays(GL_TRIANGLE_FAN, 0, 22);
                    } 
                }
                [currentArray removeObjectsAtIndexes:removals];
                [removals removeAllIndexes];   
            }
        });
        
        // find neighboring regions of the scissor region
        if (eraseRegionMessage == ERASE_FOUR_REGIONS) {
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:RIGHT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:ABOVE]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:1] inDirection:LEFT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:1] inDirection:ABOVE]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:2] inDirection:LEFT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:2] inDirection:BELOW]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:3] inDirection:RIGHT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:3] inDirection:BELOW]];
        }
        else if (eraseRegionMessage == ERASE_TWO_REGIONS_VERTICLE) {
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:RIGHT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:ABOVE]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:LEFT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:1] inDirection:RIGHT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:1] inDirection:BELOW]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:1] inDirection:LEFT]];
        }
        else if (eraseRegionMessage == ERASE_TWO_REGIONS_HORIZONTAL) {
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:RIGHT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:ABOVE]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:BELOW]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:1] inDirection:LEFT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:1] inDirection:BELOW]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:1] inDirection:ABOVE]];
        }
        else {
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:RIGHT]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:ABOVE]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:BELOW]];
            [neighbors addObject:[self neighborOfRegion:[keyArray objectAtIndex:0] inDirection:LEFT]];
        }
        
        // redraw neighboring regions of the scissor region for clipped edge content
        dispatch_sync(dispatch_get_main_queue(), ^{
            for (NSString* string in neighbors) {
                currentArray = [regionMap objectForKey:string]; 
                
                for (Dot* dot in currentArray) {
                    vert = dot.v;
                    thisColor = [dot color];
                    width = [dot radius];
                    
                    setDotColor(dotColors, thisColor);
                    
                    // update matrix for translate
                    translateAndScale[3] = vert.x;
                    translateAndScale[7] = vert.y;
                    // update martix for scale
                    translateAndScale[0] = width;
                    translateAndScale[5] = width;
                    translateAndScale[10] = width;
                    
                    // Update uniform value
                    glUniformMatrix4fv(uniforms[UNIFORM_TRANSLATE_AND_SCALE], 1, GL_FALSE, (GLfloat*)&translateAndScale[0]);
                    
                    // update attribute value
                    glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, 1, 0, dotColors);
                    
                    glDrawArrays(GL_TRIANGLE_FAN, 0, 22);
                }
            }
            
            [(EAGLView *)self.view presentFramebuffer];
        });
        [neighbors removeAllObjects];
        [removals removeAllIndexes];
        [keyArray release];
    }); // END async
} // END drawAndErase;

- (void)drawFrame {
    [(EAGLView *)self.view setFramebuffer];

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    rgba thisColor;
    vertex vert;
    GLfloat width;
    for (NSArray* array in [regionMap allValues]) {
        for (Dot* dot in array) {
            thisColor = [dot color];
            vert = dot.v;
            width = dot.radius;
            //GLfloat s = (scale>1?scale*0.625f:scale);
            
            setDotColor(dotColors, thisColor);
            
            // update matrix for translate
            translateAndScale[3] = vert.x;
            translateAndScale[7] = vert.y;
            // update martix for scale
            translateAndScale[0] = width;//*s;
            translateAndScale[5] = width;//*s;
            translateAndScale[10] = width;//*s;
            
            // Update uniform value
            glUniformMatrix4fv(uniforms[UNIFORM_TRANSLATE_AND_SCALE], 1, GL_FALSE, (GLfloat*)&translateAndScale[0]);
            
            // update attribute value
            glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, 1, 0, dotColors);
            
            glDrawArrays(GL_TRIANGLE_FAN, 0, 22);
        }
    }
    
    [(EAGLView *)self.view presentFramebuffer];
}


- (void)drawFrameCopyViewWithPosition:(vertex) position {
    [(EAGLView *)self.view setFramebuffer];

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    rgba thisColor;
    vertex vert;
    GLfloat width;
    for (NSArray* array in [regionMap allValues]) {
        for (Dot* dot in array) {
            thisColor = [dot color];
            vert = dot.v;
            width = dot.radius;
            //GLfloat s = (scale>1?scale*0.625f:scale);
            
            setDotColor(dotColors, thisColor);
            
            // update matrix for translate
            translateAndScale[3] = vert.x - position.x;
            
            translateAndScale[7] = vert.y;
            // update martix for scale
            translateAndScale[0] = width;//*s;
            translateAndScale[5] = width;//*s;
            translateAndScale[10] = width;//*s;
            
            // Update uniform value
            glUniformMatrix4fv(uniforms[UNIFORM_TRANSLATE_AND_SCALE], 1, GL_FALSE, (GLfloat*)&translateAndScale[0]);
            
            // update attribute value
            glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, 1, 0, dotColors);
            
            glDrawArrays(GL_TRIANGLE_FAN, 0, 22);
        }
    }
    
    [(EAGLView *)self.view presentFramebuffer];
}

- (void)renderLinearVector {
    [(EAGLView *)self.view setFramebuffer];
    setDotColor(dotColors, myColor);
    GLfloat currentWidth = width2;
    GLfloat tFinder = 0;
    vertex tempVert;
    for (GLfloat t = 0.f; t+tFinder<1.f; t+=tFinder) {
        // Find next t appropriate for current width:
        //
        // set tempVert to the point at the end of this spline segment
        tempVert.x = linearInterp(1.f, p2.x, p3.x);
        tempVert.y = linearInterp(1.f, p2.y, p3.y);
        if (distanceBetweenPoints(drawVertex, tempVert) < (self.strokeStep*currentWidth)) {
            // if distance between last point drawn and spline segment end point
            // is below stroke step threshold,
            // then skip over the segment entirely.
            tFinder=1.f;
        } else {
            tempVert.x = linearInterp(1.f, p2.x, p3.x);
            tempVert.y = linearInterp(1.f, p2.y, p3.y);
            if (distanceBetweenPoints(drawVertex, tempVert) == (self.strokeStep*currentWidth)) {
                // else if distance between last point drawn and spline segment end point
                // is equal to stroke step threshold,
                // then skip the following loop to determine next point
                // and draw spline segment end point.
                tFinder=1.f;
            }
            else {
                tempVert.x = linearInterp(t, p2.x, p3.x);
                tempVert.y = linearInterp(t, p2.y, p3.y);
                if (distanceBetweenPoints(drawVertex, tempVert) < (self.strokeStep*currentWidth)) {
                    // if next spline point < distance threshold
                    // find the next point in range.
                    for (tFinder = kIncrement; distanceBetweenPoints(drawVertex, tempVert) < (self.strokeStep*currentWidth)&&t+tFinder<1.f; tFinder+=kIncrement) {
                        tempVert.x = linearInterp(t+tFinder, p2.x, p3.x);
                        tempVert.y = linearInterp(t+tFinder, p2.y, p3.y);
                    }
                } else {
                    // draw spline segment start point t = 0
                    tFinder = kIncrement;
                }
            }
            
            drawVertex = tempVert;
            currentWidth = linearInterp(t, width2, width3);
            
            // store new Dot
            Dot* newDot = [[Dot alloc] init];
            [newDot setColor:myColor];
            newDot.v = drawVertex;
            [newDot setRadius:currentWidth];
            [self storeNewDot:newDot];
            [newDot release];
            
            // Draw it:
            //
            // update matrix for translate
            translateAndScale[3] = drawVertex.x;
            translateAndScale[7] = drawVertex.y;
            // update martix for scale
            translateAndScale[0] = currentWidth;
            translateAndScale[5] = currentWidth;
            translateAndScale[10] = currentWidth;
            //
            // Update uniform value.
            glUniformMatrix4fv(uniforms[UNIFORM_TRANSLATE_AND_SCALE], 1, GL_FALSE, (GLfloat*)&translateAndScale[0]);
            //
            // Update attribute values.
            glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, 1, 0, dotColors);
            //
            // Draw dot
            glDrawArrays(GL_TRIANGLE_FAN, 0, 22);
        }
    }
    
    [(EAGLView *)self.view presentFramebuffer];
}

#if NEW_METHOD
static GLfloat w = 0.5f;
static GLfloat h = 0.5f;
#endif

- (void)renderVector {
    [(EAGLView *)self.view setFramebuffer];
    setDotColor(dotColors, myColor);
    GLfloat currentWidth = width2;
    GLfloat tFinder = 0;
    vertex tempVert;
    for (GLfloat t = 0.f; t+tFinder<1.f; t+=tFinder) {
        // Find next t appropriate for current width:
        //
        // set tempVert to the point at the end of this spline segment
#if NEW_METHOD
        tempVert = weightedCurve(1.f,h,h);
#else
        tempVert = curve(1.f);
#endif  
        
        if (distanceBetweenPoints(drawVertex, tempVert) < (self.strokeStep*currentWidth)) {
            // if distance between last point drawn and spline segment end point
            // is below stroke step threshold,
            // then skip over the segment entirely.
            tFinder=1.f;
        } else {
#if NEW_METHOD
            tempVert = weightedCurve(1.f,h,h);
#else
            tempVert = curve(1.f);
#endif  
            if (distanceBetweenPoints(drawVertex, tempVert) == (self.strokeStep*currentWidth)) {
                // else if distance between last point drawn and spline segment end point
                // is equal to stroke step threshold,
                // then skip the following loop to determine next point
                // and draw spline segment end point.
                tFinder=1.f;
            }
            else {
#if NEW_METHOD
                tempVert = weightedCurve(t,h,h);
#else
                tempVert = curve(t);
#endif  
                if (distanceBetweenPoints(drawVertex, tempVert) < (self.strokeStep*currentWidth)) {
                    // if next spline point < distance threshold
                    // find the next point in range.
                    for (tFinder = kIncrement; distanceBetweenPoints(drawVertex, tempVert) < (self.strokeStep*currentWidth)&&t+tFinder<1.f; tFinder+=kIncrement) {
#if NEW_METHOD
                        tempVert = weightedCurve(t+tFinder,h,h);
#else
                        tempVert = curve(t+tFinder);
#endif
                    }
                } else {
                    // draw spline segment start point t = 0
                    tFinder = kIncrement;
                }
            }
            
            drawVertex = tempVert;
            currentWidth = linearInterp(t, width2, width3);
            
            // store new Dot
            Dot* newDot = [[Dot alloc] init];
            [newDot setColor:myColor];
            newDot.v = drawVertex;
            [newDot setRadius:currentWidth];
            [self storeNewDot:newDot];
            [newDot release];
            
            // Draw it:
            //
            // update matrix for translate
            translateAndScale[3] = drawVertex.x;
            translateAndScale[7] = drawVertex.y;
            // update martix for scale
            translateAndScale[0] = currentWidth;
            translateAndScale[5] = currentWidth;
            translateAndScale[10] = currentWidth;
            //
            // Update uniform value.
            glUniformMatrix4fv(uniforms[UNIFORM_TRANSLATE_AND_SCALE], 1, GL_FALSE, (GLfloat*)&translateAndScale[0]);
            //
            // Update attribute values.
            glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, 1, 0, dotColors);
            //
            // Draw dot
            glDrawArrays(GL_TRIANGLE_FAN, 0, 22);
        }
    }
    
    [(EAGLView *)self.view presentFramebuffer];
}

- (void)drawDot {
    
    [(EAGLView *)self.view setFramebuffer];
    
    setDotColor(dotColors, myColor);
    
    // store new Dot
    [self storeNewDot:[Dot dotWithVertex:p2 andColor:myColor andRadius:strokeWidth]];
    drawVertex = p2;
    // Draw new Dot:
    //
    // update matrix for translate
    translateAndScale[3] = p2.x;
    translateAndScale[7] = p2.y;
    // update martix for scale
    translateAndScale[0] = strokeWidth;//*4;
    translateAndScale[5] = strokeWidth;//*4;
    translateAndScale[10] = strokeWidth;//*4;
    //
    // Update uniform value.
    glUniformMatrix4fv(uniforms[UNIFORM_TRANSLATE_AND_SCALE], 1, GL_FALSE, (GLfloat*)&translateAndScale[0]);
    //
    // Update attribute values.
    glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, 1, 0, dotColors);
    //
    // Draw dot
    glDrawArrays(GL_TRIANGLE_FAN, 0, 22);

    [(EAGLView *)self.view presentFramebuffer];
}

- (void)drawDot : (vertex) drawVert withColor: (rgba) c {
    
    [(EAGLView *)self.view setFramebuffer];
    
    setDotColor(dotColors, c);
    
    // Draw new Dot:
    //
    // update matrix for translate
    translateAndScale[3] = drawVert.x;
    translateAndScale[7] = drawVert.y;
    // update martix for scale
    translateAndScale[0] = 6;
    translateAndScale[5] = 6;
    translateAndScale[10] = 6;
    //
    // Update uniform value.
    glUniformMatrix4fv(uniforms[UNIFORM_TRANSLATE_AND_SCALE], 1, GL_FALSE, (GLfloat*)&translateAndScale[0]);
    //
    // Update attribute values.
    glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, 1, 0, dotColors);
    //
    // Draw dot
    glDrawArrays(GL_TRIANGLE_FAN, 0, 22);
    
    [(EAGLView *)self.view presentFramebuffer];
}

-(void) updateDrawingWithData: (NSMutableDictionary*) data {
    [self mergeDictionary:data];
    
    [(EAGLView *)self.view setFramebuffer];
    rgba thisColor;
    vertex vert;
    GLfloat width;
    for (NSArray* currentArray in [data allValues]) {
        for (Dot* dot in currentArray) {
            thisColor = [dot color];
            vert = dot.v;
            width = dot.radius;
            
            setDotColor(dotColors, thisColor);
            
            // update matrix for translate
            translateAndScale[3] = vert.x;
            
            translateAndScale[7] = vert.y;
            // update martix for scale
            translateAndScale[0] = width;
            translateAndScale[5] = width;
            translateAndScale[10] = width;
            
            // Update uniform value
            glUniformMatrix4fv(uniforms[UNIFORM_TRANSLATE_AND_SCALE], 1, GL_FALSE, (GLfloat*)&translateAndScale[0]);
            
            // update attribute value
            glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, 1, 0, dotColors);
            
            glDrawArrays(GL_TRIANGLE_FAN, 0, 22);
        }
    }
    [(EAGLView *)self.view presentFramebuffer];
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file {
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source)
    {
        NSLog(@"Failed to load vertex shader");
        return FALSE;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0)
    {
        glDeleteShader(*shader);
        return FALSE;
    }
    
    return TRUE;
}

- (BOOL)linkProgram:(GLuint)prog {
    GLint status;
    
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0)
        return FALSE;
    
    return TRUE;
}

- (BOOL)validateProgram:(GLuint)prog {
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0)
        return FALSE;
    
    return TRUE;
}

- (BOOL)loadShaders {
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    program = glCreateProgram();
    
    // Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname])
    {
        NSLog(@"Failed to compile vertex shader");
        return FALSE;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname])
    {
        NSLog(@"Failed to compile fragment shader");
        return FALSE;
    }
    
    // Attach vertex shader to program.
    glAttachShader(program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(program, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(program, ATTRIB_VERTEX, "position");
    glBindAttribLocation(program, ATTRIB_COLOR, "color");
    
    // Link program.
    if (![self linkProgram:program])
    {
        NSLog(@"Failed to link program: %d", program);
        
        if (vertShader)
        {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader)
        {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (program)
        {
            glDeleteProgram(program);
            program = 0;
        }
        
        return FALSE;
    }
    
    // Get uniform locations.
    uniforms[UNIFORM_TRANSLATE_AND_SCALE] = glGetUniformLocation(program, "tsMatrix");
    uniforms[UNIFORM_MVP] = glGetUniformLocation(program, "u_mvpMatrix");
    
    // Release vertex and fragment shaders.
    if (vertShader)
        glDeleteShader(vertShader);
    if (fragShader)
        glDeleteShader(fragShader);
    
    return TRUE;
}

-(void)mergeDictionary:(NSMutableDictionary *)newDictionary {
    for (id key in [newDictionary allKeys]) {
        if ([regionMap objectForKey: key]!=nil) {
            NSMutableArray* arrayFoo = [regionMap objectForKey: key];
            [arrayFoo addObjectsFromArray:[newDictionary objectForKey:key]];
        }
        else [regionMap setObject:[newDictionary objectForKey:key] forKey:key];
    }
}

-(void) storeNewDot: (Dot*) newDot {
    NSString *regionKey = [self regionString: newDot.v];
    
    if ([regionMap objectForKey:regionKey] != nil) {
        [[regionMap objectForKey:regionKey] addObject:newDot];
    } else {
        [regionMap setObject:[NSMutableArray arrayWithObject:newDot] forKey:regionKey];
    }
    if ([tempDic objectForKey:regionKey] != nil) {
        [(NSMutableArray*) [tempDic objectForKey:regionKey] addObject:newDot];
    } else {
        [tempDic setObject:[NSMutableArray arrayWithObject:newDot] forKey:regionKey];
    }
}

-(NSDictionary*) drawingData {
    NSMutableDictionary *dictionaryFoo = [NSMutableDictionary dictionary];
    for (id key in [regionMap allKeys]) {
        [dictionaryFoo setObject:[NSMutableArray arrayWithArray:[regionMap objectForKey:key]] forKey:key];
    }
    return dictionaryFoo;
}

-(void) setDrawingData: (NSDictionary*) data {
    [regionMap removeAllObjects];
    for (id key in data) {
        [regionMap setObject:[NSMutableArray arrayWithArray:[data objectForKey:key]] forKey:key];
    }
}

-(void) restoreState: (NSMutableDictionary*) state{
    NSMutableDictionary *dictionaryFoo = [NSMutableDictionary dictionary];
    for (id key in [regionMap allKeys]) {
        [dictionaryFoo setObject:[NSMutableArray arrayWithArray:[regionMap objectForKey:key]] forKey:key];
    }
    [undoManager registerUndoWithTarget:self selector:@selector(restoreState:) object:dictionaryFoo];
    
    [regionMap removeAllObjects];
    for (id key in state) {
        [regionMap setObject:[[(NSArray*)[state objectForKey:key] mutableCopy] autorelease] forKey:key];
    }
    if (eraserSelected)  {
        glDisable(GL_SCISSOR_TEST);
        [self drawFrame];
        glEnable(GL_SCISSOR_TEST);
    }
    else [self drawFrame];
    [delegate infoUpdated];
}

-(UIImage *) UIImage {
    return [(EAGLView*)self.view snapshot];
}

- (UIImage *)getUIImage {
    
   /* NSInteger myDataLength = 320 * 480 * 4;
    
    // allocate array and read pixels into it.
    GLubyte *buffer = (GLubyte *) malloc(myDataLength);
    glReadPixels(0, 0, 320, 480, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    
    // gl renders "upside down" so swap top to bottom into new array.
    // there's gotta be a better way, but this works.
    GLubyte *buffer2 = (GLubyte *) malloc(myDataLength);
    for(int y = 0; y < 480; y++)
    {
        for(int x = 0; x < 320 * 4; x++)
        {
            buffer2[(479 - y) * 320 * 4 + x] = buffer[y * 4 * 320 + x];
        }
    }
    
    // make data provider with data.
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer2, myDataLength, NULL);
    
    // prep the ingredients
    int bitsPerComponent = 8;
    int bitsPerPixel = 32;
    int bytesPerRow = 4 * 320;
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    
    // make the cgimage
    CGImageRef imageRef = CGImageCreate(320, 480, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
    
    // then make the uiimage from that
    UIImage *myImage = [UIImage imageWithCGImage:imageRef];
    return myImage;*/

    CGRect frame = self.view.frame;
   // frame = CGRectMake(0, 0, 500, 80);
    NSInteger width = frame.size.width, height = frame.size.height;
    NSInteger dataLength = width * height * 4;
    
    // allocate array and read pixels into it.
    GLubyte *buffer = (GLubyte*)malloc(dataLength * sizeof(GLubyte));
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    
    // make data provider with data.
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer, dataLength, NULL);
    
    // prep the ingredients
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    
    // make the cgimage
    CGImageRef imageRef = CGImageCreate(width, height, 8, 32, width*4, colorSpaceRef, kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedLast, provider, NULL, YES, kCGRenderingIntentDefault);
    
    // OpenGL ES measures data in PIXELS
    // Create a graphics context with the target size measured in POINTS
    NSInteger widthInPoints, heightInPoints;
    if (NULL != UIGraphicsBeginImageContextWithOptions) {
        // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
        // Set the scale parameter to your OpenGL ES view's contentScaleFactor
        // so that you get a high-resolution snapshot when its value is greater than 1.0
        widthInPoints = width / scale;
        heightInPoints = height / scale;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(widthInPoints, heightInPoints), NO, scale);
    }
    else {
        // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
        widthInPoints = width;
        heightInPoints = height;
        UIGraphicsBeginImageContext(CGSizeMake(widthInPoints, heightInPoints));
    }
    
    CGContextRef cgcontext = UIGraphicsGetCurrentContext();
    
    // UIKit coordinate system is upside down to GL/Quartz coordinate system
    // Flip the CGImage by rendering it to the flipped bitmap context
    // The size of the destination area is measured in POINTS
    CGContextSetBlendMode(cgcontext, kCGBlendModeCopy);
    CGContextDrawImage(cgcontext, CGRectMake(0.0, 0.0, widthInPoints, heightInPoints), imageRef);
    
    // Retrieve the UIImage from the current context
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    // Clean up
    free(buffer);
    CFRelease(provider);
    CFRelease(colorSpaceRef);
    CGImageRelease(imageRef);
    
    return image;
}

-(void) clearView {
    // set undo manager
    NSMutableDictionary *dictionaryFoo = [NSMutableDictionary dictionary];
    for (id key in [regionMap allKeys]) {
        [dictionaryFoo setObject:[NSMutableArray arrayWithArray:[regionMap objectForKey:key]] forKey:key];
    }
    [undoManager registerUndoWithTarget:self selector:@selector(restoreState:) object:dictionaryFoo];
    
    // reset data
    [regionMap removeAllObjects];
    //[previousDataAdded removeAllObjects];
    
    // clear view
    if (eraserSelected)  {
        glDisable(GL_SCISSOR_TEST);
        [(EAGLView *)self.view erase];
        glEnable(GL_SCISSOR_TEST);
    }
    else [(EAGLView *)self.view erase];
    
    [delegate viewCleared];
}

-(void) undo {
    [undoManager undo];
}
-(void) redo {
    [undoManager redo];
}

// Erase toggle - toggles erase and returns a value for the erase button
-(BOOL)eraseToggle {
    eraserSelected = !eraserSelected;
    [(EAGLView *)self.view setFramebuffer];
    if (eraserSelected)  glEnable(GL_SCISSOR_TEST);
    else  glDisable(GL_SCISSOR_TEST);
    return eraserSelected;
}


-(void) setFrame: (CGRect) newRect {
    @synchronized (self) {
        if (!CGRectEqualToRect(self.view.frame, newRect)) {
            self.view.frame = newRect;
            [self reshape];
            [self drawFrame]; 
        }
    }
}

-(CGRect) frame {
    // this is atomic but our setter is not
    return self.view.frame;
}

-(void)setScale: (GLfloat) s {
    @synchronized (self) {
        if (scale != s) {
            scale = s;
            [self reshape];
            [self drawFrame];
        }
    }
}

-(GLfloat) scale {
    // this is atomic but our setter is not
    return scale;
}

-(void) setCenter: (CGPoint) center {
    xc = center.x;
    yc = center.y;
    [self reshape];
    [self drawFrame];
}

-(void) setColour: (UIColor*) newColor {
    CGColorRef color = [newColor CGColor];
    
    int numComponents = CGColorGetNumberOfComponents(color);
    
    if (numComponents == 4)
    {
        const CGFloat *components = CGColorGetComponents(color);
        myColor.r = components[0];
        myColor.g = components[1];
        myColor.b = components[2];
        myColor.a = components[3];
    }
    //NSLog(@"R: %f G: %f B: %F",myColor.r, myColor.g, myColor.b);
    self.selectedColour = newColor;
}

-(void) changeWidth {
    if (strokeWidth == self.maxLineWidth) {
        strokeWidth = 0.625f*self.maxLineWidth;
        widthFactor = 12.5*strokeWidth;
    } else {
        strokeWidth = self.maxLineWidth;
        widthFactor = 20*strokeWidth;
    }
}

-(GLfloat) computeWidth: (GLfloat) mag {
    // compute value
    GLfloat width = (1.5/mag*widthFactor)+0.5f;
    // return constrained value
    return (width > strokeWidth) ? strokeWidth : ((width < self.minLineWidth) ? self.minLineWidth : width);
}

#pragma mark touch handling

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    vertex newPoint = [self getOGLPos:[[touches anyObject] locationInView:self.view]];
    NSLog(@"pos began %@ x=%f y=%f",self,newPoint.x, newPoint.y);
    
    if (passThroughTouches) {
        [self.nextResponder touchesBegan:touches withEvent:event];
        return;
    }
    // UNDO Manager:
    NSMutableDictionary *dictionaryFoo = [NSMutableDictionary dictionary];
    for (id key in [regionMap allKeys]) {
        [dictionaryFoo setObject:[NSMutableArray arrayWithArray:[regionMap objectForKey:key]] forKey:key];
    }
    [undoManager registerUndoWithTarget:self selector:@selector(restoreState:) object:dictionaryFoo];
    
    if (eraserSelected) {
        [self drawAndErase: [self getOGLPos:[[touches anyObject] locationInView:self.view]]];
        //[delegate infoUpdated];
    }
    else {
        p2 = [self getOGLPos:[[touches anyObject] locationInView:self.view]];
        lastGoodPoint = p2;
        width2 = strokeWidth;
        width3 = strokeWidth;
        width4 = strokeWidth;
        [self drawDot];
        strokeCount = 1;
    }
    
    if (self.isDrawLayer2 == YES) {
        //move view
        [delegate moveView];
    }
}

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    vertex newPoint = [self getOGLPos:[[touches anyObject] locationInView:self.view]];
    NSLog(@"pos move %@ x=%f y=%f",self,newPoint.x, newPoint.y);
    

    
    if (passThroughTouches) {
        [self.nextResponder touchesBegan:touches withEvent:event];
        return;
    }
    if (eraserSelected) {
        [self drawAndErase: [self getOGLPos:[[touches anyObject] locationInView:self.view]]];
        //[delegate infoUpdated];
    }
    else {
        vertex newPoint = [self getOGLPos:[[touches anyObject] locationInView:self.view]];
        
        if (SQUARED(distanceBetweenPoints(newPoint, lastGoodPoint)) > 10.f)//0.1f) 
        {
            if (strokeCount == 1) {
                strokeCount++;
                p3 = newPoint;
                lastGoodPoint = newPoint;
                width3 = [self computeWidth: distanceBetweenPoints(p2, p3)];
            }
            else if (strokeCount == 2) { 
                vertex s3,s2,s1,s0;
                
                p4 = newPoint;
                lastGoodPoint = newPoint;
                s3 = subtractVertex(p4, p3);
                s2 = subtractVertex(p3, p2);
                s1 = subtractVertex(scaled(s2, 2), s3);
                s0 = subtractVertex(scaled(s1, 2), s2);
                p1 = subtractVertex(p2, s1);
                p0 = subtractVertex(p1, s0);
                width4 = [self computeWidth: distanceBetweenPoints(p3, p4)];
                strokeCount++;
#if NEW_METHOD
                t2 = tangentj(p0, p1, p2, p3, p4);
#else
                t2 = tangentForPoints(p0, p1, p2, p3, p4);
#endif  
            } else if (strokeCount >= 3) {
                
                p0 = p1;
                p1 = p2;
                p2 = p3;
                p3 = p4;
                p4 = newPoint;
                lastGoodPoint = newPoint;
                if (strokeCount == 3) {
                    strokeCount++;
                } else {
                    width2 = width3;
                    width3 = width4;
                    width4 = width5;
                }
                width5 = [self computeWidth: distanceBetweenPoints(p3, p4)];
                BOOL doRemoveKnot = NO;
                
                // corner detection
                vector a1 = vectorMake(p0, p1);
                vector b1 = vectorMake(p1, p2);
                vector c1 = vectorMake(p2, p3);
                vector d1 = vectorMake(p3, p4);
                
#define CORNER 1.618f
                
                if (angleInRadians(a1, b1) < CORNER && angleInRadians(b1, c1) < CORNER && angleInRadians(c1, d1) < CORNER)
                {                
                    if ([knotRemovals count]>0) {
                        for (Knot *aKnot in knotRemovals) {
                            [aKnot reset];
                        }
                    }
                    [knotRemovals addObject:[Knot knot]];
                    ((Knot*)[knotRemovals lastObject]).v = p2;
                    setCatmullConstants(p0, p1, p3, p4);
                    vertex tempLinearVertex;
                    GLfloat temp;
                    GLfloat linearTemp;
                    for (GLfloat t = 0.f; t<1.0f; t+=0.01f) {
                        for (Knot *aKnot in knotRemovals) {
                            temp = distanceBetweenPoints(aKnot.v, catmullCurve(t));
                            if (temp<aKnot.distance) {
                                aKnot.distance = temp;
                                aKnot.t = t;
                            }
                            tempLinearVertex = vertexMake(linearInterp(t, p1.x, p3.x), linearInterp(t, p1.y, p3.y));
                            linearTemp = distanceBetweenPoints(aKnot.v, tempLinearVertex);
                            if (linearTemp<aKnot.linearDistance) {
                                aKnot.linearDistance = linearTemp;
                                aKnot.closeLinear = tempLinearVertex;
                            }
                        }
                    }
                    GLfloat b, sumDistance = 0;
                    vertex prevPoint = p1;
                    for (Knot *aKnot in knotRemovals) {
                        aKnot.distanceToHere = distanceBetweenPoints(prevPoint, aKnot.v);
                        prevPoint = aKnot.v;
                        sumDistance += aKnot.distanceToHere;
                    }
                    b = distanceBetweenPoints(((Knot*)[knotRemovals lastObject]).v, p3);
                    sumDistance += b;
#if NEW_METHOD
                    doRemoveKnot = NO;
#else
                    doRemoveKnot = YES;
                    GLfloat ratio, ratioDiff;
                    
                    for (Knot *aKnot in knotRemovals) {
                        //vertex testVert;
                        
                        //if ([knotRemovals indexOfObject:aKnot] == 0) testVert = p1;
                        //else testVert = ((Knot*)[knotRemovals objectAtIndex:[knotRemovals indexOfObject:aKnot] - 1]).v;
                        
                        //vector a = vectorMake(testVert, aKnot.v);
                        //vector b = vectorMake(aKnot.v, p3);
                        
                        //NSLog(@"%f", angleInRadians(a, b));
                        
                        ratio = aKnot.distanceToHere/sumDistance;
                        ratioDiff = percentDiff(aKnot.t, ratio);
                        /*if (aKnot.linearDistance < 0.5f) {
#if DOTS
                            [self drawDot: aKnot.v withColor:rgbMake(0.f, 0.f, 1.f)];
#endif
#if NEW_METHOD
                            aKnot.remove = NO;
#else
                            aKnot.remove = YES;
#endif
                        }
                        else*/                     
                        if (aKnot.distance < 1.0 && ratioDiff < removal) {
                            if (!aKnot.remove) {
                                aKnot.remove = YES; 
                            }
                        } else {
                            doRemoveKnot = NO;
                        }
                    }
#endif
                }       
                     
                
                if (doRemoveKnot) {
#if DOTS
                    [self drawDot: p2 withColor:rgbMake(0.f, 1.f, 0.f)];
#endif
                    p2 = p1;
                    p1 = p0;
                    width3 = width2;
                }
                else {
                    t1 = t2;
#if NEW_METHOD
                    t2 = tangentj(p0, p1, p2, p3, p4);
                    setWeightedCurveConstants(t1, t2,w,w);
#else
                    t2 = tangentForPoints(p0, p1, p2, p3, p4);
                    setCurveConstants(t1, t2);
#endif  
                    
                    
                    [self renderVector];
                    [knotRemovals removeAllObjects];
#if VERTS
                    [self drawDot: p2 withColor:rgbMake(1.f, 0.f, 0.f)];
#endif
#if POLY
                    [self drawDot: cp1() withColor:rgbMake(0.2f, 1.f, 0.4f)];
                    [self drawDot: cp2() withColor:rgbMake(0.2f, 0.4f, 1.f)];
#endif
                }
                
                [delegate infoUpdatedWithData:tempDic];
                [delegate getPosition:newPoint andIsMove:YES];
                [tempDic removeAllObjects];
            } 
        }
    }
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    vertex newPoint = [self getOGLPos:[[touches anyObject] locationInView:self.view]];
    NSLog(@"pos end %@ x=%f y=%f",self,newPoint.x, newPoint.y);
    
    if (passThroughTouches) {
        [self.nextResponder touchesBegan:touches withEvent:event];
        return;
    }
    if (eraserSelected) {
        [delegate infoUpdated];
    } 
    else if (strokeCount ==1) {
        p3 = [self getOGLPos:[[touches anyObject] locationInView:self.view]];
        width3 = [self computeWidth: distanceBetweenPoints(p2, p3)];
        //NSLog(@"p2: %f %F  p3: %f %f",p2.x, p2.y,p3.x,p3.y);
        [self renderLinearVector];
        //[self drawDot];
    }
    else if (strokeCount == 2) {
        [self renderLinearVector]; 
        p2 = p3;
        p3 = [self getOGLPos:[[touches anyObject] locationInView:self.view]];
        width2 = width3;
        width3 = [self computeWidth: distanceBetweenPoints(p2, p3)];
        [self renderLinearVector]; 
    }
    else if (strokeCount >= 3) {
        strokeCount = 0;
        p0 = p1;
        p1 = p2;
        p2 = p3;
        p3 = p4;
        p4 = [self getOGLPos:[[touches anyObject] locationInView:self.view]];
        if (strokeCount == 3) {
            strokeCount++;
        } else {
            width2 = width3;
            width3 = width4;
            width4 = width5;
        }
        width5 = width4;//[self computeWidth: distanceBetweenPoints(p3, p4)];
        t1 = t2;
#if NEW_METHOD
        t2 = tangentj(p0, p1, p2, p3, p4);
        setWeightedCurveConstants(t1, t2,w,w);
#else
        t2 = tangentForPoints(p0, p1, p2, p3, p4);
        setCurveConstants(t1, t2);
#endif  
        [self renderVector];
        
#if VERTS
        [self drawDot: p2 withColor:rgbMake(1.f, 0.f, 0.f)];
#endif
        
        vertex s3,s2,s1,s0;
        s0 = subtractVertex(p1, p0);
        s1 = subtractVertex(p2, p1);
        s2 = subtractVertex(scaled(s1, 2), s0);
        s3 = subtractVertex(scaled(s2, 2), s1);
        
        p0 = p1;
        p1 = p2;
        p2 = p3;
        p3 = p4;
        p4 = addVertex(p3, s2);
        width2 = width3;
        width3 = width4;
        width4 = width5;
        t1 = t2;
#if NEW_METHOD
        t2 = tangentj(p0, p1, p2, p3, p4);
        setWeightedCurveConstants(t1, t2,w,w);
#else
        t2 = tangentForPoints(p0, p1, p2, p3, p4);
        setCurveConstants(t1, t2);
#endif  
        [self renderVector];
        
#if VERTS
        [self drawDot: p2 withColor:rgbMake(1.f, 0.f, 0.f)];
#endif
        p0 = p1;
        p1 = p2;
        p2 = p3;
        p3 = p4;
        p4 = addVertex(p3, s3);
        width2 = width3;
        width3 = width4;
        t1 = t2;
#if NEW_METHOD
        t2 = tangentj(p0, p1, p2, p3, p4);
        setWeightedCurveConstants(t1, t2,w,w);
#else
        t2 = tangentForPoints(p0, p1, p2, p3, p4);
        setCurveConstants(t1, t2);
#endif  
        [self renderVector];
        /*
#if DOTS
        myColor = rgbMake(1.f, 0.f, 0.f);
        [self drawDot];
        p2 = p3;
        [self drawDot];
        p2 = p4;
        [self drawDot];
        myColor = rgbMake(0.f, 0.f, 0.f);
#endif
        */
        
        [delegate infoUpdatedWithData:tempDic];
        [delegate getPosition:newPoint andIsMove:NO];
        [tempDic removeAllObjects];
    }
}

-(void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (eraserSelected) {
        
    } 
    else {
        strokeCount = 0;
        [tempDic removeAllObjects];
    }
}


@end
