//
//  ButtonInBarButton.h
//  Manage
//
//  Created by Thuong Nguyen on 10/11/13.
//
//

#import <UIKit/UIKit.h>

@interface ButtonInBarButton : UIButton

// The bar button item contain this button
@property (nonatomic, assign) UIBarButtonItem *barButtonItem;

@end
