//
//  ToodledoTaskCollection.h
//  Manage
//
//  Created by Cliff Viegas on 14/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Collections
@class TagCollection;
@class ToodledoFolderCollection;
@class DeletedListItemCollection;
@class TaskListCollection;

// Data Models
@class TaskList;
@class ArchiveList;

@interface ToodledoTaskCollection : NSObject {
	NSURLConnection						*toodledoConnection;
	NSMutableArray						*tasks;
	NSString							*toodledoKey;
	NSMutableData						*responseData;
}

@property	(nonatomic, copy)		NSString		*toodledoKey;
@property	(nonatomic, retain)		NSMutableArray	*tasks;


#pragma mark Initialisation
- (id)initUsingKey:(NSString *)theToodledoKey;

#pragma mark Get Methods
- (NSString *)getTasksFromServerWithModafter:(NSString *)modAfter andFields:(NSString *)fields andStart:(NSInteger)start;
- (NSString *)getDeletedTasksFromServerWithModafter:(NSString *)modAfter;
- (NSArray *)getListOfTagsCopy;
- (SInt64)getTopLevelLocalTaskIDFromToodledoParentTaskID:(SInt64)initialParentID;
- (NSInteger)getIndexOfTaskWithTaskID:(SInt64)serverTaskID;
- (NSInteger)getIndexOfTaskWithListItemID:(NSInteger)listItemID;

#pragma mark Delete Methods
- (BOOL)deleteAllTasksFromSection:(NSInteger)section mutableResponseString:(NSMutableString **)mutableResponseString
							error:(NSError **)error deletedListItemCollection:(DeletedListItemCollection *)deletedListItemCollection;

#pragma mark Add Methods
- (BOOL)addNewToodledoTasksFromTaskList:(TaskList *)taskList 
					 usingTagCollection:(TagCollection *)tagCollection
			andToodledoFolderCollection:(ToodledoFolderCollection *)toodledoFolderCollection
								section:(NSInteger)section
				  mutableResponseString:(NSMutableString **)mutableResponseString
								  error:(NSError **)error 
			   masterTaskListCollection:(TaskListCollection *)masterTaskListCollection 
                            archiveList:(ArchiveList *)archiveList;

#pragma mark Cancel Methods
- (void)cancelConnection;

#pragma mark Update Methods
- (BOOL)postUpdatesToodledoMetaWithResponseString:(NSMutableString **)mutableResponseString section:(NSInteger)section error:(NSError **)error;

- (BOOL)postUpdatesToTaskFolderNamesWithResponseString:(NSMutableString **)mutableResponseString 
                                               section:(NSInteger)section
                                                 error:(NSError **)error;

- (BOOL)postUpdatesToSubTasksWithResponseString:(NSMutableString **)mutableResponseString section:(NSInteger)section error:(NSError **)error;

- (BOOL)postUpdatesToTasksFromTaskListCollection:(TaskListCollection *)taskListCollection
							  usingTagCollection:(TagCollection *)tagCollection
					 andToodledoFolderCollection:(ToodledoFolderCollection *)toodledoFolderCollection
						  andAlreadyUpdatedTasks:(NSMutableArray *)alreadyUpdatedLocalTasks
										 section:(NSInteger)section
						   mutableResponseString:(NSMutableString **)mutableResponseString
										   error:(NSError **)error;

- (BOOL)postUpdatesToTaskFolderNamesFromTaskList:(TaskList *)taskList 
                                   usingFolderID:(SInt64)folderID 
                                  responseString:(NSMutableString **)mutableResponseString 
                                         section:(NSInteger)section
                                           error:(NSError **)error;

- (BOOL)postUpdatesToTaskFolderNamesFromTaskList:(TaskList *)taskList 
                         usingTaskListCollection:(TaskListCollection *)refTaskListCollection 
                                  responseString:(NSMutableString **)mutableResponseString 
                                         section:(NSInteger)section
                                           error:(NSError **)error;
@end
