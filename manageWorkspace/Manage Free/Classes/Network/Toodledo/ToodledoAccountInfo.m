//
//  ToodledoAccountInfo.m
//  Manage
//
//  Created by Cliff Viegas on 11/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ToodledoAccountInfo.h"

// Dictionary Method Helper
#import "DMHelper.h"

// JSON
#import	"JSON.h"

@interface ToodledoAccountInfo()
- (void)loadPropertiesFromResponse:(NSDictionary *)responseDictionary;
@end


@implementation ToodledoAccountInfo

@synthesize delegate;

@synthesize toodledoKey;

@synthesize userID;
@synthesize alias;
@synthesize pro;
@synthesize	dateFormat;
@synthesize timeZone;
@synthesize hideMonths;
@synthesize hotListPriority;
@synthesize hotListDueDate;
@synthesize hotListStar;
@synthesize hotListStatus;
@synthesize showTabNums;
@synthesize lastEditFolder;
@synthesize	lastEditContext;
@synthesize lastEditGoal;
@synthesize lastEditLocation;
@synthesize lastEditTask;
@synthesize lastDeleteTask;
@synthesize lastEditNotebook;
@synthesize	lastDeleteNotebook;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

// Getting account info
- (id)initWithDelegate:(id<ToodledoAccountInfoDelegate>)theDelegate andKey:(NSString *)theKey {
	if ((self = [super init])) {
		// First attempt to connect with existing details
		
		// Assign the toodledo key
		self.toodledoKey = theKey;
		
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Init toodledoconnection to nil
		toodledoConnection = nil;
		
		// If this doesn't work, set not connected to true (from delegate)
		
		// We can then set a footer message displaying the error when trying to connect, and tell them to
		// 'please update your details'.. or otherwise if it is connection error.
		
		// Then tell them to re-enter their account details
	}
	return self;
}

#pragma mark -
#pragma mark Public Connection Methods

- (void)connectToAccount {
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/account/get.php?key=%@", 
							self.toodledoKey];
	
	responseData = [[NSMutableData data] retain];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:10];
	toodledoConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)cancelConnection {
	if (toodledoConnection) {
		NSLog(@"ToodledoAccountInfo connection cancelled");
		[toodledoConnection cancel];
		//[responseData release];
	}
}


#pragma mark -
#pragma mark Network Delegates

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self.delegate accountInfoConnectionFailedWithError:error];
	// Call the delegate
	//[self.delegate authenticationFailedWithErrorCode:-1 andErrorDescription:[NSString str
	//[self.delegate connectionFailedWithError:error];
	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	[connection release];
	toodledoConnection = nil;
	
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	[responseData release];
	
	NSError *error;
	SBJsonParser *json = [[SBJsonParser new] autorelease];
	NSDictionary *responseDictionary = [json objectWithString:responseString error:&error];
	[responseString release];	
	
	if ([[responseDictionary valueForKey:@"errorDesc"] length] > 0) {
		// ERROR - display an error message as footer (need delegate for this)
		if ([self.delegate respondsToSelector:@selector(accountInfoFailedWithErrorCode:andErrorDescription:)]) {
			[self.delegate accountInfoFailedWithErrorCode:[[responseDictionary valueForKey:@"errorCode"] integerValue]
									  andErrorDescription:[responseDictionary valueForKey:@"errorDesc"]];
		}
	} else if ([[responseDictionary valueForKey:@"userid"] length] > 0) {
		// SUCCESS
		[self loadPropertiesFromResponse:responseDictionary];
		
		// Tell delegate that this has succeeded
		if ([self.delegate respondsToSelector:@selector(accountInfoSucceeded)]) {
			[self.delegate accountInfoSucceeded];
		}
		
	} else {
		// UNKNOWN ERROR
		if ([self.delegate respondsToSelector:@selector(accountInfoFailedWithErrorCode:andErrorDescription:)]) {
			[self.delegate accountInfoFailedWithErrorCode:-1 
									  andErrorDescription:@"Unknown error occurred"];
		}
		
	}
	//[self processResponseFromDictionary:responseDictionary];
}

#pragma mark -
#pragma mark Local Setup Properties Methods

// Loads the property fields with the provided response from toodledo
- (void)loadPropertiesFromResponse:(NSDictionary *)responseDictionary {
	self.userID = [DMHelper getNSStringValueForKey:@"userid" fromDictionary:responseDictionary];
	self.alias = [DMHelper getNSStringValueForKey:@"alias" fromDictionary:responseDictionary];
	self.pro = [DMHelper getBoolValueForKey:@"pro" fromDictionary:responseDictionary];
	self.dateFormat = [DMHelper getNSIntegerValueForKey:@"dateformat" fromDictionary:responseDictionary];
	self.timeZone = [DMHelper getNSIntegerValueForKey:@"timezone" fromDictionary:responseDictionary];
	self.hideMonths = [DMHelper getNSIntegerValueForKey:@"hidemonths" fromDictionary:responseDictionary];
	self.hotListPriority = [DMHelper getNSIntegerValueForKey:@"hotlistpriority" fromDictionary:responseDictionary];
	self.hotListDueDate = [DMHelper getNSIntegerValueForKey:@"hotlistduedate" fromDictionary:responseDictionary];
	self.hotListStar = [DMHelper getBoolValueForKey:@"hotliststar" fromDictionary:responseDictionary];
	self.hotListStatus = [DMHelper getBoolValueForKey:@"hotliststatus" fromDictionary:responseDictionary];
	self.showTabNums = [DMHelper getBoolValueForKey:@"showtabnums" fromDictionary:responseDictionary];
	self.lastEditFolder = [DMHelper getNSStringValueForKey:@"lastedit_folder" fromDictionary:responseDictionary];
	self.lastEditContext = [DMHelper getNSStringValueForKey:@"lastedit_context" fromDictionary:responseDictionary];
	self.lastEditGoal = [DMHelper getNSStringValueForKey:@"lastedit_goal" fromDictionary:responseDictionary];
	self.lastEditLocation = [DMHelper getNSStringValueForKey:@"lastedit_location" fromDictionary:responseDictionary];
	self.lastEditTask = [DMHelper getNSStringValueForKey:@"lastedit_task" fromDictionary:responseDictionary];
	self.lastDeleteTask = [DMHelper getNSStringValueForKey:@"lastdelete_task" fromDictionary:responseDictionary];
	self.lastEditNotebook = [DMHelper getNSStringValueForKey:@"lastedit_notebook" fromDictionary:responseDictionary];
	self.lastDeleteNotebook = [DMHelper getNSStringValueForKey:@"lastdelete_notebook" fromDictionary:responseDictionary];
}



@end
