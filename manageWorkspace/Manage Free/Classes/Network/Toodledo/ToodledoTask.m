//
//  ToodledoTask.m
//  Manage
//
//  Created by Cliff Viegas on 14/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ToodledoTask.h"

@implementation ToodledoTask

@synthesize serverID;
@synthesize title;
@synthesize tags;
@synthesize folderID;
@synthesize contextID;
@synthesize goalID;
@synthesize locationID;
@synthesize parentID;
@synthesize children;
@synthesize order;
@synthesize dueDate;
@synthesize dueDateMod;
@synthesize startDate;
@synthesize dueTime;
@synthesize	startTime;
@synthesize remind;
@synthesize repeat;
@synthesize repeatFrom;
@synthesize status;
@synthesize length;
@synthesize priority;
@synthesize star;
@synthesize modified;
@synthesize completed;
@synthesize added;
@synthesize timer;
@synthesize timerOn;
@synthesize note;
@synthesize meta;

// Helper properties
@synthesize needsUpdate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[title release];
	[tags release];

	[added release];
	[timer release];
	[timerOn release];
	[repeat release];

	[note release];
	[meta release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if ((self = [super init])) {
		self.needsUpdate = FALSE;
	}
	return self;
}

#pragma mark -
#pragma mark Update Task

/*+ (NSString *)updateTask:(SInt64)taskID withNewParent:(SInt64)newParentID {
	NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
	[newDictionary setValue:[NSString stringWithFormat:@"%qi", taskID] forKey:@"id"];
	[newDictionary setValue:[NSString stringWithFormat:@"%qi", newParentID] forKey:@"parent"];
	
	
}*/

/*NSMutableArray *postUpdatesArray = [[NSMutableArray alloc] init];

BOOL updatesComplete = TRUE;
NSInteger i = 1;
NSInteger maxEntries = 50;

for (ToodledoTask *task in self.tasks) {
	if ([task needsUpdate] == TRUE && i <= maxEntries) {
		// Update this task
		NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
		[newDictionary setValue:[NSString stringWithFormat:@"%qi", task.serverID] forKey:@"id"];
		[newDictionary setValue:[NSString stringWithFormat:@"%qi", task.parentID] forKey:@"parent"];
		
		[postUpdatesArray addObject:newDictionary];
		[newDictionary release];
		
		task.needsUpdate = FALSE;
		i++;
	} else if (i > maxEntries) {
		updatesComplete = FALSE;
		break;
	}
}

NSString *jsonString = @"";

// Update if we found some tasks that need to be updated
if ([postUpdatesArray count] > 0) {
	//SBJsonWriter *json = [SBJsonWriter alloc];
	jsonString = [postUpdatesArray JSONRepresentation]; //[json stringWithObject:postUpdatesArray];
	
	// Replace json string
	jsonString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
} else {
	// No updates to be made, return complete
	return YES;
}


[postUpdatesArray release];

NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/tasks/edit.php?key=%@;tasks=%@;fields=parent",
						self.toodledoKey, jsonString];

NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString]];

// Data returned by WebService
NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error:error ];
jsonString = [[NSString alloc] initWithData:returnData encoding: NSUTF8StringEncoding];

// Process data
SBJsonParser *json = [[SBJsonParser new] autorelease];

// Assign the json object
id object = [json objectWithString:jsonString];
[jsonString release];

// Create the responseArray and responseDictionary
NSArray *responseArray = nil;
NSDictionary *responseDictionary = nil;

if ([object isKindOfClass:[NSDictionary class]]) {
	responseDictionary = (NSDictionary *)object;
} else if ([object isKindOfClass:[NSArray class]]) {
	responseArray = (NSArray *)object;
}

if (*error) {
	// NSError occured
	[*mutableResponseString setString:[*error description]];
} else {
	// Go through the array
	if (responseArray != nil) {
		for (NSDictionary *responseDictionary in responseArray) {
			if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
				// ERROR
				NSString *toodledoError = [responseDictionary objectForKey:@"errorDesc"];
				[*mutableResponseString appendFormat:@"Toodledo Error: %@\n", toodledoError]; 
			} else {
				// Success
				SInt64 taskID = [DMHelper getSInt64ValueForKey:@"id" fromDictionary:responseDictionary];
				SInt64 parentID = [DMHelper getSInt64ValueForKey:@"parent" fromDictionary:responseDictionary];
				[*mutableResponseString appendFormat:@"Toodledo Server: Toodledo subtask (%qi) updated with new parent (%qi)\n", taskID, parentID];
			}
			
		}
	} else if (responseDictionary != nil) {
		if ([responseDictionary objectForKey:@"errorDesc"] != nil) {
			NSString *errorDesc = [responseDictionary valueForKey:@"errorDesc"];
			[*mutableResponseString setString:[NSString stringWithFormat:@"Toodledo Error: %@", errorDesc]];
		}
	}
	
}

return updatesComplete;*/

@end
