//
//  ToodledoSync.m
//  Manage
//
//  Created by Cliff Viegas on 9/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ToodledoAuthentication.h"
#import "JSON.h"						// JSON support
#import <CommonCrypto/CommonDigest.h>	// Need to import for CC_MD5 access
#import "KeychainWrapper.h"				// For storing stuff

// Application Setting 
#import "ApplicationSetting.h"

// Constants
#define kAppID			@"BCCE4F76BB5F4F128226D46DE1088412"
#define kMyAppToken		@"api4d58ac2e4fd58"

@interface ToodledoAuthentication()
// Signature methods
- (NSString *)generateSignatureForString:(NSString *)signatureString;
- (NSString *)generateKeyForDetail:(NSString *)detail andSessionToken:(NSString *)sessionToken;
// Handle response and error codes
- (void)processResponseFromDictionary:(NSDictionary *)responseDictionary;
// Toodledo Helper Methods
- (void)generateUserIDForAccount:(NSString *)account andDetail:(NSString *)detail;
- (void)generateSessionTokenForUserID:(NSString *)userID;
@end

@implementation ToodledoAuthentication

@synthesize delegate;
@synthesize tdDetail;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[self.tdDetail release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<ToodledoAuthenticationDelegate>)theDelegate {
	if ((self = [super init])) {
		// Assign the delegate
		self.delegate = theDelegate;
	}
	return self;
}

#pragma mark -
#pragma mark Public Methods

- (void)authenticateWithAccount:(NSString *)account andDetail:(NSString *)detail {
	// Before session token, we first need the user ID
	[self generateUserIDForAccount:account andDetail:detail];
	
	// First step is to request session token
	//[self generateSessionTokenForUserID
}

// If we have the user id, can go to the next step
- (void)authenticateWithUserID:(NSString *)userID {
	[self generateSessionTokenForUserID:userID];
}

- (void)assignDetail {
	// Load the keychain wrapper
	KeychainWrapper *keychainWrapper = [[KeychainWrapper alloc] init];
	
	NSData *data = [keychainWrapper searchKeychainCopyMatching:@"ToodledoManageDetail"];
	NSString *content = [[[NSString alloc] initWithBytes:[data bytes] 
												 length:[data length] 
											   encoding:NSUTF8StringEncoding] autorelease];
	
	//[data release];
	self.tdDetail = content;
	
	[keychainWrapper release];
}

- (void)cancelConnection {
	if (toodledoConnection) {
		[toodledoConnection cancel];
		[responseData release];
	}
}

#pragma mark -
#pragma mark Toodledo Helper Methods

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding string:(NSString *) string{
	return (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
                                                               (CFStringRef)string,
                                                               NULL,
                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]%. ",
                                                               CFStringConvertNSStringEncodingToEncoding(encoding));
}

- (void)generateUserIDForAccount:(NSString *)account andDetail:(NSString *)detail  {
	self.tdDetail = detail;
    
	NSString *signature = [self generateSignatureForString:[NSString stringWithFormat:@"%@%@", account, kMyAppToken]];
    
    detail = [self urlEncodeUsingEncoding:NSUTF8StringEncoding string:detail];
    
    
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/account/lookup.php?appid=%@;sig=%@;email=%@;pass=%@", 
							kAppID, signature, account, detail];
	
    // Replace http string using correct encoding
	//httpString = [httpString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
	responseData = [[NSMutableData data] retain];
    
    
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:10];
	toodledoConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}

- (void)generateSessionTokenForUserID:(NSString *)userID {
	userID = [userID stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
	userID = [userID stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
	NSString *signature = [self generateSignatureForString:[NSString stringWithFormat:@"%@%@", userID, kMyAppToken]];

	NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
	NSString *device = [[UIDevice currentDevice] name];
	NSString *os = [[UIDevice currentDevice] systemVersion];
	
	version = [version stringByReplacingOccurrencesOfString:@"." withString:@"_"];
	os = [os stringByReplacingOccurrencesOfString:@"." withString:@"_"];
	device = [device stringByReplacingOccurrencesOfString:@" " withString:@"_"];
	
	NSString *httpString = [NSString stringWithFormat:@"http://api.toodledo.com/2/account/token.php?userid=%@;appid=%@;vers=%@;device=%@;os=%@;sig=%@",
							userID, kAppID, version, device, os, signature];
	
    // Replace http string using correct encoding
    //httpString = [httpString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    //httpString = [httpString stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
	httpString = [httpString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	responseData = [[NSMutableData data] retain];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:httpString] 
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:10];
	toodledoConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

#pragma mark -
#pragma mark Signature Methods

- (NSString *)generateSignatureForString:(NSString *)signatureString {
	const char *cStr = [signatureString UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5(cStr, strlen(cStr), result);
	NSString *signature = [NSString stringWithFormat: @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X", result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7], result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]];
	signature = [signature lowercaseString];
	
	return signature;
}

- (NSString *)generateKeyForDetail:(NSString *)detail andSessionToken:(NSString *)sessionToken {
	NSString *detailHash = [self generateSignatureForString:detail];
	
	const char *cStr = [[NSString stringWithFormat:@"%@%@%@", detailHash, kMyAppToken, sessionToken] UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5(cStr, strlen(cStr), result);
	NSString *key = [NSString stringWithFormat: @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X", result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7], result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]];
	key = [key lowercaseString];
	
	return key;
}

#pragma mark -
#pragma mark Network Delegates

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	// Call the delegate
	//[self.delegate authenticationFailedWithErrorCode:-1 andErrorDescription:[NSString str
	if ([self.delegate respondsToSelector:@selector(authenticationConnectionFailedWithError:)]) {

		[self.delegate authenticationConnectionFailedWithError:error];
	}
	
	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	[connection release];
	
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
	[responseData release];
	
	NSError *error;
	SBJsonParser *json = [[SBJsonParser new] autorelease];
	NSDictionary *responseDictionary = [json objectWithString:responseString error:&error];
	[responseString release];	
	
	[self processResponseFromDictionary:responseDictionary];
}


- (void)processResponseFromDictionary:(NSDictionary *)responseDictionary {
	if ([[responseDictionary valueForKey:@"errorDesc"] length] > 0) {
		// ERROR
		NSInteger errorCode = [[responseDictionary valueForKey:@"errorCode"] integerValue];
		NSString *errorDescription = [responseDictionary valueForKey:@"errorDesc"];
		
		if ([self.delegate respondsToSelector:@selector(authenticationFailedWithErrorCode:andErrorDescription:)]) {
			[self.delegate authenticationFailedWithErrorCode:errorCode andErrorDescription:errorDescription];
		}
		
	} else if ([[responseDictionary valueForKey:@"userid"] length] > 0) {
		// We have the user id, save it to database
		ApplicationSetting *appSetting = [[ApplicationSetting alloc] init];
		appSetting.name = @"ToodledoUserID";
		appSetting.data = [responseDictionary valueForKey:@"userid"];
		[appSetting updateDatabase];
		[appSetting release];

		// Now generate the session token
		[self generateSessionTokenForUserID:[responseDictionary valueForKey:@"userid"]];
	} else if ([[responseDictionary valueForKey:@"token"] length] > 0) {
		// Save the session token to the database
		[ApplicationSetting updateSettingName:@"ToodledoSessionToken" withData:[responseDictionary valueForKey:@"token"]];

		NSString *toodledoKey = [self generateKeyForDetail:self.tdDetail andSessionToken:[responseDictionary valueForKey:@"token"]];

		// Save the key that was generated
		[ApplicationSetting updateSettingName:@"ToodledoKey" withData:toodledoKey];
		
		// Report success back to delegate
		if ([self.delegate respondsToSelector:@selector(authenticationSucceeded)]) {
			[self.delegate authenticationSucceeded];
		}
		
	}
}

/*
 
 1 : You did not specify a key for authentication.
 2 : The authentication key that you provided as expired or is invalid.
 3 : No userid specified when getting a token.
 4 : No AppId specified.
 5 : Invalid AppID. You must register an AppID before you can use it.
 6 : Invalid Userid. The specified user was not found
 7 : Excessive Tokens. Your app has requested too many tokens for this user in a short amount of time. You should cache and use a token until it expires.
 8 : No signature. You must sign a token request to validate it.
 9 : Invalid signature. The signature that you included is invalid.
 10 : The email address was left blank.
 11 : The password was left blank.
 12 : The email/password was invalid.
 100 : Unknown Error.
 400 : Your app has been blocked by the user.
 500 : The Toodledo server is offline for maintenance.
 
 */


@end
