//
//  DeletedListItem.h
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
#import "ListItem.h"

@interface DeletedListItem : NSObject {
	NSInteger			listItemID;
	NSString			*title;
	NSInteger			listID;
	NSInteger			parentListItemID;
	SInt64				toodledoTaskID;
	NSString			*deletedDateTime;
}

@property (nonatomic, assign)	NSInteger	listItemID;
@property (nonatomic, copy)		NSString	*title;
@property (nonatomic, assign)	NSInteger	listID;
@property (nonatomic, assign)	NSInteger	parentListItemID;
@property (nonatomic, assign)	SInt64		toodledoTaskID;
@property (nonatomic, copy)		NSString	*deletedDateTime;

#pragma mark Public Methods
- (void)softDeleteListItem:(ListItem *)listItem;
+ (void)emptyDeletedListItemDatabase;

@end
