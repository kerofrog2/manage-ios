//
//  DeletedList.h
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
#import "TaskList.h"

@interface DeletedTaskList : NSObject {
	NSInteger		listID;
	NSString		*title;
	SInt64			toodledoFolderID;
	NSString		*deletedDateTime;
    BOOL            isNote;
}

@property (nonatomic, assign)	NSInteger	listID;
@property (nonatomic, copy)		NSString	*title;
@property (nonatomic, assign)	SInt64		toodledoFolderID;
@property (nonatomic, copy)		NSString	*deletedDateTime;
@property (assign)              BOOL        isNote;

#pragma mark Public Methods
- (void)softDeleteList:(TaskList *)taskList;
+ (void)emptyDeletedListDatabase;

@end
