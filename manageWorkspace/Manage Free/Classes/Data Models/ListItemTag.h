//
//  ListItemTag.h
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ListItemTag : NSObject {
	NSInteger	listItemID;
	NSInteger	tagID;
}

@property (nonatomic)	NSInteger	listItemID;
@property (nonatomic)	NSInteger	tagID;

#pragma mark Initialisation
- (id)initWithListItemID:(NSInteger)theListItemID andTagID:(NSInteger)theTagID;

#pragma mark Database Methods
- (void)insertSelfIntoDatabase;

@end
