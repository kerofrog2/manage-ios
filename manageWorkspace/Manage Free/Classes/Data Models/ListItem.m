//
//  ListItem.m
//  Manage
//
//  Created by Cliff Viegas on 5/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "ListItem.h"

// Method Helper
#import "MethodHelper.h"

// Data Collections
#import "ListItemTagCollection.h"
#import "TagCollection.h"

// Data Models
#import "ListItemTag.h"

// Business Layer
#import	"BLListItem.h"
#import "BLRecurringListItem.h"

// Application Setting
#import "ApplicationSetting.h"


@implementation ListItem

@synthesize listItemID;
@synthesize	listID;
@synthesize categoryID;
@synthesize priority;
@synthesize title;
@synthesize creationDateTime;
@synthesize dueDate;
@synthesize dueTime;
@synthesize notes;
@synthesize scribble;
@synthesize completed;
@synthesize itemOrder;
@synthesize parentListItemID;
@synthesize subItemOrder;
@synthesize parentListTitle;
@synthesize completedDateTime;
@synthesize archived;
@synthesize archivedDateTime;
@synthesize recurringListItemID;
@synthesize hasAlarms;
@synthesize modifiedDateTime;
@synthesize toodledoTaskID;
@synthesize isSelected;


@synthesize alertTime;
@synthesize sound;
@synthesize hasSnooze;
@synthesize hasJustComplete;

// Extra properties
@synthesize listItemTagCollection;


#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[self.title release];
	[self.creationDateTime release];
	[self.dueDate release];
	[self.notes release];
	[self.scribble release];
	[self.parentListTitle release];
	[self.completedDateTime release];
	[self.archivedDateTime release];
	[self.modifiedDateTime release];
    
    [self.dueTime release];
    
    [self.alertTime release];

    [self.sound release];
    
	// Extra properties
	[self.listItemTagCollection release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation
//(update alerttime field)
- (id)initNewListItemWithID:(NSInteger)theListID andTitle:(NSString *)theTitle andNotes:(NSString *)theNotes andCompleted:(BOOL)isCompleted andParentTitle:(NSString *)theParentTitle andDueDate:(NSString *)theDueDate andDueTime:(NSString *)theDueTime andAlertTime:(NSString *)theAlertTime andSound:(NSString *)theSound andSnooze:(BOOL)theSnooze andListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection andCompletedDateTime:(NSString *)theCompletedDateTime andPriority:(NSInteger)thePriority addToEnd:(BOOL)addToEnd{
	if ((self = [super init])) {
		listID = theListID;
		self.priority = thePriority;
		categoryID = 0;
		self.title = theTitle;
		self.creationDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		self.dueDate = theDueDate;
        self.dueTime = theDueTime;
		self.notes = theNotes;
		self.scribble = @"";
		self.completed = isCompleted;
        self.hasJustComplete = isCompleted;
		self.subItemOrder = 0;
		self.parentListTitle = theParentTitle;
		self.completedDateTime = theCompletedDateTime;
		self.archived = FALSE;
		self.archivedDateTime = @"";
		self.recurringListItemID = -1;
		self.hasAlarms = FALSE;
		self.modifiedDateTime = @"";
		self.toodledoTaskID = -1;
        
        // ALert Time
        self.alertTime = theAlertTime;
        self.sound = theSound;
        self.hasSnooze = theSnooze;
		
        // Perhaps look at getting first item order... hmm... test
        
		// Need to get last item order.
        if (addToEnd == FALSE) {
            self.itemOrder = [BLListItem getNextItemOrderForListID:theListID];
        } else if (addToEnd == TRUE) {
            self.itemOrder = [BLListItem getFirstItemOrderForListID:theListID];
        }
		

		self.parentListItemID = -1;		// No parent list item ID
		
		// Insert into database and get list item id
		listItemID = [self insertSelfIntoDatabase];

		if (theListItemTagCollection != nil) {
			// Update the list items tag collection with the correct list item ID
			[theListItemTagCollection updateTagCollectionWithListItemID:self.listItemID];
			
			// Need to add the list item tag collection data to DB too
			[theListItemTagCollection insertSelfIntoDatabase];
		}
		
		// Init new tag collection
		self.listItemTagCollection = [[ListItemTagCollection alloc] initWithListItemTagsForID:self.listItemID];
		//[self.listItemTagCollection release];
	}
	return self;
}

// DBInsert is whether or not to add to db initially, and is empty tag list is for whether we want to return a tag list without using db call or not
- (id)initEmptyListItemWithListID:(NSInteger)theListID andParentTitle:(NSString *)theParentTitle DBInsert:(BOOL)dbInsert emptyTagList:(BOOL)isEmptyTagList {
	if ((self = [super init])) {
		listID = theListID;
		self.priority = 0;
		categoryID = 0;
		self.title = @"";
		self.creationDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		self.dueDate = @"";
        self.dueTime = @"";
		self.notes = @"";
		self.scribble = @"";
		self.completed = NO;
        self.hasJustComplete = NO;
		self.subItemOrder = 0;
		self.parentListTitle = theParentTitle;
		self.completedDateTime = @"";
		self.archived = FALSE;
		self.archivedDateTime = @"";
		self.recurringListItemID = -1;
		self.hasAlarms = FALSE;
		self.modifiedDateTime = @"";
		self.toodledoTaskID = -1;
        
        // Alert time
        self.alertTime = @"";
        self.sound = @"";
        self.hasSnooze = FALSE;
		
		// Need to get last item order.
		self.itemOrder = [BLListItem getNextItemOrderForListID:theListID];
		self.parentListItemID = -1;		// No parent list item ID
		
		// Insert into database and get list item id
		if (dbInsert == TRUE) {
			listItemID = [self insertSelfIntoDatabase];
		}
		
		// Init new tag collection
		if (isEmptyTagList == FALSE) {
			self.listItemTagCollection = [[[ListItemTagCollection alloc] initWithListItemTagsForID:self.listItemID] autorelease];
		} else {
			self.listItemTagCollection = [[ListItemTagCollection alloc] init];
		}

	}
	return self;
}

//(update alerttime field)
- (id)initNewSubListItemWithID:(NSInteger)theListID andTitle:(NSString *)theTitle andNotes:(NSString *)theNotes 
				  andCompleted:(BOOL)isCompleted andParentTitle:(NSString *)theParentTitle 
					andDueDate:(NSString *)theDueDate andDueTime:(NSString *)theDueTime
                  andAlertTime:(NSString *)theAlertTime
                      andSound:(NSString *)theSound
                     andSnooze:(BOOL)theSnooze
		   andParentListItemID:(NSInteger)theParentListItemID andItemOrder:(NSInteger)theItemOrder 
			   andSubItemOrder:(NSInteger)theSubItemOrder
	  andListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection
		  andCompletedDateTime:(NSString *)theCompletedDateTime {
	if ((self = [super init])) {
		listID = theListID;
		self.priority = 0;
		categoryID = 0;
		self.title = theTitle;
		self.creationDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		self.dueDate = theDueDate;
        self.dueTime = theDueTime;
		self.notes = theNotes;
		self.scribble = @"";
		self.completed = isCompleted;
        self.hasJustComplete = isCompleted;
		self.subItemOrder = theSubItemOrder;
		self.parentListTitle = theParentTitle;
		self.completedDateTime = theCompletedDateTime;
		self.archived = FALSE;
		self.archivedDateTime = @"";
		self.recurringListItemID = -1;
		self.hasAlarms = FALSE;
		self.modifiedDateTime = @"";
		self.toodledoTaskID = -1;
        
        // Alert time
        self.alertTime = theAlertTime;
        self.sound = theSound;
        self.hasSnooze = theSnooze;
		
		// Need to get last item order.
		self.itemOrder = theItemOrder;
		
		self.parentListItemID = theParentListItemID;
		
		// Insert into database and get list item id
		listItemID = [self insertSelfIntoDatabase];
		
		// Update the list items tag collection with the correct list item ID
		[theListItemTagCollection updateTagCollectionWithListItemID:self.listItemID];
		
		// Need to add the list item tag collection data to DB too
		[theListItemTagCollection insertSelfIntoDatabase];
		
		// Init new tag collection
		self.listItemTagCollection = [[ListItemTagCollection alloc] initWithListItemTagsForID:self.listItemID];
		//[self.listItemTagCollection release];
        
	}
	return self;
}

- (id)initNewSubListItemFromListItem:(ListItem *)listItem withListID:(NSInteger)theListID toParentListItemID:(NSInteger)theParentListItemID andParentTitle:(NSString *)theParentTitle andItemOrder:(NSInteger)theItemOrder
			   andSubItemOrder:(NSInteger)theSubItemOrder
	  andListItemTagCollection:(ListItemTagCollection *)theListItemTagCollection {
	if ((self = [super init])) {
        listID = theListID;
		self.priority = listItem.priority;
		categoryID = listItem.categoryID;
		self.title = listItem.title;
		self.creationDateTime = listItem.creationDateTime;
		self.dueDate = listItem.dueDate;
        self.dueTime = listItem.dueTime;
		self.notes = listItem.notes;
		self.scribble = listItem.scribble;
		self.completed = listItem.completed;
        self.hasJustComplete = listItem.hasJustComplete;
		self.subItemOrder = theSubItemOrder;
		self.parentListTitle = theParentTitle;
		self.completedDateTime = listItem.completedDateTime;
		self.archived = listItem.archived;
		self.archivedDateTime = listItem.archivedDateTime;
		self.recurringListItemID = -1;
		self.hasAlarms = listItem.hasAlarms;
		self.modifiedDateTime = listItem.modifiedDateTime;
		self.toodledoTaskID = -1;
        
        // Alert time
        self.alertTime = listItem.alertTime;
        self.sound = listItem.sound;
        self.hasSnooze = listItem.hasSnooze;
		
		// Need to get last item order.
		self.itemOrder = theItemOrder;
		
		self.parentListItemID = theParentListItemID;
		
		// Insert into database and get list item id
		listItemID = [self insertSelfIntoDatabase];
		
		// Update the list items tag collection with the correct list item ID
		[theListItemTagCollection updateTagCollectionWithListItemID:self.listItemID];
		
		// Need to add the list item tag collection data to DB too
		[theListItemTagCollection insertSelfIntoDatabase];
		
		// Init new tag collection
		self.listItemTagCollection = [[ListItemTagCollection alloc] initWithListItemTagsForID:self.listItemID];
		//[self.listItemTagCollection release];
        
	}
	return self;
}

// This is called from the TaskList, adds the rest of the properties from the TaskList method -(void)loadListItemsWithListID
- (id)initExistingListItemWithListItemID:(NSInteger)theListItemID andListID:(NSInteger)theListID 
						 andPriority:(NSInteger)thePriority andParentTitle:(NSString *)theParentTitle {
	if ((self = [super init])) {
		listItemID = theListItemID;
		listID = theListID;
		self.priority = thePriority;
		self.parentListTitle = theParentTitle;
		
		// Init new tag collection
        ListItemTagCollection *tempTagCollection = [[ListItemTagCollection alloc] initWithListItemTagsForID:self.listItemID];
		self.listItemTagCollection = tempTagCollection;
        [tempTagCollection release];
		//[self.listItemTagCollection release];
	}
	return self;
}


// Init with a copy of another list item
- (id)initWithListItemCopy:(ListItem *)listItemToCopy {
	if ((self = [super init])) {
		listID = listItemToCopy.listID;
		self.priority = listItemToCopy.priority;
		categoryID = listItemToCopy.categoryID;
		listItemID = listItemToCopy.listItemID;
		self.title = listItemToCopy.title;
		self.creationDateTime = listItemToCopy.creationDateTime;
		self.dueDate = listItemToCopy.dueDate;
        self.dueTime = listItemToCopy.dueTime;
		self.notes = listItemToCopy.notes;
		self.scribble = listItemToCopy.scribble;
		self.completed = listItemToCopy.completed;
        self.hasJustComplete = listItemToCopy.hasJustComplete;
		self.subItemOrder = listItemToCopy.subItemOrder;
		self.parentListTitle = listItemToCopy.parentListTitle;
		self.completedDateTime = listItemToCopy.completedDateTime;
		self.archived = listItemToCopy.archived;
		self.archivedDateTime = listItemToCopy.archivedDateTime;
		self.recurringListItemID = listItemToCopy.recurringListItemID;
		self.hasAlarms = listItemToCopy.hasAlarms;
		self.modifiedDateTime = listItemToCopy.modifiedDateTime;
		self.toodledoTaskID = listItemToCopy.toodledoTaskID;
		
		self.itemOrder = listItemToCopy.itemOrder;
		
		self.parentListItemID = listItemToCopy.parentListItemID;
        
        // AlertTime
        self.alertTime = listItemToCopy.alertTime;
        self.sound = listItemToCopy.sound;
        self.hasSnooze = listItemToCopy.hasSnooze;

		
		// Init new tag collection
        ListItemTagCollection *tempTagCollection = [[ListItemTagCollection alloc] initWithListItemTagsForID:self.listItemID];
        self.listItemTagCollection = tempTagCollection;
        [tempTagCollection release];
    
	}
	return self;
}

#pragma mark -
#pragma mark Update Methods

- (void)updateSelfWithListItem:(ListItem *)refListItem {
	listID = refListItem.listID;
	self.priority = refListItem.priority;
	categoryID = refListItem.categoryID;
	listItemID = refListItem.listItemID;
	self.title = refListItem.title;
	self.creationDateTime = refListItem.creationDateTime;
	self.dueDate = refListItem.dueDate;
    self.dueTime = refListItem.dueTime;
	self.notes = refListItem.notes;
	self.scribble = refListItem.scribble;
	self.completed = refListItem.completed;
    self.hasJustComplete = refListItem.hasJustComplete;
	self.subItemOrder = refListItem.subItemOrder;
	self.parentListTitle = refListItem.parentListTitle;
	self.completedDateTime = refListItem.completedDateTime;
	self.archived = FALSE;
	self.archivedDateTime = @"";
	self.recurringListItemID = refListItem.recurringListItemID;
	self.hasAlarms = refListItem.hasAlarms;
	self.modifiedDateTime = refListItem.modifiedDateTime;
	self.toodledoTaskID = refListItem.toodledoTaskID;
    
	self.itemOrder = refListItem.itemOrder;
	self.parentListItemID = refListItem.parentListItemID;
    
    // Alert time
    self.alertTime = refListItem.alertTime;
    self.sound = refListItem.sound;
    self.hasSnooze = refListItem.hasSnooze;
	
	// Init new tag collection
	[self.listItemTagCollection reloadListItemTagsForID:self.listItemID];
	//[self.listItemTagCollection release];
}

#pragma mark -
#pragma mark Sort Methods

- (NSComparisonResult)localizedCompare:(NSString *)otherDueDate {
	// Both nil
	if (self.dueDate == nil || [self.dueDate length] == 0) {
		if (otherDueDate == nil || [otherDueDate length] == 0) {
			return NSOrderedSame;
		} else if (otherDueDate != nil && [otherDueDate length] > 0) {
			// self nil, list item not nil
			return NSOrderedAscending;
		}
	}
	
	// List item nil, self not nil
	if (otherDueDate == nil || [otherDueDate length] == 0) {
		if (self.dueDate != nil && [self.dueDate length] > 0) {
			return NSOrderedDescending;
		}
	}
	
    // To fix analyze issue (API Misuse) - Argument to 'NSString' method 'compare:' cannot be nil
    if (otherDueDate == nil) {
        otherDueDate = @"";
    }
    
	// Both not nil
	return [self.dueDate compare:otherDueDate];
}

#pragma mark -
#pragma mark Database Methods


- (NSInteger)insertSelfIntoDatabase {
	listItemID = [BLListItem insertNewListItem:self];
	[ApplicationSetting updateSettingName:@"LastAddTaskDateTime" withData:self.creationDateTime];
	return listItemID;
}

- (void)updateDatabase {
	// Update list item in database
	self.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
    
    if (self.creationDateTime) {
        self.creationDateTime = self.modifiedDateTime;
    }
	
	[BLListItem updateDatabaseWithListItem:self];
	
	// Also update last edit task date time in app settings
	[ApplicationSetting updateSettingName:@"LastEditTaskDateTime" withData:self.modifiedDateTime];
}


- (void)updateDatabaseWithoutLastEditTaskDateTime {
	self.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	[BLListItem updateDatabaseWithListItem:self];
}

- (void)updateDatabaseWithoutModifiedLog {
	[BLListItem updateDatabaseWithListItem:self];
}

#pragma mark -
#pragma mark Class Methods

// Check to see if this is linked to a recurring list item in the corresponding table
- (BOOL)isLinkedToRecurringListItem {
	if ([BLRecurringListItem checkIfRecurringListItemExistsWithID:self.recurringListItemID] == TRUE) {
		return TRUE;
	}
	
	return FALSE;
}

// Due date in future = positive, past = negative
- (NSInteger)getDueDateDifferenceFromToday {
	if (self.dueDate == nil || [self.dueDate length] == 0) {
		// Just return a large number, but this should never occur
		return 100;
	}
	
	NSDate *lastDate = [MethodHelper dateGMTFromString:self.dueDate usingFormat:K_DATEONLY_FORMAT];  // [MethodHelper dateFromString:self.dueDate usingFormat:K_DATEONLY_FORMAT];
	NSString *todayDateString = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];  //[MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATEONLY_FORMAT];
	NSDate *todaysDate = [MethodHelper dateGMTFromString:todayDateString usingFormat:K_DATETIME_FORMAT]; //[MethodHelper dateFromString:todayDateString usingFormat:K_DATEONLY_FORMAT];
	NSTimeInterval lastDiff = [lastDate timeIntervalSinceNow];
	NSTimeInterval todaysDiff = [todaysDate timeIntervalSinceNow];
    // Subtracting 'todayDiff' is important due to timeIntervalSinceNow not respecting timezones.
	NSTimeInterval dateDiff = lastDiff - todaysDiff;
	NSInteger dayDifference = (int)(dateDiff / 86400.0f);
	return dayDifference;
}

// Get completed date time difference
- (NSInteger)getCompletedDateDifferenceFromToday {
	if (self.completedDateTime == nil || [self.completedDateTime length] == 0) {
		// Just return a large number, but this should never occur
		return 100;
	}
	
	NSDate *lastDate = [MethodHelper dateFromString:self.completedDateTime usingFormat:K_DATETIME_FORMAT];
	NSString *todayDateString = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
	NSDate *todaysDate = [MethodHelper dateFromString:todayDateString usingFormat:K_DATETIME_FORMAT];
	NSTimeInterval lastDiff = [lastDate timeIntervalSinceNow];
	NSTimeInterval todaysDiff = [todaysDate timeIntervalSinceNow];
	NSTimeInterval dateDiff = lastDiff - todaysDiff;
	NSInteger dayDifference = (int)(dateDiff / 86400.0f);
	return dayDifference;
}

- (NSInteger)getNewSubItemOrder {
	// Get the largest sub item order
	// Add 1 to this sub item order
	return [BLListItem getNextSubItemOrderForListItemID:self.listItemID];
	
	// It needs to be inserted after the parent list item location in array (already have method to get index)
}

// Also deletes repeating task from recurring task table
- (void)deleteRepeatingTaskDetails {
	if ([self recurringListItemID] != -1) {
		[BLRecurringListItem deleteRecurringListItemWithRecurringListItemID:[self recurringListItemID]];
		self.recurringListItemID = -1;
	}
	
}

// Get toodledo list of tags
- (NSString *)getToodledoStringOfTagsUsingTagCollection:(TagCollection *)tagCollection {
	if ([self.listItemTagCollection.listItemTags count] == 0) {
		return @"";
	}
	
	NSString *tags = @"";
	BOOL firstRun = TRUE;
	for (ListItemTag *listItemTag in self.listItemTagCollection.listItemTags) {
		if (firstRun) {
			tags = [tagCollection getNameForTagWithID:listItemTag.tagID];
			firstRun = FALSE;
		} else {
			tags = [NSString stringWithFormat:@"%@, %@",
					tags,
					[tagCollection getNameForTagWithID:listItemTag.tagID]];
		}
	}
	
	return tags;
}

+ (EnumHighlighterColor) getEnumHightlighterColorWithPriority:(NSInteger)aPriority {
    if (aPriority == 0) {
        return EnumHighlighterColorWhite;
    }
    if (aPriority == 1) {
        return EnumHighlighterColorYellow;
    }
    if (aPriority == 2) {
        return EnumHighlighterColorRed;
    }
    return EnumHighlighterColorNone;
}

@end
