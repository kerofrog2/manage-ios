//
//  UpdateDatabase.h
//  Manage
//
//  Created by Kerofrog on 5/14/13.
//
//

#import <Foundation/Foundation.h>

@interface UpdateDatabase : NSObject

+ (void) startUpdate;

@end
