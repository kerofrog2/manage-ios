//
//  Tag.m
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "Tag.h"

// Business Layer
#import "BLTag.h"

@implementation Tag

@synthesize tagID;
@synthesize name;
@synthesize tagOrder;
@synthesize parentTagID;
@synthesize tagDepth;
@synthesize colour;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[name release];
    [colour release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithTagID:(NSInteger)theTagID andName:(NSString *)theName 
	 andParentTagID:(NSInteger)theParentTagID andTagOrder:(NSInteger)theTagOrder
		andTagDepth:(NSInteger)theTagDepth andTagColour:(NSString *)theTagColour {
	if ((self = [super init])) {
		self.tagID = theTagID;
		self.name = theName;
		self.parentTagID = theParentTagID;
		self.tagOrder = theTagOrder;
		self.tagDepth = theTagDepth;
        self.colour = theTagColour;
	}
	return self;
}

#pragma mark -
#pragma mark Database Methods

- (NSInteger)insertSelfIntoDatabase {
	return [BLTag insertNewTag:self];
}

#pragma mark -
#pragma mark Class Methods

- (NSInteger)getNewTagOrder {
	return [BLTag getNextTagOrderForTag:self];
}

#pragma mark -
#pragma mark Update Methods

- (void)updateSelfInDatabase {
	[BLTag updateTag:self];
}

#pragma mark -
#pragma mark Copy Methods

- (id)copyWithZone:(NSZone *)zone {
	Tag *copyTag = [[[self class] allocWithZone:zone] init];
	copyTag.tagID = self.tagID;
	copyTag.name = self.name;
	copyTag.parentTagID = self.parentTagID;
	copyTag.tagOrder = self.tagOrder;
	copyTag.tagDepth = self.tagDepth;
	copyTag.colour = self.colour;
    
	return copyTag;
}

@end
