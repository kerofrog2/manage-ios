//
//  UpdateDatabase.m
//  Manage
//
//  Created by Kerofrog on 5/14/13.
//
//

#import "UpdateDatabase.h"
#import "SQLiteAccess.h"

@implementation UpdateDatabase

+(void)startUpdate {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    // Check database is not updated for version 2.1.1
    if ([userDefaults boolForKey:DATABASE_VERSION_2_1] == NO) {
        [SQLiteAccess updateWithSQL:@"ALTER TABLE \"main\".\"ListItem\" ADD COLUMN \"DueTime\" VARCHAR DEFAULT null"];
        [userDefaults setBool:YES forKey:DATABASE_VERSION_2_1];
    }
//    [SQLiteAccess updateWithSQL:@"ALTER TABLE \"main\".\"ListItem\" ADD COLUMN \"AlertTime\" VARCHAR DEFAULT null"];
//    [SQLiteAccess updateWithSQL:@"ALTER TABLE \"main\".\"ListItem\" ADD COLUMN \"Sound\" VARCHAR DEFAULT null"];
//    [SQLiteAccess updateWithSQL:@"ALTER TABLE \"main\".\"ListItem\" ADD COLUMN \"Snooze\" INTEGER DEFAULT 0"];
}

@end
