//
//  PropertyDetail.h
//  Manage
//
//  Created by Cliff Viegas on 5/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	EnumPropertyTypeString = 0,
	EnumPropertyTypeList = 1,
	EnumPropertyTypeBoolean = 2,
	EnumPropertyTypeDisplay = 100,	// User can't change, for display purposes only
	EnumPropertyTypeCustom = 900
} EnumPropertyType;

@interface PropertyDetail : NSObject {
	NSString		*name;			// The name of the property
	NSString		*friendlyName;	// Friendly name to display to user
	NSString		*section;		// The section in the tableview
	NSInteger		propertyType;	// The type of property, an enum
	NSString		*description;	// Helper string with descriptrion of property
	BOOL			enabled;		// Whether the property is selectable/clickable
	NSMutableArray	*list;			// Array of list items (if needed)

}

@property (nonatomic, copy)		NSString		*name;
@property (nonatomic, copy)		NSString		*friendlyName;
@property (nonatomic, copy)		NSString		*section;
@property (nonatomic)			NSInteger		propertyType;
@property (nonatomic, copy)		NSString		*description;
@property (nonatomic)			BOOL			enabled;
@property (nonatomic, retain)	NSMutableArray	*list;

#pragma mark Initialisation

- (id)initWithName:(NSString *)aName andFriendlyName:(NSString *)aFriendlyName
		andSection:(NSString *)aSection andPropertyType:(NSInteger)aType;
- (id)initWithName:(NSString *)aName andFriendlyName:(NSString *)aFriendlyName
		andSection:(NSString *)aSection andPropertyType:(NSInteger)aType andEnabled:(BOOL)isEnabled; 

@end
