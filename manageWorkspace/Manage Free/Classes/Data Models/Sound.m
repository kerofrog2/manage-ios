//
//  Sound.m
//  Manage
//
//  Created by Kerofrog on 7/5/13.
//
//

#import "Sound.h"

@implementation Sound

@synthesize textSound = _textSound;
@synthesize isSelected =_isSelected;

#pragma mark - Initialization

- (id)init {
    self = [super init];
    
    if (self) {
        // init value for object
        [self setDefaultValues];
    }
    
    return self;
}

#pragma mark - Private Methods

// init value for object
- (void)setDefaultValues {
    self.textSound = @"None";
    self.isSelected = NO;
}

+ (NSMutableArray *)createDataSource {
    
    NSMutableArray *soundArray = [[NSMutableArray alloc]init];
    
    NSMutableArray *sArray = [[NSMutableArray alloc]initWithObjects:@"None", @"Marimba", @"Alarm", @"Ascending", @"Bark", @"Bell Tower", @"Blues", @"Boing",  nil];
    
    for (NSInteger i=0; i<[sArray count]; i++) {
        Sound *sound = [[Sound alloc]init];
        sound.textSound = [sArray objectAtIndex:i];
        
        [soundArray addObject:sound];
        [sound release];
    }
    
    [sArray release];
    
    return soundArray;
}


@end
