//
//  RecurringListItem.h
//  Manage
//
//  Created by Cliff Viegas on 9/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class ListItem;

@interface RecurringListItem : NSObject {
	NSInteger		recurringListItemID;		// The unique identifier for this recurring list item
	NSInteger		frequency;					// The frequency of the recurring tasks, the minimum is one day
	NSString		*frequencyMeasurement;		// The weighting/measurement, eg: days, months, weeks, years
	NSString		*daysOfTheWeek;				// The days of the week to repeat
	BOOL			useCompletedDate;			// Whether to use the completed date (default) or 'other' (due date)
}

@property (nonatomic, assign)		NSInteger	recurringListItemID;
@property (nonatomic, assign)		NSInteger	frequency;
@property (nonatomic, copy)			NSString	*frequencyMeasurement;
@property (nonatomic, copy)			NSString	*daysOfTheWeek;
@property (nonatomic, assign)		BOOL		useCompletedDate;

#pragma mark Initialisation
- (id)initWithRecurringListItemID:(NSInteger)theRecurringListItemID;

#pragma mark Load Methods
- (void)loadUsingToodledoRepeatString:(NSString *)repeatString repeatFrom:(NSInteger)repeatFrom;

#pragma mark Update Methods

#pragma mark Delete Methods
//- (void)deleteCurrentScribble;

#pragma mark Class Methods
- (NSDate *)getNewDueDateUsingExistingDueDate:(NSString *)existingDueDate;
- (NSDate *)getNewDateForXDOfEachMonthUsingCalendar:(NSCalendar *)calendar andStartDate:(NSDate *)startDate;
- (NSInteger)getDayDifferenceUsingCurrentDay:(NSInteger)currentDay andWeekdaysArray:(NSMutableArray *)weekdaysArray;
- (NSMutableArray *)getWeekdaysArrayCopy;
- (void)setSelectedWeekdaysForArray:(NSMutableArray *)weekdaysArray;
- (NSInteger)getArrayIndexOfDay:(NSString *)day forWeekdaysArray:(NSMutableArray *)weekdaysArray;
- (NSInteger)getWeekNumberFromWeekString:(NSString *)weekNumberString;
- (NSString *)getWeekOrdinalStringFromToodledoString:(NSString *)toodledoString;
- (NSString *)replaceShortenedToodledoWeekday:(NSString *)toodledoWeekday;
- (NSInteger)getWeekdayNumberFromWeekday:(NSString *)theWeekday;
- (NSInteger)getLastWeekdayOrdinalWithCalendar:(NSCalendar *)calendar weekday:(NSInteger)weekday month:(NSInteger)month year:(NSInteger)year;
- (NSString *)getToodledoRepeatString;

#pragma mark Database Methods
- (NSInteger)insertSelfIntoDatabase;
- (void)updateSelfInDatabase;
- (void)deleteSelfFromDatabase; 

@end
