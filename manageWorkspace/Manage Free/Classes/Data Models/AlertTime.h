//
//  AlertTime.h
//  Manage
//
//  Created by Cliff Viegas on 6/6/13.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListItem.h"

@interface AlertTime : NSObject

@property (nonatomic, retain)		NSString	*text;
@property (nonatomic, retain)       NSString    *timeAlert;
@property (nonatomic)               BOOL        isSelected;

#pragma mark Instance Method

/**
 Init data souce for alert
 **/

+ (NSMutableArray *)createDataSource;

/***
 Convert minute to text of alert field
 ***/

+ (NSString *) textOnAlertWithMinute:(NSString *) minute;

#pragma mark Helper Methods
+ (BOOL)removeFromNotificationQueue:(NSInteger) itemID;

#pragma mark - Add Notification To Queue -
// Will create a new notification to add to queue.  Need to pass alarm
+ (void)addNewNotificationToQueue:(ListItem *)item;

@end
