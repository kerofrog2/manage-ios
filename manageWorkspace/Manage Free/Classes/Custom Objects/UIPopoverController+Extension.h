//
//  UIPopoverController+Extension.h
//  Manage
//
//  Created by Thuong Nguyen on 7/19/13.
//
//

#import <UIKit/UIKit.h>

@interface UIPopoverController (Extension)

-(id)initWithContentViewControllerWithAutoDismiss:(UIViewController *)viewController ;

@end
