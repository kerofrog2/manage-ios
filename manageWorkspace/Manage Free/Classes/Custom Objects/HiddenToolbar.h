//
//  HiddenToolbar.h
//  Manage
//
//  Created by Cliff Viegas on 18/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//
//  A custom toolbar that will hide itself from the user and still display its button items

#import <UIKit/UIKit.h>


@interface HiddenToolbar : UIToolbar {

}

@end
