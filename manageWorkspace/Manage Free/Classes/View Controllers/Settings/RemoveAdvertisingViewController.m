//
//  RemoveAdvertisingViewController.m
//  Manage Free
//
//  Created by Cliffard Viegas on 26/07/12.
//  Copyright (c) 2012 kerofrog. All rights reserved.
//

#import "RemoveAdvertisingViewController.h"

#import <QuartzCore/QuartzCore.h>

// InApp Purchase Manager
#import "InAppPurchaseManager.h"

// Progress Hud
#import "MBProgressHUD.h"

// Method Helper
#import "MethodHelper.h"

// Store Kit (for sk product)
#import <StoreKit/StoreKit.h>

// Flurry API
//#import "FlurryAPI.h"

@implementation RemoveAdvertisingViewController

#pragma mark - Memory Management 

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kInAppPurchaseManagerProductsFetchedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kInAppPurchaseManagerTransactionSucceededNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kInAppPurchaseManagerTransactionFailedNotification object:nil];
    
    [super dealloc];
}

#pragma mark - Initialisation

// Constants
#define kPopoverSize                    CGSizeMake(320, 318)

const static CGRect kRemoveAdvertisingButtonFrame = {10, 318 - 50, 300, 40};
const static CGRect kRemoveAdvertisingLabelFrame = {10, 10, 300, 140};
const static CGRect kKerofrogTeamImageFrame = {150, 170, 136, 72}; // 547 * 290
const static CGRect kKianaImageFrame = {34, 170, 96, 72};

// 252

- (id)init {
    self = [super init];
    if (self) {
        // Custom initialization
        // Set the title
		self.title = @"Remove Advertising";
        
        // Create notification center observer
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inAppProductsFetched:) name:kInAppPurchaseManagerProductsFetchedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transactionSucceeded:) name:kInAppPurchaseManagerTransactionSucceededNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transactionFailed:) name:kInAppPurchaseManagerTransactionFailedNotification object:nil];
        
        
        // Need to set the popover controller size
		[self setContentSizeForViewInPopover:kPopoverSize];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Notification Responders

- (void)inAppProductsFetched:(NSNotification *)notification {
    // Hide the progress hud
    //[MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}

- (void)transactionSucceeded:(NSNotification *)notification {
    [MethodHelper showAlertViewWithTitle:@"Success" andMessage:@"Advertising has now been removed :).  Restart Manage to apply changes." andButtonTitle:@"Ok"];
    
    //[FlurryAPI logEvent:@"Removed Advertising"];
    
    // Hide the progress hud
    //[MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}

- (void)transactionFailed:(NSNotification *)notification {
    //SKPaymentTransaction *transaction = [[notification userInfo] objectForKey:@"transaction"];
    
    [MethodHelper showAlertViewWithTitle:@"Failure" andMessage:@"The purchase failed, please try again later or contact manage@kerofrog.com.au for support" andButtonTitle:@"Ok"];

    // Hide the progress hud
    //[MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}

#pragma mark - Load Methods

- (void)loadView {
    [super loadView];
    
    // Load the view
    
    // Set background color
    CGRect mainViewFrame = CGRectMake(0, 0, kPopoverSize.width, kPopoverSize.height);
    self.view = [[[UIView alloc] initWithFrame:mainViewFrame] autorelease];
    CAGradientLayer* gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    UIColor* startColor = [UIColor colorWithRed:226.0/255.0 green:229.0/255.0 blue:234.0/255.0 alpha:1.0];
    UIColor* endColor = [UIColor colorWithRed:208.0/255.0 green:210.0/255.0 blue:216.0/255.0 alpha:1.0];
    
    // Cast to (id) is necessary to get rid of a compiler warning
    gradient.colors = [NSArray arrayWithObjects:(id)startColor.CGColor, (id)endColor.CGColor, nil];
    // Inserting at index position 0 ensures that the gradient is drawn
    // in the background even if the view already has subviews or other
    // sublayers
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    //[self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Need a button, label, and two imageviews
    UIButton *removeAdvertisingButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [removeAdvertisingButton addTarget:self action:@selector(removeAdvertisingButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [removeAdvertisingButton setTitle:@"Remove Advertising" forState:UIControlStateNormal];
    [removeAdvertisingButton setFrame:kRemoveAdvertisingButtonFrame];
    
    [self.view addSubview:removeAdvertisingButton];
    
    // Restore transactions button
    UIBarButtonItem *restoreBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Restore" style:UIBarButtonItemStyleBordered target:self action:@selector(restoreBarButtonItemAction)] autorelease];
    [self.navigationItem setRightBarButtonItem:restoreBarButtonItem];
    
    UILabel *removeAdvertisingLabel = [[UILabel alloc] initWithFrame:kRemoveAdvertisingLabelFrame];
    [removeAdvertisingLabel setText:@"Get rid of annoying ads and at the same time help me feed these little hungry \"Monsters\" \ue022.\n\nOr you could just click on lots of ads, which would be nice too \ue056."];
    [removeAdvertisingLabel setNumberOfLines:0];
    [removeAdvertisingLabel sizeToFit];
    [removeAdvertisingLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:removeAdvertisingLabel];
    [removeAdvertisingLabel release];
    
    UIImageView *kerofrogTeamImageView = [[UIImageView alloc] initWithFrame:kKerofrogTeamImageFrame];
    [kerofrogTeamImageView setImage:[UIImage imageNamed:@"kerofrogteam.jpg"]];
    
    UIImageView *kianaImageView = [[UIImageView alloc] initWithFrame:kKianaImageFrame];
    [kianaImageView setImage:[UIImage imageNamed:@"HungryKiana.jpg"]];
    
    
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGFloat values[4] = {56 / 255.0, 85 / 255.0, 138 / 255.0, 1.0}; 
    CGColorRef blue = CGColorCreate(space, values); 
    
    kerofrogTeamImageView.layer.borderWidth = 2.0f;
    kerofrogTeamImageView.layer.borderColor = blue;
    
    kianaImageView.layer.borderWidth = 2.0f;
    kianaImageView.layer.borderColor = blue;
    
    CGColorRelease(blue);
    CGColorSpaceRelease(space);
    
    [self.view addSubview:kerofrogTeamImageView];
    [kerofrogTeamImageView release];
    
    
    [self.view addSubview:kianaImageView];
    [kianaImageView release];
}



#pragma mark - Action Methods

- (void)removeAdvertisingButtonAction {
#ifdef IS_FREE_BUILD
    // Present dialog to purchase app
    [[InAppPurchaseManager sharedPurchaseManager] purchaseUpgrade:kInAppPurchaseRemoveAdvertisingID];
    
    //MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //progressHUD.labelText = @"Fetching...";
#endif
}

- (void)restoreBarButtonItemAction {
    [[InAppPurchaseManager sharedPurchaseManager] checkPurchasedItems];
}

#pragma mark - UIViewControllerDelegate Methods

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
