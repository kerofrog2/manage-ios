    //
//  ReplaceToodledoTasksViewController.m
//  Manage
//
//  Created by Cliff Viegas on 27/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ReplaceToodledoTasksViewController.h"

// Data Models
#import "ApplicationSetting.h" 
#import "ArchiveList.h"
#import "TaskList.h"
#import "ToodledoFolder.h"
#import "TagCollection.h"
#import "ListItem.h"
#import "ToodledoTask.h"

// Toodledo Collections
#import "ToodledoTaskCollection.h"
#import "ToodledoFolderCollection.h"

// Data Collection
#import "TaskListCollection.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#import "ListConstants.h"
#define kPopoverSize				CGSizeMake(320, 318)
#define kLogTextViewFrame			CGRectMake(10, 0, 280, 250)
#define kActivityIndicatorFrame		CGRectMake(65, 278, 20, 21)
#define kSyncLabelFrame				CGRectMake(0, 258, 320, 60)
#define kTableViewFrame				CGRectMake(0, 0, 320, 318)
#define kTextViewTag				9228

@interface ReplaceToodledoTasksViewController()
// Load methods
- (void)loadTableView;
- (void)loadBarButtonItems;
- (void)loadTextView;
- (UITextView *)getTextView; 
- (void)loadLabel;
- (void)loadActivityView;
// Replace toodledo tasks sync methods
- (void)synchronize;
- (void)removeAllToodledoTasks;
- (void)removeAllToodledoFolders;
- (void)createNewToodledoFolders;
- (void)createNewToodledoTasks;
- (void)updateToodledoSubtasks;
// Class Method Helpers
- (void)updateLogTextView:(NSString *)newText nl:(BOOL)newLine;
- (void)sameThreadTextViewUpdate:(NSString *)newText;
@end


@implementation ReplaceToodledoTasksViewController

@synthesize delegate;
@synthesize toodledoKey;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[toodledoTaskCollection release];
	[toodledoFolderCollection release];
	[tagCollection release];
	[archiveList release];
	
	// UIKit objects
	[logTextView release];
	[syncLabel release];
	[syncActivityIndicatorView release];
	[myTableView release];
	
	[logContent release];
	
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_ReplaceToodledoTasksViewController;
#endif
		// Assign the passed master task list collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Set background colour
		self.view.backgroundColor = [UIColor lightTextColor];
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Set the title
		self.title = @"Toodledo Replace";
		
		// Init log content
		logContent = [[NSMutableString alloc] initWithString:@""];
		
		// Init the archive list of tasks
		archiveList = [[ArchiveList alloc] initWithArchivedListItems];
		
        // Need to set the parent list title for all these list items
        for (ListItem *archiveListItem in archiveList.listItems) {
            archiveListItem.parentListTitle = [archiveList getParentListTitleOfListItemWithParentListID:archiveListItem.listID 
                                                                          usingMasterTaskListCollection:refMasterTaskListCollection];
        }
        
		// Init the tag collection
		tagCollection = [[TagCollection alloc] initWithAllTags];
		
		// Load tableview
		[self loadTableView];
		
		// Load the bar button items
		[self loadBarButtonItems];
		
		// Load the logging text view
		[self loadTextView];
		
		// Load the label
		[self loadLabel];
		
		// Load the activity indicator
		[self loadActivityView];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setScrollEnabled:FALSE];
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

- (void)loadBarButtonItems {
	doneBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" 
														 style:UIBarButtonItemStyleDone 
														target:self 
														action:@selector(doneBarButtonItemAction)];
	[doneBarButtonItem setEnabled:FALSE];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:YES];
	
	cancelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" 
														   style:UIBarButtonItemStyleBordered 
														  target:self 
														  action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:YES];
	[cancelBarButtonItem release];
}


- (void)loadTextView {
	logTextView = [[UITextView alloc] initWithFrame:kLogTextViewFrame];
	[logTextView setEditable:FALSE];
	[logTextView setBackgroundColor:[UIColor clearColor]];
	[logTextView setTextColor:[UIColor darkTextColor]];
	//[self.view addSubview:logTextView];
}

- (UITextView *)getTextView {
	UITextView *textView = [[[UITextView alloc] initWithFrame:kLogTextViewFrame] autorelease];
	textView.tag = kTextViewTag;
	[textView setEditable:FALSE];
	[textView setBackgroundColor:[UIColor lightTextColor]];
	[textView setTextColor:[UIColor darkTextColor]];
	
	//[textView set
	
	return textView;
}

- (void)loadLabel {
	syncLabel = [[UILabel alloc] initWithFrame:kSyncLabelFrame];
	syncLabel.text = @"Synchronizing...";
	syncLabel.textAlignment = UITextAlignmentCenter;
	syncLabel.backgroundColor = [UIColor clearColor];
	
	[self.view addSubview:syncLabel];
}

- (void)loadActivityView {
	syncActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[syncActivityIndicatorView setFrame:kActivityIndicatorFrame];
	[syncActivityIndicatorView setHidesWhenStopped:YES];
	[self.view addSubview:syncActivityIndicatorView];
	
	[syncActivityIndicatorView startAnimating];
}

#pragma mark -
#pragma mark Replace Toodledo Task Sync Methods

- (void)synchronize {
	// Try doing this in seperate thread
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	NSString *key = [ApplicationSetting getSettingDataForName:@"ToodledoKey"];
	self.toodledoKey = key;
	
	[self updateLogTextView:@"GETTING COPY OF TOODLEDO FOLDERS" nl:NO];
	toodledoFolderCollection = [[ToodledoFolderCollection alloc] initUsingKey:self.toodledoKey];
	NSString *collectFoldersResponse = [toodledoFolderCollection collectFoldersFromServer];
	if ([collectFoldersResponse length] > 0) {
		[self updateLogTextView:collectFoldersResponse nl:NO];
	}
	
	[self updateLogTextView:@"\nGETING COPY OF TOODLEDO TASKS" nl:YES];
	// Now collect the tasks
	NSString *lastSyncDateStamp = @"0"; // We always want all tasks
	toodledoTaskCollection = [[ToodledoTaskCollection alloc] initUsingKey:self.toodledoKey];
	// The extra fields to return
	NSString *fields = @"tag,folder,parent,duedate,repeat,repeatfrom,priority,note,meta";
    
    NSInteger start = 0;
	NSString *collectTasksResponse = [toodledoTaskCollection getTasksFromServerWithModafter:lastSyncDateStamp 
																				  andFields:fields
                                                                                   andStart:start];
    
    while ([collectTasksResponse isEqualToString:@"Toodledo Server: Collecting tasks..."]) {
        [self updateLogTextView:collectTasksResponse nl:NO];
        start += 1000;
        collectTasksResponse = [toodledoTaskCollection getTasksFromServerWithModafter:lastSyncDateStamp 
                                                                            andFields:fields
                                                                             andStart:start];
    }

	if ([collectTasksResponse length] > 0) {
		[self updateLogTextView:collectTasksResponse nl:NO];
	}
	
	// Delete all folders and tasks first
	[self updateLogTextView:@"\nREMOVING TOODLEDO TASKS" nl:YES];
	[self removeAllToodledoTasks];
	[toodledoTaskCollection.tasks removeAllObjects];
	
	[self updateLogTextView:@"\nREMOVING TOODLEDO FOLDERS" nl:YES];
	[self removeAllToodledoFolders];
	[toodledoFolderCollection.folders removeAllObjects];
	
	// Next we need to create new tasks and folders
	
	// Then need to update any folders that need to be 'archived'??? yes, archive.
	
	// Create toodledo folders from our lists
	[self updateLogTextView:@"\nADDING TOODLEDO FOLDERS" nl:YES];
	[self createNewToodledoFolders];  // Archived folders are added too
	
	// Also get all our archived tasks
	[self updateLogTextView:@"\nADDING TOODLEDO TASKS" nl:YES];
	[self createNewToodledoTasks];
	
	// Update tasks to subtasks where needed
	[self updateLogTextView:@"\nUPDATING TOODLEDO SUBTASKS" nl:YES];
	// TODO: Keep eye on whether subtask data is returned for non pro account
	[self updateToodledoSubtasks];
	
	// Update local database last sync time
	NSString *dateTimeString = [MethodHelper stringFromDate:[NSDate date] 
												usingFormat:K_DATETIME_FORMAT];
	[ApplicationSetting updateSettingName:@"ToodledoLastSyncDateTime" 
								 withData:dateTimeString];
	
	[self updateLogTextView:@"\nSYNCHRONIZATION COMPLETE" nl:YES];
	
	// Sync complete
	[syncActivityIndicatorView stopAnimating];
	[syncLabel setText:@"Synchronization Complete"];
	[cancelBarButtonItem setEnabled:FALSE];
	[doneBarButtonItem setEnabled:TRUE];
	
	// Create the log file
	[MethodHelper createLogFile:logContent];

	[pool release];
	
}

- (void)removeAllToodledoTasks {
	NSError *error = nil;
	NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
	[mutableResponseString setString:@""];
	
	BOOL deleteTasksComplete = FALSE;
	
	NSInteger section = 0;
	while (deleteTasksComplete == FALSE) {
		deleteTasksComplete = [toodledoTaskCollection deleteAllTasksFromSection:section 
														  mutableResponseString:&mutableResponseString 
																		  error:&error
													  deletedListItemCollection:nil];
		// Output the response
		if ([mutableResponseString length] > 0) {
			[self updateLogTextView:mutableResponseString nl:NO];
		}
		section++;
	}
	
}

- (void)removeAllToodledoFolders {
	NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
	[mutableResponseString setString:@""];
	
	[toodledoFolderCollection deleteAllFoldersUsingResponseString:&mutableResponseString];
	
	if ([mutableResponseString length] > 0) {
		[self updateLogTextView:mutableResponseString nl:NO];
	}
	
}

- (void)createNewToodledoFolders {
	NSString *responseString = @"";
	NSError *error = nil;
	
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		if ([taskList.title isEqualToString:@"Toodledo Tasks"] || [taskList isNote]) {
			continue;
		}
		
		responseString = @"";
		error = nil;
		ToodledoFolder *newFolder = [[ToodledoFolder alloc] init];
		
		NSString *modTitle = [taskList.title stringByReplacingOccurrencesOfString:@"\\" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"\'" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"\"" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"<" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@">" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"&" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@";" withString:@""];
		
		if ([modTitle isEqualToString:taskList.title] == FALSE) {
			taskList.title = modTitle;
			
			[taskList updateDatabaseWithoutEditLog];
		}
		
		if ([newFolder addFolderWithName:taskList.title usingKey:self.toodledoKey 
						  responseString:&responseString error:&error] == TRUE) {
			if ([responseString length] > 0) {
				[self updateLogTextView:responseString nl:NO];
			}
			
			// Add the folder to toodledo folder collection
			[toodledoFolderCollection.folders addObject:newFolder];
			
			// Update the task list collection with toodledo folder id (if required)
			[refMasterTaskListCollection updateWithToodledoFolder:newFolder];
			
			[newFolder release];
		} else {
			[newFolder release];
			if (error) {
				[self updateLogTextView:[NSString stringWithFormat:@"Connection Error: %@",
										 responseString] nl:NO];
			} else {
				[self updateLogTextView:[NSString stringWithFormat:@"Toodledo Error: %@",
										 responseString] nl:NO];
			}
		}
	}
	
	for (TaskList *archivedList in archiveList.lists) {
		responseString = @"";
		error = nil;
		ToodledoFolder *newFolder = [[ToodledoFolder alloc] init];
		newFolder.archived = YES;
		
		// Adding this will produce ID to use
		if ([newFolder addFolderWithName:archivedList.title usingKey:self.toodledoKey 
						  responseString:&responseString error:&error] == TRUE) {
			if ([responseString length] > 0) {
				[self updateLogTextView:responseString nl:NO];
			}
			
			// Add the folder to toodledo folder collection
			[toodledoFolderCollection.folders addObject:newFolder];
			
			// Update the task list collection with toodledo folder id (if required)
			[refMasterTaskListCollection updateWithToodledoFolder:newFolder];
			
			// Update the folder so it is archived
			[newFolder release];
		} else {
			if (error) {
				[self updateLogTextView:[NSString stringWithFormat:@"Connection Error: %@",
										 responseString] nl:NO];
				[newFolder release];
				continue;
			} else {
				[self updateLogTextView:[NSString stringWithFormat:@"Toodledo Error: %@",
										 responseString] nl:NO];
				[newFolder release];
				continue;
			}
		}
		
		if ([responseString length] > 0) {
			[self updateLogTextView:responseString nl:NO];
		}
	}
}

- (void)createNewToodledoTasks {
	
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
		NSError *error = nil;
		
		BOOL addTasksComplete = FALSE;
		NSInteger section = 0;
		while (addTasksComplete == FALSE) {
			addTasksComplete = [toodledoTaskCollection addNewToodledoTasksFromTaskList:taskList 
												 usingTagCollection:tagCollection 
										andToodledoFolderCollection:toodledoFolderCollection 
															section:section 
											  mutableResponseString:&mutableResponseString 
															  error:&error
										   masterTaskListCollection:refMasterTaskListCollection
                                                        archiveList:nil];
			if ([mutableResponseString length] > 0) {
				[self updateLogTextView:mutableResponseString nl:NO];
			}
			
			section++;
		}
		
	}
    
    // Also add tasks from archives
    NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
    NSError *error = nil;
    
    BOOL addTasksComplete = FALSE;
    NSInteger section = 0;
    while (addTasksComplete == FALSE) {
        addTasksComplete = [toodledoTaskCollection addNewToodledoTasksFromTaskList:nil 
                                                                usingTagCollection:tagCollection 
                                                       andToodledoFolderCollection:toodledoFolderCollection 
                                                                           section:section 
                                                             mutableResponseString:&mutableResponseString 
                                                                             error:&error
                                                          masterTaskListCollection:refMasterTaskListCollection
                                                                       archiveList:archiveList];
        if ([mutableResponseString length] > 0) {
            [self updateLogTextView:mutableResponseString nl:NO];
        }
        
        section++;
    }
	
	// Now need to update local list items with toodledo task server ID number
	// TODO: TEST THIS... should be doing this automatically now
	/*for (TaskList *taskList in refMasterTaskListCollection.lists) {
		for (ListItem *listItem in taskList.listItems) {
			NSInteger taskIndex = [toodledoTaskCollection getIndexOfTaskWithListItemID:listItem.listItemID];
			
			if (taskIndex == -1) {
				[self updateLogTextView:@"Toodledo Local: Unable to find linked task" nl:NO];
				continue;
			}
			
			ToodledoTask *task = [toodledoTaskCollection.tasks objectAtIndex:taskIndex];
			listItem.toodledoTaskID = task.serverID;
			
			[listItem updateDatabase];
			[self updateLogTextView:[NSString stringWithFormat:@"Local: Task (%d) linked", listItem.listItemID] nl:NO];
		}
	}*/
	
	
	/*
	 
	 NSError *error = nil;
	 NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
	 [mutableResponseString setString:@""];
	 
	 BOOL deleteTasksComplete = FALSE;
	 
	 NSInteger section = 0;
	 while (deleteTasksComplete == FALSE) {
	 deleteTasksComplete = [toodledoTaskCollection deleteAllTasksFromSection:section 
	 mutableResponseString:&mutableResponseString 
	 error:&error];
	 // Output the response
	 if ([mutableResponseString length] > 0) {
	 [self updateLogTextView:mutableResponseString];
	 }
	 section++;
	 }
	 
	 */
}

- (void)updateToodledoSubtasks {
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		for (ListItem *listItem in taskList.listItems) {
			if ([listItem parentListItemID] == -1) {
				continue;
			}
			
			// Need to get the task and the parent task
			//NSInteger taskIndex = [toodledoTaskCollection getIndexOfTaskWithListItemID:listItem.listItemID];
			NSInteger taskIndex = [toodledoTaskCollection getIndexOfTaskWithTaskID:listItem.toodledoTaskID];
			if (taskIndex == -1) {
				[self updateLogTextView:@"Toodledo Local: Unable to find linked task" nl:NO];
				continue;
			}
			
			// Need to get the parent list item
			NSInteger parentListItemIndex = [taskList getIndexOfListItemWithID:listItem.parentListItemID];
			
			if (parentListItemIndex == -1) {
				[self updateLogTextView:@"Local Error: Unable to retrieve parent task index" nl:NO];
				continue;
			}
			
			ListItem *parentListItem = [taskList.listItems objectAtIndex:parentListItemIndex];
			
			//NSInteger parentTaskIndex = [toodledoTaskCollection getIndexOfTaskWithListItemID:listItem.parentListItemID];
			NSInteger parentTaskIndex = [toodledoTaskCollection getIndexOfTaskWithTaskID:parentListItem.toodledoTaskID];
			if (parentTaskIndex == -1) {
				[self updateLogTextView:@"Toodledo Local: Unable to find linked parent task" nl:NO];
				continue;
			}
			
			ToodledoTask *task = [toodledoTaskCollection.tasks objectAtIndex:taskIndex];
			ToodledoTask *parentTask = [toodledoTaskCollection.tasks objectAtIndex:parentTaskIndex];
			
			task.parentID = parentTask.serverID;
			task.needsUpdate = YES;
		}
	}
	
	// Update tasks with subtask detail
	NSError *error = nil;
	NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
	[mutableResponseString setString:@""];
	
    NSInteger section = 0;
	BOOL postUpdatesComplete = FALSE;
	while (postUpdatesComplete == FALSE) {
		postUpdatesComplete = [toodledoTaskCollection postUpdatesToSubTasksWithResponseString:&mutableResponseString section:section error:&error];
		
		// Output the response
		if ([mutableResponseString length] > 0) {
			[self updateLogTextView:mutableResponseString nl:NO];
		}
        section++;
	}
}

#pragma mark -
#pragma mark Class Method Helpers

- (void)updateLogTextView:(NSString *)newText nl:(BOOL)newLine {
	if (newLine == FALSE) {
		newText = [newText stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
	}
	
	[logContent appendFormat:@"%@\n", newText];

	[self performSelectorOnMainThread:@selector(sameThreadTextViewUpdate:) withObject:newText waitUntilDone:YES];
}

- (void)sameThreadTextViewUpdate:(NSString *)newText {
	[logTextView setText:[NSString stringWithFormat:@"%@%@\n",
						  [logTextView text],
						  newText]];
	
	[logTextView scrollRangeToVisible:NSMakeRange([logTextView.text length] - 7, 7)];
	
}

#pragma mark -
#pragma mark Button Action Methods

- (void)doneBarButtonItemAction {
    // Update task list collection in main view controller as well
    [self.delegate replaceToodledoDataComplete];
    
	[self.delegate closeSettingsPopover];
}

- (void)cancelBarButtonItemAction {
	[toodledoTaskCollection cancelConnection];
	[toodledoFolderCollection cancelConnection];
	
	[self.navigationController popViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark UITableView Delegates

- (void)viewDidAppear:(BOOL)animated {
	[NSThread detachNewThreadSelector:@selector(synchronize) toTarget:self withObject:nil];
    
	[super viewDidAppear:animated];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		[cell.contentView addSubview:logTextView];
	}
	
	// Load tableview cell
	return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 250;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return 1;
}


@end
