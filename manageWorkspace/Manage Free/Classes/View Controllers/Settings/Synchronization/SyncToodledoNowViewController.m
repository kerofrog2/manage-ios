    //
//  SyncToodledoNowViewController.m
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "SyncToodledoNowViewController.h"

// Data Models
#import "ApplicationSetting.h"
#import "ListItem.h"
#import "TaskList.h"
#import "ToodledoFolder.h"
#import "DeletedTaskList.h"
#import "ToodledoAccountInfo.h"
#import "ToodledoTask.h"
#import "RecurringListItem.h"
#import "Tag.h"

// Data Collections
#import "TaskListCollection.h"
#import "DeletedListItemCollection.h"
#import "DeletedTaskListCollection.h"
#import "ToodledoTaskCollection.h"
#import "ToodledoFolderCollection.h"
#import "ArchiveList.h"
#import "TagCollection.h"
#import "ListItemTagCollection.h"

// Business Layer (being naughty, but we need to call this for syncListItem method)
#import "BLListItem.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#import "ListConstants.h"
#define kPopoverSize				CGSizeMake(320, 318)
#define kLogTextViewFrame			CGRectMake(10, 0, 280, 250)
#define kActivityIndicatorFrame		CGRectMake(65, 278, 20, 21)
#define kSyncLabelFrame				CGRectMake(0, 258, 320, 60)
#define kTableViewFrame				CGRectMake(0, 0, 320, 318)
#define kTextViewTag				3944

@interface SyncToodledoNowViewController()
// Load methods
- (void)loadTableView;
- (void)loadBarButtonItems;
- (void)loadTextView;
- (UITextView *)getTextView; 
- (void)loadLabel;
- (void)loadActivityView;
// Sync methods
- (void)synchronize;
// List/Folder Syncing
- (BOOL)addNewFoldersToToodledo;
- (BOOL)deleteFoldersFromToodledo;
- (void)updateLocalFolders;
- (void)updateToodledoFolders;
// Task sync methods
- (BOOL)addNewListItemsToToodledo;
- (BOOL)deleteTasksFromToodledo;
- (void)updateLocalTasks;
- (NSInteger)createRecurringListItemFromRepeatString:(NSString *)repeatString andRepeatFrom:(NSInteger)repeatFrom;
- (void)createListItemTagsFromToodledoTaskTags;
- (void)syncListItem:(ListItem *)listItem withToodledoTask:(ToodledoTask *)task forceMove:(BOOL)forceMove;
- (void)deleteLocalTasks;
- (BOOL)updateToodledoTasks;
- (void)actionLocalTasksThatNeedToBeMoved;
// Class Method Helpers
- (void)updateLogTextView:(NSString *)newText nl:(BOOL)newLine;
- (void)sameThreadTextViewUpdate:(NSString *)newText;
- (void)cancelSyncBecauseOfErrors;
@end


@implementation SyncToodledoNowViewController

@synthesize delegate;
@synthesize toodledoKey;
@synthesize lastSyncDateTimeString;
@synthesize toodledoServerHasConflictPriority;
@synthesize toodledoFoldersHaveBeenCollected;
@synthesize localChangesHaveBeenMade;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[toodledoTaskCollection release];
	[toodledoFolderCollection release];
	[tagCollection release];
	[archiveList release];
	[alreadyUpdatedLocalTasks release];
	[toodledoSubtaskCollection release];
	[moveItemsLocalTasksArray release];
	[moveSubItemsLocalTasksArray release];
	
	// UIKit objects
	[logTextView release];
	[syncLabel release];
	[syncActivityIndicatorView release];
	[myTableView release];
	
	[logContent release];
	
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection 
		  andToodledoAccountInfo:(ToodledoAccountInfo *)theAccountInfo {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_SyncToodledoNowViewController;
#endif
		// Assign the passed master task list collection
		refMasterTaskListCollection = theTaskListCollection;
        
		// Assign the passed toodledo account info
		refToodledoAccountInfo = theAccountInfo;
		
		// Init the tag collection
		tagCollection = [[TagCollection alloc] initWithAllTags];
		
		// Init archived list
		archiveList = [[ArchiveList alloc] initWithArchivedListItems];
		
        // Need to set the parent list title for all these list items
        for (ListItem *archiveListItem in archiveList.listItems) {
            archiveListItem.parentListTitle = [archiveList getParentListTitleOfListItemWithParentListID:archiveListItem.listID 
                                                                          usingMasterTaskListCollection:refMasterTaskListCollection];
        }
        
		// Init already updated local tasks array
		alreadyUpdatedLocalTasks = [[NSMutableArray alloc] initWithCapacity:10];
		
		// Init the toodledo subtask collection
		toodledoSubtaskCollection = [[ToodledoTaskCollection alloc] init];
		
		// Init the two move items arrays
		moveItemsLocalTasksArray = [[NSMutableArray alloc] initWithCapacity:5];
		moveSubItemsLocalTasksArray = [[NSMutableArray alloc] initWithCapacity:5];
		
		// Init toodledo key
		NSString *key = [ApplicationSetting getSettingDataForName:@"ToodledoKey"];
		self.toodledoKey = key;
		
		// Init toodledo folders have been collected
		self.toodledoFoldersHaveBeenCollected = FALSE;
		
		// Init local changes have been made to false
		self.localChangesHaveBeenMade = FALSE;
		
		// Assign last sync date time string
		self.lastSyncDateTimeString = [ApplicationSetting getSettingDataForName:@"ToodledoLastSyncDateTime"];
		if ([self.lastSyncDateTimeString length] == 0) {
			[MethodHelper showAlertViewWithTitle:@"Warning" 
									  andMessage:@"A full sync (replace local or toodledo tasks) is required before doing a standard sync" 
								  andButtonTitle:@"Ok"];
			[self.navigationController popViewControllerAnimated:NO];
		}
		
		// Set background colour
		self.view.backgroundColor = [UIColor lightTextColor];
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Set the title
		self.title = @"Sync Now";
		
		// Init conflict priority 
		NSString *conflictPriority = [ApplicationSetting getSettingDataForName:@"ToodledoServerHasConflictPriority"];
		if ([conflictPriority isEqualToString:@"Local"]) {
			self.toodledoServerHasConflictPriority = NO;
		} else if ([conflictPriority isEqualToString:@"Toodledo"]) {
			self.toodledoServerHasConflictPriority = YES;
		}
		
		// Init data has changed to false
		dataHasChanged = FALSE;
		
		// Init log content
		logContent = [[NSMutableString alloc] initWithString:@""];
		
		// Load tableview
		[self loadTableView];
		
		// Load the bar button items
		[self loadBarButtonItems];
		
		// Load the logging text view
		[self loadTextView];
		
		// Load the label
		[self loadLabel];
		
		// Load the activity indicator
		[self loadActivityView];
		
		// Synchronize
		//[self synchronize];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setScrollEnabled:FALSE];
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

- (void)loadBarButtonItems {
	doneBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" 
														 style:UIBarButtonItemStyleDone 
														target:self 
														action:@selector(doneBarButtonItemAction)];
	[doneBarButtonItem setEnabled:FALSE];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:YES];
	
	cancelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" 
														   style:UIBarButtonItemStyleBordered 
														  target:self 
														  action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:YES];
	[cancelBarButtonItem release];
}


- (void)loadTextView {
	logTextView = [[UITextView alloc] initWithFrame:kLogTextViewFrame];
	[logTextView setEditable:FALSE];
	[logTextView setBackgroundColor:[UIColor clearColor]];
	[logTextView setTextColor:[UIColor darkTextColor]];
	//[self.view addSubview:logTextView];
}

- (UITextView *)getTextView {
	UITextView *textView = [[[UITextView alloc] initWithFrame:kLogTextViewFrame] autorelease];
	textView.tag = kTextViewTag;
	[textView setEditable:FALSE];
	[textView setBackgroundColor:[UIColor lightTextColor]];
	[textView setTextColor:[UIColor darkTextColor]];
	
	//[textView set
	
	return textView;
}

- (void)loadLabel {
	syncLabel = [[UILabel alloc] initWithFrame:kSyncLabelFrame];
	syncLabel.text = @"Synchronizing...";
	syncLabel.textAlignment = UITextAlignmentCenter;
	syncLabel.backgroundColor = [UIColor clearColor];
	
	[self.view addSubview:syncLabel];
}

- (void)loadActivityView {
	syncActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[syncActivityIndicatorView setFrame:kActivityIndicatorFrame];
	[syncActivityIndicatorView setHidesWhenStopped:YES];
	[self.view addSubview:syncActivityIndicatorView];
	
	[syncActivityIndicatorView startAnimating];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	// This should really close everything..
	[self.delegate closeSettingsPopover];
}

- (void)cancelBarButtonItemAction {
	[toodledoTaskCollection cancelConnection];
	[toodledoFolderCollection cancelConnection];
	
	[self.navigationController popViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark Sync Methods

- (void)synchronize {
	// Try doing this in seperate thread
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	// Init toodledo task collection and toodledo folder collection
	toodledoTaskCollection = [[ToodledoTaskCollection alloc] initUsingKey:self.toodledoKey];
	toodledoFolderCollection = [[ToodledoFolderCollection alloc] initUsingKey:self.toodledoKey];
	
	//NSString *lastToodledoEditTask = [MethodHelper getDateTimeStringFromTimeInterval:[refToodledoAccountInfo.lastEditTask doubleValue] 
	//																	 usingFormat:K_DATETIME_FORMAT];
	
	//NSString *lastToodledoEditFolder = [MethodHelper getDateTimeStringFromTimeInterval:[refToodledoAccountInfo.lastEditFolder doubleValue] 
	//																	 usingFormat:K_DATETIME_FORMAT];
	
	//NSString *lastToodledoDeleteTask = [MethodHelper getDateTimeStringFromTimeInterval:[refToodledoAccountInfo.lastDeleteTask doubleValue] 
	//																	   usingFormat:K_DATETIME_FORMAT];
	
	//NSString *lastLocalEditTask = [ApplicationSetting getSettingDataForName:@"LastEditTaskDateTime"];
	//NSString *lastLocalDeleteTask = [ApplicationSetting getSettingDataForName:@"LastDeleteTaskDateTime"];
	//NSString *lastLocalEditList = [ApplicationSetting getSettingDataForName:@"LastEditListDateTime"];
	
	// Show details to user
	[self updateLogTextView:@"COMMENCING SYNC" nl:NO];
	[self updateLogTextView:@"Local: Please make sure your local time is correct, otherwise syncing problems may occur\n" nl:NO];
	[self updateLogTextView:[NSString stringWithFormat:@"Local: Last Sync (%@)", self.lastSyncDateTimeString] nl:NO];
	
//	[self updateLogTextView:[NSString stringWithFormat:@"Toodledo: Last Task Edit (%@)", lastToodledoEditTask] nl:NO];
//	[self updateLogTextView:[NSString stringWithFormat:@"Toodledo: Last Task Delete (%@)", lastToodledoDeleteTask] nl:NO];
//	[self updateLogTextView:[NSString stringWithFormat:@"Toodledo: Last Folder Edit (%@)\n", lastToodledoEditFolder] nl:NO];
	
	
	// Do we have any new folders?
	[self updateLogTextView:@"\nNEW LOCAL LISTS?" nl:YES];
	if ([self addNewFoldersToToodledo] == FALSE) {
        [self cancelSyncBecauseOfErrors];
        [pool release];
        return;
    }

	// Do we need to delete any folders?
	[self updateLogTextView:@"\nDELETED LOCAL LISTS?" nl:YES];
	if ([self deleteFoldersFromToodledo] == FALSE) {
		[self updateLogTextView:@"Local: No deleted lists found" nl:NO];
	}
	
	[self updateLogTextView:@"\nUPDATED LOCAL LISTS?" nl:YES];
	[self updateLocalFolders];
	
	[self updateLogTextView:@"\nUPDATED TOODLEDO FOLDERS?" nl:YES];
	[self updateToodledoFolders];
	
	
	// Do we have new tasks? (Need to go back after to setup subtasks)
	[self updateLogTextView:@"\nNEW LOCAL TASKS?" nl:YES];
	if ([self addNewListItemsToToodledo] == FALSE) {
        [self cancelSyncBecauseOfErrors];
        [pool release];
        return;
    }
	
	// Have we deleted any local tasks?
	[self updateLogTextView:@"\nDELETED LOCAL TASKS?" nl:YES];
	if ([self deleteTasksFromToodledo] == FALSE) {
		[self updateLogTextView:@"Local: No deleted tasks found" nl:NO];
	}
	
	// Look for any updates on server (and locally)
	[self updateLogTextView:@"\nUPDATED TOODLEDO TASKS?" nl:YES];
	[self updateLocalTasks];
	
	// Look for any deleted toodledo tasks, delete them (if found) locally
	[self updateLogTextView:@"\nDELETED TOODLEDO TASKS?" nl:YES];
	[self deleteLocalTasks];
	
	[self updateLogTextView:@"\nUPDATED LOCAL TASKS?" nl:YES];
	if ([self updateToodledoTasks] == FALSE) {
        [self cancelSyncBecauseOfErrors];
        [pool release];
        return;
    }
	
	[self updateLogTextView:@"\nSUBTASKS?" nl:YES];
	// note: Get all deleted lists, but make sure no list with same name exists
	[self updateListItemsThatNeedToBeSubtasks];
	
	// Not doing this as we gonna ignore parent tasks (with children) or subtasks from toodledo with folder changes
	[self updateLogTextView:@"\nTASKS MOVED?" nl:YES];
	[self actionLocalTasksThatNeedToBeMoved];
	
	//if ([lastToodledoSync length] > 0) {
	//	NSDate *syncDateTime = [MethodHelper dateFromString:lastToodledoSync usingFormat:K_DATETIME_FORMAT];
	//	lastSyncDateStamp = [NSString stringWithFormat:@"%f", [syncDateTime timeIntervalSince1970]];
	//}
	
	// Do we have any new tasks since last sync?
	
	// TESTING
	// Update local database last sync time
	NSString *dateTimeString = [MethodHelper stringFromDate:[NSDate date] 
												usingFormat:K_DATETIME_FORMAT];
	[ApplicationSetting updateSettingName:@"ToodledoLastSyncDateTime" 
								 withData:dateTimeString];
	
	// If so then add them
	
	// TODO: Only refresh local tasks if any changes were made locally.. need variable for this
	
	if ([self localChangesHaveBeenMade] == YES) {
		[self updateLogTextView:@"\nREFRESHING LOCAL TASKS AND LISTS" nl:YES];
		[self.delegate syncToodledoNowComplete];
		[self updateLogTextView:@"Local: Refresh completed" nl:NO];
	}
	
	[self updateLogTextView:@"\nSYNCHRONIZATION COMPLETE" nl:YES];
	
	[syncLabel setText:@"Synchronization Complete"];
	[cancelBarButtonItem setEnabled:FALSE];
	[doneBarButtonItem setEnabled:TRUE];
	
	// Create the log file
	[MethodHelper createLogFile:logContent];
	
	[syncActivityIndicatorView stopAnimating];
	
	// Release the thread pool
	[pool release];
}

#pragma mark -
#pragma mark List/Folder Syncing

// Add new folders to toodledo
- (BOOL)addNewFoldersToToodledo {
	NSString *lastAddListDateTimeString = [ApplicationSetting getSettingDataForName:@"LastAddListDateTime"];   
	// Need to compare dates
	NSComparisonResult comparisonResult = [lastAddListDateTimeString compare:self.lastSyncDateTimeString];
	// Means our sync string is earlier than last ad, and last add was after last sync
	// But we want to check ascending, so we know to jump out if needed, so we looking
	// for last add date being before last sync date
	if (comparisonResult == NSOrderedAscending || [lastAddListDateTimeString length] == 0) {
		// Then no new lists, exit
		[self updateLogTextView:@"Local: No new lists found" nl:NO];
		return TRUE;
	}
	
	TaskListCollection *newListsCollection = [[TaskListCollection alloc] init];
	[newListsCollection loadWithTaskListsCreatedAfter:self.lastSyncDateTimeString 
								  usingTaskCollection:refMasterTaskListCollection];
	
	[archiveList updateTaskListCollection:newListsCollection withListsCreatedAfter:self.lastSyncDateTimeString];
	
	if ([newListsCollection.lists count] == 0) {
		// Then no new folders, exit
		[self updateLogTextView:@"Local: No new lists found" nl:NO];
		[newListsCollection release];
		return TRUE;
	}
	
	for (TaskList *taskList in newListsCollection.lists) {
		// Don't add folder if folder name is 'Toodledo Tasks'
		if ([taskList.title isEqualToString:@"Toodledo Tasks"] || [taskList isNote]) {
			continue;
		}
		
		NSString *responseString = @"";
		NSError *error = nil;
		
		NSString *modTitle = [taskList.title stringByReplacingOccurrencesOfString:@"\\" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"\'" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"\"" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"<" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@">" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"&" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@";" withString:@""];
		
		if ([modTitle isEqualToString:taskList.title] == FALSE) {
			taskList.title = modTitle;
			
			// Need to find the task list in ref master task list collection and update it
			NSInteger listIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:taskList.listID];
			
			if (listIndex == -1) {
				TaskList *masterTaskList = [refMasterTaskListCollection.lists objectAtIndex:listIndex];
				masterTaskList.title = taskList.title;
				[masterTaskList updateDatabaseWithoutEditLog];
			}
			
			
			self.localChangesHaveBeenMade = YES;
		}
		
		ToodledoFolder *folder = [[ToodledoFolder alloc] init];
		[folder addFolderWithName:taskList.title 
						 usingKey:self.toodledoKey 
				   responseString:&responseString 
							error:&error];
		
		if (folder.folderID == 0 || folder.folderID == -1) {
			[folder release];
			continue;
		}
		
		// Update the task list with the toodledoFolder folder ID
		NSInteger taskListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:taskList.listID];
		NSInteger archiveTaskListIndex = [archiveList getIndexOfTaskListWithID:taskList.listID];
		if (taskListIndex != -1) {
			TaskList *masterTaskList = [refMasterTaskListCollection.lists objectAtIndex:taskListIndex];
			masterTaskList.toodledoFolderID = folder.folderID;
			self.localChangesHaveBeenMade = TRUE;
			[masterTaskList updateDatabaseWithoutEditLog];
		} else if (archiveTaskListIndex != -1) {
			TaskList *archiveTaskList = [archiveList.lists objectAtIndex:archiveTaskListIndex];
			archiveTaskList.toodledoFolderID = folder.folderID;
			self.localChangesHaveBeenMade = TRUE;
			[archiveTaskList updateDatabaseWithoutEditLog];
		}
		
		// Release the toodledo folder
		[folder release];
		
		if ([responseString length] > 0) {
			[self updateLogTextView:responseString nl:NO];
            
            // Check for toodledo error
            NSString *substring = @"Toodledo Error";
            NSRange textRange = [[responseString lowercaseString] rangeOfString:[substring lowercaseString]];
            if (textRange.length > 0) {
                [newListsCollection release];
                return FALSE;
            }
		}
	}
	
	[newListsCollection release];
    
    return TRUE;
}

// Delete folders from toodledo  
// RETURN: Whether at least one folder was deleted or not
- (BOOL)deleteFoldersFromToodledo {
	NSString *lastDeleteListDateTimeString = [ApplicationSetting getSettingDataForName:@"LastDeleteListDateTime"];   
	// Need to compare dates
	NSComparisonResult comparisonResult = [lastDeleteListDateTimeString compare:self.lastSyncDateTimeString];
	// Means our sync string is earlier than last ad, and last add was after last sync
	// But we want to check ascending, so we know to jump out if needed, so we looking
	// for last add date being before last sync date
	if (comparisonResult == NSOrderedAscending || [lastDeleteListDateTimeString length] == 0) {
		// Then no new deleted lists, exit
		return FALSE;
	}
	
	BOOL didDeleteAtLeastOneFolder = FALSE;
	
	DeletedTaskListCollection *deletedListsCollection = [[DeletedTaskListCollection alloc] init];
	[deletedListsCollection loadWithListsDeletedAfter:self.lastSyncDateTimeString 
								checkForAlternateCopy:YES];
	if ([deletedListsCollection.deletedLists count] == 0) {
		// Then no, no folders to delete
		[deletedListsCollection release];
		return FALSE;
	}
	
	for (DeletedTaskList *deletedTaskList in deletedListsCollection.deletedLists) {
		if (deletedTaskList.toodledoFolderID == -1 || deletedTaskList.toodledoFolderID == 0) {
			continue;
		}
        
        if ([deletedTaskList isNote]) {
            continue;
        }
		
		ToodledoFolder *folder = [[ToodledoFolder alloc] init];
		NSString *responseString = [folder deleteFolderWithID:deletedTaskList.toodledoFolderID 
											  andOriginalName:deletedTaskList.title
						  usingKey:self.toodledoKey];
		[folder release];
		
		if ([responseString length] > 0) {
			didDeleteAtLeastOneFolder = TRUE;
			[self updateLogTextView:responseString nl:NO];
		}
	}
	
	[deletedListsCollection release];
	
	return didDeleteAtLeastOneFolder;
}

- (void)updateLocalFolders {
	// Check last edit folder timestamp to see if this is newer than last sync
	NSTimeInterval timeInterval = [refToodledoAccountInfo.lastEditFolder doubleValue];
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
	NSString *lastEditDateTimeString = [MethodHelper stringFromDate:date usingFormat:K_DATETIME_FORMAT];
	
	// Need to compare dates
	NSComparisonResult comparisonResult = [lastEditDateTimeString compare:self.lastSyncDateTimeString];
	
	// Means our sync string is earlier than last edit, and last edit was after last sync
	// But we want to check ascending, so we know to jump out if needed, so we looking
	// for last edit date being before last sync date
	if (comparisonResult == NSOrderedAscending) {
		[self updateLogTextView:@"Toodledo Server: No change to folders" nl:NO];
		return;
	}
	
	// Now get the complete folder collection
	NSString *collectFoldersResponse = [toodledoFolderCollection collectFoldersFromServer];

	self.toodledoFoldersHaveBeenCollected = YES;
	if ([collectFoldersResponse length] > 0) {
		[self updateLogTextView:collectFoldersResponse nl:NO];
	}
	
	// Does the server have a folder that we don't have?
	for (ToodledoFolder *folder in toodledoFolderCollection.folders) {
		if ([refMasterTaskListCollection doesListExistWithTitle:folder.name
													 orFolderID:folder.folderID] == FALSE) {
			
			// List does not exist with this folder name, create it
			TaskList *newTaskList = [[TaskList alloc] initNewList];
			newTaskList.toodledoFolderID = folder.folderID;
			newTaskList.title = folder.name;
			
			// Update the database -> need to update database to include folder id
			self.localChangesHaveBeenMade = YES;
			[newTaskList updateDatabaseWithoutEditLog];
			
			// Need to check where to add the new list
			if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
				[refMasterTaskListCollection.lists insertObject:newTaskList atIndex:0];
			} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
				[refMasterTaskListCollection.lists addObject:newTaskList];
				
				// Need to update the list order (as we have added to end)
				[refMasterTaskListCollection commitListOrderChanges];
				[refMasterTaskListCollection updateListOrdersInDatabase];
			}
			[self updateLogTextView:[NSString stringWithFormat:@"Local: New task list created (%@)", 
									 newTaskList.title] nl:NO];
			[newTaskList release];
		}
	}
	
	// Is the server missing a folder that we have? Delete our copy then.
	// Need to go through each taskList and mark for deletion.. or delete, if empty
	for (int i = [refMasterTaskListCollection.lists count] - 1; i >= 0; i--) {
		TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:i];
        if ([taskList isNote]) {
            continue;
        }
        
		if ([toodledoFolderCollection doesFolderExistWithName:taskList.title] == FALSE) {
			//NSInteger folderID = [toodledoFolderCollection getIDOfFolderWithName:taskList.title];
			
			if ([toodledoFolderCollection doesFolderExistWithID:taskList.toodledoFolderID]) {
				// Don't add as this is an edited folder
				continue;
			}
			
			// Means server is missing folder that we have, delete our copy
			if ([taskList.listItems count] == 0) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local: Task list (%@) deleted", 
										 taskList.title] nl:NO];
				self.localChangesHaveBeenMade = YES;
				[refMasterTaskListCollection deleteListAtIndex:i permanent:NO];
			} else {
				taskList.markedForDeletion = YES;
			}
		}
	}
	// Do the same for archive list lists
	for (int i = [archiveList.lists count] - 1; i >= 0; i--) {
		TaskList *taskList = [archiveList.lists objectAtIndex:i];
        if ([taskList isNote]) {
            continue;
        }
        
		if ([toodledoFolderCollection doesFolderExistWithName:taskList.title] == FALSE) {
			//NSInteger folderID = [toodledoFolderCollection getIDOfFolderWithName:taskList.title];
			
			if ([toodledoFolderCollection doesFolderExistWithID:taskList.toodledoFolderID]) {
				// Don't add as this is an edited folder
				continue;
			}
			
			// Means server is missing folder that we have, delete our copy
			if ([taskList.listItems count] == 0) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local: Task list (%@) deleted", 
										 taskList.title] nl:NO];
				self.localChangesHaveBeenMade = YES;
				[archiveList deleteListAtIndex:i permanent:NO];
			} else {
				taskList.markedForDeletion = YES;
			}
		}
	}
	
	
	// Does a folder/list exist in both places?.. and has different content
	// This is where we need to resolve conflicts, which one do we update?
	// We update the one that has changed after last sync date, if both have changed,
	// then WE have a conflict :P.. although we don't have dates, ack
	if (self.toodledoServerHasConflictPriority == TRUE) {
		// Update our copy in case of changes
		//for (TaskList *taskList in refMasterTaskListColle
		// Only way to do this is by folder ID
		for (ToodledoFolder *folder in toodledoFolderCollection.folders) {
			NSInteger taskListIndex = [refMasterTaskListCollection getIndexOfTaskListWithToodledoFolderID:folder.folderID];
			NSInteger archiveTaskListIndex = [archiveList getIndexOfTaskListWithToodledoFolderID:folder.folderID];
			
			TaskList *taskList = nil;
			if (taskListIndex != -1) {
				taskList = [refMasterTaskListCollection.lists objectAtIndex:taskListIndex];
			} else if (archiveTaskListIndex != -1) {
				taskList = [archiveList.lists objectAtIndex:archiveTaskListIndex];
			}
			
			if (taskList == nil) {
				continue;
			}
            
            if ([taskList isNote]) {
                continue;
            }
			
			// We have a tasklist that matches, check if name doesn't match and update our copy if true
			if ([taskList.title isEqualToString:folder.name] == FALSE) {
				NSString *oldTitle = [taskList.title copy];
				taskList.title = folder.name;
				self.localChangesHaveBeenMade = YES;
				[taskList updateDatabaseWithoutEditLog];
				[self updateLogTextView:[NSString stringWithFormat:@"Local: Task list (%@) updated with server title (%@)", 
										 oldTitle, taskList.title] nl:NO];
			}
		}
	}
	
}

- (void)updateToodledoFolders {
	// Get the last time a folder was edited
	NSString *lastEditDateTimeString = [ApplicationSetting getSettingDataForName:@"LastEditListDateTime"];   
	// Need to compare dates
	NSComparisonResult comparisonResult = [lastEditDateTimeString compare:self.lastSyncDateTimeString];
	
	// Toodledo comparison
	NSTimeInterval timeInterval = [refToodledoAccountInfo.lastEditFolder doubleValue];
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
	NSString *lastToodledoEditDateTimeString = [MethodHelper stringFromDate:date usingFormat:K_DATETIME_FORMAT];
	NSComparisonResult toodledoComparison = [lastToodledoEditDateTimeString compare:self.lastSyncDateTimeString];
	
	// Means our sync string is earlier than last edit, and last edit was after last sync
	// But we want to check ascending, so we know to jump out if needed, so we looking
	// for last edit date being before last sync date
	
	if (comparisonResult == NSOrderedAscending && toodledoComparison == NSOrderedAscending) {
		[self updateLogTextView:@"Local: No change to lists" nl:NO];
		return;
	}
	
	// Now get the complete folder collection
	if (self.toodledoFoldersHaveBeenCollected == NO) {
		// Now get the complete folder collection
		NSString *collectFoldersResponse = [toodledoFolderCollection collectFoldersFromServer];

		if ([collectFoldersResponse length] > 0) {
			self.toodledoFoldersHaveBeenCollected = YES;
			[self updateLogTextView:collectFoldersResponse nl:NO];
		}
	}
	
	// Otherwise there has been a change and we need to find what has changed
	// Only way to do this is by folder ID
	for (ToodledoFolder *folder in toodledoFolderCollection.folders) {
		
		NSInteger taskListIndex = [refMasterTaskListCollection getIndexOfTaskListWithToodledoFolderID:folder.folderID];
		NSInteger archiveTaskListIndex = [archiveList getIndexOfTaskListWithToodledoFolderID:folder.folderID];
		
		TaskList *taskList = nil;
		if (taskListIndex != -1) {
			taskList = [refMasterTaskListCollection.lists objectAtIndex:taskListIndex];
		} else if (archiveTaskListIndex != -1) {
			taskList = [archiveList.lists objectAtIndex:archiveTaskListIndex];
		}
		
		if (taskList == nil) {
			continue;
		}
		
		// We have a tasklist that matches, check if name doesn't match and update toodledo copy if true
		if ([taskList.title isEqualToString:folder.name] == FALSE) {
			// Update toodledo folder
			NSString *originalName = [folder.name copy];
			
			NSString *modTitle = [taskList.title stringByReplacingOccurrencesOfString:@"\\" withString:@""];
			modTitle = [modTitle stringByReplacingOccurrencesOfString:@"\'" withString:@""];
			modTitle = [modTitle stringByReplacingOccurrencesOfString:@"\"" withString:@""];
			modTitle = [modTitle stringByReplacingOccurrencesOfString:@"<" withString:@""];
			modTitle = [modTitle stringByReplacingOccurrencesOfString:@">" withString:@""];
			modTitle = [modTitle stringByReplacingOccurrencesOfString:@"&" withString:@""];
			modTitle = [modTitle stringByReplacingOccurrencesOfString:@";" withString:@""];
			
			if ([modTitle isEqualToString:taskList.title] == FALSE) {
				taskList.title = modTitle;
				
				[taskList updateDatabaseWithoutEditLog];
				
				self.localChangesHaveBeenMade = YES;
			}
			
			folder.name = taskList.title;

			NSString *responseString = [folder updateFolderUsingKey:self.toodledoKey
														 andOldName:originalName];
			[originalName release];

			if ([responseString length] > 0) {
				[self updateLogTextView:responseString nl:NO];
			}
		}
	}
	
	// Now need to look if there are any task lists that are not listed as a folder
	for (TaskList *taskList in refMasterTaskListCollection.lists) {
		if (taskList.toodledoFolderID != -1) {
			continue;
		}
		
		if ([taskList.title isEqualToString:@"Toodledo Tasks"] || [taskList isNote]) {
			continue;
		}
		
		// Found a task list that is not linked up, check if the name exists and is duplicate
		NSInteger folderIndex = [toodledoFolderCollection getIDOfFolderWithName:taskList.title];
		
		if (folderIndex != -1) {
			if ([refMasterTaskListCollection getCountOfTaskListWithTitle:taskList.title] > 1) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Issue: Duplicate list name found (%@), please use unique list names when syncing with Toodledo", taskList.title] 
									 nl:NO];
				continue;
			}
		} 

		// List is not a duplicate and does not exist on server, lets add it and update its tasks
		NSString *responseString = @"";
		NSError *error = nil;
		
		ToodledoFolder *folder = [[ToodledoFolder alloc] init];
		
		NSString *modTitle = [taskList.title stringByReplacingOccurrencesOfString:@"\\" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"\'" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"\"" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"<" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@">" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@"&" withString:@""];
		modTitle = [modTitle stringByReplacingOccurrencesOfString:@";" withString:@""];
		
		if ([modTitle isEqualToString:taskList.title] == FALSE) {
			taskList.title = modTitle;
			
			[taskList updateDatabaseWithoutEditLog];
			
			self.localChangesHaveBeenMade = YES;
		}
		
		
		
		[folder addFolderWithName:taskList.title 
						 usingKey:self.toodledoKey 
				   responseString:&responseString 
							error:&error];
		
		if (folder.folderID == 0 || folder.folderID == -1) {
			[folder release];
			continue;
		}
		
		// Update the task list with the toodledoFolder folder ID
		taskList.toodledoFolderID = folder.folderID;
		[taskList updateDatabaseWithoutEditLog];
		
		
		
		if ([responseString length] > 0) {
			[self updateLogTextView:responseString nl:NO];
		}
		
		// Also update its children??
		// Post updates to folder names for the tasks
		// Post updates to folder names for the tasks
		error = nil;
		NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
		[mutableResponseString setString:@""];
		
        NSInteger section = 0;
		BOOL postUpdatesComplete = FALSE;
		while (postUpdatesComplete == FALSE) {
			postUpdatesComplete = [toodledoTaskCollection postUpdatesToTaskFolderNamesFromTaskList:taskList 
																					 usingFolderID:taskList.toodledoFolderID 
																					responseString:&mutableResponseString 
                                                                                           section:section
																							 error:&error];
			
			// Output the response
			if ([mutableResponseString length] > 0) {
				[self updateLogTextView:mutableResponseString nl:NO];
			}
            section++;
		}
		//NSInteger folderIndex = [toodledoFolderCollection ge
		
		// Now we need to physically change the folder for any of our tasks in this list
		// Release the toodledo folder
		[mutableResponseString release];
		[folder release];
	}
}

#pragma mark -
#pragma mark Task Sync Methods

- (BOOL)addNewListItemsToToodledo {
	NSString *lastAddListItem = [ApplicationSetting getSettingDataForName:@"LastAddTaskDateTime"]; 
	
	// Need to compare dates
	NSComparisonResult comparisonResult = [lastAddListItem compare:self.lastSyncDateTimeString];
	
	// Means our sync string is earlier than last ad, and last add was after last sync
	// But we want to check ascending, so we know to jump out if needed, so we looking
	// for last add date being before last sync date
	if (comparisonResult == NSOrderedAscending || [lastAddListItem length] == 0) {
		// Then no new tasks found, exit
		[self updateLogTextView:@"Local: No new tasks found" nl:NO];
		return TRUE;
	}
	
	// Go through our refMasterTaskListCollection and return any later than last sync date
    // Note: We don't want to return tasks with a toodledo task ID
	TaskListCollection *newListItemsCollection = [[TaskListCollection alloc] init];
	[newListItemsCollection loadWithTasksCreatedAfter:self.lastSyncDateTimeString 
							  usingTaskCollection:refMasterTaskListCollection
                                         getNonSynced:YES];
	[archiveList updateTaskListCollection:newListItemsCollection withListItemsCreatedAfter:self.lastSyncDateTimeString getNonSynced:YES];
	
	// Check whether there are actually any new tasks in these lists
	BOOL hasListItems = FALSE;
	for (TaskList *taskList in newListItemsCollection.lists) {
		if ([taskList.listItems count] > 0) {
			hasListItems = TRUE;
			break;
		}
	}
	if (hasListItems == FALSE) {
		// No new list items (tasks) found, exit
		[self updateLogTextView:@"Local: No new tasks found" nl:NO];
		[newListItemsCollection release];
		return TRUE;
	}
	
	// Now get the complete folder collection
	if (self.toodledoFoldersHaveBeenCollected == NO) {
		// Now get the complete folder collection
		NSString *collectFoldersResponse = [toodledoFolderCollection collectFoldersFromServer];
		
		if ([collectFoldersResponse length] > 0) {
			self.toodledoFoldersHaveBeenCollected = YES;
			[self updateLogTextView:collectFoldersResponse nl:NO];
		}
	}
	
	for (TaskList *taskList in newListItemsCollection.lists) {
		if ([taskList.listItems count] == 0) {
			// Skip this task list
			continue;
		}

		NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
		NSError *error = nil;
		
		BOOL addTasksComplete = FALSE;
		NSInteger section = 0;
		while (addTasksComplete == FALSE) {
			addTasksComplete = [toodledoTaskCollection addNewToodledoTasksFromTaskList:taskList 
																	usingTagCollection:tagCollection 
														   andToodledoFolderCollection:toodledoFolderCollection 
																			   section:section 
																 mutableResponseString:&mutableResponseString 
																				 error:&error
															  masterTaskListCollection:refMasterTaskListCollection
                                                                           archiveList:nil];
			if ([mutableResponseString length] > 0) {
				[self updateLogTextView:mutableResponseString nl:NO];
                
                // Check for toodledo error
                NSString *substring = @"Toodledo Error";
                NSRange textRange = [[mutableResponseString lowercaseString] rangeOfString:[substring lowercaseString]];
                if (textRange.length > 0) {
                    [newListItemsCollection release];
                    return FALSE;
                }
			}
			
			section++;
		}
	}
	
	[self actionLocalSubtasksFromCollection:newListItemsCollection];
	
	// Add all these list items to already updated list items
	for (TaskList *taskList in newListItemsCollection.lists) {
		for (ListItem *listItem in taskList.listItems) {
			// Add this list item to already updated list items
			[alreadyUpdatedLocalTasks addObject:[NSString stringWithFormat:@"%d", listItem.listItemID]];
		}
	}
	
	[newListItemsCollection release];
	
    return TRUE;
}

// Delete tasks from toodledo
// RETURN: Whether tasks were deleted or not
- (BOOL)deleteTasksFromToodledo {
	// Compare dates
	NSString *lastDeleteTask = [ApplicationSetting getSettingDataForName:@"LastDeleteTaskDateTime"];
	NSComparisonResult comparisonResult = [lastDeleteTask compare:self.lastSyncDateTimeString];
	if (comparisonResult == NSOrderedAscending || [lastDeleteTask length] == 0) {
		// No deleted tasks found
		return FALSE;
	}
	
	// Get the deleted list item collection
	DeletedListItemCollection *deletedListItemCollection = [[DeletedListItemCollection alloc] init];
	[deletedListItemCollection loadWithListItemsDeletedAfter:self.lastSyncDateTimeString];
	if ([deletedListItemCollection.deletedListItems count] == 0) {
		// No deleted tasks found
		[deletedListItemCollection release];
		return FALSE;
	}
	
	NSError *error = nil;
	NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
	[mutableResponseString setString:@""];
	
	BOOL deleteTasksComplete = FALSE;
	
	NSInteger section = 0;
	while (deleteTasksComplete == FALSE) {
		deleteTasksComplete = [toodledoTaskCollection deleteAllTasksFromSection:section 
														  mutableResponseString:&mutableResponseString 
																		  error:&error 
													  deletedListItemCollection:deletedListItemCollection];
		// Output the response
		if ([mutableResponseString length] > 0) {
			[self updateLogTextView:mutableResponseString nl:NO];
		}
		section++;
	}
	
	[deletedListItemCollection release];
	
	return TRUE;
}

// Does checks on toodledo for any local updates required
- (void)updateLocalTasks {
	// Get the last time a task was edited and compare it 
	NSString *lastEditTaskDateTimeString = [ApplicationSetting getSettingDataForName:@"LastEditTaskDateTime"];
	NSComparisonResult comparisonResult = [lastEditTaskDateTimeString compare:self.lastSyncDateTimeString];

	// Toodledo comparison
	NSTimeInterval timeInterval = [refToodledoAccountInfo.lastEditTask doubleValue];
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
	NSString *lastToodledoEditDateTimeString = [MethodHelper stringFromDate:date usingFormat:K_DATETIME_FORMAT];
	NSComparisonResult toodledoComparison = [lastToodledoEditDateTimeString compare:self.lastSyncDateTimeString];
	
	
	if (comparisonResult == NSOrderedAscending && toodledoComparison == NSOrderedAscending) {
		[self updateLogTextView:@"Local: No change to tasks" nl:NO];
		return;
	}
	
	// Does the server have a task we don't have?  If so, add them
	// This would only occur with any edits after last sync
	if (toodledoComparison == NSOrderedDescending) {
		NSDate *lastSyncDate = [MethodHelper dateFromString:self.lastSyncDateTimeString usingFormat:K_DATETIME_FORMAT];
		NSTimeInterval lastSyncTimeInterval = [lastSyncDate timeIntervalSince1970];
		NSString *lastSyncString = [NSString stringWithFormat:@"%f", lastSyncTimeInterval];
		NSString *fields = @"tag,folder,parent,duedate,repeat,repeatfrom,priority,note,meta";
        
        NSInteger start = 0;
        NSString *collectTasksResponse = [toodledoTaskCollection getTasksFromServerWithModafter:lastSyncString 
                                                                                      andFields:fields
                                                                                       andStart:start];
        
        while ([collectTasksResponse isEqualToString:@"Toodledo Server: Collecting tasks..."]) {
            [self updateLogTextView:collectTasksResponse nl:NO];
            start += 1000;
            collectTasksResponse = [toodledoTaskCollection getTasksFromServerWithModafter:lastSyncString 
                                                                                andFields:fields
                                                                                 andStart:start];
        }
        
		if ([collectTasksResponse length] > 0) {
			[self updateLogTextView:collectTasksResponse nl:NO];
		}
		
		// Need to find whether we have each task
		for (ToodledoTask *task in toodledoTaskCollection.tasks) {
			
			if ([refMasterTaskListCollection doesListItemExistWithToodledoServerID:task.serverID]) {
				// Found the task in local lists, continue
				continue;
			}
			
			if ([archiveList doesListItemExistWithToodledoServerID:task.serverID]) {
				// Found the task in archives, continue
				continue;
			}
			
			// Need to make sure we have Toodledo Tasks list if the task folderID is 0
			if ([task folderID] == 0) {
				// Create toodledo tasks list if it doesnt exist
				NSInteger toodledoTasksListIndex = [refMasterTaskListCollection getIndexOfTaskListWithTitle:@"Toodledo Tasks"];
				if (toodledoTasksListIndex == -1) {
					// Create the list
					TaskList *newTaskList = [[TaskList alloc] initNewList];
					// -1 means there is no folder id connection, 0 means its just task with no folder
					newTaskList.toodledoFolderID = 0;
					newTaskList.title = @"Toodledo Tasks";
					
					// Update the database
					[newTaskList updateDatabase];
					
					// Need to check where we add the new list
					if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
						[refMasterTaskListCollection.lists insertObject:newTaskList atIndex:0];
					} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
						[refMasterTaskListCollection.lists addObject:newTaskList];
						
						// Need to update the list order (as we have added to end)
						[refMasterTaskListCollection commitListOrderChanges];
						[refMasterTaskListCollection updateListOrdersInDatabase];
					}
					[self updateLogTextView:[NSString stringWithFormat:@"Local: New task list created (%@)", 
											 newTaskList.title] nl:NO];
					
					[newTaskList release];
					
				}
			}
			
			// Need to get the task list that matches up to this toodledo task
			NSInteger listIndex = [refMasterTaskListCollection getIndexOfTaskListWithToodledoFolderID:task.folderID];
			
			if (listIndex == -1) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to find task list with toodledo folder (%qi)", 
										 task.folderID] nl:NO];
				continue;
			}
			
			// Get the tasklist
			TaskList *refTaskList = [refMasterTaskListCollection.lists objectAtIndex:listIndex];
			
			// Start adding tasks... hmm... empty tag list prevents a db call
			ListItem *newListItem = [[ListItem alloc] initEmptyListItemWithListID:refTaskList.listID 
																   andParentTitle:refTaskList.title
																		 DBInsert:NO
																	 emptyTagList:YES];
			newListItem.toodledoTaskID = task.serverID;
			newListItem.title = task.title;
			
			if ([task completed] != 0) {
				newListItem.completed = YES;
			}
			
			// Set up due date, convert using method helper
			if ([task dueDate] != 0) {
				NSDate *localDueDate = [NSDate dateWithTimeIntervalSince1970:[task dueDate]];
				NSDate *destinationDate = [MethodHelper convertDateToUTCTimezone:localDueDate forDestinationZone:@"Local"];
				newListItem.dueDate = [MethodHelper stringFromDate:destinationDate usingFormat:K_DATEONLY_FORMAT];
			}
			
			// Check whether it repeats, setup as needed
			if ([[task repeat] length] > 0 && [[task repeat] isEqualToString:@"With Parent"] == FALSE) {
				newListItem.recurringListItemID = [self createRecurringListItemFromRepeatString:[task repeat] andRepeatFrom:[task repeatFrom]];
				[self updateLogTextView:[NSString stringWithFormat:@"Local: New repeating action assigned (%d)", 
										 newListItem.recurringListItemID] nl:NO];
			}
			
			// Priority
			if ([task priority] == -1 || [task priority] == 0) {
				newListItem.priority = EnumHighlighterColorNone;
			} else if ([task priority] == 1 || [task priority] == 2) {
				newListItem.priority = EnumHighlighterColorYellow;
			} else {
				newListItem.priority = EnumHighlighterColorRed;
			}
			
			// Update notes
			newListItem.notes = task.note;
			
			// Add the list item to the task list
			[refTaskList.listItems addObject:newListItem];
			[newListItem insertSelfIntoDatabase];
			[self updateLogTextView:[NSString stringWithFormat:@"Local: New task created (%d)", 
									 newListItem.listItemID] nl:NO];
			
			// Add this list item id to updated list items array list
			[alreadyUpdatedLocalTasks addObject:[NSString stringWithFormat:@"%d", newListItem.listItemID]];
		
			[newListItem release];
			
			// Local tasks have changed, update variable
			self.localChangesHaveBeenMade = YES;
			
			
			
			// If this task is a subtask, then we need to update local..
			if ([task parentID] != 0) {
				[toodledoSubtaskCollection.tasks addObject:task];
			}
		}
	}
	
	// Does a task exist in both places? 
	// Conflict depends on last modified datetime
	for (ToodledoTask *task in toodledoTaskCollection.tasks) {
		ListItem *listItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:task.serverID];
		NSInteger archiveListItemIndex = [archiveList getIndexOfListItemWithToodledoServerID:task.serverID];
		
		if (listItem == nil && archiveListItemIndex == -1) {
			// Not found, continue
			continue;
		}
		
		// Check whether we have alread updated this list item
		BOOL found = FALSE;
		for (NSString *alreadyEditedID in alreadyUpdatedLocalTasks) {
			if (listItem.listItemID == [alreadyEditedID integerValue]) {
				found = TRUE;
			}
		}
		// Continue to next listItem if found is true
		if (found == TRUE) {
			continue;
		}
		
		// List item is in archives as it is nil, get it from archives
		if (listItem == nil) {
			listItem = [archiveList.listItems objectAtIndex:archiveListItemIndex];
		}
		
		// We have a list item that matches
		// Need a method to check what is different.. ack...
		// Only sync if task mod date is later
		
		NSTimeInterval modInterval = [task modified];
		NSDate *taskModDate = [NSDate dateWithTimeIntervalSince1970:modInterval];
		NSString *taskModString = [MethodHelper stringFromDate:taskModDate usingFormat:K_DATETIME_FORMAT];
		
		// Compare to last modified date on list item
		NSComparisonResult comparisonResult = [taskModString compare:listItem.modifiedDateTime];
		// Looking for descending, which means mod string is later than mod date
		if (comparisonResult == NSOrderedDescending || comparisonResult == NSOrderedSame) {
			[self syncListItem:listItem withToodledoTask:task forceMove:NO];
		}
		
		// Check to see if task is a subtask, add it to be actioned list
		if ([task parentID] != 0) {
			[toodledoSubtaskCollection.tasks addObject:task];
		}
		
	}
}

- (NSInteger)createRecurringListItemFromRepeatString:(NSString *)repeatString andRepeatFrom:(NSInteger)repeatFrom {
	// Create recurring list item
	RecurringListItem *recurringListItem = [[RecurringListItem alloc] init];
	
	// Need to decipher the string, some possibles are
	// "the last tue of each month", "the 2nd mon of each month"
	// "every sat, sun", "every mon"
	// "every 1 week", "every 1 year"
	NSArray *words = [repeatString componentsSeparatedByString:@" "];
	
	if ([[words objectAtIndex:0] isEqualToString:@"the"]) {
		NSString *ordinal = [words objectAtIndex:1];
		ordinal = [recurringListItem getWeekOrdinalStringFromToodledoString:ordinal];
		recurringListItem.frequencyMeasurement = ordinal;
		
		NSString *weekday = [words objectAtIndex:2];
		weekday = [recurringListItem replaceShortenedToodledoWeekday:weekday];
		recurringListItem.daysOfTheWeek = weekday;
		
	} else if ([[words objectAtIndex:0] isEqualToString:@"Every"]) {
		NSString *word = [words objectAtIndex:1];
		word = [word stringByReplacingOccurrencesOfString:@"," withString:@""];
		if ([word isEqualToString:@"Mon"] || [word isEqualToString:@"Tue"] || [word isEqualToString:@"Wed"]
			|| [word isEqualToString:@"Thu"] || [word isEqualToString:@"Fri"]
			|| [word isEqualToString:@"Sat"] || [word isEqualToString:@"Sun"]) {
			
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Every " withString:@""];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Mon" withString:@"Monday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Tue" withString:@"Tuesday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Wed" withString:@"Wednesday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Thu" withString:@"Thursday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Fri" withString:@"Friday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Sat" withString:@"Saturday"];
			repeatString = [repeatString stringByReplacingOccurrencesOfString:@"Sun" withString:@"Sunday"];
			
			[recurringListItem setDaysOfTheWeek:repeatString];
			
		} else {
			[recurringListItem setFrequency:[[words objectAtIndex:1] integerValue]];
			NSString *measurement = [words objectAtIndex:2];
			
			measurement = [measurement capitalizedString];
			
			if ([measurement isEqualToString:@"Day"]) {
				measurement = @"Days";
			} else if ([measurement isEqualToString:@"Week"]) {
				measurement = @"Weeks";
			} else if ([measurement isEqualToString:@"Month"]) {
				measurement = @"Months";
			} else if ([measurement isEqualToString:@"Year"]) {
				measurement = @"Years";
			}
			[recurringListItem setFrequencyMeasurement:measurement];
		}
	}
	
	if (repeatFrom == 0) {
		// From due date
		[recurringListItem setUseCompletedDate:FALSE];
	} else {
		// From completed date
		[recurringListItem setUseCompletedDate:TRUE];
	}
	
	NSInteger recurringListItemID = [recurringListItem insertSelfIntoDatabase];
	[recurringListItem release];
	
	return recurringListItemID;
}



- (void)createListItemTagsFromToodledoTaskTags {
	// Tag collection already exist, with all tags
	
	for (ToodledoTask *task in toodledoTaskCollection.tasks) {
		// Need to check that there are tags
		if ([[task tags] length] > 0) {
			// Need to get correct listItemID
			ListItem *listItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:[task serverID]];
			
			if (listItem == nil) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to locate local task with toodledo task ID (%qi)", 
										 [task serverID]] nl:NO];
				continue;
			}
			
			//	ListItem *listItem = [refMasterTaskListCollection
			
			NSArray *localArray = [task.tags componentsSeparatedByString:@","];
			for (NSString *localTag in localArray) {
				// Get rid of white spaces in local tag
				localTag = [localTag stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
				
				if ([localTag length] == 0) {
					continue;
				}
				
				// Now need to get the tag id
				NSInteger tagID = [tagCollection getTagIDOfTagWithName:localTag];
				
				if (tagID == -1) {
					// Need to create tag here
					tagID = [tagCollection getNewTagID];
					NSInteger newTagOrder = [tagCollection getNextTagOrderForParentTagID:-1];
					NSInteger tagDepth = 0;
					NSInteger parentTagID = -1;
					
					Tag *tag = [[Tag alloc] initWithTagID:tagID 
												  andName:localTag 
										   andParentTagID:parentTagID 
											  andTagOrder:newTagOrder 
											  andTagDepth:tagDepth
                                             andTagColour:NSLocalizedString(@"TAG_COLOR_BROWN", nil)];
					
					[tag insertSelfIntoDatabase];
					[tagCollection insertNewTag:tag];
					[self updateLogTextView:[NSString stringWithFormat:@"Local: New tag created (%d)", 
											 tag.tagID] nl:NO];
					[tag release];
				}
				
				// Setting new list item to false will automatically put tag in db too
				[listItem.listItemTagCollection addTagWithID:tagID 
											   andListItemID:listItem.listItemID 
											andIsNewListItem:FALSE];
				
				[self updateLogTextView:[NSString stringWithFormat:@"Local: Tag (%d) assigned to task (%d)", 
										 tagID, listItem.listItemID] nl:NO];
			}
		}
	}
}

- (void)syncListItem:(ListItem *)listItem withToodledoTask:(ToodledoTask *)task forceMove:(BOOL)forceMove {
	// Update list ID
	BOOL listItemChangesMade = FALSE;

	if ([task folderID] == 0) {
		// Need to create Toodledo Tasks folder if needed
		NSInteger toodledoTasksListIndex = [refMasterTaskListCollection getIndexOfTaskListWithTitle:@"Toodledo Tasks"];
		if (toodledoTasksListIndex == -1) {
			// Create the list
			TaskList *newTaskList = [[TaskList alloc] initNewList];
			// -1 means there is no folder id connection, 0 means its just task with no folder
			newTaskList.toodledoFolderID = 0;
			newTaskList.title = @"Toodledo Tasks";
			
			// Update the database
			[newTaskList updateDatabase];
			
			// Need to check where we add the new list
			if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
				[refMasterTaskListCollection.lists insertObject:newTaskList atIndex:0];
			} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
				[refMasterTaskListCollection.lists addObject:newTaskList];
				
				// Need to update the list order (as we have added to end)
				[refMasterTaskListCollection commitListOrderChanges];
				[refMasterTaskListCollection updateListOrdersInDatabase];
			}
			[self updateLogTextView:[NSString stringWithFormat:@"Local: New task list created (%@)", 
									 newTaskList.title] nl:NO];
			
			[newTaskList release];
		}
	}
	
	// Compare title
	if ([listItem.title isEqualToString:task.title] == FALSE) {
		if ([task.title isEqualToString:@"[no title]"] && [listItem.title isEqualToString:@""]) {
			// Do nothing
		} else {
			listItem.title = task.title;
			listItemChangesMade = YES;
		}

		
	}
	
	// Compare folder ID from task to make sure it matches
	// Toodledo folder ID should exist
	NSInteger foundListIndex = [refMasterTaskListCollection getIndexOfTaskListWithToodledoFolderID:task.folderID];
	if (foundListIndex != -1) {
		TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:foundListIndex];

		if (listItem.listID != taskList.listID) {
			
			if (forceMove == FALSE) {
				if ([task parentID] != 0) {
					// Is sub task, and is different from our task, update parent id
					[moveSubItemsLocalTasksArray addObject:task];
				} else {
					// Is not a sub task, need to examine whether we can move it later
					[moveItemsLocalTasksArray addObject:task];
				}

				// TODO: Check if this is a sub task.. if so, force update toodledo server back to normal list ID
				// Afterwards we will change if needed
				
				
			} else if (forceMove == TRUE) {
				listItem.listID = taskList.listID;
				listItem.parentListTitle = taskList.title;
				listItem.itemOrder = [taskList getNextListItemOrder];
				listItemChangesMade = TRUE;
			}
			//[self updateLogTextView:@"Toodledo Limitation: Unable to update folder changes to local tasks, please move tasks locally and synchronize again" nl:NO];
			
			// Hmm.. mark the task, we can update list item if it is by itself...
			
			// If we find a task that is a subtask and all the other required tasks are not also changed,
			// then revert..
			
			
			/*
			
			// Only make changes if this task has no parent task and has no children
			if ([task parentID] != 0 || [task children] > 0) {
				[self updateLogTextView:@"Unable to change list/folder for toodledo task that has children or is subtask" nl:NO];
			} else {
				// Means there is a difference, need to add this task to tasks that need to be moved...
				listItem.listID = taskList.listID;
				listItem.parentListTitle = taskList.title;
				
				listItemChangesMade = TRUE;
			}*/

			
			// Hmm.. we can match them up in two arrays
			//[moveItemsLocalTasksArray addObject:listItem];
			//[moveItemsToodledoTasksArray addObject:task];
			
			// Means there is a difference, need to add this task to tasks that need to be moved...
			//listItem.listID = taskList.listID;
			//listItem.parentListTitle = taskList.title;
			
			// Parent list has changed
			
			// Need to check if there is any subtasks if this is a parent local task
			// If toodledo has changed the list id of a subtask, then any parent tasks or other subtasks need to
			// change too... ack...
			
			// Problem is if both have changed on toodledo server... that's ok, we just compare other changes to
			// linked parent tasks or subtasks and ignore if they already changed
			
			// Dont move tasks until end...
			
			// listItemChangesMade = TRUE;
		}
	} else {
		[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Could not locate list with folder ID (%qi)", task.folderID] nl:NO];
		return;
	}
	
	// Compare completed status (task uses timestamp for completed)
	BOOL taskCompleted = TRUE;
	if ([task completed] == 0) {
		taskCompleted = FALSE;
	}
	if (taskCompleted != [listItem completed]) {
		listItem.completed = taskCompleted;
		listItemChangesMade = YES;
	}
	
	// Compare due date
	if ([task dueDate] != 0) {
		NSDate *localDueDate = [NSDate dateWithTimeIntervalSince1970:[task dueDate]];
		NSDate *destinationDate = [MethodHelper convertDateToUTCTimezone:localDueDate forDestinationZone:@"Local"];
		NSString *taskDueDate = [MethodHelper stringFromDate:destinationDate usingFormat:K_DATEONLY_FORMAT];

		if ([taskDueDate isEqualToString:listItem.dueDate] == FALSE) {
			listItem.dueDate = taskDueDate;
			listItemChangesMade = YES;
		}
	} else if ([task dueDate] == 0) {
		// Means that list item has a due date but task does not, update
		if ([listItem.dueDate length] != 0) {
			listItem.dueDate = @"";
			listItemChangesMade = YES;
		}
	}
	
	// Compare recurring list item
	if ([task.repeat length] == 0) {
		if (listItem.recurringListItemID != -1) {
			// Remove recurring listitem
			RecurringListItem *recurringListItem = [[RecurringListItem alloc] initWithRecurringListItemID:listItem.recurringListItemID];
			[recurringListItem deleteSelfFromDatabase];
			listItem.recurringListItemID = -1;
			listItemChangesMade = YES;
			[recurringListItem release];
		}
	} else if ([task.repeat length] > 0) {
		// Check whether we have no recurring list item ID
		if (listItem.recurringListItemID == -1) {
			// Create new recurring list item
			RecurringListItem *recurringListItem = [[RecurringListItem alloc] init];
			[recurringListItem loadUsingToodledoRepeatString:[task repeat] repeatFrom:[task repeatFrom]];
			NSInteger recurringListItemID = [recurringListItem insertSelfIntoDatabase];
			listItem.recurringListItemID = recurringListItemID;
			[recurringListItem release];
			listItemChangesMade = YES;
		} else {
			// Need to compare to our recurring list item
			RecurringListItem *toodledoRecur = [[RecurringListItem alloc] init];
			[toodledoRecur loadUsingToodledoRepeatString:[task repeat] repeatFrom:[task repeatFrom]];
			
			RecurringListItem *listItemRecur = [[RecurringListItem alloc] initWithRecurringListItemID:listItem.recurringListItemID];
			
			BOOL recurChangesMade = FALSE;
			
			if (toodledoRecur.frequency != listItemRecur.frequency) {
				listItemRecur.frequency = toodledoRecur.frequency;
				recurChangesMade = TRUE;
			}
			
			// If they dont match, update
			if ([toodledoRecur.frequencyMeasurement isEqualToString:listItemRecur.frequencyMeasurement] == FALSE) {
				listItemRecur.frequencyMeasurement = toodledoRecur.frequencyMeasurement;
				recurChangesMade = TRUE;
			}
			
			// If they dont matc, update
			if ([toodledoRecur.daysOfTheWeek isEqualToString:listItemRecur.daysOfTheWeek] == FALSE) {
				listItemRecur.daysOfTheWeek = toodledoRecur.daysOfTheWeek;
				recurChangesMade = TRUE;
			}
			
			if (toodledoRecur.useCompletedDate != listItemRecur.useCompletedDate) {
				listItemRecur.useCompletedDate = toodledoRecur.useCompletedDate;
				recurChangesMade = TRUE;
			}
			
			if (recurChangesMade == TRUE) {
				listItemChangesMade = YES;
				[listItemRecur updateSelfInDatabase];
			}
			[toodledoRecur release];
			[listItemRecur release];
		}
	}
	
	// Need to compare tags
	NSString *compareTagsReturn = [listItem.listItemTagCollection compareAndUpdateUsingToodledoTags:[task tags] 
																					  tagCollection:tagCollection 
																						 listItemID:listItem.listItemID];
	
	if ([compareTagsReturn length] > 0) {
		[self updateLogTextView:compareTagsReturn nl:NO];
		listItemChangesMade = YES;
	}
	
	// Need to compare priority
	if ([task priority] == -1 || [task priority] == 0) {
		if ([listItem priority] != EnumHighlighterColorNone) {
			listItem.priority = EnumHighlighterColorNone;
			listItemChangesMade = YES;
		}
	} else if ([task priority] == 1 || [task priority] == 2) {
		if ([listItem priority] != EnumHighlighterColorYellow) {
			listItem.priority = EnumHighlighterColorYellow;
			listItemChangesMade = YES;
		}
	} else {
		if ([listItem priority] != EnumHighlighterColorRed) {
			listItem.priority = EnumHighlighterColorRed;
			listItemChangesMade = YES;
		}
	}
	
	// Need to compare notes
	if ([[task note] isEqualToString:[listItem notes]] == FALSE) {
		listItem.notes = [task note];
		listItemChangesMade = YES;
	}
	
	// Need to compare whether task sent down is not a subtask and ours is
	if ([task parentID] == 0 && [listItem parentListItemID] != -1) {
		// How to change a subtask to something which isnt... need to create new order...
		listItem.parentListItemID = -1;
		// Need to change order
		listItem.itemOrder = [BLListItem getNextItemOrderForListID:listItem.listID];
		listItem.subItemOrder = 0;
		listItemChangesMade = YES;
	}
	
	// Update list item if need be
	if (listItemChangesMade == YES) {
		// Add this list item to updated list items array list
		[alreadyUpdatedLocalTasks addObject:[NSString stringWithFormat:@"%d", listItem.listItemID]];
		
		self.localChangesHaveBeenMade = YES;
		[listItem updateDatabase];
		[self updateLogTextView:[NSString stringWithFormat:@"Local: Task (%d) synced with Toodledo Task (%qi)",
								 listItem.listItemID, [task serverID]]
							 nl:NO];
	}
}

- (void)deleteLocalTasks {
	// Check last delete task timestamp to see if this is newer than last sync
	NSTimeInterval timeInterval = [refToodledoAccountInfo.lastDeleteTask doubleValue];
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
	NSString *lastDeleteString = [MethodHelper stringFromDate:date usingFormat:K_DATETIME_FORMAT];
	
	// Need to compare dates
	NSComparisonResult comparisonResult = [lastDeleteString compare:self.lastSyncDateTimeString];
	
	// Means our sync string is earlier than last edit, and last edit was after last sync
	// But we want to check ascending, so we know to jump out if needed, so we looking
	// for last edit date being before last sync date
	if (comparisonResult == NSOrderedAscending) {
		[self updateLogTextView:@"Toodledo Server: No deleted tasks found" nl:NO];
		return;
	}
	
	// Clear any tasks in toodledo task collection (no), create new collection
	ToodledoTaskCollection *deletedToodledoTaskCollection = [[ToodledoTaskCollection alloc] initUsingKey:self.toodledoKey];
	
	
	//[toodledoTaskCollection.tasks removeAllObjects];
	
	// Get deleted tasks after last sync date
	NSDate *lastSyncDate = [MethodHelper dateFromString:self.lastSyncDateTimeString usingFormat:K_DATETIME_FORMAT];
	NSTimeInterval lastSyncTimeInterval = [lastSyncDate timeIntervalSince1970];
	NSString *lastSyncString = [NSString stringWithFormat:@"%f", lastSyncTimeInterval];

	NSString *collectTasksResponse = [deletedToodledoTaskCollection getDeletedTasksFromServerWithModafter:lastSyncString];
	if ([collectTasksResponse length] > 0) {
		[self updateLogTextView:collectTasksResponse nl:NO];
	}
	
	// Now we go through these deleted tasks
	for (ToodledoTask *task in deletedToodledoTaskCollection.tasks) {
		ListItem *listItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:task.serverID];
		NSInteger archiveListItemIndex = [archiveList getIndexOfListItemWithToodledoServerID:task.serverID];
		
		BOOL isArchivedListItem = FALSE;
		
		if (listItem == nil && archiveListItemIndex == -1) {
			// Not found, continue
			continue;
		}
		
		// List item is in archives as it is nil, get it from archives
		if (listItem == nil) {
			listItem = [archiveList.listItems objectAtIndex:archiveListItemIndex];
			isArchivedListItem = TRUE;
		}
		
		// Now need to delete this list item
		if (isArchivedListItem == FALSE) {
			// Can't delete task with subtasks, but, toodledo doesn't allow this either, so should be ok
			
			// Need to get the right task list
			NSInteger taskListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:listItem.listID];
			
			if (taskListIndex == -1) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to find task list for task (%d)", listItem.listItemID] 
									 nl:NO];
				continue;
			}
			
			TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:taskListIndex];
			
			NSInteger listItemIndex = [taskList getIndexOfListItemWithID:listItem.listItemID];
			
			if (listItemIndex == -1) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to find task (%d) in list (%d)", 
										 listItem.listItemID, taskList.listID] 
									 nl:NO];
				continue;
			}
			
			[self updateLogTextView:[NSString stringWithFormat:@"Local: Deleted task (%d)", listItem.listItemID] nl:NO];
			
			// Now delete the local task
			[taskList deleteListItemAtIndex:listItemIndex permanent:YES];
			
			self.localChangesHaveBeenMade = YES;
			
		} else {
			[self updateLogTextView:[NSString stringWithFormat:@"Local: Deleted task (%d) from archives", listItem.listItemID] nl:NO];
			
			// Delete from archives
			[archiveList deleteListItemAtIndex:archiveListItemIndex permanent:YES];
			
			self.localChangesHaveBeenMade = YES;
		}
	}
	
	[deletedToodledoTaskCollection release];
					
}

- (BOOL)updateToodledoTasks {
	// Need to get a list collection of tasks edited after last sync
	// We should have already edited some tasks... need to make sure these wont be called.
	// Might need to grab a copy of toodledo edited tasks AND local edited tasks
	
	// Getting all tasks modified after last sync
	TaskListCollection *editedListItemCollection = [[TaskListCollection alloc] init];
	[editedListItemCollection loadWithTasksModifiedAfter:self.lastSyncDateTimeString usingTaskCollection:refMasterTaskListCollection];
	[archiveList updateTaskListCollection:editedListItemCollection withListItemsModifiedAfter:self.lastSyncDateTimeString];
    
	if ([editedListItemCollection.lists count] == 0) {
		[self updateLogTextView:@"Local: No changes to tasks" nl:NO];
		[editedListItemCollection release];
		return TRUE;
	}
	
	// Now get the complete folder collection
	if (self.toodledoFoldersHaveBeenCollected == NO) {
		// Now get the complete folder collection
		NSString *collectFoldersResponse = [toodledoFolderCollection collectFoldersFromServer];
		
		if ([collectFoldersResponse length] > 0) {
			self.toodledoFoldersHaveBeenCollected = YES;
			[self updateLogTextView:collectFoldersResponse nl:NO];
		}
	}
	
	// If toodledo tasks had been edited after, we would have grabbed them already.. and if our modification
	// date had been later, then we still need to update.  BUT.. if toodledo task modification had been later,
	// we would have already updated.  As such, we need a flag to say we have already updated our local task
	// and thus do not need to update toodledo.  Can use a local array for this.
	
	// Clear toodledo tasks collection first
	//[toodledoTaskCollection.tasks removeAllObjects];
	ToodledoTaskCollection *updateToodledoTaskCollection = [[ToodledoTaskCollection alloc] initUsingKey:self.toodledoKey];
	
	// Tasks to update
	NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
	NSError *error = nil;
	
	BOOL updateTasksComplete = FALSE;
	NSInteger section = 0;
	while (updateTasksComplete == FALSE) {
		// Update the toodledo tasks
		updateTasksComplete = [updateToodledoTaskCollection postUpdatesToTasksFromTaskListCollection:editedListItemCollection 
													  usingTagCollection:tagCollection 
											 andToodledoFolderCollection:toodledoFolderCollection 
												  andAlreadyUpdatedTasks:alreadyUpdatedLocalTasks 
																 section:section 
												   mutableResponseString:&mutableResponseString 
																   error:&error];
		if ([mutableResponseString length] > 0) {
			[self updateLogTextView:mutableResponseString nl:NO];
            
            // Check for toodledo error
            NSString *substring = @"Toodledo Error";
            NSRange textRange = [[mutableResponseString lowercaseString] rangeOfString:[substring lowercaseString]];
            if (textRange.length > 0) {
                [updateToodledoTaskCollection release];
                [editedListItemCollection release];
                return FALSE;
            }
		}
		section++;
	}
	
	// We have a list of toodledotask collection, go through each, update as needed for parent and send
	/*for (ToodledoTask *task in toodledoTaskCollection.tasks) {
		ListItem *listItem = [editedListItemCollection getListItemReferenceFromListItemWithToodledoTaskID:task.serverID];
		
		if (listItem == nil) {
			continue;
		}
		
		if (listItem.parentListItemID == -1 && [task parentID] != 0) {
			// Set task parent id to 0
			task.parentID = 0;
			task.needsUpdate = YES;
			continue;
		}
		
		if (listItem.parentListItemID != -1 && [task parentID] == 0) {
			// Set task parent, but need to get correct parent ID
			
		}
	}*/
	
	// Update any subtasks in the edited list item collection
	[self actionLocalSubtasksFromCollection:editedListItemCollection];
	
	[updateToodledoTaskCollection release];
	[editedListItemCollection release];
    
    return TRUE;
}

// A limitation, we are not using this method for initial release as toodledo does not respect folders
// being changed when subtasks are around (unless we have changed parent task and there are no children)
- (void)actionLocalTasksThatNeedToBeMoved {
	if ([moveSubItemsLocalTasksArray count] == 0) {
		[self updateLogTextView:@"Local: No toodledo subtasks to be re-adjusted" nl:NO];
	} else {
		TaskList *moveSubTasksList = [[TaskList alloc] init];
		for (ToodledoTask *task in moveSubItemsLocalTasksArray) {
			ListItem *listItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:task.serverID];
			
			if (listItem == nil) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Warning: Unable to find local subtask with toodledo ref (%qi)", task.serverID] nl:NO];
			}
			
			// Need the folder id to use
			NSInteger listIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:listItem.listID];
			if (listIndex == -1) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to find local list with id (%d)", listItem.listID] nl:NO];
				continue;
			}
			
			TaskList *tempTaskList = [refMasterTaskListCollection.lists objectAtIndex:listIndex];
			if ([tempTaskList toodledoFolderID] == -1) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Task list (%d) does not have toodledo folder ref", [tempTaskList listID]] nl:NO];
			}
			
			// Add the list item to the task list for sub tasks to be updated on server
			[moveSubTasksList.listItems addObject:listItem];
		}
		
		// Post updates to folder names for the tasks
		NSError *error = nil;
		NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
		[mutableResponseString setString:@""];
		
        NSInteger section = 0;
		BOOL postUpdatesComplete = FALSE;
		while (postUpdatesComplete == FALSE) {
			postUpdatesComplete = [toodledoTaskCollection postUpdatesToTaskFolderNamesFromTaskList:moveSubTasksList 
																		   usingTaskListCollection:refMasterTaskListCollection 
																					responseString:&mutableResponseString 
                                                                                           section:section
																							 error:&error];
			
			// Output the response
			if ([mutableResponseString length] > 0) {
				[self updateLogTextView:mutableResponseString nl:NO];
			}
            section++;
		}
		
		[mutableResponseString release];
		[moveSubTasksList release];
	}
	
	// Check if there are any tasks
	if ([moveItemsLocalTasksArray count] == 0) {
		[self updateLogTextView:@"Local: No tasks to be moved" nl:NO];
		return;
	}
	
	for (ToodledoTask *task in moveItemsLocalTasksArray) {
		// First lets get the list item
		ListItem *listItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:task.serverID];
		
		// If list item is subtask... ?... don't change.., ignore..
		// If list item is parent task with subtasks, then move and update all
		
		if (listItem == nil) {
			[self updateLogTextView:[NSString stringWithFormat:@"Local Warning: Unable to find local task with toodledo ref (%qi)", task.serverID] nl:NO];
		}
		
		if ([listItem parentListItemID] != -1) {
			continue;
		}
		
		NSInteger taskListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:listItem.listID];
		TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:taskListIndex];
		NSInteger listItemIndex = [taskList getIndexOfListItemWithID:listItem.listItemID];
		
		// How to find out if list item is parent.. ???..... ah, number of children
		NSInteger numberOfSubtasks = [taskList getCountOfSubtasksForListItemAtIndex:listItemIndex];
		
		if (numberOfSubtasks == 0) {
			// Can move it, hooray.
			[self syncListItem:listItem withToodledoTask:task forceMove:YES];
		} else if (numberOfSubtasks > 0) {
			// Otherwise need to move all, including subtasks
			[self syncListItem:listItem withToodledoTask:task forceMove:YES];
			
			// Need to create 'Toodledo Tasks' list if it doesnt exist
			NSInteger toodledoTasksListIndex = [refMasterTaskListCollection getIndexOfTaskListWithTitle:@"Toodledo Tasks"];
			if (toodledoTasksListIndex == -1) {
				// Create the list
				TaskList *newTaskList = [[TaskList alloc] initNewList];
				// -1 means there is no folder id connection, 0 means its just task with no folder
				newTaskList.toodledoFolderID = 0;
				newTaskList.title = @"Toodledo Tasks";
				
				// Update the database
				[newTaskList updateDatabase];
				
				// Need to check where we add the new list
				if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"Beginning"]) {
					[refMasterTaskListCollection.lists insertObject:newTaskList atIndex:0];
				} else if ([refMasterTaskListCollection.theNewListOrder isEqualToString:@"End"]) {
					[refMasterTaskListCollection.lists addObject:newTaskList];
					
					// Need to update the list order (as we have added to end)
					[refMasterTaskListCollection commitListOrderChanges];
					[refMasterTaskListCollection updateListOrdersInDatabase];
				}
				[self updateLogTextView:[NSString stringWithFormat:@"Local: New task list created (%@)", 
										 newTaskList.title] nl:NO];
				
				[newTaskList release];
			}
			
			// Find the list index of what the toodledo task folder is pointing to
			NSInteger foundListIndex = [refMasterTaskListCollection getIndexOfTaskListWithToodledoFolderID:task.folderID];
			if (foundListIndex == -1) {
				[self updateLogTextView:[NSString stringWithFormat:@"Could not find task list with toodledo folder (%lld)", task.folderID] nl:NO];
				continue;
			}
			TaskList *tempTaskList = [refMasterTaskListCollection.lists objectAtIndex:foundListIndex];
			
			// Create a task list holder
			TaskList *moveTaskList = [[TaskList alloc] init];
			[moveTaskList.listItems addObject:listItem];
			
			// Now get all the subtasks and add them to sub move task list
			for (ListItem *subListItem in taskList.listItems) {
				if ([subListItem parentListItemID] == [listItem listItemID]) {
					subListItem.parentListTitle = tempTaskList.title;
					subListItem.listID = listItem.listID;
					subListItem.itemOrder = listItem.itemOrder;
					[subListItem updateDatabaseWithoutModifiedLog];
					self.localChangesHaveBeenMade = YES;
					[moveTaskList.listItems addObject:subListItem];
				}
			}
			
			// Now post updates for all these list items to toodledo server
			
			// Post updates to folder names for the tasks
			NSError *error = nil;
			NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
			[mutableResponseString setString:@""];
			
            NSInteger section = 0;
			BOOL postUpdatesComplete = FALSE;
			while (postUpdatesComplete == FALSE) {
				postUpdatesComplete = [toodledoTaskCollection postUpdatesToTaskFolderNamesFromTaskList:moveTaskList 
																						 usingFolderID:tempTaskList.toodledoFolderID 
																						responseString:&mutableResponseString 
                                                                                               section:section
																								 error:&error];
				
				// Output the response
				if ([mutableResponseString length] > 0) {
					[self updateLogTextView:mutableResponseString nl:NO];
				}
                section++;
			}

			[mutableResponseString release];
			[moveTaskList release];
		}
		
	}
	
	

	// Hmm.. mark the task, we can update list item if it is by itself...
	
	// If we find a task that is a subtask and all the other required tasks are not also changed,
	// then revert..
	
	
	
	/*for (int i = 0; i < [moveItemsLocalTasksArray count] - 1; i++) {
		// Need to see if this is a parent or child task
		ListItem *listItem = [moveItemsLocalTasksArray objectAtIndex:i];
		ToodledoTask *task = [moveItemsToodledoTasksArray objectAtIndex:i];
		
		
	}
	
	// Compare folder ID from task to make sure it matches
	// Toodledo folder ID should exist
	NSInteger foundListIndex = [refMasterTaskListCollection getIndexOfTaskListWithToodledoFolderID:task.folderID];
	if (foundListIndex != -1) {
		TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:foundListIndex];
		if (listItem.listID != taskList.listID) {
			// Hmm.. we can match them up in two arrays
			[moveItemsLocalTasksArray addObject:listItem];
			[moveItemsToodledoTasksArray addObject:task];
			
			// Means there is a difference, need to add this task to tasks that need to be moved...
			//listItem.listID = taskList.listID;
			//listItem.parentListTitle = taskList.title;
			
			// Parent list has changed
			
			// Need to check if there is any subtasks if this is a parent local task
			// If toodledo has changed the list id of a subtask, then any parent tasks or other subtasks need to
			// change too... ack...
			
			// Problem is if both have changed on toodledo server... that's ok, we just compare other changes to
			// linked parent tasks or subtasks and ignore if they already changed
			
			// Dont move tasks until end...
			
			// listItemChangesMade = TRUE;
		}
	} else {
		[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Could not locate list with folder ID (%qi)", task.folderID] nl:NO];
		return;
	}*/
}

#pragma mark -
#pragma mark Subtask Methods

- (void)actionLocalSubtasksFromCollection:(TaskListCollection *)taskListCollection {
	// Add list item ID's for local tasks that are subtasks to subtasks that need to be updated array
	ToodledoTaskCollection *tempToodledoCollection = [[ToodledoTaskCollection alloc] init];
	tempToodledoCollection.toodledoKey = self.toodledoKey;
	for (TaskList *taskList in taskListCollection.lists) {
		for (ListItem *listItem in taskList.listItems) {
			if ([listItem parentListItemID] == -1) {
				continue;
			}
			
			// Ok, we now have the parent list item, it should have a toodledo id
			if ([listItem toodledoTaskID] == -1) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Task (%d) does not have toodledo reference", listItem.listItemID] nl:NO];
				continue;
			}
			
			BOOL found = FALSE;
			for (NSString *alreadyEditedID in alreadyUpdatedLocalTasks) {
				if (listItem.listItemID == [alreadyEditedID integerValue]) {
					found = TRUE;
				}
			}
			// Continue to next listItem if found is true
			if (found == TRUE) {
				continue;
			}
			
			
			// This is a new subtask, if we had to add other new subtasks they would already be on toodledo server, so can do this
			ListItem *parentListItem = nil;
			
			// Get the parent toodledo ID
			NSInteger taskListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:listItem.listID];
			NSInteger listItemIndex = -1;
			if (taskListIndex != -1) {
				TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:taskListIndex];
				listItemIndex = [taskList getIndexOfListItemWithID:listItem.parentListItemID];
				if (listItemIndex != -1) {
					parentListItem = [taskList.listItems objectAtIndex:listItemIndex];
				}
			}
			NSInteger archiveListItemIndex = -1;
			if (listItemIndex == -1) {
				archiveListItemIndex = [archiveList getIndexOfListItemWithID:listItem.parentListItemID];
				if (archiveListItemIndex != -1) {
					parentListItem = [archiveList.listItems objectAtIndex:archiveListItemIndex];
				}
			}
			
			// Jump out if parent list item is nil
			if (parentListItem == nil) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to find parent task (%d)", listItem.parentListItemID] nl:NO];
				continue;
			}
			
			// Ok, we now have the parent list item, it should have a toodledo id
			if ([parentListItem toodledoTaskID] == -1) {
				[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Parent task (%d) does not have toodledo reference", listItem.parentListItemID] nl:NO];
				continue;
			}
			
			// With the toodledo ID, we can update the correct toodledo task and upate
			ToodledoTask *task = [[ToodledoTask alloc] init];
			task.serverID = listItem.toodledoTaskID;
			task.parentID = parentListItem.toodledoTaskID;
			task.needsUpdate = YES;
			[tempToodledoCollection.tasks addObject:task];
			[task release];
			
		}
	}
	// Update tasks with subtask detail
	NSError *error = nil;
	NSMutableString *mutableResponseString = [[NSMutableString alloc] init];
	[mutableResponseString setString:@""];
	
    NSInteger section = 0;
	BOOL postUpdatesComplete = FALSE;
	while (postUpdatesComplete == FALSE) {
		postUpdatesComplete = [tempToodledoCollection postUpdatesToSubTasksWithResponseString:&mutableResponseString section:section error:&error];
		
		// Output the response
		if ([mutableResponseString length] > 0) {
			[self updateLogTextView:mutableResponseString nl:NO];
		}
        section++;
	}
	[tempToodledoCollection release];
}

- (void)updateListItemsThatNeedToBeSubtasks {
	for (ToodledoTask *task in toodledoSubtaskCollection.tasks) {
		if ([task parentID] != 0) {
			// Means it is a subtask
			
			// Get the parent list item
			
			// Now point to the correct listItem
			ListItem *listItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:[task serverID]];
			
			if (listItem == nil) {
				[self updateLogTextView:@"Local Error: Unable to find correct local task" nl:NO];
				continue;
			}
			
			ListItem *parentListItem = [refMasterTaskListCollection getListItemReferenceFromListItemWithToodledoTaskID:[task parentID]];
			
			if (parentListItem == nil) {
				[self updateLogTextView:@"Local Error: Unable to find correct parent local task" nl:NO];
				continue;
			}
			
			// Sub task item order has to match parent item order
			listItem.itemOrder = parentListItem.itemOrder;
			
			NSInteger subItemOrder = [parentListItem getNewSubItemOrder];
			
			listItem.parentListItemID = parentListItem.listItemID;
			listItem.subItemOrder = subItemOrder;
			listItem.listID = parentListItem.listID;
			
			// Make sure that our list item has same parent list
			/*if ([listItem listID] != [parentListItem listID]) {
				// Need to update toodledo tasks too.. but why!?!?
				
				NSInteger parentIndex = [toodledoSubtaskCollection getIndexOfTaskWithTaskID:parentListItem.toodledoTaskID]; 
				if (parentIndex == -1) {
					[self updateLogTextView:[NSString stringWithFormat:@"Local Error: Unable to find index of local Toodledo parent task (%qi)", 
											 parentListItem.toodledoTaskID] nl:NO];
					continue;
				}
				ToodledoTask *parentTask = [toodledoSubtaskCollection.tasks objectAtIndex:parentIndex];
				
				// Update folder ID of the task
				task.folderID = parentTask.folderID;
				task.needsUpdate = TRUE; 
				
				// TODO: Need to update the folder of the 'task' not the parent task, but get both, task and parent task
				listItem.listID = parentListItem.listID;
				
			}*/
			
			[listItem updateDatabase];
			self.localChangesHaveBeenMade = YES;
			
			[self updateLogTextView:[NSString stringWithFormat:@"Local: Task (%d) converted into subtask", 
									 listItem.listItemID] nl:NO];
		}
		
	}
	
}


#pragma mark -
#pragma mark Class Method Helpers

- (void)updateLogTextView:(NSString *)newText nl:(BOOL)newLine {
	if (newLine == FALSE) {
		newText = [newText stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
	}
	
	[logContent appendFormat:@"%@\n", newText];
	
	[self performSelectorOnMainThread:@selector(sameThreadTextViewUpdate:) withObject:newText waitUntilDone:YES];
}

- (void)sameThreadTextViewUpdate:(NSString *)newText {
	//newText = [NSString stringWithFormat:@"%@%@\n", [logTextView text], newText];
	//[logTextView performSelectorOnMainThread:@selector(setText:) withObject:newText waitUntilDone:YES];
	[logTextView setText:[NSString stringWithFormat:@"%@%@\n",
						  [logTextView text],
						  newText]];
	
	[logTextView scrollRangeToVisible:NSMakeRange([logTextView.text length] - 7, 7)];
	
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewDidAppear:(BOOL)animated {
	[NSThread detachNewThreadSelector:@selector(synchronize) toTarget:self withObject:nil];
	
	[super viewDidAppear:animated];
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark UITableView Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		[cell.contentView addSubview:logTextView];
	}
	
	// Load tableview cell
	return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 250;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return 1;
}

#pragma mark -
#pragma mark Helper Methods

- (void)setSyncNowTitle:(NSString *)newTitle {
    self.title = newTitle;
}

- (void)cancelSyncBecauseOfErrors {
    [self updateLogTextView:@"\nERROR: Errors found, stopping sync. Please report this error to admin@kerofrog.com.au and try syncing again later (depending on the error). A log file has been created for this sync." nl:YES];
    [cancelBarButtonItem setEnabled:FALSE];
    [doneBarButtonItem setEnabled:TRUE];
    
    [syncLabel setText:@"Synchronization Failed"];
}

@end
