//
//  ReplaceToodledoTasksViewController.h
//  Manage
//
//  Created by Cliff Viegas on 27/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TaskListCollection;
@class TagCollection;
@class ArchiveList;

// Toodledo Collections
@class ToodledoTaskCollection;
@class ToodledoFolderCollection;

@protocol ReplaceToodledoTasksDelegate
- (void)closeSettingsPopover;
- (void)replaceToodledoDataComplete;
@end

@interface ReplaceToodledoTasksViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
	id <ReplaceToodledoTasksDelegate>		delegate;
	
	// Data Collections
	TaskListCollection					*refMasterTaskListCollection;
	ToodledoTaskCollection				*toodledoTaskCollection;
	ToodledoFolderCollection			*toodledoFolderCollection;
	TagCollection						*tagCollection;
	ArchiveList							*archiveList;
	
	// UIKit Objects
	UIBarButtonItem						*cancelBarButtonItem;
	UIBarButtonItem						*doneBarButtonItem;
	UITextView							*logTextView;
	UILabel								*syncLabel;
	UIActivityIndicatorView				*syncActivityIndicatorView;
	UITableView							*myTableView;
	
	// Keep track of Backup path
	NSString							*backupPath;
	NSMutableString						*logContent;
}

@property (nonatomic, assign)	id			delegate;
@property (nonatomic, copy)		NSString	*toodledoKey;

#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection;

@end
