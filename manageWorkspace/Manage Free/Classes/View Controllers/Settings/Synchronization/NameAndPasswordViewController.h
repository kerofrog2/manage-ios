//
//  NameAndPasswordViewController.h
//  Manage
//
//  Created by Cliff Viegas on 9/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Keychain Wrapper
@class KeychainWrapper;

// Data Models
@class ApplicationSetting;

// Toodledo Authentication
#import "ToodledoAuthentication.h"

@interface NameAndPasswordViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, ToodledoAuthenticationDelegate> {
	UITableView				*myTableView;
	NSString				*toodledoDetail;
	NSString				*toodledoUserName;
	ApplicationSetting		*toodledoUserNameAppSetting;
	KeychainWrapper			*keychainWrapper;
	
	// Connecting label and indicator view
	UILabel					*connectingLabel;
	UIActivityIndicatorView	*connectingActivityIndicatorView;
	UITextView				*connectingTextView;
}

@property (nonatomic, copy)		NSString	*toodledoDetail;
@property (nonatomic, copy)		NSString	*toodledoUserName;

@end
