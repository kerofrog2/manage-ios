//
//  ReplaceLocalTasksViewController.h
//  Manage
//
//  Created by Cliff Viegas on 17/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TaskListCollection;
@class TagCollection;
@class ToodledoAccountInfo;

// Toodledo Collections
@class ToodledoTaskCollection;
@class ToodledoFolderCollection;

// Data Models
#import "ToodledoFolder.h"

@protocol ReplaceLocalTasksDelegate
// To add delegates here
- (void)replaceLocalTasksSyncComplete;
- (void)closeSettingsPopover;
@end


@interface ReplaceLocalTasksViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, ToodledoFolderDelegate> {
	id <ReplaceLocalTasksDelegate>		delegate;
	
	// Data Collections
	TaskListCollection					*refMasterTaskListCollection;
	ToodledoTaskCollection				*toodledoTaskCollection;
	ToodledoFolderCollection			*toodledoFolderCollection;
	TagCollection						*tagCollection;
	
	// Data Models
	ToodledoAccountInfo					*refToodledoAccountInfo;
	
	NSString							*toodledoKey;
	
	// UIKit Objects
	UIBarButtonItem						*cancelBarButtonItem;
	UIBarButtonItem						*doneBarButtonItem;
	UITextView							*logTextView;
	UILabel								*syncLabel;
	UIActivityIndicatorView				*syncActivityIndicatorView;
	UITableView							*myTableView;
	
	// Keep track of Backup path
	NSString							*backupPath;
	NSMutableString						*logContent;
	BOOL								dataHasChanged;	// Need this to see whether we need to restore on a 'cancel'
}

@property (nonatomic, assign)	id			delegate;
@property (nonatomic, copy)		NSString	*toodledoKey;

#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection 
		  andToodledoAccountInfo:(ToodledoAccountInfo *)theAccountInfo;


@end
