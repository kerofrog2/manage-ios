//
//  StringSettingViewController.h
//  Manage
//
//  Created by Cliff Viegas on 4/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class ApplicationSetting;
@class PropertyDetail;

// Delegate
@protocol ListSettingDelegate
- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData;
@end


@interface ListSettingViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
	id <ListSettingDelegate>	delegate;
	
	ApplicationSetting			*refApplicationSetting;
	PropertyDetail				*refPropertyDetail;
	UITableView					*myTableView;
}

@property (nonatomic, assign)	id	delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(id<ListSettingDelegate>)theDelegate andAppSetting:(ApplicationSetting *)appSetting 
	 andPropertyDetail:(PropertyDetail *)thePropertyDetail;

@end
