//
//  DatabaseBackupViewController.m
//  Manage
//
//  Created by Cliff Viegas on 16/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "DataBackupViewController.h"

// Method Helper
#import "MethodHelper.h"

// Dropbox
#import <dropbox/dropbox.h>

// MBProgressHUB
#import "MBProgressHUD.h"

// View Controllers
#import "DataImportViewController.h"
#import "RestoreExportDataViewController.h"


#import "GAIConstants.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@interface DataBackupViewController() {
    UIBarButtonItem *editButton;
    // Delete filename
    NSString *deleteBackupFileName;
    // Indexpath of delete row.
    NSIndexPath *deleteIndexPath;
}
// Load Methods
- (void)loadTableView;

/* Dropbox variables */
@property (nonatomic, retain) DBAccountManager *manager;
@property (nonatomic, retain) DBFilesystem *filesystem;
@property (nonatomic, retain) DBPath *root;
@property (nonatomic, retain) NSMutableArray *contents;
@property (nonatomic, retain) DBPath *fromPath;
@property (nonatomic, assign) BOOL loadingFiles;

/* Full path of backup file when user selected backu */
@property (nonatomic, retain) NSString *backupFilePath;


@end

@implementation DataBackupViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[myTableView release];
	[backupListArray release];
	[importedBackupListArray release];
    [self.manager release];
    [self.filesystem release];
    [self.root release];
    [self.contents release];
    [self.fromPath release];
    [self.backupFilePath release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_DataBackupViewController;
#endif
        // Load the tableview
		[self loadTableView];
        
        // Add resync button for manual resync
        editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(editButtonTap:)];
        [self.navigationItem setRightBarButtonItem:editButton];
//        editButton.enabled = NO;
        
		// Set the title
		self.title = @"Data Backup";
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
		
		// Load the array of backup databases
		backupListArray = [[MethodHelper getSortedListOfBackupFiles:[MethodHelper getBackupPath]] retain];
		
		// Load the imported list of backup databases
		importedBackupListArray = [[MethodHelper getSortedListOfBackupFiles:[MethodHelper getHomePath]] retain];
		
        // Settings for dropbox
        self.manager = [DBAccountManager sharedManager];
        
        ManageAppDelegate *app = [[UIApplication sharedApplication] delegate];
        app.appDelegate = self;
        if (app.dbFileSystem != nil) {
            [DBFilesystem setSharedFilesystem:app.dbFileSystem];
            self.filesystem = app.dbFileSystem;
            self.root = [DBPath root];
            self.contents = [NSMutableArray array];
        } else {
            if ([DBFilesystem sharedFilesystem] == nil) {
                self.filesystem = nil;
                [[self.manager linkedAccount] unlink];
            }
        }
        // Sync with dropbox
        if ([self.manager linkedAccount]) {
            [self syncFiles];
//            editButton.enabled = YES;
        }
        
	}
	return self;
}

- (void) viewWillAppear:(BOOL)animated {
    // Reset observer.
    [self.manager removeObserver:self];
    [self.manager addObserver:self block: ^(DBAccount *account) {
        [self accountUpdated:account];
    }];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void) viewDidDisappear:(BOOL)animated {
    [self.manager removeObserver:self];
    ManageAppDelegate *app = [[UIApplication sharedApplication] delegate];
    app.appDelegate = nil;
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}
	
	// Default values
	cell.textLabel.text = @"";
	cell.textLabel.textAlignment = UITextAlignmentLeft;
	cell.textLabel.textColor = [UIColor darkTextColor];
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
	if (indexPath.section == 0) {
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.textLabel.textColor = [UIColor colorWithRed:86 / 255.0 green:104 / 255.0 blue:148 / 255.0 alpha:1.0];
		cell.textLabel.textAlignment = UITextAlignmentCenter;
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Create New Backup";
        } else {
            if ([self.manager linkedAccount]) {
                cell.textLabel.text = @"UnLink with Dropbox";
            } else {
                cell.textLabel.text = @"Link with Dropbox";
            }
        }
		
	} else if ([importedBackupListArray count] > 0 && indexPath.section == 1) {
        
		// Need list of imported backups
		NSString *backup = [importedBackupListArray objectAtIndex:indexPath.row];
		NSString *timeStampString = [MethodHelper getTimeStampStringFromFile:backup];
		NSTimeInterval timeInterval = [timeStampString doubleValue];
		NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
		cell.textLabel.text = [MethodHelper localizedDateTimeFrom:date 
												   usingDateStyle:NSDateFormatterMediumStyle 
													 andTimeStyle:NSDateFormatterMediumStyle];
	} else {
        
        if (indexPath.row < [backupListArray count]) {

            NSString *backup = [backupListArray objectAtIndex:indexPath.row];
            
            NSString *timeStampString = [MethodHelper getTimeStampStringFromFile:backup];
            
            NSTimeInterval timeInterval = [timeStampString doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
            
            //NSString *stringDate = [MethodHelper stringFromDate:date usingFormat:K_DATETIME_FORMAT];
            cell.textLabel.text = [MethodHelper localizedDateTimeFrom:date
                                                       usingDateStyle:NSDateFormatterMediumStyle
                                                         andTimeStyle:NSDateFormatterMediumStyle];
            
        } else {
            
//            DBFileInfo *info = [_contents objectAtIndex:indexPath.row - [backupListArray count]];
//            NSString *timeStampString = [MethodHelper getTimeStampStringFromFile:info.path.name];
//            
//            NSTimeInterval timeInterval = [timeStampString doubleValue];
//            NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
//            
//            //NSString *stringDate = [MethodHelper stringFromDate:date usingFormat:K_DATETIME_FORMAT];
//            cell.textLabel.text = [MethodHelper localizedDateTimeFrom:date
//                                                       usingDateStyle:NSDateFormatterMediumStyle
//                                                         andTimeStyle:NSDateFormatterMediumStyle];
            
        }
		

	}

	
	// Selecting a database will open new screen.. maybe showing more details?... oh shit.
	// After selected, there is also the option of 'exporting' that backup via email
	//
	
	
	
	// NOTE: Also need to store log (done)
	// NOTE: Also need to store images, zip them up (done)
	
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if ([importedBackupListArray count] > 0) {
		return 2;
	}
    if ([backupListArray count] > 0) {
        return 2;
    }
	return 1;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 2;
	} else if (section == 1 && [importedBackupListArray count] > 0) {
		return [importedBackupListArray count];
	} 
    if (section == 1) {
        return [backupListArray count];
    }
    return 0;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	// Backup database
	if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            
            self.backupFilePath = [MethodHelper createBackup];
            [self uploadFileToDropbox:[self.backupFilePath lastPathComponent]];
            [self.delegate dataSuccessfullyBackedUp];
            [myTableView deselectRowAtIndexPath:indexPath animated:YES];
#if ENABLE_GOOGLE_ANALYTICS
            // track this event to Google Analytics
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker sendEventWithCategory:kCategory_event
                                withAction:kAction_backup
                                 withLabel:kLabel_create_backup
                                 withValue:0];
#endif
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
                // AskingPoint event
                [APManager addEventWithName:kAskingPointEventNewBackup];
            }
            
        } else { // Link with Dropbox to get backup data.
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            if ([self.manager linkedAccount]) {
        
                dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                    [[tableView cellForRowAtIndexPath:indexPath].textLabel setText:@"Link with Dropbox"];
                    [[self.manager linkedAccount] unlink];
                    
                });
                
#if ENABLE_GOOGLE_ANALYTICS
                // track this event to Google Analytics
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker sendEventWithCategory:kCategory_event
                                    withAction:kAction_backup
                                     withLabel:kLabel_unlink_dropbox
                                     withValue:0];
#endif
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
                    // AskingPoint event
                    [APManager addEventWithName:kAskingPointEventUnLinkDropbox];
                }
                
            } else {
                [self.manager linkFromController:self.navigationController];
                [myTableView deselectRowAtIndexPath:indexPath animated:YES];
                //Hide HUD
                [MBProgressHUD hideHUDForView:self.view animated:YES];
#if ENABLE_GOOGLE_ANALYTICS
                // track this event to Google Analytics
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker sendEventWithCategory:kCategory_event
                                    withAction:kAction_backup
                                     withLabel:kLabel_link_dropbox
                                     withValue:0];
#endif
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
                    // AskingPoint event
                    [APManager addEventWithName:kAskingPointEventLinkDropbox];
                }
            }
        }
		
	} else if ([importedBackupListArray count] > 0 && indexPath.section == 1) {
		NSString *backupFile = [importedBackupListArray objectAtIndex:indexPath.row];
		NSString *backupPath = [NSString stringWithFormat:@"%@%@",
								[MethodHelper getHomePath], backupFile];
		RestoreExportDataViewController *controller = [[[RestoreExportDataViewController alloc] 
														initWithBackup:backupFile
														andBackupPath:backupPath] autorelease];
		controller.delegate = self;
		[self.navigationController pushViewController:controller animated:YES];
	} else {
        if (indexPath.row < [backupListArray count]) {

            if ([backupListArray count] > 0) {
                NSString *backupFile = [backupListArray objectAtIndex:indexPath.row];
                NSString *backupPath = [NSString stringWithFormat:@"%@%@",
                                        [MethodHelper getBackupPath], backupFile];
                
                RestoreExportDataViewController *controller = [[[RestoreExportDataViewController alloc]
                                                                initWithBackup:backupFile
                                                                andBackupPath:backupPath] autorelease];
                controller.delegate = self;
                [self.navigationController pushViewController:controller animated:YES];
            }
        } else {
            // Get info of item which user select, download file, save file then push view.
//            DBFileInfo *info = [_contents objectAtIndex:indexPath.row - [backupListArray count]];
//            [self downloadFileFromDropboxToBackup:info.path.name completionBlock:^(bool result) {
//                if (result == YES) {
//                    NSString *backupPath = [NSString stringWithFormat:@"%@%@",
//                                            [MethodHelper getBackupPath], info.path.name];
//                    
//                    RestoreExportDataViewController *controller = [[[RestoreExportDataViewController alloc]
//                                                                    initWithBackup:info.path.name
//                                                                    andBackupPath:backupPath] autorelease];
//                    controller.delegate = self;
//                    [self.navigationController pushViewController:controller animated:YES];
//                } else {
//                    [myTableView deselectRowAtIndexPath:indexPath animated:YES];
//                    return;
//                }
//            }];
        }
        [myTableView deselectRowAtIndexPath:indexPath animated:YES];
    }
	
	
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {

	return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (section == 0) {
		return @"";
	} else {
        if (section == 1) {
            if ([importedBackupListArray count] > 0) {
                return @"Imported Backups";
            } else {
                if ([backupListArray count] > 0) {
                    return @"Current Backups";
                }
            }
        }
    }
    
	return @"";
}

- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEditing] == YES) {
        if (indexPath.section == 0) {
            return NO;
        } else {
            return YES;
        }
    }
    return NO;
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        deleteBackupFileName = [backupListArray objectAtIndex:indexPath.row];
        deleteIndexPath = indexPath;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure you want to delete this backup?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alert show];
    }
}

#pragma mark - AlertView's delegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) { // YES
        // Delete file in local
        NSString *backupPath = [NSString stringWithFormat:@"%@%@",
                                [MethodHelper getBackupPath], deleteBackupFileName];
        // Create fileManager to remove file in local
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:backupPath]) {
            NSError *deleteFileError = nil;
            [fileManager removeItemAtPath:backupPath error:&deleteFileError];
            
            if (deleteFileError) {
                // Delete error
                NSLog(@"%@", [deleteFileError localizedDescription]);
            } else {
                
                // Delete file in dropbox
                [self deleteBackupFile:deleteBackupFileName];
                // Reload table view
                backupListArray = [[MethodHelper getSortedListOfBackupFiles:[MethodHelper getBackupPath]] retain];
                [myTableView reloadData];
            }
        }
    }
}

#pragma mark -
#pragma mark Restore Export Data Delegates

- (void)restoreDataComplete {
    [self.manager removeObserver:self];
	[self.delegate restoreDataComplete];
}

- (void)closeSettingsPopover {
    [self.manager removeObserver:self];
	[self.delegate closeSettingsPopover];
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


#pragma mark - Dropbox's code
NSInteger sortFileInfos(id obj1, id obj2, void *ctx) {
	return [[obj1 path] compare:[obj2 path]];
}

// Method is used for handling account's status change
- (void)accountUpdated:(DBAccount *)account {
    self.manager = [DBAccountManager sharedManager];
    ManageAppDelegate *app = [[UIApplication sharedApplication] delegate];
    if (account.linked == YES) {
        // Setup info after user login (link) Dropbox
        self.filesystem = [[DBFilesystem alloc] initWithAccount:account];
        app.dbFileSystem = _filesystem;
        self.root = [DBPath root];
        self.contents = [NSMutableArray array];
        [DBFilesystem setSharedFilesystem:self.filesystem];
        
        // Sync file after Link
        [self syncFiles];
        
//        [_filesystem addObserver:self block:^() {  }];
//        [_filesystem addObserver:self forPathAndChildren:self.root block:^() { [self loadFiles]; }];
    } else {
        app.dbFileSystem = nil;
        [DBFilesystem setSharedFilesystem:nil];
        [self.contents removeAllObjects];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            [myTableView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    }
    
    
}

- (void) didLoginDropBoxWithAccount:(DBAccount *)account {
    dispatch_async( dispatch_get_main_queue(), ^{
//        [myTableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

// Get file's info in dropbox
- (void)loadFiles {
	if (_loadingFiles) {
        return;
    }
	_loadingFiles = YES;
    
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^() {
		NSArray *immContents = [_filesystem listFolder:_root error:nil];
		NSMutableArray *mContents = [NSMutableArray arrayWithArray:immContents];
		[mContents sortUsingFunction:sortFileInfos context:NULL];
		dispatch_async(dispatch_get_main_queue(), ^() {
            [self.contents removeAllObjects];
            if ([mContents count] > 0) {
                for (int i = 0; i < [mContents count]; i++) {
                    // Check file's type, only add zip file to list. 
                    DBFileInfo *info = [mContents objectAtIndex:i];
                    NSArray *filenamePart = [info.path.name componentsSeparatedByString:@"."];
                    NSString *extention = [filenamePart objectAtIndex:[filenamePart count] - 1];
                    if ([extention isEqualToString:@"zip"]) {
                        [self.contents addObject:[mContents objectAtIndex:i]];
                    }
                }
                
            }
			_loadingFiles = NO;
			[myTableView reloadData];
		});
	});
}

// Sync files in local and dropbox
- (void)syncFiles {
	if (_loadingFiles) {
        return;
    }
	_loadingFiles = YES;
    
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^() {
		NSArray *immContents = [_filesystem listFolder:_root error:nil];
		NSMutableArray *mContents = [NSMutableArray arrayWithArray:immContents];
		[mContents sortUsingFunction:sortFileInfos context:NULL];
        
		dispatch_async(dispatch_get_main_queue(), ^() {
            [self.contents removeAllObjects];
            if ([mContents count] > 0) {
                for (int i = 0; i < [mContents count]; i++) {
                    // Check file's type, only add zip file to list.
                    DBFileInfo *info = [mContents objectAtIndex:i];
                    NSArray *filenamePart = [info.path.name componentsSeparatedByString:@"."];
                    NSString *extention = [filenamePart objectAtIndex:[filenamePart count] - 1];
                    if ([extention isEqualToString:@"zip"]) {
                        [self.contents addObject:[mContents objectAtIndex:i]];
                    }
                }
                
            }
            
            [self uploadAllBackupFileToDropbox];
            [self downloadAllBackupFileFromDropbox];
            
            // Load the array of backup databases
            backupListArray = [[MethodHelper getSortedListOfBackupFiles:[MethodHelper getBackupPath]] retain];
			_loadingFiles = NO;
            
            [myTableView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
//			[self loadFiles];
		});
	});
}

// Download file from Dropbox to local (backup directory)
- (BOOL) downloadFileFromDropboxToBackup:(NSString *)fileName {
    DBPath *backupFilePath = [[DBPath alloc] initWithString:fileName];
    DBError *error;
    DBFile *file = [self.filesystem openFile:backupFilePath error:&error];
    BOOL result;
    NSFileHandle *fileHandle = [file readHandle:&error];
    NSData *fileData = [fileHandle readDataToEndOfFile];
    NSString *backupPath = [NSString stringWithFormat:@"%@%@",
    [MethodHelper getBackupPath], fileName];
    result = [fileData writeToFile:backupPath atomically:YES];

    return result;
}

// Download file from droopbox (run in background)
- (void) downloadFileFromDropboxToBackup:(NSString *)fileName completionBlock:(void(^)(bool result))block {
    if (fileName == nil) {
        block(NO);
    }
    DBPath *backupFilePath = [[DBPath alloc] initWithString:fileName];
    DBError *error;
    DBFile *file = [self.filesystem openFile:backupFilePath error:&error];
//    DBFileStatus *status = file.status;
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Add code here to do background processing
        DBError *error;
        NSFileHandle *fileHandle = [file readHandle:&error];
        NSData *fileData = [fileHandle readDataToEndOfFile];
        dispatch_async( dispatch_get_main_queue(), ^{
            NSString *backupPath = [NSString stringWithFormat:@"%@%@",
                                    [MethodHelper getBackupPath], fileName];
            [fileData writeToFile:backupPath atomically:YES];
            block(YES);
        });
    });
}


// Upload backup file to Dropbox.
- (BOOL) uploadFileToDropbox:(NSString *)fileName {
    BOOL result = NO;
    if ([self.manager linkedAccount]) {
        DBPath *newBackupFilePath = [[DBPath alloc] initWithString:fileName];
        DBError *error;
        DBFile *file = [self.filesystem createFile:newBackupFilePath error:&error];
        BOOL result = [file writeContentsOfFile:[NSString stringWithFormat:@"%@%@",
                                                 [MethodHelper getBackupPath], fileName] shouldSteal:NO error:&error];
        if (result == YES) {
            [file close];
        } else {
            
        }
        if (self.filesystem.status != DBSyncStatusDownloading || self.filesystem.status != DBSyncStatusUploading) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }
    if (self.filesystem.status != DBSyncStatusDownloading && self.filesystem.status != DBSyncStatusUploading) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    return result;
}

// Upload all backup file to dropbox
- (void)uploadAllBackupFileToDropbox {
//    MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hub.labelText = @"Synchronizing...";
    // Check backup files in local, upload them to Dropbox if them don't exist
    for (NSString *localBackup in backupListArray) {
        BOOL isExist = NO;
        for (DBFileInfo *dropBoxFile in self.contents) {
            if ([localBackup isEqualToString:[dropBoxFile.path.name lastPathComponent]]) {
                isExist = YES;
                break;
            }
        }
        // If file is not exist, upload it to Dropbox
        if (isExist == NO) {
            [self uploadFileToDropbox:localBackup];
        }
    }
}

// Download all backup file from Dropbox
- (void)downloadAllBackupFileFromDropbox {
    
    for (DBFileInfo *dropBoxFile in self.contents) {
        BOOL isExist = NO;
        NSString *dropBoxFileName = [dropBoxFile.path.name lastPathComponent];
        for (NSString *localBackup in backupListArray) {
            if ([dropBoxFileName isEqualToString:localBackup]) {
                isExist = YES;
                break;
            }
        }
        // If dropbox file is not exist in local, down load them.
        if (isExist == NO) {
            
            [self downloadFileFromDropboxToBackup:dropBoxFileName completionBlock:^(bool result) {
                if (result) {
                    // Update when complete.
                    backupListArray = [[MethodHelper getSortedListOfBackupFiles:[MethodHelper getBackupPath]] retain];
                    [myTableView reloadData];
                    
                }
            }];
        }
    }
}

- (void) deleteBackupFile:(NSString *) backupFileName {
    
    if (backupFileName == nil || [backupFileName length] == 0) {
        return;
    }
    if ([self.manager linkedAccount]) {
        DBPath *deleteBackupFilePath = [[DBPath alloc] initWithString:backupFileName];
        DBError *error;
        [self.filesystem deletePath:deleteBackupFilePath error:&error];
    }
    
}

- (IBAction)resyncSelect:(id)sender {
    [self syncFiles];
}

- (IBAction)editButtonTap:(id)sender {
    // Check current edit mode of table.
    if ([myTableView isEditing] == NO) {
        
        [myTableView setEditing:YES animated:YES];
        [editButton setTitle:@"Done"];
    } else {
        
        [myTableView setEditing:NO animated:YES];
        [editButton setTitle:@"Edit"];
    }

    
}


@end
