//
//  RestoreExportDataViewController.h
//  Manage
//
//  Created by Cliff Viegas on 26/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// For email
#import <MessageUI/MessageUI.h>

@protocol RestoreExportDataDelegate
- (void)restoreDataComplete;
- (void)closeSettingsPopover;
@end


@interface RestoreExportDataViewController : GAITrackedViewController <MFMailComposeViewControllerDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource> {
	id <RestoreExportDataDelegate>	delegate;
	
	UITableView						*myTableView;
	NSString						*backupFile;
	NSString						*backupPath;
	NSString						*backupDataDate;
}

@property (nonatomic, assign)	id		delegate;

#pragma mark Initialisation
- (id)initWithBackup:(NSString *)theBackupFile andBackupPath:(NSString *)theBackupPath;

@end
