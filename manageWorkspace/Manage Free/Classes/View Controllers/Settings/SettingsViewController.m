    //
//  SettingsViewController.m
//  Manage
//
//  Created by Cliff Viegas on 6/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "SettingsViewController.h"

// Data Collections
#import "ApplicationSettingCollection.h"
#import "TagCollection.h"
#import "TaskListCollection.h"

// Method Helper
#import "MethodHelper.h"

// Data Models
#import "ApplicationSetting.h"
#import "PropertyDetail.h"

// View Controllers
#import "ManageTagsViewController.h"
#import "NewsGiveawaysViewController.h"
#import "Appirater.h"
#import "RemoveAdvertisingViewController.h"

// Test view controller
#import "ReplaceLocalTasksViewController.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)
#define kSwitchTag			98673
#define kNumberAdMob        4 

@interface SettingsViewController()
- (void)loadTableView;
- (UISwitch *)getSwitch;
@end


@implementation SettingsViewController

@synthesize delegate;
@synthesize refPopoverController;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[applicationSettingCollection release];
	[propertyCollection release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<SettingsDelegate>)theDelegate andTagCollection:(TagCollection *)theTagCollection 
andPropertyCollectionType:(NSInteger)propertyCollectionType 
 andTaskListCollection:(TaskListCollection *)theTaskListCollection {
	if ((self = [super init])) {
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the passed tag collection
		refTagCollection = theTagCollection;
		
		// Assign the passed task list collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Load the app setting collection
		applicationSettingCollection = [[ApplicationSettingCollection alloc] initWithAllUserSettings];
		
		// Load the property collection
		propertyCollection = [[PropertyCollection alloc] initWithCollectionType:propertyCollectionType];
		
		// Load the table view
		[self loadTableView];
		
		// Set the title
		self.title = propertyCollection.title;
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		
		[cell.contentView addSubview:[self getSwitch]];
	}
	
	// Get relevant data
	UISwitch *mySwitch = (UISwitch *)[cell.contentView viewWithTag:kSwitchTag];
	NSArray *propertySection = [propertyCollection.properties objectAtIndex:indexPath.section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:indexPath.row];

	ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:propertyDetail.name];
	
	// Default values
	cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
	cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
	cell.detailTextLabel.text = @"";
    cell.accessoryType = UITableViewCellAccessoryNone;
	
	// Set up the cell...
	cell.textLabel.text = propertyDetail.friendlyName;
    
	if (propertyDetail.propertyType == EnumPropertyTypeList) {
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		cell.detailTextLabel.text = appSetting.data;
	} else if (propertyDetail.propertyType == EnumPropertyTypeBoolean) {
		[mySwitch setHidden:NO];
		[mySwitch setOn:[[appSetting data] boolValue]];
	} else if (propertyDetail.propertyType == EnumPropertyTypeCustom) {
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	} else {
		cell.detailTextLabel.text = [appSetting data];
	}

    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return [[propertyCollection.properties objectAtIndex:section] count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [propertyCollection.properties count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSArray *propertySection = [propertyCollection.properties objectAtIndex:section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:0];
	return propertyDetail.section;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath { 
	NSArray *propertySection = [propertyCollection.properties objectAtIndex:indexPath.section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:indexPath.row];
	ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:propertyDetail.name];
	
	if ([propertyDetail propertyType] == EnumPropertyTypeList) {
		ListSettingViewController *controller = [[[ListSettingViewController alloc] initWithDelegate:self 
																					   andAppSetting:appSetting
																				   andPropertyDetail:propertyDetail] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"ManageTags"]) {
		// MANAGE TAGS
		[delegate settingsPopoverCanClose:NO];
		ManageTagsViewController *controller = [[[ManageTagsViewController alloc]
												 initWithTagCollection:refTagCollection] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"GeneralSettings"]) {
		// GENERAL SETTINGS
		SettingsViewController *controller = [[[SettingsViewController alloc] initWithDelegate:self.delegate 
																			  andTagCollection:refTagCollection 
																	 andPropertyCollectionType:EnumPropertyCollectionGeneral
																		 andTaskListCollection:refMasterTaskListCollection] autorelease];
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"Synchronize"]) {
		// SYNC WITH TOODLEDO
		SynchronizationViewController *controller = [[[SynchronizationViewController alloc] initWithMasterTaskListCollection:refMasterTaskListCollection] autorelease];
		controller.delegate = self;
		controller.refPopoverController = refPopoverController;
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"BackupDB"]) {
		// BACKUP DATABASE
		DataBackupViewController *controller = [[[DataBackupViewController alloc] init] autorelease];
		controller.delegate = self;
		[self.navigationController pushViewController:controller animated:YES];
	} else if ([[propertyDetail name] isEqualToString:@"SyncSettings"]) {
        SettingsViewController *controller = [[[SettingsViewController alloc] initWithDelegate:self.delegate 
                                                                              andTagCollection:refTagCollection 
                                                                     andPropertyCollectionType:EnumPropertyCollectionSync 
                                                                         andTaskListCollection:refMasterTaskListCollection] autorelease];
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[propertyDetail name] isEqualToString:@"PremiumThemes"]) {
        [self.delegate shopfrontSelected];
    } else if ([[propertyDetail name] isEqualToString:@"NewsGiveaways"]) {
        NewsGiveawaysViewController *controller = [[[NewsGiveawaysViewController alloc] init] autorelease];
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([[propertyDetail name] isEqualToString:@"RatingReview"]) {
        [Appirater rateApp];
    } else if ([[propertyDetail name] isEqualToString:@"SupportPage"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://manage.kerofrog.com.au/support/"]];
    } else if ([[propertyDetail name] isEqualToString:@"RemoveAdvertising"]) {
        RemoveAdvertisingViewController *controller = [[[RemoveAdvertisingViewController alloc] init] autorelease];
        [self.navigationController pushViewController:controller animated:YES];
    }
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	// Get switch object
	UISwitch *mySwitch = (UISwitch *)[cell.contentView viewWithTag:kSwitchTag];

	NSArray *propertySection = [propertyCollection.properties objectAtIndex:indexPath.section];
	PropertyDetail *propertyDetail = [propertySection objectAtIndex:indexPath.row];
	ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:propertyDetail.name];
	
	if ([propertyDetail propertyType] == EnumPropertyTypeBoolean) {
		
		BOOL switchStatus;
		// Change switch value
		if ([mySwitch isOn]) {
			[mySwitch setOn:NO animated:YES];
			switchStatus = NO;
		} else {
			[mySwitch setOn:YES animated:YES];
			switchStatus = YES;
		}
		
		// Update data
		appSetting.data = [NSString stringWithFormat:@"%d", switchStatus];
		[appSetting updateDatabase];
		
		[delegate settingWithName:appSetting.name updatedWithData:appSetting.data];
		
		return nil;
	}
	
	return indexPath;
}

#pragma mark -
#pragma mark String Settings Delegate

- (void)settingWithName:(NSString *)settingName updatedWithData:(NSString *)theData {
	ApplicationSetting *appSetting = [applicationSettingCollection getApplicationSettingWithName:settingName];
	
	appSetting.data = theData;
	
	[appSetting updateDatabase];
	
	[delegate settingWithName:settingName updatedWithData:theData];
}

#pragma mark -
#pragma mark Database Backup Delegates

- (void)dataSuccessfullyBackedUp {
	[MethodHelper showAlertViewWithTitle:@"Success" 
							  andMessage:@"Data successfully backed up" 
						  andButtonTitle:@"Ok"];
	[self.delegate closeSettingsPopover];
}

- (void)restoreDataComplete {
	[self.delegate updatesToDataComplete];
}

#pragma mark -
#pragma mark SynchronizationViewController Delegates

// Chain of delegates running back to main view, used to present a modal view...
- (void)settingsPopoverCanClose:(BOOL)canClose {
	[self.delegate settingsPopoverCanClose:canClose];
}

// Chain of delegates running back to main view, used to refresh everything
- (void)replaceLocalTasksSyncComplete {
	[self.delegate updatesToDataComplete];
}

- (void)replaceToodledoDataComplete {
    [self.delegate replaceToodledoDataComplete];
}

- (void)closeSettingsPopover {
	[self.delegate closeSettingsPopover];
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewWillAppear:(BOOL)animated {
	[myTableView reloadData];
	
	[delegate settingsPopoverCanClose:YES];
	
	[super viewWillAppear:animated];
}

#pragma mark -
#pragma mark Helper Methods

- (UISwitch *)getSwitch {
	UISwitch *switchView = [[[UISwitch alloc] initWithFrame:CGRectZero] autorelease];
	switchView.tag = kSwitchTag;
	// We dont want user interaction, clicking on table view cell will change this value
	[switchView setFrame:CGRectMake(200, 8, 0, 0)];
	switchView.userInteractionEnabled = FALSE;
	[switchView setHidden:YES];
	return switchView;
}



@end
