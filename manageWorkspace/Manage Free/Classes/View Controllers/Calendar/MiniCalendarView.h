//
//  MiniCalendarView.h
//  Manage
//
//  Created by Cliff Viegas on 7/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TaskListCollection;

// Kal Calendar (for delegate methods)
#import "Kal.h"

@class KalViewController;

@protocol MiniCalendarDelegate
- (void)didSelectDate:(NSDate *)theDate;
@end

@interface MiniCalendarView : UIView <KalDataSource> {
    KalViewController               *kalCalendar;       // The kal calendar
    NSMutableArray                  *calendarItems;
    id <KalDataSourceCallbacks>     callback;
    TaskListCollection              *refTaskListCollection;
    id <MiniCalendarDelegate>       selectionDelegate;
    BOOL                            didChangeMonth;
}

@property (nonatomic, assign)   id <MiniCalendarDelegate>   selectionDelegate;
@property (nonatomic, assign)   BOOL    didChangeMonth;

#pragma mark Initialisation
- (id)initWithFrame:(CGRect)frame andTaskListCollection:(TaskListCollection *)theTaskListCollection;


#pragma mark - Reload Methods
- (void)reloadData;

@end
