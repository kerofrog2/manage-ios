//
//  AdmobViewController.h
//  Manage Free
//
//  Created by Kerofrog on 6/14/13.
//
//

#import <UIKit/UIKit.h>
#import "GADBannerViewDelegate.h"

@class GADBannerView, GADRequest;

@interface AdmobViewController : UIViewController
<UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate>{
    GADBannerView *adBanner_;
    GADBannerView *adBanner2_;
    
    
    // Reference to the pop over controller
	UIPopoverController		*popoverController;
    
    UITableView		*myTableView;
}

@property (nonatomic, retain) GADBannerView *adBanner;
@property (nonatomic, retain) GADBannerView *adBanner2;

- (GADRequest *)createRequest;

#pragma mark Public Set Methods
- (void)setPopoverController:(UIPopoverController *)thePopoverController;

@end
