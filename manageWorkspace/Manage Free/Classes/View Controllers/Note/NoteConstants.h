//
//  NoteConstants.h
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 13/09/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

// Footer Lines
#define kFooterLine1Portrait                    CGRectMake(68, 790, 634, 1)
#define kFooterLine1Landscape                   CGRectMake(324, 530, 634, 1)

// Header and Border Lines
#define kNoteHeaderLine1Portrait				CGRectMake(68, 84, 634, 1)
#define kNoteHeaderLine1Landscape				CGRectMake(324, 84, 634, 1)

#define kNoteLeftBorderLinePortrait             CGRectMake(68, 84, 1, 706)
#define kNoteLeftBorderLineLandscape            CGRectMake(324, 84, 1, 446)

#define kNoteRightBorderLinePortrait            CGRectMake(702, 84, 1, 706)
#define kNoteRightBorderLineLandscape           CGRectMake(958, 84, 1, 446)

// Buttons
#define kNoteGreenPenButtonPortrait             CGRectMake(290, 44, 40, 37)
#define kNoteGreenPenButtonLandscape            CGRectMake(556, 44, 40, 37)

#define kNoteRedPenButtonPortrait               CGRectMake(340, 44, 40, 37)
#define kNoteRedPenButtonLandscape              CGRectMake(606, 44, 40, 37)

#define kNoteBluePenButtonPortrait              CGRectMake(390, 44, 40, 37)
#define kNoteBluePenButtonLandscape             CGRectMake(656, 44, 40, 37)

#define kNoteBlackPenButtonPortrait             CGRectMake(440, 44, 40, 37)
#define kNoteBlackPenButtonLandscape            CGRectMake(706, 44, 40, 37)

// Eraser and Clear
#define kNoteEraserButtonPortrait               CGRectMake(70, 44, 40, 37)
#define kNoteEraserButtonLandscape              CGRectMake(326, 44, 40, 37)

#define kNoteClearButtonPortrait                CGRectMake(120, 44, 40, 37)
#define kNoteClearButtonLandscape               CGRectMake(376, 44, 40, 37)

// Redo and Undo
#define kNoteRedoButtonPortrait                 CGRectMake(612, 44, 40, 37)
#define kNoteRedoButtonLandscape                CGRectMake(868, 44, 40, 37)

#define kNoteUndoButtonPortrait                 CGRectMake(662, 44, 40, 37)
#define kNoteUndoButtonLandscape                CGRectMake(918, 44, 40, 37)

// Line widths
#define kLineWidthsButtonPortrait               CGRectMake(562, 44, 40, 37)
#define kLineWidthsButtonLandscape              CGRectMake(818, 44, 40, 37)

// Scroll View
#define kNoteScrollViewPortrait                 CGRectMake(68, 85, 634, 705)
#define kNoteScrollViewLandscape				CGRectMake(324, 85, 634, 445)

/*
 
 // Divide by 5 from 405
 CGRect _ScrollControlFrame = {440, 193, 35, 81};
 */

// Divide by 5 from 705
#define kNoteScrollControllWindowFrame          CGRectMake(965, 238, 35, 141)

//#define kSortItemsSegmentedControlPortrait	CGRectMake(222, 47, 312, 29)
//#define kSortItemsSegmentedControlLandscape	CGRectMake(478, 46, 312, 29)

// Middle point in portrait is 378
// Middle point in landscape is 634

// #define kPenButtonPortrait					CGRectMake(319, 865, 35, 30)
// #define kPenButtonLandscape                 CGRectMake(575, 613, 35, 30)

// #define kDoneEditingButtonPortrait			CGRectMake(652, 46, 48, 29)
// #define kDoneEditingButtonLandscape			CGRectMake(908, 46, 48, 29)