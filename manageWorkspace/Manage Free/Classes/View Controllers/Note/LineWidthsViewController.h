//
//  LineWidthsViewController.h
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 4/10/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LineWidthsDelegate
- (void)lineWidthUpdatedWithMinSize:(CGFloat)minSize andMaxSize:(CGFloat)maxSize;
@end

@interface LineWidthsViewController : GAITrackedViewController {
    id <LineWidthsDelegate>     delegate;
    UISegmentedControl          *lineWidthsSegmentedControl;
}

@property (nonatomic, assign)       id  delegate;

#pragma mark Initialisation
- (id)initWithMaxWidth:(CGFloat)maxWidth;

#pragma mark Load Methods
- (void)loadLineWidthsSegmentedControl:(NSInteger)selectedIndex;

@end
