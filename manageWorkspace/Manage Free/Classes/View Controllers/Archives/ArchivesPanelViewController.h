//
//  ArchivePanelViewController.h
//  Manage
//
//  Created by Cliff Viegas on 23/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Responder
@class ArchivedListsTableViewResponder;

// Data Collections
@class TaskListCollection;

// Archives view controller
@class ArchivesViewController;

@interface ArchivesPanelViewController : GAITrackedViewController {
	TaskListCollection				*refMasterTaskListCollection;
	ArchivesViewController			*refArchivesViewController;
	
	UIImageView						*controlPanelImageView;			// The control panel
	UITableView						*listPadTableView;		// Table view to display archived lists
	NSInteger						currentlySelectedList;			// The currently selected task list
	UIImageView						*listPadImageView;					// The list paper background image
	UIView							*listPadHeaderLine1;				// Header line 1 for the list pad
	UIView							*listPadHeaderLine2;				// Header line 2 for the list pad
	UIView							*listPadFooterLine;					// Footer line for the list pad
	UIButton						*closeArchivesButton;			// Used to return to the main view
	UILabel							*listPadTitleLabel;
	UIView							*listPadArchivedFooterLine;		// Footer line to show when archived list button is visible
	UIButton						*archiveCompletedListsButton;	// For archiving any completed lists
}

@property (nonatomic, assign)	ArchivesViewController	*refArchivesViewController;

#pragma mark Initialisation
- (id)initWithArchivedListsTableViewResponder:(ArchivedListsTableViewResponder *)responder 
				  andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection;

#pragma mark Class Methods
- (void)hideArchiveCompleteLists:(BOOL)hide;

@end
