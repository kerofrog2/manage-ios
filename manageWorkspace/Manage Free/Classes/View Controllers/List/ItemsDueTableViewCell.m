//
//  ItemsDueTableViewCell.m
//  Manage
//
//  Created by Cliff Viegas on 16/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ItemsDueTableViewCell.h"

// Method Helper
#import "MethodHelper.h"

// Data Models
#import "ListItem.h"

CGRect scribbleRect = {4, 0, 200, 44};

@implementation ItemsDueTableViewCell

@synthesize listNameLabel;
@synthesize dueDateLabel;
@synthesize scribbleImageView;


#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[self.listNameLabel release];
    [self.dueDateLabel release];
    [self.scribbleImageView release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //Tan Nguyen fix leaks
        UIImageView * scribbTemp = [[UIImageView alloc] initWithFrame:scribbleRect];
        self.scribbleImageView = scribbTemp;
        self.scribbleImageView.image = nil;
        [self.contentView addSubview:self.scribbleImageView];
        [scribbTemp release];
        
        // Initialization code
		UILabel * lbNameTemp = [[UILabel alloc] initWithFrame:CGRectMake(109, 23, 98, 18)];
        self.listNameLabel = lbNameTemp;
		[self.listNameLabel setFont:[UIFont fontWithName:@"Helvetica" size:10]];
		[self.listNameLabel setBackgroundColor:[UIColor clearColor]];
		[self.listNameLabel setTextColor:[UIColor lightGrayColor]];
		[self.listNameLabel setTextAlignment:UITextAlignmentRight];
		[self.contentView addSubview:self.listNameLabel];
        [lbNameTemp release];
		//[self.listNameLabel release];
		
        UILabel * lbdueDate = [[UILabel alloc] initWithFrame:CGRectMake(109, -2, 98, 16)];
        self.dueDateLabel = lbdueDate;
        [self.dueDateLabel setFont:[UIFont fontWithName:@"Helvetica" size:10]];
        [self.dueDateLabel setBackgroundColor:[UIColor clearColor]];
		[self.dueDateLabel setTextColor:[UIColor lightGrayColor]];
        [self.dueDateLabel setTextAlignment:UITextAlignmentRight];
        [self.contentView addSubview:self.dueDateLabel];
        [lbdueDate release];
        
        
        
		//self.taskItem = [[UILabel alloc] initWithFrame:CGRectMake(40, 11, 450, 21)];
		//[self.taskItem setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		//[self.taskItem setBackgroundColor:[UIColor clearColor]];
		//[self.taskItem setTextColor:[UIColor darkTextColor]];
		//[self.contentView addSubview:self.taskItem];
		//[self.taskItem release];
    }
    return self;
}

- (void)loadScribbleImageForListItem:(ListItem *)listItem {
	// Display image if needed
	if ([[listItem scribble] length] > 0 && [listItem scribble] != nil) {
		self.scribbleImageView.image = nil;
        
        NSRange glsRange = [listItem.scribble rangeOfString:@"gls"];
        NSString *scribblePath = @"";
        if (glsRange.location != NSNotFound) { 
            scribblePath = [MethodHelper getPngScribblePathFromGLSScribble:listItem.scribble];
        } else {
            scribblePath = [NSString stringWithFormat:@"%@%@",
                            [MethodHelper getScribblePath],
                            listItem.scribble];
        }
		
		
		if ([listItem completed] == TRUE || [listItem archived] == TRUE) {
			[self.scribbleImageView setAlpha:0.2f];
		} else {
			[self.scribbleImageView setAlpha:1.0f];
		}
        
        UIImage *scribbleImage = [UIImage imageWithContentsOfFile:scribblePath];
        
		scribbleImage = [MethodHelper scaleImage:scribbleImage 
										maxWidth:500 * [UIScreen mainScreen].scale maxHeight:44 * [UIScreen mainScreen].scale];
		
        
        scribbleImage = [MethodHelper cropImage:scribbleImage usingCropRect:CGRectMake(0, 0, 200 * [UIScreen mainScreen].scale, 44 * [UIScreen mainScreen].scale)];
        
        [self.scribbleImageView setFrame:scribbleRect];
		[self.scribbleImageView setContentMode:UIViewContentModeScaleAspectFit];
		[self.scribbleImageView setImage:scribbleImage];
	} else {
		self.scribbleImageView.image = nil;
	}
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
