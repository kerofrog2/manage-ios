//
//  ListViewController.h
//  Manage
//
//  Created by Cliff Viegas on 2/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Responders (import needed for delegate)
#import "ItemsDueTableViewResponder.h"

// For email
#import <MessageUI/MessageUI.h>

// Progress Hud
#import "MBProgressHUD.h"

#ifdef IS_FREE_BUILD
#else
// Audio Toolbox
#import <AudioToolbox/AudioToolbox.h>
#endif

// Custom objects
@class CustomSegmentedControl;
#import "KFSegmentedControl.h"

// Data Models
@class TaskList;

// Constants
#import "ListConstants.h"
#ifdef IS_FREE_BUILD
#import "MainConstants.h"
#endif

// Data Collections
@class TagCollection;
@class ListItemTagCollection;
@class TaskListCollection;

// Pop Over Controllers (with required delegate link)
#import "NewListItemViewController.h"
#import "EditListItemViewController.h"
#import "HighlightersViewController.h"
#import "SettingsViewController.h"
#import "NewDueDateTableViewController.h"
#import "DateRangeViewController.h"
#import "DrawPadViewController.h"
#import "PngDrawPadViewController.h"
#import "PreviewListItemViewController.h"
#import "ChooseTextOCRViewController.h"

// Responders (with delegate)
#import "MiniListPadTableViewResponder.h"

// Calendar (for delegate)
#import "MiniCalendarView.h"

// Custom Objects
#import "ListItemTableViewCell.h"

#ifdef IS_FREE_BUILD
// Advertising
#import "MPAdView.h"
#endif

#ifdef __cplusplus
#include "baseapi.h"
using namespace tesseract;
#else
@class TessBaseAPI;
#endif

@class CustomSegmentedControl;

// Enum
typedef enum EnumControlPanelType {
    EnumControlPanelTypeCalendarList = 1,
    EnumControlPanelTypeDrawingTools = 0
} EnumControlPanelType;

typedef enum EnumSearchRangeType {
    EnumSearchRangeTypeAllLists = 0,
    EnumSearchRangeTypeThisList = 1
} EnumSearchRangeType;

@protocol ListViewDelegate
- (void)swapWithNote:(TaskList *)noteTaskList usingFrame:(CGRect)frame;
@end

#ifdef IS_FREE_BUILD

@interface ListViewController : GAITrackedViewController <MPAdViewDelegate, PngDrawPadDelegate, ListPadDelegate, MiniCalendarDelegate, DateRangeDelegate, MBProgressHUDDelegate, KFSegmentedControlDelegate, UISearchBarDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, NewListItemDueDateDelegate, SettingsDelegate, UIAlertViewDelegate, UITextFieldDelegate, ItemsDueResponderDelegate, UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, NewListItemDelegate, ListItemDelegate, EditListItemDelegate, DrawPadDelegate, HighlightersPopoverDelegate> {
	MPAdView                *_adView;
    
#else
    
    @interface ListViewController : GAITrackedViewController <PngDrawPadDelegate, ListPadDelegate, MiniCalendarDelegate, DateRangeDelegate, MBProgressHUDDelegate, KFSegmentedControlDelegate, UISearchBarDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, NewListItemDueDateDelegate, SettingsDelegate, UIAlertViewDelegate, UITextFieldDelegate, ItemsDueResponderDelegate, UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, NewListItemDelegate, ListItemDelegate, EditListItemDelegate, DrawPadDelegate, HighlightersPopoverDelegate, PreviewListItemViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ChooseTextOCRViewControllerDelegate> {
        
#endif
        
        id <ListViewDelegate>           delegate;
        
        // Data Models/Collections
        ListItem                        *currentListItem;
        TaskList						*refTaskList;
        TaskListCollection				*refTaskListCollection;
        TaskListCollection				*refMasterTaskListCollection;
        NSInteger						currentTaskListID;
        
        // Search Objects
        TaskListCollection				*filteredSearchTaskListCollection;
        UISearchBar						*mySearchBar;
        UISegmentedControl				*searchScopeSegmentedControl;
        UIButton						*searchButton;
        UIButton						*clearSearchObjectsButton;
        BOOL							isSearchMode;
        UILabel							*searchTitleLabel;
        UILabel                         *searchDateRangeLabel;
        UIButton                        *searchDateRangeButton;
        UIButton                        *showAllTasksButton;
        UIButton                        *searchListRangeButton;
        NSInteger                       searchRangeType;
        
        // Progress HUD
        MBProgressHUD                   *progressHUD;
        
        // Popover Controllers
        UIPopoverController				*newListItemPopOverController;
        UIPopoverController				*editListItemPopOverController;
        BOOL							editListItemViewControllerAtBaseLevel;
        UIPopoverController				*drawPadPopOverController;
        UIPopoverController				*highlightersPopoverController;
        UIPopoverController				*filterTagsPopoverController;
        UIPopoverController				*settingsPopoverController;
        UIPopoverController				*dateLabelPopoverController;
        UIPopoverController             *dateRangePopoverController;
        UIPopoverController				*contentOCRPopOverController;
        UIPopoverController             *popoverImageViewController;
        
        // Backgrounds & Images
        UIImageView						*canvassImage;
        UIImageView						*borderImage;
        UIImageView						*pageImage;
        UIImageView						*listCompletedStampImage;
        
        // List objects
        UITableView						*myTableView;
        UIView							*headerLine1;
        UIView							*headerLine2;
        NSInteger						selectedRow;
        BOOL							itemsDueSelected;
        UIButton						*listOptionsButton;
        UIActionSheet					*listOptionsActionSheet;
        UIActionSheet                   *listOptionsPhotoOCRActionSheet;
        
        // Title, completed and date field
        UITextField						*titleTextField;
        UIButton						*dateLabelButton;
        UIButton						*listCompletedButton;
        
        // Items due tableview
        UITableView						*itemsDueTableView;
        UIImageView						*itemsDuePocket;
        ItemsDueTableViewResponder		*itemsDueTableViewResponder;
        
        // Top buttons
        UIButton						*editListButton;
        UIButton						*newListItemButton;
        UIButton						*doneEditingButton;
        UIButton						*newScribbleListItemButton;
        KFSegmentedControl				*sortItemsSegmentedControl;
        UIButton                        *newListItemOCRButton;
        
        // Control Panel Objects
        UIImageView						*controlPanelImage;
        UIImageView						*highlightersContainerImageView;
        UIImageView						*pensContainerImageView;
        UIImageView                     *calendarListsDividerImageView;
        UISegmentedControl              *controlPanelTypeSegmentedControl;
        UIView                          *drawingToolsContainerView;
        UIView                          *calendarListToolsContainerView;
        UIView                          *toolsContainerView;
        MiniListPadTableViewResponder   *miniListPadTableViewResponder;         // The responder for the list pad table view
        UITableView                     *miniListPadTableView;					// The list pad table view that sits on list pad image view
        UIImageView                     *dayCalendarImageView;
        UILabel                         *dayCalendarDateLabel;
        
        // Actual highlighter and pen buttons
        UIButton						*greenPenButton;
        UIButton						*redPenButton;
        UIButton						*bluePenButton;
        UIButton						*blackPenButton;
        
        UIButton						*yellowHighlighterButton;
        UIButton						*redHighlighterButton;
        UIButton						*whiteHighlighterButton;
        
        // Pen and Highlighter Icon Buttons
        UIButton						*penButton;
        UIButton						*highlighterButton;
        UIImageView						*selectTaskImageView;
        BOOL							selectRowPrompt;
        EnumHighlighterColor			highlighterSelected;
        EnumPenColor					penSelected;
        
        // Cover View
        UIImageView						*highlighterCoverView;
        BOOL							isHelpSelected;		// For covering when help overlay is on or off
        
        // Tag related properties/Objects
        TagCollection					*refTagCollection;
        UIButton						*filterTagsButton;
        UIButton						*filterTagsClearButton;
        ListItemTagCollection			*filterListItemTagCollection;
        TaskListCollection				*filterTagsTaskListCollection;
        UIView							*filterTagsFooterLine;
        UILabel							*filterTagsLabel;
        UIImageView						*filterTagsImageView;
        
        // Settings View Objects
        BOOL							settingsViewControllerCanClose;
        UIBarButtonItem					*settingsBarButtonItem;
        
        // Need a tile view frame for shrinking (if needed), normal shrink if this is cgrectzero
        CGRect							tileViewFrame;
        
        // Calendar
        MiniCalendarView                *miniCalendarView;
        NSString                        *searchStartDateRange;
        NSString                        *searchEndDateRange;
        
        // List View Controller Object Properties
        NSString                        *backButtonTitle;
        
        NSMutableArray                  *arrayTaskList;
        
        // The Array contains task which was checked to complete, but not yet delete cell of this task
        NSMutableArray                  *completedTasksArray;
        
        // The Array contains repeat task which was checked to complete, but not yet delete cell of this task, because user is completing a repeat task
        NSMutableArray                  *completedRepeatTasksArray;
        BOOL                            isCompletingRepeatTask;
        
        ListItem                        *currentRepeatListItem;
        ListItem                        *oldRepeatListItem;
        
        TessBaseAPI *tesseract;
        uint32_t *pixels;
        NSString *textOCR;
    }
    
    @property (nonatomic, assign)   id                          delegate;
    @property (nonatomic, assign)	CGRect                      tileViewFrame;
    @property (nonatomic, assign)	BOOL                        isSearchMode;
    @property (nonatomic, retain)   MiniCalendarView            *miniCalendarView;
    @property (nonatomic, copy)     NSString                    *backButtonTitle;
    @property (nonatomic, assign)   NSInteger                   searchRangeType;
    @property (nonatomic, retain)   NSMutableArray              *arrayTaskList;
    @property (nonatomic, retain)   NSMutableArray              *completedTasksArray;
    @property (nonatomic, retain)   NSMutableArray              *completedRepeatTasksArray;
    @property (nonatomic, retain)   NSTimer                     *timer;
    @property (nonatomic) BOOL isLoadView;
    @property (nonatomic) BOOL isCompletingRepeatTask;
    // status of List Item is checked
    @property (assign) BOOL ischeckCompletedTask;
    @property (assign) BOOL isShowWritingTasksPopover;
    @property (nonatomic, retain)   TaskList					*refTaskList;
    @property (nonatomic, retain)   ListItem                    *currentListItem;
    @property (nonatomic, retain)   ListItem                    *currentRepeatListItem;
    @property (nonatomic, retain)   ListItem                    *oldRepeatListItem;
    @property (nonatomic, retain)   UIPopoverController         *popoverImageViewController;
    @property (nonatomic, retain)   NSString                    *textOCR;
    
    
#pragma mark Initialisation
    - (id)initWithTaskList:(TaskList *)theList andTagCollection:(TagCollection *)theTagCollection 
andTaskListCollection:(TaskListCollection *)theTaskListCollection 
andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection 
andBackButtonTitle:(NSString *)theBackButtonTitle;
    
    
#pragma mark Send Mail Methods
    - (void)sendTextMailForTaskList:(TaskList *)theTaskList;
    - (void)sendTextMailForTaskListCollection:(TaskListCollection *)theTaskListCollection;
    - (void)sendPDFMailForTaskList:(TaskList *)theTaskList;
    - (void)sendPDFMailForTaskListCollection:(TaskListCollection *)theTaskListCollection;
    
    @end
