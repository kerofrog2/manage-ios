//
//  ToDoTableViewCell.m
//  Manage
//
//  Created by Cliff Viegas on 5/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "BackupListItemTableViewCell.h"

// Method Helper
#import "MethodHelper.h"

// Data Models
#import "ListItem.h"
#import "Tag.h"
#import "ListItemTag.h"

// Data Collections
#import "TagCollection.h"
#import "ListItemTagCollection.h"

// Views
#import "QuartzTagView.h"

// Constants
#define kTagsViewFrame			CGRectMake(500, 12, 125, 20)
#define kEditingTagsViewFrame	CGRectMake(450, 12, 125, 20)
#define kTaskDateFrame			CGRectMake(500, 12, 125, 20)

// Private Methods
@interface BackupListItemTableViewCell()
- (UIImage *)getRandomHighlighterImageForColor:(NSInteger)theColor andSide:(NSInteger)theSide;
@end


@implementation BackupListItemTableViewCell

@synthesize delegate;
@synthesize taskItem;
@synthesize taskDate;
@synthesize taskCompleted;
@synthesize scribbleImageView;
@synthesize highlighterView;
@synthesize leftHighlighterImageView;
@synthesize rightHighlighterImageView;
@synthesize tagsView;
@synthesize tagsFont;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	/*[self.taskItem release];
	 [self.taskDate release];
	 [self.taskCompleted release];
	 [self.scribbleImageView release];
	 [self.highlighterView release];
	 [self.leftHighlighterImageView release];
	 [self.rightHighlighterImageView release];
	 
	 if (self.tagsView != nil) {
	 [self.tagsView release];
	 }
	 
	 if (self.tagsFont != nil) {
	 [self.tagsFont release];
	 }*/
    [super dealloc];
}

/*
 For getting the width of the string in the textfield, can be used for dynamic resizing of the text
 field and for positioning a notes icon (if they exist) after the text
 
 call this subclass with a (add notes image)
 
 UIFont *myFont = [UIFont boldSystemFontOfSize:15.0];
 // Get the width of a string ...
 CGSize size = [@"Some string here!" sizeWithFont:myFont];
 
 // Get the width of a string when wrapping within a particular width
 NSString *loremIpsum = @"Lorem Ipsum Delores S...";
 CGSize size = [loremIpsum sizeWithFont:myFont
 forWidth:150.0
 lineBreakMode:UILineBreakModeWordWrap];
 
 */

#pragma mark -
#pragma mark Initialisation

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
		//self.indentWidth = 23.5f;
		
		// Add the tags view in to content view
		//self.tagsView = [[UIView alloc] initWithFrame:CGRectMake(500, 21, 125, 20)];
		self.tagsView = [[[UIView alloc] initWithFrame:kTagsViewFrame] autorelease];
		[self.tagsView setBackgroundColor:[UIColor clearColor]];
		[self.contentView addSubview:self.tagsView];
		//[self.tagsView release];
		
		//CGFloat originX = 
		//	CGFloat taskItemWidth = self.frame.size.width - (40 + 105);
		//	CGFloat totalWidth = self.frame.size.width;
		self.taskItem = [[[UILabel alloc] initWithFrame:CGRectMake(40, 12, 450, 21)] autorelease];
		[self.taskItem setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[self.taskItem setBackgroundColor:[UIColor clearColor]];
		[self.taskItem setTextColor:[UIColor darkTextColor]];
		[self.contentView addSubview:self.taskItem];
		//[self.taskItem release];
		
		self.taskDate = [[[UILabel alloc] initWithFrame:kTaskDateFrame] autorelease];
		[self.taskDate setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
		[self.taskDate setBackgroundColor:[UIColor clearColor]];
		[self.taskDate setTextAlignment:UITextAlignmentRight];
		[self.taskDate setTextColor:[UIColor darkTextColor]];
		[self.contentView addSubview:self.taskDate];
		//[self.taskDate release];
		
		self.taskCompleted = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
		[self.taskCompleted setFrame:CGRectMake(2, 2, 40, 40)];
		[self.taskCompleted addTarget:self action:@selector(taskCompletedAction) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview:self.taskCompleted];
		//[self.taskCompleted release];
		
		self.scribbleImageView = [[[UIImageView alloc] initWithFrame:kListItemScribbleRect] autorelease];
		[self.scribbleImageView setContentMode:UIViewContentModeLeft];
		[self.contentView addSubview:self.scribbleImageView];
		//[self.scribbleImageView release];
		
		// Init the highlighter view
		self.highlighterView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
		[self.contentView addSubview:self.highlighterView];
		//[self.highlighterView release];
		
		// Init left highlighter view
		self.leftHighlighterImageView = [[[UIImageView alloc] initWithFrame:CGRectZero] autorelease];
		[self.contentView addSubview:self.leftHighlighterImageView];
		//[self.leftHighlighterImageView release];
		
		// Init right highlighter view
		self.rightHighlighterImageView = [[[UIImageView alloc] initWithFrame:CGRectZero] autorelease];
		[self.contentView addSubview:self.rightHighlighterImageView];
		//[self.rightHighlighterImageView release];
		
		
		
		// Init the tags font
		self.tagsFont = [UIFont fontWithName:@"Helvetica" size:11.0];
		
		// Create the quartz core tag view
		//QuartzTagView *quartzTagView = [[QuartzTagView alloc] initWithFrame:CGRectMake(200, 3, 40, 40)];
		//[self.contentView addSubview:quartzTagView];
    }
    return self;
}

- (id)initWithPreviewStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
		//self.indentWidth = 23.5f;
		
		//CGFloat originX = 
		//	CGFloat taskItemWidth = self.frame.size.width - (40 + 105);
		//	CGFloat totalWidth = self.frame.size.width;
		self.taskItem = [[[UILabel alloc] initWithFrame:CGRectMake(40, 12, 260, 21)] autorelease];
		[self.taskItem setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
		[self.taskItem setBackgroundColor:[UIColor clearColor]];
		[self.taskItem setTextColor:[UIColor darkTextColor]];
		[self.contentView addSubview:self.taskItem];
		//[self.taskItem release];
		
		self.taskDate = [[[UILabel alloc] initWithFrame:CGRectMake(500, 
																  12, 120, 20)] autorelease];
		[self.taskDate setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
		[self.taskDate setBackgroundColor:[UIColor clearColor]];
		[self.taskDate setTextAlignment:UITextAlignmentRight];
		[self.taskDate setTextColor:[UIColor darkTextColor]];
		[self.contentView addSubview:self.taskDate];
		//[self.taskDate release];
		
		self.taskCompleted = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxNotTicked.png"] forState:UIControlStateNormal];
		[self.taskCompleted setImage:[UIImage imageNamed:@"BoxTicked.png"] forState:UIControlStateSelected];
		[self.taskCompleted setFrame:CGRectMake(2, 2, 40, 40)];
		[self.taskCompleted addTarget:self action:@selector(taskCompletedAction) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview:self.taskCompleted];
		//[self.taskCompleted release];
		
		self.scribbleImageView = [[[UIImageView alloc] initWithFrame:kListItemPreviewScribbleRect] autorelease];
		[self.scribbleImageView setContentMode:UIViewContentModeLeft];
		[self.contentView addSubview:self.scribbleImageView];
		//[self.scribbleImageView release];
		
		// Init the highlighter view
		self.highlighterView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
		[self.contentView addSubview:self.highlighterView];
		//[self.highlighterView release];
		
		// Init left highlighter view
		self.leftHighlighterImageView = [[[UIImageView alloc] initWithFrame:CGRectZero] autorelease];
		[self.contentView addSubview:self.leftHighlighterImageView];
		//[self.leftHighlighterImageView release];
		
		// Init right highlighter view
		self.rightHighlighterImageView = [[[UIImageView alloc] initWithFrame:CGRectZero] autorelease];
		[self.contentView addSubview:self.rightHighlighterImageView];
		//[self.rightHighlighterImageView release];
		
		// Set tagsview and font to nil
		self.tagsView = nil;
		self.tagsFont = nil;
    }
    return self;
}

#pragma mark -
#pragma mark Editing Mode Methods

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	[self.tagsView setHidden:editing];
	[self.taskDate setHidden:editing];
	[super setEditing:editing animated:animated];
}

#pragma mark -
#pragma mark Class Methods

- (void)loadTagsForListItem:(ListItem *)listItem andTagCollection:(TagCollection *)tagCollection {
	// Remove all subviews first
	for (int i = [[self.tagsView subviews] count] - 1; i >= 0; i--) {
		[[[self.tagsView subviews] objectAtIndex:i] removeFromSuperview];
	}
	
	// Exit if there are no tags
	if ([listItem.listItemTagCollection.listItemTags count] == 0) {
		// Set task date frame back to standard
		[self.taskDate setFrame:kTaskDateFrame];
		return;
	}
	
	if ([listItem.dueDate length] > 0) {
		// Shift both frames
		[self.taskDate setFrame:CGRectMake(self.taskDate.frame.origin.x, 
										   0, self.taskDate.frame.size.width, self.taskDate.frame.size.height)];
		[self.tagsView setFrame:CGRectMake(self.tagsView.frame.origin.x, 
										   20, self.tagsView.frame.size.width, self.tagsView.frame.size.height)];
	} else {
		[self.tagsView setFrame:kTagsViewFrame];
	}
	
	
	CGFloat tagBuffer = 10.0;
	CGFloat tagBorder = 8.0;
	
	CGFloat totalBuffer = 0.0; //+ (tagBuffer / 2);
	
	NSInteger extraTagCount = 0;
	NSInteger indexCount = 0;
	
	//for (ListItemTag *listItemTag in listItem.listItemTagCollection.listItemTags) {
	for (Tag *theTag in tagCollection.tags) {
		
		if ([listItem.listItemTagCollection containsTagWithID:theTag.tagID] == FALSE) {
			continue;
		}
		
		NSString *tagName = theTag.name; //[tagCollection getNameForTagWithID:listItemTag.tagID];
		
		CGSize tagSize = [tagName sizeWithFont:self.tagsFont];
		
		// Need to make sure total buffer is less than the right border of text view (add 100 to allow it to cut partway through)
		if ((625 + 100) - (totalBuffer + tagSize.width + tagBorder) < self.taskItem.frame.origin.x + self.taskItem.frame.size.width) {
			extraTagCount++;
			if (indexCount == [listItem.listItemTagCollection.listItemTags count] - 1) {
				// Last index
				tagName = [NSString stringWithFormat:@"+%d", extraTagCount];
				tagSize = [tagName sizeWithFont:self.tagsFont];
			} else {
				indexCount++;
				continue;
			}
		}
		
		// Add the label to the tag view
		UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.tagsView.frame.size.width - totalBuffer - tagBorder - tagSize.width, 
																	  0, 
																	  tagSize.width + tagBorder, self.tagsView.frame.size.height)];
		
		totalBuffer = totalBuffer + tagLabel.frame.size.width + tagBuffer;
		
		
		
		[tagLabel setBackgroundColor:[UIColor clearColor]];
		[tagLabel setTextAlignment:UITextAlignmentCenter];
		[tagLabel setTextColor:[UIColor darkTextColor]];
		[tagLabel setText:tagName];
		[tagLabel setFont:self.tagsFont];
		
		QuartzTagView *quartzTagView = [[QuartzTagView alloc] initWithFrame:CGRectMake(tagLabel.frame.origin.x - (tagBorder / 2), 
																					   0, tagLabel.frame.size.width + tagBorder, 
																					   20)
                                        andTagColour:theTag.colour];
		// If this goes over task item then make it transparent
		[quartzTagView setAlpha:0.9f];
		[tagLabel setAlpha:0.9f];
		
		[self.tagsView addSubview:quartzTagView];
		[quartzTagView release];
		
		if (listItem.completed) {
			[self.tagsView setAlpha:0.3f];
		} else {
			[self.tagsView setAlpha:1.0f];
		}
		
		
		//[self.tagsView addSubview:tagLabel];
		[self.tagsView addSubview:tagLabel];
		[tagLabel release];
		
		// Increment index count
		indexCount++;
		
		// Position each label and reposition rest of row
	}
	
	if (625 - totalBuffer < self.taskItem.frame.origin.x + self.taskItem.frame.size.width) {
		CGFloat cutAmount = (self.taskItem.frame.origin.x + self.taskItem.frame.size.width) - (625 - totalBuffer);
		cutAmount += (tagBuffer * 2);
		[self.taskItem setFrame:CGRectMake(self.taskItem.frame.origin.x, 
										   self.taskItem.frame.origin.y, 
										   self.taskItem.frame.size.width - cutAmount, 
										   self.taskItem.frame.size.height)];
		
		
	}
	
}

- (void)setHighlightStyle:(EnumHighlighterColor)highlighter {
	UIFont *myFont = [UIFont fontWithName:@"Helvetica" size:14.0];
	// Get the width of a string ...
	NSString *taskItemString = [self.taskItem text];
	CGSize size = [taskItemString sizeWithFont:myFont];
	
	if (size.width > self.taskItem.frame.size.width) {
		size.width = self.taskItem.frame.size.width;
	}
	
	if (size.width < 35) {
		size.width = 35;
	}
	
	// Get the width of a string when wrapping within a particular width
	//NSString *loremIpsum = @"Lorem Ipsum Delores S...";
	//CGSize size = [loremIpsum sizeWithFont:myFont
	//							  forWidth:150.0
	//						 lineBreakMode:UILineBreakModeWordWrap];
	
	// red: 237, 28, 36
	// yellow: 255, 255, 51
	
	if (highlighter == EnumHighlighterColorWhite || highlighter == EnumHighlighterColorNone) {
		[self.highlighterView setFrame:CGRectZero];
		[self.highlighterView setAlpha:0.0f];
		[self.leftHighlighterImageView setFrame:CGRectZero];
		[self.leftHighlighterImageView setAlpha:0.0f];
		[self.rightHighlighterImageView setFrame:CGRectZero];
		[self.rightHighlighterImageView setAlpha:0.0f];
	} else if (highlighter == EnumHighlighterColorYellow) {
		[self.highlighterView setFrame:CGRectMake(self.taskItem.frame.origin.x, 
												  self.taskItem.frame.origin.y, 
												  size.width, 
												  self.taskItem.frame.size.height)];
		[self.highlighterView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:0.2 alpha:1.0]];
		[self.highlighterView setAlpha:0.3f];
		
		// Show the left highlighter view
		[self.leftHighlighterImageView setImage:[self getRandomHighlighterImageForColor:EnumHighlighterColorYellow 
																				andSide:EnumHighlightSideLeft]];
		[self.leftHighlighterImageView setFrame:CGRectMake(self.taskItem.frame.origin.x - 10, 
														   self.taskItem.frame.origin.y, 
														   10, 21)];
		
		//[self.leftHighlighterImageView setFrame:CGRectMake(0, 
		//													0, 
		//																	   10, 21)];
		
		[self.leftHighlighterImageView setAlpha:0.3f];
		
		// Show the right highlighter view
		[self.rightHighlighterImageView setImage:[self getRandomHighlighterImageForColor:EnumHighlighterColorYellow 
																				 andSide:EnumHighlightSideRight]];
		[self.rightHighlighterImageView setFrame:CGRectMake(self.taskItem.frame.origin.x + size.width, 
															self.taskItem.frame.origin.y, 
															10, 21)];
		[self.rightHighlighterImageView setAlpha:0.3f];
		
	} else if (highlighter == EnumHighlighterColorRed) {
		[self.highlighterView setFrame:CGRectMake(self.taskItem.frame.origin.x, 
												  self.taskItem.frame.origin.y, 
												  size.width,
												  self.taskItem.frame.size.height)];
		[self.highlighterView setBackgroundColor:[UIColor colorWithRed:0.92941176 green:0.10980392 blue:0.14117647 alpha:1.0]];
		[self.highlighterView setAlpha:0.3f];
		
		// Show the left highlighter view
		[self.leftHighlighterImageView setImage:[self getRandomHighlighterImageForColor:EnumHighlighterColorRed 
																				andSide:EnumHighlightSideLeft]];
		
		[self.leftHighlighterImageView setFrame:CGRectMake(self.taskItem.frame.origin.x - 10, 
														   self.taskItem.frame.origin.y, 
														   10, 21)];
		[self.leftHighlighterImageView setAlpha:0.3f];
		
		// Show the right highlighter view
		[self.rightHighlighterImageView setImage:[self getRandomHighlighterImageForColor:EnumHighlighterColorRed 
																				 andSide:EnumHighlightSideRight]];
		[self.rightHighlighterImageView setFrame:CGRectMake(self.taskItem.frame.origin.x + size.width, 
															self.taskItem.frame.origin.y, 
															10, 21)];
		[self.rightHighlighterImageView setAlpha:0.3f];
		
	}
}

- (void)setIndentLevel:(NSInteger)indent {
	CGFloat indentAmount = indent * self.indentationWidth;
	
	[self.taskItem setFrame:CGRectMake(40 + indentAmount, 11, 450 - indentAmount, 21)];
	[self.taskCompleted setFrame:CGRectMake(2 + indentAmount, 2, 40, 40)];
}


//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

#pragma mark -
#pragma mark Button Actions

- (void)taskCompletedAction {
	// Will also need some sort of delegate here, bugger, to update state
	// Not ... needed, as the view controller will set a tag for the button.  Just need to retrieve this on call.
	// But must have delegate... still..
	
	if ([self.taskCompleted isSelected] == FALSE) {
		[self.taskCompleted setSelected:YES];
		[self.tagsView setAlpha:0.3f];
	} else if ([self.taskCompleted isSelected] == TRUE) {
		[self.taskCompleted setSelected:NO];
		[self.tagsView setAlpha:1.0f];
	}
	
	[self.delegate taskCompleted:[self.taskCompleted isSelected] forListItemID:self.taskCompleted.tag]; 
	// - (void)taskCompleted:(BOOL)isCompleted forListID:(NSInteger)theListID;
}

#pragma mark -
#pragma mark Scribble Image Methods

- (void)loadScribbleImageForListItem:(ListItem *)listItem andIsPreviewList:(BOOL)isPreviewList {
	// Display image if needed
	if ([[listItem scribble] length] > 0 && [listItem scribble] != nil) {
		self.scribbleImageView.image = nil;
		
		NSString *scribblePath = [NSString stringWithFormat:@"%@%@",
								  [MethodHelper getScribblePath],
								  listItem.scribble];
		
		if ([listItem completed] == TRUE) {
			[self.scribbleImageView setAlpha:0.2f];
		} else {
			[self.scribbleImageView setAlpha:1.0f];
		}
		UIImage *scribbleImage = [UIImage imageWithContentsOfFile:scribblePath];
		scribbleImage = [MethodHelper scaleImage:scribbleImage 
										maxWidth:500 maxHeight:44];
		
		if (isPreviewList == TRUE) {
			[self.scribbleImageView setFrame:kListItemPreviewScribbleRect];
			
			scribbleImage = [MethodHelper cropImage:scribbleImage 
									  usingCropRect:CGRectMake(0, 0, 
															   self.taskItem.frame.size.width, 44)];
		} else if (self.taskItem.frame.size.width < 450) {
			scribbleImage = [MethodHelper cropImage:scribbleImage 
									  usingCropRect:CGRectMake(0, 0, 
															   self.taskItem.frame.size.width, 44)];
		}
		
		[self.scribbleImageView setImage:scribbleImage];
	} else {
		self.scribbleImageView.image = nil;
	}
}

#pragma mark -
#pragma mark Class Helper Methods

- (void)setStandardIndent {
	// Standard Task
	[self.taskItem setFrame:CGRectMake(40, 12, 450, 21)];
	[self.taskCompleted setFrame:CGRectMake(2, 2, 40, 40)];
	[self.scribbleImageView setFrame:kListItemScribbleRect];
}

- (void)setIndentForListItem:(ListItem *)listItem {
	if (listItem.parentListItemID == -1) {
		// Standard Task
		[self.taskItem setFrame:CGRectMake(40, 12, 450, 21)];
		[self.taskCompleted setFrame:CGRectMake(2, 2, 40, 40)];
		[self.scribbleImageView setFrame:kListItemScribbleRect];
	} else {
		// Sub Task
		[self.taskItem setFrame:CGRectMake(63, 12, 427, 21)];
		[self.taskCompleted setFrame:CGRectMake(25, 2, 40, 40)];
		[self.scribbleImageView setFrame:kSubListItemScribbleRect];
	}
}

- (void)setIndentForPreviewListItem:(ListItem *)previewListItem {
	if (previewListItem.parentListItemID == -1) {
		// Standard task
		[self.taskItem setFrame:CGRectMake(40, 12, 260, 21)];
		[self.taskCompleted setFrame:CGRectMake(2, 2, 40, 40)];
		[self.scribbleImageView setFrame:kListItemPreviewScribbleRect];
	} else {
		// Sub task
		[self.taskItem setFrame:CGRectMake(55, 12, 260, 21)];
		[self.taskCompleted setFrame:CGRectMake(17, 2, 40, 40)];
		[self.scribbleImageView setFrame:kSubListItemPreviewScribbleRect];
	}
}


- (UIImage *)getRandomHighlighterImageForColor:(NSInteger)theColor andSide:(NSInteger)theSide {
	//NSInteger randomNumber = (arc4random() % 5) + 1;
	NSInteger randomNumber = 1;

	
	if (theColor == EnumHighlighterColorRed) {
		if (theSide == EnumHighlightSideLeft) {
			NSString *imageName = [NSString stringWithFormat:@"RedHighLeft%d.png", randomNumber];
			return [UIImage imageNamed:imageName];
		} else if (theSide == EnumHighlightSideRight) {
			NSString *imageName = [NSString stringWithFormat:@"RedHighRight%d.png", randomNumber];
			return [UIImage imageNamed:imageName];
		}
		
	} else if (theColor == EnumHighlighterColorYellow) {
		if (theSide == EnumHighlightSideLeft) {
			NSString *imageName = [NSString stringWithFormat:@"YellowHighLeft%d.png", randomNumber];
			return [UIImage imageNamed:imageName];
		} else if (theSide == EnumHighlightSideRight) {
			NSString *imageName = [NSString stringWithFormat:@"YellowHighRight%d.png", randomNumber];
			return [UIImage imageNamed:imageName];
		}
	}
	
	return nil;
}

@end
