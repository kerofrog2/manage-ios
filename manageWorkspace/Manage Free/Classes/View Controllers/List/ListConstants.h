//
//  ListConstants.h
//  Manage
//
//  Created by Cliff Viegas on 4/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//
#ifdef IS_FREE_BUILD
// Import main constants
#import "MainConstants.h"
#endif
typedef enum {
	EnumHighlighterColorWhite = 0,
	EnumHighlighterColorYellow = 1,
	EnumHighlighterColorRed = 2,
	EnumHighlighterColorNone = 3
} EnumHighlighterColor;

#pragma mark -
#pragma mark Landscape & Portrait View Positions
/***********************************************/
#ifdef IS_FREE_BUILD
#define listAdHeight                        [[NSUserDefaults standardUserDefaults] integerForKey:kAdvertisingBuffer] // 66
#else
#define listAdHeight 						0
#endif

/****
 Backgrounds
 ****/
#define kPageImagePortrait					CGRectMake(43, 30, 684, 891 - listAdHeight)
#define kPageImageLandscape					CGRectMake(299, 30, 684, 635 - listAdHeight)

#define kBorderImagePortrait				CGRectMake(-256, 0, 1024, 960 - listAdHeight)
#define kBorderImageLandscape				CGRectMake(0, 0, 1024, 704 - listAdHeight)

#define kCanvassImagePortrait				CGRectMake(-5, 0, 776, 955 - listAdHeight)
#define kCanvassImageLandscape				CGRectMake(250, 0, 777, 698 - listAdHeight)

#define kListCompletedStampPortrait			CGRectMake(135, 280, 491, 306 - (listAdHeight / 2))
#define kListCompletedStampLandscape		CGRectMake(391, 150, 491, 306 - (listAdHeight / 2))

/****
 List Table View
 ****/
#define kListTableViewPortrait				CGRectMake(68, 85, 634, 705 - listAdHeight)
#define kListTableViewLandscape				CGRectMake(324, 85, 634, 445 - listAdHeight)

#define kFilterTagsListTableViewPortrait	CGRectMake(68, 129, 634, 661 - listAdHeight)
#define kFilterTagsListTableViewLandscape	CGRectMake(324, 129, 634, 401 - listAdHeight)

#define kHeaderLine1Portrait				CGRectMake(68, 84, 634, 1)
#define kHeaderLine1Landscape				CGRectMake(324, 84, 634, 1)

#define kHeaderLine2Portrait				CGRectMake(68, 82, 634, 1)
#define kHeaderLine2Landscape				CGRectMake(324, 82, 634, 1)



/****
 Search Objects
 ****/
#define kSearchBarPortrait					CGRectMake(110, 85, 247, 44)
#define kSearchBarLandscape					CGRectMake(366, 85, 247, 44)

#define kSearchScopeSegmentPortrait			CGRectMake(365, 91, 212, 32)
#define kSearchScopeSegmentLandscape		CGRectMake(621, 91, 212, 32)

#define kClearSearchButtonPortrait			CGRectMake(63, 84, 50, 44)
#define kClearSearchButtonLandscape			CGRectMake(319, 84, 50, 44)

#define kSearchDateRangeButtonPortrait      CGRectMake(577, 84, 50, 44)
#define kSearchDateRangeButtonLandscape     CGRectMake(833, 84, 50, 44)

#define kSearchListRangeButtonPortrait      CGRectMake(617, 84, 50, 44)
#define kSearchListRangeButtonLandscape     CGRectMake(873, 84, 50, 44)

#define kShowAllTasksButtonPortrait         CGRectMake(657, 84, 50, 44)
#define kShowAllTasksButtonLandscape        CGRectMake(913, 84, 50, 44)

#define kSearchTitleLabelPortrait			CGRectMake(225, 38, 318, 29)
#define kSearchTitleLabelLandscape			CGRectMake(456, 38, 368, 29)

#define kSearchDateRangeLabelPortrait       CGRectMake(225, 60, 318, 19)
#define kSearchDateRangeLabelLandscape      CGRectMake(456, 60, 368, 19)

#define kSearchButtonPortrait				CGRectMake(160, 39, 50, 44)
#define kSearchButtonLandscape				CGRectMake(415, 38, 50, 44)


/****
 Filter Tag Objects
 ****/

#define kFilterTagsFooterLinePortrait		CGRectMake(68, 128, 634, 1)
#define kFilterTagsFooterLineLandscape		CGRectMake(324, 128, 634, 1)

#define kFilterTagsLabelPortrait			CGRectMake(168, 90, 434, 30)
#define kFilterTagsLabelLandscape			CGRectMake(426, 90, 434, 30)

#define kFilterTagsImageViewPortrait		CGRectMake(351, 86, 36, 36)
#define kFilterTagsImageViewLandscape		CGRectMake(609, 86, 36, 36)

#define kFilterTagsClearButtonPortrait		CGRectMake(68, 84, 50, 44)
#define kFilterTagsClearButtonLandscape		CGRectMake(324, 84, 50, 44)

/****
 Text Fields
 ****/
#define kTitleTextFieldPortrait				CGRectMake(341, 816 - listAdHeight, 338, 43)
#define kTitleTextFieldEditingPortrait		CGRectMake(341, 616, 338, 43)

#define kTitleTextFieldLandscape			CGRectMake(597, 560 - listAdHeight, 338, 43)
#define kTitleTextFieldEditingLandscape		CGRectMake(597, 290, 338, 43)

#define kDateLabelPortrait					CGRectMake(481, 850 - listAdHeight, 198, 33)
#define kDateLabelLandscape					CGRectMake(737, 594 - listAdHeight, 198, 33)

/****
 Items Due Table View
 ****/
#define kItemsDueTableViewPortrait			CGRectMake(66, 806 - listAdHeight, 209, 121)
#define kItemsDueTableViewLandscape			CGRectMake(322, 550 - listAdHeight, 209, 121)

#define kItemsDuePocketPortrait				CGRectMake(50, 792 - listAdHeight, 241, 149)
#define kItemsDuePocketLandscape			CGRectMake(306, 536 - listAdHeight, 241, 149)

/****
 Buttons
 ****/

#define kEditListButtonPortrait				CGRectMake(70, 47, 48, 29)
#define kEditListButtonLandscape			CGRectMake(326, 46, 48, 29)

#define kNewListItemButtonPortrait			CGRectMake(660, 39, 50, 44)
#define kNewListItemButtonLandscape			CGRectMake(916, 38, 50, 44)

#define kNewScribbleListItemButtonPortrait	CGRectMake(617, 39, 50, 44)
#define kNewScribbleListItemButtonLandscape	CGRectMake(873, 38, 50, 44)

#define kDoneEditingButtonPortrait			CGRectMake(652, 46, 48, 29)
#define kDoneEditingButtonLandscape			CGRectMake(908, 46, 48, 29)

#define kSortItemsSegmentedControlPortrait	CGRectMake(222, 47, 312, 29)
#define kSortItemsSegmentedControlLandscape	CGRectMake(478, 46, 312, 29)

#define kPenButtonPortrait					CGRectMake(319, 865 - listAdHeight, 35, 30)
#define kPenButtonLandscape                 CGRectMake(575, 613 - listAdHeight, 35, 30)

#define kHighlighterButtonPortrait			CGRectMake(369, 865 - listAdHeight, 35, 30)
#define kHighlighterButtonLandscape         CGRectMake(625, 613 - listAdHeight, 35, 30)

// OCR Button
#define kNewOCRListItemButtonPortrait       CGRectMake(575, 39, 50, 44)
#define kNewOCRListItemButtonLandscape      CGRectMake(830, 38, 50, 44)

// #define kHighlighterButtonPortrait			CGRectMake(419, 865, 35, 30)

// Need to fit inbetween top two
//#define kListOptionsButtonPortrait			CGRectMake(369, 866, 35, 30)
#define kListOptionsButtonPortrait			CGRectMake(419, 865 - listAdHeight, 35, 30)
#define kListOptionsButtonLandscape			CGRectMake(672, 613 - listAdHeight, 35, 30)

#define kSelectTaskImageViewPortrait		CGRectMake(284, 400, 198, 40)
#define kSelectTaskImageViewLandscape		CGRectMake(540, 294, 198, 40)

#define kFilterTagsButtonPortrait			CGRectMake(119, 39, 50, 44)
#define kFilterTagsButtonLandscape			CGRectMake(374, 38, 50, 44)

/****
 Control Panel Objects
 ****/
// 215 * 636
//#define kControlPanelImagePortrait			CGRectMake(-236, 34, 215, 636)
//#define kControlPanelImageLandscape			CGRectMake(20, 34, 215, 636)

#define kControlPanelImagePortrait			CGRectMake(-245, 24, 224, 656 - listAdHeight)
#define kControlPanelImageLandscape			CGRectMake(14, 24, 224, 656 - listAdHeight)

#define kControlPanelTypeSegmentPortrait    CGRectMake(-232, 34, 204, 31)
#define kControlPanelTypeSegmentLandscape   CGRectMake(24, 34, 204, 31)

#define kSortItemsTableViewPortrait			CGRectMake(-252, 23, 220, 170 - listAdHeight)
#define kSortItemsTableViewLandscape		CGRectMake(16, 23, 220, 170 - listAdHeight)

#define kToolsContainerViewPortrait         CGRectMake(-240, 72, 220, 607 - listAdHeight)
#define kToolsContainerViewLandscape        CGRectMake(16, 72, 220, 607 - listAdHeight)

#define kNoteToolsContainerViewPortrait     CGRectMake(-240, 32, 220, 647 - listAdHeight)
#define kNoteToolsContainerViewLandscape    CGRectMake(16, 32, 220, 647 - listAdHeight)

#define kDrawingToolsContainerShow          CGRectMake(7, 0, 206, 607 - listAdHeight)
#define kDrawingToolsContainerHide          CGRectMake(-250, 0, 206, 607 - listAdHeight)

#define kCalendarListToolsContainerShow     CGRectMake(0, 0, 220, 607 - listAdHeight)
#define kCalendarListToolsContainerHide     CGRectMake(255, 0, 220, 607 - listAdHeight)

#define kMiniListPadTableViewFrame          CGRectMake(14, 230, 192, 377)
#define kNoteMiniListPadTableViewFrame      CGRectMake(14, 85, 192, 562)


#define kCalenderListsDividerFrame          CGRectMake(0, 220, 220, 13)

// -23, -72
#define	kHighlightersImageLandscape			CGRectMake(0, 94, 206, 192)
#define kHighlightersImagePortrait			CGRectMake(0, 94, 206, 192)

#define kPensImageLandscape					CGRectMake(2, 295, 204, 307 - listAdHeight)
#define kPensImagePortrait					CGRectMake(2, 295, 204, 307 - listAdHeight)

#define kDayCalendarImageViewFrame          CGRectMake(3.5, 0, 198, 90)
#define kNoteDayCalendarImageViewFrame      CGRectMake(10, 0, 198, 90)

#define kDayCalendarLabelFrame              CGRectMake(15, 35, 176, 45)
#define kNoteDayCalendarLabelFrame          CGRectMake(22, 35, 176, 45)

#define kMiniCalendarViewFrame              CGRectMake(3, 0, 210, 230)


/****
 Draw Pad Constants
 ****/
#define kWidthPreview                       150
#define kWidthScrollPad                     600
#define kHeightScrollPad                    132
#define kWidthDrawPad                       1500

#define kDrawPadViewControllerSize			CGSizeMake(600, 239)
#define kDrawPadScrollViewRect				CGRectMake(0, 50, 600, 132)
#define kDrawPad2ScrollViewRect				CGRectMake(0, 50, 120, 132)
#define kDrawPadScrollViewContentSize		CGSizeMake(1500, 132)
#define kDrawPadImageViewRect				CGRectMake(0, 0, 1500, 132)

// 10 times smaller than 1500 * 132
#define kScrollControllWindowFrame          CGRectMake(440, 193, 150, 35)

/// PNG Draw Pad Constants
#define kPngDrawPadViewControllerSize       CGSizeMake(614, 242) //192)
#define kPngDrawPadScrollViewRect			CGRectMake(0, 10, 614, 132)

// 614 * 132 (should make it 600)

/****
 Control Panel Button Objects
 ****/
#define kGreenPenButtonPortrait				CGRectMake(9, 306, 43, 289 - listAdHeight)
#define kGreenPenButtonLandscape			CGRectMake(9, 306, 43, 289 - listAdHeight)

#define kRedPenButtonPortrait				CGRectMake(60, 306, 43, 289 - listAdHeight)
#define kRedPenButtonLandscape				CGRectMake(60, 306, 43, 289 - listAdHeight)

#define kBluePenButtonPortrait				CGRectMake(108, 306, 43, 289 - listAdHeight)
#define kBluePenButtonLandscape				CGRectMake(108, 306, 43, 289 - listAdHeight)

#define kBlackPenButtonPortrait				CGRectMake(156, 306, 43, 289 - listAdHeight)
#define kBlackPenButtonLandscape			CGRectMake(156, 306, 43, 289 - listAdHeight)

#define kRedHighlighterButtonPortrait		CGRectMake(7, 106, 192, 48)
#define kRedHighlighterButtonLandscape		CGRectMake(7, 106, 192, 48)

#define kYellowHighlighterButtonPortrait	CGRectMake(7, 165, 192, 48)
#define kYellowHighlighterButtonLandscape	CGRectMake(7, 165, 192, 48)

#define kWhiteHighlighterButtonPortrait		CGRectMake(7, 228, 192, 48)
#define kWhiteHighlighterButtonLandscape	CGRectMake(7, 228, 192, 48)

/****
 PopOver Sizes
 ****/
#define kEditListItemViewControllerSize		CGSizeMake(320, 318)
#define kEditSubListItemViewControllerSize	CGSizeMake(320, 318)

#define kNewListItemViewControllerSize		CGSizeMake(320, 318)

#define kNewSubListItemViewControllerSize	CGSizeMake(320, 318)


#define kHighlightersPopoverSize			CGSizeMake(160, 60)

/****
 Todo List Scribble
 ****/
#define kListItemScribbleRect				CGRectMake(33, 0, 500, 44)
#define kListItemPreviewScribbleRect		CGRectMake(33, 0, 300, 44)
#define kOutsideListItemScribbleRect        CGRectMake(100, 0, 500, 44)

#define kSubListItemScribbleRect			CGRectMake(55, 0, 500, 44)
#define kSubListItemPreviewScribbleRect		CGRectMake(47, 0, 300, 44)
