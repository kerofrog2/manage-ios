//
//  ToDoTableViewCell.h
//  Manage
//
//  Created by Cliff Viegas on 5/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Constants
#import "ListConstants.h"

// Fast table view cell
#import "FastTableViewCell.h"

// GLDraw
#import "GLDrawES2ViewController.h"

#import "MainConstants.h"

// Tags View and Task Date frames
#define kTagsViewFrame					CGRectMake(280, 11, 345, 20)
#define kTagsViewFrameArchive			CGRectMake(250, 11, 345, 20)
#define kTagsViewFrameArchiveiOS7		CGRectMake(230, 11, 345, 20)
#define kShiftedTagsViewFrame			CGRectMake(280, 19, 345, 20)
#define kPreviewTagsViewFrame			CGRectMake(115, 11, 265, 20)
#define kPreviewShiftedTagsViewFrame	CGRectMake(115, 19, 265, 20)

#define kTaskDateFrame					CGRectMake(495, 14, 125, 20)
#define kShiftedTaskDateFrame			CGRectMake(495, 2, 125, 20)
#define kPreviewTaskDateFrame			CGRectMake(250, 14, 125, 20)
#define kPreviewShiftedTaskDateFrame	CGRectMake(250, 2, 125, 20)

// Task Item frames
#define kTaskItemFrame					CGRectMake(40, 13, 555, 21)
#define kSubTaskItemFrame				CGRectMake(63, 13, 532, 21)

#define kPreviewTaskItemFrame			CGRectMake(40, 12, 310, 21)
#define kPreviewSubTaskItemFrame		CGRectMake(55, 12, 295, 21)

// Task completed frames
#define kTaskCompletedFrame				CGRectMake(2, 2, 40, 40)
#define kPreviewTaskCompletedFrame		CGRectMake(2, 1, 40, 40)
#define kSubTaskCompletedFrame			CGRectMake(25, 2, 40, 40)
#define kPreviewSubTaskCompletedFrame	CGRectMake(17, 1, 40, 40)


// Repeating frame
#define kTaskRepeatingFrame (IS_RETINA) ? CGRectMake(603, 4, 36, 36) : CGRectMake(603, 4, 20, 20)
#define kPreviewTaskRepeatingFrame (IS_RETINA) ? CGRectMake(358, 4, 36, 36) : CGRectMake(358, 4, 20, 20)

// Repeating frame middle
#define kTaskRepeatingMiddleFrame (IS_RETINA) ? CGRectMake(603, 12, 36, 36) : CGRectMake(603, 12, 20, 20)
#define kPreviewTaskRepeatingMiddleFrame (IS_RETINA) ? CGRectMake(358, 12, 36, 36) : CGRectMake(358, 12, 20, 20)

// Alarm frame
#define kTaskAlarmFrame (IS_RETINA) ? CGRectMake(603, 20, 36, 36) : CGRectMake(603, 20, 20, 20)
#define kPreviewTaskAlarmFrame (IS_RETINA) ? CGRectMake(358, 20, 36, 36) : CGRectMake(358, 20, 20, 20)

// Alarm frame middle
#define kTaskAlarmMiddleFrame (IS_RETINA) ? CGRectMake(603, 12, 36, 36) : CGRectMake(603, 12, 20, 20)
#define kPreviewTaskAlarmMiddleFrame (IS_RETINA) ? CGRectMake(358, 12, 36, 36) : CGRectMake(358, 12, 20, 20)

// Note frame
#define kNoteFrame (IS_RETINA) ? CGRectMake(0, 0, 36, 36) : CGRectMake(0, 0, 20, 20)
#define kPreviewNoteFrame (IS_RETINA) ? CGRectMake(0, 0, 36, 36) : CGRectMake(0, 0, 20, 20)


// Data Collections
@class TagCollection;

// Data Models
@class ListItem;

typedef enum {
	EnumHighlightSideLeft = 0,
	EnumHighlightSideRight = 1
} EnumHighlightSide;

@protocol ListItemDelegate
@optional
- (void)taskCompleted:(BOOL)isCompleted forListItemID:(NSInteger)theListItemID;
@end

@interface ListItemTableViewCell : FastTableViewCell {
	id <ListItemDelegate>	delegate;
	
	NSString				*taskItem;
	NSString				*taskDate;
	UIButton				*taskCompleted;
	UIImageView				*scribbleImageView;
	UIImageView				*repeatingTaskImageView;
    UIImageView				*alarmTaskImageView;
	
    // Highlighters
    UIView					*highlighterView;
	UIImageView				*rightHighlighterImageView;
	UIImageView				*leftHighlighterImageView;
	
	// Tags View
	UIView					*tagsView;
	UIFont					*tagsFont;
	
	
	
	// Updated properties
	UIImageView				*cellNotesImageView;
	
	// Frames
	CGRect					taskItemFrame;
	CGRect					taskCompletedFrame;
	CGRect					taskDateFrame;
	CGRect					leftHighlighterFrame;
	CGRect					rightHighlighterFrame;
	CGRect					repeatingTaskTagsViewFrame;
	
	// Current cell type properties
	BOOL					isPreview;
	BOOL					isSubtask;
	BOOL					isNonListSort;
	BOOL					hasTags;
	BOOL					isArchived;
    
  //  GLDrawES2ViewController *drawingLayer;
    ListItem                *refListItem;
    
    NSInteger               priority;
}

@property (nonatomic, assign)	id				delegate;

@property (nonatomic, retain)	NSString		*taskItem;
@property (nonatomic, retain)	NSString		*taskDate;
@property (nonatomic, retain)	UIButton		*taskCompleted;
@property (nonatomic, retain)	UIImageView		*scribbleImageView;
@property (nonatomic, retain)	UIImageView		*repeatingTaskImageView;
@property (nonatomic, retain)	UIImageView		*alarmTaskImageView;
@property (nonatomic, retain)	UIView			*highlighterView;
@property (nonatomic, retain)	UIImageView		*leftHighlighterImageView;
@property (nonatomic, retain)	UIImageView		*rightHighlighterImageView;

@property (nonatomic, retain)	UIView			*tagsView;
@property (nonatomic, retain)	UIFont			*tagsFont;

// Updated properties
@property (nonatomic, retain)	UIImageView		*cellNotesImageView;
@property (nonatomic, assign)	CGRect			taskItemFrame;

@property (nonatomic, assign)	BOOL			isPreview;
@property (nonatomic, assign)	BOOL			isSubtask;
@property (nonatomic, assign)	BOOL			isNonListSort;
@property (nonatomic, assign)	BOOL			hasTags;
@property (nonatomic, assign)	BOOL			isArchived;

@property (nonatomic, assign)   NSInteger		priority;

//@property (nonatomic, retain) GLDrawES2ViewController   *drawingLayer;

- (id)initWithPreviewStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

- (void)loadDrawingLayerForListItem:(ListItem *)listItem;
- (void)loadGLDrawingLayerForListItem:(ListItem *)listItem;
- (void)showDrawingFrame;

#pragma mark Class Methods
- (void)loadTagsForListItem:(ListItem *)listItem andTagCollection:(TagCollection *)tagCollection;
- (void)setHighlightStyle:(EnumHighlighterColor)highlighter;
- (void)setIndentLevel:(NSInteger)indent;

#pragma mark Scribble Image Methods
- (void)loadScribbleImageForListItem:(ListItem *)listItem andIsPreviewList:(BOOL)isPreviewList;

#pragma mark Cell Notes Icon Methods
- (void)loadCellNotesImage;

#pragma mark Repeating Task Image Methods
- (void)loadRepeatingTaskImageForListItem:(ListItem *)listItem;

#pragma mark Alarm Task Image Methods
- (void)loadAlarmTaskImage:(NSString *) alarmImage andListItem:(ListItem *)listItem;

#pragma mark Class Helper Methods
- (void)setStandardIndent;
- (void)setIndentForListItem:(ListItem *)listItem;
- (void)setIndentForPreviewListItem:(ListItem *)previewListItem;

@end
