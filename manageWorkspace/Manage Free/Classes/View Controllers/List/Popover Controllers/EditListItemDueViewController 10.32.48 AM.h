//
//  EditListItemDueViewController.h
//  Manage
//
//  Created by Cliff Viegas on 15/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Super Class
#import "EditListItemViewController.h"

@interface EditListItemDueViewController : EditListItemViewController {

}

@end
