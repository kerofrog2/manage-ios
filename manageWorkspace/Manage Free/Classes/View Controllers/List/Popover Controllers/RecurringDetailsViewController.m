//
//  RecurringDetailsViewController.m
//  Manage
//
//  Created by Cliff Viegas on 9/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "RecurringDetailsViewController.h"

// Data Models
#import "ListItem.h"
#import "RecurringListItem.h"

// View Controllers
#import "EveryXTRepeatScheduleViewController.h"
#import "DaysOfTheWeekViewController.h"
#import "XDOfEachMonthRepeatScheduleViewController.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#define kViewControllerSize					CGSizeMake(320, 318)
#define kTableViewFrame						CGRectMake(0, 0, 320, 340)

@interface RecurringDetailsViewController()
// Private methods
- (void)loadBarButtonItems;
- (void)loadNavigationBar;
- (void)loadTableView;

- (void)openEveryXDayOfTheMonthViewController;
- (void)openCustomRepeatScheduleController;
- (void)openDaysOfTheWeekViewController;
@end

@implementation RecurringDetailsViewController

@synthesize delegate;
@synthesize refPopoverController;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[myTableView release];
	[repeatOptionsArray release];
	[recurringListItem release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithListItem:(ListItem *)theListItem {
	if (self = [super init]) {
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Set the title
		self.title = @"Repeat Task";
		
		// Assign the passed list item
		refListItem = theListItem;
		
		// Init recurring list item
		recurringListItem = nil;
		
		// Check to see whether this list item already has a repeating schedule?
		if ([refListItem recurringListItemID] != -1) {
			// Most likely a repeating schedule involved, get the corresponding values
			if ([refListItem isLinkedToRecurringListItem]) {
				// Load the recurring list item
				recurringListItem = [[RecurringListItem alloc] initWithRecurringListItemID:refListItem.recurringListItemID];
			}
		}
		
		// Init recurring list item if it is nil
		if (recurringListItem == nil) {
			recurringListItem = [[RecurringListItem alloc] init];
		}
		
		// Load the repeat options array
		repeatOptionsArray = [[NSMutableArray alloc] initWithObjects:@"None", @"Daily", @"Weekly", @"Every 2 Weeks", @"Every (?) Days/Weeks/Months", @"On days of the week", @"On the x day of each month", nil];	
	//WithObjects:@"None", @"Daily", @"Weekly", @"Every 2 Weeks", @"Every Month", @"Every [?] Days/Weeks/Months", "On [?] day of each month", @"On days of the week", nil];
		
		[self loadBarButtonItems];
		//[self loadNavigationBar];
		
		[self loadTableView];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadBarButtonItems {
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
										   initWithBarButtonSystemItem:UIBarButtonSystemItemDone
										   target:self
										   action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]
											 initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
											 target:self 
											 action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
	[cancelBarButtonItem release];
}

- (void)loadNavigationBar {
	UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
	//[navBar setTintColor:[UIColor darkGrayColor]];
	UINavigationItem *navItem = [[[UINavigationItem alloc] initWithTitle:@"Repeat Task"] autorelease];
	
	// Add the right bar button item
	navItem.rightBarButtonItem = [[[UIBarButtonItem alloc]
								   initWithBarButtonSystemItem:UIBarButtonSystemItemDone
								   target:self
								   action:@selector(doneBarButtonItemAction)] autorelease];
	
	// Add the left bar button item
	navItem.leftBarButtonItem = [[[UIBarButtonItem alloc]
								  initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
								  target:self 
								  action:@selector(cancelBarButtonItemAction)] autorelease];
	
	[navBar pushNavigationItem:navItem animated:NO];
	
	[self.view addSubview:navBar];
	[navBar release];
}

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDataSource:self];
	[myTableView setDelegate:self];
	[self.view addSubview:myTableView];
}

- (void)loadRecurringListItem {
	
}

#pragma mark -
#pragma mark Class Methods



#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	// Need to check if this was an existing recurring list item that has been set to 'none'
	if ([recurringListItem frequency] == 0 && [[recurringListItem frequencyMeasurement] isEqualToString:@""]
		&& [[recurringListItem daysOfTheWeek] isEqualToString:@""]) {
		
		// Check if this was newly created or an existing list item
		if (recurringListItem.recurringListItemID == -1) {
			// Newly created, do nothing
			[self.navigationController popViewControllerAnimated:YES];
			return;
		} else if (recurringListItem.recurringListItemID != -1) {
			// Existing, also have to delete from table
			[recurringListItem deleteSelfFromDatabase];
			[self.delegate didUpdateWithRecurringListItemID:-1];
			[self.navigationController popViewControllerAnimated:YES];
			return;
		}
	}
	
	// Update the database with the recurring list item
	if (recurringListItem.recurringListItemID != -1) {
		[recurringListItem updateSelfInDatabase];
	} else if (recurringListItem.recurringListItemID == -1) {
		NSInteger newRecurringListItemID = [recurringListItem insertSelfIntoDatabase];
		recurringListItem.recurringListItemID = newRecurringListItemID;
	}
	
	// Need delegate to handle this
	// Tell edit view controller that repeating settings have been updated
	[self.delegate didUpdateWithRecurringListItemID:recurringListItem.recurringListItemID];
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
	// Need delegate to dismiss
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UITableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	if (section == 1) {
		return [repeatOptionsArray count];
	}
	return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}	
	
	// Default options
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.textLabel.text = @"";
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// From completed field
	if (indexPath.section == 0) {
		if (indexPath.row == 0) {
			cell.textLabel.text = @"Completion Date";
			if ([recurringListItem useCompletedDate] == TRUE) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
		} else if (indexPath.row == 1) {
			cell.textLabel.text = @"Due Date";
			if ([recurringListItem useCompletedDate] == FALSE) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
		}
		return cell;
		
	}
	
	//:@"None", @"Daily", @"Weekly", @"Every 2 Weeks", @"Every Month", @"Every (?) Days/Weeks/Months", @"On (?) day of each month", @"On days of the week"
	
	switch (indexPath.row) {
		case 0:
			// None
			if (recurringListItem.frequency == 0 && [recurringListItem.frequencyMeasurement isEqualToString:@""] && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = [repeatOptionsArray objectAtIndex:indexPath.row];
			break;
		case 1:
			// Daily
			if (recurringListItem.frequency == 1 && [recurringListItem.frequencyMeasurement isEqualToString:@""] && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = [repeatOptionsArray objectAtIndex:indexPath.row];
			break;
		case 2:
			// Weekly
			if (recurringListItem.frequency == 7 && [recurringListItem.frequencyMeasurement isEqualToString:@""] && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = [repeatOptionsArray objectAtIndex:indexPath.row];
			break;
		case 3:
			// Every 2 weeks
			if (recurringListItem.frequency == 14 && [recurringListItem.frequencyMeasurement isEqualToString:@""] && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			}
			cell.textLabel.text = [repeatOptionsArray objectAtIndex:indexPath.row];
			break;
		case 4:
			// User defined, every weeks/days/months
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
			if ([recurringListItem.frequencyMeasurement isEqualToString:@""] == FALSE && [recurringListItem.daysOfTheWeek isEqualToString:@""]) {
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
				if (recurringListItem.frequency == 1) {
					cell.textLabel.text = [NSString stringWithFormat:@"Every %@", [recurringListItem.frequencyMeasurement stringByReplacingOccurrencesOfString:@"s" withString:@""]];
				} else {
					cell.textLabel.text = [NSString stringWithFormat:@"Every %d %@", recurringListItem.frequency, recurringListItem.frequencyMeasurement];
				}
			} else {
				cell.textLabel.text = [repeatOptionsArray objectAtIndex:indexPath.row];
			}
			break;
		case 5:
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
			if ([recurringListItem.daysOfTheWeek isEqualToString:@""] == FALSE
					   && [recurringListItem.frequencyMeasurement isEqualToString:@""] == TRUE) {
				// Need to recover the days of the week
				if ([recurringListItem.daysOfTheWeek isEqualToString:@"Saturday, Sunday"]) {
					cell.textLabel.text = @"Every Weekend";
				} else if ([recurringListItem.daysOfTheWeek isEqualToString:@"Monday, Tuesday, Wednesday, Thursday, Friday"]) {
					cell.textLabel.text = @"Every Weekday";
				} else if ([recurringListItem.daysOfTheWeek isEqualToString:@"Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday"]) {
					cell.textLabel.text = @"Every Day";
				} else {
					cell.textLabel.text = [NSString stringWithFormat:@"Every %@", recurringListItem.daysOfTheWeek];
				}
				
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
				//NSArray *days = [recurringListItem.daysOfTheWeek componentsSeparatedByString:@","];
			} else {
				//if ([recurringListItem.daysOfTheWeek isEqualToString:@""]) {
				cell.textLabel.text = [repeatOptionsArray objectAtIndex:indexPath.row];
			}

			break;
		case 6:
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
			if ([recurringListItem frequency] == 0 && [recurringListItem.frequencyMeasurement length] > 0 
				&& [recurringListItem.daysOfTheWeek length] > 0) {
				cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ of each month",
									   recurringListItem.frequencyMeasurement,
									   recurringListItem.daysOfTheWeek];
				cell.accessoryType = UITableViewCellAccessoryCheckmark;
			} else {
				cell.textLabel.text = [repeatOptionsArray objectAtIndex:indexPath.row];
			}

			
			break;

		default:
			break;
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Due date selected
	if (indexPath.section == 0) {
		if (indexPath.row == 0) {
			recurringListItem.useCompletedDate = TRUE;
		} else if (indexPath.row == 1) {
			recurringListItem.useCompletedDate = FALSE;
		}
		[myTableView reloadData];
		return;
	}
	
	// Otherwise something else was selected
	switch (indexPath.row) {
		case 0:
			// None
			recurringListItem.frequency = 0;
			recurringListItem.frequencyMeasurement = @"";
			recurringListItem.daysOfTheWeek = @"";
			break;
		case 1:
			// Daily
			recurringListItem.frequency = 1;
			recurringListItem.frequencyMeasurement = @"";
			recurringListItem.daysOfTheWeek = @"";
			break;
		case 2:
			// Weekly
			recurringListItem.frequency = 7;
			recurringListItem.frequencyMeasurement = @"";
			recurringListItem.daysOfTheWeek = @"";
			break;
		case 3:
			// Every 2 weeks
			recurringListItem.frequency = 14;
			recurringListItem.frequencyMeasurement = @"";
			recurringListItem.daysOfTheWeek = @"";
			break;
		case 4:
			// User defined, every weeks/days/months
			// Need a new view controller to select this
			recurringListItem.daysOfTheWeek = @"";
			[self openCustomRepeatScheduleController];
			[tableView deselectRowAtIndexPath:indexPath animated:YES];
			return;
			break;
		case 5:
			[self openDaysOfTheWeekViewController];
			[tableView deselectRowAtIndexPath:indexPath animated:YES];
			return;
			break;
		case 6:
			[self openEveryXDayOfTheMonthViewController];
			[tableView deselectRowAtIndexPath:indexPath animated:YES];
			return;
			break;
		default:
			break;
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[tableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (section == 1) {
		return @"Frequency";
	}
	
	return @"Start repeating from";
}

#pragma mark -
#pragma mark Open Modal Controllers Methods

- (void)openEveryXDayOfTheMonthViewController {
	XDOfEachMonthRepeatScheduleViewController *controller = [[[XDOfEachMonthRepeatScheduleViewController alloc] initWithRecurringListItem:recurringListItem] autorelease];
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)openDaysOfTheWeekViewController {
	DaysOfTheWeekViewController *controller = [[[DaysOfTheWeekViewController alloc] initWithRecurringListItem:recurringListItem] autorelease];
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)openCustomRepeatScheduleController {
	EveryXTRepeatScheduleViewController *controller = [[[EveryXTRepeatScheduleViewController alloc] initWithRecurringListItem:recurringListItem] autorelease];
	//controller.modalPresentationStyle = UIModalPresentationCurrentContext;
	[self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark UIViewController Delegates

- (void)viewWillAppear:(BOOL)animated {
	[myTableView reloadData];
	
	[super viewWillAppear:animated];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
