//
//  ChooseTimeViewController.h
//  Manage
//
//  Created by Cliff Viegas on 6/6/13.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AlertTimeViewController.h"
#import "SoundsViewController.h"
#import "ListItem.h"

@protocol ChooseTimeViewControllerDelegate
- (void)updatedCurrentTimeString:(NSString *)timeString repeat:(AlertTime *)repeatTime sound:(Sound *)sound andSnooze:(BOOL)snooze;
@end

@interface ChooseTimeViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, AlertTimeViewControllerDelegate, SoundsViewControllerDelegate, UIPickerViewDelegate> {

    id <ChooseTimeViewControllerDelegate>   delegate;
    UIDatePicker			*datePicker;
    UIPopoverController		*refPopoverController;	// Reference to the popover controller
    NSDate                  *currentDate;
    
    UITableView                 *myTableView;
    UISwitch                    *mySwitch;
    
    AlertTime       *alertTime;
    Sound           *sound;
    BOOL            snooze;
}

@property (nonatomic, assign)	UIPopoverController		*refPopoverController;
@property (nonatomic, assign)		id			delegate;
@property (nonatomic, retain)   AlertTime   *alertTime;
@property (nonatomic, retain)   Sound   *sound;
@property (nonatomic, assign)   BOOL    snooze;

- (id)initWithCurrentTime:(NSDate *)currentTime;

@end
