//
//  AlertTimeViewController.m
//  Manage
//
//  Created by Cliff Viegas on 6/6/13.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "AlertTimeViewController.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 370)
#define kPopoverSize		CGSizeMake(320, 318)

@implementation AlertTimeViewController


@synthesize delegate;
@synthesize currentAlertTime;
@synthesize timeStringArray;

#pragma mark - Memory Management

- (void)dealloc {
    [myTableView release];
    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithDelegate:(id<AlertTimeViewControllerDelegate>)theDelegate withNumberOfMinuteForAlert:(AlertTime*)currentNumberOfMinuteForAlert {
    if (self = [super init]) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_AlertTimeViewController;
#endif
        // Set the title
		self.title = @"Repeat";
        self.delegate = theDelegate;
        self.currentAlertTime = currentNumberOfMinuteForAlert;
        [self setContentSizeForViewInPopover:kPopoverSize];
        
        [self loadDataSourceForTableView];

        [self loadTableView];
    }
    return self;
}


#pragma mark - Load Methods

- (void)loadDataSourceForTableView {
  
    self.timeStringArray = [AlertTime createDataSource];
}

- (void)loadTableView {
    myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark - UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.timeStringArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
	
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    NSInteger row = indexPath.row;
    
    AlertTime *alertTime = [self.timeStringArray objectAtIndex:row];
    cell.textLabel.text = alertTime.text;

    if (alertTime.text == currentAlertTime.text) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSInteger row = indexPath.row;
    UITableViewCell *cell = [myTableView cellForRowAtIndexPath:indexPath];
    AlertTime *alertTime = [self.timeStringArray objectAtIndex:row];
    alertTime.isSelected = YES;
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    
    
    [self.delegate updatedCurrentNumberOfMinuteForAlert:alertTime];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

@end
