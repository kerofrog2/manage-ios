//
//  EditListItemViewController.h
//  Manage
//
//  Created by Cliff Viegas on 5/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Models
@class ListItem;
@class RecurringListItem;
@class AlertTime;

// Data Collections
@class TagCollection;
@class ListItemTagCollection;
@class TaskListCollection;
@class AlarmCollection;

// View Controllers (for delegate)
#import "NotesViewController.h"
#import "MoveItemsViewController.h"
#import "RecurringDetailsViewController.h"
#import "PriorityViewController.h"
#import "ChooseTimeViewController.h"
#import "AlertTimeViewController.h"
#import "TaskList.h"
#import "AlarmsViewController.h"

// Custom objects
@class HiddenToolbar;
@class CustomEdgeInsetsTextView;

// Tags for retrieving content views
// In header file to allow inherited classes to access
#define	kTextViewTag				5384
#define kPopoverSize				CGSizeMake(320, 318)
#define kTextViewHeightBuffer       11

#define kLabelTag					6098
#define kMaximumNotesHeight			300
#define kNotesLabelWidth			240
//	[myTextView setFrame:CGRectMake(28, 4, 268, 73)];
#define kTitleTextViewWidth         268


@protocol EditListItemDelegate
// No need, we have the link to the list item, this just needs to update table
- (void)listItem:(ListItem *)listItem wasUpdated:(BOOL)updated completedStatusChanged:(BOOL)completeChanged;
- (void)listItemDue:(ListItem *)listItem wasUpdated:(BOOL)updated completedStatusChanged:(BOOL)completeChanged;
- (void)dismissEditListItemPopOver;
- (void)editListItemAtBaseLevel:(BOOL)isBaseLevel;
- (void)listItemWasMoved;
- (void)newHandwrittenSubtaskCreatedForParentListItem:(ListItem *)parentListItem;
- (void)editScribbleForListItem:(ListItem *)listItem usingRect:(CGRect)rect;
@end


@interface EditListItemViewController : GAITrackedViewController <PriorityDelegate, UIActionSheetDelegate, RecurringDetailsDelegate, NotesDelegate,ChooseTimeViewControllerDelegate, MoveItemsDelegate, AlarmsViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate> {
	id <EditListItemDelegate> delegate;
	
	ListItem				*refListItem;					// Passed ListItem for editing

	RecurringListItem		*recurringListItem;				// In case the list item is set to repeat
	
	TaskListCollection		*refTaskListCollection;			// For passing onto Move View Controller
    
	AlarmCollection			*alarmCollection;				// For loading any alarms for this list item
	
	UITableView				*myTableView;					// The local table view
	
	UIBarButtonItem			*repeatBarButtonItem;			// Button for repeating
	UIBarButtonItem			*repeatActiveBarButtonItem;		// Active version of repeat button
	UIBarButtonItem			*alarmBarButtonItem;			// Button for setting up alarm
	UIBarButtonItem			*alarmActiveBarButtonItem;		// Active version of alarm button
	
	NSString				*originalListItemTitle;			// Original list item title
	NSString				*originalListItemNotes;			// Original list item notes
	BOOL					originalCompleted;				// The original completed status for the list item
	BOOL					isFilterTagsMode;				// If we are currently in filter tags mode
	NSString				*listItemTitle;					// The current list item title
	NSString				*listItemNotes;					// The current list item notes
	BOOL					boxIsTicked;
	BOOL					editListItemWillExit;
	BOOL					wasListItemMoved;				// To not do a double update if list item was moved
	BOOL					isArchivesMode;					// If we are currently in archives
	BOOL					needDBUpdate;					// To check whether or not a db update is required
	BOOL					repeatingTaskWasUpdated;		// To check whether repeating task was updated or not
    BOOL                    isDueDateViewController;        // To check whether this was called from pocket reminder or not
    BOOL					alarmTaskWasUpdated;
	
    UIActionSheet           *newSubtaskActionSheet;
    
	NSString				*originalDueDate;
	
    NSDate                  *currentDateTime;
    AlertTime               *currentAlertTime;
    Sound                   *currentSound;
    BOOL                    currrentSnooze;
    NSString                *currentDueTime;
	
	// Reference to the pop over controller
	UIPopoverController		*refPopoverController;
	
	// Reference to the the tag collection
	TagCollection			*refTagCollection;
	BOOL					didAppearFromListView;
	BOOL					didAppearFromMoveItemView;
	
	HiddenToolbar			*topRightToolbar;				// Used for holding repeat and alarm buttons
    HiddenToolbar           *topLeftToolbar;
	
	//  Need a list items tag collection
	ListItemTagCollection	*listItemTagCollection;
    
    CGRect                  popoverRect;
    
    TaskList                *taskList;
}

@property (nonatomic, assign)		id          delegate;
@property (nonatomic, assign)		BOOL        isArchivesMode;
@property (nonatomic, assign)       CGRect      popoverRect;
@property (nonatomic, assign)       BOOL        isDueDateViewController;
@property (nonatomic, retain)       NSDate      *currentDateTime;
@property (nonatomic, retain)       AlertTime   *currentAlertTime;
@property (nonatomic, retain)       Sound       *currentSound;
@property (nonatomic, assign)       BOOL        currrentSnooze;
@property (nonatomic, copy)         NSString    *currentDueTime;

#pragma mark Initialisation
- (id)initWithDelegate:(id<EditListItemDelegate>)theDelegate andSize:(CGSize)theSize andListItem:(ListItem *)theListItem 
	  andTagCollection:(TagCollection *)theTagCollection andIsFilterTagsMode:(BOOL)filterTagsMode
 andTaskListCollection:(TaskListCollection *)theTaskListCollection andTaskList:(TaskList *)theTaskList didAppearFromListView:(BOOL)listViewAppear;

#pragma mark Old Private Methods
// No longer private as a class will be inheriting from this class
- (void)loadButtons;
- (void)loadTableView;
- (CustomEdgeInsetsTextView *)getTextView;
- (UILabel *)getLabel;
- (NSString *)getRepeatingHeaderTitle;
- (void)updateTopRightToolbarWithAlarmStatus:(BOOL)alarmStatus andRepeatStatus:(BOOL)repeatStatus;

#pragma mark Public Set Methods
- (void)setPopoverController:(UIPopoverController *)thePopoverController;

@end
