//
//  DrawPadViewController.h
//  Manage
//
//  Created by Cliff Viegas on 14/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLDrawES2ViewController.h"

// Data Models
@class ListItem;

// Data Collections
@class DrawPointCollection;

@protocol DrawPadDelegate
- (void)dismissDrawPadPopOver:(BOOL)newScribbleItem;
- (void)drawPadUpdatedForListItem:(ListItem *)listItem;
@end

typedef enum {
	EnumPenColorBlack = 0,
	EnumPenColorBlue = 1,
	EnumPenColorRed = 2,
	EnumPenColorGreen = 3,
	EnumPenColorEraser = 4,
	EnumPenColorNone = 5
} EnumPenColor;

@interface DrawPadViewController : GAITrackedViewController <UIActionSheetDelegate, UIScrollViewDelegate, GLDrawViewDelegate> {
	id <DrawPadDelegate>	delegate;
	ListItem				*refListItem;
	
	// Scroll view objects
	UIScrollView			*drawPadScrollView;
    // Scroll view 2 objects
	UIScrollView			*drawPadScrollView2;
	
	// Buttons
	UIButton				*bluePenButton;
	UIButton				*greenPenButton;
	UIButton				*redPenButton;
	UIButton				*blackPenButton;
	UIButton				*dragButton;
    UIButton                *undoButton;
    UIButton                *redoButton;
	UIButton				*eraserButton;
    UIButton                *clearButton;
	
	// Drawing objects
	CGPoint					lastPoint;
	BOOL					mouseSwiped;
	BOOL					lockDrawing;				// If the user is currently in the settings view, used to prevent touches
	BOOL					imageHasChanged;			// If the image has changed since the view was entered
	BOOL					isEditMode;				// editMode == NO when the scribble is to be added, YES if it is being edited
	NSInteger				mouseMoved;
	EnumPenColor			penColor;
    UISegmentedControl      *lineWidthsSegmentedControl;
    NSDictionary            *glScribbleDictionary;
	
	// Drawing curve objects
    BOOL                    isNewScribbleItem;          // For handling whether this is a scribble that was just added, or an existing scribble
    BOOL                    isPNGScribble;              // For handling whether this is an old png scribble or not
    
    GLDrawES2ViewController *drawingLayer;
    
    NSDictionary            *dataStorage;
    
    CGSize                  scribblePadContentSize;
    
    // Sub scrolling control view
    UIImageView             *scrollControlWindow;
    UIImageView             *scrollControl;
    BOOL                    scrollControlDragging;
    CGFloat                 scrollOffset;
    UIActionSheet           *clearActionSheet;
    
    
    
    
}

@property (nonatomic, assign)	id			delegate;
@property (nonatomic, assign)   BOOL        isNewScribbleItem;
@property (nonatomic, assign)   BOOL        isPNGScribble;
@property (nonatomic, retain)   GLDrawES2ViewController *drawingLayer;
@property (nonatomic, retain)   GLDrawES2ViewController *drawingLayer2;
@property (nonatomic, retain)   UIView *grayView2; // gray background for layer 2
@property (retain)              NSDictionary    *dataStorage;
@property (retain)              NSDictionary    *glScribbleDictionary;
@property (nonatomic, assign)   CGFloat     xPositionOfAutoScroll;
@property (nonatomic, assign)   vertex     newPosition;
@property (nonatomic, retain) IBOutlet UIButton *btnScroll;

#pragma mark Initialisation
- (id)initWithDelegate:(id<DrawPadDelegate>)theDelegate andSize:(CGSize)theSize 
		   andListItem:(ListItem *)theListItem andPenColor:(EnumPenColor)thePenColor andNewScribbleItem:(BOOL)newScribbleItem;

- (void)loadDrawingLayer;

- (NSDictionary *)copyDictionary:(NSDictionary *)theDictionary;
- (void)showDrawingFrame;

- (void)cancelButtonAction;

@end
