    //
//  HighlightersViewController.m
//  Manage
//
//  Created by Cliff Viegas on 15/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "HighlightersViewController.h"

// Private Methods
@interface HighlightersViewController()
- (void)loadButtons;
@end


@implementation HighlightersViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management


- (void)dealloc {
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<HighlightersPopoverDelegate>)theDelegate andSize:(CGSize)theSize {
	if (self = [super init]) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_HighlightersViewController;
#endif
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Set initial value for highlighter color
		highlighterColor = EnumHighlighterColorNone;
		
		// Set a background color
		UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iPadGroupTableViewBackground.png"]];
		[backgroundImageView setFrame:CGRectMake(0, 0, theSize.width, theSize.height)];
		[self.view addSubview:backgroundImageView];
		[backgroundImageView release];
		//[self.view setBackgroundColor:[UIColor darkGrayColor]];
		
		// Set content size for view in popover
		[self setContentSizeForViewInPopover:theSize];
		
		// Set title
		//self.title = @"Highlighters";
		
		// Load the buttons
		[self loadButtons];
	}
	return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        _popover.popoverContentSize = CGSizeMake(self.contentSizeForViewInPopover.width, self.contentSizeForViewInPopover.height + 44);
    }
}

- (void)viewDidUnload {
    _popover = nil;
    [super viewDidUnload];
}

#pragma mark -
#pragma mark Load Methods

- (void)loadButtons {
	// Set the right bar button
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone 
																					   target:self 
																					   action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	redHighlighterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[redHighlighterButton setFrame:CGRectMake(10, 10, 40, 37)];
	[redHighlighterButton setImage:[UIImage imageNamed:@"RedHigh.png"] forState:UIControlStateNormal];
	[redHighlighterButton setImage:[UIImage imageNamed:@"RedHighSelected.png"] forState:UIControlStateSelected];
	[redHighlighterButton addTarget:self action:@selector(redHighlighterButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:redHighlighterButton];
	
	yellowHighlighterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[yellowHighlighterButton setFrame:CGRectMake(60, 10, 40, 37)];
	[yellowHighlighterButton setImage:[UIImage imageNamed:@"YellowHigh.png"] forState:UIControlStateNormal];
	[yellowHighlighterButton setImage:[UIImage imageNamed:@"YellowHighSelected.png"] forState:UIControlStateSelected];
	[yellowHighlighterButton addTarget:self action:@selector(yellowHighlighterButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:yellowHighlighterButton];
	
	whiteHighlighterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[whiteHighlighterButton setFrame:CGRectMake(110, 10, 40, 37)];
	[whiteHighlighterButton setImage:[UIImage imageNamed:@"WhiteHigh.png"] forState:UIControlStateNormal];
	[whiteHighlighterButton setImage:[UIImage imageNamed:@"WhiteHighSelected.png"] forState:UIControlStateSelected];
	[whiteHighlighterButton addTarget:self action:@selector(whiteHighlighterButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:whiteHighlighterButton];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	[delegate dismissHighlightersPopover];
}
//[delegate dismissDrawPadPopOver];

- (void)redHighlighterButtonAction {
	// Set selected highlighter
	[redHighlighterButton setSelected:YES];
	
	// Call the delegate
	[delegate highlighterSelected:EnumHighlighterColorRed];
	
	// Set others not selected
	[yellowHighlighterButton setSelected:NO];
	[whiteHighlighterButton setSelected:NO];
}

- (void)yellowHighlighterButtonAction {
	// Set selected highlighter
	[yellowHighlighterButton setSelected:YES];
	
	// Call the delegate
	[delegate highlighterSelected:EnumHighlighterColorYellow];
	
	// Set others not selected
	[redHighlighterButton setSelected:NO];
	[whiteHighlighterButton setSelected:NO];
}

- (void)whiteHighlighterButtonAction {
	// Set selected highlighter
	[whiteHighlighterButton setSelected:YES];
	
	// Call the delegate
	[delegate highlighterSelected:EnumHighlighterColorWhite];
	
	// Set others not selected
	[yellowHighlighterButton setSelected:NO];
	[redHighlighterButton setSelected:NO];
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


@end
