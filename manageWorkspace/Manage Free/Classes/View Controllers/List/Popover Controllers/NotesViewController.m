//
//  NotesViewController.m
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "NotesViewController.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kTextViewFrame		CGRectMake(6, 4, 285, 340)
#define kPopoverSize		CGSizeMake(320, 318)
#define kTextViewTag		93721
#define kRowHeight			320

@interface NotesViewController (){
    UITextView *textView;
}

@end

@implementation NotesViewController

@synthesize delegate;
@synthesize notes;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[self.notes release];
	[myTableView release];
	[super dealloc];
}


#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<NotesDelegate>)theDelegate andNotes:(NSString *)taskNotes {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_NotesViewController;
#endif
		// Set the title
		self.title = @"Edit Notes";
		
		// Set the notes
		self.notes = taskNotes;
		
		// Assign the delegate
		self.delegate = theDelegate;
		
		//[self setContentSizeForViewInPopover:kPopoverSize];
		[self setContentSizeForViewInPopover:kPopoverSize];
		
//		[self loadTableView];
	
		[self loadButtons];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
//	[myTableView setRowHeight:kRowHeight];
	[self.view addSubview:myTableView];
}

- (void) loadTextView {
    textView = [self getTextView];
    
    textView.frame = kTableViewFrame;
    [self.view addSubview:textView];
}

- (void)loadButtons {
	// Set the right bar button and the left bar button
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone 
																					   target:self 
																					   action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
																						 target:self 
																						 action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
	[cancelBarButtonItem release];
	
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
	
	// Create a new list item and add it to the list item collection (task list)
	// Will need a delegate for this! bah
//	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextView *myTextView = textView;
	
	// Resign responder when exiting
	if ([myTextView isFirstResponder]) {
		[myTextView resignFirstResponder];
	}
	
	[delegate taskNotesUpdated:myTextView.text];
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarButtonItemAction {
//	UITableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	UITextView *notesTextView = textView;
	if ([notesTextView isFirstResponder]) {
		[notesTextView resignFirstResponder];
	}
	
	[self.navigationController popViewControllerAnimated:YES];
	//[delegate dismissNewListItemPopOver];
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self setContentSizeForViewInPopover:kPopoverSize];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [textView becomeFirstResponder];
	
	[super viewDidAppear:animated];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTextView];
}

#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		
		//[cell.contentView addSubview:[self getSwitch]];
		[cell.contentView addSubview:[self getTextView]];
	}
	
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	
	return cell;
}

#pragma mark -
#pragma mark Helper Methods

- (UITextView *)getTextView {
	UITextView *atextView = [[[UITextView alloc] initWithFrame:kTextViewFrame] autorelease];
	[atextView setBackgroundColor:[UIColor clearColor]];
	
	[atextView setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
	[atextView setBackgroundColor:[UIColor whiteColor]];
	[atextView setTextColor:[UIColor darkTextColor]];
	[atextView setShowsVerticalScrollIndicator:NO];
	[atextView setAlwaysBounceVertical:NO];
	[atextView setTag:kTextViewTag];
	[atextView setText:self.notes];
	
	//[textView set
	
	return atextView;
}


@end
