//
//  MoveItemsViewController.h
//  Manage
//
//  Created by Cliff Viegas on 4/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TaskListCollection;
@class TagCollection;

// Data Models
@class ListItem;
@class ArchiveList;

@protocol MoveItemsDelegate
- (void)didDismissMoveItemsView;
- (void)didUpdateMoveItemsView;
@optional
- (void)updateMoveItemsSelectedRow:(NSInteger)newMoveItemsSelectedRow;
- (void)updateArchivesListAfterMovingTasks;
@end


@interface MoveItemsViewController : GAITrackedViewController <UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource> {
	id <MoveItemsDelegate>		delegate;						// The delegate
	TaskListCollection			*refMasterTaskListCollection;	// The passed master task list collection
	ListItem					*refListItem;					// The passed list item which is the focus
	ArchiveList					*refArchiveList;				// Reference to the passed archive list
	UITableView					*myTableView;					// Local table view for displaying lists (and maybe other tasks)
	NSInteger					currentlySelectedRow;			// The currently selected row
	NSInteger					originalParentListID;			// The original parent list id for the list item
	BOOL						isArchivesSelected;				// For tracking whether archives option is selected
	BOOL						originalIsArchivesSelected;		// For tracking whether the list item was originally archived
	TagCollection				*refTagCollection;				// For review list item controller
    
    // Manage 1.6 Update
    NSInteger                   currentlySelectedSection;       // The currently selected section
    //UIToolbar                   *myToolbar;                   // Putting this at the bottom to allow moving tasks to start or end of list
}

@property (nonatomic, assign)		id				delegate;
@property (nonatomic, assign)		ArchiveList		*refArchiveList;
@property (nonatomic, assign)		NSInteger		currentlySelectedRow;
@property (nonatomic, assign)       NSInteger       currentlySelectedSection;
@property (nonatomic, assign)		TagCollection	*refTagCollection;

#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection 
					 andListItem:(ListItem *)theListItem
				andNavigationBar:(BOOL)showNavBar;

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection
         andSelectedArchivesList:(NSMutableArray *)selectedArchivesList
				andNavigationBar:(BOOL)showNavBar;
@end
