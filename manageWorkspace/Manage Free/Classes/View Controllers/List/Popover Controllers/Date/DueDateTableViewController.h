//
//  DueDateTableViewController.h
//  Manage
//
//  Created by Cliff Viegas on 12/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Tapku  (for calendar item)
#import "TapkuLibrary.h"

// Data Models
@class ListItem;

@interface DueDateTableViewController : TKCalendarMonthTableViewController {
	ListItem *refListItem;
}

@property (nonatomic, assign) UIPopoverController *refPopover;
#pragma mark Initialisation
- (id)initWithListItem:(ListItem *)theListItem;

@end
