    //
//  RepeatStartDateTableViewController.m
//  Manage
//
//  Created by Cliff Viegas on 10/12/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "RepeatStartDateTableViewController.h"

// Data Models
#import "RecurringListItem.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#define kViewControllerSize					CGSizeMake(320, 318)

@implementation RepeatStartDateTableViewController

@synthesize delegate;
@synthesize refPopoverController;

#pragma mark -
#pragma mark Initialisation

- (id)initWithRecurringListItem:(RecurringListItem *)theRecurringListItem {
	if (self = [super init]) {
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Set title
		self.title = @"Start Date";
		
		// Assign the recurring list item
		refRecurringListItem = theRecurringListItem;
	}
	return self;
}

#pragma mark -
#pragma mark View Lifecycle

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	[self.refPopoverController setPopoverContentSize:CGSizeMake(self.monthView.frame.size.width, self.monthView.frame.size.height) animated:YES];
	//[self setContentSizeForViewInPopover:CGSizeMake(self.monthView.frame.size.width, 
	//												self.monthView.frame.size.height - 44)];
	[UIView commitAnimations];
	
	[self.monthView reload];
	
	/*if (refRecurringListItem.startPushDate != nil && [refRecurringListItem.startPushDate length] > 0) {
		NSDate *theDate = [MethodHelper dateFromString:refRecurringListItem.startPushDate usingFormat:K_DATEONLY_FORMAT];
		[self.monthView selectDate:theDate];
	}*/
	
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	/*if (refRecurringListItem.startPushDate != nil && [refRecurringListItem.startPushDate length] > 0) {
		NSDate *theDate = [MethodHelper dateFromString:refRecurringListItem.startPushDate usingFormat:K_DATEONLY_FORMAT];
		[self.monthView selectDate:theDate];
	}	*/
}

#pragma mark -
#pragma mark Alert View Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// Canceled
		return;
	} else if (buttonIndex == 1) {
		// Ok
	//	refRecurringListItem.startPushDate = [MethodHelper stringFromDate:warningSavedDate usingFormat:K_DATEONLY_FORMAT];
		
		[self performSelector:@selector(popViewDelay) withObject:nil afterDelay:0.2f];
	}
}

#pragma mark -
#pragma mark Tapku Calendar Delegates

- (void)calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)d {
	warningSavedDate = [d copy];
	
	// Make sure the date isn't before today (negative is past date)
	if ([MethodHelper getDateDifferenceFromTodayForDate:d] < 0) {
		UIAlertView *dateAlert = [[UIAlertView alloc] initWithTitle:@"Warning" 
															message:@"The start date you selected is earlier than today and may cause tasks to be repeated multiple times" 
														   delegate:self 
												  cancelButtonTitle:@"Cancel" 
												  otherButtonTitles:@"Ok", nil];
		[dateAlert show];
		[dateAlert release];
		return;
	}
	
	// Update the date
	//refRecurringListItem.startPushDate = [MethodHelper stringFromDate:d usingFormat:K_DATEONLY_FORMAT];
	
	[self performSelector:@selector(popViewDelay) withObject:nil afterDelay:0.2f];
	
}

- (void)calendarMonthView:(TKCalendarMonthView*)mv monthDidChange:(NSDate*)d {
	[self.refPopoverController setPopoverContentSize:CGSizeMake(self.monthView.frame.size.width, self.monthView.frame.size.height) animated:YES];
	//[self setContentSizeForViewInPopover:CGSizeMake(self.monthView.frame.size.width, 
	//												self.monthView.frame.size.height - 44)];
}


- (void)popViewDelay {
	[self.navigationController popViewControllerAnimated:YES];
	
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

@end
