//
//  ChooseTextOCRViewController.h
//  Manage
//
//  Created by Kerofrog on 10/17/13.
//
//

#import <UIKit/UIKit.h>

@protocol ChooseTextOCRViewControllerDelegate <NSObject>

- (void)doneChooseTextOCRToAddNewListItemWithText:(NSString*)text;

@end

@interface ChooseTextOCRViewController : UIViewController{
    
    UITextView				*contentTextView;				// UITextField for displaying content
	UIPopoverController		*refPopoverController;		// Reference to popover controller
    
    CGRect                  popoverRect;
    id <ChooseTextOCRViewControllerDelegate>	delegate;
}

@property (nonatomic, assign)       id                      delegate;
@property	(nonatomic, assign)		UIPopoverController		*refPopoverController;
@property (nonatomic, assign)       CGRect                  popoverRect;


#pragma mark Initialisation
- (id)initWithContentOCR:(NSString *)contentOCR;

#pragma mark Public Set Methods
- (void)setPopoverController:(UIPopoverController *)thePopoverController;

@end
