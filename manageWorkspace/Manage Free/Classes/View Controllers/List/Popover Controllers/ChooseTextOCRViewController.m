//
//  ChooseTextOCRViewController.m
//  Manage
//
//  Created by Kerofrog on 10/17/13.
//
//

#import "ChooseTextOCRViewController.h"

// Constants
#define kViewControllerSize					CGSizeMake(320, 318)
#define kTableViewFrame						CGRectMake(10, 0, 300, 318)

@interface ChooseTextOCRViewController ()

// Private methods
- (void)loadBarButtonItems;

@end

@implementation ChooseTextOCRViewController

@synthesize	refPopoverController;
@synthesize delegate;
@synthesize popoverRect;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[contentTextView release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithContentOCR:(NSString *)contentOCR {
	if (self = [super init]) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_AlarmsViewController;
#endif
		// Set the title
		self.title = @"Choose Text";
		
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		// Load the bar button items
		[self loadBarButtonItems];
		
		// Load the textfield
		[self loadTextFieldWithText:contentOCR];
	}
	
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadBarButtonItems {
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
										  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
										  target:self
										  action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	// Hide the back bar button item
	self.navigationItem.hidesBackButton = YES;
	
	/*UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
     target:self
     action:@selector(cancelBarButtonItemAction)];
     [self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
     [cancelBarButtonItem release];*/
}

- (void)loadTextFieldWithText:(NSString*)text{
	contentTextView = [[UITextView alloc] initWithFrame:kTableViewFrame];
    [contentTextView setText:text];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [contentTextView setBackgroundColor:[UIColor clearColor]];
    }else{
        [contentTextView setBackgroundColor:[UIColor whiteColor]];
    }

	[self.view addSubview:contentTextView];
}

#pragma mark -
#pragma mark Button Actions

- (void)doneBarButtonItemAction {
    
    if ([delegate respondsToSelector:@selector(doneChooseTextOCRToAddNewListItemWithText:)]) {
        [self.delegate doneChooseTextOCRToAddNewListItemWithText:contentTextView.text];
    }
}

- (void)cancelBarButtonItemAction {
    
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Public Set Methods

- (void)setPopoverController:(UIPopoverController *)thePopoverController {
	refPopoverController = thePopoverController;
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)viewDidAppear:(BOOL)animated {
	if (refPopoverController) {
		[refPopoverController setPopoverContentSize:kViewControllerSize animated:YES];
	}
}

- (void)viewWillAppear:(BOOL)animated {
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end



