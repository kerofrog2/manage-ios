//
//  PreviewListItemViewController.h
//  Manage
//
//  Created by Kerofrog on 6/7/13.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

// Data Models
@class TaskList;
@class ListItem;

// Data Collections
@class TagCollection;
@class ListItemTagCollection;

@protocol PreviewListItemViewControllerDelegate

@optional
- (void)updateTableViewAfterMovingTasks;
@end


@interface PreviewListItemViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
    id <PreviewListItemViewControllerDelegate>		delegate;
    ListItem		*refListItem;
    ListItem		*parentListItem;
	TaskList		*refTaskList;
	TagCollection	*refTagCollection;
    //  Need a list items tag collection
	ListItemTagCollection	*listItemTagCollection;
    NSMutableArray *arrayTaskList;
    UITableView		*myTableView;
    // Reference to the pop over controller
	UIPopoverController		*popoverController;
    
    // Get selected index on tableview
    NSInteger currentIndex;
}
@property (nonatomic, assign)		id				delegate;
@property (nonatomic, assign) ListItem      *refListItem;
@property (nonatomic, assign) NSMutableArray *arrayTaskList;
@property (nonatomic, retain) ListItemTagCollection	*listItemTagCollection;

#pragma mark Initialisation
- (id)initWithTaskList:(TaskList *)theTaskList andTagCollection:(TagCollection *)theTagCollection andListItem:(ListItem *)theListItem;

#pragma mark Public Set Methods
- (void)setPopoverController:(UIPopoverController *)thePopoverController;

@end