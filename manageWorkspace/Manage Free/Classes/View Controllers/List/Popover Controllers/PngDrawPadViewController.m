//
//  DrawPadViewController.m
//  Manage
//
//  Created by Cliff Viegas on 14/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "PngDrawPadViewController.h"

// Data Models
#import "ListItem.h"
#import "DrawPoint.h"

// Data collections
#import "DrawPointCollection.h"

// Constants
#import "ListConstants.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#define kLineWidth		5.0

@interface PngDrawPadViewController()
// Private Methods
- (void)loadButtons;
- (void)loadScrollView;
- (void)loadDrawPointsCollection;
// For deselecting all buttons
- (void)deselectAll;

@end


@implementation PngDrawPadViewController

@synthesize delegate;
@synthesize timer;
@synthesize isNewScribbleItem;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[bluePenButton release];
//	[greenPenButton release];
//	[redPenButton release];
//	[blackPenButton release];
//	[dragButton release];
//	[eraserButton release];
	
	// Scroll view objects
	[drawPadScrollView release];
	[drawPadImageView release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<PngDrawPadDelegate>)theDelegate andSize:(CGSize)theSize 
		   andListItem:(ListItem *)theListItem andPenColor:(EnumPngPenColor)thePenColor andNewScribbleItem:(BOOL)newScribbleItem {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_PngDrawPadViewController;
#endif
		// Assign the delegate
		self.delegate = theDelegate;
		
        // Assign is new scribble item
        self.isNewScribbleItem = newScribbleItem;
        
		// Init the drawing points
		[self loadDrawPointsCollection];
		
		// Set a background color
		/*UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iPadGroupTableViewBackground.png"]];
		[backgroundImageView setFrame:CGRectMake(0, 0, theSize.width, theSize.height)];
		[self.view addSubview:backgroundImageView];
		[backgroundImageView release];*/

		// Set the background color
		[self.view setBackgroundColor:[UIColor colorWithRed:0.08235294 
													  green:0.10980392 
													   blue:0.18431372 alpha:1.0]];
		
		// 21  29  49
		// 21 28 47
		
		// The pen color needs to be passed in (sometimes.. but default it to black)
		penColor = thePenColor;
		
		// Assign the passed list item
		refListItem = theListItem;
		
		// Set content size for view in popover
		[self setContentSizeForViewInPopover:theSize];
		
		// Init is dragging finger to false
		isDraggingFinger = FALSE;
		
		// Set title
		self.title = @"Scribble Pad";
		
		// Load the buttons
		[self loadButtons];
		
		// Load the scroll view
		[self loadScrollView];
		
        // Load our label that informs user that this is old scribble pad
        UILabel *oldScribbleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 192, 614, 50)];
        [oldScribbleLabel setTextAlignment:UITextAlignmentCenter];
        [oldScribbleLabel setNumberOfLines:2];
        [oldScribbleLabel setTextColor:[UIColor lightTextColor]];
        [oldScribbleLabel setBackgroundColor:[UIColor clearColor]];
        [oldScribbleLabel setText:@"NOTE: This is the old version of the Scribble Pad that is only used for scribbles that were created in Manage v1.0 - v1.6.1."];
        [self.view addSubview:oldScribbleLabel];
        [oldScribbleLabel release];
        
		// Deselect all
		[self deselectAll];
		
		switch (penColor) {
			case EnumPngPenColorNone:
				[dragButton setSelected:YES];
				[drawPadScrollView setUserInteractionEnabled:YES];
				break;
			case EnumPngPenColorRed:
				[redPenButton setSelected:YES];
				[drawPadScrollView setUserInteractionEnabled:NO];
				break;
			case EnumPngPenColorBlue:
				[bluePenButton setSelected:YES];
				[drawPadScrollView setUserInteractionEnabled:NO];
				break;
			case EnumPngPenColorBlack:
				[blackPenButton setSelected:YES];
				[drawPadScrollView setUserInteractionEnabled:NO];
				break;
			case EnumPngPenColorGreen:
				[greenPenButton setSelected:YES];
				[drawPadScrollView setUserInteractionEnabled:NO];
				break;
			default:
				break;
		}
		
		

	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadScrollView {
	drawPadScrollView = [[UIScrollView alloc] init];
	
	[drawPadScrollView setFrame:kPngDrawPadScrollViewRect];
	drawPadScrollView.delegate = self;
	[drawPadScrollView setBackgroundColor:[UIColor whiteColor]];
	[drawPadScrollView setContentSize:kDrawPadScrollViewContentSize];
	
	UILabel *magnifiedListItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 33, 1350, 63)];
	// If this is sub task, modify label position and size
	//if (refListItem.parentListItemID != -1) {
	//	[magnifiedListItemLabel setFrame:CGRectMake(84, 33, 1281, 63)];
	//}
	
	//[magnifiedListItemLabel setFont:[UIFont fontWithName:@"Helvetica" size:42.0]];
	[magnifiedListItemLabel setFont:[UIFont fontWithName:@"Helvetica" size:42.5]];
	
	[magnifiedListItemLabel setBackgroundColor:[UIColor whiteColor]];
	[magnifiedListItemLabel setText:refListItem.title];
	[magnifiedListItemLabel setTextColor:[UIColor darkTextColor]];
	[drawPadScrollView addSubview:magnifiedListItemLabel];
	[magnifiedListItemLabel release];
	
									   // font height is 42
									   
	// Add UIImageView
	drawPadImageView = [[UIImageView alloc] initWithFrame:kDrawPadImageViewRect];
	[drawPadImageView setBackgroundColor:[UIColor clearColor]];
	
	// Load the image if it exists
	if ([refListItem.scribble length] > 0 && refListItem.scribble != nil) {
		NSString *scribblePath = [NSString stringWithFormat:@"%@%@",
								  [MethodHelper getScribblePath],
								  refListItem.scribble];
		[drawPadImageView setImage:[UIImage imageWithContentsOfFile:scribblePath]];
	}
	
	[drawPadScrollView addSubview:drawPadImageView];
	
	[self.view addSubview:drawPadScrollView];
	
	//[self.view addSubview:drawPadImageView];
}

- (void)loadButtons {
	// Set the right bar button and the left bar button
	UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone 
																					   target:self 
																					   action:@selector(doneBarButtonItemAction)];
	[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
	[doneBarButtonItem release];
	
	UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
																						 target:self 
																						 action:@selector(cancelBarButtonItemAction)];
	[self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
	[cancelBarButtonItem release];
	
	// 614 width (306)
	
	// Init the pen buttons
	greenPenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[greenPenButton setFrame:CGRectMake(211, 152, 40, 37)];
	[greenPenButton setImage:[UIImage imageNamed:@"PngGreen.png"] forState:UIControlStateNormal];
	[greenPenButton setImage:[UIImage imageNamed:@"PngGreenSelected.png"] forState:UIControlStateSelected];
	[greenPenButton addTarget:self action:@selector(greenPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:greenPenButton];
	
	bluePenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[bluePenButton setFrame:CGRectMake(311, 152, 40, 37)];
	[bluePenButton setImage:[UIImage imageNamed:@"PngBlue.png"] forState:UIControlStateNormal];
	[bluePenButton setImage:[UIImage imageNamed:@"PngBlueSelected.png"] forState:UIControlStateSelected];
	[bluePenButton addTarget:self action:@selector(bluePenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:bluePenButton];
	
	redPenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[redPenButton setFrame:CGRectMake(261, 152, 40, 37)];
	[redPenButton setImage:[UIImage imageNamed:@"PngRed.png"] forState:UIControlStateNormal];
	[redPenButton setImage:[UIImage imageNamed:@"PngRedSelected.png"] forState:UIControlStateSelected];
	[redPenButton addTarget:self action:@selector(redPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:redPenButton];
	
	blackPenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[blackPenButton setFrame:CGRectMake(361, 152, 40, 37)];
	[blackPenButton setImage:[UIImage imageNamed:@"PngBlack.png"] forState:UIControlStateNormal];
	[blackPenButton setImage:[UIImage imageNamed:@"PngBlackSelected.png"] forState:UIControlStateSelected];
	[blackPenButton addTarget:self action:@selector(blackPenButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:blackPenButton];
	
	dragButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[dragButton setFrame:CGRectMake(564, 152, 40, 37)];
	[dragButton setImage:[UIImage imageNamed:@"PngHand.png"] forState:UIControlStateNormal];
	[dragButton setImage:[UIImage imageNamed:@"PngHandSelected.png"] forState:UIControlStateSelected];
	[dragButton addTarget:self action:@selector(dragButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:dragButton];
	
	eraserButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[eraserButton setFrame:CGRectMake(10, 152, 40, 37)];
	[eraserButton setImage:[UIImage imageNamed:@"PngEraser.png"] forState:UIControlStateNormal];
	[eraserButton setImage:[UIImage imageNamed:@"PngEraserSelected.png"] forState:UIControlStateSelected];
	[eraserButton addTarget:self action:@selector(eraserButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:eraserButton];
}

- (void)loadDrawPointsCollection {
	drawPointCollection = [[DrawPointCollection alloc] init];
}

#pragma mark -
#pragma mark Button Actions

- (void)deselectAll {
	[greenPenButton setSelected:NO];
	[redPenButton setSelected:NO];
	[bluePenButton setSelected:NO];
	[blackPenButton setSelected:NO];
	[dragButton setSelected:NO];
	[eraserButton setSelected:NO];
}

- (void)greenPenButtonAction {
	[drawPadScrollView setUserInteractionEnabled:NO];
	penColor = EnumPngPenColorGreen;
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[greenPenButton setSelected:YES];
}

- (void)redPenButtonAction {
	[drawPadScrollView setUserInteractionEnabled:NO];
	penColor = EnumPngPenColorRed;
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[redPenButton setSelected:YES];
}

- (void)bluePenButtonAction {
	[drawPadScrollView setUserInteractionEnabled:NO];
	penColor = EnumPngPenColorBlue;
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[bluePenButton setSelected:YES];
}

- (void)blackPenButtonAction {
	[drawPadScrollView setUserInteractionEnabled:NO];
	penColor = EnumPngPenColorBlack;
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[blackPenButton setSelected:YES];
}

- (void)dragButtonAction {
	[drawPadScrollView setUserInteractionEnabled:YES];
	penColor = EnumPngPenColorNone;
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[dragButton setSelected:YES];
}

- (void)eraserButtonAction {
	[drawPadScrollView setUserInteractionEnabled:NO];
	penColor = EnumPngPenColorEraser;
	
	// Deselect all
	[self deselectAll];
	
	// Select this pen
	[eraserButton setSelected:YES];
}

- (void)doneBarButtonItemAction {
	// Save the image
	
	// Assign a unique file name for this scribble
	if ([refListItem.scribble length] == 0) {
		refListItem.scribble = [MethodHelper getUniqueFileName];
	}
	
	// Get the file destination
	NSString *fileDestination = [[MethodHelper getScribblePath] stringByAppendingPathComponent:refListItem.scribble];

	// Write the image to the database
	[UIImagePNGRepresentation(drawPadImageView.image) writeToFile:fileDestination atomically:YES];
	
	// Update the database
	[refListItem updateDatabase];
	
	[delegate drawPadUpdatedForListItem:refListItem];
}

- (void)cancelBarButtonItemAction {
	[delegate dismissDrawPadPopOver:self.isNewScribbleItem];
}

#pragma mark -
#pragma mark Touches Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	if (drawPadScrollView.userInteractionEnabled == YES) {
		return;
	}
	
	// Clear the draw points
	[drawPointCollection clearDrawPoints];
	
	//if (lockDrawing == YES) {
		// Do nothing
	//	return;
	//}
	isDraggingFinger = TRUE;
	
	//[self runTimer];
	
	//mouseSwiped = NO;
	UITouch *touch = [touches anyObject];
	
	lastPoint = [touch locationInView:drawPadImageView];
	
	//DrawPoint *currentDrawPoint = [self.drawingPoints objectAtIndex:0];
	//[currentDrawPoint updateWithCGPoint:lastPoint];
	[drawPointCollection updateDrawPointAtIndex:0 withNewPoint:lastPoint];
	
	//lastPoint.y -= 20;
	
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	//if (lockDrawing == YES) {
		// Do nothing
		//return;
	//}
	if (drawPadScrollView.userInteractionEnabled == YES) {
		return;
	}
	
	isDraggingFinger = TRUE;
	
	mouseSwiped = YES;
	
	UITouch *touch = [touches anyObject];	
	CGPoint currentPoint = [touch locationInView:drawPadImageView];
	//currentPoint.y -= 20;
	
	
	if ([drawPointCollection isPointsFull]) {
		UIGraphicsBeginImageContext(drawPadImageView.frame.size);
		[drawPadImageView.image drawInRect:CGRectMake(0, 0, drawPadImageView.frame.size.width, drawPadImageView.frame.size.height)];
		CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
		
		/*CGPoint startPoint = CGPointMake(drawPoint1.x, drawPoint1.y);
		CGPoint endPoint = CGPointMake(drawPoint4.x, drawPoint4.y);
		
		CGFloat distance = [MethodHelper distanceBetween:startPoint and:endPoint];
		
		CGFloat newLineWidth = kLineWidth;
		if (distance < 10) {
			newLineWidth = 5;
		} else if (distance < 20) {
			newLineWidth = 4;
		} else if (distance < 30) {
			newLineWidth = 3;
		} else if (distance < 40) {
			newLineWidth = 2;
		} else {
			newLineWidth = 1;
		}*/
		
		CGContextSetLineWidth(UIGraphicsGetCurrentContext(), kLineWidth);
		
		switch (penColor) {
			case EnumPngPenColorBlack:
				CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
				break;
			case EnumPngPenColorBlue:
				CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 1.0, 1.0);
				break;
			case EnumPngPenColorGreen:
				CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.411764, 0.21568627, 1.0);
				break;
			case EnumPngPenColorRed:
				CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.8274509, 0.0235294, 0.0, 1.0);
				break;
			case EnumPngPenColorEraser:
				CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 20.0);
				CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);
				break;
			default:
				CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
				break;
		}
		
		
		CGContextBeginPath(UIGraphicsGetCurrentContext());
		CGContextSetLineJoin(UIGraphicsGetCurrentContext(), kCGLineJoinRound);
		
		DrawPoint *drawPoint1 = [drawPointCollection.drawPoints objectAtIndex:0];
		DrawPoint *drawPoint2 = [drawPointCollection.drawPoints objectAtIndex:1];
		DrawPoint *drawPoint3 = [drawPointCollection.drawPoints objectAtIndex:2];
		DrawPoint *drawPoint4 = [drawPointCollection.drawPoints objectAtIndex:3];
		
		CGContextMoveToPoint(UIGraphicsGetCurrentContext(), drawPoint1.x, drawPoint1.y);
		//CGContextAddArcToPoint(UIGraphicsGetCurrentContext(), drawPoint2.x, drawPoint2.y, 
		//					   drawPoint3.x, drawPoint3.y, 20);
		
		CGContextSetFlatness(UIGraphicsGetCurrentContext(), 0.05);
		CGContextSetAllowsAntialiasing(UIGraphicsGetCurrentContext(), YES);
		CGContextSetShouldAntialias(UIGraphicsGetCurrentContext(), YES);
		
		//CGContextSetMiterLimit(<#CGContextRef c#>, <#CGFloat limit#>)
		
		CGContextAddCurveToPoint(UIGraphicsGetCurrentContext(), drawPoint2.x, drawPoint2.y, 
								 drawPoint3.x, drawPoint3.y, drawPoint4.x, drawPoint4.y);
		
		
		//CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
		//CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
		CGContextStrokePath(UIGraphicsGetCurrentContext());
		drawPadImageView.image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[drawPointCollection resetDrawPoints];
		//[drawPointCollection rotatePointsWithNewPoint:currentPoint];
	} else {
		[drawPointCollection addPoint:currentPoint];
	}

	lastPoint = currentPoint;
	
	mouseMoved++;
	
	if (mouseMoved == 10) {
		mouseMoved = 0;
	}
	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	//if (lockDrawing == YES) {
		// Do nothing
	//	return;
	//}
	
	if (drawPadScrollView.userInteractionEnabled == YES) {
		return;
	}
	
	isDraggingFinger = FALSE;
	//[self stopTimer];
	
	//UITouch *touch = [touches anyObject];
	
	imageHasChanged = YES;
	
	//if(!mouseSwiped) {
	UIGraphicsBeginImageContext(drawPadImageView.frame.size);
	[drawPadImageView.image drawInRect:CGRectMake(0, 0, drawPadImageView.frame.size.width, drawPadImageView.frame.size.height)];
	CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
	CGContextSetLineWidth(UIGraphicsGetCurrentContext(), kLineWidth);
		
	CGContextSetFlatness(UIGraphicsGetCurrentContext(), 0.05);
	CGContextSetAllowsAntialiasing(UIGraphicsGetCurrentContext(), YES);
	CGContextSetShouldAntialias(UIGraphicsGetCurrentContext(), YES);
	
	switch (penColor) {
		case EnumPngPenColorBlack:
			CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
			break;
		case EnumPngPenColorBlue:
			CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 1.0, 1.0);
			break;
		case EnumPngPenColorGreen:
			CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.411764, 0.21568627, 1.0);
			break;
		case EnumPngPenColorRed:
			CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.8274509, 0.0235294, 0.0, 1.0);
			break;
		case EnumPngPenColorEraser:
			CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 10.0);
			CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);
			break;
		default:
			CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
			break;
	}
	
	CGContextSetLineJoin(UIGraphicsGetCurrentContext(), kCGLineJoinRound);
	
	DrawPoint *drawPoint = [drawPointCollection.drawPoints objectAtIndex:0];
	if (drawPoint.x != kNoPoint && drawPoint.y != kNoPoint) {
		CGContextMoveToPoint(UIGraphicsGetCurrentContext(), drawPoint.x, drawPoint.y);
		CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
		CGContextStrokePath(UIGraphicsGetCurrentContext());
	}
	
	CGContextFlush(UIGraphicsGetCurrentContext());
	drawPadImageView.image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	//}
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	isDraggingFinger = FALSE;
	//[self stopTimer];
}

#pragma mark -
#pragma mark Timer Methods

- (void)runTimer {
	// Load timer
	self.timer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self
										   selector:@selector(timerTick) 
										   userInfo:nil repeats:YES];
	
}

- (void)stopTimer {
	if (self.timer != nil) {
		[self.timer invalidate];
		self.timer = nil;
	}
}

- (void)timerTick {
	
}

#pragma mark -
#pragma mark UIViewController Delegate Methods

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





@end
