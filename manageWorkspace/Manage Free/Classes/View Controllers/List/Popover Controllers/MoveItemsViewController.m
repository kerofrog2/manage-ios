    //
//  MoveItemsViewController.m
//  Manage
//
//  Created by Cliff Viegas on 4/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "MoveItemsViewController.h"

// Data Collections
#import "TaskListCollection.h"
#import "TagCollection.h"

// Data Models
#import "TaskList.h"
#import "ListItem.h"
#import "ArchiveList.h"

// Method Helper
#import "MethodHelper.h"

// Custom UI Objects
#import "MoveArray.h"

// View Controllers
#import "ReviewListItemViewController.h"

// Constants
#define kViewControllerSize					CGSizeMake(320, 362)
#define kNavControllerViewControllerSize	CGSizeMake(320, 318)
#define kTableViewFrame						CGRectMake(0, 44, 320, 318)
#define kNavControllerTableViewFrame		CGRectMake(0, 0, 320, 318)
//#define kToolbarFrame                       CGRectMake(0, 318, 320, 44)

// Enum
typedef enum {
	EnumSendToBeginning = 0,
	EnumSendToEnd = 1
} EnumSend;

// Private methods
@interface MoveItemsViewController()
- (void)loadNavigationBar;
- (void)loadTableViewWithNavigationBar:(BOOL)showNavBar;
@end


@implementation MoveItemsViewController

@synthesize delegate;
@synthesize refArchiveList;
@synthesize currentlySelectedRow;
@synthesize currentlySelectedSection;
@synthesize refTagCollection;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    [myTableView release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

// Need to add own navigation bar

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection 
					 andListItem:(ListItem *)theListItem 
				andNavigationBar:(BOOL)showNavBar {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_MoveItemsViewController;
#endif
		// Assign passed task list collection to ref master task list collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Init currently selected row to -1
		self.currentlySelectedRow = -1;
		
        // Init currently selected section to -1
        self.currentlySelectedSection = -1;
        
		// Assign list item reference
		refListItem = theListItem;
		
		// Init the archive list to nil
		self.refArchiveList = nil;
        
		// Init is archives selected
		if ([refListItem archived] == TRUE) {
			isArchivesSelected = TRUE;
			originalIsArchivesSelected = TRUE;
		} else {
			isArchivesSelected = FALSE;
			originalIsArchivesSelected = FALSE;
		}
		
		// Set the original parent list id
		originalParentListID = refListItem.listID;
		
		// Set content size and title (if needed)
		if (showNavBar == TRUE) {
			[self setContentSizeForViewInPopover:kViewControllerSize];
		} else if (showNavBar == FALSE) {
			self.title = @"Move Task";
			[self setContentSizeForViewInPopover:kNavControllerViewControllerSize];
		}
		
		if (showNavBar == TRUE) {
			[self loadNavigationBar];
		} else {
			// Add a review button item to navigation controller
			UIBarButtonItem *reviewBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Review" 
																					style:UIBarButtonItemStyleBordered 
																				   target:self 
																				   action:@selector(reviewBarButtonItemAction)];
			[self.navigationItem setRightBarButtonItem:reviewBarButtonItem animated:NO];
			[reviewBarButtonItem release];
		}
		
		[self loadTableViewWithNavigationBar:showNavBar];
	}
	return self;
}

// for Archives
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection
					 andSelectedArchivesList:(NSMutableArray *)selectedArchivesList
				andNavigationBar:(BOOL)showNavBar {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_MoveItemsViewController;
#endif
		// Assign passed task list collection to ref master task list collection
		refMasterTaskListCollection = theTaskListCollection;
		
		// Init currently selected row to -1
		self.currentlySelectedRow = -1;
		
        // Init currently selected section to -1
        self.currentlySelectedSection = -1;
        
		// Assign list item reference
		refListItem = [selectedArchivesList objectAtIndex:0];
		
		// Init the archive list to nil
		self.refArchiveList = nil;
		
		// Init is archives selected
		if ([refListItem archived] == TRUE) {
			isArchivesSelected = TRUE;
			originalIsArchivesSelected = TRUE;
		} else {
			isArchivesSelected = FALSE;
			originalIsArchivesSelected = FALSE;
		}
		
		// Set the original parent list id
		originalParentListID = refListItem.listID;
		
		// Set content size and title (if needed)
		if (showNavBar == TRUE) {
			[self setContentSizeForViewInPopover:kViewControllerSize];
		} else if (showNavBar == FALSE) {
			self.title = @"Move Task";
			[self setContentSizeForViewInPopover:kNavControllerViewControllerSize];
		}
		
		if (showNavBar == TRUE) {
			[self loadNavigationBar];
		}
        
        else {
			// Add a review button item to navigation controller
			UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                        target:self
                                        action:@selector(doneMoveArchivesButtonAction)];
			[self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
            
            // Add the left bar button item
            UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                          target:self
                                          action:@selector(cancelBarButtonItemAction)];
            [self.navigationItem setLeftBarButtonItem:cancelBarButtonItem animated:NO];
            
			[doneBarButtonItem release];
            [cancelBarButtonItem release];
		}
		
		[self loadTableViewWithNavigationBar:showNavBar];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadNavigationBar {
	UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
	//[navBar setTintColor:[UIColor darkGrayColor]];
	UINavigationItem *navItem = [[[UINavigationItem alloc] initWithTitle:@"Move Task"] autorelease];
	
	// Add the right bar button item
	navItem.rightBarButtonItem = [[[UIBarButtonItem alloc]
								   initWithBarButtonSystemItem:UIBarButtonSystemItemDone
								   target:self
								   action:@selector(doneBarButtonItemAction)] autorelease];
	
	// Add the left bar button item
	navItem.leftBarButtonItem = [[[UIBarButtonItem alloc]
								  initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
								  target:self 
								  action:@selector(cancelBarButtonItemAction)] autorelease];
	
	[navBar pushNavigationItem:navItem animated:NO];
	
	
	[self.view addSubview:navBar];
	[navBar release];
}

- (void)loadTableViewWithNavigationBar:(BOOL)showNavBar {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	if (showNavBar == FALSE) {
		[myTableView setFrame:kNavControllerTableViewFrame];
	}
	
	//[myTableView setBackgroundColor:[UIColor clearColor]];
	[myTableView setDataSource:self];
	[myTableView setDelegate:self];
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([refMasterTaskListCollection.lists count] == 0) {
        return 1;
    }
    
    if ([self.delegate respondsToSelector:@selector(updateMoveItemsSelectedRow:)]) {
        return 2;
    }
    
    return 3;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 1;
	} else if (section == 1 && [self.delegate respondsToSelector:@selector(updateMoveItemsSelectedRow:)] == FALSE) {
        return 2;
    }
	
	return [refMasterTaskListCollection.lists count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}	
	
	// Default settings
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.textLabel setTextColor:[UIColor darkTextColor]];
	cell.detailTextLabel.text = @"";
	cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = @"";
	
	// Set up the cell...
	if (indexPath.section == 0) {
		cell.textLabel.text = @"Archives";
		if (isArchivesSelected == TRUE) {
			cell.accessoryType = UITableViewCellAccessoryCheckmark;
		}
        
	} else if (indexPath.section == 1 && [self.delegate respondsToSelector:@selector(updateMoveItemsSelectedRow:)] == FALSE) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Beginning of List";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"End of List";
        }
        
        if (self.currentlySelectedSection == indexPath.section && self.currentlySelectedRow == indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        
    } else {
		TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:indexPath.row];
		
        if ([taskList isNote] == TRUE) {
            [cell.textLabel setTextColor:[UIColor grayColor]];
        }
        
		cell.textLabel.text = taskList.title;
		
		// TaskList date being extracted from dateTime format, need to see if this works
		NSDate *theDate = [MethodHelper dateFromString:taskList.creationDate usingFormat:K_DATEONLY_FORMAT];
		[cell.detailTextLabel setText:[MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterLongStyle withYear:YES]];
		
		// Update the parent list id each time is selected?.. no, not good.
		if ([taskList listID] == [refListItem listID] && self.currentlySelectedRow == -1 && isArchivesSelected == FALSE) {
			cell.accessoryType = UITableViewCellAccessoryCheckmark;
		} else if (self.currentlySelectedRow == indexPath.row && self.currentlySelectedSection == indexPath.section && isArchivesSelected == FALSE) {
			cell.accessoryType = UITableViewCellAccessoryCheckmark;
		}
	}
	
	return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section != 0 && indexPath.section != 1) {
        TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:indexPath.row];
        if ([taskList isNote] == TRUE) {
            [MethodHelper showAlertViewWithTitle:@"Error" 
                                      andMessage:@"Unable to move task to a Note.  Please select a List" 
                                  andButtonTitle:@"Ok"];
            return nil;
        }
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
		isArchivesSelected = TRUE;
	} else {
		isArchivesSelected = FALSE;
	}

	NSInteger tempCurrentlySelectedRow = 0;
	if (indexPath.section == 0) {
		self.currentlySelectedRow = 0;
        self.currentlySelectedSection = 0;
		tempCurrentlySelectedRow = -1;
	} else {
        self.currentlySelectedRow = indexPath.row;
        self.currentlySelectedSection = indexPath.section;
        tempCurrentlySelectedRow = self.currentlySelectedRow;
    } 
	
    if ([self.delegate respondsToSelector:@selector(updateMoveItemsSelectedRow:)]) {
        [delegate updateMoveItemsSelectedRow:tempCurrentlySelectedRow]; 
    }

	[myTableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"";
    }
    
    if ([self.delegate respondsToSelector:@selector(updateMoveItemsSelectedRow:)]) {
        if (section == 1) {
            return @"Lists / Notes";
        }
    } else {
        if (section == 2) {
            return @"Lists / Notes";
        }
    }
    
	return @"Position";
}

#pragma mark -
#pragma mark Button Actions

- (void)reviewBarButtonItemAction {
	// Open the review edit view controller thing
	ReviewListItemViewController *controller = [[[ReviewListItemViewController alloc] initWithDelegate:nil 
																						  andSize:kNavControllerViewControllerSize
																					  andListItem:refListItem 
																				 andTagCollection:self.refTagCollection 
																			  andIsFilterTagsMode:NO 
																			andTaskListCollection:refMasterTaskListCollection
                                                                                      andTaskList:nil
                                                                                 didAppearFromListView:FALSE] autorelease];
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)doneMoveArchivesButtonAction {
	[delegate updateArchivesListAfterMovingTasks];
}

- (void)cancelBarButtonItemAction {
	[delegate didDismissMoveItemsView];
}

- (void)doneBarButtonItemAction {
	// If currently select row is -1, exit
	if (self.currentlySelectedRow == -1) {
		[delegate didDismissMoveItemsView];
		return;
	}
	
	// If list item was already archived and it has not been moved
	if (originalIsArchivesSelected == TRUE && isArchivesSelected == TRUE) {
		[delegate didDismissMoveItemsView];
		return;
	}
	
	// Handle if we just want to move list item to archives
	if (isArchivesSelected == TRUE && originalIsArchivesSelected == FALSE) {
		// Move item to archives
		NSInteger targetListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:refListItem.listID];
        
		TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:targetListIndex];
		taskList.mainViewRequiresUpdate = TRUE;
		NSInteger listItemIndex = [taskList getIndexOfListItemWithID:refListItem.listItemID];
		
		// Check if there are subtasks
		ListItem *listItem = [taskList.listItems objectAtIndex:listItemIndex];
		if ([listItem parentListItemID] == -1) {
			if ([taskList hasSubtasksForListItemAtIndex:listItemIndex]) {
				// Warn user, then go through and archive all items with parentListItemID (create method in TaskList())
				UIAlertView *subtasksAlert = [[UIAlertView alloc] initWithTitle:@"Warning" 
																		message:@"Subtasks of this task will also be archived" 
																	   delegate:self 
															  cancelButtonTitle:@"Cancel" 
															  otherButtonTitles:@"Ok", nil];
				[subtasksAlert show];
				[subtasksAlert release];
				return;
			}
		} else {
			// Single subtask to be archived, modify its parent list item id to -1
			listItem.parentListItemID = -1;
		}
		
		[taskList archiveListItemAtIndex:listItemIndex];
		[delegate didUpdateMoveItemsView];
		return;
	}
	
    // Handle if we just want to move the task to beginning or end of current list
    if (self.currentlySelectedSection == 1 && [self.delegate respondsToSelector:@selector(updateMoveItemsSelectedRow:)] == FALSE) {
        NSInteger targetListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:originalParentListID];
        
        if (targetListIndex == -1 || targetListIndex >= [refMasterTaskListCollection.lists count]) {
            // Do nothing, jump out
            [MethodHelper showAlertViewWithTitle:@"Error" 
                                      andMessage:[NSString stringWithFormat:@"Unable to find correct list for index (%d)", targetListIndex] 
                                  andButtonTitle:@"Ok"];
            return;
        }
        
        TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:targetListIndex];
        NSInteger listItemIndex = [taskList getIndexOfListItemWithID:refListItem.listItemID];
        
        if (listItemIndex == -1 || listItemIndex >= [taskList.listItems count]) {
            // Do nothing, jump out
            [MethodHelper showAlertViewWithTitle:@"Error" 
                                      andMessage:[NSString stringWithFormat:@"Unable to find correct task for index (%d)", listItemIndex] 
                                  andButtonTitle:@"Ok"];
            return;
        }
        
        // Need to make sure this particular list item index is updated
        ListItem *foundListItem = [taskList.listItems objectAtIndex:listItemIndex];

        BOOL wasSubtask = FALSE;
        if ([foundListItem parentListItemID] != -1) {
            wasSubtask = TRUE;
            foundListItem.parentListItemID = -1;
        }
        
        if ([self currentlySelectedRow] == EnumSendToBeginning) {
            // Need to move to start
            if (wasSubtask) {
                [taskList.listItems moveObjectFromIndex:listItemIndex toIndex:0];
                
                // Now need to get new item order
                foundListItem.itemOrder = [taskList getNextListOrder];
                [foundListItem updateDatabase];
            } else if (wasSubtask == FALSE) {
                [taskList moveListItemFrom:listItemIndex to:0];
            }
        } else if (self.currentlySelectedRow == EnumSendToEnd) {
            // Need to move to end, check if was subtask
            NSInteger endIndex = [taskList.listItems count] - 1;
            
            if (wasSubtask) {
                [taskList.listItems moveObjectFromIndex:listItemIndex toIndex:endIndex];
                
                // Now need to get new item order
                foundListItem.itemOrder = [taskList getFirstListItemOrder];
                [foundListItem updateDatabase];
            } else if (wasSubtask == FALSE) {
                [taskList moveListItemFrom:listItemIndex to:endIndex];
            }

        }
        
        // Check if it is standalone sub task and make it parent task
       /* if (wasSubtask == TRUE) {
            
            // Update all the subtask subitemOrders below the parent task
            NSInteger parentTaskIndex = [taskList getIndexOfListItemWithID:oldParentListItemID];
            
            if (parentTaskIndex != -1 && parentTaskIndex < [taskList.listItems count]) {
                ListItem *parentListItem = [taskList.listItems objectAtIndex:parentTaskIndex];
                
                [taskList updateSubtasksSubListItemAndListItemOrderForListItem:parentListItem];
            }
            
            refListItem.parentListItemID = -1;
            refListItem.subItemOrder = 0;
            [refListItem updateDatabaseWithoutModifiedLog];
        }*/
        
        [delegate didUpdateMoveItemsView];
        
        return;
    }
    
	// Get the currently selected parent list id
	TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:self.currentlySelectedRow];
	taskList.mainViewRequiresUpdate = TRUE;
	
	// Handle if we want to move list item from archives to a list
	if (originalIsArchivesSelected == TRUE && isArchivesSelected == FALSE) {
		// Make sure that archives ref is not nil
		if (self.refArchiveList == nil) {
			return;
		}
        
		// Move item from archives to the correct list
		NSInteger listItemIndex = [self.refArchiveList getIndexOfListItemWithID:refListItem.listItemID];
		
		// Get copy of list item from archives list
		ListItem *listItemCopy = [[ListItem alloc] initWithListItemCopy:[self.refArchiveList.listItems objectAtIndex:listItemIndex]]; 
		
		// Remove the list item from archives list
		[self.refArchiveList.listItems removeObjectAtIndex:listItemIndex];
		
		// Get the next list item order
		NSInteger nextListItemOrder = [taskList getNextListItemOrder];
		
		// Update the list item
		listItemCopy.listID = [taskList listID];
		listItemCopy.itemOrder = nextListItemOrder;
		listItemCopy.archived = FALSE;
		listItemCopy.archivedDateTime = @"";
		listItemCopy.modifiedDateTime = [MethodHelper stringFromDate:[NSDate date] usingFormat:K_DATETIME_FORMAT];
		listItemCopy.parentListTitle = [taskList title];
		
		// Need to remove completed status when moving back, and remove completed date time
		listItemCopy.completed = FALSE;
		listItemCopy.completedDateTime = @"";
		
		// Check if this is a subtask
		BOOL listItemIsSubtask = FALSE;
		if ([listItemCopy parentListItemID] != -1) {
			// Remove subtask status
			//[listItemCopy setParentListItemID:-1];
			listItemIsSubtask = TRUE;
		} 

		// Update the database
		[listItemCopy updateDatabase];
		
		// Add the list item copy to the task list
		[taskList.listItems addObject:listItemCopy];
		
		// At end of moving, look for any subtasks, and also move them
		if (listItemIsSubtask == FALSE) {
			for (int i = [refArchiveList.listItems count] - 1; i >= 0; i--) {
				ListItem *subListItem = [refArchiveList.listItems objectAtIndex:i];

				if ([subListItem parentListItemID] == listItemCopy.listItemID) {
					subListItem.itemOrder = listItemCopy.itemOrder;
					subListItem.listID = listItemCopy.listID;
					
					// Get copy of the sub list item
					ListItem *subListItemCopy = [[ListItem alloc] initWithListItemCopy:subListItem];
					[taskList.listItems addObject:subListItemCopy];
					
					// Remove original sub list item
					[refArchiveList.listItems removeObjectAtIndex:i];
					
					// Update the database
					[subListItemCopy updateDatabase];
					
					// Release our copy
					[subListItemCopy release];
				}
			}
		}
		
		// Now get rid of our list item copy
		[listItemCopy release];
		
		[delegate didUpdateMoveItemsView];
		return;
	}
	
	// Move the list item to the new selected list if it isnt the initial list
	if (originalParentListID == [taskList listID]) {
		// Same as pressing cancel button
		[delegate didDismissMoveItemsView];
		return;
	}
	
	// Move the list item
	[refMasterTaskListCollection moveListItemWithID:refListItem.listItemID 
								 fromTaskListWithID:originalParentListID 
								   toTaskListWithID:taskList.listID];
	
	// The list view controller needs to take care of the rest
	[delegate didUpdateMoveItemsView];
}

#pragma mark -
#pragma mark Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		// Cancel
		[delegate didDismissMoveItemsView];
	} else if (buttonIndex == 1) {
		// Ok
		// Need to shift parent then all child items accross
		NSInteger targetListIndex = [refMasterTaskListCollection getIndexOfTaskListWithID:refListItem.listID];
		TaskList *taskList = [refMasterTaskListCollection.lists objectAtIndex:targetListIndex];
		NSInteger listItemIndex = [taskList getIndexOfListItemWithID:refListItem.listItemID];
        
		// Need to find the new list item index (it may have changed)
		//listItemIndex = [taskList indexOfListItemWithID:refListItem.listItemID];
		
        // Move List Item to Archives
        [taskList archiveListItemAtIndex:listItemIndex];
        
		NSInteger theRefListItemID = refListItem.listItemID;

		// Need to archive any sub tasks, we do them second as the archive date has to be after the parent
		for (int i = [taskList.listItems count] - 1; i >= 0; i--) {
			ListItem *checkListItem = [taskList.listItems objectAtIndex:i];
			if (checkListItem.parentListItemID == theRefListItemID) {
				[taskList archiveListItemAtIndex:i];
			}
		}
		
		[delegate didUpdateMoveItemsView];
	}
}

#pragma mark -
#pragma mark UIViewController Methods

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewDidAppear:(BOOL)animated {
    if ([self.delegate respondsToSelector:@selector(updateMoveItemsSelectedRow:)]) {
       [delegate updateMoveItemsSelectedRow:self.currentlySelectedRow]; 
    }
}




@end
