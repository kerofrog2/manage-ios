//
//  HighlightersViewController.h
//  Manage
//
//  Created by Cliff Viegas on 15/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Constants
#import "ListConstants.h"

@protocol HighlightersPopoverDelegate
- (void)highlighterSelected:(NSInteger)selected;
- (void)dismissHighlightersPopover;
@end


@interface HighlightersViewController : GAITrackedViewController {
	id <HighlightersPopoverDelegate>	delegate;
	
	// Buttons
	UIButton							*redHighlighterButton;
	UIButton							*yellowHighlighterButton;
	UIButton							*whiteHighlighterButton;
	
	// Highlighter color
	EnumHighlighterColor				highlighterColor;

}
@property (nonatomic, assign) UIPopoverController *popover;
@property (nonatomic, assign)	id	delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(id<HighlightersPopoverDelegate>)theDelegate andSize:(CGSize)theSize;

@end
