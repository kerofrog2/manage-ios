//
//  PreviewListItemViewController.m
//  Manage
//
//  Created by Kerofrog on 6/7/13.
//
//

#import "PreviewListItemViewController.h"

// Data Models
#import "TaskList.h"
#import "ListItem.h"
#import "Tag.h"

// Data Collections
#import "TagCollection.h"

// Custom Table View Cells
#import "ListItemTableViewCell.h"

#import "MainConstants.h"

// Method Helper
#import "MethodHelper.h"

// Constants
#define kNotesIconTag		9635
#define kTableViewFrame				CGRectMake(0, 0, 320, 318)
// Constants
#define kPopoverSize				CGSizeMake(320, 318)

// Private Methods
@interface PreviewListItemViewController()
//- (UIImageView *)getCopyOfCellNotesIcon;
@end


@implementation PreviewListItemViewController

@synthesize delegate;
@synthesize arrayTaskList;
@synthesize refListItem;
@synthesize listItemTagCollection;


- (void)viewDidAppear:(BOOL)animated {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3f];
	[self setContentSizeForViewInPopover:kNewSubListItemViewControllerSize];
	[UIView commitAnimations];
		
	if (popoverController) {
		[popoverController setPopoverContentSize:kPopoverSize animated:YES];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
#if ENABLE_GOOGLE_ANALYTICS
    self.trackedViewName = kScreen_PreviewListItemViewController;
#endif
	// Do any additional setup after loading the view.
}

#pragma mark -
#pragma mark Public Set Methods

- (void)setPopoverController:(UIPopoverController *)thePopoverController {
	popoverController = thePopoverController;
}

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    [refTaskList release];
    [refTagCollection release];
    [arrayTaskList release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithTaskList:(TaskList *)theTaskList andTagCollection:(TagCollection *)theTagCollection andListItem:(ListItem *)theListItem {
	if ((self = [super init])) {
		// Assign passed task list to ref task list
		refTaskList = theTaskList;
        
        // Assign passed list item to ref list item
		refListItem = theListItem;
		
		// Assign the passed tag collection
		refTagCollection = theTagCollection;
	}
    
    self.title = @"Choose Parenttask";
    
    [self loadButton];
    [self loadTableViewDataSource];
    [self loadTableView];
    
	return self;
}

#pragma mark -
#pragma mark Load Methoads

- (void)loadButton {
    // Add a review button item to navigation controller
    UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                          target:self
                                          action:@selector(doneMoveArchivesButtonAction)];
    [self.navigationItem setRightBarButtonItem:doneBarButtonItem animated:NO];
}

- (void)loadTableViewDataSource {
    
    currentIndex = -1;
    parentListItem = [[ListItem alloc]init];
    
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
   self.arrayTaskList = [[NSMutableArray alloc] init];
    
    // Not show CompletedTasks mode
    if (isShowCompletedTasks == 0) {
        for (int i = 0; i < [refTaskList.listItems count]; i++) {
            ListItem *item = [refTaskList.listItems objectAtIndex:i];
            
            if ([item parentListItemID] == -1 && item.listItemID != refListItem.listItemID) {
                
                if (item.completed != YES) {
                    [arrayTaskList addObject:item];
                }
            }
        }
    }
    
    // show CompletedTasks mode
    else {
        for (int i = 0; i < [refTaskList.listItems count]; i++) {
            ListItem *item = [refTaskList.listItems objectAtIndex:i];
            if ([item parentListItemID] == -1 && item.listItemID != refListItem.listItemID) {

                [arrayTaskList addObject:item];
            }
        }
    }
}

- (void)loadTableView {
	
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStylePlain];
	//myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 400) style:UITableViewStyleGrouped];
	//[myTableView setAllowsSelection:FALSE];
    [myTableView setBackgroundColor:[UIColor colorWithRed:242.0 / 255.0
                                                    green:242.0 / 255.0
                                                     blue:242.0 / 255.0
                                                    alpha:1.0]];
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	//[myTableView setBackgroundView:nil];
	//[myTableView setBackgroundColor:[UIColor colorWithRed:0.24313 green:0.22352 blue:0.22352 alpha:1.0]];
	
	[self.view addSubview:myTableView];
	[myTableView reloadData];
}

#pragma mark -
#pragma mark Table View Delegates

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {

	return [arrayTaskList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
	// Set up the cell...
	ListItem *listItem = [self.arrayTaskList objectAtIndex:indexPath.row];
    
    ListItemTableViewCell *cell = (ListItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ListItemTableViewCell alloc] initWithPreviewStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		//[cell.contentView addSubview:[self getCopyOfCellNotesIcon]];
	}
	
	// Get the cell notes icon
	//UIImageView *cellNotesIcon = (UIImageView *)[cell.contentView viewWithTag:kNotesIconTag];
    
	cell.tag = listItem.listItemID;
	cell.delegate = self;
	cell.taskCompleted.tag = listItem.listItemID;
	
    cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.taskCompleted.userInteractionEnabled = NO;
	
	cell.taskItem = [NSString stringWithFormat:@"%@", listItem.title];
	
	//[cell.detailTextLabel setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
	//[cell.detailTextLabel setTextColor:[UIColor darkTextColor]];
    
    // Set the delegate and tag for the completed button on our table view cell
    [cell setIndentForPreviewListItem:listItem];
    
	if ([listItem.dueDate length] > 0) {
		NSString *dateToDisplay = @"";
		NSDate *theDate = [MethodHelper dateFromString:listItem.dueDate usingFormat:K_DATEONLY_FORMAT];
		
		// Past is negative, future is positive
		NSInteger difference = [listItem getDueDateDifferenceFromToday];
		
		// 86400 seconds in one day
		if (difference <= -1 && [listItem completed] == FALSE) {
			//[cell.detailTextLabel setTextColor:[UIColor colorWithRed:0.8274509 green:0.0235294 blue:0.0 alpha:1.0]];
			
			dateToDisplay = @"overdue";
		} else {
			//if ([listItem completed]) {
			//	[cell.detailTextLabel setTextColor:[UIColor lightGrayColor]];
			//} else{
			//	[cell.detailTextLabel setTextColor:[UIColor darkTextColor]];
			//}
			dateToDisplay = [MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterMediumStyle withYear:NO];
		}
		cell.taskDate = dateToDisplay;
		//cell.detailTextLabel.text = dateToDisplay;
	} else {
		cell.taskDate = @"";
		//cell.detailTextLabel.text = @"";
	}

    // Set up tags
	[cell loadTagsForListItem:listItem andTagCollection:refTagCollection];
    
    
	// Check to see if this is a repeating task
	if ([listItem recurringListItemID] != -1) {
		[cell loadRepeatingTaskImageForListItem:listItem];
	} else {
		cell.repeatingTaskImageView.image = nil;
	}
    
    // Load scribble image (has to be after tags, because tags modifies taskitemframe
	[cell loadScribbleImageForListItem:listItem andIsPreviewList:YES];
    
    [cell.taskCompleted setSelected:listItem.completed];

    
	if ([listItem completed] == FALSE) {
		[cell.taskCompleted setAlpha:1.0f];
		//[cell.taskItem setTextColor:[UIColor darkTextColor]];
		[cell setHighlightStyle:listItem.priority];
	} else {
		[cell.taskCompleted setAlpha:0.3f];
		//[cell.taskItem setTextColor:[UIColor lightGrayColor]];
		[cell setHighlightStyle:EnumHighlighterColorNone];
	}

	
	//[cell.contentView bringSubviewToFront:cell.highlighterView];
	//[cell.contentView bringSubviewToFront:cell.leftHighlighterImageView];
	//[cell.contentView bringSubviewToFront:cell.rightHighlighterImageView];
	//[cell.contentView bringSubviewToFront:cell.scribbleImageView];
	
	// Display cell notes icon if notes is not empty
	if ([listItem.notes length] > 0) {
		[cell loadCellNotesImage];
	} else {
		cell.cellNotesImageView.image = nil;
	}
	
	// Display cell notes icon if notes is not empty
	/*if ([listItem.notes length] > 0) {
     UIFont *myFont = [UIFont fontWithName:@"Helvetica" size:14.0];
     // Get the width of a string ...
     //NSString *taskItemString = [cell.taskItem text];
     CGSize size = [cell.taskItem sizeWithFont:myFont];
     
     if (size.width > cell.taskItemFrame.size.width) {
     size.width = cell.taskItemFrame.size.width;
     }
     
     //[cellNotesIcon setFrame:CGRectMake(cell.taskItem.frame.origin.x + cell.taskItem.frame.size.width + 35,
     //								   3, cellNotesIcon.frame.size.width, cellNotesIcon.frame.size.height)];
     
     [cellNotesIcon setFrame:CGRectMake(cell.taskItemFrame.origin.x + size.width + 3,
     3, cellNotesIcon.frame.size.width, cellNotesIcon.frame.size.height)];
     
     [cellNotesIcon setHidden:NO];
     } else {
     [cellNotesIcon setHidden:YES];
     }	*/
	
	
	/*if (listItem.priority == EnumHighlighterColorWhite) {
	 [cell.taskItem setBackgroundColor:[UIColor clearColor]];
	 } else if (listItem.priority == EnumHighlighterColorYellow) {
	 [cell.taskItem setBackgroundColor:[UIColor yellowColor]];
	 } else if (listItem.priority == EnumHighlighterColorRed) {
	 [cell.taskItem setBackgroundColor:[UIColor redColor]];
	 }*/
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    // Remove old check mark if it exist
    if (currentIndex!=-1) {
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
        ListItemTableViewCell *oldCell = (ListItemTableViewCell *)[myTableView cellForRowAtIndexPath:oldIndexPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
    }

    // Update current index to select parrent task
    currentIndex = indexPath.row;
	ListItemTableViewCell *cell = (ListItemTableViewCell *)[myTableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    // update parentListItem to move task to
    parentListItem = [self.arrayTaskList objectAtIndex:indexPath.row];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*- (UIImageView *)getCopyOfCellNotesIcon {
 UIImage *notesIconImage = [MethodHelper scaleImage:[UIImage imageNamed:@"NotesIcon.png"]
 maxWidth:22 maxHeight:22];
 UIImageView *cellNotesIcon = [[UIImageView alloc] initWithImage:notesIconImage];
 cellNotesIcon.tag = kNotesIconTag;
 [cellNotesIcon setHidden:YES];
 return cellNotesIcon;
 }*/

#pragma mark -
#pragma mark Action Methods
- (void)doneMoveArchivesButtonAction {

    // chose Task
    if (currentIndex!=-1) {
        
        refTaskList.mainViewRequiresUpdate = TRUE;
        
        // Move item from archives to the correct list
        NSInteger listItemIndex = [refTaskList getIndexOfListItemWithID:refListItem.listItemID];
        
        // Move item from archives to the correct list
        listItemIndex = [refTaskList getIndexOfListItemWithID:refListItem.listItemID];

        
        NSInteger subItemOrder = [parentListItem getNewSubItemOrder];

        // Create the sub list item
        ListItem *newSubListItem = [[ListItem alloc] initNewSubListItemFromListItem:refListItem withListID:refTaskList.listID toParentListItemID:parentListItem.listItemID andParentTitle:parentListItem.title andItemOrder:parentListItem.itemOrder andSubItemOrder:subItemOrder andListItemTagCollection:listItemTagCollection];

        // Get index of parent list item, then increment by 1 to get correct insert index
        NSInteger insertIndex = [refTaskList getIndexOfListItemWithID:parentListItem.listItemID];
        insertIndex++;
        
        if (insertIndex < [refTaskList.listItems count]) {
            [refTaskList.listItems insertObject:newSubListItem atIndex:insertIndex];
            
        } else {
            [refTaskList.listItems addObject:newSubListItem];
        }
        
        listItemIndex = [refTaskList getIndexOfListItemWithID:refListItem.listItemID];
        // Remove the list item from archives list
        [refTaskList deleteListItemRetainScribbleAtIndex:listItemIndex permanent:YES];
        
        [delegate updateTableViewAfterMovingTasks];
    }
    
    else {
        [MethodHelper showAlertViewWithTitle:@"Choose Parenttask" andMessage:@"Please choose a Task" andButtonTitle:@"Ok"];
    }
}

@end

