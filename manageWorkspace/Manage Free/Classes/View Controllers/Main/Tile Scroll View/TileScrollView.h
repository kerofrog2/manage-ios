//
//  TileScrollView.h
//  Manage
//
//  Created by Cliff Viegas on 2/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TaskListCollection;
@class NotebookCollection;

// View Controllers
@class MainViewController;

// Data Models
@class TaskList;
@class Notebook;

// Progress Hud
#import "MBProgressHUD.h"

// Responders
@class ListPadTableViewResponder;

@interface TileScrollView : UIScrollView <MBProgressHUDDelegate, UIActionSheetDelegate> {
	TaskListCollection		*refMasterTaskListCollection;
	TaskListCollection		*refTaskListCollection;
	MainViewController		*refMainViewController;
    NotebookCollection      *refNotebookCollection;
	UIView					*refTouchedView;
	NSTimer					*touchTimer;
	
	CGRect					startingFrame;
	
	CGPoint					touchPointOffset;
	
	NSInteger				currentListIndex;
	
	// Flags
	BOOL					isRearrangingList;
	BOOL					isDraggingList;
	NSInteger				touchedViewStartIndex;
	
	// Buttons
	UIButton				*deleteTileListButton;
	UIButton				*optionsTileListButton;
	UIButton				*openTileListButton;
	UIImageView				*animatedListImage;
	
	// Action Sheets
	UIActionSheet			*deleteTileListActionSheet;
	UIActionSheet			*optionsTileListActionSheet;
    UIActionSheet           *deleteTileNotebookActionSheet;
    UIActionSheet           *optionsTileNoteActionSheet;
	
	// Helper Properties
	BOOL					updated;
	BOOL					tileButtonTouchDisabled;
	CGRect					touchedViewFrame;
	
	ListPadTableViewResponder *refListPadTableViewResponder;
	UITableView				*refListPadTableView;
    
    // Progress HUD
    MBProgressHUD           *progressHUD;
}

@property (nonatomic, assign)	NSInteger					currentListIndex;
@property (nonatomic, assign)	BOOL						updated;
@property (nonatomic, assign)	CGRect						touchedViewFrame;
@property (nonatomic, assign)	BOOL						tileButtonTouchDisabled;
@property (nonatomic, assign)	ListPadTableViewResponder	*refListPadTableViewResponder;
@property (nonatomic, assign)	UITableView					*refListPadTableView;

#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection 
		   andMainViewController:(MainViewController *)theController 
	 andMasterTaskListCollection:(TaskListCollection *)theMasterTaskListCollection
           andNotebookCollection:(NotebookCollection *)theNotebookCollection;

#pragma mark Update Methods
- (void)updateScrollList;
- (void)loadCompletedStampForTile:(UIImageView *)tile;
- (void)loadIconsInTile:(UIImageView *)tileImageView forList:(TaskList *)taskList;
- (void)loadIconsInTile:(UIImageView *)tileImageView forNotebook:(Notebook *)noteBook;
- (void)rearrangeScrollList;
- (void)updateButtonTypes:(NSString *)newButtonType;

#pragma mark Helper Methods
- (void)scrollToSelectedTile:(BOOL)animated;
- (void)selectTileAtIndex:(NSInteger)index;
- (void)showTileButtons;

@end
