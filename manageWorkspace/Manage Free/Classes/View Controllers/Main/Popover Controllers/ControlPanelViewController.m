    //
//  ControlPanelViewController.m
//  Manage
//
//  Created by Cliff Viegas on 23/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "ControlPanelViewController.h"

// Table View Responder
#import "ListPadTableViewResponder.h"

// Constants
#import "MainConstants.h"

// Task List Collection
#import "TaskListCollection.h"

// Notebook collection
#import "NotebookCollection.h"

// Constants
#define kViewControllerSize	CGSizeMake(256, 703)

@implementation ControlPanelViewController

@synthesize refMainViewController;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[controlPanelImageView release];
    [listPadTableViewResponder release];
	[listPadTableView release];
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        self.contentSizeForViewInPopover = CGSizeMake(CGRectGetWidth(kMainControlPanelImageLandscape) + kMainControlPanelImageLandscape.origin.x * 2, CGRectGetHeight(kMainControlPanelImageLandscape) + CGRectGetMinY(kMainControlPanelImageLandscape) * 2);
        self.preferredContentSize = self.contentSizeForViewInPopover;
    }
}

#pragma mark -
#pragma mark Initialisation

/*
 - (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection andDelegate:(id<ListPadDelegate>)theDelegate {

 
 */

- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_ControlPanelViewController;
#endif
        // Assign references to notebook and tasklist collection
        refTaskListCollection = theTaskListCollection;
        refNotebookCollection = theNotebookCollection;
        
        // Create our list pad table view responder
        listPadTableViewResponder = [[ListPadTableViewResponder alloc] initWithTaskListCollection:refTaskListCollection notebookCollection:refNotebookCollection andDelegate:self];
        listPadTableView = [[UITableView alloc] initWithFrame:kListPadTableViewPortrait style:UITableViewStyleGrouped];
        [listPadTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
        [listPadTableView setContentInset:UIEdgeInsetsMake(0, 0, 33, 0)];
        [listPadTableView setBackgroundColor:[UIColor clearColor]];
        [listPadTableView setShowsVerticalScrollIndicator:NO];
        [listPadTableView setDataSource:listPadTableViewResponder];
        [listPadTableView setDelegate:listPadTableViewResponder];
        
        [listPadTableViewResponder setDelegate:self];
		
        // Init ref main view controller to nil
		refMainViewController = nil;
		
		// Set content size
		[self setContentSizeForViewInPopover:kViewControllerSize];
		
		[self.view setBackgroundColor:[UIColor darkGrayColor]];
		
		// Load the control panel
		controlPanelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ControlPanel.png"]];
		
		// Load the list pad table view
		/*listPadTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [listPadTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
		[listPadTableView setBackgroundColor:[UIColor clearColor]];
		[listPadTableView setShowsVerticalScrollIndicator:NO];
		[listPadTableView setDataSource:listPadTableViewResponder];
		[listPadTableView setDelegate:listPadTableViewResponder];*/

		[controlPanelImageView setFrame:kMainControlPanelImageLandscape];
		[listPadImageView setFrame:kListPadImageLandscape];
		[listPadTableView setFrame:kListPadTableViewLandscape];
			//[searchTableView setFrame:kSearchTableViewLandscape];
			//[searchBar setFrame:kSearchBarLandscape];
		
		[self.view addSubview:controlPanelImageView];
		[self.view addSubview:listPadImageView];
		[self.view addSubview:listPadTableView];
        [controlPanelImageView release];
        [listPadImageView release];
        [listPadTableView release];
	}
	return self;
}

#pragma mark -
#pragma mark Button Actions

- (void)archivesButtonAction {
	if (refMainViewController != nil) {
		[refMainViewController archivesSelected];
	}
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



#pragma mark -
#pragma mark List Pad Delegates

- (void)listSelected:(NSInteger)theSelectedRow {
    [refMainViewController listSelected:theSelectedRow];
}

- (void)loadingListsSelected {
    [refMainViewController loadingListsSelected];
}

- (void)notebookSelected:(NSInteger)theSelectedRow {    
    [refMainViewController notebookSelected:theSelectedRow];
}

- (void)archivesSelected {
    [refMainViewController archivesButtonAction];
}

- (void)shopfrontSelectedWithFrame:(CGRect)theFrame {
    [refMainViewController shopfrontSelectedWithFrame:theFrame];
}

- (void)deleteRowsAtIndexPaths:(NSArray *)indexPaths {
    [listPadTableView deleteRowsAtIndexPaths:indexPaths 
                            withRowAnimation:UITableViewRowAnimationTop];
}

- (void)insertRowsAtIndexPaths:(NSArray *)indexPaths {
    [listPadTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
}

- (void)reloadListPadTableView {
    [listPadTableView reloadData];
}


@end
