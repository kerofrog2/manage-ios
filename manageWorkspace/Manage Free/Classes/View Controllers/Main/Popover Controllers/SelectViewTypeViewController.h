//
//  MainViewTypeViewController.h
//  Manage
//
//  Created by Cliff Viegas on 3/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// View type enums


// Delegate
@protocol SelectViewTypeDelegate
- (void)viewTypeSelected:(NSInteger)theViewType;
@end


// Interface
@interface SelectViewTypeViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
	id <SelectViewTypeDelegate>		delegate;
	
	UITableView						*myTableView;
	NSInteger						currentViewType;
}

@property (nonatomic, assign) id	delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(id<SelectViewTypeDelegate>)theDelegate andCurrentViewType:(NSInteger)theViewType;

#pragma mark Load Methods
- (void)loadTableView; 

@end
