//
//  ControlPanelViewController.h
//  Manage
//
//  Created by Cliff Viegas on 23/11/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Table View Responder
#import "ListPadTableViewResponder.h"

// Main View Controller Link
@class MainViewController;

// Data Collections
@class TaskListCollection;
@class NotebookCollection;

@interface ControlPanelViewController : GAITrackedViewController <ListPadDelegate>{
	MainViewController              *refMainViewController;
    ListPadTableViewResponder       *listPadTableViewResponder;
    TaskListCollection              *refTaskListCollection;
    NotebookCollection              *refNotebookCollection;
	
	UIImageView				*controlPanelImageView;				// The rounded rectangle control panel image
	UIImageView				*listPadImageView;					// The list paper background image
	UITableView				*listPadTableView;					// The list pad table view that sits on list pad image view
}

@property (nonatomic, assign)	MainViewController	*refMainViewController;
@property (nonatomic, assign)   UIPopoverController *popover;

#pragma mark Initialisation
- (id)initWithTaskListCollection:(TaskListCollection *)theTaskListCollection notebookCollection:(NotebookCollection *)theNotebookCollection;

@end
