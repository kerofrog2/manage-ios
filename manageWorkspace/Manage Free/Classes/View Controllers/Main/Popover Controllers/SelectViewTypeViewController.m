//
//  MainViewTypeViewController.m
//  Manage
//
//  Created by Cliff Viegas on 3/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "SelectViewTypeViewController.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 110)
#define kPopoverSize		CGSizeMake(320, 110)

@implementation SelectViewTypeViewController

@synthesize delegate;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[myTableView release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithDelegate:(id<SelectViewTypeDelegate>)theDelegate andCurrentViewType:(NSInteger)theViewType {
	if (self = [super init]) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_SelectViewTypeViewController;
#endif
		// Assign the delegate
		self.delegate = theDelegate;
		
		// Assign the current view type
		currentViewType = theViewType;
		
		// Set the title
		self.title = @"Select View";
		
		// Load the tableview
		[self loadTableView];
		
		// Set the popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}	
	
	// Set default values
	[cell setAccessoryType:UITableViewCellAccessoryNone];
	
	// Handle displaying the cell
	if (indexPath.row == 0) {
		// List View
		cell.textLabel.text = @"List";
		cell.detailTextLabel.text = @"A sliding list view";
		if (currentViewType == 0) {
			[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
		}
	} else if (indexPath.row == 1) {
		// Tile View
		cell.textLabel.text = @"Tile";
		cell.detailTextLabel.text = @"A grouped tile view";
		if (currentViewType == 1) {
			[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
		}
	}

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	currentViewType = indexPath.row;
	
	[myTableView reloadData];
	
	[delegate viewTypeSelected:currentViewType];
	
	/*if (indexPath.row == 0) {
		selectedTagID = -1;
	} else {
		UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
		NSInteger tagIndex = [refTagCollection getIndexOfTagWithID:cell.tag];
		Tag *tag = [refTagCollection.tags objectAtIndex:tagIndex];
		selectedTagID = tag.tagID;
	}
	
	[tableView reloadData];
	[self.navigationController popViewControllerAnimated:YES];*/
}

#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





@end
