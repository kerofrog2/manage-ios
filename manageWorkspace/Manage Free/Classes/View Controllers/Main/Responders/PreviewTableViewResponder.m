//
//  PreviewTableViewResponder.m
//  Manage
//
//  Created by Cliff Viegas on 20/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "PreviewTableViewResponder.h"

// Data Models
#import "TaskList.h"
#import "ListItem.h"
#import "Tag.h"

// Data Collections
#import "TagCollection.h"
#import "AlarmCollection.h"

// Custom Table View Cells
#import "ListItemTableViewCell.h"

#import "MainConstants.h"

// Method Helper
#import "MethodHelper.h"
// Alert Time
#import "AlertTime.h"

// Constants
#define kNotesIconTag		9635

// Private Methods
@interface PreviewTableViewResponder()
//- (UIImageView *)getCopyOfCellNotesIcon;
@end


@implementation PreviewTableViewResponder
@synthesize arrayTaskList;
@synthesize refTaskList;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    
    id temp = refTaskList;
    refTaskList = nil;
    [temp release];
    
    //[refTaskList release];
    [refTagCollection release];
    [arrayTaskList release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithTaskList:(TaskList *)theTaskList andTagCollection:(TagCollection *)theTagCollection {
	if ((self = [super init])) {
		// Assign passed task list to ref task list
		self.refTaskList = theTaskList;
		
		// Assign the passed tag collection
		refTagCollection = theTagCollection;
	}
	return self;
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ListItemTableViewCell in Preview Table View Responder";
    
	// Set up the cell...
	ListItem *listItem = [self.arrayTaskList objectAtIndex:indexPath.row];
    
    ListItemTableViewCell *cell = (ListItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ListItemTableViewCell alloc] initWithPreviewStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		//[cell.contentView addSubview:[self getCopyOfCellNotesIcon]];
	}
	
	// Get the cell notes icon
	//UIImageView *cellNotesIcon = (UIImageView *)[cell.contentView viewWithTag:kNotesIconTag];
    
	cell.tag = listItem.listItemID;
	cell.delegate = self;
	cell.taskCompleted.tag = listItem.listItemID;
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	cell.taskItem = [NSString stringWithFormat:@"%@", listItem.title];
	
	//[cell.detailTextLabel setFont:[UIFont fontWithName:@"Helvetica" size:11.0]];
	//[cell.detailTextLabel setTextColor:[UIColor darkTextColor]];
	
    // Set the delegate and tag for the completed button on our table view cell
    [cell setIndentForPreviewListItem:listItem];
    
    //[cell.taskCompleted setSelected:listItem.completed];
    
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    if (isShowCompletedTasks == 1) {
        [cell.taskCompleted setSelected:listItem.completed];
        if (listItem.hasJustComplete == YES) {
            [cell.taskCompleted setSelected:listItem.hasJustComplete];
        }
    } else {
        [cell.taskCompleted setSelected:listItem.hasJustComplete];
    }

	//[cell.taskCompleted setSelected:listItem.completed];
    
	if ([listItem.dueDate length] > 0) {
		NSString *dateToDisplay = @"";
		NSDate *theDate = [MethodHelper dateFromString:listItem.dueDate usingFormat:K_DATEONLY_FORMAT];
		
		// Past is negative, future is positive
		NSInteger difference = [listItem getDueDateDifferenceFromToday];
		
		// 86400 seconds in one day
		if (difference <= -1 && [listItem completed] == FALSE) {
			//[cell.detailTextLabel setTextColor:[UIColor colorWithRed:0.8274509 green:0.0235294 blue:0.0 alpha:1.0]];
			
			dateToDisplay = @"overdue";
		} else {
			//if ([listItem completed]) {
			//	[cell.detailTextLabel setTextColor:[UIColor lightGrayColor]];
			//} else{
			//	[cell.detailTextLabel setTextColor:[UIColor darkTextColor]];
			//}
			dateToDisplay = [MethodHelper localizedDateFrom:theDate usingStyle:NSDateFormatterMediumStyle withYear:NO];
		}
		cell.taskDate = dateToDisplay;
		//cell.detailTextLabel.text = dateToDisplay;
	} else {
		cell.taskDate = @"";
		//cell.detailTextLabel.text = @"";
	}
    
	// Set up tags
    if (_shouldShowTags) {
        [cell loadTagsForListItem:listItem andTagCollection:refTagCollection];
    }
	
    // Check to see if this is a repeating task
	if ([listItem recurringListItemID] != -1) {
		[cell loadRepeatingTaskImageForListItem:listItem];
	} else {
		cell.repeatingTaskImageView.image = nil;
	}
    
    // Check show icon alarm
    if (listItem.hasAlarms == TRUE) {
        
        // Init any loaded alarms
        AlarmCollection *alarmCollection = [[AlarmCollection alloc] init];
        [alarmCollection loadAlarmsWithListItemID:listItem.listItemID];
        
        // Update reflistItem data if no alarms found
        if ([alarmCollection.alarms count] == 0) {
            
            // Set has alarm is false
            listItem.hasAlarms = FALSE;
            [listItem updateDatabase];
            
            cell.alarmTaskImageView.image = nil;
            
        }else{
            if (listItem.completed != YES && listItem.hasJustComplete != YES) {
                [cell loadAlarmTaskImage:@"AlarmIcon.png" andListItem:listItem];
            }else{
                cell.alarmTaskImageView.image = nil;
            }
        }
        
    }else{
        cell.alarmTaskImageView.image = nil;
    }
    
    // Load scribble image (has to be after tags, because tags modifies taskitemframe
	[cell loadScribbleImageForListItem:listItem andIsPreviewList:YES];
    
	if ([listItem completed] == TRUE || listItem.hasJustComplete == TRUE) {
        [cell.taskCompleted setAlpha:0.3f];
        [cell.scribbleImageView setAlpha:0.3f];
		//[cell.taskItem setTextColor:[UIColor lightGrayColor]];
		[cell setHighlightStyle:EnumHighlighterColorNone];
	} else {
        [cell.taskCompleted setAlpha:1.0f];
        [cell.scribbleImageView setAlpha:1.0f];
		//[cell.taskItem setTextColor:[UIColor darkTextColor]];
		[cell setHighlightStyle:listItem.priority];
        
	}
	
	//[cell.contentView bringSubviewToFront:cell.highlighterView];
	//[cell.contentView bringSubviewToFront:cell.leftHighlighterImageView];
	//[cell.contentView bringSubviewToFront:cell.rightHighlighterImageView];
	//[cell.contentView bringSubviewToFront:cell.scribbleImageView];
	
	// Display cell notes icon if notes is not empty
	if ([listItem.notes length] > 0) {
		[cell loadCellNotesImage];
	} else {
		cell.cellNotesImageView.image = nil;
	}
	
	// Display cell notes icon if notes is not empty
	/*if ([listItem.notes length] > 0) {
		UIFont *myFont = [UIFont fontWithName:@"Helvetica" size:14.0];
		// Get the width of a string ...
		//NSString *taskItemString = [cell.taskItem text];
		CGSize size = [cell.taskItem sizeWithFont:myFont];
		
		if (size.width > cell.taskItemFrame.size.width) {
			size.width = cell.taskItemFrame.size.width;
		}
		
		//[cellNotesIcon setFrame:CGRectMake(cell.taskItem.frame.origin.x + cell.taskItem.frame.size.width + 35, 
		//								   3, cellNotesIcon.frame.size.width, cellNotesIcon.frame.size.height)];
		
		[cellNotesIcon setFrame:CGRectMake(cell.taskItemFrame.origin.x + size.width + 3, 
										   3, cellNotesIcon.frame.size.width, cellNotesIcon.frame.size.height)];
		
		[cellNotesIcon setHidden:NO];
	} else {
		[cellNotesIcon setHidden:YES];
	}	*/
	
	
	/*if (listItem.priority == EnumHighlighterColorWhite) {
	 [cell.taskItem setBackgroundColor:[UIColor clearColor]];
	 } else if (listItem.priority == EnumHighlighterColorYellow) {
	 [cell.taskItem setBackgroundColor:[UIColor yellowColor]];
	 } else if (listItem.priority == EnumHighlighterColorRed) {
	 [cell.taskItem setBackgroundColor:[UIColor redColor]];
	 }*/
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    
    NSInteger isShowCompletedTasks = [[NSUserDefaults standardUserDefaults] integerForKey:showCompletedTasksSetting];
    
    if (self.refTaskList.listItems.retainCount > 0) {
        
        self.arrayTaskList = [self.refTaskList.listItems mutableCopy];
        
        if (isShowCompletedTasks == 0) {
            self.arrayTaskList = [[NSMutableArray alloc] init];
            for (int i = 0; i < [refTaskList.listItems count]; i++) {
                ListItem *item = [refTaskList.listItems objectAtIndex:i];
                if (item != nil) {
                    if (item.completed != YES) {
                        [arrayTaskList addObject:item];
                    }
                }
            }
        }
    }
    
	return [arrayTaskList count];
}

/*- (UIImageView *)getCopyOfCellNotesIcon {
	UIImage *notesIconImage = [MethodHelper scaleImage:[UIImage imageNamed:@"NotesIcon.png"] 
											  maxWidth:22 maxHeight:22];
	UIImageView *cellNotesIcon = [[UIImageView alloc] initWithImage:notesIconImage];
	cellNotesIcon.tag = kNotesIconTag;
	[cellNotesIcon setHidden:YES];
	return cellNotesIcon;
}*/

@end
