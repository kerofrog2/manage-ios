//
//  InAppPurchaseManager.h
//  Manage - To Do Lists
//
//  Created by Cliff Viegas on 21/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Store Kit
#import <StoreKit/StoreKit.h>



// Constants
#define kInAppPurchaseManagerTransactionFailedNotification @"kInAppPurchaseManagerTransactionFailedNotification"
#define kInAppPurchaseManagerTransactionSucceededNotification @"kInAppPurchaseManagerTransactionSucceededNotification"
#define kInAppPurchaseManagerProductsFetchedNotification @"kInAppPurchaseManagerProductsFetchedNotification"

// Product id Constants
#define kInAppPurchasePremiumThemeNinjaBunniesID @"MANAGEFREE_NINJABUNNIES_THEME"
#define kInAppPurchasePremiumThemePurpleHazeID @"MANAGEFREE_PURPLEHAZE_THEME"
#define kInAppPurchasePremiumThemeOldTimerID @"MANAGEFREE_OLDTIMER_THEME"
#define kInAppPurchasePremiumThemeSweetCravingsID @"MANAGEFREE_SWEETCRAVINGS_THEME"
#define kInAppPurchasePremiumThemeNitroBurnID @"MANAGEFREE_NITROBURN_THEME"

#define kInAppPurchaseRemoveAdvertisingID @"MANAGEFREE_REMOVE_ADVERTISING"

// Receipt Constants
#define kInAppPurchasePremiumThemeNinjaBunniesReceipt @"premiumThemeNinjaBunniesReceipt"
#define kInAppPurchasePremiumThemePurpleHazeReceipt @"premiumThemePurpleHazeReceipt"
#define kInAppPurchasePremiumThemeOldTimerReceipt @"premiumThemeOldTimerReceipt"
#define kInAppPurchasePremiumThemeSweetCravingsReceipt @"premiumThemeSweetCravingsReceipt"
#define kInAppPurchasePremiumThemeNitroBurnReceipt @"premiumThemeNitroBurnReceipt"

#define kInAppPurchaseRemoveAdvertisingReceipt @"MANAGE_FREE_REMOVE_ADVERTISING_RECEIPT"

// Provide Content Constants
#define kInAppPurchasePremiumThemeNinjaBunniesProvideContent @"premiumThemeNinjaBunniesProvideContent"
#define kInAppPurchasePremiumThemePurpleHazeProvideContent @"premiumThemePurpleHazeProvideContent"
#define kInAppPurchasePremiumThemeOldTimerProvideContent @"premiumThemeOldTimerProvideContent"
#define kInAppPurchasePremiumThemeSweetCravingsProvideContent @"premiumThemeSweetCravingsProvideContent"
#define kInAppPurchasePremiumThemeNitroBurnProvideContent @"premiumThemeNitroBurnProvideContent"

#define kInAppPurchaseRemoveAdvertisingProvideContent @"MANAGE_FREE_REMOVE_ADVERTISING_PROVIDE_CONTENT"

@interface InAppPurchaseManager : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    SKProduct               *proUpgradeProduct;
    SKProductsRequest       *productsRequest;
    NSMutableArray          *products;
}

@property   (nonatomic, retain)        NSMutableArray   *products;

+ (id)sharedPurchaseManager;

//+ (InAppPurchaseManager *)sharedPurchaseManager;

// Public methods
- (void)requestProUpgradeProductData;
- (void)requestRemoveAdvertisingData;
- (void)loadStore;
- (BOOL)canMakePurchases;
- (void)purchaseUpgrade:(NSString *)productID;
- (NSString *)getPremiumThemeName:(NSString *)productID;
- (BOOL)contentPurchased:(NSString *)productID;

- (void)checkPurchasedItems;
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue;


@end
