//
//  ShopFrontViewController.m
//  Manage
//
//  Created by Cliff Viegas on 19/07/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ShopFrontViewController.h"

// In App Purchase Manager
#import "InAppPurchaseManager.h"

// Store Kit (for sk product)
#import <StoreKit/StoreKit.h>

// Method Helper
#import "MethodHelper.h"

// Flurry
#import "FlurryAPI.h"

#define kTableViewFrame     CGRectMake(35, 75, 361, 440)

@interface ShopFrontViewController()
// Private methods
- (void)loadTableView;
@end

@implementation ShopFrontViewController

@synthesize delegate;

#pragma mark - Memory Management

- (void)dealloc {
    [myTableView release];
    [titlesArray release];
    [imagesArray release];
    [activityIndicatorView release];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kInAppPurchaseManagerProductsFetchedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kInAppPurchaseManagerTransactionSucceededNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kInAppPurchaseManagerTransactionFailedNotification object:nil];
    
    [super dealloc];
}

#pragma mark - Initialisation

- (id)initWithSize:(CGSize)theSize {
    self = [super init];
    
    if (self) {
        // Set the popover size
        [self setContentSizeForViewInPopover:theSize];
        
        // Create notification center observer
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inAppProductsFetched:) name:kInAppPurchaseManagerProductsFetchedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transactionSucceeded:) name:kInAppPurchaseManagerTransactionSucceededNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transactionFailed:) name:kInAppPurchaseManagerTransactionFailedNotification object:nil];
        
        // Size is 441, 551
        // Create the titles array
        titlesArray = [[NSArray alloc] initWithObjects:@"Nitro Burn", @"Violet Utopia", @"Ninja Bunnies", @"Old Timer", @"Sweet Cravings", nil];
        imagesArray = [[NSArray alloc] initWithObjects:@"NitroBurnBorder.png", @"VioletUtopiaBorder.png", @"NinjaBunniesBorder.png", @"OldTimerBorder.png", @"SweetCravingsBorder.png", nil];
        
        // Add a background view
        UIImageView *shopFrontImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ShopFront.png"]];
        [self.view addSubview:shopFrontImageView];
        [shopFrontImageView release];
        
        [self loadTableView];
        
        // Create the header and footer lines
        UIView *headerLine1 = [[UIView alloc] initWithFrame:CGRectMake(35, 74, 361, 1)];
        [headerLine1 setBackgroundColor:[UIColor grayColor]];
        [headerLine1 setAlpha:0.7f];
        [self.view addSubview:headerLine1];
        [headerLine1 release];
        
        UIView *headerLine2 = [[UIView alloc] initWithFrame:CGRectMake(35, 72, 361, 1)];
        [headerLine2 setBackgroundColor:[UIColor grayColor]];
        [headerLine2 setAlpha:0.7f];
        [self.view addSubview:headerLine2];
        [headerLine2 release];
     
        UIView *footerLine = [[UIView alloc] initWithFrame:CGRectMake(35, 515, 361, 1)];
        [footerLine setBackgroundColor:[UIColor grayColor]];
        [footerLine setAlpha:0.7f];
        [self.view addSubview:footerLine];
        [footerLine release];
        
        // Create a restore button to restore transactions
        // Need a button, label, and two imageviews
        UIButton *restoreTransactionsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [restoreTransactionsButton addTarget:self action:@selector(restoreTransactionsButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [restoreTransactionsButton setTitle:@"Restore" forState:UIControlStateNormal];
        [restoreTransactionsButton setFrame:CGRectMake(310, 34, 90, 31)];
        [self.view addSubview:restoreTransactionsButton];
        
        // Init our activity indicator view
        activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [activityIndicatorView setFrame:CGRectMake(205, 262, 21, 21)];
        [activityIndicatorView startAnimating];
        [self.view addSubview:activityIndicatorView];
        
        [[InAppPurchaseManager sharedPurchaseManager] loadStore];
    }
    
    return self;
}



#pragma mark - Load Methods

- (void)loadTableView {
    myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [myTableView setBackgroundColor:[UIColor clearColor]];
    [myTableView setShowsVerticalScrollIndicator:FALSE];
    
    [self.view addSubview:myTableView];
}

// Now, to use a tableview or fixed images?....  Just use fixed for now?...  Tableview will probably be easier...

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    ShopFrontTableViewCell *cell = (ShopFrontTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ShopFrontTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		//[cell.contentView addSubview:[self getTextFieldForIndexPath:indexPath]];
	}	
    
    if ([[[InAppPurchaseManager sharedPurchaseManager] products] count] == 0) {
        [cell.itemPurchaseButton setHidden:YES];
        [cell.itemPreviewButton setHidden:YES];
        return cell;
    } else {
        [cell.itemPurchaseButton setHidden:NO];
        [cell.itemPreviewButton setHidden:NO];
    }
    
    // Set the delegate
    cell.delegate = self;
    
   /* cell.itemScreenshot.image = [UIImage imageNamed:[imagesArray objectAtIndex:indexPath.row]];
    cell.itemTypeLabel.text = @"Premium Themes";
    cell.itemTitleLabel.text = @"Sweet Cravings";
    cell.itemPriceLabel.text = @"0.99";
    cell.itemDescriptionLabel.text = @" cell.itemScreenshot.image = [UIImage imageNamed:[self getImageNameForProduct:product.productIdentifier]];cell.itemTypeLabel.text = Premium Themes; cell.itemTitleLabel.text = product.localizedTitle;NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];";
    [cell setSelectionStyle:UITableViewCellEditingStyleNone];
    */
    
    SKProduct *product = [[[InAppPurchaseManager sharedPurchaseManager] products] objectAtIndex:indexPath.row];
    
    cell.itemScreenshot.image = [UIImage imageNamed:[self getImageNameForProduct:product.productIdentifier]];
    cell.itemTypeLabel.text = @"Premium Themes";
    
    // Vertical align the description
    CGSize stringSize = [product.localizedDescription sizeWithFont:cell.itemDescriptionLabel.font 
                            constrainedToSize:CGSizeMake(cell.itemDescriptionLabel.frame.size.width, cell.itemDescriptionLabel.frame.size.width) 
                                lineBreakMode:UILineBreakModeWordWrap];
    cell.itemDescriptionLabel.frame = CGRectMake(cell.itemDescriptionLabel.frame.origin.x, 
                                                 cell.itemDescriptionLabel.frame.origin.y, 
                                                 cell.itemDescriptionLabel.frame.size.width, 
                                                 stringSize.height);
    cell.itemDescriptionLabel.text = product.localizedDescription;
    
    cell.itemProductID = product.productIdentifier;
    
    cell.itemTitleLabel.text = product.localizedTitle;
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:product.priceLocale];
    NSString *formattedString = [numberFormatter stringFromNumber:product.price];
    [numberFormatter release];
    [cell.itemPriceLabel setText:formattedString];
    
    // Check to see if this product has already been purchased
    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:cell.itemProductID] == TRUE) {
        [cell.itemPriceLabel setText:@"[Purchased]"];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	
    return cell;
}


- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    NSInteger count = [[[InAppPurchaseManager sharedPurchaseManager] products] count];
    
    if (count == 0) {
        count = 1;
    } 

	return count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 202;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark - Class Methods

- (NSString *)getImageNameForProduct:(NSString *)productID {
    if ([productID isEqualToString:kInAppPurchasePremiumThemeSweetCravingsID]) {
        return @"SweetCravingsPreview.png";
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemeOldTimerID]) {
        return @"OldTimerPreview.png";
    } else if ([productID isEqualToString:kInAppPurchasePremiumThemePurpleHazeID]) {
        return @"PurpleHazePreview.png";
    }  else if ([productID isEqualToString:kInAppPurchasePremiumThemeNinjaBunniesID]) {
        return @"NinjaBunniesPreview.png";
    }  else if ([productID isEqualToString:kInAppPurchasePremiumThemeNitroBurnID]) {
        return @"NitroBurnPreview.png";
    }  
    
    return @"NitroBurnPreview.png";
}

#pragma mark - Action Methods

- (void)restoreTransactionsButtonAction {
    [[InAppPurchaseManager sharedPurchaseManager] checkPurchasedItems];
}

#pragma mark - Notification Responders

- (void)inAppProductsFetched:(NSNotification *)notification {
  //  NSLog(@"notification description: %@", [notification description]);

    
    //InAppPurchaseManager *purchaseManager = [InAppPurchaseManager sharedPurchaseManager];

    
    //NSLog(@"number of products: %d", [[(InAppPurchaseManager *)notification products] count]);
    [activityIndicatorView stopAnimating];
    [activityIndicatorView removeFromSuperview];
    
    //NSLog(@"FETCHED");
    // Reload the table view
    [myTableView reloadData];
}

- (void)transactionSucceeded:(NSNotification *)notification {
    SKPaymentTransaction *transaction = [[notification userInfo] objectForKey:@"transaction"];
    
    [MethodHelper showAlertViewWithTitle:@"Success" andMessage:@"Your new theme has been set for you.  In future you can choose this theme from the Themes section in Settings." andButtonTitle:@"Ok"];

    NSString *themeName = [[InAppPurchaseManager sharedPurchaseManager] getPremiumThemeName:transaction.payment.productIdentifier];

    //[FlurryAPI logEvent:[NSString stringWithFormat:@"%@ purchased", themeName]];
    
    [self.delegate purchasedTheme:themeName];
}

- (void)transactionFailed:(NSNotification *)notification {
    //SKPaymentTransaction *transaction = [[notification userInfo] objectForKey:@"transaction"];
    
   [MethodHelper showAlertViewWithTitle:@"Failure" andMessage:@"The purchase failed, please try again later or contact manage@kerofrog.com.au for support" andButtonTitle:@"Ok"];
}

#pragma mark - Table View Cell Delegates

- (void)purchaseButtonPressed:(NSString *)productID {
    // Need to set this as the new theme
    [[InAppPurchaseManager sharedPurchaseManager] purchaseUpgrade:productID];
}

- (void)previewButtonPressed:(NSString *)productID {
    // Need to get the product name
    [self.delegate previewTheme:[[InAppPurchaseManager sharedPurchaseManager] getPremiumThemeName:productID]];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

@end
