//
//  SelectListItemTagViewController.h
//  Manage
//
//  Created by Cliff Viegas on 23/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TagCollection;

// Data Models
@class Tag;

@protocol SelectListItemTagDelegate
- (void)tagIDSelected:(NSInteger)theSelectedTagID;
@end


@interface SelectListItemTagViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
	id <SelectListItemTagDelegate>	delegate;
	
	NSInteger						selectedTagID;
	TagCollection					*refTagCollection;
	UITableView						*myTableView;
}

@property (nonatomic, assign)	id	delegate;

#pragma mark Initialisation
- (id)initWithDelegate:(id<SelectListItemTagDelegate>)theDelegate andTagCollection:(TagCollection *)theTagCollection andSelectedTagID:(NSInteger)theSelectedTagID;

@end
