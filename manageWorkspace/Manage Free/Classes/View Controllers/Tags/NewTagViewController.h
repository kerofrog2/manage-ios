//
//  NewTagViewController.h
//  Manage
//
//  Created by Cliff Viegas on 16/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data Collections
@class TagCollection;

// Data Models
@class Tag;
@class PropertyDetail;
@class ApplicationSetting;

// View Controllers (import for delegates)
#import "SelectListItemTagViewController.h"
#import "ListSettingViewController.h"

// Delegates
#import "NewTagDelegate.h"

@interface NewTagViewController : GAITrackedViewController <ListSettingDelegate, SelectListItemTagDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
	id <NewTagDelegate>	delegate;
	
	UITableView			*myTableView;
	UIBarButtonItem		*doneBarButtonItem;
	
	TagCollection		*refTagCollection;
	NSInteger			selectedTagID;
    
    // Colour settings
    ApplicationSetting  *tagColourSetting;
    PropertyDetail      *tagColourPropertyDetail;
}

@property (nonatomic, assign)	id		delegate;

#pragma mark Initialisation
- (id)initWithTagCollection:(TagCollection *)theTagCollection andSelectedTagID:(NSInteger)theSelectedTagID;
- (id)initWithDelegate:(id<NewTagDelegate>)theDelegate andTagCollection:(TagCollection *)theTagCollection andSelectedTagID:(NSInteger)theSelectedTagID;
@end
