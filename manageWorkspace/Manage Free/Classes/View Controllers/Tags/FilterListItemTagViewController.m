//
//  FilterListItemTagViewController.m
//  Manage
//
//  Created by Cliff Viegas on 26/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "FilterListItemTagViewController.h"

// Data Collections
#import "TagCollection.h"
#import "ListItemTagCollection.h"

// Data Models
#import "Tag.h"
#import "ListItemTag.h"

// Constants
#define kTableViewFrame		CGRectMake(0, 0, 320, 318)
#define kPopoverSize		CGSizeMake(320, 318)

@interface FilterListItemTagViewController()
// Private methods
- (void)loadTableView;
@end


@implementation FilterListItemTagViewController

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        _popover.popoverContentSize = CGSizeMake(kTableViewFrame.size.width, kTableViewFrame.size.height);
    }
}

- (void)viewDidUnload {
    _popover = nil;
    [super viewDidUnload];
}

#pragma mark -
#pragma mark Initialisation

// Need to keep filterListItemTagCollection as a List View item in header, to work out if it is populated
- (id)initWithFilterListItemTagCollection:(ListItemTagCollection *)theFilterListItemTagCollection 
						 andTagCollection:(TagCollection *)theTagCollection {
	if ((self = [super init])) {
#if ENABLE_GOOGLE_ANALYTICS
        self.trackedViewName = kScreen_FilterListItemTagViewController;
#endif
		// Assign the filter list tag collection
		refFilterListItemTagCollection = theFilterListItemTagCollection;
		
		// Assign the tag collection
		refTagCollection = theTagCollection;
		
		// Set the title
		self.title = @"Filter by Tags";
		
		// Create a clear bar button item
		UIBarButtonItem *clearBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered 
																			  target:self action:@selector(clearBarButtonItemAction)];
		[self.navigationItem setRightBarButtonItem:clearBarButtonItem animated:NO];
		[clearBarButtonItem release];
		
		// Load the table view
		[self loadTableView];
		
		// Set popover size
		[self setContentSizeForViewInPopover:kPopoverSize];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadTableView {
	myTableView = [[UITableView alloc] initWithFrame:kTableViewFrame style:UITableViewStyleGrouped];
	
	[myTableView setDelegate:self];
	[myTableView setDataSource:self];
	
	[self.view addSubview:myTableView];
}

#pragma mark -
#pragma mark Button Actions

- (void)clearBarButtonItemAction {
	// Clear all selected tags
	[refFilterListItemTagCollection.listItemTags removeAllObjects];
	
	// Reload the tableview
	[myTableView reloadData];
}	

#pragma mark -
#pragma mark Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [refTagCollection.tags count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}	

	// Set default values
	[cell setIndentationWidth:20.0];
	[cell setIndentationLevel:0];
	[cell setAccessoryType:UITableViewCellAccessoryNone];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.imageView.image = [UIImage imageNamed:@"TagNoRightBuffer.png"];
	
	// Display the cell
	Tag *tag = [refTagCollection.tags objectAtIndex:indexPath.row];
	cell.textLabel.text = tag.name;
	cell.tag = tag.tagID;
	[cell setIndentationLevel:tag.tagDepth];
	
	if ([refFilterListItemTagCollection containsTagWithID:tag.tagID]) {
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	if ([refFilterListItemTagCollection containsTagWithID:cell.tag]) {
		// Remove it
		NSInteger tagIndex = [refFilterListItemTagCollection getIndexOfTagWithID:cell.tag];
		[refFilterListItemTagCollection.listItemTags removeObjectAtIndex:tagIndex];
	} else {
		ListItemTag *listItemTag = [[ListItemTag alloc] initWithListItemID:-1 andTagID:cell.tag];
		[refFilterListItemTagCollection.listItemTags addObject:listItemTag];
		[listItemTag release];
	}
	
	[myTableView reloadData];
}




#pragma mark -
#pragma mark UIViewController Delegates

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}




@end
