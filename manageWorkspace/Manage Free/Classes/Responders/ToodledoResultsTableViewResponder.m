//
//  ToodledoResultsTableViewResponder.m
//  Manage
//
//  Created by Cliff Viegas on 14/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "ToodledoResultsTableViewResponder.h"

// This is where we need to have collections to work with, will have
// to pass in requisite arrays

@implementation ToodledoResultsTableViewResponder

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		
	}
	return self;
}

#pragma mark -
#pragma mark Table View Delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
	
	// Set up the cell...
	// Perhaps good idea to do custom tableviewcell of this... but performance cost unless optomized
	cell.textLabel.text = @"test line";
	
	
    return cell;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return 10;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Return nil to select nothing
	//return nil;
	return indexPath;
}

@end
