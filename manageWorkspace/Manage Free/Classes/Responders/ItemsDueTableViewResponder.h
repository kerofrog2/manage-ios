//
//  ItemsDueTableViewDelegate.h
//  Manage
//
//  Created by Cliff Viegas on 4/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Data Models
@class ListItem;

// Data Collections
@class ItemsDueCollection;
@class TaskListCollection;

@protocol ItemsDueResponderDelegate
// No need, we have the link to the list item, this just needs to update table
- (void)listItemSelected:(ListItem *)selectedListItem atLocation:(CGRect)popOverRect;
@end


@interface ItemsDueTableViewResponder : NSObject <UITableViewDelegate, UITableViewDataSource> {
	id <ItemsDueResponderDelegate>		delegate;
	
	ItemsDueCollection					*itemsDueCollection;
	TaskListCollection					*refTaskListCollection;
	
	NSString							*currentParentListTitle;
	NSInteger							currentParentListID;
	UIColor								*textLabelColour;
	UIColor								*detailTextLabelColour;
	UIColor								*listLabelColour;
}

@property (nonatomic, assign)		id			delegate;

@property (nonatomic, retain)		UIColor		*textLabelColour;
@property (nonatomic, retain)		UIColor		*detailTextLabelColour;
@property (nonatomic, retain)		UIColor		*listLabelColour;

#pragma mark Initialisation
- (id)initWithDelegate:(id<ItemsDueResponderDelegate>)theDelegate andParentListTitle:(NSString *)theTitle 
	   andParentListID:(NSInteger)theParentListID andTaskListCollection:(TaskListCollection *)theTaskListCollection; 
- (void)reloadTableView:(UITableView *)tableView;

#pragma mark Class Methods
- (void)reloadDataWithListParentTitle:(NSString *)theTitle andTableView:(UITableView *)tableView andParentListID:(NSInteger)theParentListID 
						  andListItem:(ListItem *)listItem;
@end
