//
//  DeletedListItemCollection.m
//  Manage
//
//  Created by Cliff Viegas on 30/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "DeletedListItemCollection.h"

// Business Layer
#import "BLDeletedListItem.h"

// Data Models
#import "DeletedListItem.h"

// DM Helper
#import "DMHelper.h"

@implementation DeletedListItemCollection

@synthesize deletedListItems;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[self.deletedListItems release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)init {
	if (self = [super init]) {
		// Init the deleted list items array
		self.deletedListItems = [NSMutableArray array];
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

- (void)loadWithListItemsDeletedAfter:(NSString *)dateTimeString {
	// Remove all deleted list items from array (although shouldn't be any)
	[self.deletedListItems removeAllObjects];
	
	// Get the array of deleted lists
	NSArray *localArray = [BLDeletedListItem getAllDeletedListItems];
	
	// Create a new object for each deleted list taken from database
	for (NSDictionary *listItemDictionary in localArray) {
		DeletedListItem *deletedListItem = [[[DeletedListItem alloc] init] autorelease];
		
		deletedListItem.listItemID = [DMHelper getNSIntegerValueForKey:@"ListItemID" fromDictionary:listItemDictionary];
		deletedListItem.title = [DMHelper getNSStringValueForKey:@"Title" fromDictionary:listItemDictionary];
		deletedListItem.listID = [DMHelper getNSIntegerValueForKey:@"ListID" fromDictionary:listItemDictionary];
		deletedListItem.parentListItemID = [DMHelper getNSIntegerValueForKey:@"ParentListItemID" fromDictionary:listItemDictionary];
		deletedListItem.toodledoTaskID = [DMHelper getSInt64ValueForKey:@"ToodledoTaskID" fromDictionary:listItemDictionary];
		deletedListItem.deletedDateTime = [DMHelper getNSStringValueForKey:@"DeletedDateTime" fromDictionary:listItemDictionary];
		
		// Need to compare dates
		NSComparisonResult comparisonResult = [deletedListItem.deletedDateTime compare:dateTimeString];
		
		// Means our dateTimeString (last sync datetime) is earlier than the deletion date, so get it
		if (comparisonResult == NSOrderedDescending) {
			[self.deletedListItems addObject:deletedListItem];
		} 
		
	}
}

@end
