//
//  ListCollection.h
//  Manage
//
//  Created by Cliff Viegas on 17/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class ListItemTag;
@class TaskList;
@class ArchiveList;
@class ListItem;
@class ToodledoFolder;

// Data Collections
@class ListItemTagCollection;
@class TagCollection;

typedef enum {
	EnumCompletedDurationAlways = 0,
	EnumCompletedDurationOneDay = 1,
	EnumCompletedDurationOneWeek = 7,
	EnumCompletedDurationFourWeeks = 28
} EnumCompletedDuration;

@interface TaskListCollection : NSObject {
	NSMutableArray	*lists;
	BOOL			showCompletedTasks;
	NSInteger		showCompletedTasksDuration;
	
	// Helper property
	NSInteger		currentTaskListID;
	BOOL			sortOrderUpdated;
	NSString		*archiveSetting;		// Archive duration setting
	BOOL			pocketReminder;
	NSString		*defaultView;
	NSString		*theNewListOrder;
	NSString		*theNewTaskOrder;
	NSString		*badgeNotification;
	NSString		*defaultSortOrder;
	NSString		*selectedTheme;			// The currently selected theme
    NSString        *previewTheme;          // The currently selected preview theme
}

@property (nonatomic, retain)		NSMutableArray	*lists;
@property (nonatomic, assign)		BOOL			showCompletedTasks;
@property (nonatomic, assign)		NSInteger		showCompletedTasksDuration;
@property (nonatomic, assign)		NSInteger		currentTaskListID;
@property (nonatomic, assign)		BOOL			sortOrderUpdated;
@property (nonatomic, copy)			NSString		*archiveSetting;
@property (nonatomic, assign)		BOOL			pocketReminder;
@property (nonatomic, copy)			NSString		*defaultView;
@property (nonatomic, copy)			NSString		*theNewListOrder;
@property (nonatomic, copy)			NSString		*theNewTaskOrder;
@property (nonatomic, copy)			NSString		*badgeNotification;
@property (nonatomic, copy)			NSString		*defaultSortOrder;
@property (nonatomic, copy)			NSString		*selectedTheme;
@property (nonatomic, copy)         NSString        *previewTheme;

#pragma mark Initialisation
- (id)initWithAllLists;

#pragma mark Reload/Load Methods
- (void)reloadAllLists;
- (void)reloadWithMasterTaskListCollection:(TaskListCollection *)masterTaskListCollection;
- (void)reloadWithMasterTaskListCollection:(TaskListCollection *)masterTaskListCollection usingList:(NSArray *)listArray;
- (void)loadWithTasksCreatedAfter:(NSString *)dateTimeString usingTaskCollection:(TaskListCollection *)masterTaskListCollection getNonSynced:(BOOL)getNonSynced;
- (void)loadWithTasksModifiedAfter:(NSString *)dateTimeString usingTaskCollection:(TaskListCollection *)masterTaskListCollection;
- (void)loadWithTaskListsCreatedAfter:(NSString *)dateTimeString usingTaskCollection:(TaskListCollection *)masterTaskListCollection;

#pragma mark Filter Methods
- (void)filterListsUsingListItemTagCollection:(ListItemTagCollection *)listItemTagCollection
				andOriginalTaskListCollection:(TaskListCollection *)originalTaskListCollection;

#pragma mark Archive Methods
- (NSInteger)archiveAllCompletedListsToArchiveList:(ArchiveList *)archiveList;
- (NSInteger)archiveAllCompletedListsUsingDuration:(NSInteger)duration;

#pragma mark Select/Get Methods
- (NSInteger)getIndexOfTaskListWithTitle:(NSString *)theTitle;
- (NSString *)getListTitleForListWithID:(NSInteger)theListID;
- (NSInteger)getIndexOfTaskListWithListItemID:(NSInteger)theListItemID;
- (NSInteger)getIndexOfTaskListWithID:(NSInteger)theListID; 
- (NSInteger)getIndexOfTaskListWithToodledoFolderID:(NSInteger)theToodledoFolderID;
- (TaskList *)getTaskListWithID:(NSInteger)theListID;
- (NSInteger)getShowCompletedDurationEnumForData:(NSString *)theData;
- (NSInteger)getNumberOfCompletedListItems;
- (NSInteger)getNumberOfCompletedLists;
- (ListItem *)getListItemReferenceFromListItemWithToodledoTaskID:(SInt64)theTaskID;
- (NSInteger)getCountOfTaskListWithTitle:(NSString *)theTitle;
- (NSString *)getImageNameForSection:(NSString *)section;
- (NSString *)getPreviewImageNameForSection:(NSString *)section;
- (UIColor *)getColourForSection:(NSString *)section;
- (UIColor *)getPreviewColourForSection:(NSString *)section;
- (NSString *)getTileViewButtonType;
- (NSString *)getPreviewTileViewButtonType;

#pragma mark Move Methods
- (void)moveListItemWithID:(NSInteger)theListItemID fromTaskListWithID:(NSInteger)fromTaskListID toTaskListWithID:(NSInteger)toTaskListID; 

#pragma mark Query Methods
- (BOOL)doesListExistWithTitle:(NSString *)theTitle orFolderID:(NSInteger)folderID;
- (BOOL)doesListItemExistWithToodledoServerID:(SInt64)theToodledoServerID;

#pragma mark Update Methods
- (void)findAndUpdateListItem:(ListItem *)listItemUpdate;
- (BOOL)insertListItem:(ListItem *)listItem atCorrectLocationUsingArchiveList:(ArchiveList *)archiveList;
- (void)updateWithToodledoFolder:(ToodledoFolder *)folder;

#pragma mark Sort Methods
//- (void)moveFromListWithID:(NSInteger)fromID toListWithID:(NSInteger)toID; 
- (void)moveObjectWithListID:(NSUInteger)fromID toObjectWithListID:(NSUInteger)toID;

#pragma mark Database Methods
- (void)commitListOrderChanges;
- (void)updateListOrdersInDatabase;

#pragma mark Delete Methods
- (void)deleteAllLists:(BOOL)isPermanent;
- (void)deleteListAtIndex:(NSInteger)index permanent:(BOOL)isPermanent;

#pragma mark PDF Mail Methods
- (NSString *)createPDFMessageWithTagCollection:(TagCollection *)tagCollection showSubtasks:(BOOL)showSubtasks;
- (NSString *)getTextMailMessageWithTagCollection:(TagCollection *)tagCollection showSubtasks:(BOOL)showSubtasks;

@end
