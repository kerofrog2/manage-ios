//
//  PropertyCollection.h
//  Manage
//
//  Created by Cliff Viegas on 5/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	EnumPropertyCollectionApplication = 0,
	EnumPropertyCollectionGeneral = 1,
	EnumPropertyCollectionApplicationList = 2,  // For getting subset without backup and sync options
    EnumPropertyCollectionSync = 3              // Syncing options
} EnumPropertyCollection;

@interface PropertyCollection : NSObject {
	NSMutableArray		*properties;	// List of properties in the collection
	NSString			*title;			// Title for the collection
}

@property (nonatomic, retain)	NSMutableArray	*properties;
@property (nonatomic, copy)		NSString		*title;

#pragma mark Initialisation
- (id)initWithCollectionType:(NSInteger)collectionType;

@end
