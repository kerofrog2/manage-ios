//
//  DeletedListItemCollection.h
//  Manage
//
//  Created by Cliff Viegas on 30/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
#import "DeletedListItem.h"

@interface DeletedListItemCollection : NSObject {
	NSMutableArray		*deletedListItems;
}

@property (nonatomic, retain)	NSMutableArray	*deletedListItems;

#pragma mark Load Methods
- (void)loadWithListItemsDeletedAfter:(NSString *)dateTimeString;

@end
