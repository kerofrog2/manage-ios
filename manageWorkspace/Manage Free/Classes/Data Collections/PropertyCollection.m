//
//  PropertyCollection.m
//  Manage
//
//  Created by Cliff Viegas on 5/10/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "PropertyCollection.h"

// Data Models
#import "PropertyDetail.h"

// In app purchase manager
#import "InAppPurchaseManager.h"

// Helpers
#import "MethodHelper.h"

// Private Methods
@interface PropertyCollection()
- (NSArray *)getApplicationSettingsArray;
- (NSArray *)getListApplicationSettingsArray;
- (NSArray *)getGeneralSettingsArray;
- (NSArray *)getSyncSettingsArray;
@end

@implementation PropertyCollection

@synthesize properties;
@synthesize title;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	[properties release];
	[title release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithCollectionType:(NSInteger)collectionType {
	if ((self = [super init])) {
		if (collectionType == EnumPropertyCollectionApplication) {
			self.properties = [NSMutableArray arrayWithArray:[self getApplicationSettingsArray]];
			self.title = @"Settings";
		} else if (collectionType == EnumPropertyCollectionGeneral) {
			self.properties = [NSMutableArray arrayWithArray:[self getGeneralSettingsArray]];
			self.title = @"General Settings";
		} else if (collectionType == EnumPropertyCollectionApplicationList) {
			self.properties = [NSMutableArray arrayWithArray:[self getListApplicationSettingsArray]];
			self.title = @"Settings";
		} else if (collectionType == EnumPropertyCollectionSync) {
            self.properties = [NSMutableArray arrayWithArray:[self getSyncSettingsArray]];
            self.title = @"Sync Settings";
        }
	}
	return self;
}

#pragma mark -
#pragma mark Load Methods

// Return the array of sync setting properties
- (NSArray *)getSyncSettingsArray {
    PropertyDetail *a1 = [[[PropertyDetail alloc] initWithName:@"ToodledoAutoSync" andFriendlyName:@"Auto Sync on Startup"
													andSection:@"" andPropertyType:EnumPropertyTypeBoolean] autorelease];
	
	PropertyDetail *a2 = [[[PropertyDetail alloc] initWithName:@"ToodledoServerHasConflictPriority" andFriendlyName:@"Conflict Priority" andSection:@""
											   andPropertyType:EnumPropertyTypeList] autorelease];
	NSArray *a2List = [NSArray arrayWithObjects:@"Toodledo", @"Local", nil];
	a2.description = @"Conflict priority setting only relates to lists (Toodledo refers to them as folders).  Conflict priority for tasks is handled by which task was most recently modified (local or Toodledo).";
	[a2.list addObjectsFromArray:a2List];
    
    NSArray *array1 = [NSArray arrayWithObjects:a1, a2, nil];
	NSArray *newArray = [NSArray arrayWithObjects:array1, nil];
	return newArray;
}

// Return the array of application setting properties
- (NSArray *)getApplicationSettingsArray {
    PropertyDetail *adMod = [[[PropertyDetail alloc] initWithName:@"ForAds" andFriendlyName:@"ForAds"
													andSection:@"Ads" andPropertyType:EnumPropertyTypeCustom] autorelease];
    
	PropertyDetail *a1 = [[[PropertyDetail alloc] initWithName:@"GeneralSettings" andFriendlyName:@"General Settings"
													andSection:@"" andPropertyType:EnumPropertyTypeCustom] autorelease];
    
    PropertyDetail *a2 = [[[PropertyDetail alloc] initWithName:@"SyncSettings" andFriendlyName:@"Sync Settings"
													andSection:@"" andPropertyType:EnumPropertyTypeCustom] autorelease];
    
	PropertyDetail *a3 = [[[PropertyDetail alloc] initWithName:@"ManageTags" andFriendlyName:@"Manage Tags"
													andSection:@"" andPropertyType:EnumPropertyTypeCustom] autorelease];
	
    
	/*PropertyDetail *b = [[[PropertyDetail alloc] initWithName:@"ShowCompleted" andFriendlyName:@"Show"
     andSection:@"Completed Tasks and Lists" andPropertyType:EnumPropertyTypeBoolean] autorelease];
     
     PropertyDetail *b1 = [[[PropertyDetail alloc] initWithName:@"ShowCompleted" andFriendlyName:@"Show"
     andSection:@"Completed Tasks and Lists" andPropertyType:EnumPropertyTypeBoolean] autorelease];
     
     //NSArray *b1List = [NSArray arrayWithObjects:@""
     PropertyDetail *b2 = [[[PropertyDetail alloc] initWithName:@"ShowCompletedDuration" andFriendlyName:@"Duration"
     andSection:@"Completed Tasks and Lists" andPropertyType:EnumPropertyTypeList] autorelease];
     NSArray *b2List = [NSArray arrayWithObjects:@"One Day", @"One Week", @"Four Weeks", @"Always", nil];
     b2.description = @"The duration that completed lists and tasks will be visible";
     [b2.list addObjectsFromArray:b2List];*/
	
	PropertyDetail *b1 = [[[PropertyDetail alloc] initWithName:@"ArchiveItems" andFriendlyName:@"Archive"
													andSection:@"Archive Tasks and Lists" andPropertyType:EnumPropertyTypeList] autorelease];
	NSArray *b1List = [NSArray arrayWithObjects:@"Daily", @"Weekly", @"On Application Launch", @"Manually", nil];
	b1.description = @"When or how completed lists and tasks are archived";
	[b1.list addObjectsFromArray:b1List];
	
	PropertyDetail *c1 = [[[PropertyDetail alloc] initWithName:@"Synchronize" andFriendlyName:@"Synchronize"
													andSection:@"Syncing & Backup" andPropertyType:EnumPropertyTypeCustom] autorelease];
	
	PropertyDetail *c2 = [[[PropertyDetail alloc] initWithName:@"BackupDB" andFriendlyName:@"Backup"
													andSection:@"Syncing & Backup" andPropertyType:EnumPropertyTypeCustom] autorelease];
	
    PropertyDetail *d1 = [[[PropertyDetail alloc] initWithName:@"RatingReview" andFriendlyName:@"Leave a Rating or Review"
                                                    andSection:@"About Manage" andPropertyType:EnumPropertyTypeCustom] autorelease];
    
    PropertyDetail *d3 = [[[PropertyDetail alloc] initWithName:@"SupportPage" andFriendlyName:@"Support Page"
                                                    andSection:@"About Manage" andPropertyType:EnumPropertyTypeCustom] autorelease];
    
	PropertyDetail *d4 = [[[PropertyDetail alloc] initWithName:@"VersionNumber" andFriendlyName:@"Version"
													andSection:@"About Manage" andPropertyType:EnumPropertyTypeDisplay] autorelease];
    
    PropertyDetail *d5 = [[[PropertyDetail alloc] initWithName:@"SendReport" andFriendlyName:@"Send Bug Report"
													andSection:@"About Manage" andPropertyType:EnumPropertyTypeCustom] autorelease];
	
    PropertyDetail *aa1 = [[[PropertyDetail alloc] initWithName:@"SelectedTheme" andFriendlyName:@"Current Theme"
                                                     andSection:@"Themes" andPropertyType:EnumPropertyTypeList] autorelease];
    
	NSArray *aa1List = [NSArray arrayWithObjects:@"Classic", @"Autumn Dreams", @"Hippy Wonderland", @"Red Rocket", @"Gold Rush", @"Purple Haze", @"Nitro Burn", @"Sweet Cravings", @"Old Timer", @"Ninja Bunnies", @"Christmas Wonderland", nil];
    
	[aa1.list addObjectsFromArray:aa1List];
    
//    // Check for premium themes
//    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemePurpleHazeID]) {
//        [aa1.list addObject:@"Purple Haze"];
//    }
//    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeNitroBurnID]) {
//        [aa1.list addObject:@"Nitro Burn"];
//    }
//    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeSweetCravingsID]) {
//        [aa1.list addObject:@"Sweet Cravings"];
//    }
//    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeOldTimerID]) {
//        [aa1.list addObject:@"Old Timer"];
//    }
//    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchasePremiumThemeNinjaBunniesID]) {
//        [aa1.list addObject:@"Ninja Bunnies"];
//    }
    
//    PropertyDetail *aa2 = [[[PropertyDetail alloc] initWithName:@"PremiumThemes" andFriendlyName:@"Premium Themes" andSection:@"Current Theme" andPropertyType:EnumPropertyTypeCustom] autorelease];
    
    NSArray *adsArray = [NSArray arrayWithObject:adMod];
    
	NSMutableArray *array1 = [NSMutableArray arrayWithObjects: a1, a2, a3, nil];
#ifdef IS_FREE_BUILD
    // Check to see whether advertising has been purchased
    if ([[InAppPurchaseManager sharedPurchaseManager] contentPurchased:kInAppPurchaseRemoveAdvertisingID] == FALSE) {
        PropertyDetail *a4 = [[[PropertyDetail alloc] initWithName:@"RemoveAdvertising" andFriendlyName:@"Remove Advertising"
                                                        andSection:@"" andPropertyType:EnumPropertyTypeCustom] autorelease];
        
        [array1 addObject:a4];
    }
#endif
    NSArray *array1a = [NSArray arrayWithObjects:aa1, nil];
	NSArray *array2 = [NSArray arrayWithObjects:b1, nil];
	NSArray *array3 = [NSArray arrayWithObjects:c1, c2, nil];
	NSArray *array4 = [NSArray arrayWithObjects:d1, d3, d5, d4, nil];
    NSArray *newArray;
    if ([MethodHelper connectedToInternet]) {
        newArray = [NSArray arrayWithObjects:array1, array1a, array2, array3, array4, adsArray, nil];
    } else {
        newArray = [NSArray arrayWithObjects:array1, array1a, array2, array3, array4, nil];
    }
	return newArray;
}

// Returns the array of application settings properties for when settings selected from 'list view'
- (NSArray *)getListApplicationSettingsArray {
	PropertyDetail *a1 = [[[PropertyDetail alloc] initWithName:@"GeneralSettings" andFriendlyName:@"General Settings"
													andSection:@"" andPropertyType:EnumPropertyTypeCustom] autorelease];
	
	PropertyDetail *a2 = [[[PropertyDetail alloc] initWithName:@"ManageTags" andFriendlyName:@"Manage Tags"
													andSection:@"Tags" andPropertyType:EnumPropertyTypeCustom] autorelease];
	
    /*PropertyDetail *b = [[[PropertyDetail alloc] initWithName:@"ShowCompleted" andFriendlyName:@"Show"
	 andSection:@"Completed Tasks and Lists" andPropertyType:EnumPropertyTypeBoolean] autorelease];
	 //NSArray *b1List = [NSArray arrayWithObjects:@""
	 PropertyDetail *b2 = [[[PropertyDetail alloc] initWithName:@"ShowCompletedDuration" andFriendlyName:@"Duration"
	 andSection:@"Completed Tasks and Lists" andPropertyType:EnumPropertyTypeList] autorelease];
	 NSArray *b2List = [NSArray arrayWithObjects:@"One Day", @"One Week", @"Four Weeks", @"Always", nil];
	 b2.description = @"The duration that completed lists and tasks will be visible";
	 [b2.list addObjectsFromArray:b2List];*/
	
	PropertyDetail *b1 = [[[PropertyDetail alloc] initWithName:@"ArchiveItems" andFriendlyName:@"Archive"
													andSection:@"Archive Tasks and Lists" andPropertyType:EnumPropertyTypeList] autorelease];
	NSArray *b1List = [NSArray arrayWithObjects:@"Daily", @"Weekly", @"On Application Launch", @"Manually", nil];
	b1.description = @"When or how completed lists and tasks are archived";
	[b1.list addObjectsFromArray:b1List];
	
	PropertyDetail *d1 = [[[PropertyDetail alloc] initWithName:@"VersionNumber" andFriendlyName:@"Version"
													andSection:@"About Manage" andPropertyType:EnumPropertyTypeDisplay] autorelease];
	
	NSArray *array1 = [NSArray arrayWithObjects:a1, a2, nil];
	NSArray *array2 = [NSArray arrayWithObjects:b1, nil];
	NSArray *array4 = [NSArray arrayWithObjects:d1, nil];
	NSArray *newArray = [NSArray arrayWithObjects:array1, array2, array4, nil];
	return newArray;
}

- (NSArray *)getGeneralSettingsArray {
	PropertyDetail *a1 = [[[PropertyDetail alloc] initWithName:@"PocketReminder" andFriendlyName:@"Pocket Reminder"
													andSection:@"" andPropertyType:EnumPropertyTypeBoolean] autorelease];
    
    PropertyDetail *a7 = [[[PropertyDetail alloc] initWithName:@"HideCompletedTasks" andFriendlyName:NSLocalizedString(@"HIDE_COMPLETED_TASKS", nil)
													andSection:@"" andPropertyType:EnumPropertyTypeBoolean] autorelease];
    
	
	PropertyDetail *a2 = [[[PropertyDetail alloc] initWithName:@"DefaultView" andFriendlyName:@"Default View" andSection:@""
											   andPropertyType:EnumPropertyTypeList] autorelease];
	NSArray *a2List = [NSArray arrayWithObjects:@"List", @"Thumbnail", nil];
	a2.description = @"The default view that is shown on startup";
	[a2.list addObjectsFromArray:a2List];

	PropertyDetail *a3 = [[[PropertyDetail alloc] initWithName:@"NewListOrder" andFriendlyName:@"New List Order" andSection:@"" andPropertyType:EnumPropertyTypeList] autorelease];
	NSArray *a3List = [NSArray arrayWithObjects:@"Beginning", @"End", nil];
	a3.description = @"The order in which new lists are added";
	[a3.list addObjectsFromArray:a3List];
	
	PropertyDetail *a4 = [[[PropertyDetail alloc] initWithName:@"NewTaskOrder" andFriendlyName:@"New Task Order" andSection:@"" andPropertyType:EnumPropertyTypeList] autorelease];
	NSArray *a4List = [NSArray arrayWithObjects:@"Beginning", @"End", nil];
	a4.description = @"The order in which new tasks are added (Please note that new 'scribble' tasks will always be added at the beginning)";
	[a4.list addObjectsFromArray:a4List];
	
	//PropertyDetail *a4 = [[[PropertyDetail alloc] initWithName:@"DefaultEmail" andFriendlyName:@"Email" andSection:@"" andPropertyType:EnumPropertyTypeString] autorelease];
	//a4.description = @"The default email address for exporting lists to";
	
	PropertyDetail *a5 = [[[PropertyDetail alloc] initWithName:@"BadgeNotification" andFriendlyName:@"Badge Notification"
													andSection:@"" andPropertyType:EnumPropertyTypeList] autorelease];
	NSArray *a5List = [NSArray arrayWithObjects:@"No Badge", @"Due and Overdue Tasks", @"Overdue Tasks", nil];
	a5.description = @"The types of tasks that will be counted in the Application Icon badge totals";
	[a5.list addObjectsFromArray:a5List];
	
	// Default sort order settings
	PropertyDetail *a6 = [[[PropertyDetail alloc] initWithName:@"DefaultSortOrder" andFriendlyName:@"Default Sort Order"
													andSection:@"" andPropertyType:EnumPropertyTypeList] autorelease];
    
	NSArray *a6List = [NSArray arrayWithObjects:@"List", @"Priority", @"Date", @"Completed", @"Last Selected", nil];
	a6.description = @"The default sort order for your lists.  'Last Selected' will keep the last selected sort order for each list for the current session.";
	[a6.list addObjectsFromArray:a6List];
    
    // Setting Default Panel View
    PropertyDetail *a8 = [[[PropertyDetail alloc] initWithName:@"DefaultPanelView" andFriendlyName:@"Default Panel View" andSection:@""
											   andPropertyType:EnumPropertyTypeList] autorelease];
    NSArray *a8List = [NSArray arrayWithObjects:@"Drawing Tools", @"Calendar / Lists", nil];
	a8.description = @"The default panel view that is shown on startup";
	[a8.list addObjectsFromArray:a8List];
	
	NSArray *array1 = [NSArray arrayWithObjects:a1, a7, a2, a3, a4, a5, a6, a8, nil];
	NSArray *newArray = [NSArray arrayWithObjects:array1, nil];
	return newArray;
}

@end
