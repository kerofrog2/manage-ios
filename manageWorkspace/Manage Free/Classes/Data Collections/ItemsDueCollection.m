//
//  ItemsDueCollection.m
//  Manage
//
//  Created by Cliff Viegas on 15/07/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "ItemsDueCollection.h"

// Data Models
#import "ListItem.h"
#import "TaskList.h"

// Data Collections
#import "TaskListCollection.h"

// Business Layer
#import "BLListItem.h"

// Data Layer
#import "SQLiteAccess.h"

// Dictionary Method Helper
#import "DMHelper.h"

@implementation ItemsDueCollection

@synthesize listItems;
//@synthesize taskListCollection;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	//[self.taskListCollection release];
    [self.listItems release];
	[currentParentListTitle release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithListParentTitle:(NSString *)theTitle andParentListID:(NSInteger)theParentListID 
		andTaskListCollection:(TaskListCollection *)theTaskListCollection {
	if ((self = [super init])) {
		self.listItems = [NSMutableArray array];
		
		// Assign the reference to the task list collection
		refTaskListCollection = theTaskListCollection;
		
		// Assign the current parent list title
		currentParentListTitle = theTitle;

		// Set the current parent list ID
		currentParentListID = theParentListID;
		
		// Alloc the task list collection
		//self.taskListCollection = [[TaskListCollection alloc] init];
		
		[self updateListItemsWithListParentTitle:theTitle andParentListID:theParentListID];
	}
	return self;
}

#pragma mark -
#pragma mark Update Methods

- (void)sortListItems {
	// Sort the list items
	NSSortDescriptor *sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"dueDate" 
																	ascending:YES] autorelease];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	NSArray *sortedArray = [self.listItems sortedArrayUsingDescriptors:sortDescriptors];
	[self.listItems removeAllObjects];
	[self.listItems addObjectsFromArray:sortedArray];
}

- (void)updateListItemsWithListItem:(ListItem *)listItem {
	NSInteger listItemIndex = [self getIndexOfListItemWithID:listItem.listItemID];
	
	if (listItemIndex == -1 && [listItem completed] == FALSE && [listItem.dueDate length] > 0) {
		
		// Not found in list, completed is false and has due date
		ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
		[self.listItems addObject:newListItem];
		[newListItem release];
		
		// Now need to sort
		[self sortListItems];
	} else if (listItemIndex != -1 && [listItem completed] == FALSE && [listItem.dueDate length] > 0) {
		// Found, update the indexed list item
		ListItem *newListItem = [self.listItems objectAtIndex:listItemIndex];
		[newListItem updateSelfWithListItem:listItem];
		
		// Now need to sort
		[self sortListItems];
	} else if (listItemIndex != -1 && [listItem completed] == TRUE) {
		// Found in the list and list item completed is true, remove it
		[self.listItems removeObjectAtIndex:listItemIndex];
	} else if (listItemIndex != -1 && [listItem.dueDate length] == 0) {
		// If the date is now zero length, then need to remove it
		[self.listItems removeObjectAtIndex:listItemIndex];
	}
	
	
}

- (void)updateListItemsWithListParentTitle:(NSString *)theTitle andParentListID:(NSInteger)theParentListID {
	// Remove any list items in the array
	[self.listItems removeAllObjects];
	
    if ([refTaskListCollection.lists retainCount] > 0) {
        for (TaskList *taskList in refTaskListCollection.lists) {
            
            // Get any items with a due date
            for (ListItem *listItem in taskList.listItems) {
                if (listItem.dueDate != nil && [listItem.dueDate length] > 0 && listItem.completed == FALSE) {
                    // Found a list item with a due date
                    ListItem *newListItem = [[ListItem alloc] initWithListItemCopy:listItem];
                    [self.listItems addObject:newListItem];
                    [newListItem release];
                    //NSLog(@"newlistitem retaincount: %d", [newListItem retainCount]);
                }
            }
            
        }
    }

	// Sort the list items
	[self sortListItems];
}

#pragma mark -
#pragma mark Get Methods

- (NSInteger)getIndexOfListItemWithID:(NSInteger)theListItemID {
	NSInteger i = 0;
	for (ListItem *listItem in self.listItems) {
		if (listItem.listItemID == theListItemID) {
			return i;
		}
		i++;
	}
	
	// Not found, return -1
	return -1;
}

@end
