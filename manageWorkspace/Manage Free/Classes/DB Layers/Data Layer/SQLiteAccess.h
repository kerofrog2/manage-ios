//
//  SQLiteAccess.h
//  iDesign
//
//  Created by Cliff Viegas on 24/02/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

@interface SQLiteAccess : NSObject {
}

+ (NSString *)selectOneValueSQL:(NSString *)sql;
+ (NSArray *)selectManyValuesWithSQL:(NSString *)sql;
+ (NSDictionary *)selectOneRowWithSQL:(NSString *)sql;
+ (NSArray *)selectManyRowsWithSQL:(NSString *)sql;
+ (NSNumber *)insertWithSQL:(NSString *)sql;
+ (void)updateWithSQL:(NSString *)sql;
+ (void)deleteWithSQL:(NSString *)sql;

@end