//
//  BLListItem.h
//  Manage
//
//  Created by Cliff Viegas on 17/06/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class ListItem;

// For enum keys
#import "TaskList.h"

// CREATE TABLE "ListItem" ("ListItemID" INTEGER PRIMARY KEY  NOT NULL , "ListID" INTEGER NOT NULL , 
// "CategoryID" INTEGER, "PriorityID" INTEGER, "Title" VARCHAR, "CreationDateTime" VARCHAR NOT NULL , 
// "DueDate" VARCHAR, "Notes" TEXT, "Scribble" VARCHAR, "Completed" INTEGER)


// CREATE TABLE "ListItem" ("ListItemID" INTEGER PRIMARY KEY  NOT NULL , "ListID" INTEGER NOT NULL , "CategoryID" INTEGER, 
// "PriorityID" INTEGER, "Title" VARCHAR, "CreationDateTime" VARCHAR NOT NULL , "DueDate" VARCHAR, "Notes" TEXT, 
// "Scribble" VARCHAR, "Completed" INTEGER, "ItemOrder" INTEGER NOT NULL  DEFAULT 0, "ParentListItemID" INTEGER, 
// "SubItemOrder" INTEGER NOT NULL  DEFAULT 0)


@interface BLListItem : NSObject {

}

#pragma mark SELECT Methods
+ (NSArray *)getAllListItemsWithListID:(NSInteger)listID;
+ (NSArray *)getAllArchivedListItems;
+ (NSArray *)getAllListItemsWithListID:(NSInteger)listID sortedBy:(NSInteger)sorted;
+ (NSArray *)getAllListItemsWithDueDate;
+ (NSArray *)getDateDifferenceForAllListItemsWithDueDate;
+ (NSInteger)getNextItemOrderForListID:(NSInteger)listID;
+ (NSInteger)getFirstItemOrderForListID:(NSInteger)listID;
+ (NSInteger)getNextSubItemOrderForListItemID:(NSInteger)listItemID;
+ (NSInteger)getNumberOfListItemsWithDueDate;

#pragma mark INSERT Methods
+ (NSInteger)insertNewListItem:(ListItem *)listItem;

#pragma mark UPDATE Methods
+ (void)updateDatabaseWithListItem:(ListItem *)listItem;
+ (void)updateItemOrder:(NSInteger)newOrder forListItemID:(NSInteger)listItemID;
+ (void)updateSubItemOrder:(NSInteger)newOrder forListItemID:(NSInteger)listItemID;
//+ (void)updateDatabaseWithListItem:(ListItem *)listItem;

#pragma mark DELETE Methods
+ (void)deleteListItemWithListItemID:(NSInteger)listItemID;
+ (void)deleteAllArchivedListItems;

@end
