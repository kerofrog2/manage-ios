//
//  BLDeletedList.m
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "BLDeletedList.h"

// Data Layer
#import "SQLiteAccess.h"

// Data Models
#import "DeletedTaskList.h"

// Method Helper
#import "MethodHelper.h"

@implementation BLDeletedList

#pragma mark -
#pragma mark SELECT Methods

+ (NSArray *)getAllDeletedLists {
	NSString *sql = @"SELECT * FROM DeletedList";
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

#pragma mark -
#pragma mark INSERT Methods

+ (void)insertNewDeletedList:(DeletedTaskList *)deletedList {
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO DeletedList (ListID, Title, ToodledoFolderID, DeletedDateTime, IsNote) "
					 "VALUES (%d, %@, %qi, %@, %d)",
					 deletedList.listID, [MethodHelper escapeStringsForSQL:deletedList.title],
					 deletedList.toodledoFolderID, [MethodHelper escapeStringsForSQL:deletedList.deletedDateTime], deletedList.isNote];
	//NSLog(@"%@", sql);
	[SQLiteAccess insertWithSQL:sql];
}

#pragma mark -
#pragma mark DELETE Methods

+ (void)emptyDatabase {
	NSString *sql = @"DELETE FROM DeletedList";
	[SQLiteAccess deleteWithSQL:sql];
}

@end
