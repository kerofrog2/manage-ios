//
//  BLTag.h
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class Tag;

@interface BLTag : NSObject {

}

#pragma mark SELECT Methods
+ (NSArray *)getAllTags;
+ (NSArray *)getAllTagsWithParentTagID:(NSInteger)parentTagID;
+ (NSArray *)getListItemTagsWithListItemID:(NSInteger)listItemID;
+ (NSInteger)getNextTagOrderForParentTagID:(NSInteger)parentTagID;
+ (NSInteger)getNextTagOrderForTag:(Tag *)tag;
+ (NSInteger)getNewTagID;

#pragma mark INSERT Methods
+ (NSInteger)insertNewTag:(Tag *)tag;
+ (NSInteger)insertNewTagWithID:(NSInteger)theTagID andName:(NSString *)theTagName;

#pragma mark DELETE Methods
+ (void)deleteTagWithID:(NSInteger)tagID;
+ (void)deleteAllTags;

#pragma mark UPDATE Methods
+ (void)updateTagOrder:(NSInteger)newOrder forTagID:(NSInteger)tagID;
+ (void)updateTag:(Tag *)tag;

@end
