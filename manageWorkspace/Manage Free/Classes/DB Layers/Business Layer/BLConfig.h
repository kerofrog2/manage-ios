//
//  BLConfig.h
//  Manage
//
//  Created by Cliff Viegas on 5/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//
//  Used for various database checking and updating of tables and fields

#import <Foundation/Foundation.h>


@interface BLConfig : NSObject {

}

#pragma mark Check Methods
+ (BOOL)checkIfColumn:(NSString *)column existsInTable:(NSString *)tableName;
+ (BOOL)checkIfTableExists:(NSString *)tableName;

#pragma mark Alter Methods
+ (void)addField:(NSString *)column toTable:(NSString *)tableName withType:(NSString *)type; 
+ (void)addField:(NSString *)column toTable:(NSString *)tableName withType:(NSString *)type andNullable:(BOOL)isNullable andDefault:(NSString *)defaultValue;
+ (void)createTable:(NSString *)tableName withPrimaryKey:(NSString *)primaryField ofType:(NSString *)type;
+ (void)createTable:(NSString *)tableName withPrimaryKey:(NSString *)primaryField 
	andSecondaryKey:(NSString *)secondaryField ofType:(NSString *)type;

#pragma mark Helper Methods
+ (void)createApplicationSettingTable;
+ (void)createAlarmTable;
+ (void)createDeletedListTable;
+ (void)createDeletedListItemTable;

@end
