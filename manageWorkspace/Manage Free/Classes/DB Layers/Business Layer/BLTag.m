//
//  BLTag.m
//  Manage
//
//  Created by Cliff Viegas on 9/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

// Header File
#import "BLTag.h"

// Data Layer
#import "SQLiteAccess.h"

// Data Models
#import "Tag.h"

// Method Helper
#import "MethodHelper.h"

@implementation BLTag

#pragma mark -
#pragma mark SELECT Methods

+ (NSArray *)getAllTags {
	NSString *sql = @"SELECT * FROM Tag ORDER BY TagDepth, TagOrder";
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

+ (NSArray *)getAllTagsWithParentTagID:(NSInteger)parentTagID {
	NSString *sql = [NSString stringWithFormat:@"SELECT * FROM Tag WHERE ParentTagID = %d ORDER BY TagOrder",parentTagID];
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

+ (NSArray *)getListItemTagsWithListItemID:(NSInteger)listItemID {
	NSString *sql = [NSString stringWithFormat:@"SELECT * FROM ListItemTag WHERE ListItemID = %d",
					 listItemID];
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

+ (NSInteger)getNextTagOrderForTag:(Tag *)tag {
	NSString *sql = @"SELECT TagOrder FROM Tag";
	
	if (tag.parentTagID != -1) {
		sql = [NSString stringWithFormat:@"%@ WHERE ParentTagID = %d "
			   "ORDER BY TagOrder DESC LIMIT 1", sql, tag.parentTagID];
	} else {
		sql = [NSString stringWithFormat:@"%@ WHERE ParentTagID = -1", sql];
	}
	
	NSString *largestTagOrder = [SQLiteAccess selectOneValueSQL:sql];
	NSInteger nextTagOrder = 0;
	if (largestTagOrder != nil) {
		nextTagOrder = [largestTagOrder integerValue] + 1;
	}
	return nextTagOrder;
}

+ (NSInteger)getNextTagOrderForParentTagID:(NSInteger)parentTagID {
	NSString *sql = @"SELECT TagOrder FROM Tag";
	
	if (parentTagID != -1) {
		sql = [NSString stringWithFormat:@"%@ WHERE ParentTagID = %d "
			   "ORDER BY TagOrder DESC LIMIT 1", sql, parentTagID];
	} else {
		sql = [NSString stringWithFormat:@"%@ WHERE ParentTagID = -1", sql];
	}
	
	NSString *largestTagOrder = [SQLiteAccess selectOneValueSQL:sql];
	NSInteger nextTagOrder = 0;
	if (largestTagOrder != nil) {
		nextTagOrder = [largestTagOrder integerValue] + 1;
	}
	return nextTagOrder;
}

+ (NSInteger)getNewTagID {
	NSString *sql = @"SELECT TagID FROM Tag ORDER BY TagID DESC LIMIT 1";
	NSString *newTagID = [SQLiteAccess selectOneValueSQL:sql];
	return [newTagID integerValue] + 1;
}

#pragma mark -
#pragma mark INSERT Methods

// Insert the passed page into the page table
+ (NSInteger)insertNewTag:(Tag *)tag {
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO Tag (TagID, Name, TagOrder, ParentTagID, TagDepth, Colour) "
					 "VALUES (%d, %@, %d, %d, %d, %@)",
					 tag.tagID, [MethodHelper escapeStringsForSQL:tag.name], tag.tagOrder, tag.parentTagID,
					 tag.tagDepth, [MethodHelper escapeStringsForSQL:tag.colour]];
	
	return [[SQLiteAccess insertWithSQL:sql] integerValue];
	
}

+ (NSInteger)insertNewTagWithID:(NSInteger)theTagID andName:(NSString *)theTagName {
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO Tag (TagID, Name) VALUES (%d, %@)",
					 theTagID, [MethodHelper escapeStringsForSQL:theTagName]];
	
	return [[SQLiteAccess insertWithSQL:sql] integerValue];
}

#pragma mark -
#pragma mark DELETE Methods

+ (void)deleteTagWithID:(NSInteger)tagID {
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM Tag WHERE TagID = %d", tagID];
	[SQLiteAccess deleteWithSQL:sql];
}

+ (void)deleteAllTags {
	NSString *sql = @"DELETE FROM Tag";
	[SQLiteAccess deleteWithSQL:sql];
}

#pragma mark -
#pragma mark UPDATE Methods

// Updates the tag order for the specified tag
+ (void)updateTagOrder:(NSInteger)newOrder forTagID:(NSInteger)tagID {
	NSString *sql = [NSString stringWithFormat:@"UPDATE Tag SET TagOrder = %d WHERE TagID = %d", newOrder, tagID];
	[SQLiteAccess updateWithSQL:sql];
}

+ (void)updateTag:(Tag *)tag {
	NSString *sql = [NSString stringWithFormat:@"UPDATE Tag SET Name = %@, TagOrder = %d, "
					 "ParentTagID = %d, TagDepth = %d, Colour = %@ WHERE TagID = %d",
					 [MethodHelper escapeStringsForSQL:tag.name], tag.tagOrder,
					 tag.parentTagID, tag.tagDepth, [MethodHelper escapeStringsForSQL:tag.colour], tag.tagID];
	[SQLiteAccess updateWithSQL:sql];
}

@end
