//
//  BLAlarm.h
//  Manage
//
//  Created by Cliff Viegas on 14/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
@class Alarm;

@interface BLAlarm : NSObject {

}

#pragma mark SELECT Methods
+ (NSArray *)getAllAlarmsWithListItemID:(NSInteger)listItemID; 
+ (NSDictionary *)getAlarmWithAlarmGUID:(NSString *)alarmGUID;
+ (NSInteger)getNumberOfAlarmsWithListItemID:(NSInteger)listItemID;

#pragma mark INSERT Methods
+ (void)insertNewAlarm:(Alarm *)alarm;

#pragma mark DELETE Methods
+ (void)deleteAlarmWithGUID:(NSString *)alarmGUID;

#pragma mark UPDATE Methods
+ (void)updateDatabaseWithAlarm:(Alarm *)alarm; 

@end
