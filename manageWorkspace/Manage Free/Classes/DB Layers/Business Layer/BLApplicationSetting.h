//
//  BLApplicationSetting.h
//  Manage
//
//  Created by Cliff Viegas on 5/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <Foundation/Foundation.h>

// Data Models
#import "ApplicationSetting.h"

@interface BLApplicationSetting : NSObject {

}

#pragma mark SELECT Methods
+ (NSArray *)getAllApplicationSettings;
+ (NSArray *)getAllUserApplicationSettings;
+ (NSString *)getSettingData:(NSString *)settingName;
+ (NSString *)getVersionNumber;

#pragma mark UPDATE METHODS
+ (void)updateSetting:(NSString *)settingName withData:(NSString *)data;

#pragma mark INSERT Methods
+ (void)insertSetting:(ApplicationSetting *)appSetting;

#pragma mark Helper Methods
+ (NSString *)escapeStringsForSQL:(NSString *)inputString;
+ (BOOL)checkIfSettingExists:(NSString *)settingName;

@end
