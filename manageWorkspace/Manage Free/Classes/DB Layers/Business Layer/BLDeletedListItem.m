//
//  BLDeletedListItem.m
//  Manage
//
//  Created by Cliff Viegas on 29/03/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "BLDeletedListItem.h"

// Data Layer
#import "SQLiteAccess.h"

// Data Models
#import "DeletedListItem.h"

// Method Helper
#import "MethodHelper.h"

@implementation BLDeletedListItem

#pragma mark -
#pragma mark SELECT Methods

+ (NSArray *)getAllDeletedListItems {
	NSString *sql = @"SELECT * FROM DeletedListItem";
	//NSLog(@"%@", sql);
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

#pragma mark -
#pragma mark INSERT Methods

+ (void)insertNewDeletedListItem:(DeletedListItem *)deletedListItem {
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO DeletedListItem (ListItemID, Title, ListID, ParentListItemID, ToodledoTaskID, DeletedDateTime) "
					 "VALUES (%d, %@, %d, %d, %qi, %@)",
					 deletedListItem.listItemID, [MethodHelper escapeStringsForSQL:deletedListItem.title], deletedListItem.listID, deletedListItem.parentListItemID,
					 deletedListItem.toodledoTaskID, [MethodHelper escapeStringsForSQL:deletedListItem.deletedDateTime]];
	//NSLog(@"%@", sql);
	[SQLiteAccess insertWithSQL:sql];
}

#pragma mark -
#pragma mark DELETE Methods

+ (void)emptyDatabase {
	NSString *sql = @"DELETE FROM DeletedListItem";
	[SQLiteAccess deleteWithSQL:sql];
}

@end
