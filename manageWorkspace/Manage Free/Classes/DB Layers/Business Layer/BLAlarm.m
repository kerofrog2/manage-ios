//
//  BLAlarm.m
//  Manage
//
//  Created by Cliff Viegas on 14/01/11.
//  Copyright 2011 kerofrog. All rights reserved.
//

#import "BLAlarm.h"

// Data Layer
#import "SQLiteAccess.h"

// Data Models
#import "Alarm.h"

// Method Helper
#import "MethodHelper.h"

@implementation BLAlarm

#pragma mark -
#pragma mark SELECT Methods

+ (NSArray *)getAllAlarmsWithListItemID:(NSInteger)listItemID {
	NSString *sql = [NSString stringWithFormat:@"SELECT * FROM Alarm WHERE ListItemID = %d",
					 listItemID];
	return [SQLiteAccess selectManyRowsWithSQL:sql];
}

+ (NSDictionary *)getAlarmWithAlarmGUID:(NSString *)alarmGUID {
	NSString *sql = [NSString stringWithFormat:@"SELECT * FROM Alarm WHERE AlarmGUID = %@",
					 [MethodHelper escapeStringsForSQL:alarmGUID]];
	return [SQLiteAccess selectOneRowWithSQL:sql];
}

// Helper method used at application launch to check whether 'hasAlarms' field of listItem object
// needs to be updated due to there bing no more alarms left linked to that list item.
+ (NSInteger)getNumberOfAlarmsWithListItemID:(NSInteger)listItemID {
	NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM Alarm WHERE ListItemID = %d", listItemID];
	return [[SQLiteAccess selectOneValueSQL:sql] integerValue];
}

#pragma mark -
#pragma mark INSERT Methods

+ (void)insertNewAlarm:(Alarm *)alarm {
	NSString *sql = [NSString stringWithFormat:@"INSERT INTO Alarm (AlarmGUID, ListItemID, FireDateTime, Delay, "
					 "Measurement) Values (%@, %d, %@, %d, %@)",
					 [MethodHelper escapeStringsForSQL:alarm.alarmGUID],
					 alarm.listItemID, [MethodHelper escapeStringsForSQL:alarm.fireDateTime],
					 alarm.delay, [MethodHelper escapeStringsForSQL:alarm.measurement]];
	[SQLiteAccess insertWithSQL:sql];
}


#pragma mark -
#pragma mark DELETE Methods

+ (void)deleteAlarmWithGUID:(NSString *)alarmGUID {
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM Alarm WHERE AlarmGUID = %@",
					 [MethodHelper escapeStringsForSQL:alarmGUID]];
	[SQLiteAccess deleteWithSQL:sql];
}

#pragma mark -
#pragma mark UPDATE Methods

+ (void)updateDatabaseWithAlarm:(Alarm *)alarm {
	NSString *sql = [NSString stringWithFormat:@"UPDATE Alarm SET ListItemID = %d, FireDateTime = %@, "
					 "Delay = %d, Measurement = %@ WHERE AlarmGUID = %@",
					 alarm.listItemID, [MethodHelper escapeStringsForSQL:alarm.fireDateTime],
					 alarm.delay, [MethodHelper escapeStringsForSQL:alarm.measurement],
					 [MethodHelper escapeStringsForSQL:alarm.alarmGUID]];

	[SQLiteAccess updateWithSQL:sql];
}


@end
