//
//  ManageAppDelegate.h
//  Manage
//
//  Created by Cliff Viegas on 16/06/10.
//  Copyright kerofrog 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

// Google Analytics lib
#import "GAI.h"

// Dropbox
#import <dropbox/dropbox.h>


// Code for free build
#ifdef IS_FREE_BUILD
// Rev Mob
#import <RevMobAds/RevMobAds.h>

#else
// Code for Pro build
#import <AudioToolbox/AudioToolbox.h>
#endif

// Admob
#import "GADBannerViewDelegate.h"

//mMedia
#import "MMSDK.h"

// Data Collections
@class TaskListCollection;

@protocol ManageAppDelegate <NSObject>

-(void) didLoginDropBoxWithAccount:(DBAccount *)account;

@end

@interface ManageAppDelegate : NSObject <UIApplicationDelegate> {
	TaskListCollection			*masterTaskListCollection;
    UIWindow					*window;
	UINavigationController		*navigationController;
    
    UILocalNotification         *localNotifi;
    
    NSInteger       gNumberOfListItemsWithDueDate;
    
}

@property (nonatomic) BOOL            gRunToodledoAutoSync;

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) UINavigationController *navigationController;
@property (nonatomic, retain) UILocalNotification         *localNotifi;

// Google Analytics Tracker
@property(nonatomic, retain) id<GAITracker> tracker;

// Dropbox's variable.
@property (nonatomic, retain) DBFilesystem *dbFileSystem;

// Admob's variable
//@property (nonatomic, retain) GADBannerView *adBanner;

@property (nonatomic, retain) id<ManageAppDelegate> appDelegate;

#pragma mark Class Methods
- (void)createResourceFolders;

#pragma mark Custom Methods
- (void)archiveCheck;
- (void)databaseChecks;
- (void)updateSettings:(NSString *)version;

@end
