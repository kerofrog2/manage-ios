//
//  QuartzTagView.m
//  Manage
//
//  Created by Cliff Viegas on 11/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "QuartzTagView.h"

// Quartz 2D
#import <QuartzCore/QuartzCore.h>

@implementation QuartzTagView

@synthesize whiteBackground;
@synthesize tagColour;
@synthesize tagColourRed;
@synthesize tagColourBlue;
@synthesize tagColourGreen;
@synthesize tagColourAlpha;

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    [tagColour release];
    [super dealloc];
}

#pragma mark -
#pragma mark Initialisation

- (id)initWithFrame:(CGRect)aframe andTagColour:(NSString *)theTagColour {
    self = [super initWithFrame:aframe];
    if (self) {
        // Initialization code
		self.backgroundColor = [UIColor clearColor];
		self.whiteBackground = FALSE;
        self.tagColour = theTagColour;
        
        [self setTagColours];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawInContext:(CGContextRef)context {
	// As a bonus, we'll combine arcs to create a round rectangle!
	
	CGContextSetLineWidth(context, 0.5 * [UIScreen mainScreen].scale);
	
	// Drawing with a white stroke color
	CGContextSetRGBStrokeColor(context, 0.1, 0.1, 0.1, 1.0);
    
	// Set the fill color
	//CGContextSetRGBFillColor(context, 204.0 / 255.0, 174.0 / 255.0, 127.0 / 255.0, 1.0);
	CGContextSetRGBFillColor(context, self.tagColourRed / 255.0, 
                             self.tagColourGreen / 255.0, 
                             self.tagColourBlue / 255.0, self.tagColourAlpha);
    
	if (self.whiteBackground == TRUE) {
		CGContextSetRGBFillColor(context, 255.0, 255.0, 255.0, 1.0);
	}
	
	// If you were making this as a routine, you would probably accept a rectangle
	// that defines its bounds, and a radius reflecting the "rounded-ness" of the rectangle.
	//CGRect rrect = CGRectMake(210.0, 90.0, 60.0, 60.0);
	//CGRect rrect = CGRectMake(2, 2, 35.0, 35.0);
	CGRect rrect = CGRectMake(1, 1, self.frame.size.width - 2, self.frame.size.height - 2);
    CGFloat radius;
	if (rrect.size.height > 35) {
        radius = 14.0;
    } else {
        radius = 7.0;
    }
	// NOTE: At this point you may want to verify that your radius is no more than half
	// the width and height of your rectangle, as this technique degenerates for those cases.
	
	// In order to draw a rounded rectangle, we will take advantage of the fact that
	// CGContextAddArcToPoint will draw straight lines past the start and end of the arc
	// in order to create the path from the current position and the destination position.
	
	// In order to create the 4 arcs correctly, we need to know the min, mid and max positions
	// on the x and y lengths of the given rectangle.
	CGFloat minx = CGRectGetMinX(rrect), midx = CGRectGetMidX(rrect), maxx = CGRectGetMaxX(rrect);
	CGFloat miny = CGRectGetMinY(rrect), midy = CGRectGetMidY(rrect), maxy = CGRectGetMaxY(rrect);
	
	// Next, we will go around the rectangle in the order given by the figure below.
	//       minx    midx    maxx
	// miny    2       3       4
	// midy   1 9              5
	// maxy    8       7       6
	// Which gives us a coincident start and end point, which is incidental to this technique, but still doesn't
	// form a closed path, so we still need to close the path to connect the ends correctly.
	// Thus we start by moving to point 1, then adding arcs through each pair of points that follows.
	// You could use a similar tecgnique to create any shape with rounded corners.
	
	// Start at 1
	CGContextMoveToPoint(context, minx, midy);
	// Add an arc through 2 to 3
	CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
	// Add an arc through 4 to 5
	CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
	// Add an arc through 6 to 7
	CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
	// Add an arc through 8 to 9
	CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
	// Close the path
	CGContextClosePath(context);
	// Fill & stroke the path
	CGContextDrawPath(context, kCGPathFillStroke);
	
}

- (void)drawRect:(CGRect)rect {
	// Since we use the CGContextRef a lot, it is convienient for our demonstration classes to do the real work
	// inside of a method that passes the context as a parameter, rather than having to query the context
	// continuously, or setup that parameter for every subclass.
	[self drawInContext:UIGraphicsGetCurrentContext()];
}

#pragma mark -
#pragma mark Helper Methods

- (void)setTagColours {
    // CGContextSetRGBFillColor(context, 204.0 / 255.0, 174.0 / 255.0, 127.0 / 255.0, 1.0);
    // Use the passed colour to set tag colours
    if ([self.tagColour isEqualToString:NSLocalizedString(@"TAG_COLOR_BROWN", nil)]) {
        self.tagColourRed = 204.0;
        self.tagColourGreen = 174.0;
        self.tagColourBlue = 127.0;
        self.tagColourAlpha = 1.0;
    } else if ([self.tagColour isEqualToString:NSLocalizedString(@"TAG_COLOR_BLUE", nil)]) {
        // Blue colour
        self.tagColourRed = 120.0;
        self.tagColourGreen = 151.0;
        self.tagColourBlue = 201.0;
        self.tagColourAlpha = 1.0;
    } else if ([self.tagColour isEqualToString:NSLocalizedString(@"TAG_COLOR_RED", nil)]) {
        // Red colour
        self.tagColourRed = 201.0;
        self.tagColourGreen = 130.0;
        self.tagColourBlue = 120.0;
        self.tagColourAlpha = 1.0;
    } else if ([self.tagColour isEqualToString:NSLocalizedString(@"TAG_COLOR_GREEN", nil)]) {
        // Green colour
        self.tagColourRed = 175.0;
        self.tagColourGreen = 187.0;
        self.tagColourBlue = 84.0;
        self.tagColourAlpha = 1.0;
    } else if ([self.tagColour isEqualToString:NSLocalizedString(@"TAG_COLOR_PINK", nil)]) {
        // Pink colour
        self.tagColourRed = 229.0;
        self.tagColourGreen = 189.0;
        self.tagColourBlue = 224.0;
        self.tagColourAlpha = 1.0;
    } else if ([self.tagColour isEqualToString:NSLocalizedString(@"TAG_COLOR_PURPLE", nil)]) {
        // Purple colour
        self.tagColourRed = 204.0;
        self.tagColourGreen = 153.0;
        self.tagColourBlue = 255.0;
        self.tagColourAlpha = 1.0;
    } else if ([self.tagColour isEqualToString:NSLocalizedString(@"TAG_COLOR_YELLOW", nil)]) {
        // Yellow colour
        self.tagColourRed = 255.0;
        self.tagColourGreen = 255.0;
        self.tagColourBlue = 179.0;
        self.tagColourAlpha = 1.0;
    } else if ([self.tagColour isEqualToString:NSLocalizedString(@"TAG_COLOR_ORANGE", nil)]) {
        // Yellow colour
        self.tagColourRed = 255.0;
        self.tagColourGreen = 181.0;
        self.tagColourBlue = 107.0;
        self.tagColourAlpha = 1.0;
    } else {
        // Default brown colour
        self.tagColourRed = 204.0;
        self.tagColourGreen = 174.0;
        self.tagColourBlue = 127.0;
        self.tagColourAlpha = 1.0;
    }
}


@end
