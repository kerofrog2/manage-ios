//
//  QuartzTagView.h
//  Manage
//
//  Created by Cliff Viegas on 11/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface QuartzTagView : UIView {
	BOOL            whiteBackground;
    NSString        *tagColour;

    // Floats for tag colour settings
    CGFloat         tagColourRed;
    CGFloat         tagColourGreen;
    CGFloat         tagColourBlue;
    CGFloat         tagColourAlpha;
}

@property (nonatomic, assign)	BOOL		whiteBackground;
@property (nonatomic, copy)     NSString    *tagColour;
@property (nonatomic, assign)   CGFloat     tagColourRed;
@property (nonatomic, assign)   CGFloat     tagColourGreen;
@property (nonatomic, assign)   CGFloat     tagColourBlue;
@property (nonatomic, assign)   CGFloat     tagColourAlpha;

#pragma mark Initialisation
- (id)initWithFrame:(CGRect)frame andTagColour:(NSString *)theTagColour;

#pragma mark Helper Methods
- (void)setTagColours;

@end
