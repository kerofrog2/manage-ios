//
//  TestOpenGLView.m
//  Manage
//
//  Created by Cliff Viegas on 29/09/10.
//  Copyright 2010 kerofrog. All rights reserved.
//

#import "TestOpenGLView.h"


@implementation TestOpenGLView


/*
 I want to introduce a feature for pen-and-ink style finger drawing in my fairly successful iPhone/iPad app using OpenGL ES. I am looking to commission a developer who can work with me on this feature.
 
 The problem is quit simple. I need code to draw a pen-and-ink style stroke. 2 key requirements are:
 
 -1- I need a smooth curve between the points provided by the device when the user moves finger on the screen (i.e. need some type of bezier logic to estimate the points between).
 
 -2- The stroke should be velocity sensitive --- thickness should reduce with higher velocity.
 
 The stroke should resemble a traditional sketch pen.
 
 
 */

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
    [super dealloc];
}


@end
